<%
'******************************************************************************
' OPCIONES PARA DEBUGAR 
'******************************************************************************
blnDebugar=false
blnPintarSql=false
if session("codigoUsuario")="2"  then'or session("codigoUsuario")="84" then 'Alberto o Fideliza
    if instr(lcase(request.servervariables("SCRIPT_NAME")),"ajax")=0 then
        'blnDebugar=true 
        'blnPintarSql=true 
   end if
end if

'Variables para el control de cursores y conexiones abiertas
strCursorAbrir    = ""
strCursorCerrar   = ""
intCursorAbrir    = 0
intCursorCerrar   = 0
strConexionCerrar = ""
strConexionAbrir  = ""
intConexionAbrir  = 0
intConexionCerrar = 0
HoraInicio=now()
CommandTimeout = 240
ConnectionTimeout = 15
'******************************************************************************


'################################################################################################################
'#################################### CLASE PARA EL RECORDSET ###################################################

class DMWRecordset

	Private Rst

	Private Sub Class_Initialize()
		Set Rst = Server.CreateObject("ADODB.RecordSet")
    End Sub

	Public Sub MoveNext()
		'On Error resume next

        intInicio=now()
		Rst.MoveNext
        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - MOVENEXT </font><br>"
            end if	
        end if
		    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	Public Sub MoveLast()
		'On Error resume next
		    
        intInicio=now()
		Rst.MoveLast
        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - MOVELAST </font><br>"
            end if	
        end if
		    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub
    	
	Public Function EOF()
		On Error resume next

		EOF = Rst.EOF
	    
		if Err<>0 then
			EscribirError
		end if
		On Error GoTo 0
	End Function
	
	Public Function Clone()
		'On Error resume next

		Clone = Rst.Clone(adLockBatchOptimistic)
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Function
	
	Public Function BOF()
		'On Error resume next

		BOF = Rst.BOF

		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Function
	
	Public Sub MoveFirst()
		'On Error resume next

        intInicio=now()
		Rst.MoveFirst
        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - MOVEFIRST </font><br>"
            end if	
        end if
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub
	
	Public Sub MovePrevious()
		'On Error resume next

        intInicio=now()
		Rst.MovePrevious
        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - MOVEPREVIOUS </font><br>"
            end if	
        end if
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub
	
	Public Sub AddNew()
		'On Error resume next

		Rst.AddNew
	'response.write err.Description   
		if Err<>0 then
			EscribirError
		end if
		'On Error GoTo 0
	End Sub
	
	Public Sub Delete()
		'On Error resume next

		Rst.Delete
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub
	
	Public Sub Update()
		'On Error resume next
	    
		Rst.Update
	    
		if Err<>0 then
			EscribirError
		end if
		On Error GoTo 0
	End Sub

	Public Function RecordCount()

        intInicio=now()

            if not (Rst.BOF and Rst.EOF) then 
			    rsArray = Rst.GetRows() 
			    RecordCount = UBound(rsArray, 2) + 1 
			    Rst.movefirst
		    else
			    RecordCount = 0
		    end if


        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - RECORDCOUNT </font><br>"
            end if	
        end if

	End Function

	Public Function PageCount()
		'On Error resume next

		PageCount = Rst.PageCount
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Function

	Public Property Get PageSize()
		'On Error resume next
	    
		PageSize = Rst.PageSize
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property
	
	Public Property Let PageSize(Valor)
		'On Error resume next
	    
		Rst.PageSize = Valor
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property
	
	Public Property Get AbsolutePosition()
		'On Error resume next
	    
		ApsolutePosition = Rst.AbsolutePosition
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property
	
	Public Property Let AbsolutePosition(Valor)
		'On Error resume next
	    
		Rst.AbsolutePosition = Valor
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property

	Public Property Get AbsolutePage()
		'On Error resume next

		AbsolutePage = Rst.AbsolutePage
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property

	Public Function GetRows()
		'On Error resume next

		GetRows = Rst.GetRows
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Function

	Public Function GetString()
		'On Error resume next

		GetRows = Rst.GetString
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Function

	Public Property Let AbsolutePage(Valor)
		'On Error resume next

		Rst.AbsolutePage = Valor
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property
	
	Public default Property Get Fields(Field)
		'On Error resume next
			'response.Write(Field)&"<br>"
		If Field = "" Then
			Set Fields = Rst.Fields
		Else
			If IsNumeric(Field) Then
				Set Fields = Rst.Fields(Field)
			Else
'	rESPONSE.Write "<b>" & FIELD & "</b><br>"
            'rESPONSE.WRITE Rst.Fields(Field) & "<BR>"
				Set Fields = Rst.Fields(Field)
				
			End If
		End If
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property

	Public Property let Fields(Field, Valor)
		On Error resume next

		If IsNumeric(Field) Then
			Rst.Fields(Field) = Valor
		Else
			Rst.Fields(Field) = Valor
		End If
	    
		if Err<>0 then
			response.Write(Field & "-" & Valor & "<br>")
			EscribirError
		end if
		On Error GoTo 0
	End Property

	Public Sub Abrir(Sql, Conex, Opcion1, Opcion2)
		On Error resume next
	    'imanol 18/06/2008 reemplazamos para sqlseerver (var_sgbd=2)
		if not isnull(Sql) and var_sgbd=2 then Sql=replace(Sql,"lcase","lower")
		'reemplazamos la funcion q solo funciona en access isnull por la sentencia is null que funciona tanto en access como en sql server
		if InStr(Sql,"isnull")>0 then
			atributo =  ""
			Sql=replace(Sql,"isnull(" & atributo & ")", atributo & " is null")
		end if
  		'response.write "<hr>"&sql&"<br>parametro1 = " & Opcion1 & "<br>parametro2 = " & Opcion2 & "<br>"
		'response.Write("RS state ===>" & rst.state & "<===<br>")
        if blnPintarSql then response.Write("<FONT COLOR=CCCCCC>ABRIR: " & sql & "</FONT><Br>")
		Rst.Open Sql, Conex, Opcion1, Opcion2
'RESPONSE.WRITE ERR.DESCRIPTION & "<BR>"
		'response.Write("RS state ===>" & rst.state & "<===<br>")
		if blnDebugar then
			intCursorAbrir=intCursorAbrir+1
			strCursorAbrir=strCursorAbrir & "<br>" & rst.Source		
		end if
		if Err.number<>0 then
			enviarError "Abrir Sql BD", "&sql=" & server.HTMLEncode(sql) & "&pagina=" & server.HTMLEncode(Request.ServerVariables("PATH_INFO"))
		end if
		On Error GoTo 0
	End Sub

	Public Sub Cerrar()
		'On Error resume next

		if blnDebugar then	
			intCursorCerrar=intCursorCerrar+1
			strCursorCerrar=strCursorCerrar & "<br>" & rst.Source		
		end if
		
		Rst.Close
		SET RST=NOTHING
		
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	Public Sub Move(Cuanto)
		'On Error resume next
	    
		Rst.Move (Cuanto)

		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	Public Property Let Filter(Valor)
		'On Error resume next
	'Response.Write Valor & "-<br>"
		

        intInicio=now()
		Rst.Filter = Valor
'response.Write("<br>" & sql & "<br>" & err.description)
        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - FILTER " & Valor & "</font><br>"
            end if	
        end if
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property

	Public Property Get Filter()
		'On Error resume next

		Filter = Rst.Filter
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Property

    Public Property Get NumCampos()

		NumCampos = Rst.Fields.count
	    
	End Property

end class

'################################################################################################################
'################################## FIN CLASE PARA EL RECORDSET #################################################


'################################################################################################################
'#################################### CLASE PARA LA CONEXION ####################################################

class DMWConexion

	' con el password de la base de datos
	Private PassBD
	Private Conn
	Private Sub Class_Initialize()
		'PassBD = "hjmje%�fr?_!3re09?�hjmje%�fr?_!3re09?�"
		Set Conn = Server.CreateObject("ADODB.Connection")
    End Sub

	'Variables de la conexion

	Public Sub Abrir(StrConexion)
		'On Error resume next
		'Abre la conexi�n en el caso de tener un numero de serie valido
		'response.Write("conn.State: " & conn.State & "<br />")
		if conn.State = adStateClosed then
            Conn.CommandTimeout = CommandTimeout   
            Conn.ConnectionTimeout = ConnectionTimeout
			Conn.Open CStr(StrConexion)
			if blnDebugar then			
				intConexionAbrir=intConexionAbrir+1
				strConexionAbrir=strConexionAbrir & "<br>" & conn.ConnectionString 		
			end if
		end if
		'response.Write("conn.State: " & conn.State & "<br />")
		
		if Err.number<>0 then
			enviarError "Abrir Conexion BD", ""			
		end if
		On Error GoTo 0
		
		
	End Sub
	
	Public Sub AbrirSQL(StrConexion)
		On Error resume next
		'Abre la conexi�n en el caso de tener un numero de serie valido
		if conn.State = adStateClosed then
            Conn.CommandTimeout = CommandTimeout   
            Conn.ConnectionTimeout = ConnectionTimeout
			Conn.Open CStr(StrConexion)
			if blnDebugar then			
				intConexionAbrir=intConexionAbrir+1
				strConexionAbrir=strConexionAbrir & "<br>" & conn.ConnectionString 		
			end if
			
		end if
	
	
		if Err.number<>0 then
			enviarError "Abrir Conexion BD", ""			
		end if
		On Error GoTo 0
		
		
	End Sub

	Public Sub Cerrar()
		'On Error resume next
	    
		if blnDebugar then	    
			intConexionCerrar=intConexionCerrar+1
			strConexionCerrar=strConexionCerrar & "<br>" & conn.ConnectionString 		
		end if
		
		'Cierra la conexi�n
		Conn.Close
		SET CONN=NOTHING
			    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	Public Function AbrirRecordSet(Sql, Opcion1, Opcion2)
	'Abre un recordset con la sql y las opciones que se le pasan
		On Error resume next
		Set MiRecordset = New DMWRecordSet

        intInicio=now()
		MiRecordset.Abrir CStr(Sql), Conn, CLng(Opcion1), CLng(Opcion2)
'response.Write("<br>" & sql & "<br>" & err.description)
        intFin=now()
        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - SQL " & sql & "</font><br>"
            end if	
        end if
				
		set AbrirRecordSet = MiRecordset
		
		if Err.number<>0 then
			enviarError "AbrirRecordSet dll.asp", "&sql=" & server.HTMLEncode(sql) & "&pagina=" & server.HTMLEncode(Request.ServerVariables("PATH_INFO"))
			
		end if
		On Error GoTo 0
	
	End Function
	
	Public Function AbrirRecordSetSinErrores(Sql, Opcion1, Opcion2)
	'Abre un recordset con la sql y las opciones que se le pasan
	'esta versi�n no para si hay error. Sirve para cuando el error es parte de la programaci�n (por ejemplo, scripts de actualizaci�n de BD)
		Set MiRecordset = New DMWRecordSet

		MiRecordset.AbrirSinErrores CStr(Sql), Conn, CLng(Opcion1), CLng(Opcion2)
		set AbrirRecordSetSinErrores = MiRecordset
		
		
	End Function

	Public Sub Execute(Sql,intNumRegistros)
	'Ejecuta la sql
		'On Error resume next
        if blnPintarSql then response.Write("<FONT COLOR=CCCCCC>EXECUTE: " & sql & "</FONT><Br>")
        intInicio=now()
        Conn.Execute Sql,intNumRegistros
        intFin=now()

        if blnDebugar then
            if datediff("s",intInicio,intFin)>0 then
	            Response.Write "<font size=1 face=verdana color=ff0000><b>" & datediff("s",intInicio,intFin) & "</b> - EXECUTE " & Sql & "</font><br>"
            end if	
        end if

        'Si el execute ha sido de un delete lo logeo en APP_LOGDELETE
        if instr(ucase(sql),"DELETE")>0 and instr(ucase(sql),"APP_LOGDELETE")=0 then
            'strNow=year(date)& "-" & month(date)& "-" & day(date)& " " & hour(now) & ":" & minute(now) & ":" & second(now) 
            sqlLog="Insert into APP_LOGDELETE (APPD_FECHA,APPD_USUARIO,APPD_SQL,APPD_URL,APPD_NUMREGISTROS)"
            sqlLog=sqlLog & " VALUES "
            sqlLog=sqlLog & " (getdate(),'" & session("emailAdministracion") & " - " & session("codigoUsuario")& "','" & left(replace(sql,"'","�"),255) & "','" & left(Request.ServerVariables("PATH_INFO") & "?" & Request.ServerVariables("QUERY_STRING"),255)  & "'," & intNumRegistros &  ")" 

            conn.execute (sqlLog)

        end if

		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	
	Public Sub BeginTrans()
		'On Error resume next

		Conn.BeginTrans
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	Public Sub CommitTrans()
		'On Error resume next

		Conn.CommitTrans
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub

	Public Sub rollback()
		'On Error resume next

		Conn.rollback
	    
		'if Err<>0 then
		'	EscribirError
		'end if
		'On Error GoTo 0
	End Sub


	Public Sub EscribirError(Especificacion)
		Response.Write ("<br>" & Err.Number & " " & Err.Description & " " & Especificacion & "<br>")
		Response.End
	End Sub

	Public Sub EscribirErrorPersonalizado(ElError)
		Response.Write ("<br>" & ElError & "<br>")
		Response.End
	End Sub

end class

'################################################################################################################
'################################## FIN CLASE PARA LA CONEXION ##################################################
%>
 