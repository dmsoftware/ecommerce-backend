<%
function VariablesEmail(strTexto,variable,valor)

    if isnull(valor) then valor=""
    VariablesEmail=replace(strTexto,"/*" & variable & "*/",valor)
    
end function

sub EnvioEmailDM (varNumError,varDescripcionError,varMailHost,varMailUsername,varMailPassword,varMailFrom,varMailFromName,varMailAddAddress,varMailAddCc,varMailAddBcc,varMailSubject,varMailBody)

	'varMailAddAddress: string separado por ; de las direcciones a las que enviar el email PUBLICAS
	'varMailAddBcc: string separado por ; de las direcciones a las que enviar el email OCULTAS
	'Para cuestiones de privacidad siempre se concatenan los destinatarios p�blicos y privados y se env�a 1 email por cada uno de ellos, poniendo p�blico el DESTINATARIO y nadie en el bcc.
	'De la lista de emails eliminamos los que est�n repetidos.
	

    'Creamos la lista de destinatarios, la ordenamos y eliminamos los duplicados
    strDestinatarios=varMailAddAddress
    strDestinatarios=replace(strDestinatarios,";;",";")
    arrDestinatarios=split(strDestinatarios,";")
    'Creo un cursor
	Set rstDestinatarios = Server.CreateObject("ADODB.Recordset")
	rstDestinatarios.Fields.Append "email", 200,255
	rstDestinatarios.CursorLocation = 3 'adUseClient
	rstDestinatarios.CursorType = 2 'adOpenDynamic
	rstDestinatarios.Open    
	for intI=0 to ubound(arrDestinatarios)
        'Comprobamos la sintaxis del email
        if validarEmail(trim(arrDestinatarios(intI))) then
			rstDestinatarios.AddNew
			rstDestinatarios.Fields("email") = trim(arrDestinatarios(intI))
			rstDestinatarios.Update 
		end if
	next
	'Ordeno el cursor
	rstDestinatarios.Sort = "email asc"
	'Elimino repetidos
	while not rstDestinatarios.EOF
	    if strDestinatarioAnterior = rstDestinatarios("email") then
	        rstDestinatarios.delete 
	    else
	        strDestinatarioAnterior = rstDestinatarios("email")
	    end if
	    rstDestinatarios.MoveNext 
	wend
	rstDestinatarios.MoveFirst 	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
    strCc=varMailAddCc
    strCc=replace(strCc,";;",";")
    arrCc=split(strCc,";")
    'Creo un cursor
	Set rstCc = Server.CreateObject("ADODB.Recordset")
	rstCc.Fields.Append "email", 200,255
	rstCc.CursorLocation = 3 'adUseClient
	rstCc.CursorType = 2 'adOpenDynamic
	rstCc.Open    
	for intI=0 to ubound(arrCc)
        'Comprobamos la sintaxis del email
        if validarEmail(trim(arrCc(intI))) then
			rstCc.AddNew
			rstCc.Fields("email") = trim(arrCc(intI))
			rstCc.Update 
		end if
	next
	'Ordeno el cursor
	rstCc.Sort = "email asc"
	'Elimino repetidos
	strCc = ""
	while not rstCc.EOF
	    if strCc = rstCc("email") then
	        rstCc.delete 
	    else
	        strCc = rstCc("email")
	    end if
	    rstCc.MoveNext 
	wend	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
    strBcc=varMailAddBcc
    strBcc=replace(strBcc,";;",";")
    arrBcc=split(strBcc,";")
    'Creo un cursor
	Set rstBcc = Server.CreateObject("ADODB.Recordset")
	rstBcc.Fields.Append "email", 200,255
	rstBcc.CursorLocation = 3 'adUseClient
	rstBcc.CursorType = 2 'adOpenDynamic
	rstBcc.Open    
	for intI=0 to ubound(arrBcc)
        'Comprobamos la sintaxis del email
        if validarEmail(trim(arrBcc(intI))) then
			rstBcc.AddNew
			rstBcc.Fields("email") = trim(arrBcc(intI))
			rstBcc.Update 
		end if
	next
	'Ordeno el cursor
	rstBcc.Sort = "email asc"
	'Elimino repetidos
	strBcc = ""
	while not rstBcc.EOF
	    if strBcc = rstBcc("email") then
	        rstBcc.delete 
	    else
	        strBcc = rstBcc("email")
	    end if
	    rstBcc.MoveNext 
	wend	
        
  	On Error Resume Next
	    
	if rstBcc.RecordCount<> 0 then
		rstBcc.MoveFirst 
	end if
	if rstCc.RecordCount<> 0 then
		rstCc.MoveFirst 
	end if
				
	Set oMail = Server.CreateObject("CDO.Message")
	Set oConf = Server.CreateObject("CDO.Configuration")
	oConf.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = varMailHost
	oConf.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 25 
	oConf.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	oConf.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
	'if ComponenteEmail=3 then
		'Autentificaci�n SMTP
		oConf.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = true 'basic (clear-text) authentication
		oConf.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = varMailUsername 
		oConf.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = varMailPassword 
	'end if
	oConf.Fields.Update
	
	oMail.Configuration = oConf
	oMail.From = varMailFrom
	while not rstDestinatarios.eof
		oMail.To = rstDestinatarios("email")
	wend
	while not rstBcc.eof
		oMail.Bcc = rstBcc("email")
		rstBcc.movenext
	wend
	while not rstCc.eof
		oMail.Cc = rstCc("email")
		rstCc.movenext
	wend
	oMail.Subject = varMailSubject
	oMail.HTMLBody = varMailBody
	oMail.Send 
	rstDestinatarios.movenext

	set oMail=nothing
	set oConf=nothing
		

	
	rstDestinatarios.close
	set rstDestinatarios=nothing
		
	'Devuelvo los errores
	varNumError=err.number 
	varDescripcionError=err.Description 
end sub

function validarEmail(email) 
    dim partes, parte, i, c 
    'rompo el email en dos partes, antes y despu�s de la arroba 
    partes = Split(email, "@") 
    if UBound(partes) <> 1 then 
       'si el mayor indice del array es distinto de 1 es que no he obtenido las dos partes 
       validarEmail = false 
       exit function 
    end if 
    'para cada parte, compruebo varias cosas 
    for each parte in partes 
       'Compruebo que tiene alg�n caracter 
       if Len(parte) <= 0 then 
          validarEmail = false 
          exit function 
       end if 
       'para cada caracter de la parte 
       for i = 1 to Len(parte) 
          'tomo el caracter actual 
          c = Lcase(Mid(parte, i, 1)) 
          'miro a ver si ese caracter es uno de los permitidos 
          if InStr("._-abcdefghijklmnopqrstuvwxyz", c) <= 0 and not IsNumeric(c) then 
             validarEmail = false 
             exit function 
          end if 
       next 
       'si la parte actual acaba o empieza en punto la direcci�n no es v�lida 
       if Left(parte, 1) = "." or Right(parte, 1) = "." then 
          validarEmail = false 
          exit function 
       end if 
    next 
    'si en la segunda parte del email no tenemos un punto es que va mal 
    if InStr(partes(1), ".") <= 0 then 
       validarEmail = false 
       exit function 
    end if 
    'calculo cuantos caracteres hay despu�s del �ltimo punto de la segunda parte del mail 
    i = Len(partes(1)) - InStrRev(partes(1), ".") 
    'si el n�mero de caracteres es distinto de 2 y 3 
    if not (i = 2 or i = 3) then 
       validarEmail = false 
       exit function 
    end if 
    'si encuentro dos puntos seguidos tampoco va bien 
    if InStr(email, "..") > 0 then 
       validarEmail=false 
       exit function 
    end if 
    validarEmail = true 
end function 

%>