<%
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++ Ver: http://www.forosdelweb.com/f15/asp-xml-aproximacion-cambiar-idioma-nuestro-sitio-web-524120/ ++
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
Class clsIdioma
   
  private idiomaXML
   
  public function Load(strIdioma)
        if len(strIdioma) > 0 then 
            idiomaXML.load(strIdioma)
   
            ' Procesamos los errores de lectura
            if idiomaXML.parseError.errorCode <> 0 Then
                Load = "Error de Lectura (Por favor, ponerse en contacto con el webmaster).<br />FilePos: " & idiomaXML.parseError.filepos & "<br /> L�nea: " & idiomaXML.parseError.Line & "<br /> Causa: " & idiomaXML.parseError.reason & "<br /> Ocurri� en: " & idiomaXML.parseError.srcText & "<br /> Archivo: " & idiomaXML.parseError.URL
            else
                Load = ""
            end if
        else
            Load = "�Nada que leer!"
        end if
  end function
   
  Public function GetIdioma(etiqueta)
        Dim Texto                   
        on error resume next
        Texto = idiomaXML.getElementsByTagName(etiqueta).Item(0).Text
        if err.number > 0 then
            GetIdioma = "[" & etiqueta  & "]"
        else 
			Texto=replace(Texto,"|br|","<br />")
            GetIdioma=Texto
        end if
        on error goto 0           
  end function
      
  '*********************************************************************
  ' Inicializacion/Terminacion
  '*********************************************************************
  Private Sub Class_Initialize()
      Set idiomaXML = Server.CreateObject("Microsoft.XMLDOM")
      ' Para evitar que el objeto est� en escucha ya que 
      ' solo lo vamos a abrir una vez
      idiomaXML.async=false
  End Sub
   
  Private Sub Class_Terminate()
      Set idiomaXML = Nothing
  End Sub

end Class%>