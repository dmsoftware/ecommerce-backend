
<%
function FormatearFecha(fecha)
	if not isdate(fecha) then exit function
	if isnull(fecha) then
	    FormatearFecha=fecha
	else
		FormatearFecha= month(fecha) & "/" & day(fecha)& "/" & year(fecha)
	end if
end function

function bck(valor) 'funci�n usada para poner o no background en el nuevo dise�o
	if trim("" & valor)="" then
		bck = ""
	else
		bck = "background=""" & nivelbd & "modulos/usuariosftp/conexion/" & valor & """"
	end if
end function
  
function espacios(numero)
	for numEspacios = 1 to numero
		espacios = espacios & "&nbsp;"
	next
end function

'Funcion que dado un n�mero, me devuelve un string despu�s de aplicar una funcion hash
'CUTRE CUTRE, pero sirve
'function Hash(numero)
'	dim parcial
'	parcial = cLng(numero & "925") 
'	parcial = parcial * 137
'	parcial = parcial + 5
'	parcial = parcial \ 7
'	parcial = parcial *13
	'parcial = parcial mod 100000
'	Hash = parcial
'end function

function HexToDec(numero)
	HexToDec = Clng("&H" & numero)
end function

'Funci�n que dado un c�digo de formulario, y un n�mero, nos devuelve un array de tantos pines como ese n�mero
'Que son v�lidos para ese formulario.
function DamePines(codForm,numeroDePines)
	dim intContador
	dim strAux
	dim intNumero
	if not isNumeric(numeroDePines) then numeroDePines = 0
	strAux = ""
	for intContador = 217 to numeroDePines+216
		intNumero = Hex(right(100000+intContador,5) & right(100000+codForm,5))
		strAux = strAux & intNumero & ","
	next
	if strAux<>"" then strAux = left(strAux,len(strAux)-1) 'le quitamos la ultima coma
	DamePines = split(strAux,",")
end function

function EsPin(pin,codForm)
	dim aux
	'desconvertimos de hexadecimal
	aux = HexToDec(pin)
	if aux = -1 then 'no era hexadecimal
		EsPin = false
	else
		'sacamos la parte del codigo de formulario
		aux = right(aux,5)

		if cLng(aux) = cLng(codForm) then
			EsPin = true
		else
			EsPin = false
		end if
	end if 'aux = -1
end function


'miarray = damepines(2,100)
'for i = 0 to ubound(miarray)
'	response.Write miarray(i) & "::"
'	response.Write EsPin(miarray(i),2) & "<br>"
'next

Function escribirAgenda(Texto)
    
    Aux=replace(Texto,"<br>",Chr(13)&Chr(10))
    Aux=replace(Aux,"<BR>",Chr(13)&Chr(10))
    Aux=replace(Aux,"<p>",Chr(13)&Chr(10))
    Aux=replace(Aux,"<P>",Chr(13)&Chr(10))
    Aux=replace(Aux,"</p>",Chr(13)&Chr(10))
    Aux=replace(Aux,"</P>",Chr(13)&Chr(10))
	
	fin=false    
    while not fin=true
		a = instr(Aux,"<")
		b = instr(Aux,">")

		if a<>0 and b<>0 and a < b then
			'En Aux guardamos el texto quitando este primer tag
			Aux=left(Aux,a-1) & right(Aux,len(Aux)-b)	        
		elseif a<>0 and b=0 then 'se habre el tag pero no se cierra.
			'En Aux guardamos el texto quitando este primer tag
			Aux=left(Aux,a-1)
			fin=true			
		else 
		    fin=true
		end if
	wend
	
    escribirAgenda=Aux
    
end function


'Modificado por IVAN el 6/10/04
function suPortal(cod)
'Funcion que dado un elemento cualquiera devuelve el portal del que cuelga

	if cod > 0 then 'A�adido por I�igo (s�lo el if con su else :))
		sql = "SELECT suportal FROM Menus WHERE CodM=" & cod
		Set rsPortal = connCRM.AbrirRecordset(sql,3,3)
		suportal=cint(rsportal("suportal"))
		rsPortal.Cerrar
		set rsPortal=nothing	
	else
		suportal = 1	
	end if	

end function

'Modificado por IVAN el 6/10/04
'Funcion que dado un elemento cualquiera devuelve el idioma del que cuelga
function suIdioma(cod)
	dim miRuta
	dim posicionComa
	
	sql = "SELECT ruta FROM Menus WHERE CodM=" & cod
	Set rsIdioma = connCRM.AbrirRecordset(sql,3,3)	
	miRuta = rsIdioma("Ruta")	
	rsIdioma.cerrar
	set rsIdioma=nothing
	posicioncoma = inStr(miRuta,",")
	
	if posicionComa = 0 then 'Es idioma
		suIdioma = CLng(miRuta)
	else
		suIdioma = CLng(left(miRuta,posicionComa-1))
	end if
end function

function nombreIdioma(cod)
'Funcion que dado un elemento cualquiera devuelve el idioma del que cuelga
	sql = "SELECT nombreM FROM Menus WHERE CodM=" & cod
	Set rsPortal = connCRM.AbrirRecordset(sql,3,3)	
	
	nombreidioma=rsportal("nombrem")
	rsPortal.Cerrar
	set rsPortal=nothing
end function


function thumbnail (imagen,width,height,alt)
'Parametros:
	'imagen: ruta relativa a la imagen desde la raiz
	'width: Ancho que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al alto
	'height: Alto que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al ancho
	if thumbnailHabilitado=0 then
		strTmp = "<img border=""0"" src=""" & pathabsoluto & imagen & """ border=""0"""
		if width<>0 then strTmp = strTmp & """ width=""" & width
		if height<>0 then strTmp = strTmp & """ height=""" & height
		strTmp = strTmp & """ alt=""" & alt & """>"
		'strTmp = strTmp &  """>"
		thumbnail = strTmp
	else
		Set OFSO = Server.CreateObject("Scripting.FileSystemObject")
		if OFSO.fileexists(strlocal & imagen) then
			strTmp= "<img border=""0"" src=""" & pathabsoluto & "thumbnailimage.aspx?filename=" & imagen 
			if width<>0 then strTmp = strTmp & """&width=""" & width
			if height<>0 then strTmp = strTmp & """&height=""" & height
			strTmp = strTmp & """ alt=""" & alt & """>"
			'strTmp = strTmp &  """>"
			thumbnail=strTmp
		else 
			thumbnail=""
		end if	
	end if
end function

function AlbumThumbnail (imagen,width,height)
'Parametros:
	'imagen: ruta relativa a la imagen desde la raiz
	'width: Ancho que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al alto
	'height: Alto que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al ancho
	if thumbnailHabilitado=0 then
		strTmp = pathabsoluto & imagen 
		AlbumThumbnail = strTmp
	else
		Set OFSO = Server.CreateObject("Scripting.FileSystemObject")
		if OFSO.fileexists(strlocal & imagen) then
			strTmp= pathabsoluto & "thumbnailimage.aspx?filename=" & imagen 
			if width<>0 then strTmp = strTmp & "&width=" & width
			if height<>0 then strTmp = strTmp & "&height=" & height
			AlbumThumbnail=strTmp
		else 
			AlbumThumbnail=""
		end if	
	end if
end function

Sub TextoPlano(campo)
    campoTmp=replace(replace(campo,"<br>",chr(13) & chr(10)),"<BR>",chr(13) & chr(10))
    posicion1=1
    posicion2=1
    while posicion1>0 and posicion2>0
		posicion1=InStr(1,campoTmp,"<")
		if posicion1>0 then
			posicion2=InStr(posicion1,campoTmp,">")
		else
			posicion2=0
		end if
		if posicion1>0 and posicion2>0 then	campoTmp=left(campoTmp,posicion1-1) & right(campotmp,len(campoTmp)-posicion2)
	wend
	Response.Write (campoTmp)
end sub

function quitaScripts2(campo)
	posicion1=1
	posicion2=1
	campoTmp=campo
	while posicion1>0 and posicion2>0
		posscript=0
		posicion1=InStr(1,lcase(campoTmp),"<script")
		if posicion1>0 then
			posscript=InStr(posicion1,lcase(campoTmp),"</script")
			posicion2=InStr(posscript,campoTmp,">")
		else
			posicion2=0
		end if
		if posicion1>0 and posicion2>0 and posscript>0 and posscript<posicion2 then	campoTmp=left(campoTmp,posicion1-1) & right(campotmp,len(campoTmp)-posicion2)
	wend
	quitaScripts=campoTmp
end function

'Aritz Lopez: Filtramos la cadena que pasa por la funci�n						
function quitaScripts(cadena)													

dim chars1
dim chars2

	chars1 = array("<",">","\","%00","&","#","�","0x")
	chars2 = array("","","/","","","","","")
	campo = cadena
	if isnull(campo) = false then
		for i = 0 to ubound(chars1)
		'while InStr(campo, chars1(i)) <> 0
			campo = replace(campo,chars1(i),chars2(i))
		'wend
		next
	end if	
	quitaScripts = replace(campo, "'", "''")
	
end function

Sub Pres_Memo(campo)
    campo=campo & Chr(13)&Chr(10) 'Concateno para que no casque
    while (Len(campo)>2) 'Es un dos la condici�n de salida del loop p.q. al final hay un return
		posicion_enter=InStr(1,campo,(Chr(13)&Chr(10)))
		palabra=Left(campo, posicion_enter-1)
		cadena=cadena&palabra &"<br>"
		longitud=(Len(campo)-(posicion_enter))
		campo=Right(campo,CDbl(longitud))
	wend
	Response.Write (cadena)
end sub

Sub Pres_Memo_Tabla(campo)
    campo=campo & Chr(13)&Chr(10) 'Concateno para que no casque
    while (Len(campo)>2) 'Es un dos la condici�n de salida del loop p.q. al final hay un return
		posicion_enter=InStr(1,campo,(Chr(13)&Chr(10)))
		palabra=Left(campo, posicion_enter-1)
		'Solo concateno un "br" si no hay una tabla
		'Solo concateno un "br" si no hay un br al final

		'a�adido por oscar el 23-1-03 para permitir applets en elementos
		if instr(1,ucase(palabra),"<APPLET")>0 then
			en_aplet=true
		end if
		if instr(1,ucase(palabra),"</APPLET")>0 then
			en_aplet=false
		end if
		'fin a�adido oscar

		if not en_aplet and not( instr(1,ucase(palabra),"<TABLE")>0 or instr(1,ucase(palabra),"<TR")>0 or instr(1,ucase(palabra),"<TD")>0 or instr(1,ucase(palabra),"</TABLE")>0 or instr(1,ucase(palabra),"</TR")>0 or instr(1,ucase(palabra),"</TD")>0 or uCASE(right(palabra,4))="<BR>") then
		   cadena=cadena&palabra &"<br>"
		else
		   cadena=cadena&palabra
		end if
		longitud=(Len(campo)-(posicion_enter))
		campo=Right(campo,CDbl(longitud))
	wend
	Response.Write (cadena)
end sub

Sub Pres_Memo_java(campo)
    
    'Eliminamos las comillas simples (alberto 08/07/2002)
    campo=replace(campo,"'"," ")
    
    campo=campo & Chr(13)&Chr(10) 'Concateno para que no casque
	cadena=replace(campo,(Chr(13) & Chr(10)),"<br>")
	'Elimino el �ltimo br introducido
	Cadena=left(cadena,len(cadena)-4)
	Response.Write(cadena)
end sub

' Modificado por I�igo (29/7/2002)
' INICIO

function escribir_Hora()
    escribir_Hora = escribir_Saludo & "  " & escribir_Fecha(date())
end function

function escribir_Saludo()
    if hour(time)< 7 then
    escribir_Saludo = traducir(17)
    else if hour(time)< 14 then
         escribir_Saludo = traducir(15)
         else if hour(time)< 20 then
              escribir_Saludo = traducir(16)
			  else if hour(time)<= 24 then
                   escribir_Saludo = traducir(17)
                   end if
              end if
         end if
    end if
end function

function escribir_Fecha(fecha)
	
	Select case WeekDay(fecha)
		case "1"
			dia = traducir (39)
	    case "2"
			dia = traducir (33)
		case "3"
			dia = traducir (34)
		case "4"
			dia = traducir (35)
		case "5"
			dia = traducir (36)
		case "6"
			dia = traducir (37)
		case "7"
			dia = traducir (38)	    
		case else
			dia = WeekDayName(fecha)	    
	end select
	
	Select case Month(fecha)
		case "1"
			mes = traducir (40)
	    case "2"
			mes = traducir (41)
		case "3"
			mes = traducir (42)
		case "4"
			mes = traducir (43)
		case "5"
			mes = traducir (44)
		case "6"
			mes = traducir (45)
		case "7"
			mes = traducir (46)
		case "8"
			mes = traducir (47)
		case "9"
			mes = traducir (48)
		case "10"
			mes = traducir (49)
		case "11"
			mes = traducir (50)
		case "12"
			mes = traducir (51)					    
		case else
			mes = WeekDayName(fecha)	    
	end select
	
	Select case day(fecha)
		case "1"
			ordIng = "st "
			ordFr = "<sup>er</sup> "
		case "2"
			ordIng = "nd "
			ordFr = " "
		case else
			ordIng = "th "
			ordFr = " "
	end Select
	
	idioma = session(denominacion & "_idioma")
	Select case Idioma
		case "1" ' castellano
			escribir_Fecha = "  " & dia &", "& day(fecha) & " de " & mes & " de " & year(fecha)
		case "14" ' euskara
			escribir_Fecha = "  " & dia &", "& year(fecha) & "ko " & mes & "ren " & day(fecha) & "a"
		case "1347" ' Ingl�s
			escribir_Fecha = "  " & dia & ", " & mes & " " & day(fecha) & ordIng & year(fecha)
		case "1348" ' Franc�s
			escribir_Fecha = "  " & dia & ", " & day(fecha) & ordFr & mes & " " & year(fecha)
		case "1601" ' alem�n
			escribir_Fecha = "  " & dia &", "& day(fecha) & ". " & mes & " " & year(fecha)
		case else
			'se ha muerto la session
			escribir_Fecha = "  " & FormatDateTime(fecha, 1)
	end select
	
end function

'FIN (I�igo 29/7/2002)

'I�igo (1/VIII/2002)
Function escribir_Fecha_Simple (fecha)

	idioma = session(denominacion & "_idioma")
	Select case Idioma
		case "1" ' castellano (d�a/mes/a�o)
			escribir_Fecha_Simple = day(fecha) &"/"& month(fecha) &"/"& year(fecha)
		case "14" ' euskara (urtea/hilabetea/eguna)
			escribir_Fecha_Simple = year(fecha) &"/"& month(fecha) &"/"& day(fecha)
		case else
			'se ha muerto la session (sistema)
			escribir_Fecha_Simple = FormatDateTime(fecha, 2)
	end select
	
end Function
'FIN (I�igo 1/VIII/2002)

Sub pts(valor)
    v=Cstr(valor)
    longitud = len(v)
    salida= v
     if longitud > 3 then
        salida =""
		while longitud > 3
			salida = "." & right(v,3) & salida
			v = left(v,longitud-3)
			longitud=longitud-3
		wend	
		if  longitud > 0 then
			salida = v & salida	
		else 
		    salida = right(salida,len(salida)-1)
		end if
	end if
    
    Response.write(salida & " Ptas.")

end sub

Sub pts_euros(valor,euros)
    v = round(valor/euros,2)
    v=Cstr(v)
    salida = v
    if longitud > 3 then
		salida =right(v,3)
		longitud = len(v)
		v = left(v,longitud-3)
		longitud=longitud-3
    
		while longitud > 3
			salida = "." & right(v,3) & salida
			v = left(v,longitud-3)
			longitud=longitud-3
		wend	
		if  longitud > 0 then
			salida = v & salida	
		else 
		    salida = right(salida,len(salida)-1)
		end if
	end if
    
    Response.write(salida & " �.")

end sub


'rs: un recorset de la tabla Menus,   cod:c�digo del elemento actual
function Linea_Estado(res) 
	dim salida		'String de salida
	salida=""
	while not res.eof
		salida = salida & res("NombreM")
		res.movenext()
		if not res.eof then salida = salida & "/ "
	wend
	Linea_Estado =salida
end function



'FUNCION PARA EL DEVOLVER LA RUTA COMPLETA DE UN NODO
'si hay s�lo un idioma no lo escribe.
Function padresinconexion(codigo,padre)
	
	inicio = 0  'empezar a escribir desde el idioma
	
	'determinar si hay m�s de un idioma
	padre.filter = "PadreM = -1"
	if padre.recordcount = 1 then
		'hay s�lo un idioma
		inicio = 1 'empezar a escribir desde despu�s del idioma.
	end if
	'-------------------------

	pinta=""
	if codigo<>"" then
		padre.filter = " CODM=" & Codigo
		if not padre.eof then
			LosCodigos=split(padre("RUTA"),",")
			for ContPadre=inicio to ubound(LosCodigos)
				padre.filter = " CODM=" & LosCodigos(ContPadre)
				if not padre.eof then
					if pinta<>"" then pinta=pinta & "->"
					pinta=pinta & padre ("nombreM")
				end if
			next 
		end if
		padre.filter=0	
	end if
	padresinconexion = pinta
end function

'Esta funci�n simula un LEFT con las siguientes caracter�sticas:
'   * si existe un < y un > elimina el contenido del medio
'   * si s�lo existe un < elimina a partir de ah�.
Function CortarTexto(Texto,Numero)
    
    ''Texto=Left(Texto,Numero)
    Aux=Texto
   
    'Para evitar errores si el usuario ha escrito alg�n ">" eliminarmos los "->" posibles
    if len(aux)>0 then
      Aux=replace(Aux,"->","-")
    end if
    
    Fin=false
    while not fin=true
		a = instr(Aux,"<")
		b = instr(Aux,">")

		if a<>0 and b<>0 and a < b then
			'En Aux guardamos el texto quitando este primer tag
			Aux=left(Aux,a-1) & right(Aux,len(Aux)-b)	        
		elseif a<>0 and b=0 then 'se habre el tag pero no se cierra.
			'En Aux guardamos el texto quitando este primer tag
			Aux=left(Aux,a-1)
			fin=true			
		else 
		    fin=true
		end if
	wend
	
    CortarTexto=left(Aux,Numero)
    
end function



'Funci�n para aplicar descuentos a los productos seg�n el porcentaje del cliente y la ponderaci�n del
'producto.
function aplicarDescuento (valor, descuento, ponderacion)
	if isnull(descuento) or descuento = "" then
		descuento = 0
	end if
	
	if isnull(ponderacion) or ponderacion = "" then
		ponderacion = 0
	end if
		
	if conPonderacion = 1 then
		resul = valor - ((valor * descuento * ponderacion) / 10000)
	else
		resul = valor - ((valor * descuento) / 100)
	end if	
	
	aplicarDescuento = round(resul,2)
end function

'Funci�n para controlar la publicaci�n de los nodos seg�n la fecha de inicio y la de fin
function IntervaloPublicacion (fInicio, fFin)
	'Entrada: fInicio, fFin. Fechas de inicio y fin
	'Salida: 1- No se ha llegado a la fechad de inicio
	'		 2- Se est� entre las dos fechas
	'		 3- Se ha pasado de la fecha de fin
	'        4- No hay fecha de inicio ni de fin
	if isnull(fInicio) or fInicio = "" then
		if isnull(fFin) or fFin = "" then
			IntervaloPublicacion = 4
		else
			if date() <= fFin then
				IntervaloPublicacion = 2
			else
				IntervaloPublicacion = 3
			end if
		end if	
    else
    	if isnull(fFin) or fFin = "" then
    		if date() < fInicio then
    			IntervaloPublicacion = 1
    		else
    			IntervaloPublicacion = 2
    		end if	
    	else
    		if date() > fFin then
    			IntervaloPublicacion = 3
    		elseif date() < fInicio then
    			IntervaloPublicacion = 1
    		else
    			IntervaloPublicacion = 2
    		end if
    	end if
    end if
end function

'Funci�n que saca el c�digo de un nodo de una url
'Por ejemplo: '../antcatalogo.asp?cod=666&nombre=666' ---> 666
Function obtenerCodigo (direccion)
	if isnull(direccion) or direccion = "" then
		obtenerCodigo = 0
	else
		if instr(1,ucase(direccion),"COD=") > 0 then
			pos = instr(1,ucase(direccion),"COD=") + 3			
			substring = right(direccion,len(direccion)-pos)
			if instr(1,substring,"&") > 0 then
				obtenerCodigo = cint(left(substring,instr(1,substring,"&")-1))
			else
				obtenerCodigo = cint(substring)
			end if
		elseif instr(1,ucase(direccion),"NOMBRE=") > 0 then
			pos = instr(1,ucase(direccion),"NOMBRE=") + 6
			substring = right(direccion,len(direccion)-pos)
			if instr(1,substring,"&") > 0 then
				obtenerCodigo = cint(left(substring,instr(1,substring,"&")-1))
			else
				obtenerCodigo = cint(substring)
			end if
		else
			obtenerCodigo = 0
		end if
	end if	
End Function


'sub ListaDescendientes (elemento, byref rec)
	'funcion que devuelve una lista con todos los descendientes de un nodo separados por ","
	'a�adida por oscar el 13/10/03
'	set rec = Server.CreateObject("ADODB.Recordset")

'	Const adInteger = 3
'	rec.Fields.Append "CodM", adInteger
'	rec.Open

	'busco los hijos del elemento
'	sql="select * from menus where padreM=" & elemento
'	Set rstmp = connCRM.AbrirRecordset(sql,3,3)
'	while not rstmp.eof
'		if rstmp("hoja")=0 then
			'es un nodo men�, por tanto hago la llamada recursiva
'			ListaDescendientes rstmp("codM"),rstmp2
			'a�ado todos los elementos obtenidos al registro que voy a devlover
'			while not rstmp2.eof
'				rec.AddNew ()
'				rec.Fields("codM").value = rstmp2("CodM")
'				rstmp2.MoveNext 
'			wend
'			rstmp2.close()
'		else
'			rec.AddNew ()
'			rec.Fields("CodM").Value = rstmp("CodM")
'		end if
'		rstmp.movenext
'	wend
'	rstmp.cerrar()
	'por ultimo a�ado el nodo de la busqueda
'	rec.AddNew ()
'	rec.fields("codm").value = elemento
'	rec.movefirst
'end sub
'######################################## FUNCIONES PARA EL ENVIO DE EMAILS AL DAR DE ALTA NODOS PRIVADOS
'############# IVAN 5-2-2004

'procedimiento recursivo usado por ListaEmails
Sub sacarEmails (rsLista,rsEmails)
	while not rsEmails.eof
		rsLista.AddNew
		rsLista.Fields("Email").Value = rsEmails("Usu_Email")
		rsEmails.movenext()
	wend
end sub

'procedimiento que nos devuelve una lista de emails de usuarios que tienen permiso de lectura
'del nodo "elemento." que se acaba de dar de alta. Puede haber repetidos dependiendo de los permisos por niveles.
'Ivan 2-2-2004
sub ListaEmails (elemento, byref rsLista)
	set rsLista = Server.CreateObject("ADODB.Recordset")
	Const adVarChar = 200
	rsLista.Fields.Append "Email", adVarChar, 255
	rsLista.Open

	sql = "SELECT DISTINCT Usu_Email FROM ("
	sql = sql & " SELECT USU_EMAIL FROM Users WHERE USU_CODIGO IN  (SELECT MEPE_SUUSUARIO FROM MENUS_PERMISOS WHERE MEPE_SUMENU=" & ELEMENTO & " AND Usu_Email<>'' AND NOT isNull(Usu_Email)) "
	sql = sql & " UNION (SELECT USU_EMAIL FROM Usuarios_Grupos left JOIN Users ON Usuarios_Grupos.USG_SUUSUARIO = Users.USU_CODIGO WHERE USG_SUGRUPO  IN  (SELECT MEPE_SUGRUPO FROM MENUS_PERMISOS WHERE MEPE_SUMENU=" & ELEMENTO & " AND Usu_Email<>'' AND NOT isNull(Usu_Email)))"
	sql = sql & ")"
	
	Set rsEmails = connCRM.AbrirRecordset(sql,3,3) 'RecordSet con todos los emails de todos los usuarios con permiso de todos los nodos
	sacarEmails rsLista,rsEmails
	rsEmails.cerrar
	set rsEmails=nothing
	if not rsLista.eof then rsLista.moveFirst
end sub

'Funcion para meter un fin de linea
Public Function new_line ()
	new_line = Chr(13) & Chr(10)
End Function

'Funcion que me dice si una foto hay que pintarla a la derecha o no
'( si ya se ha metido en el centro o no).

Function PintarFotoDerecha(Descripcion,foto)
	PintarFotoDerecha=instr(Descripcion,foto)
End Function

'Funcion que env�a mensajes a todos los usuarios con permiso de un nodo
'Se usa cuando se da de alta un elemento en una zona privada
Sub EnviarMensajesAlta(Cod)

	Dim strBody
	Dim sql
	Dim strNombreElemento
	Dim strDescripElemento
	Dim datFecha
	Dim strPublicador
	Dim intPadre
	Dim strNombrePadre

	sql="select * from Configuraciones"	
	Set rstConf = connCRM.AbrirRecordset(sql,3,3)
	Host=rstconf("CFG_RecomendarHost")
	From=rstconf("CFG_RecomendarEmail")
	Emisor=rstconf("CFG_DenominacionEmail")
	Dominio=rstconf("CFG_Dominio")
	ImagenEnvio=rstconf("CFG_ImagenEnvio")
	rstConf.cerrar()

	'*********************************
	'Componemos el cuerpo del mensaje
	'*********************************
	
	'Ponemos una im�gen en la parte superior
	strBody = "<html><link rel='stylesheet' href='" & pathAbsoluto & "conf/estilos.css'><body>" & new_line()
	
	
	'comento la cabecera porque ahora no interesa que se vea:
	'strBody = strBody & "<a href='" & pathAbsoluto & "' target='_blank'><img src='" & pathAbsoluto & ImagenEnvio & "' border=0></a><br><br>" & new_line()
	
	
	strBody = strBody & "<blockquote>" & new_line()

	'Sacamos el nombre del elemento que se ha a�adido, y el nombre del padre, o sea, la secci�n del elemento.
	sql = "SELECT CodM,NombreM,PadreM,DescripM,Fecha,Hoja,Usu_Nombre,Usu_Apellido1,Usu_Apellido2 FROM Menus LEFT JOIN Users ON Menus.Autor=Users.Usu_Codigo"
	Set rsMenus = connCRM.abrirRecordset(sql,3,3)
	rsMenus.filter = "CodM = " & Cod
	strNombreElemento = rsMenus("NombreM")
	strDescripElemento = rsMenus("DescripM")
	datFecha = rsMenus("Fecha")
	if rsMenus("Hoja")=1 then 'Hoja
		strPagina = "antBusPre.asp"
	else 'Menu
		strPagina = "antCatalogo.asp"
	end if
	
	strPublicador = rsMenus("Usu_Nombre") & " " & rsMenus("Usu_Apellido1") & " " & rsMenus("Usu_Apellido2")
	intPadre = rsMenus("PadreM")
	rsMenus.filter = 0 'Quitar el filtro
	rsMenus.filter = "CodM=" & intPadre
	strNombrePadre = rsMenus("NombreM")
	rsMenus.filter = 0
	rsMenus.cerrar

	'Ponemos un link al nodo que se ha publicado
	strBody = strBody & "Se ha a�adido nueva informaci�n privada a la p�gina web.<br><br>"
	strBody = strBody & ":: <b>Secci�n:</b> <a target=_blank href='" & pathAbsoluto & "antCatalogo.asp?Cod=" & intPadre & "&Nombre=" & intPadre & "&sesion=" & session(denominacion & "_idioma") & "'>" & strNombrePadre & "</a><br>"
	strBody = strBody & ":: <b>Elemento a�adido:</b> <a target=_blank href='" & pathAbsoluto & strPagina & "?Cod=" & cod & "&Nombre=" & cod & "&sesion=" & session(denominacion & "_idioma") & "'>" & strNombreElemento & "</a><br>"
	strBody = strBody & cortarTexto(strDescripElemento,200) & "...<br><br>"
	strBody = strBody & ":: <b>A�adido el d�a:</b> " & datFecha & "<br>"
	if trim (strPublicador) <> "" then
		strBody = strBody & "<b>:: Por:</b> " & strPublicador & "<br><br>"
	end if

	strBody = strBody & "Recuerde que necesitar� autentificarse para poder ver esta informaci�n.<br>"

	strBody = strBody & "</blockquote>" & new_line()

	'Ponemos el pie de p�gina
	strBody = strBody & "<br><hr height=1 width=50% align=center>" & new_line()
	strBody = strBody & "<br><center><font face=Verdana size=1 color=#000000>Informaci�n enviada por <a href='" & pathAbsoluto & "?sesion=" & session(denominacion & "_idioma") & "' target='_blank'><font color=#0000FF><u>" & Emisor & "</u></font></a></font></center>" & new_line()
	strBody = strBody & "</body></html>" & new_line()

	'Metemos todos los destinatarios en la lista BCC (ocultos, para que un cliente no pueda ver las direcciones de los dem�s)
	ListaEmails Cod, emails

	'*********************************
	'Componemos el mensaje
	'*********************************
	if ConAspEmail = 1 then
		Set Mail = Server.CreateObject("Persits.MailSender")
		Mail.IsHTML = True
		Mail.Host = Host
		Mail.From = From
		Mail.FromName = Emisor
		Mail.Subject = "Nueva informaci�n en " & Dominio
		Mail.Body = strBody     

		if emails.eof then
			blnHayQueEnviar = false
		else
			blnHayQueEnviar = true
			while not emails.eof
				if Trim(emails(0)) <> "" then Mail.AddBcc emails(0) 'Puede haber repetidos, pero la funccion AddBcc los obvia.
				emails.movenext
			wend
		end if

		emails.close
		Set emails = nothing

		if blnHayQueEnviar then 
			On Error Resume Next
			Mail.Send
		end if
	else 'Enviamos con CDONTS
		Dim MyCDONTSMail
		Set MyCDONTSMail = CreateObject("CDONTS.NewMail")
		MyCDONTSMail.From = EmailContacto
		'MyCDONTSMail.To = Request.Form("usuario")
		MyCDONTSMail.Subject = "Nueva informaci�n en " & Dominio
		MyCDONTSMail.BodyFormat = 0
		MyCDONTSMail.MailFormat = 0
		MyCDONTSMail.Body = strBody
		
		if emails.eof then
			blnHayQueEnviar = false
		else
			blnHayQueEnviar = true
			listaDirecciones = ""
			while not emails.eof
				if Trim(emails(0)) <> "" then listaDirecciones = listaDirecciones & emails(0) & ";" 'Puede haber repetidos, pero la funccion AddBcc los obvia.
				emails.movenext
			wend
			listaDirecciones = left(listaDirecciones,len(listaDirecciones)-1)
			MyCDONTSMail.Bcc = listaDirecciones
		end if
		
		if blnHayQueEnviar then 
			On Error Resume Next
			MyCDONTSMail.Send				
		end if
		set MyCDONTSMail=nothing
	end if

End sub

'#################################### FIN FUNCIONES PARA EL ENVIO DE EMAILS AL DAR DE ALTA NODOS PRIVADOS

'#################################### FUNCIONES PARA CLONAR SUBARBOLES DE NODOS

Sub ClonarSubArbol(nodoOrigen,nodoDestino,replicarElementos,replicarImagenes,replicarArchivos,RutaPadre,byref codigoDestino)
	'response.Write nodoOrigen & "-" & nodoDestino & ".-<br>"
	dim sql
	dim codigoTmp
	dim filename
	
	sql = "Select NivelM FROM Menus WHERE CodM = " & nodoDestino
	Set rsNivelDestino = connCRM.abrirRecordSet(sql,3,3)
	nivelDestino = rsNivelDestino("NivelM")
	rsNivelDestino.cerrar
	set rsNivelDestino = nothing
	
	'clono todos los hijos del registro actual
	sql = "INSERT INTO menus (codm, nombrem, DescripcionCortaM, descripm, foto2m, preciom, nivelm, padrem, hoja, hco, url, interna, columnas, extranet, autor, subanner, fecha, orden, actualizable, posicion, ImagenGrande, comprable, ponderacion, fechainiciopublicacion, fechafinpublicacion, replicar, suportal,esClonable,peso,iva) "
	sql = sql & " SELECT (select " & codigoDestino & " + count(*) from menus as m where m.codm < tmp.codm and padreM=" & nodoOrigen 
	if not replicarElementos then sql = sql & " AND hoja=0"
	sql = sql & "), nombrem, DescripcionCortaM, descripm, foto2m, preciom, " & nivelDestino + 1 & ", " & nodoDestino & ", hoja, hco, url, interna, columnas, extranet, autor, subanner, fecha, orden, actualizable, posicion, ImagenGrande, comprable, ponderacion, fechainiciopublicacion, fechafinpublicacion, replicar, suportal, esClonable, peso, iva FROM menus AS tmp "
	sql = sql & " WHERE padrem=" & nodoOrigen
	if not replicarElementos then
		sql = sql & " AND hoja=0"
	end if

	connCRM.Execute sql,intNumRegistros
	set rsTmp = connCRM.abrirRecordset("select max(codm) as codigo from menus",3,3)
	if rsTmp.eof then
		codigoDestino=1
	else
		codigoDestino=rsTmp("codigo") + 1
	end if
	'creo los recordsets para ir replicando
	sql = "select CodM from menus where padrem=" & nodoOrigen
	if not replicarElementos then
		sql = sql & " AND hoja=0"
	end if
	
	Set rsOrigenes = connCRM.abrirRecordSet(sql,3,3)
	sql = "select CodM,Ruta from menus where padrem=" & nodoDestino
	Set rsDestinos = connCRM.abrirRecordSet(sql,3,3)
	
	'hago las llamadas recursivas
	while not rsOrigenes.EOF
		'response.Write rsOrigenes("codm") & "-" & rsdestinos("codm") & "-<br>"
		rsDestinos("Ruta") = RutaPadre & "," & rsdestinos("codm")
		rsDestinos.update
		ClonarSubArbol rsOrigenes("codm"),rsdestinos("codm"),replicarElementos,replicarImagenes,replicarArchivos,RutaPadre & "," & rsdestinos("codm"),codigoDestino
		rsorigenes.MoveNext
		rsdestinos.MoveNext
		codigoDestino=codigoDestino+1
	wend
	
	rsOrigenes.cerrar
	rsDestinos.cerrar
	
	'clono los permisos del nodo
	AnadirPermisosNodo nododestino,nodoorigen
	
	Set fso = CreateObject("Scripting.FileSystemObject")

	if replicarImagenes then
		'clono las imagenes asociadas
		sql = "SELECT * FROM imagenes_web WHERE ima_sumenu=" & nodoorigen
		Set rsImagenes= connCRM.abrirRecordSet (sql,3,3)
		'intento copiar los ficheros 
		while not rsImagenes.EOF 
			'Sacamos el m�ximo c�digo
			sql = "SELECT MAX(ima_codigo) AS codigo FROM imagenes_web"
			Set rsTmp = connCRM.abrirRecordSet(sql,3,3)
			CodigoTmp=rsTmp("codigo")+1
			rsTmp.cerrar

			sql = "insert into imagenes_web (ima_codigo,ima_descripcion,ima_imagen,ima_sumenu,ima_ancho,ima_alto,ima_imagen_peq) VALUES ( " & codigotmp & ",'" & rsImagenes("ima_descripcion") &  "','" & left(rsImagenes("ima_imagen"),5) & codigoTmp & right(rsImagenes("ima_imagen"),5) & "'," & nododestino & "," & rsImagenes("ima_ancho") & "," & rsImagenes("ima_alto") & ","
			if trim(rsImagenes("ima_imagen_peq"))<>"" then
				sql=sql & "'" & left(rsImagenes("ima_imagen_peq"),5) & codigoTmp & right(rsImagenes("ima_imagen_peq"),5) & "')"
			else
				sql=sql & "'')"
			end if				
			connCRM.execute sql,intNumRegistros

			if trim(rsImagenes("ima_imagen"))<>"" then			
				'Imagen grande
				filename= strlocal & "modulos/usuariosftp/conexion/" & rsImagenes("ima_imagen")
				if fso.FileExists(filename) then
					set mainfile=fso.GetFile(filename)
					mainfile.Copy strlocal & "modulos/usuariosftp/conexion/" & left(rsImagenes("ima_imagen"),5) & codigoTmp & right(rsImagenes("ima_imagen"),5)
				end if
				
				'Thumnail imagen grande
				'filename= strlocal & "modulos/usuariosftp/temp/thn" & rsImagenes("ima_imagen")
				'if fso.FileExists(filename) then
				'	set mainfile=fso.GetFile(filename)
				'	mainfile.Copy strlocal & "modulos/usuariosftp/temp/thn" & left(rsImagenes("ima_imagen"),5) & codigoTmp & right(rsImagenes("ima_imagen"),5)
				'end if
							
				'ThumnailFoto imagen grande
				filename= strlocal & "modulos/usuariosftp/temp/thnAlb" & rsImagenes("ima_imagen")
				if fso.FileExists(filename) then
					set mainfile=fso.GetFile(filename)
					mainfile.Copy strlocal & "modulos/usuariosftp/temp/thnAlb" & left(rsImagenes("ima_imagen"),5) & codigoTmp & right(rsImagenes("ima_imagen"),5)
				end if
			end if
									
			'Si tiene foto peque�a
			if trim(rsImagenes("ima_imagen_peq"))<>"" then
				filename= strlocal & "modulos/usuariosftp/conexion/" & rsImagenes("ima_imagen_peq")
				if fso.FileExists(filename) then
					set mainfile=fso.GetFile(filename)
					mainfile.Copy strlocal & "modulos/usuariosftp/conexion/" & left(rsImagenes("ima_imagen_peq"),5) & codigoTmp & right(rsImagenes("ima_imagen_peq"),5)
				end if
			end if
						
			rsImagenes.MoveNext 
		wend

		rsImagenes.cerrar 
	end if
	
	if replicarArchivos then
		'clono los archivos asociados
		sql = "SELECT * FROM Archivos WHERE arc_sumenu=" & nodoorigen
		Set rsArchivos= connCRM.abrirRecordSet (sql,3,3)
		'intento copiar los ficheros 
		while not rsArchivos.EOF 
			'Sacamos el m�ximo c�digo
			sql = "SELECT MAX(Arc_Codigo) AS codigo FROM archivos"
			Set rsTmp = connCRM.abrirRecordSet(sql,3,3)
			CodigoTmp=rsTmp("codigo")+1
			rsTmp.cerrar

			sql = "insert into Archivos (Arc_codigo,Arc_descripcion,Arc_Archivo,Arc_SuFormato,Arc_SuMenu) VALUES ( " & codigotmp & ",'" & rsArchivos("Arc_descripcion") &  "','Archi" & codigoTmp & right(rsArchivos("Arc_Archivo"),5) & "'," & rsArchivos("Arc_SuFormato") & "," & nododestino & ")"
			connCRM.execute sql,intNumRegistros

			filename= strlocal & "modulos/usuariosftp/conexion/" & rsArchivos("Arc_Archivo")
			if fso.FileExists(filename) then
				set mainfile=fso.GetFile(filename)
				mainfile.Copy strlocal & "modulos/usuariosftp/conexion/archi" & codigoTmp & right(rsArchivos("Arc_Archivo"),5)
			end if
			rsArchivos.MoveNext 
		wend
		rsArchivos.cerrar 
	end if
	
	Set fso=nothing
	
end Sub
'#################################### FIN FUNCIONES PARA CLONAR SUBARBOLES DE NODOS

'Funcion que asigna el nuevo nivel al nodo, y lo va propagando por sus hijos
Sub propagarNivelYPortal (nodo,nuevoNivel,nuevoPortal,nuevaRuta)
	connCRM.execute "UPDATE Menus SET NivelM=" & nuevoNivel & ",suPortal=" & nuevoPortal & ", ruta='" & nuevaRuta & "' WHERE CodM=" & nodo ,intNumRegistros
	'sacamos todos sus hijos
	sql = "SELECT CodM FROM Menus WHERE PadreM=" & nodo
	Set rsHijos = connCRM.abrirRecordSet(sql,3,3)
	while not rsHijos.eof
		propagarNivelYPortal rsHijos("CodM"),nuevoNivel+1,nuevoPortal,nuevaRuta & "," & rsHijos("codm")
		rsHijos.movenext
	wend
	rsHijos.cerrar
End sub

'Nos da un string con los codigos de la ruta separados por comas
function Ruta(res,code,modo) 
	salida=""		'String de salida
	codigo = clng(code)	'variable que guarda el codigo en curso
	salir = false	'condicion de salida del bucle principal
	primero=true
	while not salir = true
		res.filter=" codm=" & clng(codigo)
		if res.eof then
			salir=true
		else
			if  primero=true then
				salida= res("codm") & salida
			else
				if codigo<>-1 then
					salida = res("codm") & "," & salida
				end if
			end if
			primero=false	
			if codigo=-1 then 
				salir=true
			else
				codigo = clng(res("padreM"))			
			end if
		end if
	wend
	Ruta =salida		
end function

'Esta funcion nos dice si un nodo es privado o no.
'Tenemos en cuenta que un nodo es privado si uno de sus ascendientes (padres) es privado (extranet=true)
'Le pasamos el c�digo del padre del nodo.
function tieneAscendientePrivado (cod)
	tieneAscendientePrivado = false
	if cod > -1 then 'Es idioma
		'Sacamos los datos de todos los menus
		sql = "SELECT CodM,padreM,NivelM,Extranet FROM Menus"
		Set rsPrivado = connCRM.AbrirRecordset(sql,3,3)

		'Sacamos los datos del nodo
		rsPrivado.filter = "CodM=" & Cod

		local_nivelM=rsPrivado("NIVELM")
		local_padreM=rsPrivado("PADREM")

		if rsPrivado("EXTRANET") then
			tieneAscendientePrivado = True
		end if
		rsPrivado.filter = 0 'Quitar filtro
		
		while local_PadreM<>-1 and tieneAscendientePrivado=false
			'Sacamos la informaci�n del padre
			rsPrivado.filter = "CodM=" & Local_padreM
			if rsPrivado("EXTRANET") then
				tieneAscendientePrivado = True
			end if
			local_padreM = rsPrivado("PADREM")
			rsPrivado.filter = 0 'Quitar filtro
		wend
		rsPrivado.Cerrar ()
	end if
end function

'Traducir buleano
function TraducirBuleano(Valor)
	if Valor then
		TraducirBuleano="-1"
	else
		TraducirBuleano="0"
	end if	
end function

'#############################################################################################
'#################################### FUNCIONES DE PERMISOS ##################################
'#############################################################################################

'A�adirPermiso
function AnadirPermiso(Nodo,Usuario,Grupo,Tipo)
	sql="SELECT Extranet as privado, (SELECT COUNT(*) FROM MENUS_PERMISOS WHERE MEPE_SUMENU=" & NODO & " AND MEPE_SUUSUARIO=" & USUARIO & " AND MEPE_SUGRUPO=" & GRUPO & " AND MEPE_SUTIPO=" & TIPO & " ) as ConPermiso FROM MENUS WHERE CODM=" & NODO
	Set rsPermisos = connCRM.AbrirRecordset(sql,3,3)
	'Mirar primero si existe el registro
	'Miro a ver si es privado
	if (tipo=1 and rsPermisos("privado")=-1 AND  rsPermisos("ConPermiso")=0) or (tipo=2 AND  rsPermisos("ConPermiso")=0) then
		sql="INSERT INTO MENUS_PERMISOS (MEPE_SUMENU,MEPE_SUUSUARIO,MEPE_SUGRUPO,MEPE_SUTIPO)"
		sql=sql & " VALUES (" & Nodo & "," & Usuario & "," & Grupo & "," & Tipo & ")"
		connCRM.execute sql,intNumRegistros
	end if
	rspermisos.cerrar()
	set rsPermisos=nothing
end function

'A�ada al Nodo1 los permisos del Nodo2
function AnadirPermisosNodo(Nodo1,Nodo2)
	sql = "SELECT * FROM MENUS_PERMISOS WHERE MEPE_SUMENU=" & nodo2
	Set rsPermisos = connCRM.AbrirRecordset(sql,3,3)

	while not rsPermisos.eof
		AnadirPermiso NODO1,rsPermisos("MEPE_SUUSUARIO"),rsPermisos("MEPE_SUGRUPO"),rsPermisos("MEPE_SUTIPO")
		rsPermisos.movenext()
	wend
	rspermisos.cerrar()
	set rsPermisos=nothing
end function

'A�ade los permisos ascendentemente
function AnadirPermisosLecturaAscendente(Nodo,Usuario,Grupo,Tipo)
	sql = "SELECT Ruta,MEPE_SUUSUARIO,MEPE_SUGRUPO FROM MENUS_PERMISOS INNER JOIN MENUS ON MENUS_PERMISOS.MEPE_SUMENU=MENUS.CODM WHERE MEPE_SUTIPO=1 AND MEPE_SUMENU=" & nodo
	if Tipo>0 then sql=sql & " AND MEPE_SUTIPO=" & Tipo
	if Usuario>0 then sql=sql & " AND MEPE_SUUSUARIO=" & Usuario
	if Grupo>0 then sql=sql & " AND MEPE_SUGRUPO=" & Grupo
	Set rsPermisos = connCRM.AbrirRecordset(sql,3,3)
	if not rsPermisos.eof then
		NodosPadres=split(rsPermisos("RUTA"),",")
		while not rsPermisos.eof
			for contPer=1 to ubound(NodosPadres)
				AnadirPermiso NodosPadres(contPer),rsPermisos("MEPE_SUUSUARIO"),rsPermisos("MEPE_SUGRUPO"),1
			next
			rsPermisos.movenext()
		wend
	end if	
	rspermisos.cerrar()
	set rsPermisos=nothing
end function

'Propaga 
function PropagarPrivacidad(Ruta)
	sql = "SELECT Extranet FROM MENUS WHERE  left(RUTA," & len(ruta)+1 & ")='" & ruta & ",' ORDER BY RUTA ASC"
	Set rsMenus = connCRM.AbrirRecordset(sql,3,3)
		while not rsMenus.eof
			rsMenus("extranet")=true
			rsMenus.update()
			rsMenus.movenext()
		wend
	rsMenus.cerrar()
	set rsMenus=nothing
end function


'A�ade los permisos descendentemente
function AnadirPermisosDescendentemente(Nodo,Usuario,Grupo,Tipo)
	
	sql = "SELECT Ruta,MEPE_SUUSUARIO,MEPE_SUGRUPO,MEPE_SUTIPO FROM MENUS_PERMISOS INNER JOIN MENUS ON MENUS_PERMISOS.MEPE_SUMENU=MENUS.CODM WHERE MEPE_SUMENU=" & nodo 
	if Tipo>0 then sql=sql & " AND MEPE_SUTIPO=" & Tipo
	if Usuario>0 then sql=sql & " AND MEPE_SUUSUARIO=" & Usuario
	if Grupo>0 then sql=sql & " AND MEPE_SUGRUPO=" & Grupo
	Set rsPermisos = connCRM.AbrirRecordset(sql,3,3)

	sql = "SELECT codM,Ruta,HEREDA_PERMISOS FROM MENUS WHERE  left(RUTA," & len(rsPermisos("RUTA"))+1 & ")='" & rsPermisos("RUTA") & ",' AND CODM<>" & nodo & " ORDER BY RUTA ASC"
	Set rsMenus = connCRM.AbrirRecordset(sql,3,3)
	
	while not rsPermisos.eof
		if not rsMenus.bof then rsMenus.movefirst 		
		NodoNO=""
		while not rsMenus.eof
			if NodoNO<>"" then
				if NodoNO<>left(rsMenus("RUTA"),len(nodono)) then
					if rsMenus("HEREDA_PERMISOS")=false then 
						NodoNo=rsMenus("RUTA")
					else
						NodoNo=""
					end if
				end if		
			else
				if rsMenus("HEREDA_PERMISOS")=false then 
					NodoNo=rsMenus("RUTA")
				else
					NodoNo=""
				end if
			end if
			if NodoNO="" then AnadirPermiso rsMenus("codM"),rsPermisos("MEPE_SUUSUARIO"),rsPermisos("MEPE_SUGRUPO"),rsPermisos("MEPE_SUTIPO")			
			rsMenus.movenext()
		wend
		rsPermisos.movenext()
	wend
	
	rsMenus.cerrar()
	set rsMenus=nothing
	rspermisos.cerrar()
	set rsPermisos=nothing
end function

'Borrar permisos descendentemente
function BorrarPermisosDescendentemente(Nodo,Usuario,Grupo,Tipo)
	sql = "SELECT Ruta FROM MENUS WHERE codM=" & nodo 
	Set rsPermisos = connCRM.AbrirRecordset(sql,3,3)
	
	sql = "SELECT CodM,RUTA,HEREDA_PERMISOS FROM MENUS WHERE  left(RUTA," & len(rsPermisos("RUTA"))+1 & ")='" & rsPermisos("RUTA") & ",' AND CODM<>" & nodo & " ORDER BY RUTA ASC"
	Set rsMenus = connCRM.AbrirRecordset(sql,3,3)
	
	if not rsPermisos.eof then
		NodoNO=""
		while not rsMenus.eof
			if NodoNO<>"" then
				if NodoNO<>left(rsMenus("RUTA"),len(nodono)) then
					if rsMenus("HEREDA_PERMISOS")=false then 
						NodoNo=rsMenus("RUTA")
					else
						NodoNo=""
					end if
				end if		
			else
				if rsMenus("HEREDA_PERMISOS")=false then 
					NodoNo=rsMenus("RUTA")
				else
					NodoNo=""
				end if
			end if
			
			sql="DELETE FROM MENUS_PERMISOS WHERE MEPE_SUMENU=" & rsMenus("codM")
			if Tipo>0 then sql=sql & " AND MEPE_SUTIPO=" & Tipo
			if Usuario>0 then sql=sql & " AND MEPE_SUUSUARIO=" & Usuario
			if Grupo>0 then sql=sql & " AND MEPE_SUGRUPO=" & Grupo
			if NodoNO="" then connCRM.execute sql,intNumRegistros
			rsMenus.movenext()
		wend
	end if
	
	rsMenus.cerrar()
	set rsMenus=nothing
	rspermisos.cerrar()
	set rsPermisos=nothing
end function

'Borra los permisos de un usuario
function BorrarPermisosUsuario(USUARIO)
	sql = "DELETE FROM MENUS_PERMISOS WHERE MEPE_SUUSUARIO=" & USUARIO
	connCRM.execute sql,intNumRegistros
end function

'Borra los permisos de usuario
function BorrarPermisosGrupo(GRUPO)
	sql = "DELETE FROM MENUS_PERMISOS WHERE MEPE_SUGRUPO=" & GRUPO
	connCRM.execute sql,intNumRegistros
end function

'Borra los permisos de un menu
function BorrarPermisosMenu(MENU)
	sql = "DELETE FROM MENUS_PERMISOS WHERE MEPE_SUMENU=" & MENU
	connCRM.execute sql,intNumRegistros
end function

'Borra los permisos de un menu
function BorrarPermisosTipo(MENU,Tipo)
	sql = "DELETE FROM MENUS_PERMISOS WHERE MEPE_SUTIPO=" & tipo & " AND MEPE_SUMENU=" & MENU
	connCRM.execute sql,intNumRegistros
end function
'#############################################################################################
'#################################### FUNCIONES DE PERMISOS ##################################
'#############################################################################################
%>
