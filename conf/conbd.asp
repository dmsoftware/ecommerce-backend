<!-- #include virtual="/dmcrm/conf/dll.asp"-->
<!-- #include virtual="/dmcrm/conf/clsIdiomas.asp"-->
<!-- #include virtual="/dmcrm/conf/conexionCRM.asp"-->
<%
'BASE DATOS DMGESTION Y DMGESTION WEB


'Abrimos una conexion auxiliar para la maestra si es necesario
if trim(request("conex"))<>"" then    
    select case trim(request("conex"))
        case "2"
            conexionMaestra = "Provider=SQLNCLI10;Data Source=;Database=;Uid=;Password="
    end select
    set connMaestra = new DMWConexion
    connMaestra.abrir ("" & conexionMaestra)
end if

nivelbd = "/dmcrm/"

var_sgbd="2"
almohadiFecha = "'"

'Para la clase mantenimientonomSubMenu
session("_conexion1001")=conexion 

'Conexion DMCorporative +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	'Comprabamos si el CRM tiene parte publica, y en caso afirmativo, generamos la conexion hacia DMCorporative. 
	'Guardamos en una variable boolean que se a abierto la conexion para luego cerrarla.
	'Tambi�n aprobechamos el acceso para almacenar el booleano clubPaginaInicio para saber si hay que redirigir a la pagina de socios del club de compra o al que corresponse seg�n el ussuario.
	'Y el dominio del CRM
	conDMCorporative=false
	conDMIntegra=false		
	clubPaginaInicio=false
	conIVAIncluido=false
	dominioCRM=""
	sql=" Select CONF_CONIVAINCLUIDO,CONF_CONDMCORPORATIVE,CONF_DOMINIOSITIOWEBDMINTEGRA,CONF_CONCLUBCOMPRA,CONF_DOMINIOSITIOWEB,CONF_PAGINICIO,CONF_PAGINICIO_DIRECTORCUENTA,CONF_PAGINICIO_COMERCIOS From CONFIGURACION "
	Set rsConDMCorporative= connCRM.AbrirRecordset(sql,3,1)

	if not rsConDMCorporative.EOf then
		conIVAIncluido=rsConDMCorporative("CONF_CONIVAINCLUIDO")	
		conDMCorporative=rsConDMCorporative("CONF_CONDMCORPORATIVE")
		conDMIntegra=rsConDMCorporative("CONF_DOMINIOSITIOWEBDMINTEGRA")				
		conClubCompra=rsConDMCorporative("CONF_CONCLUBCOMPRA")
		dominioCRM=rsConDMCorporative("CONF_DOMINIOSITIOWEB")
		
		pagInicioNormal=rsConDMCorporative("CONF_PAGINICIO")
		pagInicioDirectorCuenta=rsConDMCorporative("CONF_PAGINICIO_DIRECTORCUENTA")
		pagInicioComercios=rsConDMCorporative("CONF_PAGINICIO_COMERCIOS")				
		
	end if
	rsConDMCorporative.cerrar()
	set rsConDMCorporative=nothing 

'Conexion DMCorporative +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

'Conexion DMIntegra +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if blnAbrirConexionDMIntegra then
    conexionDMI = "Provider=SQLNCLI10;Data Source=;Database=;Uid=;Password="
    set connDMI = new DMWConexion
    connDMI.abrir ("" & conexionDMI)
    strNombreBDCRM=""
end if

'Idioma Usuario +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  Const DEFAULT_LANGUAGE = "es"
    '1/ Idioma del par�metro
  if request.QueryString("lang")<>"" then
     Idioma = request.QueryString("lang")
     'Guardamos la opci�n para el usuario actual (si est� logeado)
     if session("codigoUsuario")<>"" then
        if session("TipoUsuario")="analistas" then
            sql="Update ANALISTAS SET ANA_IDIOMA='" & Idioma & "' WHERE ANA_CODIGO=" & session("codigoUsuario")
        else
            sql="Update CLUB_COMERCIOS SET COMER_IDIOMA='" & Idioma & "' WHERE COMER_CODIGO=" & session("codigoUsuario")
        end if
        connCRM.execute sql,intNumeroRegistros
     end if 
     'Cambiamos la sesion
     Session("IdiomaUsuario") = Idioma  
  end if
  '2/ Idioma de la sesion
  If Idioma = "" then Idioma = Session("IdiomaUsuario")
  '3/ Idioma de la cookie
  if Idioma = "" then
     if Request.Cookies("UltimoUsuarioIdioma")<>"" then
         Idioma=Request.Cookies("UltimoUsuarioIdioma")
     end if
  end if
  '4/ Idioma del navegador
  If Idioma = "" then 
        select case Trim(left(request.ServerVariables("HTTP_ACCEPT_LANGUAGE"),2))
            'Marcamos los idiomas que est�n disponibles
            case "es","eu"
                Idioma = Trim(left(request.ServerVariables("HTTP_ACCEPT_LANGUAGE"),2))
        end select
  end if
  '5/ Si, viene vacia despu�s de todo lo que ha pasado, pues le asignamos la constante
  If Idioma = "" then Idioma = DEFAULT_LANGUAGE
  
'Idioma usuario +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

'Creaci�n de las COOKIES RECORDATORIAS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'Las creamos antes de escribir <HTML> si ha cambiado su contenido
if Request.Cookies("UltimoUsuario")<>session("emailAdministracion") then
    if session("emailAdministracion")<>"" then
        Response.Cookies ("UltimoUsuario") = session("emailAdministracion")
        Response.Cookies ("UltimoUsuario").Expires = dateadd("m",1,date)
    end if
end if
if Request.Cookies("UltimoUsuarioIdioma")<>Idioma then
    if Idioma<>"" then
        Response.Cookies ("UltimoUsuarioIdioma") = Idioma
        Response.Cookies ("UltimoUsuarioIdioma").Expires = dateadd("m",1,date)
        ''Response.Cookies ("UltimoUsuario").Path = "/"
    end if
end if

'Si estamos en index.asp, necesitaremos leer la cookie
UltimoUsuario=Request.Cookies("UltimoUsuario")
UltimoUsuarioIdioma = Request.Cookies("UltimoUsuarioIdioma")
'Creaci�n de las COOKIES RECORDATORIAS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


'Menu/Submenu +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
pagina=Request.ServerVariables ("SCRIPT_NAME")
sql="select * from ((app_menuprincipal inner join app_menus on app_menuprincipal.menp_codigo=app_menus.men_sumenuprincipal )"
sql=sql & " inner join app_menus_analistas on app_menus.men_codigo=app_menus_analistas.mena_sumenu) "
''sql=sql & " where mena_suanalista=" & session("codigoUsuario") & " And MEN_Enlace like '" & pagina & "%'"
sql=sql & " where MEN_Enlace like '" & pagina & "%'"
sql=sql & " order by menp_orden,men_orden "
Set rsObtenerMenu= connCRM.AbrirRecordset(sql,0,1)
nomMenu=""
nomSubMenu=""
separadorMenu=""

'Buscamos, si la p�gina no corresponde con un men�, si el referer si proviene de un men� 
if rsObtenerMenu.eof then
    'Buscamos el men� al que corresponde el REFERER de la p�gina
    pagina=Request.ServerVariables ("HTTP_REFERER")
    'Si no tenemos referer (por ejemplo en el index.asp)
    if pagina="" and nomFicheroIdiomas="" then
        nomFicheroIdiomas="SinMenu"
    else
        arrPagina=split(pagina,"/")
        for intI=3 to ubound(arrPagina)
            strPagina= strPagina &  "/" & arrPagina(intI)
        next
        'Eliminamos los par�metros de la p�gina
        arrPagina=split(strPagina,"?")
		if ubound(arrPagina)>0 then
	        strPaginaSinParametros=arrPagina(0)
		else
	        strPaginaSinParametros=strPagina	
		end if	
        sql="select * from ((app_menuprincipal inner join app_menus on app_menuprincipal.menp_codigo=app_menus.men_sumenuprincipal )"
        sql=sql & " inner join app_menus_analistas on app_menus.men_codigo=app_menus_analistas.mena_sumenu) "
        ''sql=sql & " where mena_suanalista=" & session("codigoUsuario") & " And MEN_Enlace like '" & strPagina & "%'"
        sql=sql & " where MEN_Enlace like '" & strPaginaSinParametros & "%'"
        sql=sql & " order by menp_orden,men_orden "
        rsObtenerMenu.cerrar
        Set rsObtenerMenu= connCRM.AbrirRecordset(sql,0,1)
    end if
end if

if not rsObtenerMenu.eof then
    if rsObtenerMenu("MENP_MENU_" & Idioma)<>"" then
        nomMenu=rsObtenerMenu("MENP_MENU_" & Idioma)
    else
        'Ponemos la de castellano
        nomMenu=rsObtenerMenu("MENP_MENU_ES")
    end if
    if rsObtenerMenu("men_menu_" & Idioma)<>"" then
        nomSubMenu=rsObtenerMenu("MEN_MENU_" & Idioma)
    else
        'Ponemos la de castellano
        nomSubMenu=rsObtenerMenu("MEN_MENU_ES")
    end if
    'S�lo hallamos el nombre del fichero de idiomas si no lo hemos escrito en la p�gina
    if nomFicheroIdiomas="" then nomFicheroIdiomas=rsObtenerMenu("MENP_MENU_ES")
	separadorMenu=" --> "	
end if
rsObtenerMenu.cerrar()
set rsObtenerMenu=nothing 
'Fin Menu/Submenu +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

'Idiomas +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'Creamos objeto de idioma
'Hallamos el idioma del usuario
'Cargamos el fichero de idioma adecuado al idioma del usuario y a la p�gina en curso
  
   
'Abrimos el fichero espec�fico de cada p�gina
if nomFicheroIdiomas<>"SinMenu" then
    'Creamos nuestro objeto para leer XML
    set objIdioma = new clsIdioma  
      
    set fs=Server.CreateObject("Scripting.FileSystemObject")
    if fs.FileExists(Server.MapPath("\dmcrm\app\lang") & "\" & Idioma & "\Idiomas_" & nomFicheroIdiomas & ".xml")=true then
        strError = objIdioma.Load(Server.MapPath("\dmcrm\app\lang") & "\" & Idioma & "\Idiomas_" & nomFicheroIdiomas & ".xml")
    else
        set fs=Server.CreateObject("Scripting.FileSystemObject")
        if fs.FileExists(Server.MapPath("\dmcrm\app\lang") & "\ES\Idiomas_" & nomFicheroIdiomas & ".xml")=true then
            strError = objIdioma.Load(Server.MapPath("\dmcrm\app\lang") & "\ES\Idiomas_" & nomFicheroIdiomas & ".xml")
        end if
    end if
    set fs=nothing
    if strError<>"" then response.write strError & "<br><br>"

end if

'Abrimos el fichero xml General
'Creamos nuestro objeto para leer XML
set objIdiomaGeneral = new clsIdioma  
strError = objIdiomaGeneral.Load(Server.MapPath("\dmcrm\app\lang") & "\" & Idioma & "\Idiomas_General.xml")
if strError<>"" then response.write strError & "<br><br>"

'Fin Idiomas +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%>
