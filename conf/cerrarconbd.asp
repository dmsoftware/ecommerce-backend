<!-- #include virtual="/dmcrm/conf/conexionCRM_cerrar.asp"-->
<%
'Cerramos conexiones a BDs

if blnAbrirConexionFacturaplus=true then
'    connF.cerrar
 '   set connF=nothing
end if

'Conexion DMIntegra
if blnAbrirConexionDMIntegra then
    connDMI.cerrar
    set connDMI=nothing
end if

'Cerramos la conexion de la maestra si es necesaria
if trim(request("conex"))<>"" then  
    connMaestra.cerrar()
    set connMaestra=nothing
end if

'Cerramos los dos objetos de idiomas
if nomFicheroIdiomas<>"SinMenu" then
    set objIdioma=nothing
end if
set objIdiomaGeneral=nothing


'*******************************************************************
'Comprobamos los contadores de CURSORES Y CONEXIONES
'INICIO UNOS CURSORES PARA ORDENAR LAS APERTURAS Y CIERRES

if blnDebugar then
    
    '********** Tiempos de ejecucion de la p�gina *********************
    response.write "<br><br><hr><font face=verdaza size=1 color=ff0000>Modo Debug. <b>"
    response.write datediff("s",HoraInicio,now) & " seg.</b><br></font>"


	'***************** CURSORES ****************************************
	Set rstAuxAbrir = Server.CreateObject("ADODB.Recordset")
	rstAuxAbrir.Fields.Append "cadena", 200,255
	rstAuxAbrir.CursorLocation = 3 'adUseClient
	rstAuxAbrir.CursorType = 2 'adOpenDynamic
	rstAuxAbrir.Open	

	Set rstAuxCerrar = Server.CreateObject("ADODB.Recordset")
	rstAuxCerrar.Fields.Append "cadena", 200,255
	rstAuxCerrar.CursorLocation = 3 'adUseClient
	rstAuxCerrar.CursorType = 2 'adOpenDynamic
	rstAuxCerrar.Open
	arrCursorAbrir=split(strCursorAbrir,"<br>")
	for intI=0 to ubound(arrCursorAbrir)
		rstAuxAbrir.AddNew
		rstAuxAbrir.Fields("cadena") = replace(left(arrCursorAbrir(intI),255),"'","")
		rstAuxAbrir.Update 
	next

	arrCursorCerrar=split(strCursorCerrar,"<br>")
	for intI=0 to ubound(arrCursorCerrar)
		rstAuxCerrar.AddNew
		rstAuxCerrar.Fields("cadena") = replace(left(arrCursorCerrar(intI),255),"'","")
		rstAuxCerrar.Update 
	next

	'Ordeno por nombre y comparo
	rstAuxAbrir.Sort = "cadena asc"
	while not rstAuxAbrir.EOF
	    
	    rstAuxCerrar.Filter = "cadena='" & rstAuxAbrir("cadena") & "'"
	    if not rstAuxCerrar.EOF then
	       'Borro los dos registros
	       rstAuxCerrar.Delete
	       rstAuxAbrir.Delete
	    else
	        strCursorSinCerrar=strCursorSinCerrar & "<br><br>" & rstAuxAbrir("cadena")
	    end if
	    rstAuxAbrir.MoveNext 
	wend	
	rstAuxAbrir.close
	rstAuxCerrar.close
	set rstAuxAbrir=nothing
	set rstAuxCerrar=nothing
	'***************** CURSORES ****************************************

	'***************** CONEXIONES ****************************************
	Set rstAuxAbrir = Server.CreateObject("ADODB.Recordset")
	rstAuxAbrir.Fields.Append "cadena", 200,255
	rstAuxAbrir.CursorLocation = 3 'adUseClient
	rstAuxAbrir.CursorType = 2 'adOpenDynamic
	rstAuxAbrir.Open	

	Set rstAuxCerrar = Server.CreateObject("ADODB.Recordset")
	rstAuxCerrar.Fields.Append "cadena", 200,255
	rstAuxCerrar.CursorLocation = 3 'adUseClient
	rstAuxCerrar.CursorType = 2 'adOpenDynamic
	rstAuxCerrar.Open

	arrConexionAbrir=split(strConexionAbrir,"<br>")
	for intI=0 to ubound(arrConexionAbrir)
		rstAuxAbrir.AddNew
		rstAuxAbrir.Fields("cadena") = replace(left(arrConexionAbrir(intI),255),"'","")
		rstAuxAbrir.Update 
	next

	arrConexionCerrar=split(strConexionCerrar,"<br>")
	for intI=0 to ubound(arrConexionCerrar)
		rstAuxCerrar.AddNew
		rstAuxCerrar.Fields("cadena") = replace(left(arrConexionCerrar(intI),255),"'","")
		rstAuxCerrar.Update 
	next

	'Ordeno por nombre y comparo
	rstAuxAbrir.Sort = "cadena asc"
	while not rstAuxAbrir.EOF
	    
	    rstAuxCerrar.Filter = "cadena='" & rstAuxAbrir("cadena") & "'"
	    if not rstAuxCerrar.EOF then
	       'Borro los dos registros
	       rstAuxCerrar.Delete
	       rstAuxAbrir.Delete
	    else
	        strConexionSinCerrar=strConexionSinCerrar & "<br><br>" & rstAuxAbrir("cadena")
	    end if
	    rstAuxAbrir.MoveNext 
	wend	
	rstAuxAbrir.close
	rstAuxCerrar.close
	'***************** CONEXIONES ****************************************
						
	if strConexionSinCerrar<>"" then
	   Response.Write "<br><br><b><font color=ff0000>CONEXION: abiertas " & intConexionAbrir & " - cerradas " & intConexionCerrar & "</b><br>"
	   Response.Write strConexionSinCerrar  
       Response.write "<br><br>" 
    else
       Response.Write "<br><br><b><font color=ff0000>CONEXIONES UTILIZADAS: " & intConexionAbrir & "</b><br>"
	end if
	if strCursorSinCerrar<>"" then
	   Response.Write "<br><br><b><font color=ff0000>CURSOR: abiertos " & intCursorAbrir & " - cerrados " & intCursorCerrar & "</b><br>"
	   Response.Write strCursorSinCerrar
    else
       Response.Write "<br><br><b><font color=ff0000>CURSORES UTILIZADOS: " & intCursorAbrir & "<br>"
	end if

end if
'*********************************************************************
%>
