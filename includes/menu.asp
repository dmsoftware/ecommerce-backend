<%
'** VALIDACI�N DE CAMBIO DE CONTRASE�A **********************************************
'** para que mientras exista la sesion siempre se presente el formulario de cambio
if session("ContrasenaCambiar") or session("ContrasenaVacia") then
    if instr(request.servervariables("script_name"),"cambiarcontrasena.asp")=0 then
        response.redirect "/dmcrm/app/rrhh/cambiarcontrasena.asp"
    end if
end if

'************************************************************************************
%>


<div id="capaMenu">
    <ul id="nav" style="z-index:2;">
        <li <%if trim(nomMenu)="" then%> class="activo"<%end if%>><a href="/dmcrm/APP/principal.asp"><%=objIdiomaGeneral.getIdioma("Menu_1")%></a></li>
        <%
        'Hallamos los men�s principales y secundarios a los que tiene acceso el usuario. Distinguimos Analistas y Comercios
        if session("TipoUsuario")="analistas" then
            sql="select * from ((app_menuprincipal inner join app_menus on app_menuprincipal.menp_codigo=app_menus.men_sumenuprincipal )"
            sql=sql & " inner join app_menus_analistas on app_menus.men_codigo=app_menus_analistas.mena_sumenu) "
            sql=sql & " where mena_suanalista=" & session("codigoUsuario")
            sql=sql & " order by menp_orden,men_orden "
        else
            'A los comercios les ponemos los men�s 14,15 y 16
            sql="select * from (app_menuprincipal inner join app_menus on app_menuprincipal.menp_codigo=app_menus.men_sumenuprincipal )"
            sql=sql & " where men_sumenuprincipal in (14,15,16)"
            sql=sql & " order by menp_orden,men_orden "            
        end if
        Set rstMenus= connCRM.AbrirRecordset(sql,0,1)
	    registrosVacios=False
	    if rstMenus.eof then 
		    registrosVacios=True
	    end if	
        while not rstMenus.eof
    
            '1/ Lo primero pintamos el menu principal
            if strMenuPrincipalAnterior<>rstMenus("menp_codigo") then
                'La primera vez no pongo los cierres del menu principal
                if strMenuPrincipalAnterior<>"" then%>
                        </ul>
                    </li>
                <%end if
                strMenuPrincipalAnterior=rstMenus("menp_codigo")%>
                <li<%if trim(lcase(nomMenu))=trim(lcase(rstMenus("menp_menu_" & Idioma))) then%> class="activo"<%end if%>>
                    <a href="#">
                    <%if rstMenus("menp_menu_" & Idioma)<>"" then
                        response.write rstMenus("menp_menu_" & Idioma)
                    else
                        'Ponemos la de castellano
                        response.write rstMenus("menp_menu_es")
                    end if%>
                    </a>
                <ul class="submenu">
            <%end if %>
    
                <%'Comprobamos si viene un elemento de men� o un separador
                if ucase(rstMenus("men_menu_es"))="SEPARADOR" then%>
                     <li class="linea">&nbsp;</li>
                <%else%>
                     <li<%if trim(lcase(nomSubMenu))=trim(lcase(rstMenus("men_menu_" & Idioma))) then%> class="activo"<%end if%>>
                     <a href="<%=rstMenus("men_enlace")%>">
                     <%if rstMenus("men_menu_" & Idioma)<>"" then
                        response.write rstMenus("men_menu_" & Idioma)
                     else
                        'Ponemos la de castellano
                        response.write rstMenus("men_menu_es")
                     end if%>                     
                     </a></li>
                <%end if%>
            <%rstMenus.movenext
        wend
	    rstMenus.cerrar
        set rstMenus=nothing
	    if not registrosVacios then%>
            </ul> 
	    <%end if%>
        </li>
        <li><a href="/dmcrm/index.asp"><%=objIdiomaGeneral.getIdioma("Menu_2")%></a></li>
    </ul>
</div>