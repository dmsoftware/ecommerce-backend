<%
''''''''Normalizaciones''''''''''
function normalizarNombre(nombre)
	nombreNomalizado=""'nombre
	
	separadorLetra=true
	for contLetras=1 to len(nombre) 
		letraActual=Mid(nombre,contLetras,1)
		'response.Write(letraActual)

		if separadorLetra then
			nombreNomalizado=nombreNomalizado & UCase(letraActual)
			separadorLetra=false
		else
			nombreNomalizado=nombreNomalizado & lcase(letraActual)			
		end if

            set patron = new regexp
            patron.Pattern = "^[A-Za-z�-��-���]*$"
            patron.IgnoreCase=false
            if not patron.Test(letraActual) then
                on error resume next
				separadorLetra=true				
            end if
            set patron=nothing
'		nombreNomalizado=nombreNomalizado & Mid(nombre,contLetras,1)

	next
	
	normalizarNombre=nombreNomalizado
end function


function normalizarNumericos(texto)
	textoNomalizado=texto
	
	for contLetras=1 to len(texto) 
		letraActual=Mid(texto,contLetras,1)

            set patron = new regexp
            patron.Pattern = "^[0-9]*$"
            patron.IgnoreCase=false
            if not patron.Test(letraActual) then
                on error resume next
				textoNomalizado=replace(textoNomalizado,letraActual,"")
            end if
            set patron=nothing

	next
	
	normalizarNumericos=textoNomalizado
end function

function normalizarDNI(texto)
	textoNomalizado=texto
	
	for contLetras=1 to len(texto) 
		letraActual=Mid(texto,contLetras,1)

            set patron = new regexp
            patron.Pattern = "^[0-9A-Za-z�-��-���]*$"
            patron.IgnoreCase=false
            if not patron.Test(letraActual) then
                on error resume next
				textoNomalizado=replace(textoNomalizado,letraActual,"")
            end if
            set patron=nothing

	next
	
	normalizarDNI=textoNomalizado
end function

''''''''Normalizaciones''''''''''
'**************************************************************** Nuevas funciones para thumbnail ***************************
'****************************************************************************************************************************
thumbnailHabilitado=1
function thumbnail (imagen,width,height,alt)
	thumbnail = thumbnailOrigen(imagen,width,height,alt)
end function

function thumbnailOrigen(imagen,width,height,alt)
'Parametros:

	'imagen: ruta relativa a la imagen desde la raiz
	'width: Ancho que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al alto
	'height: Alto que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al ancho
	
	'Remplazo los %20 por espacios
	Imagen=replace(imagen,"%20"," ")
		
	'Si EsCuadrado calculo los nuevos anchos y altos
	'width: Ancho que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al alto
	'height: Alto que tendra la imagen despu�s del proceso. Si es cero se hace proporcional al ancho
	
	'Remplazo los %20 por espacios
	Imagen=replace(imagen,"%20"," ")
	
	'comprobamos que la imagen tenga una extension v�lida
	valido_extension=false
	strFormatos=",jpg,jpeg,gif,png,bmp,"
	arrAux=split(imagen,".")
	if imagen<>"" and not isnull(imagen) then
		extension_img=right(imagen,4)
	end if
	extension_img=replace(extension_img,".","")
	if instr(strFormatos,","&lcase(extension_img)&",")>0 then
		valido_extension=true
	end if

	if valido_extension=true then
		if thumbnailHabilitado=0 or extension_img = "gif" or extension_img = "png" then
			strTmp = "<img style=""border-width:0"" src=""" & pathabsoluto & imagen & """"
			if width<>0 then strTmp = strTmp & " width=""" & width & """"
			if height<>0 then strTmp = strTmp & " height=""" & height & """"
			if alt<>"" then	strTmp = strTmp & " alt=""" & alt & """ "
			strTmp = strTmp &  "/>"
			thumbnailOrigen = strTmp
		else	    
			'Creamos un objeto general a toda la p�gina de fileSystemObject para usar �nicamente este y no abrir m�s
			'Abrimos el objeto s�lo la primera vez que lo llamamos en esta p�gina
			'Lo cerramos en cerrarConbd.asp si se ha creado
			if not blnYaEstaCreadoObjetoFileSystemObject then    
				Set oFSO = Server.CreateObject("Scripting.FileSystemObject")
				blnYaEstaCreadoObjetoFileSystemObject=true
			end if
		
			if oFSO.fileexists(server.MapPath(imagen)) then
	
					'Primero: debemos comprobar que existe el thumbnail a ese tama�. Si no existe se crea
					'Segundo: enviamos la ruta del archivo thumbnail adecuado con el formato --> TMP/THN_NOMBREIMAGEN_WIDHT_HEIGHT_AMPLIAR.EXT
					strRutaThumbnail=ComprobarThumbnail (imagen,width,height) 
					thumbnailOrigen= "<img style=""border-width:0"" src=""" & strRutaThumbnail & """ alt=""" &alt & """/>"			
	
			else 
				thumbnailOrigen=""
			end if	
		end if
	else
		thumbnailOrigen=""
	end if
end function

'Esta funcion me comprueba que exista el thumbnail a los tama�os y con el ampliar en el directorio TMP. Si no existe lo crea.
function ComprobarThumbnail(imagen,width,height)

    'Compruebo que exista la imagen
    'TMP/THN_NOMBREIMAGEN_WIDHT_HEIGHT_AMPLIAR.EXT 
    strNombreThn="_" & width & "_" & height 
	
		
    arrAux=split(imagen,".")
    strExtensionOriginal=arrAux(ubound(arrAux))
    strAuxNombre=left(imagen,len(imagen)-len(strExtensionOriginal)-1)
    arrAux1=split(strAuxNombre,"/")
    strNombreOriginal=arrAux1(ubound(arrAux1))
    strNombreArchivoThumbnail=  "thn_" &  strNombreOriginal & strNombreThn &  "." & strExtensionOriginal

    
    'RutaThumbnail= "/" & carpetaInterna & "/usuariosftp/tmp/" & strNombreArchivoThumbnail
	RutaThumbnail= "/dmcrm/APP/UsuariosFtp/thn/" & strNombreArchivoThumbnail

    
    'Creamos un objeto general a toda la p�gina de fileSystemObject para usar �nicamente este y no abrir m�s
    'Abrimos el objeto s�lo la primera vez que lo llamamos en esta p�gina
    'Lo cerramos en cerrarConbd.asp si se ha creado
    if not blnYaEstaCreadoObjetoFileSystemObject then    
		Set oFSO = Server.CreateObject("Scripting.FileSystemObject")
		blnYaEstaCreadoObjetoFileSystemObject=true
	end if

    if not oFSO.fileexists(Server.MapPath(RutaThumbnail)) then
            'Alberto 19/02/2009.-
            'De todos modos quito el tema de la lupa en arsys.
            
					'Creamos un objeto general a toda la p�gina de fileSystemObject para usar �nicamente este y no abrir m�s
					'Abrimos el objeto s�lo la primera vez que lo llamamos en esta p�gina
					'Lo cerramos en cerrarConbd.asp si se ha creado
					if not blnYaEstaCreadoObjetosASPJPEG then    
						Set aspJpeg = Server.CreateObject("Persits.Jpeg")
						Set LupaJpeg = Server.CreateObject("Persits.Jpeg")
						blnYaEstaCreadoObjetosASPJPEG=true
					end if
					aspJpeg.Open(server.MapPath(replace(replace("/" & imagen,"//","/"),"\\","\")))
					aspJpeg.PreserveAspectRatio = True
							
					x = width
					if x="0" then x=""
					y = height
					if y="0" then y=""

						if x="" and y<>"" then
							''aspJpeg.Width = aspJpeg.OriginalWidth * y / aspJpeg.OriginalHeight
							aspJpeg.Height = y
						end if
								
						if y="" and x<>"" then
							aspJpeg.Width = x
							''aspJpeg.Height = aspJpeg.OriginalHeight * x / aspJpeg.OriginalWidth
						end if
						
						'Si me vienen las dos medidas hago para que me aparezca la imagen SIN RECORTAR, de manera proporcional
						if y<>"" and x<>"" then
							'limitamos la presentaci�n del alto de la imagen
							if cdbl(aspJpeg.OriginalHeight/aspJpeg.OriginalWidth)>cdbl(y/x) then
								aspJpeg.Height=y
							else
								aspJpeg.Width=x										
							end if	
						end if
					'Creamos la imagen thumbnail

					aspJpeg.Save  server.MapPath(RutaThumbnail)            
            
    end if
    
    'Devolvemos la ruta del thumbnail
    ComprobarThumbnail= RutaThumbnail
end function



''Obtener el tama�a de un fiechero
function tamannoArchivo(ruta) 
	tamanno=""
	Dim objFSO, objFile
	On Error Resume Next
	Set objFSO	= Server.CreateObject("Scripting.FileSystemObject")
	Set objFile	= objFSO.GetFile(ruta)
	If Err Then
		FileLen = Null
'		tamanno="archivo Invalido o no encontrado"
		tamanno=""
	Else
		FileLen = CLng( objFile.Size )
'		tamanno= " Archivo encontrado, tama�o "&FileLen&""
		tamanno= " [" & formatnumber(FileLen,0) & "  bytes]"
	End If
	Set objFile	= Nothing
	Set objFSO	= Nothing
	tamannoArchivo=tamanno
	On Error GoTo 0
End Function
''

function devuelveDimensionesImagen(img)
	width=0
	height=0
	respuesta=""
	dim myImg, fs
	On Error Resume Next	
	Set fs= CreateObject("Scripting.FileSystemObject")
	if not fs.fileExists(img) then
	exit function
	end if
	set myImg = loadpicture(img)
	width = round(myImg.width / 26.4583)
	height = round(myImg.height / 26.4583)
	if width > 1000 then
	
	dim originalWidth
	originalWidth = width
	width = 1000
	height = int(cint(width) * (height / originalWidth))
	
	end if
	
	if width>0 And height>0 then
		respuesta=" [" & width & " x " & height & " pixel]"
	end if
	set myImg = nothing
	On Error GoTo 0	
	
	devuelveDimensionesImagen=respuesta
End Function



%>