    <link rel="stylesheet" href="/dmcrm/css/estilosMenu.css" media="screen" />
    <link rel="stylesheet" href="/dmcrm/css/estilos.css" media="screen" />
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">    
    <link rel="stylesheet" href="/dmcrm/js/alerts/jquery.alerts.css" />
    <link rel="stylesheet" href="/dmcrm/css/print.css" media="print" />
    
    <script type="text/javascript" src="/dmcrm/js/jquery/jquery.js"></script>
    <script type="text/javascript" src="/dmcrm/js/menu/jquery.backgroundPosition.js"></script>
    <script type="text/javascript" src="/dmcrm/js/menu/menu.js"></script>
    <script type="text/javascript" src="/dmcrm/js/jquery.livequery.js"></script>
    
     <script type="text/javascript" src="/dmcrm/js/fancybox/fancybox.js"></script>
     <link rel="stylesheet" href="/dmcrm/js/fancybox/fancybox.css" type="text/css" media="screen" />
    
    
     <script type="text/javascript" src="/dmcrm/js/jquery.iframe-auto-height.plugin.js"></script>    

	    <script type="text/javascript" src="/dmcrm/js/jtables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/dmcrm/js/jtables/js/jquery.dataTables.utilidades.js"></script>
	    <script type="text/javascript" src="/dmcrm/js/jtables/js/jquery.jeditable.mini.js"></script>        

	    <link rel="stylesheet" href="/dmcrm/js/jtables/css/table.css" />        

    <script type="text/javascript" src="/dmcrm/APP/Comun2/FormChek2.js"></script>        


		<!--Editor-->        
   		<script type="text/javascript" src="/dmcrm/js/editor/nicEdit.js"></script>		
        
        
       <link rel="stylesheet" type="text/css" href="/dmcrm/js/jGrowl/jgrowl.css" />
	  <script type="text/javascript" src="/dmcrm/js/jGrowl/jgrowl.js"></script>           


		<!--Plugin de toolrip-->
        <script type="text/javascript" src="/DMCrm/JS/jquery.simpletip.js"></script> 
		 <script type="text/javascript">
			 $(document).ready(function(){
				 $('.msgTootlTip').simpletip({
					baseClass: 'estilomsgTootlTip',
					position: ["0", "14px"]
				 });
			});
         </script>     
         
         
		<!--Para el plugin de google maps -->            
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="/dmcrm/js/maps/jquery.gmap.js"></script>
		<script type="text/javascript" src="/dmcrm/js/maps/jquery.gmap.min.js"></script>        


		<!--Funciones genericas-->
       <script type="text/javascript">
	   
	 	function cambiarEstadoCheck(check)
		{
			if (check.checked)
			{
				check.value="1";	
			}
			else
			{
				check.value="0";	
			}
		}
		

	 	function lanzarConsultaInicial()
		{
			formClientes.submit();
		}
		
		function lanzarConsultaInicialAcciones()
		{
			formAcciones.submit();
		}
		
		function inicializarFiltros()
		{
			
			$('#divControlesTodos input').val('');
			$('#divControlesTodos select').val('0');			

			$.ajax({
				type: "POST",
				url: "/dmcrm/APP/Comercial/limpiarVariables.asp?tabla=<%=request("tabla")%>",
				success: function(datos) {
						//alert(datos);				
							}
			});
			
			$('.dataTables_filter input').val(''); 
/*			$('.dataTables_length').val(''); 			*/

		   diferenciarFiltro();
					   
		}
		
		
		function inicializarFiltrosDominios()
		{

			if (document.getElementById("txtFiltroDominiosCliente")!=null)
			{document.getElementById("txtFiltroDominiosCliente").value="";}
			
			if (document.getElementById("txtFiltroDominiosDominio")!=null)			
			{document.getElementById("txtFiltroDominiosDominio").value="";}
			
			if (document.getElementById("txtFiltroDominiosRegistro")!=null)			
			{document.getElementById("txtFiltroDominiosRegistro").value = "";}
			
			if (document.getElementById("txtFiltroDominiosDNS")!=null)			
			{document.getElementById("txtFiltroDominiosDNS").value = "";}
			
			if (document.getElementById("txtFiltroDominiosCorreo")!=null)			
			{document.getElementById("txtFiltroDominiosCorreo").value="";}
			
			if (document.getElementById("txtFiltroDominiosIP")!=null)			
			{document.getElementById("txtFiltroDominiosIP").value="";}
			
			$.ajax({
				type: "POST",
				url: "/dmcrm/APP/Comercial/limpiarVariables.asp?tabla=<%=request("tabla")%>",
				success: function(datos) {
						//alert(datos);				
							}
			});
			
			$('.dataTables_filter input').val(''); 
					   
					   
		}
		
		function pantallaCargada()
		{
			if (document.getElementById("divCargando")!=null)
			{
				document.getElementById("divCargando").style.display="none";
			}
		}
		
		function diferenciarFiltro()
		{
		

			  if (document.getElementById("divControlesTodos")!=null)
			  {
				  var filas=document.getElementById("divControlesTodos").getElementsByTagName("input");
					var i 
					for (i=0;i<filas.length;i++) { 
						var elemento=filas.item(i);					
						if (elemento.value!="" & elemento.value!="0" & elemento.value!="=" )
						{ elemento.parentNode.parentNode.style.fontWeight="bold";
					 	  elemento.parentNode.parentNode.style.color="#ff6600"; }
						  else
						  {elemento.parentNode.parentNode.style.fontWeight="inherit"; 
						  elemento.parentNode.parentNode.style.color="black"; }	
					} 
					
				  var filas=document.getElementById("divControlesTodos").getElementsByTagName("select");
					var i 
					for (i=0;i<filas.length;i++) { 
						var elemento=filas.item(i);					
						if (elemento.value!="" & elemento.value!="0" & elemento.value!="=" )
						{ elemento.parentNode.parentNode.style.fontWeight="bold";
					 	  elemento.parentNode.parentNode.style.color="#ff6600"; }
						  else
						  {elemento.parentNode.parentNode.style.fontWeight="inherit"; 
						  elemento.parentNode.parentNode.style.color="black"; }						  
					} 					
			  }
		  
		}
		
		
	
    </script>
          
        
        
        <!--Calendarios -->
        <!-- #include virtual="/dmcrm/js/JSCalendar/date.asp"-->
        <script type="text/javascript" src="/dmcrm/js/JSCalendar/date.js"></script>
		<script type="text/javascript" src="/dmcrm/js/JSCalendar/jquery.datePicker.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="/dmcrm/js/JSCalendar/datePicker.css">

			<script type="text/javascript" >
            $(function()
            {
				/*$('#txtFDesde').datePicker();
				$('#txtFHasta').datePicker();*/
				/*$('.cajaTextoFecha').datePicker();
				$('.cajaTextoModalFecha').datePicker();				*/
				$('.cajaTextoFecha').datePicker({startDate:'01/01/1900',clickInput:true});
				$('.cajaTextoFecha').dpSetOffset(20, 0);
				$('.cajaTextoModalFecha').datePicker({startDate:'01/01/1900',clickInput:true});				
				$('.cajaTextoModalFecha').dpSetOffset(20, 0);
				
            });
		</script>
        

        <!--Horas-->
        <link rel="stylesheet" href="/dmcrm/js/timePicker/include/jquery-ui-1.8.14.custom.css" type="text/css" />
		<link rel="stylesheet" href="/dmcrm/js/timePicker/jquery.ui.timepicker.css?v=0.2.5" type="text/css" />
<!--	    <script type="text/javascript" src="/dmcrm/js/timePicker/include/jquery-1.5.1.min.js"></script>-->
    	<script type="text/javascript" src="/dmcrm/js/timePicker/include/jquery.ui.core.min.js"></script>
	    <script type="text/javascript" src="/dmcrm/js/timePicker/include/jquery.ui.widget.min.js"></script>
    	<script type="text/javascript" src="/dmcrm/js/timePicker/include/jquery.ui.tabs.min.js"></script>
	    <script type="text/javascript" src="/dmcrm/js/timePicker/include/jquery.ui.position.min.js"></script>
	    <script type="text/javascript" src="/dmcrm/js/timePicker/jquery.ui.timepicker.js?v=0.2.5"></script>
	    <script type="text/javascript" src="/dmcrm/js/timePicker/include/plusone.js"></script>
        <!--Fin horas-->



		<!--Js autocomplementar filtros-->
        <script type="text/javascript" src="/dmcrm/js/autocompletar/jquery.autocomplete.js"></script>   
        
        <%''A�adimos una funion asp para pintar de forma generica el jquery de los filtros autocomplementar.
		'Sobrecarga: 
		'	1 - nomCajaTexto: Id del input que se pinta.
		'	2 - anchoPopUp: Ancho en pixels del popUp de resultados.
		'   3 - tablaConsulta: tabla sobre la que se va a lanzar la consulta.
		'	4 - campoConsulta: campo que se va a visualizar y por el que va a filtrar la consulta. (Para concatenar varios campos, usar '|'). Por ejemplo, CON_NOMBRE|CON_APELLIDO1
		sub pintarFiltrosAutoComplementar(tablaConsulta,campoConsulta,nomCajaTexto,anchoPopUp)
				'Si no recibimos ancho, ponemos 300px.
				if not isnumeric(anchoPopUp) then
					anchoPopUp=300
				end if
				'Si lo recibimos en formarto texto, lo ponemos en numerico
				anchoPopUp=cint(anchoPopUp)%>
				 <input  id="<%=nomCajaTexto%>" name="<%=nomCajaTexto%>" type="text"  maxlength="250"  value="<%=request.Form("" & nomCajaTexto)%>" class="cajaTexto relleno"/>        
				<script language="javascript" >
                      var options, a;
                    jQuery(function(){
                        var a = $('#<%=nomCajaTexto%>').autocomplete({
                            serviceUrl: '/dmcrm/app/Comun2/FiltrosPantallas/ajaxAutocompletarFiltro.asp?tabla=<%="" & tablaConsulta%>&campo=<%="" & campoConsulta%>&id=<%=encode(year(date) & "935" & month(date) & day(date))%><%=parametrosRuta%>',
                            minChars: 2,
                            delimiter: /(,|;)\s*/, // regex or character
                            maxHeight: 400,
                            width: <%=anchoPopUp%>,
                            zIndex: 9999,
                            deferRequestBy: 0, //miliseconds
                            ///params: { country: 'Yes' }, //aditional parameters
                            noCache: false, //default is false, set to true to disable caching
                            // callback function:
                            ///onSelect: function (value, data) { alert('You selected: ' + value + ', ' + data); c.value=data;},
                        });
                    });
                                        
                 </script>    			
		<%end sub%>
                                
                                
		<script type="text/javascript">
			$(document).ready(function() {            
            	$(".cEmergenteNotifCad").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'titleShow'				: false	,
						'onClosed'		        : function() {
							    recargarAlCerrar();
						}											
					}).trigger("click"); 
					return false; 	
				});	
				
				$(".ventanaModal").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'titleShow'		        : false,					
						'onClosed'		        : function() {
							
						}
					}).trigger("click"); 
					return false; 	

				});					
			});					   				
		</script>	                                    
        
               
