<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<%

	'Segun esta respuesta la pagina mostrara notificaciones o no.
	'Si respuesta = "", no se muestran notificaciones.
	respuesta=""
	
	'Si soloActH es 0, hacemos las validaciones de notificaciones pertinenetes.
	'Sino, solo actualizamos la hora.
	soloActH=request("soloActH")

	if trim(soloActH)="1" then
		session("horaNotificacion")=now()	
	else 'if trim(soloActH)="0" then
	


		codigoUsuario=session("codigoUsuario")
		if trim(codigoUsuario)<>"" then
		
		
				tienePermisosSocios=false
				tienePermisosLiquidaciones=false
				tienePermisosVales=false
				tienePermisosAcciones=false		
				tienePermisosCanjes=false
				tienePermisosSorteos=false
				tienePermisosConsultas=false				
				tienePermisosTransacciones=false												
				tienePermisosVales=false				
				''Obtenemos los socios sin trajeta
				sql=" Select MEN_CODIGO  "
				sql=sql & " From APP_MENUS M Inner Join APP_MENUS_ANALISTAS A on (M.MEN_CODIGO=A.MENA_SUMENU) "
				sql=sql & " Where MEN_CODIGO in (121,156,159,46,162,123,174,153) And MENA_SUANALISTA=" & codigoUsuario

				Set rsPermisos = connCRM.AbrirRecordSet(sql,3,1) 
				while not rsPermisos.eof 
						if trim(rsPermisos("MEN_CODIGO"))="121" then
							tienePermisosSocios=true				
						elseif trim(rsPermisos("MEN_CODIGO"))="156" then	
							tienePermisosLiquidaciones=true				
						elseif trim(rsPermisos("MEN_CODIGO"))="159" then	
							tienePermisosVales=true								
						elseif trim(rsPermisos("MEN_CODIGO"))="46" then	
							tienePermisosAcciones=true																
						elseif trim(rsPermisos("MEN_CODIGO"))="162" then	
							tienePermisosCanjes=true																
						elseif trim(rsPermisos("MEN_CODIGO"))="123" then	
							tienePermisosSorteos=true																														
						elseif trim(rsPermisos("MEN_CODIGO"))="174" then	
							tienePermisosConsultas=true	
						elseif trim(rsPermisos("MEN_CODIGO"))="153" then																																						
							tienePermisosTransacciones=true							
						end if
					rsPermisos.movenext
				wend
				rsPermisos.cerrar()
				set rsPermisos=nothing
				
				
				
				permiteVales=false
				saldoEmisionVales=0
				numDiasEmisionVales=0
				numDiasCaducidadVales=0
				sql=" Select top 1 CONF_PERMITE_VALES,CONF_SALDO_EMITE_VALES,CONF_NUMDIAS_EMITE_VALES,CONF_CADUCIDAD_VALES From CONFIGURACION "
				Set rsConf = connCRM.AbrirRecordSet(sql,3,1) 
				if not rsConf.eof then
					if not rsConf("CONF_PERMITE_VALES") then
						tienePermisosVales=false
					end if
					if not isnull(rsConf("CONF_SALDO_EMITE_VALES")) then
						saldoEmisionVales=rsConf("CONF_SALDO_EMITE_VALES")
					end if
					if not isnull(rsConf("CONF_NUMDIAS_EMITE_VALES")) then			
						numDiasEmisionVales=rsConf("CONF_NUMDIAS_EMITE_VALES")
					end if
					if not isnull(rsConf("CONF_CADUCIDAD_VALES")) then						
						numDiasCaducidadVales=rsConf("CONF_CADUCIDAD_VALES")
					end if
				end if
				rsConf.cerrar()
				set rsConf=nothing
		
		
		
				contNotificaciones=0	

				numSociosSinTarjetas=0				
				if tienePermisosSocios then
						''Obtenemos los socios sin trajeta
						filtroSocios=" ( (ltrim(rtrim(CON_NTARJETA))<>'' And CON_NTARJETA is not null ) Or (ltrim(rtrim(CON_CODIGOEXTERNO))<>'' And CON_CODIGOEXTERNO is not null And CON_CODIGOEXTERNO<>0) ) "
						numSociosTarjetas_1=0%>
                        <!-- #include virtual="/DMCrm/APP/Club/socios/controlesFiltrosSociosSinTarjeta.asp"-->
						<%numSociosSinTarjetas=numSociosTarjetas_1
						if numSociosTarjetas_1>0 then
							contNotificaciones=contNotificaciones + 1		
						end if
				end if 'end de if tienePermisosSocios then
				
				
				numLiquidacionesPendientes=0
				if tienePermisosLiquidaciones then %>
					<!-- #include virtual="/DMCrm/APP/Club/transacciones/LiquidacionesControlesFiltrosPendientes.asp"-->				  
					 			  
                    <%Set rsLiquiPend = connCRM.AbrirRecordSet(sql,3,1) 
						numLiquidacionesPendientes=rsLiquiPend.recordcount()
						if not ultimoMesLiquidado then
							numLiquidacionesPendientes=numLiquidacionesPendientes + 1
						end if
				  	  rsLiquiPend.cerrar()
				      set rsLiquiPend=nothing 
					
					if numLiquidacionesPendientes>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosLiquidaciones then	
				
				
				numValesPorEmitir=0
				if tienePermisosVales then %>
					<!-- #include virtual="/DMCrm/APP/Club/transacciones/ValesControlesFiltrosPorEmitir.asp"-->				  
					 			  
					<%if numValesPorEmitir>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosLiquidaciones then	
				
				numCanjesSinSolicitar=0
				if tienePermisosCanjes then 
					sql = "SELECT TI_ESTADOCANJE.id, TI_ESTADOCANJE.ESTADO ,count(TI_Canjes.id_canje) NUMPEDIDOS from TI_ESTADOCANJE LEFT JOIN TI_Canjes ON  TI_ESTADOCANJE.id = TI_Canjes.estadoCanje GROUP BY TI_ESTADOCANJE.id, TI_ESTADOCANJE.ESTADO ORDER BY TI_ESTADOCANJE.id"
                        Set rsEstadosPedidos = connCRM.abrirRecordSet(sql,0,1)
                        if not rsEstadosPedidos.eof then
							numCanjesSinSolicitar=rsEstadosPedidos("NUMPEDIDOS")
                        end if
                        rsEstadosPedidos.cerrar
                        Set rsEstadosPedidos = nothing					
					
					 			  
					if numCanjesSinSolicitar>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosLiquidaciones then					
								
		
		
				numSorteosSinRealizar=0
				if tienePermisosSorteos then 
				
					sql=" Select * From CLUB_PROMOCIONES "
					sql=sql & " Where  ( convert(smalldatetime,OFER_FECHAFINPUBLICACION," & obtenerFormatoFechaSql() & ") >= convert(smalldatetime,'" & FormatearFechaSQLSever(date()) & "'," & obtenerFormatoFechaSql() & ") ) "
					Set rsSorteos = connCRM.abrirRecordSet(sql,0,1)
					while not rsSorteos.eof 
						if trim(rsSorteos("OFER_SUTIPOOFERTA"))="3" then
							if rsSorteos("OFER_FECHAINICIO")>date() then
							elseif rsSorteos("OFER_FECHAINICIO")<=date() and  rsSorteos("OFER_FECHAFIN")>date() then
							else
								tieneGanadores=false
								sqlGanadores="Select count(OFGA_SUSOCIO) as contador From CLUB_PROMOCIONES_GANADORES Where OFGA_SUOFERTA=" & rsSorteos("OFER_CODIGO")
								Set rsGanadores = connCRM.abrirRecordSet(sqlGanadores,3,1)
								if not rsGanadores.eof then
									if 	rsGanadores("contador")>0 then
										tieneGanadores=true
									end if
								end if
								rsGanadores.cerrar()
								Set rsGanadores = nothing
								
								if not tieneGanadores then
									numSorteosSinRealizar=numSorteosSinRealizar + 1
								end if							
							end if
						end if
						rsSorteos.movenext
					wend
					rsSorteos.cerrar
                    Set rsSorteos = nothing						
					
					 			  
					if numSorteosSinRealizar>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosSorteos then			
				
				
				numConsultasPendientes=0
				if tienePermisosConsultas then 
				
					sql="SELECT count(TI_Incidencias.ticket) numIncidencias from TI_EstadosIncidencia LEFT JOIN TI_Incidencias ON  TI_EstadosIncidencia.id = TI_Incidencias.estado "
					sql=sql & " WHERE TI_Incidencias.estado=1 AND ( (TI_Incidencias.usuarioAdmin IS NULL OR TI_Incidencias.usuarioAdmin='') Or (TI_Incidencias.estado=1 AND TI_Incidencias.usuarioAdmin=" & session("codigoUsuario") & ")  ) "	
					Set rsConsultas = connCRM.abrirRecordSet(sql,0,1)
					if not rsConsultas.eof then
						if 	rsConsultas("numIncidencias")>0 then
							numConsultasPendientes=rsConsultas("numIncidencias")
						end if						
					end if
					rsConsultas.cerrar
                    Set rsConsultas = nothing						
					 			  
					if numConsultasPendientes>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosConsultas then	
				
				
				numSociosCaducidadTrans=0
				if tienePermisosTransacciones then 
					pintarTransPorCaducar=false %>
					<!-- #include virtual="/DMCrm/APP/Club/transacciones/transaccionesPorCaducar.asp"-->          											
					 			  
					<%if numSociosCaducidadTrans>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosTransacciones then	
				
				numSociosCaducidadVales=0
				if tienePermisosVales then 
					pintarValesPorCaducar=false%>
					<!-- #include virtual="/DMCrm/APP/Club/transacciones/valesPorCaducar.asp"-->  
					 			  
					<%if numSociosCaducidadVales>0 then
						contNotificaciones=contNotificaciones + 1					
					end if
				end if 'end de if tienePermisosVales then					


				contAccionesTipo7=0		
				if tienePermisosAcciones then
						sql = "Select count(acci_codigo) as contAccionesTipo7 "
						sql=sql & " from (((acciones_cliente LEFT join clientes on acciones_cliente.acci_sucliente=clientes.clie_codigo ) "
						sql=sql & " inner join tipos_accion on acciones_cliente.acci_sutipo=tipos_accion.tiac_codigo ) "
						sql=sql & " inner join campanas on acciones_cliente.acci_sucampana=campanas.camp_codigo ) "
						sql=sql & " where (acci_sucomercial=" & codigoUsuario & " Or acci_sucomercial2=" & codigoUsuario & " Or acci_sucomercial3=" & codigoUsuario & " Or acci_sucomercial4=" & codigoUsuario & " ) " 
						sql=sql & " and acci_terminada = 0 And ACCI_SUTIPO=7 And acci_fecha < getDate()  "
						Set rsAccionesTipo7 = connCRM.AbrirRecordSet(sql,3,1) 
				
						if not rsAccionesTipo7.eof  then
							contAccionesTipo7=rsAccionesTipo7("contAccionesTipo7")
							if contAccionesTipo7 >0 then
								contNotificaciones=contNotificaciones + 1										
							end if
						end if
						rsAccionesTipo7.cerrar()
						set rsAccionesTipo7=nothing				  				
				end if 'end de if tienePermisosAcciones then
				
				
					if numSociosSinTarjetas>0 Or numLiquidacionesPendientes>0 Or numValesPorEmitir>0 Or numSorteosSinRealizar>0 Or numConsultasPendientes>0 Or contAccionesTipo7>0 then
					
						
						nomMostrarNotifi=""
						
						''Comprobamos las notificaciones del usuario
						respuesta="<input type=""hidden"" id=""hidNumNotifiPend"" value=""" & contNotificaciones & """ />"'"2"
						respuesta=respuesta & "<div class=""titMensajesNoti"">" & objIdiomaGeneral.getIdioma("notificacionesCRM_1") & "</div>"
						if numSociosSinTarjetas > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifSocios"
							
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifSocios"" href=""/dmcrm/APP/CLUB/socios/Socios.asp?tabla=socios"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numSociosSinTarjetas & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_3") 
							respuesta=respuesta & "</a>"
						end if
						if numLiquidacionesPendientes > 0 then				
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifLiqui"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifLiqui"" href=""/dmcrm/APP/CLUB/transacciones/liquidaciones.asp?tabla=club_liquidaciones"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numLiquidacionesPendientes & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_4") 
							respuesta=respuesta & "</a>"
						end if				
						if numSociosCaducidadTrans > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifVales"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifVales"" href=""/dmcrm/APP/CLUB/transacciones/transacciones.asp"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numSociosCaducidadTrans & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_14") 
							respuesta=respuesta & "</a>"
						end if
						if numValesPorEmitir > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifVales"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifVales"" href=""/dmcrm/APP/CLUB/transacciones/vales.asp"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numValesPorEmitir & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_5") 
							respuesta=respuesta & "</a>"
						end if						
						if numSociosCaducidadVales > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifVales"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifVales"" href=""/dmcrm/APP/CLUB/transacciones/vales.asp"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numSociosCaducidadVales & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_15") 
							respuesta=respuesta & "</a>"
						end if												
						if numSorteosSinRealizar > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifSorteos"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifSorteos"" href=""/dmcrm/APP/CLUB/promociones/controlSorteos.asp"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numSorteosSinRealizar & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_12") 
							respuesta=respuesta & "</a>"
						end if						
						if numCanjesSinSolicitar > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifCanjes"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifCanjes"" href=""/DMCrm/APP/Club/tienda/pedidos.asp"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numCanjesSinSolicitar & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_11") 
							respuesta=respuesta & "</a>"
						end if	
						if numConsultasPendientes > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifConsultas"				
						
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifConsultas"" href=""/dmcrm/APP/Sistema/Incidencias/incidencias.asp"" >"
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_2") & " <span class=""numAccionesTareaNoti"">" & numConsultasPendientes & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_13") 
							respuesta=respuesta & "</a>"
						end if												
						if contAccionesTipo7 > 0 then
							if trim(nomMostrarNotifi)<>"" then
								nomMostrarNotifi=nomMostrarNotifi & ","
							end if
							nomMostrarNotifi = nomMostrarNotifi & "capaNotifAcciones"
											
							respuesta=respuesta & "<a class=""capaCadaNotificacion"" id=""capaNotifAcciones"" href=""/dmcrm/APP/comercial/resumenPendiente.asp?co=" & codigoUsuario &  """ >"																						
							respuesta=respuesta & objIdiomaGeneral.getIdioma("notificacionesCRM_6") & " <span class=""numAccionesTareaNoti"">" & contAccionesTipo7 & "</span> " & objIdiomaGeneral.getIdioma("notificacionesCRM_7") 
							respuesta=respuesta & "</a>"
						end if
						respuesta=respuesta & "<div class=""separaNotificacion""  >&nbsp;</div>"
						respuesta=respuesta & "<div class=""enlCerrarMensajes"">" & objIdiomaGeneral.getIdioma("notificacionesCRM_8") & "</div>"
						respuesta=respuesta & "</div>"
						
						arrNomMostrarNotifi=split(nomMostrarNotifi,",")
						Dim numAleatorio
						Randomize
						numAleatorio = Int(Rnd * ubound(arrNomMostrarNotifi)) 
		
						
						respuesta=respuesta & "<input type=""hidden"" id=""hidNumMostrar"" value=""" & arrNomMostrarNotifi(numAleatorio) & """ />"'"2"
						
					
					end if
	
		end if 'end de if trim(codigoUsuario)<>"" then		
	
	end if








response.Write(respuesta)

%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->
