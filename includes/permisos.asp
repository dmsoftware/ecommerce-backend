<%
'Si se ha perdido la session o no se ha identificado
if session("emailAdministracion") = "" then
    'Guardamos de donde venimos para pasarlo a INDEX.ASP y poder entrar directamente a donde se perdio la sesi�n
    'Alberto.- 8/6/2012.- Eliminamos la opci�n de volver a la p�gina donde nos echa ya que hemos detectado problemas cuando la p�gina donde perdemos la sesi�n es de modificaci�n de datos
    ' por ejemplo, modificaciones.asp de COMUN.
    ''session("PagRetorno")=Request.ServerVariables("PATH_INFO") & "?" & Request.ServerVariables("QUERY_STRING")
	response.Redirect("/dmcrm/index.asp")
end if

'No permitimos la entrada directa a la aplicaci�n ni desde otras aplicaciones.
if INSTR(UCASE(request.ServerVariables("HTTP_REFERER")),"/DMCRM/")=0 then
    response.Redirect("/dmcrm/index.asp")
end if

%>