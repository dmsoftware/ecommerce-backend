<script type="text/javascript" src='/dmcrm/js/multiSelectAutocomplete/jquery.js'></script>

<script type='text/javascript' src='/dmcrm/js/multiSelectAutocomplete/jquery.autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="/dmcrm/js/multiSelectAutocomplete/jquery.autocomplete.css" />



<style type='text/css'>

#ops {

	background-color: #fcfcfc;

	border: 1px solid #000;

	width: 300px;

	margin-top: 5px;

	height: 200px;

	max-height: 200px;

	overflow: auto;

}



#input {

	background: #f0f0f0 url(images/magnifier.png) no-repeat 1px 1px;

	border: 1px solid #000;

	height: 16px;

	padding-left: 18px;

	width: 282px !important;

}



#ops > .row {

	border-bottom: 1px dotted #ccc;

	padding: 2px;

}



#ops > .row:last-child {

	border-bottom: 0;

}



#ops > .row > .delete {

	cursor: pointer;

	margin-bottom: -2px;

}



#result {

	background-color: #fcfcfc;

	border: 1px solid #000;

	width: 300px;

	margin-top: 5px;

	height: 200px;

	max-height: 200px;

	overflow: auto;

}



.defText { width: 300px; }

.defTextActive { color: #a1a1a1; font-style: italic; }

</style>

<script type="text/javascript">

var maxH = 200;

$().ready(function()
{
	$.fn = $.extend($.fn,
	{
		autoSize : function(max)
		{
			var height = 0;
			var max    = parseInt(max) || 0;
			return this.css('height', 'auto').each(function()
			{
				height = Math.max(height, jQuery(this).outerHeight());
				if (max > 0 && height > max)
					height = max;
			}).css('height', height);
		},
		preSet : function(data)
		{
			data = $.extend({}, data);
			self = $(this);
			jQuery.each(data, function(k, v)
			{
				var html = $("<div class='row'><img src='images/delete.png' class='delete' border='0' alt='' /> <span>"+v+"</span></div>");
				html.data('acVal', k).find('.delete').click(function()
				{
					$('#values').remValue($(this).parent().data('acVal'));
					$(this).parent().remove();
					$('#ops').checkValues().autoSize(maxH);
					$('#input').focus();
				});
				$('#values').addValue(k);
				$(self).checkValues(true).append(html).autoSize(maxH);
			});
		},

		checkValues : function()
		{
			if (arguments[0] === true)
				$(this).find('.default').remove();
			else
				if (!$(this).find('.row').length)
					$(this).append("<div align='center' class='default'><small><i>Search and select your items</i></small></div>");
			return $(this);
		},

		addValue : function(val, sep)
		{
			var arr = $(this).val().split(sep || ',');
			if (jQuery.inArray(val, arr) < 0)
				arr[arr.length] = val;
			$(this).val(arr.join(sep || ','));
			return true;
		},

		hasValue : function(val, sep)
		{
			var arr = $(this).val().split(sep || ',');
			return (jQuery.inArray(val, arr) > -1) ? true : false;
		},

		remValue : function(val, sep)
		{
			var arr = $(this).val().split(sep || ',');
			if (jQuery.inArray(val, arr) > -1)
				arr = jQuery.grep(arr, function(v)
				{
					return v != val;
				});

			$(this).val(arr.join(sep || ','));
			return true;
		}
	});

	$('#ops').checkValues().autoSize();
	$('#values').val('').data('chosen', {});
	$('#input').autocomplete('search.php',
	{
		minChars      : 1,
		matchContains : true,
		formatItem    : function(row)
		{
			if ($('#values').hasValue(row[1]))
				return false;
			return row[0];
		}

	}).result(function(evt, data, fmt)
	{
		var html = $("<div class='row'><img src='images/delete.png' class='delete' border='0' alt='' /> <span>"+fmt+"</span></div>");
		html.data('acVal', data[1]).find('.delete').click(function()
		{
			$('#values').remValue($(this).parent().data('acVal'));
			$(this).parent().remove();
			$('#ops').checkValues().autoSize(maxH);
			$('#input').focus();
		});

		$('#values').addValue(data[1]);
		$('#ops').checkValues(true).append(html).autoSize(maxH);
		$('#input').val('');

		return;
	});



	$('#submit').click(function()
	{
		var self = $(this);
		self.attr('disabled', true);
		$('#result').empty();
		$('#loading').show();
		$.post('submit.php',
		{
			values : $('#values').val()

		}, function(d)
		{
			self.attr('disabled', false);
			$('#loading').hide();
			$('#result').html(d);
		});

		return false;
	});

	$('#clear').click(function(e)
	{
		e.preventDefault();
		$('#result').empty();
		$(this).blur();
	});

	$('#ops').preSet(
	{
		key : "Value",
		test : "Test",
		shawn : "Dean"
	});

});

</script>



	<input type='hidden' id='values' value='' />

	<input type='text' class='defText' defText='Enter search terms...' id='input' autocomplete='off' />

	<div id='ops'></div>



	<div style='margin-top:10px'>

		<input type='button' id='submit' value='Submit' />

		<div><br />

			<strong>Result <small>[<a href='#' id='clear'>Clear</a>]</small></strong>

			<div id='result'></div>

		</div>

	</div>



	</div>

	<div class='clear'></div>
