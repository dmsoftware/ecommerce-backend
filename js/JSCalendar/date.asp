<script type="text/javascript">

Date.dayNames = ['<%=objIdiomaGeneral.getIdioma("DiasLargo_1")%>', '<%=objIdiomaGeneral.getIdioma("DiasLargo_2")%>', '<%=objIdiomaGeneral.getIdioma("DiasLargo_3")%>', '<%=objIdiomaGeneral.getIdioma("DiasLargo_4")%>', '<%=objIdiomaGeneral.getIdioma("DiasLargo_5")%>', '<%=objIdiomaGeneral.getIdioma("DiasLargo_6")%>', '<%=objIdiomaGeneral.getIdioma("DiasLargo_7")%>'];

Date.abbrDayNames = ['<%=objIdiomaGeneral.getIdioma("DiasCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("DiasCorto_2")%>', '<%=objIdiomaGeneral.getIdioma("DiasCorto_3")%>', '<%=objIdiomaGeneral.getIdioma("DiasCorto_4")%>', '<%=objIdiomaGeneral.getIdioma("DiasCorto_5")%>', '<%=objIdiomaGeneral.getIdioma("DiasCorto_6")%>', '<%=objIdiomaGeneral.getIdioma("DiasCorto_7")%>'];

Date.monthNames = ['<%=objIdiomaGeneral.getIdioma("MesesLargo_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_2")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_3")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_4")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_5")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_6")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_7")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_8")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_9")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_10")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_11")%>', '<%=objIdiomaGeneral.getIdioma("MesesLargo_12")%>'];

Date.abbrMonthNames = ['<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>', '<%=objIdiomaGeneral.getIdioma("MesesCorto_1")%>'];

Date.firstDayOfWeek = 1;

Date.format = '<%=objIdiomaGeneral.getIdioma("FormatoFecha_1")%>';

Date.fullYearStart = '20';
</script>
