
/** Animated TreeMenu script by Garrett Smith.
  * Size: 10k
  * URL: http://dhtmlkitchen.com/
  * email: admin@dhtmlkitchen.com
  * 
  * Usage:see url below (subject to change)
  * http://dhtmlkitchen.com/dhtml/ui/menutree/
  */

  TreeGlobals = {
   // don't change these
      activeMenu : null,
      menuToOpen : null,
               
       // You may add to the browser properties, 
       // but do not change existing properties.
       
      browser:new function(){
          this.OPERA   = (navigator.userAgent.indexOf("Opera")> 0);
          this.NS4     = (typeof document.layers != "undefined");
          this.ICAB    = (navigator.userAgent.toLowerCase().indexOf("icab")> 0);
          this.IE5     = (navigator.userAgent.indexOf("MSIE 5")> 0 && !this.OPERA);
          this.MAC     = (navigator.platform.indexOf("PPC")> 0);
          this.MAC_IE5 = this.IE5 && (navigator.platform.indexOf("PPC")> 0);    

     // this.OPERA = true;  
     // this.NS4 = false;     
     // this.ICAB = true;    
     // this.IE5 = true;     
     // this.MAC = true;     
     // this.MAC_IE5 = true; 


      },
      
      inited:false,
      one:1
  };
  
  if(typeof window.TreeParams == "undefined")
      TreeParams = {
  
       // NOTE: You change these in * your own * TreeParams object.

  /** TreeParams:
   *  OPEN_MULTIPLE_MENUS     -- Boolean
   *                         if true, more than one menu can 
   *                         be open at a time. Otherwise, 
   *                         opening a new menu closes any 
   *                         open menu.
   *
   *
   *  OPEN_MULTIPLE_SUBMENUS  -- Boolean
   *                         Same as OPEN_MULTIPLE_MENUS but                          
   *                         applies to the submenus of each menu.
   *                       
   *  TIME_DELAY              -- int
   *                         How slowly a menuNode collapses
   *                         in milliseconds. (0 to 100).
   *
   *  OPEN_WHILE_CLOSING      -- Boolean
   *                         If either OPEN_MULTIPLE_MENUS or
   *                         OPEN_MULTIPLE_SUBMENUS are true,
   *                         OPEN_WHILE_CLOSING will simultaneously
   *                         open a new menu while closing the 
   *                         currently open menu.
   */
      
      OPEN_MULTIPLE_MENUS   : false,
      OPEN_MULTIPLE_SUBMENUS: false,
      
      TIME_DELAY            : 0,
      OPEN_WHILE_CLOSING    : true,
      
      OPEN_MENU_ICON        : "open-menu.gif",
      CLOSED_MENU_ICON      : "closed-menu.gif"
  
  };
  
  // Unless you really know what you are doing,
  // don't change anything below this line!
  //_________________________________________________________________________
  ///////////////////////////////////////////////////////////////////////////



function NSInf62(){  
// Prueba para NS < 6.2
var its_ie = false
var its_ns = false
var major_version = parseInt(navigator.appVersion)
var full_version = parseFloat(navigator.appVersion)
var ns6_version = parseFloat(navigator.vendorSub)

// Caracteres en min�scula
var user_agent = navigator.userAgent.toLowerCase()

// DETECCI�N DEL NAVEGADOR NETSCAPE (si no es netscape, supongo que es explorer)
if (user_agent.indexOf("mozilla") != -1) {its_ns = true}

if (its_ns) {
	//alert ('Es Netscape');
	if (major_version == 5) {
		if (ns6_version >= 6.2) {
			//alert ('Es >= 6.2');
			return true;
		}
	}
return false;
}
//Fin Prueba para NS < 6.2
}

  
function toggleMenu(label){

// alert (NSInf62());
// if (NSInf62() == true) {alert ("voy bien");}


if(TreeGlobals.browser.OPERA||TreeGlobals.browser.NS4)return;

//alert (TreeGlobals.browser.OPERA);
//alert (TreeGlobals.browser.NS4);
//alert (TreeGlobals.browser.ICAB);
//alert (TreeGlobals.browser.IE5);
//alert (TreeGlobals.browser.MAC);
//alert (TreeGlobals.browser.MAC_IE5); 
//alert (navigator.userAgent.indexOf("MSIE 5"));



var button1=TreeUtils.findAncestorWithClass(label,"button1");
if(typeof button1.label=="undefined")TreeFunctions.initbutton1(button1, label);
if(!button1.menu.isSubmenu&&TreeGlobals.menuToOpen||button1.menu.isSubmenu&&button1.menu.parentMenu.menuToOpen!=null)return;
if(button1.isDepressed){
if(TreeGlobals.activeMenu==button1.menu||button1.menu.isSubmenu&&button1.menu.parentMenu.activeMenu==button1.menu||TreeParams.OPEN_MULTIPLE_MENUS||TreeParams.OPEN_MULTIPLE_SUBMENUS){
TreeFunctions.closeMenu(button1.menu,"TreeFunctions.setDefaultLabel('"+button1.id+"')");
if(!button1.menu.isSubmenu)
TreeGlobals.activeMenu=null;
else
button1.menu.parentMenu.activeMenu=null;}}
else{
if(!button1.menu.isSubmenu)
TreeGlobals.menuToOpen=button1.menu;
else
button1.menu.parentMenu.menuToOpen=button1.menu;
if(button1.label.icon!=null)
button1.label.icon.src=TreeParams.OPEN_MENU_ICON;
if(TreeParams.OPEN_MULTIPLE_MENUS&&!button1.menu.isSubmenu||TreeGlobals.activeMenu==null||(button1.menu.isSubmenu&&
(TreeParams.OPEN_MULTIPLE_SUBMENUS||button1.menu.parentMenu.activeMenu==null)))
TreeFunctions.openMenu(button1.menu);
else if(TreeGlobals.activeMenu!=null&&!button1.menu.isSubmenu)
TreeFunctions.closeMenu(TreeGlobals.activeMenu,
"TreeFunctions.openMenu(TreeGlobals.menuToOpen);TreeFunctions.setDefaultLabel('"
+TreeGlobals.activeMenu.ownerbutton1.id+"');");
else if(button1.menu.parentMenu.activeMenu!=null&&button1.menu.isSubmenu)
TreeFunctions.closeMenu(button1.menu.parentMenu.activeMenu,
"TreeFunctions.openMenu(document.getElementById('"
+button1.menu.id+"'));TreeFunctions.setDefaultLabel('"
+button1.menu.parentMenu.activeMenu.ownerbutton1.id+"');");
if(!button1.menu.isSubmenu)
TreeGlobals.activeMenu=button1.menu;
else
button1.menu.parentMenu.activeMenu=button1.menu;}}
function activateMenu(sbutton1Id){
if(!window.toggleMenu) 
return;
var button1=document.getElementById(sbutton1Id);
if(!button1)return;
var label=button1.getElementsByTagName("span")[0];
toggleMenu(label);
}

function button1Over(label){
window.status=label.parentNode.id;
if(!label.parentNode.isDepressed)
label.className+=" labelHover";
else
label.className+=" labelDown labelHover";}
function button1Off(label){
window.status=window.defaultStatus;
TreeUtils.removeClass(label,"labelHover");}
TreeFunctions={
initbutton1 : function(button1, label){
button1.label=label;
button1.catagory=button1.id;
button1.menuName=String(button1.catagory)+"Menu"
button1.menu=document.getElementById(button1.menuName);
button1.menu.ownerbutton1=button1;
var icons=button1.label.getElementsByTagName("img");
label.icon=(icons.length>0) ? 
icons[0] : null;
if(label.tagName=="IMG"){
label.isIcon=true;
label.icon=label;
}
button1.menu.items=TreeUtils.getChildNodesWithClass(button1.menu, 
"div", "menuNode");
button1.menu.allItems=TreeUtils.getElementsWithClass(button1.menu, 
"div", "menuNode");
button1.menu.isSubmenu=TreeUtils.findAncestorWithClass(button1,"menu")!=null;if(button1.menu.isSubmenu){
button1.menu.parentMenu=TreeUtils.findAncestorWithClass(button1,"menu");
if(typeof button1.menu.parentMenu.activeMenu == "undefined"){
button1.menu.parentMenu.activeMenu=null;
button1.menu.parentMenu.menuToOpen=null;
}
}
button1.menu.activeMenu=null;
button1.isDepressed=false;
button1.menu.performAction=TreeFunctions.performAction;
},initMenu:function(){
if(document.getElementById&&!TreeGlobals.browser.OPERA&&!TreeGlobals.inited){
document.write("<style type='text/css'>");
document.write("/* <![CDATA[ */");
document.write(".menu,.menuNode{display:none;}");
document.write("/* ]]> */");
document.write("<"+"/style>");
TreeGlobals.inited=true;}},
openMenu:function(menu){
var delay=TreeParams.TIME_DELAY;
if(menu==null||
!menu.isSubmenu&&TreeGlobals.menuToOpen&&TreeGlobals.menuToOpen!=menu||menu.isSubmenu&&menu.parentMenu.menuToOpen!=menu||TreeGlobals.menuToOpen==menu&&menu.isOpening)
return;
menu.itemsToOpen=new Array();
if(menu.itemsToClose&&menu.itemsToClose.length>menu.items.length){
menu.itemsToOpen=menu.itemsToClose.reverse();}
else
for(var i=0,counter=0;i<menu.items.length;i++){
var item=menu.items[i];
if(item.style.display!="none")
item.style.display="none";
menu.itemsToOpen[counter++]=item;
if(item.menu&&item.isDepressed){
TreeFunctions.addActionItems(menu,menu,counter,item.menu);}}
if(!menu.ownerbutton1.label.isIcon)
menu.ownerbutton1.label.className+=" labelDown";
menu.style.display="block";
menu.performAction(0,menu.itemsToOpen.length,"block")
menu.ownerbutton1.isDepressed=true;},
addActionItems:function(menuInAction,menu,counter,submenu){
for(var i=0;i<submenu.items.length;i++){
menuInAction.itemsToOpen[counter++]=submenu.items[i];
if(submenu.items[i].menu)
TreeFunctions.addActionItems(menu,submenu,counter,
submenu.items[i].menu);}},
closeMenu:function(menu,sEvalCode){
menu.itemsToClose=new Array();
for(var i=menu.allItems.length,counter=0;i>1;i--){
if(menu.allItems[i-1].style.display=="block")
menu.itemsToClose[counter++]=menu.allItems[i-1];}
menu.itemsToClose[menu.itemsToClose.length]=menu;
menu.performAction(0,menu.itemsToClose.length,"none")
if(TreeParams.OPEN_WHILE_CLOSING)
setTimeout(sEvalCode,10 );
else menu.sEvalCode=(sEvalCode!=null)?sEvalCode:";";
menu.ownerbutton1.isDepressed=false;},
performAction:function(iCurr,iTotal,sDisplay){
if(sDisplay=="block")
this.itemsToOpen[iCurr].style.display="block";

else
this.itemsToClose[iCurr].style.display="none";clearInterval(this.performActionTimer);if(++iCurr<iTotal)
this.performActionTimer=
setInterval("document.getElementById('"+this.id 
+"').performAction("+iCurr+","+iTotal+",'"+sDisplay+"');",
TreeParams.TIME_DELAY); else TreeFunctions.performActionEnd(this,sDisplay); },
performActionEnd:function(menu,sDisplay){ menu.style.display=sDisplay;
TreeFunctions.endAnim(menu,sDisplay=='block')
},endAnim:function(menu,open){ if(open)
if(menu.isSubmenu)
menu.parentMenu.menuToOpen=null;
else TreeGlobals.menuToOpen=null;else if(!TreeGlobals.OPEN_WHILE_CLOSING)
eval(menu.sEvalCode );},
setDefaultLabel:function(button1Id){
var button1=document.getElementById(button1Id);
if(button1.label.isIcon)
return void(button1.label.icon.src=TreeParams.CLOSED_MENU_ICON);
TreeUtils.removeClass(button1.label,"labelHover");
TreeUtils.removeClass(button1.label,"labelDown");
if(button1.label.icon!=null)
button1.label.icon.src=TreeParams.CLOSED_MENU_ICON;
TreeGlobals.one=-TreeGlobals.one;
/*self.resizeBy(0,TreeGlobals.one);*/}};
TreeFunctions.initMenu();
TreeUtils={
getChildNodesWithClass:function(parent,tagName,className){
var collection;
var returnedCollection=[];
var classArray=className.split(" ");
var collection=parent.childNodes;
for(var i=0,counter=0;i<collection.length;i++){
if(!collection[i].className||collection[i].tagName.toUpperCase()!=tagName.toUpperCase())
continue;
var elmClassArray=collection[i].className.split(" ");
jloop:
for(var j=0;j<elmClassArray.length;j++)
for(var k=0;k<classArray.length;k++)
if(elmClassArray[j]==classArray[k]){
if(!collection[i].id)
collection[i].id=parent.id+"_item"+counter;
returnedCollection[counter++]=collection[i];
break jloop;}}
return returnedCollection;},
getElementsWithClass:function(parent,tagName,className){
var collection;
var returnedCollection=[];
var classArray=className.split(" ");
if(parent.all&&tagName=="*")collection=parent.all;
else collection=parent.getElementsByTagName(tagName);
for(var i=0,counter=0;i<collection.length;i++){
if(!collection[i].className)continue;
var elmClassArray=collection[i].className.split(" ");
jloop:for(var j=0;j<elmClassArray.length;j++)
for(var k=0;k<classArray.length;k++)
if(elmClassArray[j]==classArray[k]){
returnedCollection[counter++]=collection[i];
break jloop;}}
return returnedCollection;},
findAncestorWithClass:function(el,klass){
var parent=el.parentNode;
while(parent){
if(parent.className){
var classList=parent.className.split(" ");
for(var i=0;i<classList.length;i++)
if(classList[i]==klass)return parent;}
parent=parent.parentNode;}
return null;},
removeClass:function(el,name){
if(el.className==null)return;
var classList=el.className.split(" ");
var newClasslist="";
for(var i=0;i<classList.length;i++)
if(classList[i]!=name)
newClasslist+=classList[i]+ " ";
el.className=newClasslist.trim();}};
Array.prototype.contains=function(obj){
for(var i=0;i<this.length;i++)
if(this[i]==obj)return true;
return false;};
String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"");};
