<style>
.ui-draggable-dragging { background:url(/dmcrm/js/Gantt/lineas.png) !important; /*border-color:#ff6600 !important;*/ }
.ui-draggable-dragging .ganttview-block-text { /*color:#333 !important;*/ }
.ui-draggable-dragging .nombreNuevo { /*color:#ff6600 !important;*/ }

/****/

.ganttview-weekend { background:#dedede !important; }
.ganttview-grid-row-cell.hoy { /*background:#fffb83;*/ }
.ganttview-grid-row-cell { position:relative; overflow:hidden; }
.iconoHoy { position:absolute; top:0; left:0; width:3px; height:25px; background:#245d9d; }
</style>

<script type="text/javascript">
	var posiciones = new Array();
	
	$(document).ready(function() {
		var meses = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		var primerMes = $('#ganttChart_proyectos .ganttview-hzheader-month:first').text().substr(0, 3);
		var primerMesNum;
		switch(primerMes){
		   case 'Jan':
			primerMesNum = 1;
			break;
		   case 'Feb':
			primerMesNum = 2;
			break;
		   case 'Mar':
			primerMesNum = 3;
			break;
		   case 'Apr':
			primerMesNum = 4;
			break;
		   case 'May':
			primerMesNum = 5;
			break;
		   case 'Jun':
			primerMesNum = 6;
			break;
		   case 'Jul':
			primerMesNum = 7;
			break;
		   case 'Aug':
			primerMesNum = 8;
			break;
		   case 'Sep':
			primerMesNum = 9;
			break;
		   case 'Oct':
			primerMesNum = 10;
			break;
		   case 'Nov':
			primerMesNum = 11;
			break;
		   case 'Dec':
			primerMesNum = 12;
			break;
		};
		
		var diasMostrados = new Array();
		var contaDias = 0;
		var diaAnterior;
		var mesActual;
		var mesDia;
		
			$('#ganttChart_proyectos .ganttview-hzheader-day').each(function(){
				if(mesActual == undefined){
					mesActual = primerMesNum;
				};
				if (parseFloat($(this).text()) < parseFloat(diaAnterior)){
					if (mesActual == 12){
						mesActual = 1;
					} else {
						mesActual ++;
					};
				};
				
				objetoDia = new Array(2);
				objetoDia[0] = mesActual;
				objetoDia[1] = $(this).text();
				diasMostrados.push(objetoDia);
				
				mesDia = diasMostrados[parseFloat(contaDias)];
				
				diaAnterior = $(this).text();
				var i = 0;
				var k = contaDias;
				$('#ganttChart_proyectos .ganttview-grid-row').each(function(){
					var blokeCompleto = (i * $('.ganttview-grid-row-cell', this).length) + k;
					trabajador = $('#ganttChart_proyectos .ganttview-block:nth('+ i +') .nombre').text();
					$('#ganttChart_proyectos .ganttview-grid-row-cell:nth('+ blokeCompleto +')').addClass(trabajador + '-'+ mesDia[0] + '-' + mesDia[1]).addClass('General-'+ mesDia[0] + '-' + mesDia[1]);
					i++;
				});
				contaDias++;
			});
			
			var fechaHoy = new Date();
			var diaHoy = fechaHoy.getDate();
			var mesHoy = fechaHoy.getMonth()+1; //Enero es el 0
			if(diaHoy < 10){
				diaHoy = '0' + diaHoy
			};
			if(mesHoy < 10){
				mesHoy = '0' + mesHoy
			}
			var fechaHoy = mesHoy + '-' + diaHoy;
			$('.General-' + fechaHoy).addClass('hoy').prepend('<div class="iconoHoy"></div>');
	
	
			var arrayDiasFiesta = "<%= strDiasEspeciales2 %>".split(',');
			for (i = 0; i < arrayDiasFiesta.length; i++){
				var selector = '.' + arrayDiasFiesta[i].substr(0, arrayDiasFiesta[i].length - 7).replace(' ', '');
				var color = '#' + arrayDiasFiesta[i].substr(arrayDiasFiesta[i].length - 6, arrayDiasFiesta[i].length);
				$(selector).css('background', color).addClass('fiesta');
			};
	});
	
	$(window).ready(function(){						  
			var j = 0;
			$('#ganttChart_proyectos .ganttview-block.ui-draggable').each(function(){
				//var actual = $(this).offset().left - 541;
				var actual = $(this).css('marginLeft');
				$(this).attr('id', 'bloke' + j);
				posiciones.push(actual);
				j++;
			});
	});
</script>