<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!-- #include virtual="/dmcrm/conf/inc_func_asp.asp"-->

<style type="text/css">
	body { font-family:Arial, Helvetica, sans-serif; font-size:12px;}
</style>
<%
sql = "SELECT ANA_PASSWORD,ANA_EMAIL,ANA_FECHACAMBIOPASSWORD FROM ANALISTAS WHERE ANA_CODIGO = " & request("cod")
Set rsClaves = connCRM.AbrirRecordset(sql,3,3)

'1/ Generamos la nueva contrase�a
'==========================================================
Function PwdAleatorio ( Longitud, Repetir )
    '----------------------------------------------------------
    ' por Carlos de la Orden Dijs, Abril 2001 - 100% gratis! ;-)
    '----------------------------------------------------------
    ' Devuelve una cadena con una secuencia de caracteres
    ' aleatoria, de longitud especificada.
    ' Si Repetir = True la secuencia puede contener caracteres
    ' repetidos. Si Repetir = False, todos los caracteres son
    ' �nicos. 
    ' Para a�adir m�s caracteres posibles, a�adirlos al vector
    ' vCaracteres simplemente separando como comas, como los
    ' que est�n ya escritos, y la funci�n los escoger�.
    '----------------------------------------------------------
    Dim vPass(), I, J ' nuestro vector y dos contadores
    Dim vNumeros()	  ' vector para guardar lo que llevamos
    Dim n, bRep		  
    Dim vCaracteres	  ' vector donde est�n los posibles caract.

    vCaracteres = Array("a","b", "c", "d", "e", "f", "g", "h", _
    "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", _
    "u", "v", "w", "x", "y", "z", "A","B", "C", "D", "E", "F", _ 
    "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", _
    "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", _
    "5", "6", "7", "8", "9", "0")

    'Establezco la longitud del vector
    Redim vPass(Longitud-1)
    'Y del vector auxiliar que guarda los caracteres ya escogidos
    Redim vNumeros(Longitud-1)
    I = 0
    'Inicializo los n�s aleatorios
    Randomize
    'Hasta que encuentre todos los caracteres
    do until I = Longitud
	    'Hallo un n�mero aleatorio entre 0 y el m�ximo indice
	    ' del vector de caracteres.
	    n = int(rnd*Ubound(vCaracteres))
	    'Si no puedo repetir...
	    if not Repetir then
		    bRep = False
		    'Busco el numero entre los ya elegidos
		    for J = 0 to UBound(vNumeros)
			    if n = vNumeros(J) then
			    'Si esta, indico que ya estaba
				    bRep = True
			    end if
		    next
		    'Si ya estaba, tengo que repetir este caracter
		    'as� que resto 1 a I y volvemos sobre la misma
		    'posici�n.
		    if bRep then 
			    I = I - 1
		    else
			    vNumeros(I) = n
			    vPass(I) = vCaracteres(n)	
		    end if
	    else
	    'Me da igual que est� o no repetido
		    vNumeros(I) = n
		    vPass(I) = vCaracteres(n)
	    end if
    'Siguiente car�cter!
    I = I + 1
    loop

    'Devuelvo la cadena. Join une los elementos de un vector
    'utilizando como separador el segundo par�metro: en este
    'caso, nada -> "".
    PwdAleatorio = Join(vPass, "")

End Function 'PwdAleatorio
'==========================================================
ContrasenaNueva =  PwdAleatorio(4, False)

'2/ Updateamos la contase�a
rsClaves("ana_password")=encode(ContrasenaNueva)

'3/ Vaciamos la fecha de cambio de contase�a para que se le obligue al usuario a modificarla la primera vez
rsClaves("ana_FECHACAMBIOPASSWORD")=null
rsClaves.update

%>

<table border="0" cellpadding="0" cellspacing="0" width="96%" height="96%">
    <tr>
		<td align="center" valign="middle">
        	<p>
            	<%=objIdioma.getIdioma("ResetearContrasenaUsuario_Texto1")%><br />
                <strong style="color:#f60;"><%= rsClaves("ana_email")%></strong>.<br /><br />
            	<%=objIdioma.getIdioma("ResetearContrasenaUsuario_Texto2")%> <br />
				<%=objIdioma.getIdioma("ResetearContrasenaUsuario_Texto3")%><br />
                <strong><br />
                <%=objIdioma.getIdioma("ResetearContrasenaUsuario_Texto4")%></strong> <%=ContrasenaNueva%><br />
            </p>
		</td>
	</tr>
</table>

<%
rsClaves.cerrar
set rsClaves=nothing
%>

<!--#include virtual="/dmcrm/conf/cerrarconbd.asp"-->