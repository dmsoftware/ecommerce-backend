<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%'Llamadas ajax para modificar los permisos %>
<script language = "javascript">

    function CambiaPermiso(UrlCambioPermiso, parametros, idImagen) {
        $.ajax({
            type: "GET",
            //url: "ajaxCambiaPermiso.asp",
            //data: "codAnalista=29&codMenu=5&tipo=anadirPermiso&id=MjAxMTkzNTgzMQ",
            url: UrlCambioPermiso,
            data: parametros,
            success: function (data) {
                var NomImagen = document.images[idImagen].src;
                if (NomImagen.indexOf("/dmcrm/APP/imagenes/red.png") > 0) {
                    document.images[idImagen].src = '/dmcrm/APP/imagenes/green.png';
                }
                else {
                    document.images[idImagen].src = '/dmcrm/APP/imagenes/red.png';
                }
            },
            error: function (data) {
                alert("error: " + data);
            }
        });
    }

    function recargaEmpresas() {
        tmpE = document.forms.fPaginacion2.comboEmpresas;
        document.forms.fPaginacion2.comboEmpresas.value = tmpE.options[tmpE.selectedIndex].value;
        document.location = 'PermisosAplicacion.asp?emp=' + tmpE.options[tmpE.selectedIndex].value + '&p=1';
        //}
    }
</script>

<h1><%=objIdioma.getIdioma("PermisosAplicacion_Titulo")%></h1>

<form name="fPaginacion2" method="POST" ID="Form1">
<div class="FiltradoCab2" style="clear:both;">
<span class="cab1"></span><br />
	<div>
		<%
        'Valor por defecto del combo empresa
        Empresa=request("emp")
    
        sql="SELECT * from DEPARTAMENTOS"      
        sql=sql & " order by emp_descripcion "       
        Set rstAux= connCRM.AbrirRecordset(sql,0,1)  
        %>
            <span class="cab2"><%=objIdiomaGeneral.getIdioma("Departamento")%></span>
            <select name="comboEmpresas" onchange="javascript:recargaEmpresas();" class="combos" ID="Select1">
                <%while not rstAux.eof
                    if Empresa="" then 
                        if session("DepartamentoUsuario")=rstAux("EMP_CODIGO") then
                            Empresa=rstAux("emp_codigo")
                        end if
                    end if
                    rstAux.movenext
                wend   
                rstAux.movefirst
                while not rstAux.eof%>
                    <option value="<%=rstAux("emp_codigo")%>" <%if cint(Empresa)=rstAux("emp_codigo") then response.write ("selected")%>><%=rstAux("emp_descripcion")%></option>
                    <%rstAux.movenext()
                wend
                %>
            </select>
    
       <%rstAux.cerrar
       set rstAux=nothing %>
	</div>
</div>
</form>


<%
'Hallamos el plano de servicios
sql=" Select * from app_menus inner join app_menuprincipal on app_menus.men_sumenuprincipal=app_menuprincipal.menp_codigo "
sql=sql & " where menp_codigo not in(14,15,16) " 'Quitamos los men�s de la ZONA DE COMERCIOS del club de fidelizaci�n
sql=sql & " order by menp_orden,men_orden" 
Set rstMenus= connCRM.AbrirRecordset(sql,0,1)

'Hallamos los analistas
sql=" Select * from analistas "
sql=sql & " where ana_activo=1 and ana_sudepartamento=" & empresa
sql=sql & " order by ana_sudepartamento,ana_directorcuenta,ana_nombre,ana_codigo" 
Set rstPersonas= connCRM.AbrirRecordset(sql,3,3)

%>

<table width="100%" border=0 cellpadding=2 cellspacing=1>
    <%while not rstMenus.eof       

        if strMenuAnterior<>rstMenus("Menp_Menu_ES") then
            strMenuAnterior=rstMenus("Menp_Menu_ES")%>
            <tr class="nombre">
                <td colspan=100>
                    <%if rstMenus("menp_menu_" & Idioma)<>"" then
                        response.write rstMenus("menp_menu_" & Idioma)
                    else
                        'Ponemos la de castellano
                        response.write rstMenus("menp_menu_es")
                    end if%>
                </td>
            </tr>
            <tr class="celdaGris noBorder">
                <td height="27">&nbsp;</td>
                <%rstPersonas.movefirst
                while not rstPersonas.eof%>
                    <td><%if rstPersonas("ana_directorcuenta") then response.write "<b>"%><%=rstPersonas("ana_nombre")%></td>
                    <%rstPersonas.movenext
                wend%>
            </tr>
         <%end if%>

       <tr class="celdaBlanca" >
           <td>
                <%if rstMenus("men_menu_" & Idioma)<>"" then
                    response.write rstMenus("men_menu_" & Idioma)
                else
                    'Ponemos la de castellano
                    response.write rstMenus("men_menu_es")
                end if%>   
                <%'Hallamos si hay alguna pesta�a asociada a este menu
                sql=" Select MPE_TABLA,MPE_PESTANA_ES,MPE_PESTANA_EU from MAESTRA_PESTANAS "
                sql=sql & " where MPE_MENUENLACEPERMISOS="  & rstMenus("MEN_CODIGO")
                Set rstPestanas= connCRM.AbrirRecordset(sql,3,1) 
                while not rstPestanas.eof 
                    if rstPestanas("MPE_PESTANA_" & Idioma)<>"" then
                        response.write "<FONT COLOR=666666><br> _ Tabla: " & rstPestanas("MPE_TABLA") & " � Pesta�a: " & rstPestanas("MPE_PESTANA_" & Idioma) & "</FONT><br>"
                    else
                        'Ponemos la de castellano
                        response.write "<br> _ Tabla: " & rstPestanas("MPE_TABLA")  & " � Pesta�a: " & rstPestanas("MPE_PESTANA_ES") & "</FONT><br>"
                    end if
                    rstPestanas.movenext
                wend
                rstPestanas.cerrar%>                        
           </td>
           <%
            'Hallamos los permisos ordenados primero por personas (order by ana_sudepartamento,ana_directorcuenta) de este menu
            'En la tabla APP_MENUS_ANALISTAS s�lo hay registro si el analista tiene permisos para dicho men�
            sql=" Select ana_codigo,ana_nombre from analistas inner join app_menus_analistas on analistas.ana_codigo=app_menus_analistas.mena_suanalista"
            sql=sql & " WHERE mena_sumenu=" & rstMenus("men_codigo")
            sql=sql & " and ana_activo=1 and ana_sudepartamento=" & empresa
            sql=sql & " ORDER BY ana_sudepartamento,ana_directorcuenta,ana_nombre,ana_codigo"
            Set rstPermisos= connCRM.AbrirRecordset(sql,0,1)
           
           rstPersonas.movefirst
           while not rstPersonas.eof%>
                <td align=center>
                <%'Comprobamos que el d�a exista en el cursor, si no existe lo pasamos
                if not rstPermisos.eof then
                    if rstPermisos("ana_codigo")=rstPersonas("ana_codigo") then%>
                        <img name="imagen_<%=rstPersonas("ana_codigo")%>_<%=rstMenus("men_codigo")%>" width=15 src="/dmcrm/APP/imagenes/green.png" onClick="CambiaPermiso('ajaxCambiaPermiso.asp','codAnalista=<%=rstPersonas("ana_codigo")%>&codMenu=<%=rstMenus("men_codigo") %>&tipo=quitarPermiso&id=<%=encode(year(date) & "935" & month(date) & day(date))%>','imagen_<%=rstPersonas("ana_codigo")%>_<%=rstMenus("men_codigo")%>');">
                        <%rstPermisos.movenext
                    else%>
                        <img name="imagen_<%=rstPersonas("ana_codigo")%>_<%=rstMenus("men_codigo")%>" width=15 src="/dmcrm/APP/imagenes/red.png" onClick="CambiaPermiso('ajaxCambiaPermiso.asp','codAnalista=<%=rstPersonas("ana_codigo")%>&codMenu=<%=rstMenus("men_codigo") %>&tipo=anadirPermiso&id=<%=encode(year(date) & "935" & month(date) & day(date))%>','imagen_<%=rstPersonas("ana_codigo")%>_<%=rstMenus("men_codigo")%>');">
                    <%end if
                else 'Pongo al usuario que no tiene permiso%>
                    <img name="imagen_<%=rstPersonas("ana_codigo")%>_<%=rstMenus("men_codigo")%>" width=15 src="/dmcrm/APP/imagenes/red.png" onClick="CambiaPermiso('ajaxCambiaPermiso.asp','codAnalista=<%=rstPersonas("ana_codigo")%>&codMenu=<%=rstMenus("men_codigo") %>&tipo=anadirPermiso&id=<%=encode(year(date) & "935" & month(date) & day(date))%>','imagen_<%=rstPersonas("ana_codigo")%>_<%=rstMenus("men_codigo")%>');">
                <%end if%>  
                </td>            
                <%rstPersonas.movenext
            wend
            rstPermisos.cerrar%>
       </tr>
   
       <%rstMenus.movenext
    wend
    rstMenus.cerrar
    rstPersonas.cerrar%>
</table>


<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->