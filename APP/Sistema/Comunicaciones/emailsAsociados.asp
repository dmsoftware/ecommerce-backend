<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <link rel="shortcut icon" href="/dmcrm/favicon.ico">   
    <title><% if trim(nomSubMenu)<>"" then %><%=nomSubMenu%><%else%>DM GESTION<%end if%></title>
    <!-- #include virtual="/dmcrm/includes/jquery.asp"--> 
        
</head>
<body style="margin:0; padding:0;">
<h1><%=objIdiomaGeneral.getIdioma("EmailsSeguimiento_Titulo1")%></h1>
<% tabla = "CON_EMAILS" %>                                  
<%
registrosPorPagina = 10
especifico = 1
pagina = "1"
if(request("pagina") <> "") then
	pagina = request("pagina")
end if

if(request.QueryString("eliminar") <> "") then

	sql = "DELETE FROM COM_CLIENTES_EMAILS WHERE COMCE_CODIGO = " & request.QueryString("eliminar")
	connCRM.execute sql,numregistrosInsert
	
end if

%>
<div id="gestionclientes" class="contenidoSeccion" style="z-index:1;">
	<script type="text/javascript">
        $(document).ready(function() {            
            $(".cEmergente").live('click', function(){
                $(this).fancybox({
                    'imageScale'			: true,
                    'zoomOpacity'			: true,
                    'overlayShow'			: true,
                    'overlayOpacity'		: 0.7,
                    'overlayColor'			: '#333',
                    'centerOnScroll'		: true,
                    'zoomSpeedIn'			: 600,
                    'zoomSpeedOut'			: 500,
                    'transitionIn'			: 'elastic',
                    'transitionOut'			: 'elastic',
                    'type'					: 'iframe',
                    'frameWidth'			: '70%',
                    'frameHeight'			: '70%',
                    'titleShow'				: false	,
                    'onClosed'		        : function() {
                            recargarAlCerrar();
                    }											
                }).trigger("click"); 
                return false; 	
            });	
        });
            
        function recargarAlCerrar() {
            if (document.forms[0] != null)
            { document.forms[0].submit(); }
            else { window.location.reload(); }
            //		document.location.reload();
        }
		
    </script>
    <div style="clear:both; height:5px;  width:100%;"></div>
    <div class="bloqueSeleccion blokeResponsiveIzq" style="width: 100%;">
    	<div id="contenedorSeleccion">
        	<div id="tablaContenido_wrapper" class="dataTables_wrapper">
            <table id="tablaContenido" width="99%" cellspacing="1" cellpadding="0" border="0" style="z-index:1; padding:0px; margin:0px; " rel="CON_CONSULTAS">
            	<thead>
					<tr id="cabeceraTabla">
                    	<th style="text-align:left;"><%=objIdiomaGeneral.getIdioma("EmailsSeguimiento_Texto1")%></th>
                        <th><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto3")%></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                
                    par = 1
                    'sql = "SELECT * FROM CON_EMAILS WHERE CONE_ASIGNADO = 0 ORDER BY CONE_CODIGO DESC"
                    sql = "SELECT * FROM (" &_
                            " SELECT ROW_NUMBER()Over(Order by COMCE_CODIGO Desc) As RowNum, COMCE_CODIGO, COMCE_IDCLIENTE," &_
                            " 	COMCE_EMAIL" &_
                            " FROM COM_CLIENTES_EMAILS " &_
                            " WHERE COMCE_IDCLIENTE = " & request("cod") &_
							consultaWhere &_
                        " )" &_
                        " AS ResultadoPaginado" &_
                        " WHERE RowNum BETWEEN (" & pagina & " - 1) * " & registrosPorPagina & " + 1 AND " & pagina & " * " & registrosPorPagina & ""
                    'response.Write(sql)
                    'response.End()
                    Set rsEmails = connCRM.abrirRecordSet(sql, 3, 1)
                    while not rsEmails.eof
                        response.Write("<tr")
						if (par mod 2 = 0) then
							response.Write(" style=""background-color:#F1F3F3;""")
						end if
                        response.Write(">")
                        response.Write("<td>" & rsEmails("COMCE_EMAIL") & "</td>")
						response.Write("<td style=""text-align:center;"">")
						response.Write("<a href=""?cod=" & request("cod") & "&eliminar=" & rsEmails("COMCE_CODIGO") & """ title=""""><img src=""/dmcrm/APP/comun2/imagenes/delete.gif"" style=""width:2.2em;"" alt="""" /></a>")
						response.Write("</td>")
						response.Write("</tr>")
                        par = par + 1
                        rsEmails.movenext()
                    wend
                    %>
            	</tbody>
            </table>
            </div>
        </div>
    </div>
    <div id="tablaContenido_paginate" class="dataTables_paginate paging_full_numbers">
    <span>
    <%
	sql = "SELECT COUNT(*) as num FROM COM_CLIENTES_EMAILS WHERE COMCE_IDCLIENTE = " & request("cod") & consultaWhere
                        
	Set rsEmails = connCRM.abrirRecordSet(sql, 3, 1)
	num = 0
	if not rsEmails.EOF then
		numEmails = rsEmails("num")
	end if
	num = ceil(numEmails / registrosPorPagina)
	for i = 1 to num
		if(i & "" = pagina) then
			response.Write("<span class=""paginate_active"">" & i & "</span>")
		else
			response.Write("<span class=""paginate_button""><a href=""?pagina=" & i & "&txtAsignado=" & request("txtAsignado") & "&txtEstado=" & request("txtEstado") & "&txtTipo=" & request("txtTipo") & "&cod=" & request("cod") & """ title="""" style=""color:#666666; text-decoration:none;"">" & i & "</a></span>")
		end if
	next
	%>
    </span>
    </div>
</div>
<%
function ceil(x)
	dim temp
	 
	temp = Round(x)
	 
	if temp < x then
	temp = temp + 1
	end if
	 
	ceil = temp
end function
%>
</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->






