<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
</head>
<body>
<!--Inicio contenido-->
<form method="post" id="busquedaClientes">
    <%=objIdiomaGeneral.getIdioma("PromocionesSocios_Texto40")%>:<br />
    <% pintarFiltrosAutoComplementar "CONTACTOS","CON_NOMBRE","nombreCliente",300 %>
    <input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonBuscar_Texto1")%>" id="botonCliente" name="botonCliente" />
</form>
<table id="tablaClientes">
    <thead>
        <tr>
            <th><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto23")%></th>
            <th><%=objIdiomaGeneral.getIdioma("PromocionesSocios_Texto40")%></th>
            <th><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto10")%></th>
        </tr>
    </thead>
    <tbody id="registrosCliente">
    </tbody>
</table>
<script type="text/javascript">
	
	function asignarCliente(id, nombre){
		window.opener.document.getElementById("cliente").value = nombre;
		window.opener.document.getElementById("idcliente").value = id;
		window.close();
	}
	
	$(".autocomplete").live("click", function(){ cargarClientes(); });

	$(document).keypress(function(e) {
		if(e.which == 13) {
			cargarClientes();
		}
	});
	$(document).ready(function () {
		$('#botonCliente').click(function() {
			cargarClientes();
		});
	});
	function cargarClientes(){
		$.ajax({
			type: "POST",
			url: "/DMCrm/APP/Sistema/Comunicaciones/listadoClientes.asp",
			data: "nombre=" + $('#nombreCliente').val(),
			async: false,
			success: function(datos) {
				$('#registrosCliente').children( 'tr' ).remove();
				dividirDatos = datos.split("###");
				for(var i = 0; i < dividirDatos.length - 1; i++){
					dividirDatos2 = dividirDatos[i].split("&&&");
					
					tr = document.createElement("tr");
					td1 = document.createElement("td");
					td2 = document.createElement("td");
					td3 = document.createElement("td");
					a = document.createElement("a");
					a.href = "javascript: asignarCliente('" + dividirDatos2[0] + "', '" + dividirDatos2[1] + "')";
					a.innerHTML = dividirDatos2[1];
					td1.innerHTML = dividirDatos2[0];
					td3.innerHTML = dividirDatos2[2];
					td2.appendChild(a);
					tr.appendChild(td1);
					tr.appendChild(td2);
					tr.appendChild(td3);
					document.getElementById("registrosCliente").appendChild(tr);
				}
			},
			error: function(data) {
				alert("error: " + data);
			}
		});
	}
</script> 
<!--Fin contenido-->
</body>
</html>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->