﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.Headers;
using Limilabs.Mail.MIME;


public partial class index : System.Web.UI.Page
{

	private const string connectionString = "";
	private const string spam = "Please purchase Mail.dll license at http://www.limilabs.com/mail";

    protected void Page_Load(object sender, EventArgs e)
    {
        string server = "";
        string user = "";
        string password = "";
		long lastId = 0;
		using (OleDbConnection connection = new OleDbConnection(connectionString))
		{
			try
			{

                OleDbCommand command = new OleDbCommand("SELECT [CONF_CONSULTAHOST], [CONF_CONSULTAENVIO], [CONF_CONSULTAUSER], [CONF_CONSULTAPASSWORD] FROM CONFIGURACION WHERE CONF_CODIGO = 1", connection);
				//resultado.Text = command.CommandText;
				connection.Open();
				OleDbDataReader reader = command.ExecuteReader();
				while (reader.Read())
				{
                    server = reader[0].ToString();
                    user = reader[2].ToString();
                    password = reader[3].ToString();
				}
				reader.Close();

			}
			catch (Exception ex)
			{
                resultado.Text = ex.Message;
			}

		}
		using (OleDbConnection connection = new OleDbConnection(connectionString))
		{
			try
			{

                OleDbCommand command = new OleDbCommand("SELECT COM_UID FROM COM_UID WHERE COM_CODIGO = 1", connection);
				//resultado.Text = command.CommandText;
				connection.Open();
				OleDbDataReader reader = command.ExecuteReader();
				while (reader.Read())
				{
                    lastId = long.Parse(reader[0].ToString());
				}
				reader.Close();

			}
			catch (Exception ex)
			{
                resultado.Text = ex.Message;
			}

		}
        String numeroConsulta;
        String idEmail;
        String asignado;
        bool estaEnTabla;
        DateTime time = DateTime.Now;              // Use current time
        string format = "yyyy-MM-dd HH:mm:ss";    // Use this format
        String hoy = time.ToString(format);

        //Correos recibidos
        using (Imap imap = new Imap())
        {
            
            imap.Connect(server);                              // Use overloads or ConnectSSL if you need to specify different port or SSL.
            imap.Login(user, password);                       // You can also use: LoginPLAIN, LoginCRAM, LoginDIGEST, LoginOAUTH methods,
            // or use UseBestLogin method if you want Mail.dll to choose for you.

            imap.SelectInbox();                                 // You can select other folders, e.g. Sent folder: imap.Select("Sent");

            List<long> uids = imap.Search(Flag.Unseen);     // Find all unseen messages.

            foreach (long uid in uids)
            {
                numeroConsulta = "0";
                idEmail = "0";
                asignado = "0";
                estaEnTabla = false;

                IMail email = new MailBuilder().CreateFromEml(  // Download and parse each message.
                    imap.GetMessageByUID(uid));

                //ProcessMessage(email);
                //From
                String usuario_completo = email.From.ToString().Replace("'", "`");
                char[] delimiterChars = { '`' };
                String[] usuario_dividido = usuario_completo.Split(delimiterChars);
                String usuario = "";
                if (usuario_dividido.Length > 1)
                {
                    usuario = usuario_dividido[3];
                }
                String texto = email.Text.Replace("'", "`");
                String codUsuario = "0";
                //buscar cliente en tabla COM_CLIENTES_EMAILS
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    try
                    {

                        OleDbCommand command = new OleDbCommand("SELECT COMCE_IDCLIENTE FROM COM_CLIENTES_EMAILS WHERE COMCE_EMAIL LIKE '" + usuario + "'", connection);
                        //resultado.Text = command3.CommandText;
                        connection.Open();
                        OleDbDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            codUsuario = reader[0].ToString();
                            asignado = "1";
                            estaEnTabla = true;
                        }
                        reader.Close();

                    }
                    catch (Exception ex)
                    {
                        resultado.Text = ex.Message;
                    }

                }
                if (asignado.Equals("0"))
                {
                    //buscar cliente en tabla CLIENTES
                    using (OleDbConnection connection = new OleDbConnection(connectionString))
                    {
                        try
                        {

                            OleDbCommand command = new OleDbCommand("SELECT CON_CODIGO FROM CONTACTOS WHERE CON_EMAIL LIKE '" + usuario + "'", connection);
                            //resultado.Text = command3.CommandText;
                            connection.Open();
                            OleDbDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                codUsuario = reader[0].ToString();
                                asignado = "1";
                            }
                            reader.Close();

                        }
                        catch (Exception ex)
                        {
                            resultado.Text = ex.Message;
                        }

                    }
                }
                //To
                usuario_completo = email.To.ToString().Replace("'", "`");
                usuario_dividido = usuario_completo.Split(delimiterChars);
                String usuarioTo = "";
                int i;
                if (usuario_dividido.Length > 1)
                {
                    for (i = 3; i < usuario_dividido.Length; i = i + 4)
                    {
                        if (i > 3)
                        {
                            usuarioTo += "; ";
                        }
                        usuarioTo += usuario_dividido[i];
                    }
                }
                //Cc
                usuario_completo = email.Cc.ToString().Replace("'", "`");
                usuario_dividido = usuario_completo.Split(delimiterChars);
                String usuarioCc = "";
                String htmlUsuarioCc = "";
                if (usuario_dividido.Length > 1)
                {
                    for (i = 3; i < usuario_dividido.Length; i = i + 4)
                    {
                        if (i > 3)
                        {
                            usuarioCc += "; ";
                        }
                        usuarioCc += usuario_dividido[i];
                    }
                }
                //Bcc
                usuario_completo = email.Bcc.ToString().Replace("'", "`");
                usuario_dividido = usuario_completo.Split(delimiterChars);
                String usuarioBcc = "";
                String htmlUsuarioBcc = "";
                if (usuario_dividido.Length > 1)
                {
                    for (i = 3; i < usuario_dividido.Length; i = i + 4)
                    {
                        if (i > 3)
                        {
                            usuarioBcc += "; ";
                        }
                        usuarioBcc += usuario_dividido[i];
                    }
                }
                //Si no está en la tabla COM_CLIENTES_EMAILS, lo insertamos
                if (!estaEnTabla && !codUsuario.Equals("0"))
                {
                    using (OleDbConnection connection = new OleDbConnection(connectionString))
                    {
                        OleDbCommand command = new OleDbCommand("INSERT INTO COM_CLIENTES_EMAILS ([COMCE_IDCLIENTE], [COMCE_EMAIL]) VALUES ('" + codUsuario + "', '" + usuario + "')", connection);
                        try
                        {

                            connection.Open();
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            resultado.Text = ex.Message;
                        }
                    }
                }
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    OleDbCommand command = new OleDbCommand("INSERT INTO COM_EMAILS ([COME_ASUNTO], [COME_FROM], [COME_TO], [COME_CC], [COME_CCO], [COME_FECHA], [COME_MENSAJE], [COME_ASIGNADO], [COME_ENVIADO]) VALUES ('" + (email.Subject.Replace("'", "`")).Replace(spam, "") + "', '" + usuario + "', '" + usuarioTo + "', '" + usuarioCc + "', '" + usuarioBcc + "', '" + hoy + "', '" + texto + "', " + asignado + ", 0)", connection);
                    try
                    {

                        connection.Open();
                        command.ExecuteNonQuery();
                        
                    }
                    catch (Exception ex)
                    {
                        resultado.Text = ex.Message;
                    }
                }
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    try
                    {

                        OleDbCommand command = new OleDbCommand("SELECT MAX(COME_CODIGO) FROM COM_EMAILS", connection);
                        //resultado.Text = command3.CommandText;
                        connection.Open();
                        OleDbDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            idEmail = reader[0].ToString();
                        }
                        reader.Close();

                    }
                    catch (Exception ex)
                    {
                        resultado.Text = ex.Message;
                    }

                }
                //Si el cliente existe, lo importamos al seguimiento
                if (!codUsuario.Equals("0"))
                {
                    using (OleDbConnection connection = new OleDbConnection(connectionString))
                    {
                        OleDbCommand command = new OleDbCommand("INSERT INTO COM_SEGUIMIENTO ([COMS_IDCLIENTE], [COMS_IDEMAILS], [COMS_ASUNTO], [COMS_MENSAJE], [COMS_FECHA], [COMS_ESTADO], [COMS_IDTIPO]) VALUES ('" + codUsuario + "', '" + idEmail + "', '" + (email.Subject.Replace("'", "`")).Replace(spam, "") + "', '" + texto + "', '" + hoy + "', 0, 1)", connection);
                        try
                        {

                            connection.Open();
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            resultado.Text = ex.Message;
                        }
                    }
                }

                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    OleDbCommand command;
				    foreach (MimeData attachment in email.Attachments)
				    {
					    try
					    {
                            command = new OleDbCommand("INSERT INTO COM_ADJUNTOS ([COMA_IDEMAILS], [COMA_RUTA]) VALUES ('" + idEmail + "', '" + idEmail + attachment.FileName + "')", connection);

                            /*command.Parameters.AddWithValue("@param1", idEmail);
						    command.Parameters.AddWithValue("@param2", attachment.FileName);*/

                            //resultado.Text = command.CommandText;
						    connection.Open();
                            command.ExecuteNonQuery();
                            connection.Close();
                            String ocPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"attach/" + idEmail + attachment.SafeFileName);
                            attachment.Save(ocPath);
					    }
					    catch (Exception ex)
					    {
                            resultado.Text = ex.Message;
					    }
				    }

                }
            }
            imap.Close();
        }

        //Correos enviados
        using (Imap imap = new Imap())
        {

            imap.Connect(server);                           // Use overloads or ConnectSSL if you need to specify different port or SSL.
            imap.Login(user, password);                     // You can also use: LoginPLAIN, LoginCRAM, LoginDIGEST, LoginOAUTH methods,
            // or use UseBestLogin method if you want Mail.dll to choose for you.

            CommonFolders folders = new CommonFolders(imap.GetFolders());
            String carpetas = "";
            foreach (FolderInfo folder in imap.GetFolders())
            {
                carpetas += " [" + folder.Name + "] ";
            }
            resultado.Text = carpetas;
            imap.Select("Elementos enviados");
            //imap.Select("Sent Messages");                    // You can select other folders, e.g. Sent folder: imap.Select("Sent");

            //List<long> uids = imap.Search(Flag.Unseen);
	        List<long> uids = imap.Search().Where(Expression.UID(Range.From(lastId + 1)));
            //List<long> uids = imap.GetAll();     // Find all unseen messages.
            resultado.Text = uids.Count.ToString();
			long firstId = lastId;

            foreach (long uid in uids)
            {
				lastId = uid;
				
				if(firstId < lastId){
				
					numeroConsulta = "0";
					idEmail = "0";
					asignado = "0";
					estaEnTabla = false;
					IMail email = null;
	
					try
					{
						email = new MailBuilder().CreateFromEml(  // Download and parse each message.
							imap.GetMessageByUID(uid));
					}
					catch (Exception ex)
					{
						resultado.Text = ex.Message;
					}
	
					if(email != null)
					{
						//From
						String usuario_completo = email.From.ToString().Replace("'", "`");
						char[] delimiterChars = { '`' };
						String[] usuario_dividido = usuario_completo.Split(delimiterChars);
						String usuario = "";
						if (usuario_dividido.Length > 1)
						{
							usuario = usuario_dividido[3];
						}
						//Texto
						String texto = email.Text.Replace("'", "`");
						//To
						usuario_completo = email.To.ToString().Replace("'", "`");
						usuario_dividido = usuario_completo.Split(delimiterChars);
						String usuarioTo = "";
						bool masDeUno = false;
						int i;
						if (usuario_dividido.Length > 1)
						{
							for (i = 3; i < usuario_dividido.Length; i = i + 4)
							{
								if (i > 3)
								{
									masDeUno = true;
									usuarioTo += "; ";
								}
								usuarioTo += usuario_dividido[i];
							}
						}
						//Cc
						usuario_completo = email.Cc.ToString().Replace("'", "`");
						usuario_dividido = usuario_completo.Split(delimiterChars);
						String usuarioCc = "";
						if (usuario_dividido.Length > 1)
						{
							for (i = 3; i < usuario_dividido.Length; i = i + 4)
							{
								if (i > 3)
								{
									usuarioCc += "; ";
								}
								usuarioCc += usuario_dividido[i];
							}
						}
						//Bcc
						usuario_completo = email.Bcc.ToString().Replace("'", "`");
						usuario_dividido = usuario_completo.Split(delimiterChars);
						String usuarioBcc = "";
						if (usuario_dividido.Length > 1)
						{
							for (i = 3; i < usuario_dividido.Length; i = i + 4)
							{
								if (i > 3)
								{
									usuarioBcc += "; ";
								}
								usuarioBcc += usuario_dividido[i];
							}
						}
						String codUsuario = "0";
						//buscar cliente en tabla COM_CLIENTES_EMAILS
						if (!masDeUno)
						{
							using (OleDbConnection connection = new OleDbConnection(connectionString))
							{
								try
								{
	
									OleDbCommand command = new OleDbCommand("SELECT COMCE_IDCLIENTE FROM COM_CLIENTES_EMAILS WHERE COMCE_EMAIL LIKE '" + usuarioTo + "'", connection);
									//resultado.Text = command3.CommandText;
									connection.Open();
									OleDbDataReader reader = command.ExecuteReader();
									while (reader.Read())
									{
										codUsuario = reader[0].ToString();
										asignado = "1";
										estaEnTabla = true;
									}
									reader.Close();
	
								}
								catch (Exception ex)
								{
									resultado.Text = ex.Message;
								}
	
							}
							if (asignado.Equals("0"))
							{
								//buscar cliente en tabla CLIENTES
								using (OleDbConnection connection = new OleDbConnection(connectionString))
								{
									try
									{
	
										OleDbCommand command = new OleDbCommand("SELECT CON_CODIGO FROM CONTACTOS WHERE CON_EMAIL LIKE '" + usuarioTo + "'", connection);
										//resultado.Text = command3.CommandText;
										connection.Open();
										OleDbDataReader reader = command.ExecuteReader();
										while (reader.Read())
										{
											codUsuario = reader[0].ToString();
											asignado = "1";
										}
										reader.Close();
	
									}
									catch (Exception ex)
									{
										resultado.Text = ex.Message;
									}
	
								}
							}
							//Si no está en la tabla COM_CLIENTES_EMAILS, lo insertamos
							if (!estaEnTabla && asignado.Equals("1") && !codUsuario.Equals("0"))
							{
								using (OleDbConnection connection = new OleDbConnection(connectionString))
								{
									OleDbCommand command = new OleDbCommand("INSERT INTO COM_CLIENTES_EMAILS ([COMCE_IDCLIENTE], [COMCE_EMAIL]) VALUES ('" + codUsuario + "', '" + usuarioTo + "')", connection);
									try
									{
	
										connection.Open();
										command.ExecuteNonQuery();
	
									}
									catch (Exception ex)
									{
										resultado.Text = ex.Message;
									}
								}
							}
						}
						using (OleDbConnection connection = new OleDbConnection(connectionString))
						{
							OleDbCommand command = new OleDbCommand("INSERT INTO COM_EMAILS ([COME_ASUNTO], [COME_FROM], [COME_TO], [COME_CC], [COME_CCO], [COME_FECHA], [COME_MENSAJE], [COME_ASIGNADO], [COME_ENVIADO]) VALUES ('" + (email.Subject.Replace("'", "`")).Replace(spam, "") + "', '" + usuario + "', '" + usuarioTo + "', '" + usuarioCc + "', '" + usuarioBcc + "', '" + hoy + "', '" + texto + "', " + asignado + ", 1)", connection);
							try
							{
	
								connection.Open();
								command.ExecuteNonQuery();
	
							}
							catch (Exception ex)
							{
								resultado.Text = ex.Message;
							}
						}
						using (OleDbConnection connection = new OleDbConnection(connectionString))
						{
							try
							{
	
								OleDbCommand command = new OleDbCommand("SELECT MAX(COME_CODIGO) FROM COM_EMAILS", connection);
								//resultado.Text = command3.CommandText;
								connection.Open();
								OleDbDataReader reader = command.ExecuteReader();
								while (reader.Read())
								{
									idEmail = reader[0].ToString();
								}
								reader.Close();
	
							}
							catch (Exception ex)
							{
								resultado.Text = ex.Message;
							}
	
						}
						//Si el cliente existe, lo importamos al seguimiento
						if (!codUsuario.Equals("0"))
						{
							using (OleDbConnection connection = new OleDbConnection(connectionString))
							{
								OleDbCommand command = new OleDbCommand("INSERT INTO COM_SEGUIMIENTO ([COMS_IDCLIENTE], [COMS_IDEMAILS], [COMS_ASUNTO], [COMS_MENSAJE], [COMS_FECHA], [COMS_ENVIADO], [COMS_ESTADO], [COMS_IDTIPO]) VALUES ('" + codUsuario + "', '" + idEmail + "', '" + (email.Subject.Replace("'", "`")).Replace(spam, "") + "', '" + texto + "', '" + hoy + "', 1, 0, 1)", connection);
								try
								{
	
									connection.Open();
									command.ExecuteNonQuery();
	
								}
								catch (Exception ex)
								{
									resultado.Text = ex.Message;
								}
							}
						}
	
						using (OleDbConnection connection = new OleDbConnection(connectionString))
						{
							OleDbCommand command;
							foreach (MimeData attachment in email.Attachments)
							{
								try
								{
									command = new OleDbCommand("INSERT INTO COM_ADJUNTOS ([COMA_IDEMAILS], [COMA_RUTA]) VALUES ('" + idEmail + "', '" + idEmail + attachment.FileName + "')", connection);
	
									/*command.Parameters.AddWithValue("@param1", idEmail);
									command.Parameters.AddWithValue("@param2", attachment.FileName);*/
	
									//resultado.Text = command.CommandText;
									connection.Open();
									command.ExecuteNonQuery();
									connection.Close();
									String ocPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"attach/" + idEmail + attachment.SafeFileName);
									attachment.Save(ocPath);
								}
								catch (Exception ex)
								{
									resultado.Text = ex.Message;
								}
							}
	
						}
	
					}
					
				}
				
            }

            /*imap.Select("Sent");                    // You can select other folders, e.g. Sent folder: imap.Select("Sent");
            uids = imap.Search(Flag.Unseen); 

            foreach (long uid in uids)
            {
                numeroConsulta = "0";
                idEmail = "0";
                asignado = "0";
                estaEnTabla = false;
                IMail email = null;

                try
                {
                    email = new MailBuilder().CreateFromEml(  // Download and parse each message.
                        imap.GetMessageByUID(uid));
                }
                catch (Exception ex)
                {
                    resultado.Text = ex.Message;
                }

                if (email != null)
                {
                    //From
                    String usuario_completo = email.From.ToString().Replace("'", "`");
                    char[] delimiterChars = { '`' };
                    String[] usuario_dividido = usuario_completo.Split(delimiterChars);
                    String usuario = "";
                    if (usuario_dividido.Length > 1)
                    {
                        usuario = usuario_dividido[3];
                    }
                    //Texto
                    String texto = email.Text.Replace("'", "`");
                    //To
                    usuario_completo = email.To.ToString().Replace("'", "`");
                    usuario_dividido = usuario_completo.Split(delimiterChars);
                    String usuarioTo = "";
                    bool masDeUno = false;
                    int i;
                    if (usuario_dividido.Length > 1)
                    {
                        for (i = 3; i < usuario_dividido.Length; i = i + 4)
                        {
                            if (i > 3)
                            {
                                masDeUno = true;
                                usuarioTo += "; ";
                            }
                            usuarioTo += usuario_dividido[i];
                        }
                    }
                    //Cc
                    usuario_completo = email.Cc.ToString().Replace("'", "`");
                    usuario_dividido = usuario_completo.Split(delimiterChars);
                    String usuarioCc = "";
                    if (usuario_dividido.Length > 1)
                    {
                        for (i = 3; i < usuario_dividido.Length; i = i + 4)
                        {
                            if (i > 3)
                            {
                                usuarioCc += "; ";
                            }
                            usuarioCc += usuario_dividido[i];
                        }
                    }
                    //Bcc
                    usuario_completo = email.Bcc.ToString().Replace("'", "`");
                    usuario_dividido = usuario_completo.Split(delimiterChars);
                    String usuarioBcc = "";
                    if (usuario_dividido.Length > 1)
                    {
                        for (i = 3; i < usuario_dividido.Length; i = i + 4)
                        {
                            if (i > 3)
                            {
                                usuarioBcc += "; ";
                            }
                            usuarioBcc += usuario_dividido[i];
                        }
                    }
                    String codUsuario = "0";
                    //buscar cliente en tabla COM_CLIENTES_EMAILS
                    if (!masDeUno)
                    {
                        using (OleDbConnection connection = new OleDbConnection(connectionString))
                        {
                            try
                            {

                                OleDbCommand command = new OleDbCommand("SELECT COMCE_IDCLIENTE FROM COM_CLIENTES_EMAILS WHERE COMCE_EMAIL LIKE '" + usuarioTo + "'", connection);
                                //resultado.Text = command3.CommandText;
                                connection.Open();
                                OleDbDataReader reader = command.ExecuteReader();
                                while (reader.Read())
                                {
                                    codUsuario = reader[0].ToString();
                                    asignado = "1";
                                    estaEnTabla = true;
                                }
                                reader.Close();

                            }
                            catch (Exception ex)
                            {
                                resultado.Text = ex.Message;
                            }

                        }
                        if (asignado.Equals("0"))
                        {
                            //buscar cliente en tabla CLIENTES
                            using (OleDbConnection connection = new OleDbConnection(connectionString))
                            {
                                try
                                {

                                    OleDbCommand command = new OleDbCommand("SELECT CON_CODIGO FROM CONTACTOS WHERE CON_EMAIL LIKE '" + usuarioTo + "'", connection);
                                    //resultado.Text = command3.CommandText;
                                    connection.Open();
                                    OleDbDataReader reader = command.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        codUsuario = reader[0].ToString();
                                        asignado = "1";
                                    }
                                    reader.Close();

                                }
                                catch (Exception ex)
                                {
                                    resultado.Text = ex.Message;
                                }

                            }
                        }
                        //Si no está en la tabla COM_CLIENTES_EMAILS, lo insertamos
                        if (!estaEnTabla && asignado.Equals("1"))
                        {
                            using (OleDbConnection connection = new OleDbConnection(connectionString))
                            {
                                OleDbCommand command = new OleDbCommand("INSERT INTO COM_CLIENTES_EMAILS ([COMCE_IDCLIENTE], [COMCE_EMAIL]) VALUES ('" + codUsuario + "', '" + usuarioTo + "')", connection);
                                try
                                {

                                    connection.Open();
                                    command.ExecuteNonQuery();

                                }
                                catch (Exception ex)
                                {
                                    resultado.Text = ex.Message;
                                }
                            }
                        }
                    }
                    using (OleDbConnection connection = new OleDbConnection(connectionString))
                    {
                        OleDbCommand command = new OleDbCommand("INSERT INTO COM_EMAILS ([COME_ASUNTO], [COME_FROM], [COME_TO], [COME_CC], [COME_CCO], [COME_FECHA], [COME_MENSAJE], [COME_ASIGNADO], [COME_ENVIADO]) VALUES ('" + (email.Subject.Replace("'", "`")).Replace(spam, "") + "', '" + usuario + "', '" + usuarioTo + "', '" + usuarioCc + "', '" + usuarioBcc + "', '" + hoy + "', '" + texto + "', " + asignado + ", 1)", connection);
                        try
                        {

                            connection.Open();
                            command.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            resultado.Text = ex.Message;
                        }
                    }
                    using (OleDbConnection connection = new OleDbConnection(connectionString))
                    {
                        try
                        {

                            OleDbCommand command = new OleDbCommand("SELECT MAX(COME_CODIGO) FROM COM_EMAILS", connection);
                            //resultado.Text = command3.CommandText;
                            connection.Open();
                            OleDbDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                idEmail = reader[0].ToString();
                            }
                            reader.Close();

                        }
                        catch (Exception ex)
                        {
                            resultado.Text = ex.Message;
                        }

                    }
                    //Si el cliente existe, lo importamos al seguimiento
                    if (!codUsuario.Equals("0"))
                    {
                        using (OleDbConnection connection = new OleDbConnection(connectionString))
                        {
                            OleDbCommand command = new OleDbCommand("INSERT INTO COM_SEGUIMIENTO ([COMS_IDCLIENTE], [COMS_IDEMAILS], [COMS_ASUNTO], [COMS_MENSAJE], [COMS_FECHA], [COMS_ESTADO], [COMS_IDTIPO]) VALUES ('" + codUsuario + "', '" + idEmail + "', '" + (email.Subject.Replace("'", "`")).Replace(spam, "") + "', '" + texto + "', '" + hoy + "', 1, 1)", connection);
                            try
                            {

                                connection.Open();
                                command.ExecuteNonQuery();

                            }
                            catch (Exception ex)
                            {
                                resultado.Text = ex.Message;
                            }
                        }
                    }

                    using (OleDbConnection connection = new OleDbConnection(connectionString))
                    {
                        OleDbCommand command;
                        foreach (MimeData attachment in email.Attachments)
                        {
                            try
                            {
                                command = new OleDbCommand("INSERT INTO COM_ADJUNTOS ([COMA_IDEMAILS], [COMA_RUTA]) VALUES ('" + idEmail + "', '" + idEmail + attachment.FileName + "')", connection);

                                //command.Parameters.AddWithValue("@param1", idEmail);
						        //command.Parameters.AddWithValue("@param2", attachment.FileName);

                                //resultado.Text = command.CommandText;
                                connection.Open();
                                command.ExecuteNonQuery();
                                connection.Close();
                                String ocPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"attach/" + idEmail + attachment.SafeFileName);
                                attachment.Save(ocPath);
                            }
                            catch (Exception ex)
                            {
                                resultado.Text = ex.Message;
                            }
                        }

                    }

                }
            }*/
            imap.Close();
        }
		
		using (OleDbConnection connection = new OleDbConnection(connectionString))
		{
			
			OleDbCommand command = new OleDbCommand("UPDATE COM_UID SET [COM_UID] = " + lastId + " WHERE [COM_CODIGO] = 1", connection);
			try
			{

				connection.Open();
				command.ExecuteNonQuery();

			}
			catch (Exception ex)
			{
				resultado.Text = ex.Message;
			}

		}
		
    }

    private static bool findTicket(String text)
    {

        bool encontrado = false;
        String sPattern = @"#\d*:";

        if (System.Text.RegularExpressions.Regex.IsMatch(text, sPattern))
        {
            encontrado = true;
        }

        return encontrado;

    }

    private static string JoinAddresses(IList<MailBox> mailboxes)
    {
        return string.Join(",",
            new List<MailBox>(mailboxes).ConvertAll(m => string.Format("{0} <{1}>", m.Name, m.Address))
            .ToArray());
    }

    private static string JoinAddresses(IList<MailAddress> addresses)
    {
        StringBuilder builder = new StringBuilder();

        foreach (MailAddress address in addresses)
        {
            if (address is MailGroup)
            {
                MailGroup group = (MailGroup)address;
                builder.AppendFormat("{0}: {1};, ", group.Name, JoinAddresses(group.Addresses));
            }
            if (address is MailBox)
            {
                MailBox mailbox = (MailBox)address;
                builder.AppendFormat("{0} <{1}>, ", mailbox.Name, mailbox.Address);
            }
        }
        return builder.ToString();
    }
}