<!-- #include virtual="/dmcrm/APP/Sistema/Comunicaciones/utf8Decode.asp"-->  
<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->
<script type="text/javascript" src="js/rotate.js"></script>
<%
tabla = "CON_EMAILS" 
if(request("buzonCorreo") <> "") then
	buzonCorreo = request("buzonCorreo")
 	%><h1>Buz�n: Varios</h1><%
else
	buzonCorreo = "INBOX"
 	%><h1><%=objIdiomaGeneral.getIdioma("Emails_Titulo1")%></h1><%
end if

registrosPorPagina = 30
pagina = "1"
if(request("pagina") <> "") then
	pagina = request("pagina")
end if

hoy = date()
Function ContrasenaCodifica(Contrasena)    
    Dim vLongitud, vCaracter, vCodificado
    vLongitud = Len(Contrasena)
    vCodificado = ""
    For vCaracter = 1 To vLongitud
        vCodificado = vCodificado & Chr(Asc(Mid(Contrasena, vCaracter, 1)) + 50)
    Next
    ContrasenaCodifica = vCodificado
End Function
%>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<% if(request("buzonCorreo") <> "")then %>
	<!-- #include virtual="/dmcrm/APP/Sistema/Comunicaciones/buzonCorreo.asp"-->
	<div id="gestionclientes" class="contenidoSeccion" style="width:85%; position:relative; float:right; z-index:1;">
<% else %>
	<div id="gestionclientes" class="contenidoSeccion" style="z-index:1;">
<% end if %>
	<!-- #include virtual="/dmcrm/APP/Sistema/Comunicaciones/controlesFiltrosEmails.asp"-->  
	<script type="text/javascript">
        $(document).ready(function() {            
            $(".cEmergente").live('click', function(){
                $(this).fancybox({
                    'imageScale'			: true,
                    'zoomOpacity'			: true,
                    'overlayShow'			: true,
                    'overlayOpacity'		: 0.7,
                    'overlayColor'			: '#333',
                    'centerOnScroll'		: true,
                    'zoomSpeedIn'			: 600,
                    'zoomSpeedOut'			: 500,
                    'transitionIn'			: 'elastic',
                    'transitionOut'			: 'elastic',
                    'type'					: 'iframe',
                    'frameWidth'			: 925,
                    'frameHeight'			: '80%',
                    'titleShow'				: false	,
                    'onClosed'		        : function() {
                            recargarAlCerrar();
                    }											
                }).trigger("click"); 
                return false; 	
            });	
        });
            
        function recargarAlCerrar() {
            if (document.forms[0] != null)
            { document.forms[0].submit(); }
            else { window.location.reload(); }
            //		document.location.reload();
        }
		
		function actualizarEmails(){
			$.ajax({
				type: "POST",
				url: "",
				data: {"var": "<%=ContrasenaCodifica(hoy)%>"},
				success: function() 
				{
					window.location.href = window.location.href;
				}
			});
		}
		
		function asignarAutomaticamente(){
			$.ajax({
				type: "POST",
				url: "",
				success: function() 
				{
					window.location.href = window.location.href;
				}
			});
		}
		
		function marcarSpam(id){
			$.ajax({
				type: "POST",
				url: "marcarSpam.asp",
				data: {"id": id, "marcar": 2},
				success: function() 
				{
					window.location.href = window.location.href;
				}
			});
		}
		
		function enviarVarios(id){
			$.ajax({
				type: "POST",
				url: "enviarVarios.asp",
				data: {"id": id, "marcar": 1},
				success: function() 
				{
					window.location.href = window.location.href;
				}
			});
		}
		
		function desmarcarSpam(id){
			$.ajax({
				type: "POST",
				url: "marcarSpam.asp",
				data: {"id": id, "marcar": 0},
				success: function() 
				{
					window.location.href = window.location.href;
				}
			});
		}
		
		$(document).ready(function(){
			$("#actualizarEmails").click(function(){
				var angle = 0;
				setInterval(function(){
					  angle-=30;
					 $("#imgRotate").rotate(angle);
				},50);
			});
		});
		
    </script>
    
    <div id="actualizarEmails" class="floatleft"><a href="javascript: actualizarEmails();" title="Enviar y recibir e-mails"><img src="img/actualizar.png" id="imgRotate" style="width:3em;" alt="" /></a></div>
    <div id="asignarAutomaticamente" class="floatleftmargin"><a href="javascript: asignarAutomaticamente();" title="Asginar autom�ticamente"><img src="img/asignarAuto.png" style="width:3em;" alt="" /></a></div>
    <div class="clearEmails"></div>
    <div class="bloqueSeleccion blokeResponsiveIzq" style="width: 100%;">
    	<div id="contenedorSeleccion">
        	<div id="tablaContenido_wrapper" class="dataTables_wrapper">
            <table id="tablaContenido" width="99%" cellspacing="1" cellpadding="0" border="0" style="z-index:1; padding:0px; margin:0px; " rel="CON_CONSULTAS">
            	<thead>
					<tr id="cabeceraTabla">
                    	<th></th>
                        <th><%=objIdiomaGeneral.getIdioma("Emails_Texto9")%></th>
                        <th><%=objIdiomaGeneral.getIdioma("Emails_Texto10")%></th>
                        <th><%=objIdiomaGeneral.getIdioma("Emails_Texto3")%></th>
                        <th><%=objIdiomaGeneral.getIdioma("Emails_Texto12")%></th>
                        <th><%=objIdiomaGeneral.getIdioma("Emails_Texto13")%></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                
                    par = 1
                    'sql = "SELECT * FROM CON_EMAILS WHERE CONE_ASIGNADO = 0 ORDER BY CONE_CODIGO DESC"
                    sql = "SELECT * FROM (" &_
                            " SELECT ROW_NUMBER()Over(Order by COME_FECHA Desc) As RowNum, COME_FROM, COME_TO, COME_CC, COME_CCO," &_
                            " 	COME_ASUNTO, COME_MENSAJE, COME_CODIGO, COME_ENVIADO, COME_ASIGNADO, COME_FECHA" &_
                            " FROM COM_EMAILS" &_
                            " WHERE COME_BUZON = '" & buzonCorreo & "' " &_
							consultaWhere &_
                        " )" &_
                        " AS ResultadoPaginado" &_
                        " WHERE RowNum BETWEEN (" & pagina & " - 1) * " & registrosPorPagina & " + 1 AND " & pagina & " * " & registrosPorPagina & "" &_
						" ORDER BY COME_FECHA DESC"
                    'response.Write(sql)
                    'response.End()
                    Set rsEmails = connCRM.abrirRecordSet(sql, 3, 1)
                    while not rsEmails.eof
                        response.Write("<tr")
						if (rsEmails("COME_ASIGNADO") = 2) then
							response.Write(" style=""background-color:#FF9900;""")
						else
							if (par mod 2 = 0) then
								response.Write(" style=""background-color:#F1F3F3;""")
							end if
						end if
                        response.Write(">")
						if(rsEmails("COME_ENVIADO") = True) then
							response.Write("<td><a class=""cEmergente"" href=""verSeguimiento.asp?codigo=" & rsEmails("COME_CODIGO") & "&esEmail=ok"" title=""""><img src=""/dmcrm/APP/Sistema/Comunicaciones/img/email-send.png"" style=""width:3em;"" alt="""" /></a></td>")
						else
							response.Write("<td><a class=""cEmergente"" href=""verSeguimiento.asp?codigo=" & rsEmails("COME_CODIGO") & "&esEmail=ok"" title=""""><img src=""/dmcrm/APP/Sistema/Comunicaciones/img/email-receive.png"" style=""width:3em;"" alt="""" /></a></td>")
						end if
                        response.Write("<td><a class=""cEmergente"" href=""verSeguimiento.asp?codigo=" & rsEmails("COME_CODIGO") & "&esEmail=ok"" title="""">" & rsEmails("COME_FROM") & "</a></td>")
						response.Write("<td><a class=""cEmergente"" href=""verSeguimiento.asp?codigo=" & rsEmails("COME_CODIGO") & "&esEmail=ok"" title="""">" & objIdiomaGeneral.getIdioma("Seguimiento_Texto5") & ":")
						aquien = split(rsEmails("COME_TO"), ";")
						For Each a In aquien
							 response.Write("<span>" & a & "</span><br />")
						Next
						response.Write("" & objIdiomaGeneral.getIdioma("Seguimiento_Texto6") & ":")
						ccquien = split(rsEmails("COME_CC"), ";")
						For Each cc In ccquien
							 response.Write("<span>" & cc & "</span><br />")
						Next
						response.Write("" & objIdiomaGeneral.getIdioma("Seguimiento_Texto7") & ":")
						ccoquien = split(rsEmails("COME_CCO"), ";")
						For Each cco In ccoquien
							 response.Write("<span>>" & cco & "</span>><br />")
						Next
                        response.Write("</a></td>")
						if InStr(rsEmails("COME_ASUNTO"), "�") > 0 or InStr(rsEmails("COME_ASUNTO"), "�") > 0 then
							asunto = DecodeUTF8(rsEmails("COME_ASUNTO"))
						else
							asunto = rsEmails("COME_ASUNTO")
						end if
                        response.Write("<td><a class=""cEmergente"" href=""verSeguimiento.asp?codigo=" & rsEmails("COME_CODIGO") & "&esEmail=ok"" title="""">" & asunto & "</a></td>")
						sql = "SELECT * FROM COM_ADJUNTOS WHERE COMA_IDEMAILS = " & rsEmails("COME_CODIGO")
						Set rsAdjuntos = connCRM.abrirRecordSet(sql, 3, 1)
						response.Write("<td class=""contenidoCentrado"">")
						'while not rsAdjuntos.eof
						'	response.Write("<a href=""mail/attach/" & rsAdjuntos("COMA_RUTA") & """ title="""" target=""_blank"">" & rsAdjuntos("COMA_RUTA") & "</a><br />")
						'	rsAdjuntos.movenext()
						'wend
						if not rsAdjuntos.eof then
							response.Write("<a class=""cEmergente"" href=""verSeguimiento.asp?codigo=" & rsEmails("COME_CODIGO") & "&esEmail=ok"" title=""""><img src=""img/attach.png"" alt="""" style=""width:3em;"" /></a>")
						end if
						response.Write("</td>")
						response.Write("<td style=""width:11em;""><a class=""cEmergente"" href=""/dmcrm/APP/Sistema/Comunicaciones/importarEmails.asp?idEmails=" & rsEmails("COME_CODIGO") & "&pagina=" & pagina & """ title=""Asignar"" style=""float:left;""><img src=""/dmcrm/APP/Sistema/Comunicaciones/img/asignar.png"" alt="""" style=""width:3em; margin-right:1em;"" /></a>&nbsp;<a class=""cEmergente"" href=""/dmcrm/APP/Sistema/Comunicaciones/clasificarEmails.asp?idEmails=" & rsEmails("COME_CODIGO") & "&pagina=" & pagina & """ title=""Archivar en Buz�n: Varios"" style=""float:left;""><img src=""/dmcrm/APP/Sistema/Comunicaciones/img/varios.jpg"" alt="""" style=""width:2.5em;"" /></a>")
						if (rsEmails("COME_ASIGNADO") = 2) then
							response.Write("<a href=""javascript: desmarcarSpam(" & rsEmails("COME_CODIGO") & ");"" title=""Desmarcar como spam""><img src=""/dmcrm/APP/Sistema/Comunicaciones/img/spam.png"" style=""width:3em; margin-left:1em;"" alt="""" /></a>")
						else
							response.Write("<a href=""javascript: marcarSpam(" & rsEmails("COME_CODIGO") & ");"" title=""Marcar como spam""><img src=""/dmcrm/APP/Sistema/Comunicaciones/img/spam.png"" style=""width:3em; margin-left:1em;"" alt="""" /></a>")
						end if
						response.Write("</td>")
						response.Write("</tr>")
                        par = par + 1
                        rsEmails.movenext()
                    wend
                    %>
            	</tbody>
            </table>
            </div>
        </div>
    </div>
    <div id="tablaContenido_paginate" class="dataTables_paginate paging_full_numbers">
    <span>
    <%
	sql = "SELECT COUNT(*) as num FROM COM_EMAILS WHERE COME_BUZON = '" & request("buzonCorreo") & "' " & consultaWhere
                        
	Set rsEmails = connCRM.abrirRecordSet(sql, 3, 1)
	num = 0
	if not rsEmails.EOF then
		numEmails = rsEmails("num")
	end if
	num = ceil(numEmails / registrosPorPagina)
	if CInt(pagina) < 6 then
		inicio = 0
	else
		inicio = CInt(pagina) - 5
	end if
	if CInt(pagina) + 5 > num then
		fin = num
	else
		fin = CInt(pagina) + 5
	end if
	if inicio > 1 then
		response.Write("<span class=""paginate_button""><a href=""?pagina=1&txtAsignado=" & request("txtAsignado") & "&txtEstado=" & request("txtEstado") & "&txtTipo=" & request("txtTipo") & "&txtTexto=" & request("txtTexto") & "&txtFiltro=" & request("txtFiltro") & "&buzonCorreo=" & request("buzonCorreo") & """ title="""" style=""color:#666666; text-decoration:none;""> << </a></span>")
		response.Write("<span class=""paginate_button""><a href=""?pagina=" & (CInt(pagina) - 1) & "&txtAsignado=" & request("txtAsignado") & "&txtEstado=" & request("txtEstado") & "&txtTipo=" & request("txtTipo") & "&txtTexto=" & request("txtTexto") & "&txtFiltro=" & request("txtFiltro") & "&buzonCorreo=" & request("buzonCorreo") & """ title="""" style=""color:#666666; text-decoration:none;""> < </a></span>")
	end if
	for i = 1 to num
		if(inicio <= i) and (fin >= i) then
			if(i & "" = pagina) then
				response.Write("<span class=""paginate_active"">" & i & "</span>")
			else
				response.Write("<span class=""paginate_button""><a href=""?pagina=" & i & "&txtAsignado=" & request("txtAsignado") & "&txtEstado=" & request("txtEstado") & "&txtTipo=" & request("txtTipo") & "&txtTexto=" & request("txtTexto") & "&txtFiltro=" & request("txtFiltro") & "&buzonCorreo=" & request("buzonCorreo") & """ title="""" style=""color:#666666; text-decoration:none;"">" & i & "</a></span>")
			end if
		end if
	next
	if fin < num then
		response.Write("<span class=""paginate_button""><a href=""?pagina=" & (CInt(pagina) + 1) & "&txtAsignado=" & request("txtAsignado") & "&txtEstado=" & request("txtEstado") & "&txtTipo=" & request("txtTipo") & "&txtTexto=" & request("txtTexto") & "&txtFiltro=" & request("txtFiltro") & "&buzonCorreo=" & request("buzonCorreo") & """ title="""" style=""color:#666666; text-decoration:none;""> > </a></span>")
		response.Write("<span class=""paginate_button""><a href=""?pagina=" & num & "&txtAsignado=" & request("txtAsignado") & "&txtEstado=" & request("txtEstado") & "&txtTipo=" & request("txtTipo") & "&txtTexto=" & request("txtTexto") & "&txtFiltro=" & request("txtFiltro") & "&buzonCorreo=" & request("buzonCorreo") & """ title="""" style=""color:#666666; text-decoration:none;""> >> </a></span>")
	end if
	%>
    </span>
    </div>
</div>
<%
function ceil(x)
	dim temp
	 
	temp = Round(x)
	 
	if temp < x then
	temp = temp + 1
	end if
	 
	ceil = temp
end function
%>
<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->






