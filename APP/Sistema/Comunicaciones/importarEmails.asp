<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
</head>
<body>
<!--Inicio contenido-->
<%
if(request("idCliente") <> "" and request("idEmails") <> "" and request("seguro") = "ok") then
	sql = "SELECT * FROM COM_EMAILS WHERE COME_CODIGO = " & request("idEMails")
	Set rsEmail = connCRM.abrirRecordSet(sql, 3, 1)
	if not rsEmail.EOF then
		asunto = rsEmail("COME_ASUNTO")
		mensaje = rsEmail("COME_MENSAJE")
		fecha = rsEmail("COME_FECHA")
		email = rsEmail("COME_FROM")
		sql = "SELECT * FROM COM_CLIENTES_EMAILS WHERE COMCE_EMAIL LIKE '" & email & "'"
		Set rsClienteEmail = connCRM.abrirRecordSet(sql, 3, 1)
		if(rsClienteEmail.EOF) then
			sql = "INSERT INTO COM_CLIENTES_EMAILS ([COMCE_IDCLIENTE], [COMCE_EMAIL]) VALUES('" & request("idCliente") & "', '" & email & "')"
			connCRM.execute sql,numregistrosInsert
		end if
		sql = "INSERT INTO COM_SEGUIMIENTO ([COMS_IDCLIENTE], [COMS_IDEMAILS], [COMS_ASUNTO], [COMS_MENSAJE], [COMS_FECHA], [COMS_ESTADO], [COMS_IDTIPO]) VALUES ('" & request("idCliente") & "', '" & request("idEMails") & "', '" & asunto & "', '" & mensaje & "', '" & fecha & "', 0, 1)"
		connCRM.execute sql,numregistrosInsert
		sql = "UPDATE COM_EMAILS SET COME_ASIGNADO = 1 WHERE COME_CODIGO = " & request("idEMails")
		connCRM.execute sql,numregistrosInsert
		response.Write("<script>parent.$.fancybox.close();</script>")
	end if
else
	if(request("idCliente") <> "" and request("idEMails") <> "") then
		response.Write("<p><strong>" & objIdiomaGeneral.getIdioma("Seguimiento_Texto34") & "</strong></p><p><a href=""importarEmails.asp?idCliente=" & request("idCliente") & "&idEmails=" & request("idEmails") & "&seguro=ok"" title="""">" & objIdiomaGeneral.getIdioma("Seguimiento_Texto35") & "</a> | <a href=""importarEmails.asp?idEmails=" & request("idEmails") & """ title="""">" & objIdiomaGeneral.getIdioma("Seguimiento_Texto36") & "</a></p>")
	else
	%>
		<form method="post" id="busquedaClientes">
			<%=objIdiomaGeneral.getIdioma("PromocionesSocios_Texto40")%>:<br />
			<input type="text" value="" id="nombreCliente" name="nombreCliente" onKeyPress="return event.keyCode!=13" />
			<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonBuscar_Texto1")%>" id="botonCliente" name="botonCliente" />
		</form>
		<table id="tablaClientes">
			<thead>
				<tr>
					<th><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto23")%></th>
					<th><%=objIdiomaGeneral.getIdioma("PromocionesSocios_Texto40")%></th>
					<th><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto10")%></th>
				</tr>
			</thead>
			<tbody id="registrosCliente">
			</tbody>
		</table>
	<%
	end if
end if
%>
<script type="text/javascript">
	$(document).keypress(function(e) {
		if(e.which == 13) {
			cargarClientes();
		}
	});
	$(document).ready(function () {
		$('#botonCliente').click(function() {
			cargarClientes();
		});
	});
	function cargarClientes(){
		$.ajax({
			type: "POST",
			url: "/DMCrm/APP/Sistema/Comunicaciones/listadoClientes.asp",
			data: "nombre=" + $('#nombreCliente').val(),
			async: false,
			success: function(datos) {
				$('#registrosCliente').children( 'tr' ).remove();
				dividirDatos = datos.split("###");
				for(var i = 0; i < dividirDatos.length - 1; i++){
					dividirDatos2 = dividirDatos[i].split("&&&");
					
					tr = document.createElement("tr");
					td1 = document.createElement("td");
					td2 = document.createElement("td");
					td3 = document.createElement("td");
					a = document.createElement("a");
					a.href = "importarEmails.asp?idCliente=" + dividirDatos2[0] + "&idEmails=<%=request("idEmails")%>";
					a.innerHTML = dividirDatos2[1];
					td1.innerHTML = dividirDatos2[0];
					td3.innerHTML = dividirDatos2[2];
					td2.appendChild(a);
					tr.appendChild(td1);
					tr.appendChild(td2);
					tr.appendChild(td3);
					document.getElementById("registrosCliente").appendChild(tr);
				}
			},
			error: function(data) {
				alert("error: " + data);
			}
		});
	}
</script> 
<!--Fin contenido-->
</body>
</html>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->