<!-- #include virtual="/dmcrm/APP/Sistema/Comunicaciones/utf8Decode.asp"-->  
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->

<script src="/dmcrm/js/jquery/jquery.js"></script>
<script src="js/mousewheel.js"></script>
<script src="js/jscrollpane.min.js"></script>
<script type="text/javascript" id="sourcecode">
	$(function()
	{
		var win = $(window);
		// Full body scroll
		var isResizing = false;
		win.bind(
			'resize',
			function()
			{
				if (!isResizing) {
					isResizing = true;
					var container = $('#contenidoScroll');

					container.css(
						{
							'width': 1,
							'height': 1
						}
					);
					container.css(
						{
							'width': win.width(),
							'height': win.height() - 20
						}
					);
					isResizing = false;
					container.jScrollPane(
						{
							'showArrows': true
						}
					);
				}
			}
		).trigger('resize');
		$('body').css('overflow', 'hidden');
		if ($('#full-page-container').width() != win.width()) {
			win.trigger('resize');
		}
	});
</script>
<style>
*:focus { outline:none; }
/* JSCROLLPANE */
#contenidoScroll { margin:10px 0; }
.jspContainer { overflow:hidden; position:relative; }
.jspPane { position:absolute; }

.jspVerticalBar { position:absolute; top:0; right:0; width:5px; height:100%; }
.jspCap { display: none; }
.jspTrack { background:#ccc; position:relative; }
.jspDrag { background:#999; position:relative; top:0; left:0;}
.jspHorizontalBar .jspTrack { float: left; height:100%; }
.jspArrow { background:#999; text-indent:-20000px; display: block; padding: 0; margin: 0; }
.jspVerticalBar .jspArrow { height:5px; }
.jspCorner { background:#eeeef4; float: left; height: 100%; }
.jspDrag:hover { background:#e55d39; }
.jspVerticalBar .jspArrow:hover { background:#e55d39; }
</style>
<div id="contenidoScroll">
<% 
'Response.CharSet = "utf-8"
cod = request("codigo")

Set rsTicket = connCRM.abrirRecordSet("SELECT * FROM COM_EMAILS WHERE COME_CODIGO=" & cod & " ORDER BY COME_CODIGO DESC",0,1)
if not rsTicket.eof then
	if InStr(rsTicket("COME_MENSAJE"), "�") > 0 or InStr(rsTicket("COME_MENSAJE"), "�") > 0 then
		mensaje = DecodeUTF8(rsTicket("COME_MENSAJE"))
	else
		mensaje = rsTicket("COME_MENSAJE")
	end if
	response.Write(mensaje)
end if
rsTicket.cerrar
set rsTicket = nothing
%>
</div>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->