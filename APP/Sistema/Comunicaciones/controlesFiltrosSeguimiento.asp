<!-- #include virtual="/dmcrm/APP/Sistema/Comunicaciones/seguimientoPrepararFiltros.asp"-->
<%
filtrosBusqueda = "'{ ""name"": ""txtUsuario"", ""value"": ""'+$('#txtUsuario').val()+'"" }**"
filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""txtAsunto"", ""value"": ""'+$('#txtAsunto').val()+'"" }'"
'filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""cboAdmin"", ""value"": ""'+$('#cboAdmin').val()+'"" }**"
'filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""txtFechaDesde"", ""value"": ""'+$('#txtFechaDesde').val()+'"" }**"
'filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""txtFechaHasta"", ""value"": ""'+$('#txtFechaHasta').val()+'"" }**"
'filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""cboEstado"", ""value"": ""'+$('#cboEstado').val()+'"" }'" %>

<script type="text/javascript">
	function marcarFiltroResumen(valorFiltro) {
		document.getElementById("btnLimpiar").click();										
		if (document.getElementById("txtFiltroEstadoResumen")!=null) {
			document.getElementById("txtFiltroEstadoResumen").value=valorFiltro;
		}	
		document.getElementById("btnBuscarFiltros").click();
	}

	$(document).ready(function() {
		$("#btnMostrarResumen").live('click', function(){
			$("#capaDatosResumen").show('slow');
			$("#btnOcultarResumen").show('slow');					
			$("#btnMostrarResumen").hide('slow');		
			$("#txtPanelCerrado").val('');
		});		
		
		$("#btnOcultarResumen").live('click', function(){
			$("#capaDatosResumen").hide('slow');
			$("#btnMostrarResumen").show('slow');					
			$("#btnOcultarResumen").hide('slow');	
			$("#txtPanelCerrado").val('1');					
		});		
		
		$("#btnBuscarFiltros").click(function(){
	  		$("#buscadorFiltros").submit();
	  	});
	
		$("#txtTexto").keypress(function(event) {
			if (event.which == 13) {
				$("#buscadorFiltros").submit(); 
			}
		});
	});					   				
</script>									   

<style type="text/css">
	.bloqueCabeceraIzq {float:left; margin:0; padding:0; width:49%; margin-right:1%;}
	.bloqueCabeceraDer {float:left; margin:0; padding:0; width:49%;}
	
	.tablaBloqueCabecera {float:left; clear:both;}
	.filaTablaDatos {   }
	.celdaTablaResumen { min-height:40px; border-bottom:dashed 1px #CDCDCD;  }
	.celdaTablaNumerico { min-height:40px; border-bottom:dashed 1px #CDCDCD; color:#FF6600; font-family:Arial; font-size:14px; font-weight:bold; }

	.imgFilaBloque {position:relative; top:5px; left:0;}
	#sociosEnviarMailsTodos {padding:2px 0 0 30px; text-align:left; line-height:1em; font-size:11px; color:#8C8C8D; width:118px;}
	
	.enlaceVerSocios { font-style:italic; float:right; }
	.cabTabla { text-align:left; border-bottom:1px solid #000000; font-weight:bold; padding-top:10px }
	.cajaTexto { width: 200px; }

	.bloqueColumna { width:92%; }
	.bloke { float:left; margin:3px 3px 0 0; width:32%; }
	.bloke label { width:22%; padding:7px 0; background:#ccc; display:block; float:left; margin:0; text-align:center; border-bottom:3px solid #999; }
	.bloke:hover label { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }
	.bloke input, .bloke textarea, .bloke select { width:73%; margin:0; padding:7px 6px 6px; background:#eee; border:0; border-bottom:3px solid #ccc; font-size:12px; font-family:Arial, Verdana, sans-serif; color:#666; }
	.bloke select { /*width:300px;*/ margin:0; padding:6px 7px 5px; }	
</style>

<form action="?cod=<%=request("cod")%>&pagina=<%=pagina%>" method="post" id="buscadorFiltros">   
<div id="divControlesTodos" >   
<!--          								
    <div id="filtro" class="filtro">
        &nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("Seguimiento_Texto1")%>
    </div>
-->
    <div class="bloqueColumna" >   
    	<div class="bloke">                         				                                       
	    	<label id="labelAsignado" for="txtAsignado">Asignado:</label>
	        <select id="txtAsignado" name="txtAsignado">
	            <option value="0" <% if(request("txtAsignado") = "0") then response.write("selected") end if %>>Todos</option>
	        	<option value="1" <% if(request("txtAsignado") = "1") then response.write("selected") end if %>>Vacio</option>
	        	<option value="2" <% if(request("txtAsignado") = "2") then response.write("selected") end if %>>Usuarios</option>
	        </select>
	    </div>


		<% if(especifico = 1) then %>    				                                       

	        <div class="bloke">  
	            <label id="labelEstado" for="txtEstado">Estado:</label>
	            <select id="txtEstado" name="txtEstado">
	            	<option value="0" <% if(request("txtEstado") = "0") then response.write("selected") end if %>>Sin tratar</option>
	                <option value="1" <% if(request("txtEstado") = "1") then response.write("selected") end if %>>Todos</option>
	            </select>
	        </div> 
		<% else %>
        	<input type="hidden" name="txtEstado" value="0" />
        <% end if %>     

        <div class="bloke"> 
        	<label id="labelTexto" for="txtTexto">Texto libre:</label>
        	<input id="txtTexto" name="txtTexto" type="text" class="cajaTexto" maxlength="250"  value="<%=request("txtTexto")%>"  />
        </div>
    </div>                					
</div>
<p>&nbsp;</p>	
    
    <input id="btnLimpiar" name="btnLimpiar" type="button" class="boton" value="<%=objIdiomaGeneral.getIdioma("BotonLimpiar_Texto1")%>" onClick="javascript:inicializarFiltros();"  style="float:right; margin:-25px 0 0 0;" />   
    <input type="button" name="btnBuscarFiltros" id="btnBuscarFiltros"  value="<%=objIdiomaGeneral.getIdioma("BotonBuscar_Texto1")%>"  class="boton" style="float:right; margin:-25px  100px 0 0;"  />

    <input type="text" id="txtPanelCerrado" name="txtPanelCerrado" value="<%=request.Form("txtPanelCerrado")%>" style="display:none;" />
    <input type="text" id="txtFiltroEstadoResumen" name="txtFiltroEstadoResumen" value="<%=request.Form("txtFiltroEstadoResumen")%>" style="display:none;" />	
</form>				   																		

<div style="clear:both; width:100%; height:20px; margin:0; padding:0; float:left;"></div>


<script type="text/javascript">
	diferenciarFiltro();
</script>