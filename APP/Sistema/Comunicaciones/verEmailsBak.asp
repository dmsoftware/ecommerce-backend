<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
</head>
<body>
<!--Inicio contenido-->
<% ticket = request("codigo")
		
	if request.QueryString("clasificar") = 1 then
		if(request.Form("clasificarEmail") = 1) then
			id = ticket
			idadmin = request.Form("idadmin")
			Set rsEmail = connCRM.abrirRecordSet("SELECT * FROM CON_EMAILS WHERE CONE_CODIGO=" & id & " ORDER BY CONE_CODIGO DESC",3,1)
			if not rsEmail.eof then
				id = rsEmail("CONE_CODIGO")
				usuario = rsEmail("CONE_USUARIO")
				asunto = rsEmail("CONE_ASUNTO")
				mensaje = rsEmail("CONE_MENSAJE")
				
				codusuario = 0
				Set rsCliente = connCRM.abrirRecordSet("SELECT CLIE_CODIGO FROM CLIENTES WHERE CLIE_EMAIL LIKE '" & usuario & "'",3,1)
				if not rsCliente.eof then
					codusuario = rsCliente("CLIE_CODIGO")
				end if
				
				sqlInsert = "INSERT INTO CON_CONSULTAS"&_
							" (CONC_CODUSUARIO, CONC_MAILUSUARIO, CONC_ASUNTO, CONC_SUESTADO, CONC_FECHA, CONC_FECHAACT, CONC_SUADMINISTRADOR)"&_
							" VALUES ('" & codusuario & "' ,'" & usuario & "' ,'" & asunto & "',1,'" & now & "','" & now & "', '" & idadmin & "')"
				connCRM.execute sqlInsert,numregistrosInsert
				codconsulta = 0
				Set rsConsulta = connCRM.abrirRecordSet("SELECT CONC_CODIGO FROM CON_CONSULTAS ORDER BY CONC_CODIGO DESC",3,1)
				if not rsConsulta.eof then
					codconsulta = rsConsulta("CONC_CODIGO")
				end if
				
				sqlInsert = "INSERT INTO CON_LINEASCONSULTA"&_
							" (CONL_SUCONSULTA, CONL_CODUSUARIO, CONL_ESADMIN, CONL_MENSAJE, CONL_LEIDO, CONL_IP, CONL_NAVEGADOR, CONL_FECHA)"&_
							" VALUES ('" & codconsulta & "' ,0 ,0,'" & mensaje & "' ,0 ,'','','" & now & "')"
				connCRM.execute sqlInsert,numregistrosInsert
			end if
		else
			id = request.QueryString("id")
			idadmin = request.Form("idadmin")
			codconsulta = request.Form("consultas")
			Set rsEmail = connCRM.abrirRecordSet("SELECT * FROM CON_EMAILS WHERE CONE_CODIGO=" & id & " ORDER BY CONE_CODIGO DESC",3,1)
			if not rsEmail.eof then
				id = rsEmail("CONE_CODIGO")
				usuario = rsEmail("CONE_USUARIO")
				asunto = rsEmail("CONE_ASUNTO")
				mensaje = rsEmail("CONE_MENSAJE")
				
				codusuario = 0
				Set rsCliente = connCRM.abrirRecordSet("SELECT CLIE_CODIGO FROM CLIENTES WHERE CLIE_EMAIL LIKE '" & usuario & "'",3,1)
				if not rsCliente.eof then
					codusuario = rsCliente("CLIE_CODIGO")
				end if
				
				sqlInsert = "INSERT INTO CON_LINEASCONSULTA"&_
							" (CONL_SUCONSULTA, CONL_CODUSUARIO, CONL_ESADMIN, CONL_MENSAJE, CONL_LEIDO, CONL_IP, CONL_NAVEGADOR, CONL_FECHA, CONL_EMAILUSUARIO)"&_
							" VALUES ('" & codconsulta & "' ,0 ,0,'" & mensaje & "' ,0 ,'','','" & now & "', '" & usuario & "')"
				connCRM.execute sqlInsert,numregistrosInsert
			end if
		end if
		sqlUpdate = "UPDATE CON_EMAILS"&_
					" SET CONE_ASIGNADO = 1"&_
					" WHERE CONE_CODIGO = " & id
		connCRM.execute sqlUpdate,numregistrosInsert
	end if
	
	Set rsTicket = connCRM.abrirRecordSet("SELECT * FROM CON_EMAILS WHERE CONE_CODIGO=" & ticket & " ORDER BY CONE_CODIGO DESC",3,1)
	if not rsTicket.eof then
		id = rsTicket("CONE_CODIGO")
		usuario = rsTicket("CONE_USUARIO")
		asunto = rsTicket("CONE_ASUNTO")
		mensaje = rsTicket("CONE_MENSAJE")
		responsable = 0
		%>
      
		<div id="tituloTicketAdmin">  
		<h3><%=usuario%></h3>
		<h2><%=asunto%></h3>
        <p>&nbsp;</p>
        <p><%=mensaje%></p>
        <p>&nbsp;</p>
        <p><%=objIdioma.getIdioma("Emails_Texto4")%></p>
        <form action="verEmails.asp?clasificar=1&codigo=<%=id%>" method="post">
        	<select name="clasificarEmail" onChange="javascript: if(this.value=='2'){ $('#consultas').show(); $('#idadmin').hide(); }else{ $('#consultas').hide(); $('#idadmin').show(); }">
            	<option value="1"><%=objIdioma.getIdioma("Emails_Texto6")%></option>
            	<option value="2"><%=objIdioma.getIdioma("Emails_Texto7")%></option>
            </select>
            <p>&nbsp;</p>
            <div id="consultas" style="display:none;">
                <p><%=objIdioma.getIdioma("Emails_Texto3")%></p>
                <select name="consultas">
                    <%
                    Set rsConsultas = connCRM.abrirRecordSet("SELECT CONC_CODIGO, CONC_ASUNTO FROM CON_CONSULTAS WHERE CONC_SUESTADO <> 3 ORDER BY CONC_ASUNTO ASC",3,1)
                    while not rsConsultas.eof
                        response.write("<option value=""" & rsConsultas("CONC_CODIGO") & """>" & rsConsultas("CONC_ASUNTO") & "</option>")
                        rsConsultas.movenext()
                    wend
                    %>
                </select>
            </div>
            <div id="idadmin">
                <p><%=objIdioma.getIdioma("Emails_Texto5")%></p>
                <select name="idadmin">
                    <%
                    Set rsAdministradores = connCRM.abrirRecordSet("SELECT ANA_CODIGO, ANA_NOMBRE, ANA_APELLIDOS FROM ANALISTAS ORDER BY ANA_NOMBRE ASC",3,1)
                    while not rsAdministradores.eof
                        response.write("<option value=""" & rsAdministradores("ANA_CODIGO") & """>" & rsAdministradores("ANA_NOMBRE") & " " & rsAdministradores("ANA_APELLIDOS") & "</option>")
                        rsAdministradores.movenext()
                    wend
                    %>
                </select>
            </div>
            <p>&nbsp;</p>
            <p><input type="submit" value="<%=objIdioma.getIdioma("Emails_Texto8")%>" /></p>
        </form>
        <%
	end if
    rsTicket.cerrar
	set rsTicket = nothing
	if request.Form("nuevaIncidencia")=1 then
		'enviamos el EMAIL con el historial
		intCodigoPlantillaEmail="29"
		idiomaContacto = 1
		
		emailUsu = request.Form("emailUsuario")

		respPlantilla=obtenerPlantillasEmail(intCodigoPlantillaEmail,idiomaContacto,asuntoMail,cuerpoMail)

		asuntoMail=replace(asuntoMail,"/*ticket*/",ticket)
		asuntoMail=replace(asuntoMail,"/*asuntoTicket*/",asunto)

		cuerpoMail=replace(cuerpoMail,"/*ticket*/",ticket)
		cuerpoMail=replace(cuerpoMail,"/*asuntoTicket*/",asunto)
		cuerpoMail=replace(cuerpoMail,"/*conversacion*/",strConversacion)
		cuerpoMail=replace(cuerpoMail,"/*denominacion*/",denominacion)

		respEnvio=enviarEmailsAPP(asuntoMail,cuerpoMail,"",emailUsu,descripError,"")
	end if%>
    </div>
<script type="text/javascript">
		$(document).ready(function () {
			$('#cerrarConsulta').click(function() {
				if (($('#cerrarConsulta').html())!="Cerrada"){
					$.ajax({
						type: "POST",
						url: "/DMCrm/APP/Sistema/incidencias/cerrarIncidencia.asp",
						data: "ticket=<%=ticket%>&estado=<%=estado%>",
						async: false,
						success: function(datos) {
							$('#estadoTicketAdmin').html("Cerrada");
						},
						error: function(data) {
							alert("error: " + data);
						}
					});	
				}
			});
		});
</script> 
<!--Fin contenido-->
</body>
</html>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->