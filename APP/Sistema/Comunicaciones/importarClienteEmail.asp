<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
<style>
.bloke { width:100%; float:left; margin:0 0 0 -5px; display:block; clear:both; }
.bloke label { width:70px; padding:7px 0; background:#ccc; display:block; float:left; margin:0; text-align:center; border-bottom:3px solid #999; }
.bloke:hover label { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }
.bloke input { margin:0; float:left; padding:7px 6px 6px; background:#eee; border:0; border-bottom:3px solid #ccc; font-size:12px; font-family:Arial, Verdana, sans-serif; color:#666; }
.bloke input#nombreCliente { width:60% !important; }
.bloke input#btnCliente { width:100px !important; border-radius:0; background:#999; color:#fff; border-bottom:3px solid #ccc; padding:7px 8px 6px; float:left; margin:0 4px 0; font-size:12px; font-family:Arial, Verdana, sans-serif; }
.bloke input#btnCliente:hover { background:#e55d39; border-bottom:3px solid #c3411f;  }

table { width:98% !important; float:left; clear:both; margin:10px 1% 0; }
table thead th { position:relative; width:100%; font-size:12px; font-weight:normal; padding:10px 10px 9px; margin:0 0 2px; color:#fff; background:#e55d39; border-bottom:3px solid #c3411f; }
table thead th#thCodigo { width:6%; }
table thead th#thNombre { width:46%; }
table tbody td { font-size:12px; font-family:Arial, Verdana, sans-serif; padding:10px 0; color:#666; }
</style>
</head>
<body>
<!--Inicio contenido-->
<form method="post" id="busquedaClientes">
    <div class="bloke">                         				                                       
		<label id="labelCliente" for="nombreCliente"><%= objIdiomaGeneral.getIdioma("PromocionesSocios_Texto40") %>:</label>
    	<% pintarFiltrosAutoComplementar "CONTACTOS","CON_NOMBRE","nombreCliente", 300 %>
    	<input id="btnCliente" type="button" value="<%=objIdiomaGeneral.getIdioma("BotonBuscar_Texto1")%>" name="botonCliente" />
    </div>
</form>
<table id="tablaClientes">
    <thead>
        <tr>
            <th id="thCodigo"><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto23")%></th>
            <th id="thNombre"><%=objIdiomaGeneral.getIdioma("PromocionesSocios_Texto40")%></th>
            <th id="thMail"><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto10")%></th>
        </tr>
    </thead>
    <tbody id="registrosCliente">
    </tbody>
</table>
<script type="text/javascript">
	
	function asignarCliente(email){
		if(email != ""){
			if(window.opener.document.getElementById("a").value == ""){
				window.opener.document.getElementById("a").value = email;
			}else{
				window.opener.document.getElementById("a").value = window.opener.document.getElementById("a").value + "; " + email;
			}
		}
	}
	
	$(".autocomplete").live("click", function(){ cargarClientes(); });

	$(document).keypress(function(e) {
		if(e.which == 13) {
			cargarClientes();
		}
	});
	$(document).ready(function () {
		$('#btnCliente').click(function() {
			cargarClientes();
		});
	});
	function cargarClientes(){
		$.ajax({
			type: "POST",
			url: "/DMCrm/APP/Sistema/Comunicaciones/listadoClientes.asp",
			data: "nombre=" + $('#nombreCliente').val(),
			async: false,
			success: function(datos) {
				$('#registrosCliente').children( 'tr' ).remove();
				dividirDatos = datos.split("###");
				for(var i = 0; i < dividirDatos.length - 1; i++){
					dividirDatos2 = dividirDatos[i].split("&&&");
					
					tr = document.createElement("tr");
					td1 = document.createElement("td");
					td2 = document.createElement("td");
					td3 = document.createElement("td");
					a = document.createElement("a");
					a.href = "javascript: asignarCliente('" + dividirDatos2[2] + "')";
					a.innerHTML = dividirDatos2[1];
					td1.innerHTML = dividirDatos2[0];
					td3.innerHTML = dividirDatos2[2];
					td2.appendChild(a);
					tr.appendChild(td1);
					tr.appendChild(td2);
					tr.appendChild(td3);
					document.getElementById("registrosCliente").appendChild(tr);
				}
			},
			error: function(data) {
				alert("error: " + data);
			}
		});
	}
</script> 
<!--Fin contenido-->
</body>
</html>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->