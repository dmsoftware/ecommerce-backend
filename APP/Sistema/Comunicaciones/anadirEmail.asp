<!-- #include virtual="/dmcrm/APP/Sistema/Comunicaciones/utf8Decode.asp"-->  
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!-- #include file="xelupload.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
<script>
	function calculaAlturaBtnEnvio(){
		$('#btnEnviar').height($('#metaDatos').height() - 6);
	}
	var i = 1;
	function otroAdjunto(){
		$('#filaAdjuntos').append('<input type="file" id="adjunto'+ i +'" name="adjunto'+ i +'" class="adjunto" /><input type="hidden" id="campo'+ i +'" name="campo'+ i +'" value="0" />');		
		document.getElementById("numeroAdjuntos").value = parseInt(document.getElementById("numeroAdjuntos").value) + 1;
		
		if(i > 1){
			calculaAlturaBtnEnvio();
			$('#btnEnviar a').stop(true, true).animate({
				'top': 25 + (i * 22)
			})
		}
		i++;
	}
	$(document).ready(function(){
		$('#otroAdjunto').click(function(){
			otroAdjunto();
		})
		calculaAlturaBtnEnvio();
	})
	
	function leerAdjuntos(){
		numero = document.getElementById("numeroAdjuntos").value;
		for(i = 0; i <= numero; i++){
			adjunto = document.getElementById("adjunto" + i).value;
			arrayNombres = adjunto.split("/");
			nombreCompleto = arrayNombres(arrayNombre.length - 1);
			nombreCompletoDividido = nombreCompleto.split(".");
			nombre = nombreCompletoDividido[0];
			extension = nombreCompletoDividido[1];
			document.getElementById('formEmail').action = document.getElementById('formEmail').action + "&nombre=" + i + "=" + nombre + "&extension" + i + "=" + extension
		}
	}
</script>
</head>
<body>
<div id="cuerpoSeguimiento">
<!--Inicio contenido-->
	<%
    
    Set rsConfig = connCRM.abrirRecordSet("SELECT CONF_CONSULTAENVIO, CONF_CONSULTAHOST, CONF_CONSULTAUSER, CONF_CONSULTAPASSWORD FROM CONFIGURACION WHERE CONF_CODIGO = 1",3,1)
    emailDefecto = rsConfig("CONF_CONSULTAENVIO")
	denominacionEmail=emailDefecto
	RecomendarHost=replace(rsConfig("CONF_CONSULTAHOST"), "mail", "smtp")
	RecomendarEmail=rsConfig("CONF_CONSULTAENVIO")
	mailUsr = rsConfig("CONF_CONSULTAUSER")
	mailPwd = rsConfig("CONF_CONSULTAPASSWORD")

	if(request.QueryString("enviar") = "ok") then

		Set rsEmail = connCRM.abrirRecordSet("SELECT * FROM COM_EMAILS WHERE COME_CODIGO = " & request.QueryString("idEmail"),3,1)
		
		emailUsu = rsEmail("COME_TO")
			
		emailCc = rsEmail("COME_CC")
			
		emailBcc = rsEmail("COME_CCO")
	
		asuntoEmail=rsEmail("COME_ASUNTO")
	
		cuerpoEmail=rsEmail("COME_MENSAJE")
		
		numAdjuntos=request.QueryString("numAdjuntos")
	
		set objUpload = new xelUpload 
		objUpload.Upload()
		directorio = "/dmcrm/app/sistema/comunicaciones/adj/"
		directorioAdj = "/dmcrm/app/sistema/comunicaciones/mail/attach/"
	
		maxtam = 50000000
		Redim adjunto(numAdjuntos)
		hayAdjunto = false
		
		if objUpload.Ficheros.Count>0 then 
		
			For i=0 to numAdjuntos
			
				if(request.QueryString("campo" & i) = "1") then
		
					set objFich = objUpload.Ficheros("adjunto" & i)
					
					if objFich.Tamano <= maxtam then
						strNombreFichero = objFich.Nombre
						objFich.GuardarComo strNombreFichero, Server.MapPath(directorio)
						''' guardar adjuntos '''
						objFich.GuardarComo request.QueryString("idEmail") & strNombreFichero, Server.MapPath(directorioAdj)
						sql = "INSERT INTO COM_ADJUNTOS ([COMA_RUTA], [COMA_IDEMAILS]) VALUES ('" & request.QueryString("idEmail") & strNombreFichero & "', '" & request.QueryString("idEmail") & "') "
						connCRM.execute sql,numregistrosInsert
						''' fin '''
						strPath = Server.MapPath(".")
						adjunto(i) = strPath & "\adj\" & strNombreFichero
						hayAdjunto = true
					else
						adjunto(i) = null
					end if
					
				else
					
					adjunto(i) = null
					
				end if
				
			Next
		end if
		
		descripError=""
	
		'respEnvio=enviarEmailsAPP(asuntoMail,cuerpoMail,emailDefecto,emailUsu,descripError,adjunto)
		
		''''''''''''''''''''''''''''''''
		MailHost = RecomendarHost              
		MailUsername = mailUsr
		MailPassword = mailPwd			
		MailFrom = RecomendarEmail              
		if trim(nomEmisor)<>"" then
			MailFromName = nomEmisor
		elseif 	not isnull(denominacionEmail) And denominacionEmail<>""  then
			MailFromName = denominacionEmail
		else
			MailFromName = RecomendarEmail		
		end if
		MailAddAddress = emailUsu
		MailSubject = asuntoEmail
		MailBody = cuerpoEmail
		NumError=0
		DescripcionError=""
		MailAddCc = emailCc
		MailAddBcc = emailBcc
		BlnCola=false 'Desactivamos la cola de ASPEMAIL		
		
		on error resume next
		if not hayAdjunto then
			EnvioEmailDM NumError,DescripcionError,MailHost,MailUsername,MailPassword,MailFrom,MailFromName,MailAddAddress,MailAddCc,MailAddBcc,MailSubject,MailBody,BlnCola
		else
			EnvioEmailDMAdjuntos NumError,DescripcionError,MailHost,MailUsername,MailPassword,MailFrom,MailFromName,MailAddAddress,MailAddCc,MailAddBcc,MailSubject,MailBody,BlnCola,adjunto
		end if
		''''''''''''''''''''''''''''''''
		
		Dim FSObj : Set FSObj = Server.CreateObject("Scripting.FileSystemObject")
		For i=0 to j - 1
			FSObj.DeleteFile(adjunto(i))
		Next
	
		response.Write("<script>parent.$.fancybox.close();</script>")
		
	end if
    
	emailCliente = 0
	emailClienteTexto = ""
	if(request.QueryString("idCliente") <> "") then
	    Set rsCliente = connCRM.abrirRecordSet("SELECT CON_EMAIL FROM CONTACTOS WHERE CON_CODIGO = " & request("idCliente"),3,1)
   		emailCliente = rsCliente("CON_CODIGO")
		emailClienteTexto = rsCliente("CON_EMAIL")
	end if
            
    %>
	<% if(request.QueryString("codigo") <> "") then
	
		Set rsTicket = connCRM.abrirRecordSet("SELECT * FROM COM_SEGUIMIENTO LEFT OUTER JOIN COM_EMAILS ON COMS_IDEMAILS = COME_CODIGO WHERE COMS_CODIGO=" & request.QueryString("codigo") & " ORDER BY COMS_CODIGO DESC",3,1)
		if not rsTicket.eof then
			id = rsTicket("COMS_CODIGO")
			'From
			if(rsTicket("COMS_ENVIADO")) then
				if(rsTicket("COMS_IDANALISTA") <> "") then
					Set rsAnalista = connCRM.abrirRecordSet("SELECT * FROM ANALISTAS WHERE ANA_CODIGO=" & rsTicket("COMS_IDANALISTA"),3,1)
					if not rsAnalista.eof then
						de = rsAnalista("ANA_NOMBRE") & " <<a href=""mailto:" & rsAnalista("ANA_EMAIL") & """ title="""">" & rsAnalista("ANA_EMAIL") & "</a>>"
						deRespuesta = rsAnalista("ANA_EMAIL")
					end if
				else
					de = " <a href=""mailto:" & rsTicket("COME_FROM") & """ title="""">" & rsTicket("COME_FROM") & "</a>"
					deRespuesta = rsTicket("COME_FROM")
				end if
			else
				Set rsCliente = connCRM.abrirRecordSet("SELECT * FROM CONTACTOS WHERE CON_CODIGO=" & rsTicket("COMS_IDCLIENTE"),3,1)
				if not rsCliente.eof then
					if rsTicket("COMS_IDTIPO") = 1 then
						de = rsCliente("CON_NOMBRE") & " " & rsCliente("CON_APELLIDO1") & " <<a href=""mailto:" & rsTicket("COME_FROM") & """ title="""">" & rsTicket("COME_FROM") & "</a>>"
					else
						if rsTicket("COMS_IDTIPO") = 2 then
							tel = ""
							if rsCliente("CON_TELEFONO") <> "" then
								tel = "(" & rsCliente("CON_TELEFONO") & ")"
							end if
							de = rsCliente("CON_NOMBRE") & " " & rsCliente("CON_APELLIDO1") & " " & tel
						else
							de = rsCliente("CON_NOMBRE") & " " & rsCliente("CON_APELLIDO1")
						end if
					end if
					deRespuesta = rsTicket("COME_FROM")
				end if
			end if
			if right(deRespuesta, 1) = " " then
				deRespuesta = left(deRespuesta, Len(deRespuesta) - 1)
			end if
			if right(deRespuesta, 1) = ";" then
				deRespuesta = left(deRespuesta, Len(deRespuesta) - 1)
			end if
		
			if(rsTicket("COME_TO") <> "") then
				aquien = split(rsTicket("COME_TO"), ";")
				For Each a In aquien
					 tocccco = tocccco & "<a href=""mailto:" & a & """ title="""">" & a & "</a><br />"
					 if (a <> emailDefecto) then
						 toccccoRespuesta = toccccoRespuesta & a & "; "
					 end if
				Next
			end if
			if(rsTicket("COME_CC") <> "") then
				tocccco = tocccco & "CC:"
				ccquien = split(rsTicket("COME_CC"), ";")
				For Each cc In ccquien
					 tocccco = tocccco & "<a href=""mailto:" & cc & """ title="""">" & cc & "</a><br />"
					 if (cc <> emailDefecto) then
						toccccoRespuesta = toccccoRespuesta & cc & "; "
					 end if
				Next
			end if
			if(rsTicket("COME_CC") <> "") then
				tocccco = tocccco & "CCO:"
				ccoquien = split(rsTicket("COME_CCO"), ";")
				For Each cco In ccoquien
					 tocccco = tocccco & "<a href=""mailto:" & cco & """ title="""">" & cco & "</a><br />"
					 if (cco <> emailDefecto) then
						toccccoRespuesta = toccccoRespuesta & cco & "; "
					 end if
				Next
			end if
			if(toccccoRespuesta = "") then
				tocccco = emailDefecto
			end if
			if right(toccccoRespuesta, 1) = " " then
				toccccoRespuesta = left(toccccoRespuesta, Len(toccccoRespuesta) - 1)
			end if
			if right(toccccoRespuesta, 1) = ";" then
				toccccoRespuesta = left(toccccoRespuesta, Len(toccccoRespuesta) - 1)
			end if
			analista = rsTicket("COMS_IDANALISTA")
			if InStr(rsTicket("COMS_ASUNTO"), "�") > 0 or InStr(rsTicket("COMS_ASUNTO"), "�") > 0 then
				asunto=DecodeUTF8(rsTicket("COMS_ASUNTO"))
			else
				asunto=rsTicket("COMS_ASUNTO")
			end if
			if InStr(rsTicket("COMS_MENSAJE"), "�") > 0 or InStr(rsTicket("COMS_MENSAJE"), "�") > 0 then
				mensaje=DecodeUTF8(rsTicket("COMS_MENSAJE"))
			else
				mensaje=rsTicket("COMS_MENSAJE")
			end if
			fecha = rsTicket("COMS_FECHA")
			nota = rsTicket("COMS_NOTA")
			tratado = rsTicket("COMS_ESTADO")
			idcliente = rsTicket("COMS_IDCLIENTE")
			idemail = rsTicket("COMS_IDEMAILS")
			idcodigo = rsTicket("COMS_CODIGO")
			
			Set rsCliente = connCRM.abrirRecordSet("SELECT CON_NOMBRE, CON_TELEFONO, CON_EMAIL FROM CONTACTOS WHERE CON_CODIGO=" & rsTicket("COMS_IDCLIENTE"),3,1)
			if not rsCliente.eof then
				clienteNombre = rsCliente("CON_NOMBRE")
				clienteTelefono = rsCliente("CON_TELEFONO")
				clienteEmail = rsCliente("CON_EMAIL")
				if clienteEmail = "" then
					Set rsEmails = connCRM.abrirRecordSet("SELECT COMCE_EMAIL FROM COM_CLIENTES_EMAILS WHERE COMCE_IDCLIENTE=" & rsTicket("COMS_IDCLIENTE"),3,1)
					if not rsEmails.eof then
						clienteEmail = rsEmails("COMCE_EMAIL")
					end if
				end if
			end if
		end if %>
    	
	<% end if %>

	<style>
	#cuerpoSeguimiento { width:100%; overflow:hidden; }
	#cabecera { position:relative; width:100%; padding:10px 10px 9px; margin:0 0 2px; color:#fff; background:#e55d39; border-bottom:3px solid #c3411f; }
	#cabecera a { position:absolute; top:0; right:30px; padding:0; border-radius:0; background:none; text-decoration:none; color:#fff; }
	#cabecera a:hover { color:#000; }

	#metaDatos { position:relative; width:100%; overflow:hidden; }
	#metaDatos .fila { position:relative; width:100%; display:block; float:left; clear:both; margin:3px 0; }
	#metaDatos .fila label { width:70px; padding:7px 0; background:#ccc; display:block; float:left; margin:0; text-align:center; border-bottom:3px solid #999; }
	#metaDatos .fila:hover label { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }
	#metaDatos .fila input, #metaDatos .fila textarea, #metaDatos .fila select { width:700px; margin:0; padding:7px 6px 6px; background:#eee; border:0; border-bottom:3px solid #ccc; font-size:12px; font-family:Arial, Verdana, sans-serif; color:#666; }

	#metaDatos #filaA.fila input#a { width:600px; }
	#metaDatos #filaA.fila input#selectA { width:97px; background:#999; color:#fff; border-bottom:3px solid #ccc; }
	#metaDatos #filaA.fila input#selectA:hover { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }

	#metaDatos #filaAdjuntos { min-height:64px; }
	#metaDatos #filaAdjuntos.fila input#otroAdjunto { position:absolute; top:34px; left:30px; width:40px; background:#999; color:#fff; border-bottom:3px solid #ccc; }
	#metaDatos #filaAdjuntos.fila input#otroAdjunto:hover { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }
	#metaDatos #filaAdjuntos.fila input.adjunto { clear:right; /*text-indent:-89px;*/ padding:3px 6px 2px; margin:3px 70px 0; color:#777; }
	#metaDatos #filaAdjuntos.fila input.adjunto#adjunto0 { margin:0; }
	
	#metaDatos #btnEnviar { position:absolute; top:0; left:787px; width:140px; height:1000px; display:block; background:#eee; border-bottom:3px solid #ccc; }
	#metaDatos #btnEnviar:hover { background:#e55d39; border-bottom:3px solid #c3411f; }
	#metaDatos #btnEnviar a { position:absolute; top:60px; left:35px; }

	#editorMensaje > div { width:99.8% !important; background:#fff; border:0 !important; }
	#editorMensaje > div:first-child { margin-top:2px; background:none; }
	#editorMensaje .nicEdit-panelContain { border:0 !important; border-bottom:3px solid #ccc; }
	#editorMensaje .nicEdit-main { width:96% !important; font-size:12px !important; padding:10px 20px 10px 10px; color:#333 !important; }
	#editorMensaje .nicEdit-main body,#editorMensaje .nicEdit-main p, #editorMensaje .nicEdit-main table, #editorMensaje .nicEdit-main  td { font-size:12px !important; color:#666 !important; }
	#editorMensaje .nicEdit-main a { color:#e55d39; text-decoration:none; }
	#editorMensaje .nicEdit-main a:hover { color:#000; }
	#mensaje { width:100%; display:block; float:left; }
	</style>

<form action="?codigo=<% if request.QueryString("codigo") <> "" then response.Write(request.QueryString("codigo")) end if%>&enviar=ok" method="post" id="formEmail" enctype="multipart/form-data">

	<div id="cabecera">
    	<% if(request.QueryString("responder") = "todos") then %>Responder a todos<% elseif(request.QueryString("responder") = "uno") then %>Responder<% else %>Nuevo mensaje<% end if%>
    	<% if(request.QueryString("responder") <> "") then %><a id="volverCabecera" href="verSeguimiento.asp?codigo=<%=request.QueryString("codigo")%>" class="volverSeguimiento" title="">< volver</a><% end if%>
		
	</div>

	<div id="metaDatos">

		<div id="filaDe" class="fila">
			<label id="labelDe" for="de"><%= objIdiomaGeneral.getIdioma("Seguimiento_Texto4") %>:</label>
			<input id="de" type="text" name="de" class="max500px" value="<%= emailDefecto %>" disabled />
		</div>

		<div id="filaA" class="fila">
			<label id="labelA" for="a"><%= objIdiomaGeneral.getIdioma("Seguimiento_Texto5") %>:</label>
	        <input id="a" type="text" name="a"  class="max500px" value="<%=deRespuesta%><% if(request.QueryString("responder") = "todos" and replace(trim(toccccoRespuesta), ";", "") <> "") then response.Write("; " & toccccoRespuesta) end if %>" />
	        <input id="selectA" type="button" onClick="javascript: window.open('importarClienteEmail.asp','popup','width=600,height=400,scrollbars=1');" value="<%=objIdiomaGeneral.getIdioma("Seguimiento_Texto24")%>" />
		</div>

		<div id="filaCc" class="fila">
			<label id="labelCc" for="cc"><%= objIdiomaGeneral.getIdioma("Seguimiento_Texto6") %>:</label>
			<input id="cc" type="text" name="cc" class="max500px" value="" />
		</div>

		<div id="filaCco" class="fila">
			<label id="labelCco" for="cco"><%= objIdiomaGeneral.getIdioma("Seguimiento_Texto7") %>:</label>
			<input id="cco" type="text" name="cco"  class="max500px" value="" />
		</div>

		<div id="filaAdjuntos" class="fila">
			<label id="labelAdjuntos" for="adjuntos"><%= objIdiomaGeneral.getIdioma("Seguimiento_Texto8") %>:</label>
			<input id="otroAdjunto" type="button" value="+" />
			<input id="adjunto0" type="file" name="adjuntos" class="adjunto" />
			<input type="hidden" id="campo0" name="campo0" value="0" />
			<input type="hidden" name="numeroAdjuntos" id="numeroAdjuntos" value="0" />
		</div>

		<div id="filaAsunto" class="fila">
			<label id="labelAsunto" for="asunto"><%= objIdiomaGeneral.getIdioma("Emails_Texto3") %>:</label>
			<input id="asunto" type="text" name="asunto"  class="max600px" value="<% if(request.QueryString("responder") <> "") then response.Write("RE: ") end if %><%=asunto%>" />
		</div>

	    <div id="btnEnviar" class="reply2">
	        <a href="javascript: var nicG = new nicEditors.findEditor('mensaje'); texto = nicG.getContent(); document.getElementById('formEmail').action = document.getElementById('formEmail').action + '&numAdjuntos=' + document.getElementById('numeroAdjuntos').value + '&responder=uno'; insertarMensajeBD(document.getElementById('de').value, document.getElementById('a').value, document.getElementById('cc').value, document.getElementById('cco').value, document.getElementById('asunto').value, texto, <% if idcliente <> "" then response.Write(idcliente) else response.Write(emailCliente) end if %>, <% if request.QueryString("codigo") <> "" then response.Write(request.QueryString("codigo")) else response.Write("0") end if %>);" title="">
	        	
	        	
	        	<img src="/DMCrm/APP/Sistema/Comunicaciones/img/avion.png" class="max5em" alt="Responder" /> 
		     	<% 'if(request.QueryString("responder") = "todos") then %>
		        	<!-- <img src="/dmcrm/APP/Sistema/Comunicaciones/img/replyall.png" class="max5em" alt="Responder" /> -->
		     	<% 'else %>
		        	<!-- <img src="/dmcrm/APP/Sistema/Comunicaciones/img/reply.png" class="max5em" alt="Responder" /> -->
		     	<% 'end if %>
	        </a>
	    </div>

	</div>

	<div id="editorMensaje">
		<textarea id="mensaje" name="mensaje" class="cajaTextoModal"><%=mensaje%></textarea>
	</div>





<script type="text/javascript">
	bkLib.onDomLoaded(function() {
		new nicEditor({fullPanel : true}).panelInstance('mensaje');	
	});

	$(document).ready(function(){
		$('.adjunto').live('change', function(){
			$('#campo'+ $(this).attr('id').replace('adjunto', '')).val(1);
			document.getElementById('formEmail').action = document.getElementById('formEmail').action + "&campo" + $(this).attr('id').replace('adjunto', '') + "=1";
		});
	});
	
	function insertarMensajeBD(de, a, cc, cco, asunto, mensaje, idcliente, idmensaje){
		$.ajax({
			type: "POST",
			url: "insertarMensaje.asp",
			data: {de: de, a: a, cc: cc, cco: cco, asunto: asunto, mensaje: mensaje, idcliente: idcliente, idmensaje: idmensaje},
			success: function(data) 
			{
				$('#formEmail').attr("action", $('#formEmail').attr("action") + "&idEmail=" + data);
				$('#formEmail').submit();
			}
		});
	}
</script> 
</div>
</body>
</html>