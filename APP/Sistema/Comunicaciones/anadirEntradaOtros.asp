<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
</head>
<body>
<div id="cuerpoSeguimiento">
<!--Inicio contenido-->
	<%
    
    Set rsConfig = connCRM.abrirRecordSet("SELECT CONF_CONSULTAENVIO FROM CONFIGURACION WHERE CONF_CODIGO = 1",3,1)
    emailDefecto = rsConfig("CONF_CONSULTAENVIO")
	
	if request.Form("idcliente") <> "" then
		mes = Month(Now())
		if(Month(Now()) < 10) then
			mes = "0" & mes
		end if
		dia = Day(Now())
		if(Day(Now()) < 10) then
			dia = "0" & dia
		end if
		hora = Hour(Now())
		if(Hour(Now()) < 10) then
			hora = "0" & hora
		end if
		min = Minute(Now())
		if(Minute(Now()) < 10) then
			min = "0" & min
		end if
		sec = Second(Now())
		if(Second(Now()) < 10) then
			sec = "0" & sec
		end if
		
		sql = "INSERT INTO COM_SEGUIMIENTO ([COMS_IDCLIENTE], [COMS_ASUNTO], [COMS_MENSAJE], [COMS_FECHA], [COMS_ESTADO], [COMS_IDTIPO], [COMS_ENVIADO], [COMS_CONTACTO]) VALUES ('" & request.Form("idcliente") & "', '" & request.Form("asunto") & "', '" & request.Form("mensaje") & "', '" & Year(Now()) & "-" & mes & "-" & dia & " " & hora & ":" & min & ":" & sec & "', 0, '" & request.Form("tipo") & "', '" & request.Form("enviado") & "', '" & request.Form("contacto") & "') "
		connCRM.execute sql,numregistrosInsert
		
		response.Write("<script>parent.$.fancybox.close();</script>")
		
	else
	
		if request.Form("tipo") <> "" then
	
			response.Write("<p><strong>" & objIdiomaGeneral.getIdioma("Seguimiento_Texto27") & "</strong></p>")
			
		end if
		
	end if
            
    %>
    <style>
    #cuerpoSeguimiento { width:100%; overflow:hidden; }
    .cabeceraEmail { position:relative; width:100%; font-size:12px; font-weight:normal; padding:10px 10px 9px; margin:0 0 2px; color:#fff; background:#e55d39; border-bottom:3px solid #c3411f; }

	.fila { position:relative; width:100%; display:block; float:left; clear:both; margin:3px 0; }
	.fila label { width:70px; padding:7px 0; background:#ccc; display:block; float:left; margin:0; text-align:center; border-bottom:3px solid #999; }
	.bloke:hover label { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }
	.fila input, .fila textarea, .fila select { width:840px; margin:0; padding:7px 6px 6px; background:#eee; border:0; border-bottom:3px solid #ccc; font-size:12px; font-family:Arial, Verdana, sans-serif; color:#666; }
	.fila select { width:300px; margin:0; }

	.fila .bloke { width:100%; float:left; width:auto; margin:3px 3px 0 0; }
	.fila input#buscaCliente { width:40px; background:#999; color:#fff; border-bottom:3px solid #ccc; }
	.fila input#buscaCliente:hover { color:#fff; background:#e55d39; border-bottom:3px solid #c3411f; }
	.fila input#cliente { width:797px; }
	.fila#filaSelects { position:relative; top:5px; }

	#editorMensaje { width:100%; float:left; clear:both; display:block; }
	#editorMensaje > div { width:99.8% !important; background:#fff; border:0 !important; }
	#editorMensaje > div:first-child { margin-top:2px; background:none; }
	#editorMensaje .nicEdit-panelContain { border:0 !important; border-bottom:3px solid #ccc; }
	#editorMensaje .nicEdit-main { width:96% !important; font-size:12px !important; padding:10px 20px 10px 10px; color:#333 !important; }
	#editorMensaje .nicEdit-main body,#editorMensaje .nicEdit-main p, #editorMensaje .nicEdit-main table, #editorMensaje .nicEdit-main  td { font-size:12px !important; color:#666 !important; }
	#editorMensaje .nicEdit-main a { color:#e55d39; text-decoration:none; }
	#editorMensaje .nicEdit-main a:hover { color:#000; }

	#btnGuardar { background:none; border-radius:0; margin:0; padding:0; background:#999; color:#fff; display:block; float:left; padding:8px; margin:0; border:0; font-size:12px; border-bottom:3px solid #ccc; }
	#btnGuardar:hover { background:#e55d39; border-bottom:3px solid #c3411f; color:#fff; }
    </style>
    
    <% if request("accion") = 1 then %>
    	<div class="cabeceraEmail"> <%=objIdiomaGeneral.getIdioma("Seguimiento_Texto31")%></div>
    <% elseif request("accion") = 2 then %>
    	<div class="cabeceraEmail"> <%=objIdiomaGeneral.getIdioma("Seguimiento_Texto33")%></div>
    <% else %>
    	<div class="cabeceraEmail"> <%=objIdiomaGeneral.getIdioma("Seguimiento_Texto28")%></div>
    <% end if %>

    <form action="anadirEntradaTelefono.asp" method="post" id="anadirEntrada">

		<div id="filaSelects" class="fila">

	        <% if request("accion") = 1 then %>
	    		<input type="hidden" name="enviado" value="0" />
	        <% elseif request("accion") = 2 then %>
	    		<input type="hidden" name="enviado" value="1" />
	        <% else %>
		            <select id="selectEnviado" name="enviado">
		                <option value="0"><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto25")%></option>
		                <option value="1"><%=objIdiomaGeneral.getIdioma("Seguimiento_Texto26")%></option>
		            </select>
	        <% end if %>
        
			<% if request("tipoEntrada") = 1 then
				sql = "SELECT * FROM COM_TIPO WHERE COMT_NOMBRE LIKE 'Tel�fono' ORDER BY COMT_NOMBRE ASC"
				Set rsTipos = connCRM.abrirRecordSet(sql, 3, 1)
				response.Write("<input type=""hidden"" name=""tipo"" value=""" & rsTipos("COMT_CODIGO") & """ />")
			else %>
	            <select id="selectTipo" name="tipo">
		            <%
		            sql = "SELECT * FROM COM_TIPO WHERE COMT_NOMBRE NOT LIKE 'Tel�fono' AND COMT_NOMBRE NOT LIKE 'E-mail' ORDER BY COMT_NOMBRE ASC"
		            Set rsTipos = connCRM.abrirRecordSet(sql, 3, 1)
		            while not rsTipos.eof
		                response.Write("<option value=""" & rsTipos("COMT_CODIGO") & """>" & rsTipos("COMT_NOMBRE") & "</option>")
		                rsTipos.movenext()
		            wend
		            %>
	            </select>
	        <% end if %>
    	</div>
        <input type="hidden" name="contacto" />
        <input type="hidden" name="idcliente" id="idcliente" />

        <div id="filaAsunto" class="fila">

        	<div id="blokeAsunto" class="bloke">
        		<label id="labelAsunto" for="asunto"><%=objIdiomaGeneral.getIdioma("Emails_Texto3")%>:</label>
        		<input id="asunto" type="text" name="asunto" />
        	</div>

        	<div id="blokeCliente" class="bloke">
		        <% if (request("idCliente") <> "") then %>
					<input type="hidden" value="<%= request("idCliente") %>" name="cliente" />
				<% else %>
			        <label id="labelCliente" for="cliente"><%= objIdiomaGeneral.getIdioma("Seguimiento_Texto32") %>:</label>
			        <input id="cliente" type="text" name="cliente"  disabled />
			        <input id="buscaCliente" type="button" onClick="javascript:window.open('importarCliente.asp','popup','width=600,height=400');" value="+" />
				<% end if %>
        	</div>



        </div>

		<div id="editorMensaje">
        	<textarea type="text" name="mensaje" id="mensaje"></textarea>
        </div>


        <script type="text/javascript">
			bkLib.onDomLoaded(function() {
				new nicEditor({fullPanel : true}).panelInstance('mensaje');	
			});
		</script> 
        <input id="btnGuardar" type="submit" value="<%=objIdiomaGeneral.getIdioma("Seguimiento_Texto30")%>" />
    </form>
<!--Fin contenido-->
</div>
</body>
</html>