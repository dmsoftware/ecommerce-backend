<style>
	#listadoBuzonCorreo{ width:12%;float:left; position:absolute; }
	#listadoBuzonCorreo span{ font-weight:bold; font-size:1.4em; }
	#listadoBuzonCorreo li{ list-style-type:square; padding:5px 0; }
	#listadoBuzonCorreo li a{ color:#000; text-decoration:none; display:block; }
	#listadoBuzonCorreo li:hover{ background-color:#5893D5;}
	#listadoBuzonCorreo li a:hover{ color:#FFF; }
	#listadoBuzonCorreo .selected_buzon{ background-color:#5893D5;}
	#listadoBuzonCorreo .selected_buzon a{ color:#FFF; }
</style>
<%
sql = "SELECT COME_BUZON FROM COM_EMAILS" &_
    " WHERE COME_BUZON <> 'Sent' AND COME_BUZON <> 'Sent Items' AND COME_BUZON <> 'Elementos enviados' AND COME_BUZON <> 'INBOX'" &_
    " GROUP BY COME_BUZON" &_
	" ORDER BY COME_BUZON ASC"
Set rsBuzones = connCRM.abrirRecordSet(sql, 3, 1)
response.Write("<div id=""listadoBuzonCorreo""><span>BUZONES DE CORREO</span><ul>")
while not rsBuzones.EOF
	if(rsBuzones("COME_BUZON") = request("buzonCorreo")) then
		response.Write("<li class=""selected_buzon""><a href=""?buzonCorreo=" & rsBuzones("COME_BUZON") & """ title="""">" & replace(rsBuzones("COME_BUZON"), "BANDEJA/", "") & "</a></li>")
	else
		response.Write("<li><a href=""?buzonCorreo=" & rsBuzones("COME_BUZON") & """ title="""">" & replace(rsBuzones("COME_BUZON"), "BANDEJA/", "") & "</a></li>")
	end if
	rsBuzones.movenext()
wend
response.Write("</ul></div>")
%>