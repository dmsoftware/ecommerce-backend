<%response.Charset="ISO-8859-1"%>
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Club de compra</title>
		<link rel="stylesheet" href="/dmcrm/css/estilos.css">
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">
        
         

<style type="text/css">
	.tabCorreos {width:100%; padding:0; margin:0; float:left; clear:both; font-size:12px;}	
	.tabCorreos th {  background:#5893D5; border-right:solid 1px white; cursor:default;}		
	.tabCorreos td { font-weight:inherit; color:#555; padding-left:10px;}
	
	
	.modificarDatoSocio {width:100%; margin:0; padding:0; text-align:right; /*border:none;*/}
	.FomulaVacia { border:solid 1px red; }
</style>     

		<script type="text/javascript">
	
		
		
			$(document).ready(function() {
				$('.formulaComercios').blur( function() {
					var idComer=$(this).attr('id');
					var nomComer=$(this).attr('name');					
					var valor=$(this).val();
					if (valor=="") {
						valor="0";
						$(this).val('0');
					}
					$(this).removeClass('FomulaVacia');

					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/Sistema/ConfiguracionPuntosComerModificar.asp",
						data: "idComer=" + idComer + "&valor=" + encodeURIComponent(valor),
						success: function(hayCambios) {
							if (hayCambios=="1") {
								$.jGrowl('La f�rmula del comercio ' + nomComer + ' se ha modificado correctamente.');
							}
						}
					});						
					
				});
				
				
			});						   
		</script>

</head>
<body >



	<form id="formListadoComercios" name="formListadoComercios" runat="server" method="post"  action="ConfiguracionPuntosComer.asp" >
    	<center><h1>F�rmula de conversi�n de dinero a puntos de cada comercios</h1></center>
	    <br/>
        
			<%	sql=" Select * "
				sql=sql & ",(CASE WHEN CAST(D.IDID_DESCRIPCION AS varchar(max))='' Or D.IDID_DESCRIPCION is null THEN TC.CLAC_DESCRIPCION ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as tipoDescrip "
				sql=sql & " From CLUB_COMERCIOS C Inner Join TIPOS_CLASIFICACION_COMERCIOS TC on (C.COMER_SUCLASIFICACION=TC.CLAC_CODIGO) "
				sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (TC.CLAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_CLASIFICACION_COMERCIOS' And D.IDID_CAMPO='CLAC_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "				
				sql=sql & " Order by C.COMER_NOMBRE ASC "
				Set rsComercios = connCRM.AbrirRecordSet(sql,3,1)
				numRegistros=rsComercios.recordcount%>
                <p>
					Edita las f�rmulas de conversi�n de dinero a puntos para cada comercio.<br/>
                </p>
				<p>Se van ha tratar los siguientes <strong><%=numRegistros%></strong> comercios</p>                

                <table  class="tabCorreos" border="0" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th >Nombre</th>
                            <th >Clasificaci�n</th>                            
                            <th >F�rmula(%)</th>
                        </tr>
                    </thead>
                    <tbody>                
				<%while not rsComercios.eof 
							colorFila=""
							if contFila mod 2 = 0 then
								colorFila="bgcolor=""#F1F1F1"""				
							else
								colorFila=""									
							end if
							
							
						%>
							<tr <%=colorFila%> >
								<td >
                                	<%=rsComercios("COMER_NOMBRE")%>
								</td>
								<td style="width:300px;">
                                	<%=rsComercios("tipoDescrip")%>
								</td>
								<td style="width:60px;" >
                                	<%LiuidacionComer=""
									estiloSinFormula=" FomulaVacia"
									if not isnull(rsComercios("COMER_FORMULA_DINERO_A_PUNTOS")) And trim(rsComercios("COMER_FORMULA_DINERO_A_PUNTOS"))<>"" then
										LiuidacionComer=rsComercios("COMER_FORMULA_DINERO_A_PUNTOS")
										estiloSinFormula=""
									end if
									
									%>
                                	<input type="text" class="formulaComercios<%=estiloSinFormula%>"  id="<%=rsComercios("COMER_CODIGO")%>" name="<%=rsComercios("COMER_NOMBRE")%>"  value="<%=LiuidacionComer%>"    />								
								</td>
							</tr>
			
							<%contFila=contFila + 1
							rsComercios.movenext
						wend
           		rsComercios.cerrar()
				set rsComercios=nothing%>                 
            </tbody>
        </table>          
        <br/><br/>          
	</form>

</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->
