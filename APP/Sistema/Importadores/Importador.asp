<%response.buffer=false%>
<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<script type="text/javascript">
    $(document).ready(function() {
        $(".lnkSubidaExcel").fancybox({
            'imageScale': true,
            'zoomOpacity': true,
            'overlayShow': true,
            'overlayOpacity': 0.7,
            'overlayColor': '#333',
            'centerOnScroll': true,
            'zoomSpeedIn': 600,
            'zoomSpeedOut': 500,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'type': 'iframe',
            'frameWidth': 420,
            'frameHeight': 120,
            'titleShow': false
        });
    });
</script>

<h1>Importador de datos de empresas</h1>
<p>Desde aqu� puedes importar empresas directamente mediante un fichero excel y a�adir una acci�n de tipo LLAMADA a la campa�a que selecciones.  Es muy importante que el formato del fichero EXCEL (.xls) que utilices sea similar al siguiente (<a href="ModeloFicheroEmpresas.xls" target=_blank>ver modelo de fichero excel</a>)</p>
Reglas:
<ul>
<li>Respeta la cabecera del fichero .XLS modelo.  No la modifiques.</li>
<li>Los datos referentes a PROVINCIA, PAIS y CATEGOR�A deben existir en las tablas correspondientes. Si no se escriben de forma correcta no se importar�n.</li>
<li>Para saber si una empresa del nuevo listado ya existe en DMGestion comparamos bien por el TEL�FONO o bien por el NOMBRE Y SU LOCALIDAD (deben coincidir ambas)</li>
<li>Si la nueva empresa no existe en DMGesti�n, se insertar�n sus datos</li>
<li>Si la empresa ya existe modificaremos el registro existente seg�n el siguiente criterio: si el cliente NO es cliente actual actualizaremos los datos de DMGesti�n con los nuevos datos, sin embargo si la empresa S� ES CLIENTE ACTUAL mantenemos los datos actuales y modificamos s�lo aquellos que est�n vac�os.</li>
<li>Si deseas a�adir una acci�n a una campa�a son OBLIGATORIOS los cuatro campos reservados a la campa�a: CAMPA�A, PROYECTO, COMERCIAL y la FECHA de la LLAMADA.  Si no deseas crear una acci�n deja dichos campos vac�os.</li>
</ul>


<%'Funci�n que permite comparar con todas las letras de la palabra en dicho orden pero que puedan contener otros caracteres en medio (espacios, comas, puntos, guiones...)
function LimpiezaRegular(palabra)    
    if not isnull(palabra) then
        strAux=""
        'Elimino los caracteres que no me interesan
        palabra=replace(palabra," ","")
        palabra=replace(palabra,",","")
        palabra=replace(palabra,".","")
        palabra=replace(palabra,"/","")
        palabra=replace(palabra,"_","")
        palabra=replace(palabra,"-","")
        palabra=replace(palabra,"|","")
        palabra=replace(palabra,";","")
        palabra=replace(palabra,":","")
        palabra=replace(palabra,"&","")
        palabra=replace(palabra,"(","")
        palabra=replace(palabra,")","")
        palabra=replace(palabra,"[","")
        palabra=replace(palabra,"]","")
        for intI=1 to len(palabra)
            strAux=strAux & "%" & mid(palabra,intI,1)
        next
        'Detras s�lo permito un caracter
        LimpiezaRegular=strAux & "%"
    else
        LimpiezaRegular=""
    end if
end function

'Par�metros
intNumRegistrosATratar=200
intRegistroActual=cint(request("r"))
if intRegistroActual=0 then intRegistroActual=1

'Comprobamos si necesitamos subir el fichero o ya lo tenemos subido
if request("fi")="" then
    'SUBIMOS EL FICHERO%>
    <p>&nbsp;</p><p align=center>
    <span id="divSubidaExcel"><font size=3><b><a class="lnkSubidaExcel" href="upload.asp">SUBIR FICHERO .XLS</a></b></font></span>            
    </p>
<%else
    'Apertura del fichero excel
    Set conexionExcel = Server.CreateObject("ADODB.Connection")
    conexionExcel.Open "DRIVER={Microsoft Excel Driver (*.xls)};DBQ=" & Server.MapPath("./tmp/" & request("fi")  )

    Set rstExcel = Server.CreateObject("ADODB.Recordset") 
    rstExcel.Open "Select * From A1:R10000", conexionExcel,3,3 

    'Movemos el cursor hasta el registro actual
    rstExcel.move intRegistroActual - 1

    'A�adimos el n�mero de pasos que necesitamos
    if int(rstExcel.recordcount/intNumRegistrosATratar) = rstExcel.recordcount/intNumRegistrosATratar then
        intNumPasos = int(rstExcel.recordcount/intNumRegistrosATratar) 
    else
        intNumPasos = int(rstExcel.recordcount/intNumRegistrosATratar) + 1
    end if%>
    <p><Font size=2>El proceso se realizar� en <%=intNumPasos%> Pasos: </Font>
    <%for intI=1 to intNumPasos
        if intRegistroActual = (intI-1)*intNumRegistrosATratar + 1 then
            response.write "<font size=4><B>" & intI & " </B></font>"
        else
            response.write "<font size=2>" & intI & " </font>"
        end if
    next%>
    </p>
    <p>En cada paso se mostrar� la simulaci�n de las acciones a realizar y se pedir� confirmaci�n al usuario para realizarlas.</p>
    <table border=0 cellspacing=1 cellpadding=2>
        <tr>
            <td  bgcolor=555555><font color=ffffff>#</td>
            <%For i = 0 to rstExcel.Fields.Count - 5%>
                <TD  bgcolor=555555><font color=ffffff><%=rstExcel(i).name%></TD>
            <%Next%>
            <TD  bgcolor=cccccc><font color=000000>ACCI�N</TD>
            <%For i = rstExcel.Fields.Count - 4 to rstExcel.Fields.Count - 1%>
                <TD  bgcolor=555555><font color=ffffff><%=rstExcel(i).name%></TD>
            <%Next%>
        </tr>
        <%
        contInsertar=0
        contEditar=0
        if rstExcel.eof then 
            blnFin=true
        else
            blnFin=false
        end if
        cont=intRegistroActual-1
        do while not blnFin
			if isnull(rstExcel("clie_nombre")) then exit do
            cont=cont+1        
            'Comprobamos si el cliente existe o no en la tabla CLIENTES
		    'Busco en CLIENTES si coincide el tel�fono
            'Si no coincide el tel�fono compruebo que el NOMBRE y la LOCALIDAD sean los mismos.
            sql="select * from CLIENTES where CLIE_TELEFONO like '" & LimpiezaRegular(rstExcel("clie_telefono")) & "' and LTRIM(RTRIM(CLIE_TELEFONO))<>'' AND LEN(CLIE_TELEFONO)<=12"
            Set rstBusqueda = connCRM.AbrirRecordset(sql,3,3)
            blnExisteEnCrm=false
            intCoincide=0
		    if not rstBusqueda.eof then        
                blnExisteEnCrm=true
                intCoincide=1
		    else
		        'Compruebo el nombre y la localidad (si la localidad est� rellena)

                if rstExcel("clie_localidad")<>"" then 
                    sql="select * from CLIENTES where CLIE_nombre like '" & LimpiezaRegular(replace(rstExcel("clie_nombre"),"'","")) & "' AND LEN(CLIE_nombre)<=" & LEN(rstExcel("clie_nombre")) + 8 & " and clie_localidad like '" &  LimpiezaRegular(rstExcel("clie_localidad")) & "' AND LEN(clie_localidad)<=" & LEN(rstExcel("clie_localidad")) + 4                  
                else
                    'No est� rellena la localidad
                    sql="select * from CLIENTES where CLIE_nombre like '" & LimpiezaRegular(replace(rstExcel("clie_nombre"),"'","")) & "' AND LEN(CLIE_nombre)<=" & LEN(rstExcel("clie_nombre")) + 8             
                end if
                rstBusqueda.cerrar               
                Set rstBusqueda = connCRM.AbrirRecordset(sql,3,3)
		        if not rstBusqueda.eof then
                    blnExisteEnCrm=true
                    intCoincide=2
                end if
                
            end if%>
            <tr>
                <td style="border-bottom: solid 0px #ccc;"><%=cont%></td>
                <%For i = 0 to rstExcel.Fields.Count - 5%>
                    <TD style="border-bottom: solid 0px #ccc;">
                        <%'Cambio el color de letra si ha coincidido el tel�fono o el nombre y localidad
                        if intCoincide=1 and rstExcel(i).name="CLIE_TELEFONO" then response.write "<font color=ff0000>"
                        if intCoincide=2 and (rstExcel(i).name="CLIE_NOMBRE" OR rstExcel(i).name="CLIE_LOCALIDAD") then response.write "<font color=ff0000>"%>
                        <%response.write rstExcel(i)%>
                    </TD>
                <%Next%>
                <TD style="border-bottom: solid 0px #ccc;">&nbsp;
                    
                </TD>
                <%For i = rstExcel.Fields.Count - 4 to rstExcel.Fields.Count - 1%>
                    <TD style="border-bottom: solid 0px #ccc;">
                        <%response.write rstExcel(i)%>
                    </TD>
                <%Next%>
            </tr>

            <%'Si existe el registro muestro el de DM 
            if blnExisteEnCrm then
                contEditar=contEditar+1%>
                <tr>
                    <td style="border-bottom: solid 2px #ccc;"><FONT COLOR=555555><i>CRM</td>
                    <%For i = 0 to rstExcel.Fields.Count - 5%>
                        <TD style="border-bottom: solid 2px #ccc;">
                            <FONT COLOR=555555>
                            <%'Cambio el color de letra si ha coincidido el tel�fono o el nombre y localidad
                            if intCoincide=1 and rstExcel(i).name="CLIE_TELEFONO" then response.write "<font color=ff0000>"
                            if intCoincide=2 and (rstExcel(i).name="CLIE_NOMBRE" OR rstExcel(i).name="CLIE_LOCALIDAD") then response.write "<font color=ff0000>"%>
                            <i><%=rstBusqueda("" & rstExcel(i).name)%>
                        </TD>
                    <%Next%>
                    <%'compruebo si es un cliente actual
                    if rstBusqueda("clie_clienteactual") then%>
                        <TD style="border-bottom: solid 2px #ccc;">
                            <font color=ff0000>Cliente ACTUAL.<br /><b>MODIFICAR</b> registro existente con los datos no existentes
                        </TD>
                    <%else%>
                        <TD style="border-bottom: solid 2px #ccc;">
                            <font color=ff0000><b>MODIFICAR</b> registro existente
                        </TD>
                    <%end if%>
                    <%For i = rstExcel.Fields.Count - 4 to rstExcel.Fields.Count - 1%>
                        <TD style="border-bottom: solid 2px #ccc;">&nbsp;</TD>
                    <%Next%>
                </tr>
        
            <%else
                contInsertar=contInsertar+1%>
                <tr>
                    <td style="border-bottom: solid 2px #ccc;">&nbsp;</td>
                    <%For i = 0 to rstExcel.Fields.Count - 5%>
                        <TD style="border-bottom: solid 2px #ccc;">&nbsp;
                            
                        </TD>
                    <%Next%>
                    <TD style="border-bottom: solid 2px #ccc;"><font color=ff0000><b>INSERTAR</b> registro</TD>
                    <%For i = rstExcel.Fields.Count - 4 to rstExcel.Fields.Count - 1%>
                        <TD style="border-bottom: solid 2px #ccc;">&nbsp;</TD>
                    <%Next%>
                </tr>

            <%end if%>

            <%'**************************************************************************************
            'Realizamos las acciones necesarias
            '****************************************************************************************
            if request("action")="si" then%>
                <tr bgcolor=cccccc>
                <%'1/ Insertar el registro **********************************************************
                if not blnExisteEnCrm then
			        rstBusqueda.ADDNEW
                    'Hallamos el c�digo del nuevo clinete
                    sql="select max(clie_codigo) from clientes"
                    Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                    intCodigo=rstAux(0)+1
                    rstAux.cerrar
                    rstBusqueda("clie_codigo")=intCodigo

			        rstBusqueda("clie_nombre")=trim(ltrim(rstExcel("clie_nombre")))
			        rstBusqueda("clie_telefono")=trim(ltrim(rstExcel("clie_telefono")))
                    rstBusqueda("clie_calidadregistro")=2 'No confirmado
			        rstBusqueda("clie_direccion")=trim(ltrim(rstExcel("clie_direccion")))
                    rstBusqueda("clie_localidad")=trim(ltrim(rstExcel("clie_localidad")))
			        rstBusqueda("clie_cp")=trim(ltrim(rstExcel("clie_cp")))	
                    Response.write "<td colspan=5 style=""border-bottom: solid 2px #333;""><b>REGISTRO INSERTADO</td>"

                    'Hallamos la provincia	
                    if  rstExcel("clie_provincia")<>"" then
                        sql="select * from PROVINCIAS where PROV_nombre='" & rstExcel("clie_provincia") & "'"
                        Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                        if not rstAux.eof then
			                rstBusqueda("clie_provincia")=rstAux("PROV_codigo")
                            Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("PROV_codigo") & "</td>"
                        else
                            Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("clie_provincia") & " NO EXISTE</td>"
                        end if
                        rstAux.cerrar
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
                    end if

                    'Hallamos el pa�s	
                    if  rstExcel("clie_pais")<>"" then
                        sql="select * from PAISES where pai_nombre='" & rstExcel("clie_pais") & "'"
                        Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                        if not rstAux.eof then
			                rstBusqueda("clie_pais")=rstAux("pai_codigo")
                            Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("pai_codigo") & "</td>"
                        else
                            Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("clie_pais") & " NO EXISTE</td>"
                        end if
                        rstAux.cerrar
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
                    end if

			        rstBusqueda("clie_fax")=trim(ltrim(replace(rstExcel("clie_fax")," ","")))
			        rstBusqueda("clie_email")=trim(ltrim(rstExcel("clie_email")))
			        rstBusqueda("clie_url")=trim(ltrim(rstExcel("clie_url")))
                    if isNumeric(trim(ltrim(rstExcel("clie_numtrabajadores")))) then rstBusqueda("clie_numtrabajadores")=trim(ltrim(rstExcel("clie_numtrabajadores")))						
                    rstBusqueda("clie_fuente")=rstExcel("clie_fuente") 
                    rstBusqueda("clie_actividad")=left(trim(ltrim(rstExcel("clie_actividad"))),255)    
                    Response.write "<td colspan=5 style=""border-bottom: solid 2px #333;"">&nbsp;</td>"

                    'Hallamos la categoria
                    if  rstExcel("clie_categoria1")<>"" then	
                        sql="select * from TIPOS_CATEGORIAS where CATEG_DESCRIPCION='" & rstExcel("clie_categoria1") & "'"
                        Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                        if not rstAux.eof then
			                rstBusqueda("clie_categoria1")=rstAux("CATEG_CODIGO")
                            Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("categ_codigo") & "</td>"
                        else
                            Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("clie_categoria1") & " NO EXISTE</td>"
                        end if
                        rstAux.cerrar
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
			        end if

                    'ponemos la fecha de alta y la de modificacion
                    rstBusqueda("clie_fechaalta")=FormatearFechaSQLSever(date)
                    rstBusqueda("clie_fechamodificacion")=FormatearFechaSQLSever(date)
                    Response.write "<td colspan=2 style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
                 
			        rstBusqueda.UPDATE

                else
                '2/ Modificar el registro **********************************************************
                ' Ponemos siempre los datos nuevos sobre los viejos si el cliente no es actual.  Si es actual mantenemos los datos actuales y rellenamos s�lo aquellos que est�n vac�os.

                    'Distinguimos si el registro es ya cliente actual o no
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_telefono")="" or isnull(rstBusqueda("clie_telefono"))) and rstExcel("clie_telefono")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_telefono")<>"" ) then
                        'Si el tel�fono est� confirmado no lo cambiamos
                        if rstBusqueda("clie_calidadregistro")<>1 then
                            rstBusqueda("clie_telefono")=trim(ltrim(rstExcel("clie_telefono")))
                            'Ponemos la calidad del registro a "no confirmado"
                            rstBusqueda("clie_calidadregistro")=2
                        end if
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_nombre")="" or isnull(rstBusqueda("clie_nombre"))) and rstExcel("clie_nombre")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_nombre")<>"" ) then
                        rstBusqueda("clie_nombre")=trim(ltrim(rstExcel("clie_nombre")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_direccion")="" or isnull(rstBusqueda("clie_direccion"))) and rstExcel("clie_direccion")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_direccion")<>"" ) then
                        rstBusqueda("clie_direccion")=trim(ltrim(rstExcel("clie_direccion")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_localidad")="" or isnull(rstBusqueda("clie_localidad"))) and rstExcel("clie_localidad")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_localidad")<>"" ) then
                        rstBusqueda("clie_localidad")=trim(ltrim(rstExcel("clie_localidad")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_cp")="" or isnull(rstBusqueda("clie_cp"))) and rstExcel("clie_cp")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_cp")<>"" ) then
                        rstBusqueda("clie_cp")=trim(ltrim(rstExcel("clie_cp")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_fax")="" or isnull(rstBusqueda("clie_fax"))) and rstExcel("clie_fax")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_fax")<>"" ) then
                        rstBusqueda("clie_fax")=trim(ltrim(rstExcel("clie_fax")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_email")="" or isnull(rstBusqueda("clie_email"))) and rstExcel("clie_email")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_email")<>"" ) then
                        rstBusqueda("clie_email")=trim(ltrim(rstExcel("clie_email")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_url")="" or isnull(rstBusqueda("clie_url"))) and rstExcel("clie_url")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_url")<>"" ) then
                        rstBusqueda("clie_url")=trim(ltrim(rstExcel("clie_url")))
                    end if
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_numtrabajadores")="" or isnull(rstBusqueda("clie_numtrabajadores"))) and rstExcel("clie_numtrabajadores")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_numtrabajadores")<>"" ) then
                        if isNumeric(trim(ltrim(rstExcel("clie_numtrabajadores")))) then rstBusqueda("clie_numtrabajadores")=trim(ltrim(rstExcel("clie_numtrabajadores")))
                    end if

                    Response.write "<td colspan=5 style=""border-bottom: solid 2px #333;""><b>REGISTRO MODIFICADO</td>"
                               	
                    'Hallamos la provincia	
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_provincia")="" or isnull(rstBusqueda("clie_provincia"))) and rstExcel("clie_provincia")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_provincia")<>"" ) then
                        sql="select * from PROVINCIAS where PROV_nombre='" & rstExcel("clie_provincia") & "'"
                        Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                        if not rstAux.eof then
			                rstBusqueda("clie_provincia")=trim(ltrim(rstAux("PROV_codigo")))
                            Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("PROV_codigo") & "</td>"
                        else
                            Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("clie_provincia") & " NO EXISTE</td>"
                        end if
                        rstAux.cerrar
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
                    end if

                    'Hallamos el pa�s	
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_pais")="" or isnull(rstBusqueda("clie_pais"))) and rstExcel("clie_pais")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_pais")<>"" ) then
                        sql="select * from PAISES where pai_nombre='" & trim(ltrim(rstExcel("clie_pais"))) & "'"
                        Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                        if not rstAux.eof then
			                rstBusqueda("clie_pais")=trim(ltrim(rstAux("pai_codigo")))
                            Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("pai_codigo") & "</td>"
                        else
                            Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("clie_pais") & " NO EXISTE</td>"
                        end if
                        rstAux.cerrar
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
                    end if     

                    Response.write "<td colspan=5 style=""border-bottom: solid 2px #333;"">&nbsp;</td>"

                    'Hallamos la categoria
                    if ( rstBusqueda("clie_clienteactual") and (rstBusqueda("clie_categoria1")="" or isnull(rstBusqueda("clie_categoria1"))) and rstExcel("clie_categoria1")<>"" ) or ( not rstBusqueda("clie_clienteactual") and rstExcel("clie_categoria1")<>"" ) then	
                        sql="select * from TIPOS_CATEGORIAS where CATEG_DESCRIPCION='" & rstExcel("clie_categoria1") & "'"
                        Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                        if not rstAux.eof then
			                rstBusqueda("clie_categoria1")=trim(ltrim(rstAux("CATEG_CODIGO")))
                            Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("categ_codigo") & "</td>"
                        else
                            Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("clie_categoria1") & " NO EXISTE</td>"
                        end if
                        rstAux.cerrar
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"
			        end if
                
                    'Fuente
                    if rstExcel("clie_fuente")<>"" then rstBusqueda("clie_fuente")=trim(ltrim(rstExcel("clie_fuente"))) 

                    'Actividad, Si existe Actividad el CRM lo concateno
                    if trim(ltrim(rstExcel("clie_ACTIVIDAD")))<>"" then 
                        if rstBusqueda("clie_ACTIVIDAD")="" then
                            rstBusqueda("clie_ACTIVIDAD")=left(trim(ltrim(rstExcel("clie_ACTIVIDAD"))),255)
                        else
                            rstBusqueda("clie_ACTIVIDAD")=left(rstBusqueda("clie_ACTIVIDAD") & " | " & trim(ltrim(rstExcel("clie_ACTIVIDAD"))),255)
                        end if
                    end if

                    'ponemos la fecha de modificacion
                    rstBusqueda("clie_fechamodificacion")=FormatearFechaSQLSever(date) 

                    Response.write "<td colspan=2 style=""border-bottom: solid 2px #333;"">&nbsp;</td>"

                    rstBusqueda.UPDATE

                end if
        

                '3/ Crear la acci�n a la campa�a correspondiente 
                'Comprobamos que en el excel se haya rellenado
                if rstExcel("CAMPA�A")<>"" then

                    'Hallamos la campa�a                
                    sql="select * from CAMPANAS where CAMP_NOMBRE='" & rstExcel("CAMPA�A") & "'"
                    Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                    if not rstAux.eof then
			            intCampana = rstAux("CAMP_CODIGO")
                        Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("CAMP_CODIGO") & "</td>"
                        blnInsertarAccion=true
                    else
                        Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("CAMPA�A") & " NO EXISTE</td>"
                        blnInsertarAccion=false
                    end if
                    rstAux.cerrar
                
                    'S�LO SI EXISTE LA CAMPA�A
                    if blnInsertarAccion then
                        'Abrimos la tabla de acciones para insertarlas
                        sql="select * from acciones_cliente"
                        sql=sql & " where ACCI_SUCAMPANA=" & intCampana & " and ACCI_PROYECTO='" & rstExcel("PROYECTO") & "' and ACCI_SUCLIENTE=" & rstBusqueda("CLIE_CODIGO") & " and ACCI_SUTIPO=1"
                        Set rstAcciones= connCRM.AbrirRecordset(sql,3,3)
                        if rstAcciones.eof then
		                    rstAcciones.addnew
		                    'Comprobamos que para la campa�a,proyecto y cliente no tengamos ya una 
                            rstAcciones("acci_fecha") = FormatearFechaSQLSever(rstExcel("FECHA LLAMADA"))
                            rstAcciones("acci_fechamodificacion") = FormatearFechaSQLSever(date)
		                    rstAcciones("acci_sutipo") = 1 'llamada

                            Response.write "<td style=""border-bottom: solid 2px #333;""><B>ACCION INSERTADA</td>"

                            'Hallamos el comercial                
                            sql="select * from ANALISTAS where ANA_NOMBRE='" & rstExcel("COMERCIAL") & "'"
                            Set rstAux = connCRM.AbrirRecordset(sql,0,1)
                            if not rstAux.eof then
			                    rstAcciones("acci_sucomercial")=rstAux("ANA_CODIGO")
                                Response.write "<td style=""border-bottom: solid 2px #333;"">" & rstAux("ANA_CODIGO") & "</td>"
                            else
                                Response.write "<td style=""border-bottom: solid 2px #333;""><font color=ff0000>" & rstExcel("COMERCIAL") & " NO EXISTE</td>"
                            end if
                            rstAux.cerrar

                            Response.write "<td style=""border-bottom: solid 2px #333;"">&nbsp;</td>"

			                rstAcciones("acci_sucampana")=intCampana
		                    rstAcciones("acci_proyecto") = rstExcel("PROYECTO")
                            rstAcciones("acci_sucliente") = rstBusqueda("CLIE_CODIGO")
		                    rstAcciones("acci_terminada") = false	
		                    rstAcciones.update
                        else
                            Response.write "<td colspan=3 style=""border-bottom: solid 2px #333;""><color=ff0000>YA EXISTE ACCION PARA ESTA CAMPA�A Y PROYECTO</td>"
                        end if
                        rstAcciones.cerrar
                    else
                        Response.write "<td colspan=3 style=""border-bottom: solid 2px #333;""><color=ff0000>NO SE INSERTA ACCI�N</td>"
                    end if
                else
                    Response.write "<td colspan=4 style=""border-bottom: solid 2px #333;""><color=ff0000>NO SE INSERTA ACCI�N</td>"
                end if%>
                </tr>
            <%end if
             '**************************************************************************************
            rstBusqueda.cerrar
            rstExcel.movenext
            intRegistro=intRegistro+1
            if (intRegistro=intNumRegistrosATratar) or rstExcel.eof then blnFin=true
        loop
      '  if request("action")="si" then rstAcciones.cerrar%>
    </table>

    <br />
    Registros a insertar: <b><%=contInsertar%></b><br />
    Registros a modificar: <b><%=contEditar%></b><br />

    <%'Si estamos en etapa de previsi�n mostramos la etapa de escritura, y viceversa
    if request("action")<>"si" then%>
        <p ALIGN=CENTER><font size=3>
        <b>Si desea confirmar la importaci�n de los datos clicke <a href="Importador.asp?action=si&r=<%=intRegistroActual%>&fi=<%=request("fi")%>">aqu�</a></b></font>
        </p>
    <%else
        'Comprobamos si ya hemos terminado los registros o no
        if (intRegistroActual + intRegistro -1 ) < rstExcel.recordcount then%>
            <p ALIGN=CENTER><font size=3>
            <b>Para realizar el siguiente paso clicka <a href="Importador.asp?r=<%=intRegistroActual+intNumRegistrosATratar%>&fi=<%=request("fi")%>">aqu�</a></b></font>
            </p>
        <%else%>
            <p ALIGN=CENTER><font size=3>
            <b>IMPORTACI�N FINALIZADA</b></font>
            </p>
        <%end if%>
    <%end if%>

<%
rstExcel.close
set rstExcel=nothing
conexionExcel.close
set conexionExcel=nothing
%>

<%end if 'Si existe el fichero subido%>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->

