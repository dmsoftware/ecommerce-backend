
<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->


<%
Sub FormatoEuros(valor)
 response.Write FormatNumber(valor, 2) & " �"
end sub
%>


<style type="text/css">
<!--
.sca {
	float: right;
}
-->
</style>

<script type="text/javascript">
    /***** Funci�n para generar un id aleatorio para evitar la cache *****/
    function generarGUID() {
        var result, i, j;
        result = '';
        for (j = 0; j < 32; j++) {
            if (j == 8 || j == 12 || j == 16 || j == 20)
                result = result + '-';
            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
            result = result + i;
        }
        return result;
    }
	
	
	$(".cEmergente").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '98%',
						'frameHeight'			: '98%'
					}).trigger("click"); 
					return false; 	
	});
	
	$(".cEmergentePequeno").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '30%',
						'frameHeight'			: '30%',
						'titleShow'		        : false					
					}).trigger("click"); 
					return false; 	
				});	
	
    $(document).ready(function () {

        oTable = $('#tablaListado').dataTable({
            "oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
            "bAutoWidth": false,
            "sPaginationType": "full_numbers",
            "bFilter": true,
            "bProcessing": true,
            "bSort": true,
            "iDisplayLength": 10,
            "aoColumns": [
				null,
				null,
                {"bSortable": false}
				]

        });
		
    });
</script>



<h1><%=objIdioma.getIdioma("ResetearContrasenas_Titulo")%></h1>
<%=objIdioma.getIdioma("ResetearContrasenas_Texto1")%>
<br><br />


<div id="contenedorSeleccion">
    <table id="tablaListado" name="tablaListado" width="100%" cellpadding=3 cellspacing=1 bgcolor=ffffff>
        <thead> 
        <tr class="titularT">
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResetearContrasenas_Texto2")%></th>
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResetearContrasenas_Texto3")%></th>
            <th valign="middle" id="celda1" style="cursor:default;">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <%
        sql="Select ana_codigo,ana_nombre,ana_email "
        sql=sql & " from analistas where ana_email<>''"
        sql=sql & " order by ana_nombre "
        Set rstAnalistas= connCRM.AbrirRecordset(sql,0,1)
    
        while not rstAnalistas.eof
            cont=cont+1%>
            <tr class="celdaBlanca">
                <td><%=rstAnalistas("ana_nombre")%></td>
                <td><%=rstAnalistas("ana_email")%></td>
                <td><a class="cEmergentePequeno" href="/dmcrm/APP/sistema/ResetearContrasenaUsuario.asp?cod=<%=rstAnalistas("ana_codigo")%>"><img src="/dmcrm/APP/Comun2/imagenes/password.png" title="<%=objIdioma.getIdioma("ResetearContrasenas_Texto4")%>" /></a></td>
                
            </tr>
            <%rstAnalistas.movenext()%>
        
        <%wend
        rstAnalistas.cerrar%>
        </tbody>
    </TABLE>
</div>





<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->

