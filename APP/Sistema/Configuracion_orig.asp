<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%'tabla="configuracion"%>
<!-- include virtual="/dmcrm/app/comun2/seleccion.asp"-->
 
<style type="text/css">
.bloqueColumna { margin-top:10px;}
.filtro { margin:0;}

#capaTieneRegalos { float:right;}

.lnkPuntosComer { margin-left:10px; text-decoration:none; color:#FF6600;  }
.lnkPuntosComer:hover { color:#CDCDCD; }

#validComerSinFormula {border:dashed 1px #CDCDCD; background:red; width:auto; width:300px; padding:2px 0 2px 10px; margin-left:10px; color:#fff;}
</style>


<h1><%=objIdioma.getIdioma("Configuracion_Titulo")%></h1>

			<script type="text/javascript" language="javascript" >
			  $(document).ready(function() {
					$(".lnkPuntosComer").live('click', function(){
						$(this).fancybox({
							'imageScale'			: true,
							'zoomOpacity'			: true,
							'overlayShow'			: true,
							'overlayOpacity'		: 0.7,
							'overlayColor'			: '#333',
							'centerOnScroll'		: true,
							'zoomSpeedIn'			: 600,
							'zoomSpeedOut'			: 500,
							'transitionIn'			: 'elastic',
							'transitionOut'			: 'elastic',
							'type'					: 'iframe',
							'frameWidth'			: '60%',
							'frameHeight'			: '80%',
							'titleShow'				: false	,
							'onClosed'		        : function() {
									//recargarAlCerrar();
							}											
						}).trigger("click"); 
						return false; 	
					});						
					
			   });
			</script>

<%if request("msg")="1" then%>

			<script type="text/javascript" language="javascript" >
			  $(document).ready(function() {
					$.jGrowl("<%=objIdiomaGeneral.getIdioma("GuardadoCorrectamente_Texto1")%>");
					
			   });
			</script>


<%end if%>

                    

	<form id="frmConf" name="frmConf" method="post" action="configuracion.asp?gu=1" >


		<%if request("gu")="1" then

			sql=" Update CONFIGURACION "
			sql=sql & " Set CONF_DOMINIOSITIOWEB='" & request.Form("CONF_DOMINIOSITIOWEB") & "' "
			valorBooleano="0"
			if request.Form("CONF_CONDMCORPORATIVE")="on" then
				valorBooleano="1"
			end if
			sql=sql & ", CONF_CONDMCORPORATIVE=" & valorBooleano

			sql=sql & ",CONF_PAGINICIO='" & trim(request.Form("CONF_PAGINICIO")) & "' "			
			sql=sql & ",CONF_PAGINICIO_DIRECTORCUENTA='" & trim(request.Form("CONF_PAGINICIO_DIRECTORCUENTA")) & "' "			
			sql=sql & ",CONF_PAGINICIO_COMERCIOS='" & trim(request.Form("CONF_PAGINICIO_COMERCIOS")) & "' "									

			if trim(request.Form("CONF_OBJETOCORREO"))<>"" then
				sql=sql & ", CONF_OBJETOCORREO=" & trim(request.Form("CONF_OBJETOCORREO"))
			end if
			if trim(request.Form("CONF_EMAILHOST"))<>"" then			
				sql=sql & ",CONF_EMAILHOST='" & trim(request.Form("CONF_EMAILHOST")) & "' "
			end if
			if trim(request.Form("CONF_EMAILENVIO"))<>"" then			
				sql=sql & ",CONF_EMAILENVIO='" & trim(request.Form("CONF_EMAILENVIO")) & "' "
			end if
			if trim(request.Form("CONF_EMAILUSER"))<>"" then			
				sql=sql & ",CONF_EMAILUSER='" & trim(request.Form("CONF_EMAILUSER")) & "' "
			end if
			if trim(request.Form("CONF_EMAILPASSWORD"))<>"" then			
				sql=sql & ",CONF_EMAILPASSWORD='" & trim(request.Form("CONF_EMAILPASSWORD")) & "' "
			end if				
			if trim(request.Form("CONF_CLIENTEA"))<>"" then			
				sql=sql & ",CONF_CLIENTEA='" & trim(request.Form("CONF_CLIENTEA")) & "' "
			else
				sql=sql & ",CONF_CLIENTEA=0"
			end if		
			if trim(request.Form("CONF_CLIENTEB"))<>"" then			
				sql=sql & ",CONF_CLIENTEB='" & trim(request.Form("CONF_CLIENTEB")) & "' "
			else
				sql=sql & ",CONF_CLIENTEB=0"
			end if	
			valorBooleano="0"
			if request.Form("CONF_CONCLUBCOMPRA")="on" then
				valorBooleano="1"
			end if
			sql=sql & ", CONF_CONCLUBCOMPRA=" & valorBooleano	
			if trim(request.Form("CONF_NCUENTAPAGOS"))<>"" then			
				sql=sql & ",CONF_NCUENTAPAGOS='" & trim(request.Form("CONF_NCUENTAPAGOS")) & "' "
			else
				sql=sql & ",CONF_NCUENTAPAGOS=NULL"
			end if	
			valorBooleano="0"
			if request.Form("CONF_CONIVAINCLUIDO")="on" then
				valorBooleano="1"
			end if		
			if trim(request.Form("CONF_FORMULALIQUIDACION"))<>"" then			
				sql=sql & ",CONF_FORMULALIQUIDACION='" & trim(request.Form("CONF_FORMULALIQUIDACION")) & "' "
			else
				sql=sql & ",CONF_FORMULALIQUIDACION=NULL"
			end if		
			

			if trim(request.Form("CONF_FORMULA_PUNTOSADINERO"))<>"" then			
				sql=sql & ",CONF_FORMULA_PUNTOSADINERO='" & trim(request.Form("CONF_FORMULA_PUNTOSADINERO")) & "' "
			else
				sql=sql & ",CONF_FORMULA_PUNTOSADINERO=NULL"
			end if					
			
			if trim(request.Form("CONF_GASTOS_DOM"))<>"" then			
				sql=sql & ",CONF_GASTOS_DOM='" & trim(request.Form("CONF_GASTOS_DOM")) & "' "
			end if	
			if trim(request.Form("CONF_GASTOS_ESTAB"))<>"" then			
				sql=sql & ",CONF_GASTOS_ESTAB='" & trim(request.Form("CONF_GASTOS_ESTAB")) & "' "
			end if


			sql=sql & ", CONF_ENVIO_DOMICILIO=" & trim(request.Form("CONF_ENVIO_DOMICILIO"))

			''Jon: temporalmente no updateamos el campo para que sea true
			'sql=sql & ", CONF_PERMITE_VALES=" & trim(request.Form("CONF_PERMITE_VALES"))
			if trim(request.Form("CONF_SALDO_EMITE_VALES"))<>"" then			
'				sql=sql & ",CONF_SALDO_EMITE_VALES='" & replace(trim(request.Form("CONF_SALDO_EMITE_VALES")),",",".") & "' "
			else
'				sql=sql & ",CONF_SALDO_EMITE_VALES=NULL"
			end if				
			if trim(request.Form("CONF_NUMDIAS_EMITE_VALES"))<>"" then			
'				sql=sql & ",CONF_NUMDIAS_EMITE_VALES=" & trim(request.Form("CONF_NUMDIAS_EMITE_VALES")) 
			else
'				sql=sql & ",CONF_NUMDIAS_EMITE_VALES=0"
			end if							
			if trim(request.Form("CONF_CADUCIDAD_VALES"))<>"" then			
'				sql=sql & ",CONF_CADUCIDAD_VALES=" & trim(request.Form("CONF_CADUCIDAD_VALES")) 
			else
'				sql=sql & ",CONF_CADUCIDAD_VALES=0"
			end if	
			'''''''''''''''
			
			valorBooleano="0"
			if request.Form("CONF_PERMITE_REDENCION")="on" then
				valorBooleano="1"
			end if									
			sql=sql & ", CONF_PERMITE_REDENCION=" & valorBooleano							
			valorBooleano="0"
			if request.Form("CONF_PERMITE_REGALOS")="on" then
				valorBooleano="1"
			end if									
			sql=sql & ", CONF_PERMITE_REGALOS=" & valorBooleano	
			if trim(request.Form("CONF_COSTESDEVOLUCION"))<>"" then			
				sql=sql & ",CONF_COSTESDEVOLUCION='" & replace(trim(request.Form("CONF_COSTESDEVOLUCION")),",",".") & "' "
			else
				sql=sql & ",CONF_COSTESDEVOLUCION=NULL"
			end if		
			if trim(request.Form("CONF_IDPRODUCTO_GENVIO"))<>"" then			
				sql=sql & ",CONF_IDPRODUCTO_GENVIO='" & trim(request.Form("CONF_IDPRODUCTO_GENVIO")) & "' "
			else
				sql=sql & ",CONF_IDPRODUCTO_GENVIO=NULL"
			end if	
			if trim(request.Form("CONF_IDPRODUCTO_DESCUENTOS"))<>"" then			
				sql=sql & ",CONF_IDPRODUCTO_DESCUENTOS='" & trim(request.Form("CONF_IDPRODUCTO_DESCUENTOS")) & "' "
			else
				sql=sql & ",CONF_IDPRODUCTO_DESCUENTOS=NULL"
			end if													
			if trim(request.Form("CONF_FINICIO_ESTADISTICAS"))<>"" then			
				sql=sql & ",CONF_FINICIO_ESTADISTICAS='" & FormatearFechaSQLSever(trim(request.Form("CONF_FINICIO_ESTADISTICAS"))) & "' "
			else
				sql=sql & ",CONF_FINICIO_ESTADISTICAS=NULL"
			end if					
			if trim(request.Form("CONF_CADUCIDAD_PUNTOS"))<>"" then			
				sql=sql & ",CONF_CADUCIDAD_PUNTOS=" & trim(request.Form("CONF_CADUCIDAD_PUNTOS")) 
			else
				sql=sql & ",CONF_CADUCIDAD_PUNTOS=NULL"
			end if				
			
			sql=sql & " Where CONF_CODIGO=" & trim(request.Form("txtCod"))


		  	connCRM.execute sql,intNumeroRegistros			

			response.Redirect("/DMCrm/APP/sistema/configuracion.asp?msg=1")


		else 'else de if request("gu")="1" then 

		
			sql="Select * From CONFIGURACION "
            Set rsDatos = connCRM.AbrirRecordSet(sql,0,1)
            if not rsDatos.eof then
                CONF_CODIGO=rsDatos("CONF_CODIGO")
                CONF_DOMINIOSITIOWEB=rsDatos("CONF_DOMINIOSITIOWEB")		
                CONF_CONDMCORPORATIVE=rsDatos("CONF_CONDMCORPORATIVE")		
                CONF_OBJETOCORREO=rsDatos("CONF_OBJETOCORREO")		
                CONF_EMAILHOST=rsDatos("CONF_EMAILHOST")		
                CONF_EMAILENVIO=rsDatos("CONF_EMAILENVIO")		
                CONF_EMAILUSER=rsDatos("CONF_EMAILUSER")		
                CONF_EMAILPASSWORD=rsDatos("CONF_EMAILPASSWORD")		
                CONF_CLIENTEA=rsDatos("CONF_CLIENTEA")		
                CONF_CLIENTEB=rsDatos("CONF_CLIENTEB")		
                CONF_CONCLUBCOMPRA=rsDatos("CONF_CONCLUBCOMPRA")	
                CONF_NCUENTAPAGOS=rsDatos("CONF_NCUENTAPAGOS")		
                CONF_CONIVAINCLUIDO=rsDatos("CONF_CONIVAINCLUIDO")		
                CONF_FORMULALIQUIDACION=rsDatos("CONF_FORMULALIQUIDACION")
                CONF_FORMULA_PUNTOSADINERO=rsDatos("CONF_FORMULA_PUNTOSADINERO")
                
				''Jon: temporalmente ponemos a FALSE el campo, porque es true para pruebas
                CONF_PERMITE_VALES=false'rsDatos("CONF_PERMITE_VALES")
                CONF_SALDO_EMITE_VALES=rsDatos("CONF_SALDO_EMITE_VALES")		
                CONF_NUMDIAS_EMITE_VALES=rsDatos("CONF_NUMDIAS_EMITE_VALES")		
                CONF_CADUCIDAD_VALES=rsDatos("CONF_CADUCIDAD_VALES")		
				''''''''''''''
								
				CONF_ENVIO_DOMICILIO=rsDatos("CONF_ENVIO_DOMICILIO")
				CONF_GASTOS_DOM=rsDatos("CONF_GASTOS_DOM")
				CONF_GASTOS_ESTAB=rsDatos("CONF_GASTOS_ESTAB")
                CONF_PERMITE_REDENCION=rsDatos("CONF_PERMITE_REDENCION")		
                CONF_PERMITE_REGALOS=rsDatos("CONF_PERMITE_REGALOS")		
                CONF_TIPO_CANGEO=rsDatos("CONF_TIPO_CANGEO")			
                
                CONF_COSTESDEVOLUCION=rsDatos("CONF_COSTESDEVOLUCION")			
                CONF_IDPRODUCTO_GENVIO=rsDatos("CONF_IDPRODUCTO_GENVIO")			
                CONF_IDPRODUCTO_DESCUENTOS=rsDatos("CONF_IDPRODUCTO_DESCUENTOS")		
				
				CONF_FINICIO_ESTADISTICAS=rsDatos("CONF_FINICIO_ESTADISTICAS")
				
				CONF_PAGINICIO=rsDatos("CONF_PAGINICIO")
				CONF_PAGINICIO_DIRECTORCUENTA=rsDatos("CONF_PAGINICIO_DIRECTORCUENTA")
				CONF_PAGINICIO_COMERCIOS=rsDatos("CONF_PAGINICIO_COMERCIOS")
				
				CONF_CADUCIDAD_PUNTOS=rsDatos("CONF_CADUCIDAD_PUNTOS")							
                
				'CONF_FORMULA_PUNTOSADINERO
				'CONF_USUARIO_DMMARQUETING
				'CONF_CONTRASENA_DMMARQUETING
				'CONF_NUMDIAS_PERMITIR_REDENCION
				
            end if
            rsDatos.cerrar()
            set rsDatos=nothing %>

                <input type="hidden" id="txtCod" name="txtCod" value="<%=CONF_CODIGO%>" />
        
                    <div id="filtro" class="filtro">
                        &nbsp;&nbsp;<%=objIdioma.getIdioma("Configuracion_Texto1")%>
                    </div>
                    <div class="separadorMed"></div>
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:180px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip2")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>
                                    <%=objIdioma.getIdioma("Configuracion_Texto2")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:140px;" >
                                    <input id="CONF_DOMINIOSITIOWEB" name="CONF_DOMINIOSITIOWEB" type="text" class="cajaTexto" maxlength="50"  value="<%=CONF_DOMINIOSITIOWEB%>"  />
                                </div>
                            </div>   			
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:200px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip3")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>
                                    <%=objIdioma.getIdioma("Configuracion_Texto3")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:40px;" >
                                    <input id="CONF_CONDMCORPORATIVE" name="CONF_CONDMCORPORATIVE" type="checkbox" <%if CONF_CONDMCORPORATIVE then%> checked="checked" <%end if%>   />
                                </div>
                            </div>   			                            
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:140px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip4")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>
                                    <%=objIdioma.getIdioma("Configuracion_Texto4")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:220px;" >
                                    <input id="CONF_PAGINICIO" name="CONF_PAGINICIO" type="text" class="cajaTexto" maxlength="200"  value="<%=CONF_PAGINICIO%>"  />
                                </div>
                            </div>   			
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:240px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip5")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>
                                    <%=objIdioma.getIdioma("Configuracion_Texto5")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:220px;" >
                                    <input id="CONF_PAGINICIO_DIRECTORCUENTA" name="CONF_PAGINICIO_DIRECTORCUENTA" type="text" class="cajaTexto" maxlength="200"  value="<%=CONF_PAGINICIO_DIRECTORCUENTA%>"  />
                                </div>
                            </div>   			
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:210px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip6")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto6")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:220px;" >
                                    <input id="CONF_PAGINICIO_COMERCIOS" name="CONF_PAGINICIO_COMERCIOS" type="text" class="cajaTexto" maxlength="200"  value="<%=CONF_PAGINICIO_COMERCIOS%>"  />
                                </div>
                            </div>   			                                                                                    
                    

                    <div class="separadorMed"></div>			
                    
        
                    <div id="filtro" class="filtro">
                        &nbsp;&nbsp;<%=objIdioma.getIdioma("Configuracion_Texto7")%>
                    </div>
                    
                    <div class="separadorMed"></div>            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:190px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip8")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto8")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:100px;" >
                                    <select id="CONF_OBJETOCORREO" name="CONF_OBJETOCORREO"  class="cboFiltros" >
                                        <option value="0" ></option>
                                        <option <% if trim(CONF_OBJETOCORREO)="1" then %>  selected="selected" <% end if%>  value="1" >1/CDONTS</option>
                                        <option <% if trim(CONF_OBJETOCORREO)="2" then %>  selected="selected" <% end if%>  value="2" >2/ASP-EMAIL</option>                                    
                                    </select>
                                </div>
                            </div>   
        
                            <div class="separadorMed" style="height:5px;" ></div>					
        
                            <div class="bloqueColumna"  >                            				                                       
                                <div class="columnaTablaDerecha" style="width:185px; " >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip9")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto9")%>&nbsp;
                                </div>
                                    <div class="columnaTablaIzquierda columnaTablaBloqueInterno"  > <!--Poner ancho 160 para ver en vertical-->
                                        <%=objIdioma.getIdioma("Configuracion_Texto10")%>&nbsp;<input id="CONF_EMAILHOST" name="CONF_EMAILHOST" type="text" class="cajaTexto" maxlength="50" style="width:160px;"  value="<%=CONF_EMAILHOST%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                        <%=objIdioma.getIdioma("Configuracion_Texto11")%>&nbsp;<input id="CONF_EMAILENVIO" name="CONF_EMAILENVIO" type="text" class="cajaTexto" maxlength="50" style="width:160px;"  value="<%=CONF_EMAILENVIO%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                        <%=objIdioma.getIdioma("Configuracion_Texto12")%>&nbsp;<input id="CONF_EMAILUSER" name="CONF_EMAILUSER" type="text" class="cajaTexto" maxlength="50" style="width:160px;"  value="<%=CONF_EMAILUSER%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                        <%=objIdioma.getIdioma("Configuracion_Texto13")%>&nbsp;<input id="CONF_EMAILPASSWORD" name="CONF_EMAILPASSWORD" type="password" class="cajaTexto" maxlength="50" style="width:160px;"  value="<%=CONF_EMAILPASSWORD%>"  />
                                    </div>  
                           </div>
                           
                           
                           
                    <div class="separadorMed" style="height:60px;"></div>					
                     
                     
                    <div id="filtro" class="filtro" >
                        &nbsp;&nbsp; <%=objIdioma.getIdioma("Configuracion_Texto14")%>
                    </div>
                    
                    <div class="separadorMed"></div>
                                
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:320px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip15")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto15")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:40px;" >
                                    <input id="CONF_CLIENTEA" name="CONF_CLIENTEA" type="text" class="cajaTexto" maxlength="50"  value="<%=CONF_CLIENTEA%>"  />
                                </div>
                            </div>   
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:320px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip16")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto16")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:40px;" >
                                    <input id="CONF_CLIENTEB" name="CONF_CLIENTEB" type="text" class="cajaTexto" maxlength="50"  value="<%=CONF_CLIENTEB%>"  />
                                </div>
                            </div>   								
                    
                    
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:140px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip17")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto17")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:40px;" >
                                    <input id="CONF_CONIVAINCLUIDO" name="CONF_CONIVAINCLUIDO" type="checkbox" <%if CONF_CONIVAINCLUIDO then%> checked="checked" <%end if%>   />
                                </div>
                            </div>  	       
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:150px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip18")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto18")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:60px;" >
                                    <input id="CONF_COSTESDEVOLUCION" name="CONF_COSTESDEVOLUCION" type="text" class="cajaTexto" maxlength="50"  value="<%=CONF_COSTESDEVOLUCION%>"  />
                                </div>
                            </div>                          
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:180px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip19")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto19")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:60px;" >
                                    <input id="CONF_IDPRODUCTO_GENVIO" name="CONF_IDPRODUCTO_GENVIO" type="text" class="cajaTexto" maxlength="50"  value="<%=CONF_IDPRODUCTO_GENVIO%>"  />
                                </div>
                            </div>                          
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:160px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip20")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto20")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:60px;" >
                                    <input id="CONF_IDPRODUCTO_DESCUENTOS" name="CONF_IDPRODUCTO_DESCUENTOS" type="text" class="cajaTexto" maxlength="50"  value="<%=CONF_IDPRODUCTO_DESCUENTOS%>"  />
                                </div>
                            </div>         
                            
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:190px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip21")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto21")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:60px;" >
                                	<%valorFecha=""
									if not isnull(CONF_FINICIO_ESTADISTICAS) And trim(CONF_FINICIO_ESTADISTICAS)<>"01/01/1900" then
										valorFecha=CONF_FINICIO_ESTADISTICAS
									end if%>
                                    <input id="CONF_FINICIO_ESTADISTICAS" name="CONF_FINICIO_ESTADISTICAS" type="text" class="cajaTextoFecha" maxlength="50"  value="<%=valorFecha%>"  />
                                </div>
                            </div>                                                                                              
                    
        
                    <div class="separadorMed"></div>
                     
        
                    <div id="filtro" class="filtro">
                        &nbsp;&nbsp;<%=objIdioma.getIdioma("Configuracion_Texto22")%>
                    </div>			 	
                    
                    
                    <div class="separadorMed"></div>
        
                            <%claseFidelizaOculto=""
                            if not CONF_CONCLUBCOMPRA then
                                claseFidelizaOculto=" display:none; "
                            end if %>
                                                    
                            <div class="bloqueColumna" >                            				                                       
                                <div class="columnaTablaDerecha" style="width:200px;" >
	                                <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip23")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                    <%=objIdioma.getIdioma("Configuracion_Texto23")%>&nbsp;
                                </div>
                                <div class="columnaTablaIzquierda" style="width:40px;" >
                                    <input id="CONF_CONCLUBCOMPRA" name="CONF_CONCLUBCOMPRA" type="checkbox" <%if CONF_CONCLUBCOMPRA then%> checked="checked" <%end if%>   />
                                </div>
                            </div>  
        
                            
                            <div id="capaOcultarFideliza"  style=" float:left; border:solid 1px #CDCDCD; clear:both; padding:20px; width:90%; margin-left:2%;   <%=claseFidelizaOculto%> " >
                            
                        
                                <div class="bloqueColumna"  >                            				                                       
                                    <div class="columnaTablaDerecha" style="width:150px; " >
                                        <a class="msgTootlTip" rel="<%=objIdioma.getIdioma("Configuracion_Descrip24")%>"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                         <%=objIdioma.getIdioma("Configuracion_Texto40")%>&nbsp;
                                    </div>
                                        <div class="columnaTablaIzquierda columnaTablaBloqueInterno"  > <!--Poner ancho 160 para ver en vertical-->
                                            <%=objIdioma.getIdioma("Configuracion_Texto24")%>&nbsp;<input id="CONF_NCUENTAPAGOS" name="CONF_NCUENTAPAGOS" type="text" class="cajaTexto" style="width:200px;" maxlength="50"  value="<%=CONF_NCUENTAPAGOS%>"  />&nbsp;&nbsp;&nbsp;&nbsp;<br /><br />
                                            <%=objIdioma.getIdioma("Configuracion_Texto41")%>&nbsp;<i><%=objIdioma.getIdioma("Configuracion_Texto42")%>&nbsp;&nbsp;&nbsp;&nbsp;</i>

                                        </div>  
                               </div>
                                                       
                                <div class="separadorMed" style="height:100px;" ></div>					            


                                <div class="bloqueColumna"  >                            				                                       
                                    <div class="columnaTablaDerecha" style="width:120px; " >
                                        <a class="msgTootlTip" rel="Formulas:<br /><br />"><img src="/DMCrm/APP/Imagenes/info.png" /></a>                                
                                         <%="Formulas:"%>&nbsp;
                                    </div>
                                        <div class="columnaTablaIzquierda columnaTablaBloqueInterno"  > <!--Poner ancho 160 para ver en vertical-->
                                        	<span style="float:left; text-decoration:underline; clear:both; width:100%; margin:0; padding:0;">Socios:</span>
                                            
                                        
                                            &nbsp;&nbsp;&nbsp;<%="Para asignar las f�rmulas de conversi�n de dinero a puntos pulse"%>&nbsp;
											 <a class="lnkPuntosComer" style="margin:0; padding:0;" href="/DMCrm/APP/Sistema/ConfiguracionPuntosComer.asp" >aqui</a>
                                             <%sql="Select count(COMER_CODIGO) contSinFormulas From CLUB_COMERCIOS Where COMER_FORMULA_DINERO_A_PUNTOS is null "
											    Set rsFormulas = connCRM.AbrirRecordSet(sql,0,1)
												if not rsFormulas.eof then
													if rsFormulas("contSinFormulas") > 0 then %>
			                                             <div id="validComerSinFormula" >Existen comercios sin f�rmula de conversi�n.</div>													
													<%end if
												end if
										 		rsFormulas.cerrar()
								            	set rsFormulas=nothing %>


                                            <div class="separadorMed">&nbsp;</div>
                                            &nbsp;&nbsp;&nbsp;<%="Convertir de puntos a dinero en compra"%>&nbsp;<input id="CONF_FORMULA_PUNTOSADINERO" name="CONF_FORMULA_PUNTOSADINERO" type="text" class="cajaTexto" maxlength="50" style="width:40px;"  value="<%=CONF_FORMULA_PUNTOSADINERO%>"  />
                                            <span style=" font-size:14px; font-weight:bold;">%</span>

			                                <div class="separadorMed"></div>
                                        	<span style="float:left; text-decoration:underline; clear:both; width:100%; margin:0; padding:0;">Comercios:</span>
											&nbsp;&nbsp;&nbsp;<%="Liquidaciones mensuales"%>&nbsp;<input id="CONF_FORMULALIQUIDACION" name="CONF_FORMULALIQUIDACION" type="text" class="cajaTexto" maxlength="50" style="width:40px;"  value="<%=CONF_FORMULALIQUIDACION%>"  />
                                            <span style=" font-size:14px; font-weight:bold;">%</span>                                            
                                           
                                            

                                        </div>  
                               </div>
                               
                            <div class="separadorMed" style="height:160px;" ></div>		                               
                               
        
                                <div class="separadorMed"></div>
                                
                                <div class="bloqueColumna" >                            				                                       
                                    <div class="columnaTablaDerecha" style="width:160px;" >
                                        <%=objIdioma.getIdioma("Configuracion_Texto27")%>&nbsp;
                                    </div>
                                    <div class="columnaTablaDerecha" style="width:60px;" >
                                        <%=objIdiomaGeneral.getIdioma("ValorAfirmativo")%>&nbsp;<input id="rdbPermiteVales" name="CONF_PERMITE_VALES" type="radio" value="1" <%if CONF_PERMITE_VALES then%> checked="checked" <%end if%> />                            
                                    </div>                                                        
                                    <div class="columnaTablaDerecha" style="width:60px;" >
                                        <%=objIdiomaGeneral.getIdioma("ValorNegativo")%>&nbsp;<input id="rdbNoPermiteVales" name="CONF_PERMITE_VALES" type="radio" value="0" <%if not CONF_PERMITE_VALES then%> checked="checked" <%end if%> />                            
                                    </div>  
                                    
                                </div>    
                                
                                 <div class="separadorMed"></div>
                                
                                <%claseTieneValesOculto=""
                                if  not CONF_PERMITE_VALES then
                                    claseTieneValesOculto=" style=""display:none;"" "
                                end if %>                               
                                 <div id="capaTieneVales" class="columnaTablaIzquierda columnaTablaBloqueInterno" <%=claseTieneValesOculto%>  >
                                    <%=objIdioma.getIdioma("Configuracion_Texto28")%>&nbsp;<input id="CONF_SALDO_EMITE_VALES" name="CONF_SALDO_EMITE_VALES" type="text" class="cajaTexto" maxlength="50" style="width:60px;"  value="<%=CONF_SALDO_EMITE_VALES%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <%=objIdioma.getIdioma("Configuracion_Texto29")%>&nbsp;<input id="CONF_NUMDIAS_EMITE_VALES" name="CONF_NUMDIAS_EMITE_VALES" type="text" class="cajaTexto" maxlength="50" style="width:60px;"  value="<%=CONF_NUMDIAS_EMITE_VALES%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <%=objIdioma.getIdioma("Configuracion_Texto30")%>&nbsp;<input id="CONF_CADUCIDAD_VALES" name="CONF_CADUCIDAD_VALES" type="text" class="cajaTexto" maxlength="50" style="width:60px;"  value="<%=CONF_CADUCIDAD_VALES%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>                                   
                                
                                <%claseNoTieneValesOculto=""
                                if  CONF_PERMITE_VALES then
                                    claseNoTieneValesOculto=" style=""display:none;"" "
                                end if %>                         
                                 <div id="capaNoTieneVales" class="columnaTablaIzquierda columnaTablaBloqueInterno" <%=claseNoTieneValesOculto%>  >
                                 
                                    <div class="bloqueColumna" >                            				                                       
                                        <div class="columnaTablaDerecha" style="width:200px;" >
                                            <%=objIdioma.getIdioma("Configuracion_Texto36")%>&nbsp;
                                        </div>
                                        <div class="columnaTablaIzquierda" style="width:60px;" >
                                            <input id="CONF_CADUCIDAD_PUNTOS" name="CONF_CADUCIDAD_PUNTOS" type="text" class="cajaTexto" maxlength="50" style="width:60px;"  value="<%=CONF_CADUCIDAD_PUNTOS%>"  />&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>
                                    </div>                                                            
                                    <div class="separadorMed"></div>   
                                 
                                    <%=objIdioma.getIdioma("Configuracion_Texto31")%>&nbsp; <input id="CONF_PERMITE_REDENCION" name="CONF_PERMITE_REDENCION" type="checkbox" <%if CONF_PERMITE_REDENCION then%> checked="checked" <%end if%>   />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <%=objIdioma.getIdioma("Configuracion_Texto32")%>&nbsp;<input id="CONF_PERMITE_REGALOS" name="CONF_PERMITE_REGALOS" type="checkbox" <%if CONF_PERMITE_REGALOS then%> checked="checked" <%end if%>   />
                                    
                                    <div class="separadorMed"></div>   
                                    <%clasetieneRegalosOculto=""
                                    if not CONF_PERMITE_REGALOS then
                                        clasetieneRegalosOculto=" style=""display:none;"" "
                                    end if %>                                           
                                    <div id="capaTieneRegalos" <%=clasetieneRegalosOculto%>  >
                                        <input id="rdbSoloPuntos" name="CONF_TIPO_CANGEO" type="radio" value="0" <%if trim(CONF_TIPO_CANGEO)="0" then%> checked="checked" <%end if%> />&nbsp;<%=objIdioma.getIdioma("Configuracion_Texto33")%><br/>
                                        <input id="rdbCanjeoMixto"  name="CONF_TIPO_CANGEO" type="radio" value="1" <%if trim(CONF_TIPO_CANGEO)="1" then%> checked="checked" <%end if%>  />&nbsp;<%=objIdioma.getIdioma("Configuracion_Texto34")%>
                                        
                                     	<div class="separadorMed"></div> 
                                        <div class="bloqueColumna" >                            				                                       
                                            <div class="columnaTablaDerecha" style="width:220px; background:#999999" >
                                                <%=objIdioma.getIdioma("Configuracion_Texto38")%>&nbsp;
                                            </div>
                                            <div class="columnaTablaDerecha" style="width:60px;" >
                                                <input id="CONF_GASTOS_ESTAB" name="CONF_GASTOS_ESTAB" type="text" class="cajaTexto" style="width:20px;" maxlength="5" value="<%=CONF_GASTOS_ESTAB%>"  />&euro;                           
                                            </div>   
                                        </div>    
                                        
                                        <div class="separadorMed"></div>
                                        <div class="bloqueColumna">                        				                                       
                                            <div class="columnaTablaDerecha" style="width:230px; background-color:#666666" >
                                                <%=objIdioma.getIdioma("Configuracion_Texto37")%>&nbsp;
                                            </div>
                                            <div class="columnaTablaDerecha" style="width:60px;" >
                                                <%=objIdiomaGeneral.getIdioma("ValorAfirmativo")%>&nbsp;<input id="rdbEnvioDomicilio" name="CONF_ENVIO_DOMICILIO" type="radio" value="1" <%if CONF_ENVIO_DOMICILIO then%> checked="checked" <%end if%> />
                                                                           
                                            </div>                                                        
                                            <div class="columnaTablaDerecha" style="width:60px;" >
                                                <%=objIdiomaGeneral.getIdioma("ValorNegativo")%>&nbsp;<input id="rdbEnvioDomicilio" name="CONF_ENVIO_DOMICILIO" type="radio" value="0" <%if not CONF_ENVIO_DOMICILIO then%> checked="checked" <%end if%> />                            
                                            </div>
                                            &nbsp;&nbsp;
                                            <%if CONF_ENVIO_DOMICILIO then%>
                                                <div class="columnaTablaDerecha" style="width:220px;" >
                                                    <%=objIdioma.getIdioma("Configuracion_Texto39")%>&nbsp;&nbsp;
                                                    <input id="CONF_GASTOS_DOM" name="CONF_GASTOS_DOM" type="text" class="cajaTexto" style="width:20px;" maxlength="5" value="<%=CONF_GASTOS_DOM%>"  />&euro;
                                                </div>
                                            <%end if%> 
                                        </div>  
                                     </div>   
                                    
                                </div>                                                           
                                
            
                            </div>                                  
                    
                    
                    <div class="separadorMed" style="height:40px;"></div>
                    
                    <hr />
                    <div style="clear:both; float:left; text-align:right; width:100%; padding-right:20px; padding-bottom:20px; padding-top:20px;">
                         <font color="#000000" face="verdana" size="1"><i><%=objIdiomaGeneral.getIdioma("CamposObligatorios_Texto1")%></i></font>                            
                        <input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton" id="btnGuardarConf" name="btnGuardarConf"   />
                        <input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonInicializar_Texto1")%>" class="boton" id="btnInicializarConf" name="btnInicializarConf"   />  <!--onclick="this.form.reset();" -->
                     </div>												
             
             
			<%end if 'end de if request("gu")="1" then              %>

		</form>


		<script type="text/javascript" language="javascript" >
		$(document).ready(function() {
								   
			$("#CONF_CONCLUBCOMPRA").change(function() {
			  if ( $(this).attr('checked') ) {
					$('#capaOcultarFideliza').show('slow');				  
			  } else {
					$('#capaOcultarFideliza').hide('fast');	
					$('#capaOcultarFideliza input').val('');
					$('#capaOcultarFideliza input').attr('checked',false);					
					$('#capaOcultarFideliza select').val('0');
					 $('#capaTieneVales').hide('fast');					
					 $('#capaNoTieneVales').hide('fast');	
					 $('#capaTieneRegalos').hide('fast');						 

			  }
			});					


			$("#rdbPermiteVales").change(function() {
				 if ( $(this).attr('checked') ) {
					 $('#capaTieneVales').show('slow');
					 $('#capaNoTieneVales').hide('fast');					 
					$('#CONF_PERMITE_REGALOS').attr('checked',false)	
					$('#CONF_PERMITE_REDENCION').attr('checked',false)										
				 	$('#rdbSoloPuntos').attr('checked',false)					
					 $('#rdbCanjeoMixto').attr('checked',false)						
					 $('#capaTieneRegalos').hide('fast');	
				 }
				 
			});					
			
			$("#rdbNoPermiteVales").change(function() {
				 if ( $(this).attr('checked') ) {
					$('#capaTieneVales').hide('fast');
					$('#capaTieneVales input').val('');					 
					$('#capaNoTieneVales').show('slow');					
				 }
			});		
			
			
			$("#CONF_PERMITE_REGALOS").change(function() {
				 if ( $(this).attr('checked') ) {
					 $('#capaTieneRegalos').show('slow');
				 } else {  
				 	 $('#rdbSoloPuntos').attr('checked',false)					
					 $('#rdbCanjeoMixto').attr('checked',false)					
					 $('#capaTieneRegalos').hide('fast');
				 }
			});								
			

			$("#btnGuardarConf").click(function() {
				//Realizar validaciones pertienentes
				var permitirGuardar=true;
				if ( $('#CONF_OBJETOCORREO').val()=="0" || $.trim($('#CONF_EMAILHOST').val())=="" || $.trim($('#CONF_EMAILENVIO').val())=="" || $.trim($('#CONF_EMAILUSER').val())=="" || $.trim($('#CONF_EMAILPASSWORD').val())=="" ) {
					permitirGuardar=false;
					alert("<%=objIdioma.getIdioma("Configuracion_Texto35")%>");
				}
				
				if (permitirGuardar) {
					document.frmConf.submit();												
				}
												
			});																					
													
			$("#btnInicializarConf").click(function() {
				document.location="/DMCrm/APP/sistema/configuracion.asp";										

			});

		});			
		
		
        
        </script>

             

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->


<!--
	  //Escribimos las funciones de validacion de los campos obligatorios
	  if (stripInitialWhitespace(theForm.CONF_DOMINIOSITIOWEB.value)=="") { alert("Escriba un valor para el campo \"Dominio del sitio web general\"."); theForm.CONF_DOMINIOSITIOWEB.focus();   return(false); } if (stripInitialWhitespace(theForm.CONF_OBJETOCORREO.value)=="") { alert("Escriba un valor para el campo \"Objeto para los env�os de email (1/CDONTS 2/ASPEM)\"."); theForm.CONF_OBJETOCORREO.focus();   return(false); } if (stripInitialWhitespace(theForm.CONF_EMAILHOST.value)=="") { alert("Escriba un valor para el campo \"Cuenta email: host env�o\"."); theForm.CONF_EMAILHOST.focus();   return(false); } if (stripInitialWhitespace(theForm.CONF_EMAILENVIO.value)=="") { alert("Escriba un valor para el campo \"Cuenta email: email\"."); theForm.CONF_EMAILENVIO.focus();   return(false); } if (stripInitialWhitespace(theForm.CONF_EMAILUSER.value)=="") { alert("Escriba un valor para el campo \"Cuenta email: usuario\"."); theForm.CONF_EMAILUSER.focus();   return(false); } if (stripInitialWhitespace(theForm.CONF_EMAILPASSWORD.value)=="") { alert("Escriba un valor para el campo \"Cuenta email: password\"."); theForm.CONF_EMAILPASSWORD.focus();   return(false); } 
	  //Escribimos las funciones de validacion de los campos de texto
	  
	  //Escribimos las funciones de validacion de los campos de IP
	  
	  //Escribimos las funciones de validacion de los campos de email
	  
	  //Escribimos las funciones de validacion de los campos num�ricos
	   for (k=0;k<theForm.CONF_OBJETOCORREO.value.length;k++) { 
  if (theForm.CONF_OBJETOCORREO.value.charAt(k) < '0' || theForm.CONF_OBJETOCORREO.value.charAt(k)>'9') { 
    alert("El campo \"Objeto para los env�os de email (1/CDONTS 2/ASPEM)\" s�lo puede contener caracteres num�ricos. Elimine los caracteres no num�ricos.");
    theForm.CONF_OBJETOCORREO.focus();  
    return(false); } } 

 
	  //Escribimos las funciones de validacion de los campos num�ricos decimales
	  
  Valor=theForm.CONF_CLIENTEA.value; 
  if (  isFloat(theForm.CONF_CLIENTEA.value,true) == false & isSignedFloat (theForm.CONF_CLIENTEA.value,true)==false ) { 
    alert("El campo \"Cantidad media de ventas anuales de los clientes A\" debe tener un contenido decimal. Si ha introducido un punto, sustit�yalo por una coma. (Ej. 10,45)");
    theForm.CONF_CLIENTEA.focus();  
    return(false); } 


  Valor=theForm.CONF_CLIENTEB.value; 
  if (  isFloat(theForm.CONF_CLIENTEB.value,true) == false & isSignedFloat (theForm.CONF_CLIENTEB.value,true)==false ) { 
    alert("El campo \"Cantidad media de ventas anuales de los clientes B\" debe tener un contenido decimal. Si ha introducido un punto, sustit�yalo por una coma. (Ej. 10,45)");
    theForm.CONF_CLIENTEB.focus();  
    return(false); } 

 
	  //Escribimos las funciones de validacion de los campos fecha
	   
	  //Escribimos las funciones de validacion de los campos hora
	   

	  //si se ha validado todo submitimos el formulario con los datos
	  document.formEdtar.submit();
-->





