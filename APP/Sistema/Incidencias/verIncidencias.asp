<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Titulo</title>
<!-- #include virtual="/dmcrm/includes/jquery.asp"-->
</head>
<body>
<!--Inicio contenido-->
<% ticket = request("codigo")
		
	if request.Form("nuevaIncidencia")=1 then
		ticketForm = request.Form("codigo")
		sqlInsert = "INSERT INTO TI_LineaIncidencias"&_
					" (ticket, usuario, esAdmin, mensaje, leido, ip, navegador, fecha)"&_
					" VALUES ('" & ticketForm & "' ,'" & request.Form("usuario") & "' ,1,'" & request.Form("descripIncidenciaAdmin") & "' ,0 ,'','','" & now & "')"
		connCRM.execute sqlInsert,numregistrosInsert
		Set rsAdminVacio = connCRM.abrirRecordSet("SELECT usuarioAdmin FROM TI_Incidencias WHERE ticket=" & ticketForm,3,1)
		if not rsAdminVacio.eof then
			if not isnull(rsAdminVacio("usuarioAdmin")) then
				if trim(rsAdminVacio("usuarioAdmin"))<>"" then
					updatear = false
				else	
					updatear = true
				end if
			else
				updatear = true
			end if
		end if
		rsAdminVacio.cerrar
		set rsAdminVacio = nothing
		sqlSet = "SET estado=2"
		if updatear then sqlSet = sqlSet & ",usuarioAdmin=" & session("codigoUsuario")
		connCRM.execute "UPDATE TI_Incidencias " & sqlSet & " WHERE ticket=" & ticketForm,numregistrosUpadte
	end if
	
	Set rsTicket = connCRM.abrirRecordSet("SELECT * FROM TI_Incidencias as I INNER JOIN TI_LineaIncidencias as LI ON I.ticket=LI.ticket WHERE I.ticket=" & ticket & " ORDER BY LI.fecha DESC",3,1)
	if not rsTicket.eof then
		estado = rsTicket("estado")
		usuario = rsTicket("usuario")
		asunto = rsTicket("asunto")
		emailUsuario = rsTicket("usuario_email")
		responsable = 0
		if not isnull(rsTicket("usuarioAdmin")) and not (rsTicket("usuarioAdmin")="") then responsable = rsTicket("usuarioAdmin")%>
      
		<div id="tituloTicketAdmin">  
		<span id="NumeroTicketAdmin"><h3><%=objIdioma.getIdioma("Consultas_Texto8")%> #<%=rsTicket("ticket")%></h3></span>
		<span id="AsuntoTicketAdmin"><h3><%=asunto%></h3></span>
		<span id="fechaTicketAdmin"><%=formatdatetime(rsTicket("fecha"),2)%>&nbsp;<%=formatdatetime(rsTicket("fecha"),4)%></span>
        <%if estado = 2 then%>
        <span id="estadoTicketAdmin">
            	<a id="cerrarConsulta"><%=objIdioma.getIdioma("Consultas_Texto19")%></a>
        </span>
        <%end if%>
        <form name="ResAdmin" id="ResAdmin">
        <label for="cboResponsable"><%=objIdioma.getIdioma("Consultas_Texto9")%> </label>
        <select name="cboResponsable" id="cboResponsable">
        	<option value=""></option>
        	<%Set rsAnalistas = connCRM.abrirRecordSet("SELECT * FROM ANALISTAS WHERE ANA_EMAIL IS NOT NULL ORDER BY ANA_NOMBRE ASC",3,1)
			while not rsAnalistas.eof %>
            	<option value="<%=rsAnalistas("ANA_CODIGO")%>"<%if cint(responsable) = cint(rsAnalistas("ANA_CODIGO")) then%> selected<%end if%>><%=rsAnalistas("ANA_NOMBRE")%></option>
				<% rsAnalistas.movenext
			wend
			rsAnalistas.cerrar
			set rsAnalistas = nothing%>
        </select>
        <input id="guardarResponsable" type="button" value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" />
        <div id="mensajeOKAdmin"></div>
		</form>
        
        <form name="frmIncidenciaAdmin" id="frmIncidenciaAdmin" method="post" action="verIncidencias.asp">
            <input type="hidden" name="nuevaIncidencia" id="nuevaIncidencia" value="1" />
            <input type="hidden" name="emailUsuario" id="emailUsuario" value="<%=emailUsuario%>" />
            <input type="hidden" name="codigo" id="codigo" value="<%=ticket%>" />
            <input type="hidden" name="usuario" id="usuario" value="<%=session("codigoUsuario")%>" />
            <label for="descripIncidenciaAdmin"><%=objIdioma.getIdioma("Consultas_Texto20")%> *</label><br />
            <textarea name="descripIncidenciaAdmin" id="descripIncidenciaAdmin" cols="70"></textarea><br /><br />
            <input type="submit" name="enviarIncidenciaAdmin" id="enviarIncidenciaAdmin" value="<%=objIdiomaGeneral.getIdioma("BotonEnviar_Texto1")%>" />
        </form>
        
        </div>
        
        
        
        <div id="chatAdminAdmin">
		<% primeraVez = 0
		strConversacion = ""
		while not rsTicket.eof
			if rsTicket("esAdmin") then
				Set rsDatosAdmin = connCRM.abrirRecordSet("SELECT * FROM ANALISTAS WHERE ANA_CODIGO=" & cint(rsTicket("usuario")),3,1)
				if not rsDatosAdmin.eof then
					nombre = rsDatosAdmin("ANA_NOMBRE")
					nick = rsDatosAdmin("ANA_NICK")
					if (trim(nick)="" OR isnull(nick)) then nick = nombre
					if trim(rsDatosAdmin("ANA_FOTO"))="" then 
						foto = "/dmcrm/public/imagenes/perfil.png"
					else
						foto = "/dmcrm/APP/UsuariosFtp/imagenes/" & rsDatosAdmin("ANA_FOTO")
					end if
				end if
				rsDatosAdmin.cerrar
				set rsDatosAdmin = nothing%>
			<div class="globoIzqAdmin">
				<div class="capaTapaIzqAdmin capaIzqAdmin"></div>
				<div class="cuerpoIzqAdmin capaIzqAdmin">
					<span class="fechaInAdmin"><%=formatdatetime(rsTicket("fecha"),2)%>&nbsp;<%=formatdatetime(rsTicket("fecha"),4)%></span><br />
					<span class="mensajeInAdmin"><%=rsTicket("Mensaje")%></span><br /><br />
					<div class="datosIzqAdmin">
					<span class="fotoAdminAdmin"><img src="<%=foto%>" /></span>
					<span class="autorInAdmin"><%=nick%></span>
					</div>
				</div>
				<div class="capaBaseIzqAdmin capaIzqAdmin"></div>
			</div>
				<% strConversacion = strConversacion & "<hr /><p><strong>" & objIdiomaGeneral.getIdioma("ConsultasAC_Texto1") &  " " & nick & " , " & formatdatetime(rsTicket("fecha"),2) & " " & formatdatetime(rsTicket("fecha"),4) & ":</strong><br /><br />" & rsTicket("Mensaje") & "<br /><br />" & nombre  & "</p>"
			else 
				if primeraVez = 0 then
					conFoto = false
					Set rsFoto = connCRM.abrirRecordSet("SELECT CON_NOMBRE,CON_AVATAR_FACEBOOK FROM CONTACTOS WHERE CON_CODIGO=" & rsTicket("usuario"),3,1)
					if not rsFoto.eof then
						if not isnull(rsFoto("CON_AVATAR_FACEBOOK")) then 
							rutaFoto = rsFoto("CON_AVATAR_FACEBOOK")
							conFoto = true
						end if
						nombreUsuario = rsFoto("CON_NOMBRE")
					end if
					rsFoto.cerrar
					set rsFoto = nothing
					primeraVez = 1
				end if%>
			<div class="globoDchaAdmin">
				<div class="capaTapaDchaAdmin capaDchaAdmin"></div>
				<div class="cuerpoDchaAdmin capaDchaAdmin">
					<span class="fechaInAdmin"><%=formatdatetime(rsTicket("fecha"),2)%>&nbsp;<%=formatdatetime(rsTicket("fecha"),4)%></span><br />
					<span class="mensajeInAdmin"><%=rsTicket("Mensaje")%></span><br /><br />
					<div class="datosDchaAdmin">
					<span class="fotoUserAdmin">
						<%if conFoto then%>
							<img src="<%=rutaFoto%>" />
						<%end if%>
					</span>
					<span class="autorInAdmin"><%=nombreUsuario%></span>
					</div>
				</div>
				<div class="capaBaseDchaAdmin capaDchaAdmin"></div>
			</div>
				<% strConversacion = strConversacion & "<hr /><p><strong>" & objIdiomaGeneral.getIdioma("ConsultasAC_Texto1") &  " " & nombreUsuario & " , " & formatdatetime(rsTicket("fecha"),2) & " " & formatdatetime(rsTicket("fecha"),4) & ":</strong><br /><br />" & rsTicket("Mensaje") & "</p>"
			end if
			autorUltimoMensaje = rsTicket("esAdmin")
			rsTicket.movenext
		wend
	end if
    rsTicket.cerrar
	set rsTicket = nothing
	if request.Form("nuevaIncidencia")=1 then
		'enviamos el EMAIL con el historial
		intCodigoPlantillaEmail="29"
		idiomaContacto = 1
		
		emailUsu = request.Form("emailUsuario")

		respPlantilla=obtenerPlantillasEmail(intCodigoPlantillaEmail,idiomaContacto,asuntoMail,cuerpoMail)

		asuntoMail=replace(asuntoMail,"/*ticket*/",ticket)
		asuntoMail=replace(asuntoMail,"/*asuntoTicket*/",asunto)

		cuerpoMail=replace(cuerpoMail,"/*ticket*/",ticket)
		cuerpoMail=replace(cuerpoMail,"/*asuntoTicket*/",asunto)
		cuerpoMail=replace(cuerpoMail,"/*conversacion*/",strConversacion)
		cuerpoMail=replace(cuerpoMail,"/*denominacion*/",denominacion)

		respEnvio=enviarEmailsAPP(asuntoMail,cuerpoMail,emailUsu,descripError,"")
	end if%>
    </div>
<script type="text/javascript">
		$(document).ready(function () {
			$('#cerrarConsulta').click(function() {
				if (($('#cerrarConsulta').html())!="Cerrada"){
					$.ajax({
						type: "POST",
						url: "/DMCrm/APP/Sistema/incidencias/cerrarIncidencia.asp",
						data: "ticket=<%=ticket%>&estado=<%=estado%>",
						async: false,
						success: function(datos) {
							$('#estadoTicketAdmin').html("Cerrada");
						},
						error: function(data) {
							alert("error: " + data);
						}
					});	
				}
			});
			$('#guardarResponsable').click(function() {
				var analista = $("#cboResponsable").val();
				$.ajax({
					type: "POST",
					url: "/DMCrm/APP/Sistema/incidencias/guardarResponsable.asp",
					data: "ticket=<%=ticket%>&ana=" + analista + "&estado=<%=estado%>",
					async: false,
					success: function(datos) {
						if (datos==0) {
							$('#mensajeOKAdmin').html("<%=objIdiomaGeneral.getIdioma("GuardadoCorrectamente_Texto1")%>");
						} else {
							$('#mensajeOKAdmin').html("<%=objIdioma.getIdioma("Consultas_Texto22")%>");
						}
					},
					error: function(data) {
						alert("error: " + data);
					}
				});	
			});
		});
</script> 
<!--Fin contenido-->
</body>
</html>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->