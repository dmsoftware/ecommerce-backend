<%
filtrosBusqueda = "'{ ""name"": ""txtTicket"", ""value"": ""'+$('#txtTicket').val()+'"" }**"
filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""txtContacto"", ""value"": ""'+$('#txtContacto').val()+'"" }**"
filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""cboAdmin"", ""value"": ""'+$('#cboAdmin').val()+'"" }**"
filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""txtFechaDesde"", ""value"": ""'+$('#txtFechaDesde').val()+'"" }**"
filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""txtFechaHasta"", ""value"": ""'+$('#txtFechaHasta').val()+'"" }**"
filtrosBusqueda = filtrosBusqueda & "{ ""name"": ""cboEstado"", ""value"": ""'+$('#cboEstado').val()+'"" }'" %>

<script type="text/javascript">
	function marcarFiltroResumen(valorFiltro) {
		document.getElementById("btnLimpiar").click();										
		if (document.getElementById("txtFiltroEstadoResumen")!=null) {
			document.getElementById("txtFiltroEstadoResumen").value=valorFiltro;
		}	
		document.getElementById("btnBuscarFiltros").click();
	}
				

	$(document).ready(function() {
		$("#btnMostrarResumen").live('click', function(){
			$("#capaDatosResumen").show('slow');
			$("#btnOcultarResumen").show('slow');					
			$("#btnMostrarResumen").hide('slow');		
			$("#txtPanelCerrado").val('');
		});		
		
		$("#btnOcultarResumen").live('click', function(){
			$("#capaDatosResumen").hide('slow');
			$("#btnMostrarResumen").show('slow');					
			$("#btnOcultarResumen").hide('slow');	
			$("#txtPanelCerrado").val('1');					
		});		
	});					   				
</script>									   

<style type="text/css">
	.bloqueCabeceraIzq {float:left; margin:0; padding:0; width:49%; margin-right:1%;}
	.bloqueCabeceraDer {float:left; margin:0; padding:0; width:49%;}
	
	.tablaBloqueCabecera {float:left; clear:both;}
	.filaTablaDatos {   }
	.celdaTablaResumen { min-height:40px; border-bottom:dashed 1px #CDCDCD;  }
	.celdaTablaNumerico { min-height:40px; border-bottom:dashed 1px #CDCDCD; color:#FF6600; font-family:Arial; font-size:14px; font-weight:bold; }

	.imgFilaBloque {position:relative; top:5px; left:0;}
	#sociosEnviarMailsTodos {padding:2px 0 0 30px; text-align:left; line-height:1em; font-size:11px; color:#8C8C8D; width:118px;}
	
	.enlaceVerSocios { font-style:italic; float:right; }
	.cabTabla { text-align:left; border-bottom:1px solid #000000; font-weight:bold; padding-top:10px }
	
</style>
 
		<%codResponsable = request("cboAdmin")
		if codResponsable = "" then codResponsable = session("codigoUsuario")
		
		codEstado = request("cboEstado")
		if codEstado = "" then codEstado = 1%>
			<div id="capaDatosResumen" <%if trim(request.Form("txtPanelCerrado"))="1" then%> style="display:none;"<%end if%> >
                <div class="bloqueCabeceraIzq">
                <table  width="80%" border="0" cellpadding="0" cellspacing="0"  class="tablaBloqueCabecera">
                    <tbody>
                        <tr class="filtro" style="height:25px;" valign="middle" align="left"  >
                            <th colspan="5">&nbsp;&nbsp;&nbsp;<%=objIdioma.getIdioma("Consultas_Texto1")%></th>
                        </tr> 
                        <% sql = "SELECT count(TI_Incidencias.ticket) numIncidencias from TI_EstadosIncidencia LEFT JOIN TI_Incidencias ON  TI_EstadosIncidencia.id = TI_Incidencias.estado WHERE TI_Incidencias.estado=1 AND (TI_Incidencias.usuarioAdmin IS NULL OR TI_Incidencias.usuarioAdmin='')"			
                        Set rsEstadosPedidos = connCRM.abrirRecordSet(sql,0,1)
                        if not rsEstadosPedidos.eof then
							numIncidencias = rsEstadosPedidos("numIncidencias")
						end if
						rsEstadosPedidos.cerrar
						set rsEstadosPedidos = nothing%>
						<tr valign="middle" class="filaTablaDatos"  align="left">
                            <td class="celdaTablaResumen" width="20px" valign="top" align="center"><img class="imgFilaBloque" src="/dmcrm/APP/imagenes/EstadoIncidencia_0.png" /></td>                                    
                            <td class="celdaTablaResumen" ><%=objIdioma.getIdioma("Consultas_Texto2")%></td>
                            <td class="celdaTablaResumen" width="50px" align="right" ><%if numIncidencias>0 then%><a class="enlaceVerSocios" href="incidencias.asp?cboEstado=2" class="bcRes">[<%=objIdioma.getIdioma("Consultas_Texto3")%>]</a><%end if%></td>
							<td class="celdaTablaNumerico" width="20px" align="right" ><%=numIncidencias%></td>
                            <td class="celdaTablaResumen" width="20px" align="right" >&nbsp;</td>
                        </tr>
						<% sql = "SELECT count(TI_Incidencias.ticket) numIncidencias from TI_EstadosIncidencia LEFT JOIN TI_Incidencias ON  TI_EstadosIncidencia.id = TI_Incidencias.estado WHERE TI_Incidencias.estado=1 AND TI_Incidencias.usuarioAdmin=" & session("codigoUsuario") & " GROUP BY TI_EstadosIncidencia.id, TI_EstadosIncidencia.nombre"
                        Set rsEstadosPedidos = connCRM.abrirRecordSet(sql,0,1)
						numIncidencias = 0
                        if not rsEstadosPedidos.eof then numIncidencias = rsEstadosPedidos("numIncidencias")
						rsEstadosPedidos.cerrar
						set rsEstadosPedidos = nothing%>
                        <tr valign="middle" class="filaTablaDatos"  align="left">
                            <td class="celdaTablaResumen" width="20px" valign="top" align="center"><img class="imgFilaBloque" src="/dmcrm/APP/imagenes/EstadoIncidencia_1.png" /></td>                                    
                            <td class="celdaTablaResumen" ><%=objIdioma.getIdioma("Consultas_Texto4")%></td>
                            <td class="celdaTablaResumen" width="50px" align="right" ><%if numIncidencias>0 then%><a class="enlaceVerSocios" href="incidencias.asp?cboEstado=3&cboAdmin=<%=session("codigoUsuario")%>" class="bcRes">[<%=objIdioma.getIdioma("Consultas_Texto3")%>]</a><%end if%></td>
                            <td class="celdaTablaNumerico" width="20px" align="right" ><%=numIncidencias%></td>
                            <td class="celdaTablaResumen" width="20px" align="right" >&nbsp;</td>
                        </tr>
                        <% sql = "SELECT count(TI_Incidencias.ticket) numIncidencias from TI_EstadosIncidencia LEFT JOIN TI_Incidencias ON  TI_EstadosIncidencia.id = TI_Incidencias.estado WHERE TI_Incidencias.estado=2 AND TI_Incidencias.usuarioAdmin=" & session("codigoUsuario") & " GROUP BY TI_EstadosIncidencia.id, TI_EstadosIncidencia.nombre"
                        Set rsEstadosPedidos = connCRM.abrirRecordSet(sql,0,1)
						numIncidencias = 0
                        if not rsEstadosPedidos.eof then numIncidencias = rsEstadosPedidos("numIncidencias")
						rsEstadosPedidos.cerrar
						set rsEstadosPedidos = nothing %>
                        <tr valign="middle" class="filaTablaDatos"  align="left">
                            <td class="celdaTablaResumen" width="20px" valign="top" align="center"><img class="imgFilaBloque" src="/dmcrm/APP/imagenes/EstadoIncidencia_2.png" /></td>                                    
                            <td class="celdaTablaResumen" width="100px"><%=objIdioma.getIdioma("Consultas_Texto5")%></td>
                            <td class="celdaTablaResumen" width="40px" align="right"><%if numIncidencias>0 then%><a class="enlaceVerSocios" href="incidencias.asp?cboEstado=4&cboAdmin=<%=session("codigoUsuario")%>" class="bcRes">[<%=objIdioma.getIdioma("Consultas_Texto3")%>]</a><%end if%></td>
                            <td class="celdaTablaNumerico" width="20px" align="right" ><%=numIncidencias%></td>
                            <td class="celdaTablaResumen" width="20px" align="right" >&nbsp;</td>
                        </tr>
                        <% sql = "SELECT count(TI_Incidencias.ticket) numIncidencias from TI_EstadosIncidencia LEFT JOIN TI_Incidencias ON  TI_EstadosIncidencia.id = TI_Incidencias.estado WHERE TI_Incidencias.estado=3 AND TI_Incidencias.usuarioAdmin=" & session("codigoUsuario") & " GROUP BY TI_EstadosIncidencia.id, TI_EstadosIncidencia.nombre"
                        Set rsEstadosPedidos = connCRM.abrirRecordSet(sql,0,1)
						numIncidencias = 0
                        if not rsEstadosPedidos.eof then numIncidencias = rsEstadosPedidos("numIncidencias")
						rsEstadosPedidos.cerrar
						set rsEstadosPedidos = nothing %>
                        <tr valign="middle" class="filaTablaDatos"  align="left">
                            <td class="celdaTablaResumen" width="20px" valign="top" align="center"><img class="imgFilaBloque" src="/dmcrm/APP/imagenes/EstadoIncidencia_3.png" /></td>                                    
                            <td class="celdaTablaResumen" ><%=objIdioma.getIdioma("Consultas_Texto6")%></td>
                            <td class="celdaTablaResumen" width="50px" align="right" ><%if numIncidencias>0 then%><a class="enlaceVerSocios" href="incidencias.asp?cboEstado=5&cboAdmin=<%=session("codigoUsuario")%>" class="bcRes">[<%=objIdioma.getIdioma("Consultas_Texto3")%>]</a><%end if%></td>
                            <td class="celdaTablaNumerico" width="20px" align="right" ><%=numIncidencias%></td>
                            <td class="celdaTablaResumen" width="20px" align="right" >&nbsp;</td>
                        </tr>
					</tbody>
				</table>
			</div>
            <div style="clear:both; height:40px; width:100%;"></div>					
		</div>									
                    
					
		<input type="button" name="btnOcultarResumen" id="btnOcultarResumen"  value="<%=objIdiomaGeneral.getIdioma("BotonOcultarResumen_Texto1")%>"  class="boton" style="float:right; margin:-25px  200px 0 0; <%if trim(request.Form("txtPanelCerrado"))="1" then%> display:none; <%end if%> "  />				
		<input type="button" name="btnMostrarResumen" id="btnMostrarResumen"  value="<%=objIdiomaGeneral.getIdioma("BotonMostrarResumen_Texto1")%>"  class="boton" style="float:right; margin:-25px  200px 0 0; <%if trim(request.Form("txtPanelCerrado"))<>"1" then%> display:none; <%end if%> "  />				
		
		<input id="btnLimpiar" name="btnLimpiar" type="button" class="boton" value="<%=objIdiomaGeneral.getIdioma("BotonLimpiar_Texto1")%>" onClick="javascript:inicializarFiltros();"  style="float:right; margin:-25px 0 0 0;" />   
		<input type="button" name="btnBuscarFiltros" id="btnBuscarFiltros"  value="<%=objIdiomaGeneral.getIdioma("BotonBuscar_Texto1")%>"  class="boton" style="float:right; margin:-25px  100px 0 0;"  />

<div id="divControlesTodos" >                								
               	<div id="filtro" class="filtro">
                	&nbsp;&nbsp;<%=objIdioma.getIdioma("Consultas_Texto7")%>
                </div>
						<div class="bloqueColumna" >                            				                                       
	                    	<div class="columnaTablaDerecha" style="width:50px;" >
		                        <%=objIdioma.getIdioma("Consultas_Texto8")%>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:50px;" >
    		    	        	<input id="txtTicket" name="txtTicket" type="text" class="cajaTexto" maxlength="250"  value="<%=request.Form("txtTicket")%>"  />
                    	    </div>
                        </div>   
                        <div class="bloqueColumna" >                            				                                       
	                    	<div class="columnaTablaDerecha" style="width:150px;" >
		                        <%=objIdioma.getIdioma("Consultas_Texto9")%>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:100px;" >
                            	<select name="cboAdmin" id="cboAdmin" class="cboFiltros">
                                <option value="0"<%if cint(codResponsable)=0 then%> selected="selected"<%end if%>>&nbsp;</option>
                                <%Set rsANA = connCRM.abrirRecordSet("SELECT * FROM ANALISTAS WHERE ANA_ACTIVO = 1 ORDER BY ANA_NOMBRE",0,1)
								while not rsANA.eof %>
                                	<option value="<%=rsANA("ANA_CODIGO")%>"<%if cint(rsANA("ANA_CODIGO"))=cint(codResponsable) then%> selected="selected"<%end if%>><%=rsANA("ANA_NOMBRE")%></option>
									<% rsANA.movenext
								wend
								rsANA.cerrar
								Set rsANA = nothing%>
                                </select>
                    	    </div>
                        </div> 
                        <div class="bloqueColumna" >                            				                                       
	                    	<div class="columnaTablaDerecha" style="width:120px;" >
		                        <%=objIdioma.getIdioma("Consultas_Texto10")%>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:200px;" >
                            	<select id="cboEstado" name="cboEstado" class="cboFiltros">
                                	<option value="0"<% if cint(codEstado)=0 then %> selected="selected"<%end if%>>&nbsp;</option>
                                    <option value="1"<% if cint(codEstado)=1 then %> selected="selected"<%end if%>><%=objIdioma.getIdioma("Consultas_Texto11")%></option>
                                    <option value="2"<% if cint(codEstado)=2 then %> selected="selected"<%end if%>><%=objIdioma.getIdioma("Consultas_Texto12")%></option>
                                    <option value="3"<% if cint(codEstado)=3 then %> selected="selected"<%end if%>><%=objIdioma.getIdioma("Consultas_Texto13")%></option>
                                    <option value="4"<% if cint(codEstado)=4 then %> selected="selected"<%end if%>><%=objIdioma.getIdioma("Consultas_Texto14")%></option>
                                    <option value="5"<% if cint(codEstado)=5 then %> selected="selected"<%end if%>><%=objIdioma.getIdioma("Consultas_Texto15")%></option>
                                </select>
                    	    </div>
                        </div>                    
                        <div class="bloqueColumna" >                            				                                       
	                    	<div class="columnaTablaDerecha" style="width:80px;" >
		                        <%=objIdioma.getIdioma("Consultas_Texto16")%>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:230px;" >
                                <%pintarFiltrosAutoComplementar "CONTACTOS","CON_NOMBRE|CON_APELLIDO1|CON_APELLIDO2|CON_EMAIL|CON_NTARJETA","txtContacto",300 %>           
                    	    </div>
                        </div>         
						<div class="bloqueColumna">                            				                                       
	                    	<div class="columnaTablaDerecha" style="width:150px;" >
		                        <%=objIdioma.getIdioma("Consultas_Texto17")%>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:120px;" >
    		    	        	<input id="txtFechaDesde" name="txtFechaDesde" type="text" class="cajaTextoFecha" maxlength="250"  value="<%=request.Form("txtFechaDesde")%>"  />
                    	    </div>
                        </div>
                        <div class="bloqueColumna" >                            				                                       
	                    	<div class="columnaTablaDerecha" style="width:150px;" >
		                        <%=objIdioma.getIdioma("Consultas_Texto18")%>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:120px;" >
    		    	        	<input id="txtFechaHasta" name="txtFechaHasta" type="text" class="cajaTextoFecha" maxlength="250"  value="<%=request.Form("txtFechaHasta")%>"  />
                    	    </div>
                        </div>                      					
</div>

        <input type="text" id="txtPanelCerrado" name="txtPanelCerrado" value="<%=request.Form("txtPanelCerrado")%>" style="display:none;" />
        <input type="text" id="txtFiltroEstadoResumen" name="txtFiltroEstadoResumen" value="<%=request.Form("txtFiltroEstadoResumen")%>" style="display:none;" />					   																		

<div style="clear:both; width:100%; height:20px; margin:0; padding:0; float:left;"></div>


<script type="text/javascript">
	diferenciarFiltro();
</script>