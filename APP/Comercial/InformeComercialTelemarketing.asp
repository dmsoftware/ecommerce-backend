<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%
anoActual=request("an")
if anoActual="" or anoActual="0" then anoActual=year(date)

function Mes(Valor)
	select case Valor
	case 1
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_1"))
	case 2
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_2"))
	case 3
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_3"))
	case 4
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_4"))
	case 5
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_5"))
	case 6
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_6"))
	case 7
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_7"))
	case 8
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_8"))
	case 9
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_9"))
	case 10
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_10"))
	case 11
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_11"))
	case 12
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_12"))
	end select   
end function

Sub FormatoEuros(valor)
    if isnull(valor) then
        response.write "0 �"
    else
        response.Write FormatNumber(valor, 2) & " �"
    end if
end sub
%>

<script language=javascript>
function recarga(){
	tmp2=document.forms.fPaginacion2.comboVerAnos;
	document.forms.fPaginacion2.comboVerAnos.value = tmp2.options[tmp2.selectedIndex].value;
	tmp = document.forms.fPaginacion2.comboVerComerciales;
	document.forms.fPaginacion2.comboVerComerciales.value = tmp.options[tmp.selectedIndex].value;
	document.location='InformeComercialTelemarketing.asp?co=' + tmp.options[tmp.selectedIndex].value + '&an=' + tmp2.options[tmp2.selectedIndex].value;
	//}
}
</script>


<h1><%=objIdioma.getIdioma("InformeComercialTelemarketing_Titulo")%></h1>

<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto1")%>
<br>
<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto2")%> <a href="SeguimientoTelemarketing.asp"><b><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto3")%></b></a>.
<br><br />

<%'############################################################################################%>
<form name="fPaginacion2" method="POST" ID="Form1">
<div class="FiltradoCab">
	<div>
        <span class="cab2"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto4")%></span>
        <select name="comboVerAnos" onchange="javascript:recarga();" class="combos" id="Select1">
          <%sql="Select distinct(year(camp_finicio)) as Ano from campanas " 		
	        sql=sql & " order by year(camp_finicio) desc"
	        Set rstAnos= connCRM.AbrirRecordset(sql,0,1)
	        while not rstAnos.eof%>
              <option value="<%=rstAnos("Ano")%>"<%if cint(anoActual) = rstAnos("Ano") then response.Write(" selected")%>><%=rstAnos("Ano")%></option>
              <%rstAnos.movenext
	        wend
            rstAnos.cerrar%>
        </select>
        
        <span class="cab2"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto5")%></span>
        <select name="comboVerComerciales" onchange="javascript:recarga();" class="combos" ID="Select2">
            <%	sql="SELECT distinct(ANA_CODIGO),ANA_NOMBRE FROM ACCIONES_CLIENTE inner join ANALISTAS ON ANALISTAS.ANA_CODIGO=ACCIONES_CLIENTE.ACCI_SUCOMERCIAL"
                sql=sql & " WHERE year(acci_fecha)=" & anoActual
                sql=sql & " and acci_sutipo=1"
                sql=sql & " order by ANA_NOMBRE"
            Set rstComerciales= connCRM.AbrirRecordset(sql,0,1)
            %><option value="0"<%if request("co") = "0" then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option><%
            while not rstComerciales.eof%>
                <option value="<%=rstComerciales("ana_codigo")%>"<%if request("co") = cstr(rstComerciales("ana_codigo")) then response.Write(" selected")%>><%=rstComerciales("ana_nombre")%></option>
                <%rstComerciales.movenext
            wend
            rstComerciales.cerrar
            set rstComerciales=nothing%>
        </select>
    </div>
</div>
</form>
<%'############################################################################################%>
  
<span style="float:right; cursor:pointer;" class="todo"><img src="/dmcrm/app/imagenes/toggle.jpg" title="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto14")%>" alt="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto15")%>"/></span>
<TABLE cellSpacing="1" cellPadding="0" width="100%" ID="Table2">

<tr>     
	<td height="25" colspan=15 class="titularT" td>
	<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto6")%>
	</td>
</tr>
<tr>		       
	<td class="titular" height="25" colspan=2>
							            
    <%for i=1 to 12%>
        <td class="titular" align=center <%if i=month(date) and cint(anoActual)=year(date) then response.write (" style=""border:solid 4px #8fd400;"" ")%>><B><%=Mes(i)%></B></td>
    <%next%>
					
	<td height="25" colspan=4 align=center class="titular" style="background:#3468a3; color:#fff">
		<B><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto7")%></B>
	</td>		            		            		            
</tr>


<%'***********************************************************************************************%>
<%'***********************************************************************************************%>
<%'****************************** CAMPA�AS                 ***************************************%>
<%'***********************************************************************************************%>
<%'***********************************************************************************************%>

<%'Hallamos las campa�as donde el comercial ha participado llamando
'Buscamos aquellas que aunque no haya llamada el a�o actual, haya habido visita, propuesta o venta
sql = "select distinct(camp_codigo),camp_finicio,camp_nombre,tica_codigo,camp_codigo "
sql = sql & ",(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN tipos_campanas.TICA_TIPO ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as tica_tipo "
sql = sql & " from campanas, acciones_cliente as A, tipos_campanas, IDIOMAS_DESCRIPCIONES as D  "
sql = sql & " where "
sql = sql & " campanas.camp_codigo=A.acci_sucampana "
sql = sql & " and (campanas.camp_sutipo=tipos_campanas.tica_codigo or campanas.camp_sutipo IS NULL) "
sql = sql &  " and  ((tipos_campanas.tica_codigo=D.IDID_INDICE And UPPER(D.IDID_TABLA)='TIPOS_CAMPANAS' And UPPER(D.IDID_CAMPO)='TICA_TIPO' And D.IDID_SUIDIOMA='" & Idioma & "') Or D.IDID_TABLA IS NULL) "
sql = sql & " and  year(acci_fecha)=" & anoActual

sql = sql & " and ( "
'Buscamos las campa�as donde el comercial ha participado con una llamada
' a) llamadas
if request("co")<>"" and request("co")<>"0" then
    sql = sql & " (acci_sutipo=1 and acci_terminada = 1 and acci_sucomercial=" & request("co") & ")"
else
    sql = sql & " (acci_sutipo=1 and acci_terminada = 1)"
end if
' b) visitas
sql = sql & " or (acci_sutipo=4 "
sql = sql & " and exists (select acci_codigo "
sql = sql & " from acciones_cliente "
sql = sql & " where acci_sucampana=A.acci_sucampana"
sql = sql & " and acci_sucliente=A.acci_sucliente"
sql = sql & " and acci_proyecto=A.acci_proyecto"
sql = sql & " and acci_sutipo=1 " 'LLAMADA
if request("co")<>"" and request("co")<>"0" then
    sql = sql & " and acci_sucomercial=" & request("co") & ") )"
else
    sql = sql & " ) )"
end if
' b) propuestas
sql = sql & " or (acci_sutipo=6 "
sql = sql & " and exists (select acci_codigo "
sql = sql & " from acciones_cliente "
sql = sql & " where acci_sucampana=A.acci_sucampana"
sql = sql & " and acci_sucliente=A.acci_sucliente"
sql = sql & " and acci_proyecto=A.acci_proyecto"
sql = sql & " and acci_sutipo=1 " 'LLAMADA
if request("co")<>"" and request("co")<>"0" then
    sql = sql & " and acci_sucomercial=" & request("co") & ") )"
else
    sql = sql & " ) )"
end if
' b) ventas
sql = sql & " or (acci_sutipo=7 and (acci_tipo7finalizacion = 1 or acci_tipo7finalizacion=3) " 'Aprobada o preaprobada "
sql = sql & " and exists (select acci_codigo "
sql = sql & " from acciones_cliente "
sql = sql & " where acci_sucampana=A.acci_sucampana"
sql = sql & " and acci_sucliente=A.acci_sucliente"
sql = sql & " and acci_proyecto=A.acci_proyecto"
sql = sql & " and acci_sutipo=1 " 'LLAMADA
if request("co")<>"" and request("co")<>"0" then
    sql = sql & " and acci_sucomercial=" & request("co") & ") )"
else
    sql = sql & " ) )"
end if
sql = sql & " )"

sql=sql & " order by camp_finicio desc" 

Set rstCamp= connCRM.AbrirRecordset(sql,0,1)
'Inicializamos los contadores mensuales
arrintLlamadasMes=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrintVisitasMes=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrintNumPropuestasMes=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrintCantPropuestasMes=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrintNumVentasMes=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrintCantVentasMes=array(0,0,0,0,0,0,0,0,0,0,0,0,0)

while not rstCamp.eof%>
      
     <tr align="left" class="TR_<%=rstCamp("camp_codigo")%> campanas">
	  	
	  	<td VALIGN="TOP" class="celdas" id="celda1">
	  	    <a href="#" rel="TR_<%=rstCamp("camp_codigo")%>" style="float:right;" class="mostramos"><img src="/dmcrm/app/imagenes/toggle.jpg" height=20 title="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto16")%>" alt="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto16")%>"/></a>
            <b><%=ucase(rstCamp("camp_nombre"))%></b><br>
            <span>[<i><%=rstCamp("tica_tipo")%></i>]<br />
            <%'Hallamos el n�mero de llamadas de esta campa�a
            sql = "Select acci_codigo from acciones_cliente where acci_sucampana=" & rstCamp("camp_codigo") & " and acci_sutipo=1"
            Set rstCont= connCRM.AbrirRecordset(sql,3,3)%>
            <%=rstCont.recordcount%>&nbsp;<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto18")%>,
            <%rstCont.cerrar%>
            <%'Hallamos el n�mero de llamadas que faltan de esta campa�a
            sql = "Select acci_codigo from acciones_cliente where acci_sucampana=" & rstCamp("camp_codigo") & " and acci_sutipo=1 and acci_terminada=0"
            Set rstCont= connCRM.AbrirRecordset(sql,3,3)%>
            <%=rstCont.recordcount%>&nbsp;<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto19")%><br />
            <%rstCont.cerrar%>
            <%'Hallamos si la campa�a est� abierta o cerrada
            sql = "Select camp_cerrada from campanas where camp_codigo=" & rstCamp("camp_codigo")
            Set rstCont= connCRM.AbrirRecordset(sql,3,3)%>
            <%if not rstCont("camp_cerrada") then%>
                <img src="/dmcrm/app/imagenes/green.png" width="20" alt="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto16")%>" title="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto16")%>">
            <%else%>
                <img src="/dmcrm/app/imagenes/red.png" width="20" alt="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto17")%>" title="<%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto17")%>">
			<%end if%>
            <%rstCont.cerrar%>
            </span>
        </td>

        <td VALIGN="TOP" class="celdas2" id="celda">
            <span style="display:block; background:#fff; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto8")%><br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto9")%><br /></span>
            <span style="display:block; background:#fff; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto10") & " #"%><br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto10") & " �"%><br /></span>
            <span style="display:block; background:#fff; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto11") & " #"%><br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto11") & " �"%></span>
        </td>

        <%'Inicializamos contadores anuales
        intLlamadasAno=0
        intVisitasAno=0
        intNumPropuestas=0
        intCantPropuestas=0
        intNumVentas=0
        intCantVentas=0
        if cint(anoActual)=year(date) then 
            blnPintarCampana=false
        else
            blnPintarCampana=true
        end if
        
        for i=1 to 12%>
        
               <%intNumLlamadasTerminadas=0
               intNumVisitas=0
               intNumPropuestasMes=0
               intCantPropuestasMes=0
               intNumPropuestasAprobadasMes=0
               intCantVentasMes=0%>

                <%'Hallamos LLAMADAS TERMINADAS					     
				sql="select acci_codigo from acciones_cliente where acci_sucampana= " & rstCamp("camp_codigo")
				if anoActual<>"" and anoActual<>0 then
					sql=sql & " and  year(acci_fecha)=" & anoActual 
				end if				
                sql = sql & " and month(acci_fecha)=" & i
				sql = sql & " and acci_sutipo=1 and acci_terminada = 1 "
                if request("co")<>"" and request("co")<>"0" then
                    sql = sql & " and acci_sucomercial=" & request("co") 
                end if
				Set rstNum= connCRM.AbrirRecordset(sql,3,1)
                intNumLlamadasTerminadas=rstNum.recordcount
                rstNum.cerrar
                intLlamadasAno=intLlamadasAno+intNumLlamadasTerminadas
                arrintLlamadasMes(i)=arrintLlamadasMes(i)+intNumLlamadasTerminadas%>        

				<%'Hallamos VISITAS (TERMINADAS O NO) QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA					     
				sql="select count(*) as numReg from acciones_cliente as A where acci_sucampana= " & rstCamp("camp_codigo")
				if anoActual<>"" and anoActual<>0 then
					sql=sql & " and  year(acci_fecha)=" & anoActual 
				end if				
                sql = sql & " and month(acci_fecha)=" & i
				sql = sql & " and acci_sutipo=4"
                sql = sql & " and exists (select acci_codigo "
                sql = sql & " from acciones_cliente "
                sql = sql & " where acci_sucampana=A.acci_sucampana"
                sql = sql & " and acci_sucliente=A.acci_sucliente"
                sql = sql & " and acci_proyecto=A.acci_proyecto"
                sql = sql & " and acci_sutipo=1 " 'LLAMADA
                if request("co")<>"" and request("co")<>"0" then
                    sql = sql & " and acci_sucomercial=" & request("co") & ")"
                else
                    sql = sql & ")"
                end if
				Set rstNum= connCRM.AbrirRecordset(sql,3,1)
				intNumVisitas=rstNum("numReg") 
                intVisitasAno=intVisitasAno+rstNum("numReg")
                arrintVisitasMes(i)=arrintVisitasMes(i)+rstNum("numReg")
                rstNum.cerrar%>

                <%'NUMERO DE PROPUESTAS (TERMINADAS O NO) QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA	
							     
				sql="select count(*)as numReg,sum(acci_tipo6cantidad) as cantidad from acciones_cliente as A where acci_sucampana= " & rstCamp("camp_codigo")
				if anoActual<>"" and anoActual<>0 then
					sql=sql & " and  year(acci_fecha)=" & anoActual 
				end if		
                sql = sql & " and month(acci_fecha)=" & i
				sql = sql & " and acci_sutipo=6"
                sql = sql & " and exists (select acci_codigo "
                sql = sql & " from acciones_cliente "
                sql = sql & " where acci_sucampana=A.acci_sucampana"
                sql = sql & " and acci_sucliente=A.acci_sucliente"
                sql = sql & " and acci_proyecto=A.acci_proyecto"
                sql = sql & " and acci_sutipo=1 "
                if request("co")<>"" and request("co")<>"0" then
                    sql = sql & " and acci_sucomercial=" & request("co") & ")"
                else
                    sql = sql & ")"
                end if
				Set rstNum= connCRM.AbrirRecordset(sql,3,1)                		
                intNumPropuestasMes=rstNum("numReg")
                intNumPropuestas=intNumPropuestas+rstNum("numReg")
                arrintNumPropuestasMes(i)=arrintNumPropuestasMes(i)+rstNum("numReg")
                if not isnull(rstNum("cantidad")) then intCantPropuestasMes=rstNum("cantidad"):intCantPropuestas=intCantPropuestas+rstNum("cantidad"):arrintCantPropuestasMes(i)=arrintCantPropuestasMes(i)+rstNum("cantidad")
                rstNum.cerrar			
				%> 
                
				<%'NUMERO DE PROPUESTAS APROBADAS QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA					     
				sql="select count(*)as numReg,sum(acci_tipo6cantidad) as cantidad,sum(acci_tipo7Gastos) as gastos from acciones_cliente as A where acci_sucampana= " & rstCamp("camp_codigo")
				if anoActual<>"" and anoActual<>0 then
					sql=sql & " and  year(acci_fecha)=" & anoActual 
				end if		
                sql = sql & " and month(acci_fecha)=" & i
				sql = sql & " and acci_sutipo=7"
                sql = sql & " and (acci_tipo7finalizacion = 1 or acci_tipo7finalizacion=3)" 'Aprobada o preaprobada
                sql = sql & " and exists (select acci_codigo "
                sql = sql & " from acciones_cliente "
                sql = sql & " where acci_sucampana=A.acci_sucampana"
                sql = sql & " and acci_sucliente=A.acci_sucliente"
                sql = sql & " and acci_proyecto=A.acci_proyecto"
                sql = sql & " and acci_sutipo=1 "
                if request("co")<>"" and request("co")<>"0" then
                    sql = sql & " and acci_sucomercial=" & request("co") & ")"
                else
                    sql = sql & ")"
                end if
				Set rstNum= connCRM.AbrirRecordset(sql,3,1)                		
				intNumPropuestasAprobadasMes=rstNum("numReg")
                intNumVentas=intNumVentas+rstNum("numReg")
                arrintNumVentasMes(i)=arrintNumVentasMes(i)+rstNum("numReg")
                if not isnull(rstNum("cantidad")) then intCantVentasMes=rstNum("cantidad"):intCantVentas=intCantVentas+rstNum("cantidad"):arrintCantVentasMes(i)=arrintCantVentasMes(i)+rstNum("cantidad")
                if not isnull(rstNum("gastos")) then intCantVentasMes=intCantVentasMes-rstNum("gastos"):intCantVentas=intCantVentas-rstNum("gastos"):arrintCantVentasMes(i)=arrintCantVentasMes(i)-rstNum("gastos")		
                rstNum.cerrar			
				
                'Comprobamos si la campa�a tiene datos en el mes actual o en el anterior
                if (i=month(date) or i=month(date)-1) and cint(anoActual)=year(date) then
                    if intNumLlamadasTerminadas<>0 then blnPintarCampana=true
                    if intNumVisitas<>0 then blnPintarCampana=true
                    if intNumPropuestasMes<>0 then blnPintarCampana=true
                    if intCantPropuestasMes<>0 then blnPintarCampana=true
                    if intNumPropuestasAprobadasMes<>0 then blnPintarCampana=true
                    if intCantVentasMes<>0 then blnPintarCampana=true
                end if
                %>
                                                      		  
	  	    <td align=right VALIGN="TOP" id="celda1"  class="celdas2" <%if i=month(date) and cint(anoActual)=year(date) then response.write (" style=""border:solid 4px #8fd400;"" ")%>>

        		<span style="display:block; background:#fff; padding:0 3px;">		
                <%=intNumLlamadasTerminadas%>
                <br /></span>

                <span style="display:block; background:#cdcdcd; padding:0 3px;">
                <%=intNumVisitas%>
                <br /></span>

                <span style="display:block; background:#fff; padding:0 3px;">
                <%=intNumPropuestasMes%>
                <br /></span>

                <span style="display:block; background:#cdcdcd; padding:0 3px;">
                <%FormatoEuros(intCantPropuestasMes)%>
                <br /></span>

                <span style="display:block; background:#fff; padding:0 3px;">
                <%=intNumPropuestasAprobadasMes%>
                <br /></span>
                <span style="display:block; background:#cdcdcd; padding:0 3px;">
                <%FormatoEuros(intCantVentasMes)%>
                <br /></span>
	  	    </td>
        <%next%>
			
	  	<td align=right VALIGN="TOP" class="celdas2" style="background:#3468a3;" id="celda1">
            <span style="display:block; color:#fff; padding:0 3px;"><b><%=intLlamadasAno%><br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=intVisitasAno%><br /></span>
            <span style="display:block; color:#fff; padding:0 3px;"><%=intNumPropuestas%><br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;"><%FormatoEuros(intCantPropuestas)%><br /></span>
            <span style="display:block; color:#fff; padding:0 3px;"><%=intNumVentas%><br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;"><%FormatoEuros(intCantVentas)%></span></b>
	  	</td>
	  		  	
	</tr>     
	
    <%'Ocultamos la campa�a si es necesario
    if blnPintarCampana=false then
        
        strToggle=strToggle & "$("".TR_" & rstCamp("camp_codigo") & " td span"").toggle();"

    end if%>

    <%rstCamp.movenext
wend
rstCamp.cerrar%>

<tr align="left">
	  	
	<td VALIGN="TOP" style="background:#ff6600; border-top: solid 5px #000;" id="celda1">
	  	<b><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto12")%></b>
    </td>

    <td VALIGN="TOP" class="celdas2" id="celda" style="background:#ff6600; border-top: solid 5px #000;">
        <span style="display:block; color:#fff;  padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto8")%><br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto9")%><br /></span>
        <span style="display:block; color:#fff;  padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto10")%> #<br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto10")%> �<br /></span>
        <span style="display:block; color:#fff; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto11")%> #<br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto11")%> �</span>
    </td>

    <%for i=1 to 12%>			  
	  	<td align=right VALIGN="TOP" id="celda1" class="celdas2" style="background:#ff6600; border-top: solid 5px #000;<%if i=month(date) and cint(anoActual)=year(date) then response.write (" border-left:solid 4px #8fd400;border-right:solid 4px #8fd400;border-bottom:solid 4px #8fd400;"" ")%>">

        	<span style="display:block; color:#fff; padding:0 3px;">		
            <%'Hallamos LLAMADAS TERMINADAS					     
			response.Write arrintLlamadasMes(i)
            intTotalLlamadasAno=intTotalLlamadasAno+arrintLlamadasMes(i)%>
            <br /></span>

            <span style="display:block; background:#cdcdcd; padding:0 3px;">
			<%'Hallamos VISITAS (TERMINADAS O NO) QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA
			response.Write arrintVisitasMes(i)
            intTotalVisitasAno=intTotalVisitasAno+arrintVisitasMes(i)%>
            <br /></span>

            <span style="display:block; color:#fff; padding:0 3px;">
            <%'NUMERO DE PROPUESTAS (TERMINADAS O NO) QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA				                    		
            response.Write arrintNumPropuestasMes(i)
            intNumPropuestasAno=intNumPropuestasAno+arrintNumPropuestasMes(i)%>
            <br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;">
            <%FormatoEuros(arrintCantPropuestasMes(i))
            intCantPropuestasAno=intCantPropuestasAno+arrintCantPropuestasMes(i)%>
            <br /></span>

            <span style="display:block; color:#fff; padding:0 3px;">
			<%response.Write arrintNumVentasMes(i)
            intNumVentasAno=intNumVentasAno+arrintNumVentasMes(i)%>
            <br /></span>
            <span style="display:block; background:#cdcdcd; padding:0 3px;">
            <%FormatoEuros(arrintCantVentasMes(i))
            intCantVentasAno=intCantVentasAno+arrintCantVentasMes(i)%>
            <br /></span>
	  	</td>
    <%next%>
			
	<td align=right VALIGN="TOP" class="celdas2" id="celda1" style="background:#c44e00; border-top: solid 5px #000;">
        <span style="display:block; color:#fff; padding:0 3px;"><b><%=intTotalLlamadasAno%><br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=intTotalVisitasAno%><br /></span>
        <span style="display:block; color:#fff; padding:0 3px;"><%=intNumPropuestasAno%><br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%FormatoEuros(intCantPropuestasAno)%><br /></span>
        <span style="display:block; color:#fff; padding:0 3px;"><%=intNumVentasAno%><br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%FormatoEuros(intCantVentasAno)%></span></b>
	</td>
	  		  	
</tr> 

<tr align="left">
	  	
	<td VALIGN="TOP" class="celdas" id="celda1" style="background:#ffa264; border-top: solid 5px #333;">
	  	<b><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto13")%></b>
    </td>

    <td VALIGN="TOP" class="celdas2" id="celda" style="background:#ffa264; border-top: solid 5px #333;">
        <span style="display:block; color:#000; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto8")%><br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto9")%><br /></span>
        <span style="display:block; color:#000; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto10")%> #<br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;"><%=objIdioma.getIdioma("InformeComercialTelemarketing_Texto11")%> #<br /></span>
    </td>

    <%intNumDiasAno=0
    for i=1 to 12
        'Hallamos los d�as de trabajo del comercial cada mes.  Si el mes es el mes actual hallamos los d�as hasta hoy
        if ( cint(anoActual)=year(date) and i<=month(date) ) or ( cint(anoActual)<year(date) )then
                sql = "Select count(*) from rh_calendariolaboral where rhc_sudepartamento=1"
                if i=month(date) and cint(anoActual)=year(date) then
                    sql = sql & " and year(rhc_fecha)=" & anoActual & " and month(rhc_fecha)=" & i & " and day(rhc_fecha)<=" & day(date)
                else
                    sql = sql & " and year(rhc_fecha)=" & anoActual & " and month(rhc_fecha)=" & i
                end if
                sql = sql & " and rhc_horastrabajo>0"
		        Set rstDias= connCRM.AbrirRecordset(sql,0,1)

                'S�lo miramos la tabla rh_dias si estamos viendo una persona concreta
                'En realidad las medias no van a salir muy bien... deber�a ser una media de las medias de todos.  Pero es c�lculo es muy dif�cil. La media de todos va a ser una aproximaci�n.
                if request("co")<>"" and request("co")<>"0" then

                    sql = "Select count(*) from rh_dias "
                    sql = sql & " where rhd_supersonal=" & request("co")
                    if i=month(date) and cint(anoActual)=year(date) then
                        sql = sql & " and year(rhd_sudia)=" & anoActual & " and month(rhd_sudia)=" & i & " and day(rhd_sudia)<=" & day(date)
                    else
                        sql = sql & " and year(rhd_sudia)=" & anoActual & " and month(rhd_sudia)=" & i
                    end if
                    Set rstDias2= connCRM.AbrirRecordset(sql,0,1)
                    intNumDiasMes=rstDias(0)-rstDias2(0)
                    intNumDiasAno=intNumDiasAno+rstDias(0)-rstDias2(0)
                    rstDias2.cerrar                
                else
                    intNumDiasMes=rstDias(0)
                    intNumDiasAno=intNumDiasAno+rstDias(0)
                end if
                rstDias.cerrar
         else
                'El mes es posterior al mes actual
                intNumDiasMes=30
         end if%>			  
	  	<td align=right VALIGN="TOP"  id="celda1" style="background:#ffa264; border-top: solid 5px #333;<%if i=month(date) and cint(anoActual)=year(date) then response.write (" border-left:solid 4px #8fd400;border-right:solid 4px #8fd400;border-bottom:solid 4px #8fd400;"" ")%>">
        	<span style="display:block; color:#000; padding:0 3px;">		
            <%'Hallamos LLAMADAS TERMINADAS			
			if intNumDiasMes>0 then
				response.Write round(arrintLlamadasMes(i)/intNumDiasMes,2)
			else
				response.Write round(arrintLlamadasMes(i),2)	
			end if	
			
			%>
            <br /></span>

            <span style="display:block; background:#cdcdcd; padding:0 3px;">
			<%'Hallamos VISITAS (TERMINADAS O NO) QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA
			if intNumDiasMes>0 then
				response.Write round(arrintVisitasMes(i)/intNumDiasMes,2)
			else
				response.Write round(arrintVisitasMes(i),2)			
			end if	%>
            <br /></span>

            <span style="display:block; color:#000; padding:0 3px;">
            <%'NUMERO DE PROPUESTAS (TERMINADAS O NO) QUE HAN SURGIDO DE ALGUNA LLAMADA DE AMAIA				                    		
			if intNumDiasMes>0 then
            	response.Write round(arrintNumPropuestasMes(i)/intNumDiasMes,2)
			else
            	response.Write round(arrintNumPropuestasMes(i),2)			
			end if%>
            <br /></span>

            <span style="display:block; background:#cdcdcd; padding:0 3px;">
			<%if intNumDiasMes>0 then
				response.Write round(arrintNumVentasMes(i)/intNumDiasMes,2)
			else
				response.Write round(arrintNumVentasMes(i),2)
			end if%>
            <br /></span>

	  	</td>
    <%next%>
			
	<td align=right VALIGN="TOP" class="celdas2" id="celda1"  style="background:#e18f59; border-top: solid 5px #333;">
        <span style="display:block; color:#000; padding:0 3px;"><b>
        <%if intNumDiasAno>0 then
			response.Write(round(intTotalLlamadasAno/intNumDiasAno,2))
        else   
			response.Write(round(intTotalLlamadasAno,2))		
        end if%>
        <br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;">
        <%if intNumDiasAno>0 then
			response.Write(round(intTotalVisitasAno/intNumDiasAno,2))
        else   
			response.Write(round(intTotalVisitasAno,2))		
        end if%>        
        <br /></span>
        <span style="display:block; color:#000; padding:0 3px;">
        <%if intNumDiasAno>0 then
			response.Write(round(intNumPropuestasAno/intNumDiasAno,2))
        else   
			response.Write(round(intNumPropuestasAno,2))		
        end if%>                
        <br /></span>
        <span style="display:block; background:#cdcdcd; padding:0 3px;">
        <%if intNumDiasAno>0 then
			response.Write(round(intNumVentasAno/intNumDiasAno,2))
        else   
			response.Write(round(intNumVentasAno,2))		
        end if%>                		
        <br /></span></b>
	</td>
	  		  	
</tr>

</table>


<%'Ocultamos con jQuery aquellas campa�as que no tienen movimiento el mes actual y el mes anterior %>

 <script type="text/javascript">
     $(document).ready(function () {
         //$(".TR_184 td span").toggle();
         //$(".TR_183 td span").toggle();
         <%=strToggle%>

         $(".mostramos").click(function () {
             linkM = "." + $(this).attr("rel");
             //$(linkM + " td span").css('display', 'block');
             $(linkM + " td span").toggle();
             return false;
         });

         $(".todo").click(function () {
            if ($(this).hasClass("activo")) {
                $(".campanas td span").css("display","block");
                $(this).removeClass("activo");
                return false;
            }else{
                $(".campanas td span").css("display","none");
                $(this).addClass("activo");
                return false;
             }
         });

     });
    </script>



<%'############################################################################################%>
<%'############################################################################################%>


<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->

