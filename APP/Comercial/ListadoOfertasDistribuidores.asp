<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%
Sub FormatoEuros(valor)
 response.Write FormatNumber(valor, 2) & " �"
end sub
%>






<script type="text/javascript">
			/***** Funci�n para generar un id aleatorio para evitar la cache *****/
			function generarGUID()
			{
				var result, i, j;
				result = '';
				for(j=0; j<32; j++)
				{
					if( j == 8 || j == 12 || j == 16 || j == 20)
						result = result + '-';
					i = Math.floor(Math.random()*16).toString(16).toUpperCase();
					result = result + i;
				}
				return result;
			}
			
			
			$(document).ready(function() {

				oTable = $('#tablaListado').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt"},
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 100,
					"aoColumns": [ 
						null,
						null,
						null,
						null,
						{ "sType": "date" },
						{ "sType": "formatted-num" },						
						{ "sType": "formatted-num" },
						null

						]
						
				});										
			});
		

		
		</script>







<!-- CABECERA DE LA SELECCION -->

<h1><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Titulo")%></h1>
<%'Bucle de distribuidores
sql="select * from clientes where clie_distribuidor=1"
sql = sql & " order by clie_nombre "
Set rstDistribuidores= connCRM.AbrirRecordset(sql,0,1)%>

<div id="contenedorSeleccion">
<table id="tablaListado" name="tablaListado" width="100%" cellpadding=3 cellspacing=1>
    <thead> 
		<tr>
        	<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto1")%></th>   	
	   		<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto2")%></th>
            <th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto3")%></th>
	   		<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto4")%></th>
	  		<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto5")%></th>
	   		<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto6")%></th>	
	  		<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto7")%></th>
	  		<th valign="top" id="celda1"><%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto8")%></th>	  	
		</tr>
    </thead>
    <tbody>
        
<%cont=0
while not rstDistribuidores.eof
	cont=cont+1
	
	if cont=2 then
		cont = 0
		colorDistri= "#e3e3e3"
	else
		colorDistri= "#f1f1f1"
	end if
	   %>

	

        <%'Hallamos primero los clientes de este distribuidor que tenemos ofertas
        sql="select *,(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN TIPOS_CAMPANAS.TICA_TIPO ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TICA_TIPO_IDI "
		sql = sql & " from (((((clientes inner join acciones on clientes.clie_codigo=acciones.acci_sucliente ) inner join tipos_aux_propuesta_finalizacion on acciones.acci_tipo7finalizacion=tipos_aux_propuesta_finalizacion.tapf_codigo) "
        sql = sql & " inner join campanas on acciones.acci_sucampana=campanas.camp_codigo ) inner join TIPOS_CAMPANAS on CAMPANAS.CAMP_SUTIPO=TIPOS_CAMPANAS.TICA_CODIGO) "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (TIPOS_CAMPANAS.TICA_CODIGO=D.IDID_INDICE And UPPER(D.IDID_TABLA)='TIPOS_CAMPANAS' And UPPER(D.IDID_CAMPO)='TICA_TIPO' And D.IDID_SUIDIOMA='" & Idioma & "')) "			
        sql = sql & " where clientes.clie_sudistribuidor=" & rstDistribuidores("clie_codigo")
        sql = sql & " and acciones.acci_proyecto<>'' "
        sql = sql & " and acciones.acci_sutipo=7 "
        sql = sql & " order by acci_sucliente, acci_fecha "
        Set rstClientesDistrubuidor= connCRM.AbrirRecordset(sql,0,1)
        while not rstClientesDistrubuidor.eof%>
             <tr class="celdaBlanca">  
             	<td valign="top" style="background:<%=colorDistri%>">
                	<strong><%=rstDistribuidores("clie_nombre")%></strong>
                </td>
	   	        <td valign="top">
	  	            <%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto9")%>
                    <br /><%=ucase(rstClientesDistrubuidor("clie_nombre"))%>
	  	        </td>
	   	        <td valign="top">
	  	            <%=rstClientesDistrubuidor("acci_proyecto")%>
	  	        </td>
	   	        <td valign="top">
	  	            <%=UCASE(rstClientesDistrubuidor("camp_nombre"))%><br />
                    [<I><%=rstClientesDistrubuidor("TICA_TIPO_IDI")%></I>]
	  	        </td>
	  	        <td align="center" valign="top">
	  	            <%=rstClientesDistrubuidor("acci_fecha")%>
	  	        </td>
	   	        <td align="right" valign="top">
	  	            <%FormatoEuros(rstClientesDistrubuidor("acci_tipo6Cantidad"))%>
	  	        </td>
	  	        <td align="right" valign="top">
	  	            <%FormatoEuros(rstClientesDistrubuidor("acci_tipo7gastos"))%>
	  	        </td>
	  	        <td valign="top" align="center">
                	<span style="display:none;"><%=rstClientesDistrubuidor("tapf_descripcion")%></span>
	  	            <%if rstClientesDistrubuidor("tapf_descripcion")="Aprobada" then%>
                    	<img src="../imagenes/green.png" width="30">
                    <%else%>
                    	<img src="../imagenes/red.png" width="30">
                    <%end if%>
	  	        </td>	  	
	        </tr>            
            <%rstClientesDistrubuidor.movenext
        wend
        rstClientesDistrubuidor.cerrar%>        
       
       <%'Segundo: Seleccionamos los DISTRIBUIDORES y las acciones de tipo 7 asociadas a ellos
        sql="select *,(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN TIPOS_CAMPANAS.TICA_TIPO ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TICA_TIPO_IDI "
		sql = sql & " from (((((clientes inner join acciones on clientes.clie_codigo=acciones.acci_sucliente ) inner join tipos_aux_propuesta_finalizacion on acciones.acci_tipo7finalizacion=tipos_aux_propuesta_finalizacion.tapf_codigo) "
        sql = sql & " inner join campanas on acciones.acci_sucampana=campanas.camp_codigo ) inner join TIPOS_CAMPANAS on CAMPANAS.CAMP_SUTIPO=TIPOS_CAMPANAS.TICA_CODIGO) "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (TIPOS_CAMPANAS.TICA_CODIGO=D.IDID_INDICE And UPPER(D.IDID_TABLA)='TIPOS_CAMPANAS' And UPPER(D.IDID_CAMPO)='TICA_TIPO' And D.IDID_SUIDIOMA='" & Idioma & "')) "					
        sql = sql & " where clientes.clie_codigo=" & rstDistribuidores("clie_codigo")
        sql = sql & " and acciones.acci_proyecto<>'' "
        sql = sql & " and acciones.acci_sutipo=7 "
        sql = sql & " order by clie_nombre, acci_fecha "
        Set rstOfertasDistribuidores= connCRM.AbrirRecordset(sql,0,1)
        while not rstOfertasDistribuidores.eof%>
             <tr class="celdaBlanca">
             	<td valign="top" style="background:<%=colorDistri%>">
                	<strong><%=rstDistribuidores("clie_nombre")%></strong>
                </td>
	   	        <td valign="top">
	  	            <%=objIdioma.getIdioma("ListadoOfertasDistribuidores_Texto10")%>
	  	        </td>      	
	   	        <td valign="top">
	  	            <%=rstOfertasDistribuidores("acci_proyecto")%>
	  	        </td>
	   	        <td valign="top">
	  	            <%=UCASE(rstOfertasDistribuidores("camp_nombre"))%><br />
                    [<I><%=rstOfertasDistribuidores("TICA_TIPO_IDI")%></I>]
	  	        </td>
	  	        <td align="center" valign="top">
	  	            <%=rstOfertasDistribuidores("acci_fecha")%>
	  	        </td>
	   	        <td align="right" valign="top">
	  	            <%FormatoEuros(rstOfertasDistribuidores("acci_tipo6Cantidad"))%>
	  	        </td>
	  	        <td align="right" valign="top">
	  	            <%FormatoEuros(rstOfertasDistribuidores("acci_tipo7gastos"))%>
	  	        </td>
	  	        <td valign="top" align="center">
                	<span style="display:none;"><%=rstOfertasDistribuidores("tapf_descripcion")%></span>
	  	            <%if rstOfertasDistribuidores("tapf_descripcion")="Aprobada" then%>
                    	<img src="../imagenes/green.png" width="30">
                    <%else%>
                    	<img src="../imagenes/red.png" width="30">
                    <%end if%>
	  	        </td>	  	
	        </tr>
            <%rstOfertasDistribuidores.movenext
        wend
        rstOfertasDistribuidores.cerrar%>

   <%rstDistribuidores.movenext
wend
rstDistribuidores.cerrar
%>
	</tbody>    
  </table>
</div>


<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->