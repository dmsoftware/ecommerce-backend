
<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->


<%
Sub FormatoEuros(valor)
 response.Write FormatNumber(valor, 2) & " �"
end sub
%>


<style type="text/css">
<!--
.sca {
	float: right;
}
.filaVaciaTabla { color:#ff6600; border:none; background-color:#ff6600; height:2px;  margin:0px; padding:0px; }
.filaVaciaTabla td { color:#ff6600; border:none!important; background-color:#ff6600; height:0px;  margin:0px; padding:0px;}
.filaVaciaTabla:hover {  color:#ff6600; border:none; background-color:#ff6600; height:2px;  margin:0px; padding:0px;}
-->
</style>

<script type="text/javascript">
    /***** Funci�n para generar un id aleatorio para evitar la cache *****/
    function generarGUID() {
        var result, i, j;
        result = '';
        for (j = 0; j < 32; j++) {
            if (j == 8 || j == 12 || j == 16 || j == 20)
                result = result + '-';
            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
            result = result + i;
        }
        return result;
    }
	
	
	$(".cEmergente").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'onClosed'		        : function() {
							    recargarAlCerrar();
						}						
					}).trigger("click"); 
					return false; 	
	});

    $(document).ready(function () {

        oTable = $('#tablaListado').dataTable({
            "oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
            "bAutoWidth": false,
            "sPaginationType": "full_numbers",
            "bFilter": true,
            "bProcessing": true,
            "bSort": true,
            "iDisplayLength": 100,
			"aaSorting": [[ 0, "asc" ],[ 1, "asc" ]/*,[ 4, "desc" ]*/],			
            "aoColumns": [
				{ "bVisible": false },				
			    { "sType": "uk_date" },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },				
				null,
				null,
				null,
				null,
				null,
				null,
                {"bSortable": false}
				]

        });
		
    });
</script>



<h1><%=objIdioma.getIdioma("ResumenPendiente_Titulo")%></h1>
<%=objIdioma.getIdioma("ResumenPendiente_Texto1")%>
<br><br />

<%'############################################################################################%>
<%intCodigoComercial=cint(request("co"))
if intCodigoComercial=0 then intCodigoComercial=session("codigoUsuario")%>
<script language=javascript>
    function recarga() {
        tmp2 = document.forms.fPaginacion2.comboVerComercial;
        document.forms.fPaginacion2.comboVerComercial.value = tmp2.options[tmp2.selectedIndex].value;
        document.location = 'ResumenPendiente.asp?co=' + tmp2.options[tmp2.selectedIndex].value;
        //}
    }
</script>

<form name="fPaginacion2" method="POST" ID="Form1">
    <div class="FiltradoCab">
    <span class="cab1"><%=objIdioma.getIdioma("ResumenPendiente_Texto2")%></span><br />
	    <div>
            <select name="comboVerComercial" onchange="javascript:recarga();" class="combos" ID="Select1">
                <%sql="Select ana_nombre,ana_codigo  from analistas "
                sql = sql & " where ana_directorcuenta = 1" 		
                sql=sql & " order by ana_nombre"
                Set rstComerciales= connCRM.AbrirRecordset(sql,0,1)
                while not rstComerciales.eof%>
                    <option value="<%=rstComerciales("ana_codigo")%>"<%if intCodigoComercial = rstComerciales("ana_codigo") then response.Write(" selected")%>><%=rstComerciales("ana_nombre")%></option>
                    <%rstComerciales.movenext
                wend
                rstComerciales.cerrar
                set rstComerciales=nothing%>
            </select>
	    </div>
    </div>
</form>
<%'############################################################################################%>



<div id="contenedorSeleccion">
    <table id="tablaListado" name="tablaListado" width="100%" cellpadding=3 cellspacing=1 bgcolor=ffffff>
        <thead> 
        <tr class="titularT">
            <th valign="middle" id="celda1">ordenFecha</th>        
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResumenPendiente_Texto3")%></th>
            <th valign="middle" id="celda1" style="cursor:default;">&nbsp;</th>
            <th valign="middle" id="celda1" style="cursor:default;">&nbsp;</th>
            <th valign="middle" id="celda1" style="cursor:default;">&nbsp;</th>
            <th valign="middle" id="celda1" ><%=objIdioma.getIdioma("ResumenPendiente_Texto4")%></th>
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResumenPendiente_Texto5")%></th>
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResumenPendiente_Texto6")%></th>
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResumenPendiente_Texto7")%></th>
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResumenPendiente_Texto8")%></th>
            <th valign="middle" id="celda1"><%=objIdioma.getIdioma("ResumenPendiente_Texto9")%></th>
            <th valign="middle" id="celda1" style="cursor:default;">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <%
		

		
sql = "Select top 500 acci_codigo,acci_sucliente,"
sql=sql & " (CASE WHEN convert(varchar, acci_fecha, 3) = convert(varchar, getDate(),3) And (not ACCI_HORAINICIO IS NULL And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') "
sql=sql & " And ACCI_HORAINICIO < CONVERT(VARCHAR(8),GETDATE(),108) THEN 1 ELSE"
sql=sql & " (CASE WHEN convert(varchar, acci_fecha, 3) = convert(varchar, getDate(),3) "
sql=sql & " And (not ACCI_HORAINICIO IS NULL And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') And ACCI_HORAINICIO >= CONVERT(VARCHAR(8),GETDATE(),108) THEN 2 ELSE"
sql=sql & " (CASE WHEN  convert(varchar, acci_fecha, 3) = convert(varchar, getDate(),3) And ( ACCI_HORAINICIO IS NULL Or LTRIM(RTRIM(ACCI_HORAINICIO)) = '') THEN 3 ELSE"
sql=sql & " (CASE WHEN acci_fecha < getDate() And (not ACCI_HORAINICIO IS NULL And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') THEN 4 ELSE"
sql=sql & " (CASE WHEN acci_fecha < getDate() And  (ACCI_HORAINICIO IS NULL Or LTRIM(RTRIM(ACCI_HORAINICIO)) = '') THEN 5 ELSE"
sql=sql & " (CASE WHEN acci_fecha > getDate() And (not ACCI_HORAINICIO IS NULL And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') THEN 6 ELSE"
sql=sql & " (CASE WHEN acci_fecha > getDate() And (ACCI_HORAINICIO IS NULL Or LTRIM(RTRIM(ACCI_HORAINICIO)) = '') THEN 7 ELSE 0 END) END) END) END) END) END) END) as ordenFecha,"
sql=sql & " acci_fecha, tiac_nombre,clie_nombre,camp_nombre,acci_proyecto,acci_sucontacto,acci_contacto,con_nombre,con_apellido1,"
sql=sql & " acci_comentario,clie_calidadregistro,ACCI_HORAINICIO ,(CASE WHEN LTRIM(RTRIM(CAST(IDID_DESCRIPCION AS varchar(max)))) = '' Or D.IDID_DESCRIPCION IS NULL THEN"
sql=sql & " tipos_accion.TIAC_NOMBRE ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TIAC_NOMBRE_DES"
sql=sql & " from ((((acciones_cliente LEFT join clientes on acciones_cliente.acci_sucliente=clientes.clie_codigo ) "
sql=sql & " inner join tipos_accion on acciones_cliente.acci_sutipo=tipos_accion.tiac_codigo ) "
sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (tipos_accion.TIAC_CODIGO=D.IDID_INDICE And UPPER(D.IDID_TABLA)='TIPOS_ACCION' "
sql=sql & " And UPPER(D.IDID_CAMPO)='TIAC_NOMBRE' And D.IDID_SUIDIOMA='es') )  "
sql=sql & " inner join campanas on acciones_cliente.acci_sucampana=campanas.camp_codigo ) "
sql=sql & " left join contactos on acciones_cliente.acci_sucontacto=contactos.con_codigo  "
sql=sql & " where (acci_sucomercial=" & intCodigoComercial & " Or acci_sucomercial2=" & intCodigoComercial & " Or acci_sucomercial3=" & intCodigoComercial & " Or acci_sucomercial4=" & intCodigoComercial & " ) " 
sql=sql & " and acci_terminada = 0  Order by (CASE WHEN convert(varchar, acci_fecha, 3) = convert(varchar, getDate(),3) "
sql=sql & " And (not ACCI_HORAINICIO IS NULL And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') And ACCI_HORAINICIO < CONVERT(VARCHAR(8),GETDATE(),108) THEN 1 ELSE"
sql=sql & " (CASE WHEN convert(varchar, acci_fecha, 3) = convert(varchar, getDate(),3) And (not ACCI_HORAINICIO IS NULL "
sql=sql & " And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') And ACCI_HORAINICIO >= CONVERT(VARCHAR(8),GETDATE(),108) THEN 2 ELSE "
sql=sql & " (CASE WHEN  convert(varchar, acci_fecha, 3) = convert(varchar, getDate(),3) "
sql=sql & " And ( ACCI_HORAINICIO IS NULL Or LTRIM(RTRIM(ACCI_HORAINICIO)) = '') THEN 3 ELSE (CASE WHEN acci_fecha < getDate() And (not ACCI_HORAINICIO IS NULL"
sql=sql & " And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') THEN 4 ELSE (CASE WHEN acci_fecha < getDate() And  (ACCI_HORAINICIO IS NULL Or LTRIM(RTRIM(ACCI_HORAINICIO)) = '') THEN 5 ELSE"
sql=sql & " (CASE WHEN acci_fecha > getDate() And (not ACCI_HORAINICIO IS NULL And LTRIM(RTRIM(ACCI_HORAINICIO)) <> '') THEN 6 ELSE"
sql=sql & " (CASE WHEN acci_fecha > getDate() And (ACCI_HORAINICIO IS NULL Or LTRIM(RTRIM(ACCI_HORAINICIO)) = '') THEN 7 ELSE 0 END) END) END) END) END) END) END) "
sql=sql & " ASC,acci_fecha ASC, ACCI_HORAINICIO ASC "



listaCodigosVisualizados=""
lineaEscrita=false
esPrimeraFila=true
        Set rstAcciones= connCRM.AbrirRecordset(sql,0,1)
    	contador=0
        while not rstAcciones.eof
		
			if trim(listaCodigosVisualizados)<>"" then 
				listaCodigosVisualizados=listaCodigosVisualizados & ","
			end if
			listaCodigosVisualizados=listaCodigosVisualizados & rstAcciones("acci_codigo")		
		
            cont=cont+1%>
            
<%'''''''''''''''''''''''''''''''''''''''''%>    
            <% if isnumeric(rstAcciones("ordenFecha")) then
				if (rstAcciones("ordenFecha")>1 and not lineaEscrita) then
				lineaEscrita=true
				  %><tr  class="filaVaciaTabla" <%if cstr(contador)="0" then%> style="display:none;" <%end if%> >
		                <td>1</td> 
        		        <td  colspan="11"></td>
		                <td style="display:none;"></td>
        		        <td style="display:none;"></td>
		                <td style="display:none;"></td>
        		        <td style="display:none;"></td>
                        <td style="display:none;"></td>
		                <td style="display:none;"></td>
        		        <td style="display:none;"></td>
		                <td style="display:none;"></td>
        		        <td style="display:none;"></td>
		               <td style="display:none;"></td>
        		    </tr>
	            <%end if
				end if%>
<%'''''''''''''''''''''''''''''''''''''''''%>                        
            
            
            <tr class="celdaBlanca">
                <td><%=rstAcciones("ordenFecha")%></td>            
                <td>
				<%	fechaAccion=rstAcciones("acci_fecha")
					if instr(rstAcciones("acci_horainicio"),"01/01/1900")>0 then
						fechaAccion=fechaAccion & " " & replace(rstAcciones("acci_horainicio"),"01/01/1900","")
					else
						fechaAccion=fechaAccion & " " & rstAcciones("acci_horainicio")
					end if
					response.Write(fechaAccion)%>
                </td>
                <td align="center"><%'if datediff("d",rstAcciones("acci_fecha"),date())>0 then response.write "<img src=""/dmcrm/app/imagenes/alert.png"" width=""30"">"%>
	                <% if rstAcciones("ordenFecha")="1" or  rstAcciones("ordenFecha")="2" or  rstAcciones("ordenFecha")="3" then%>
						<img src="/dmcrm/app/imagenes/1_Hoy_date.png" width="28" >                  
	                <% elseif rstAcciones("ordenFecha")="4" Or rstAcciones("ordenFecha")="5" then%>
						<img src="/dmcrm/app/imagenes/2_Alert.png" width="25" >                  
	                <% elseif rstAcciones("ordenFecha")="6" Or rstAcciones("ordenFecha")="7" then%>
						<img src="/dmcrm/app/imagenes/3_trans.png" width="25" >                                                              
	                <% end if%>                    
                </td>
                <td align="center">
                    <%Select case rstAcciones("clie_calidadregistro") 
                        case 1
                            response.write "<img title=""" & objIdioma.getIdioma("ResumenPendiente_Texto14") & """ src=""/dmcrm/app/imagenes/green.png"" width=""18"">"
                        case 2
                            response.write "<img title=""" & objIdioma.getIdioma("ResumenPendiente_Texto15") & """ src=""/dmcrm/app/imagenes/orange.png"" width=""18"">"
                        case 3
                            response.write "<img title=""" & objIdioma.getIdioma("ResumenPendiente_Texto16") & """ src=""/dmcrm/app/imagenes/red.png"" width=""18"">"
                    end select%>
                </td>
                <td align="center">
                	<%if trim(rstAcciones("ACCI_SUCLIENTE"))="" Or isnull(rstAcciones("ACCI_SUCLIENTE")) then%>
	                   <img src="/dmcrm/APP/imagenes/imgPersona.gif" title="<%=objIdiomaGeneral.getIdioma("AccionA_Texto1")%>" />                    
                    <%else%>
	                   <img src="/dmcrm/APP/imagenes/imgEmpresa.gif" title="<%=objIdiomaGeneral.getIdioma("AccionA_Texto2")%>" />
                   <%end if%>    
                </td>                
                <td><%=rstAcciones("TIAC_NOMBRE_DES")%></td>
                <td><%=rstAcciones("clie_nombre")%></td>
                <td><%=rstAcciones("camp_nombre")%></td>
                <td><%=rstAcciones("acci_proyecto")%></td>
                <%'Presentamos el contacto asociado o el contacto r�pido
                if isnull(rstAcciones("acci_sucontacto")) then%>
                    <td><%=rstAcciones("acci_contacto")%></td>
                <%else%>
                    <td><%=rstAcciones("con_nombre") & " " & rstAcciones("con_apellido1")%></td>
                <%end if%>
                <td><%=left(rstAcciones("acci_comentario"),250)%></td>
                <%'posCodigoReal=contador+1%>
                <td><a class="cEmergente" href="/dmcrm/APP/Comercial/Empresas/clientes_Acciones.asp?codigo=<%=rstAcciones("ACCI_CODIGO")%>&posCodigo=<%=contador%>&modo=modificacion&tabla=ACCIONES_CLIENTE"><img src="/dmcrm/APP/Comun2/imagenes/c0.gif" title="<%=objIdioma.getIdioma("ResumenPendiente_Texto10")%>" /></a>	 </td>
                
            </tr>
            
            
            <%contador=contador+1
			esPrimeraFila=false
			rstAcciones.movenext()%>
        
        <%wend%>
        </tbody>
    </TABLE>
</div>
<%if cont=500 then %>
    <br /><br /><br /><%=objIdioma.getIdioma("ResumenPendiente_Texto13")%>
<%end if %>
<%
rstAcciones.cerrar

	session(denominacion & "acciones_pendientes" & "_listaCodigosVisualizados")=listaCodigosVisualizados

%>

<script language="Javascript">
	function recargarAlCerrar()
	{
		//window.location.reload();
		if (document.forms[0]!=null)
		{document.forms[0].submit();}
		else {window.location.reload();}
	}
</script>    

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->

