<%Server.ScriptTimeout=60%>
<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<h1>Indicadores generales</h1>



<script type="text/javascript">
	$(document).ready(function() {
	
	
				$("#cResumenOfertas").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: 550,
						'frameHeight'			: 600,
						'titleShow'		        : false
					}).trigger("click"); 
					return false; 	

				});	
	
				$('.linkResumen').click(function () { 	
					if (parseInt($(this).html())>0) {
						document.getElementById("cResumenOfertas").click();					
					} else {
						alert("No hay ofertas vivas bajo los parαmetros marcados.");					
					}

				});	
	
			$('#lnkVerProvincias').click(function () { 
				    $("#filtroCapaDatos").val("");
					$('#capaDatos').toggle('slow');
					$('#cerrarCapaDatos').show('fast');										
					$('#lnkVerProvincias').hide('fast');
					$('#capaVerProvincias').hide('fast');
					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/Comercial/ReporteComercialProvincias.asp",
						data: "idsSel=" + document.getElementById("hidTerritorios").value,
						success: function(datos) {
									 document.getElementById("listaDatos").innerHTML=datos;
								}
						});
			});

			
			$('#lnkVerProvincias').mouseover(function (event) { 
				mostrarProvinciasSeleccionadas();
				$('#capaVerProvincias').show('fast');																							   
			});				
			
			$('#lnkVerProvincias').mouseout(function (event) { 
				$('#capaVerProvincias').hide('fast');
			});				
			
			
			$('#filtroCapaDatos').keypress(function (event) { 
					var letra=String.fromCharCode(event.charCode);

					var texto=$("#filtroCapaDatos").val();
					
					if (event.charCode==0)
					{
						texto=texto.substr(0,texto.length-1);
					}
					
					if (letra!="")
					{ texto = texto + letra; }

					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/Comercial/ReporteComercialProvincias.asp",
						data: "idsSel=" + document.getElementById("hidTerritorios").value + "&filtroCapaDatos=" + texto,
						success: function(datos) {
									 document.getElementById("listaDatos").innerHTML=datos;
								}
						});
			});
			
			$('#cerrarCapaDatos').click(function () { 
					$('#capaDatos').hide('slow');
					$('#cerrarCapaDatos').hide('fast');					
					$('#lnkVerProvincias').show('fast');
					 document.getElementById("listaDatos").innerHTML="";
			});
			
			
			$('#btnCapaDatos').click(function () { 
			 /*  var idProvincias=document.getElementById("listaDatos").getElementsByTagName("li");
			   var i 
				var idSeleccionados="";

				for (i=0;i<idProvincias.length;i++) { 
					if (idProvincias.item(i).className=="elemListaDatosSeleccion")
					{
						if (idSeleccionados!="")
						{ idSeleccionados= idSeleccionados + ","; }
						idSeleccionados=idSeleccionados + idProvincias.item(i).id;
					}
					
				}
				document.getElementById("hidTerritorios").value=idSeleccionados;*/
				$('#capaDatos').hide('slow');
				$('#lnkVerProvincias').show('fast');
				 document.getElementById("listaDatos").innerHTML="";
			});	
			

			
			

	});
	
	
	
	function mostrarCarga()
	{
		if (document.getElementById("divCargando")!=null)
		{document.getElementById("divCargando").style.display="";}
	}
	
	function actDescListaProv(elemento)
	{
		if (elemento.className=="elemListaDatosSeleccion")
		{elemento.className="elemListaDatos";}
		else
		{elemento.className="elemListaDatosSeleccion";}
		
		 var idProvincias=document.getElementById("listaDatos").getElementsByTagName("li");
			   var i 
				var idSeleccionados="";

				for (i=0;i<idProvincias.length;i++) { 
					if (idProvincias.item(i).className=="elemListaDatosSeleccion")
					{
						if (idSeleccionados!="")
						{ idSeleccionados= idSeleccionados + ","; }
						idSeleccionados=idSeleccionados + idProvincias.item(i).id;
					}
					
				}
				document.getElementById("hidTerritorios").value=idSeleccionados;
		
	}
	
	function mostrarProvinciasSeleccionadas()
	{
//		var contenidoProvincias="";
		document.getElementById("capaVerProvincias").innerHTML="";
		var idsSeleccionados=document.getElementById("hidTerritorios").value;
		if (idsSeleccionados=="")
		{idsSeleccionados="<%=request("idTerritorios")%>";}

/*		if (idsSeleccionados!="")
		{*/
			$.ajax({
				type: "POST",
				url: "/dmcrm/APP/Comercial/ReporteComercialProvincias.asp",
				data: "idsSel=" + idsSeleccionados + "&soloSeleccionados=1",
				success: function(datos) {
											document.getElementById("capaVerProvincias").innerHTML=datos;											
										}
			});
//		}

		
	}
</script>

<style type="text/css">
	#divCargando {position:absolute; width:50%; height:50%; padding:0px; margin:0px; background:#F5F5F5; text-align:center; vertical-align:middle; }
	
	.linkResumen { cursor:pointer; }
	.linkResumen:hover { background:#CDCDCD; }	
</style> 

<!--<div id="divCargando" >
W
aaa
</div>-->
<%'Aqui iria el contenido de la pantalla IndicadoresGeneralesCopiaConsulta.asp

comercial=request.Form("cboComercial")
campana=request.Form("cboCampanna")
fechaDesde=request.Form("txtFDesde")
fechaHasta=request.Form("txtFHasta")
territorios=request.Form("hidTerritorios")
conAreas=false
if lcase(trim(request.Form("chkConAreas")))="on" then
	conAreas=true
end if



if trim(request("buscar"))<>"1" then
	comercial=""
	if trim(request("conComercial"))="1" then
		comercial=session("codigoUsuario")
	end if
	campana="0"
	fechaDesde=dateadd("d",(-day(date()) + 1),date())
	fechaHasta=""
	territorios=request("idTerritorios")'"5,7"
	conAreas=false
	if trim(request("mostrarAreas"))="1" then
		conAreas=true
	end if
end if

filtros=""
filtrosGeneral=""

filtroComercial=""
filtroComercialGeneral=""

filtroCampana=""
filtroCampanaGeneral=""

fitroFechas=""
fitroFechasGeneral=""

filtroAnnoDesde=""
filtroAnnoDesdeGeneral=""

if trim(comercial)<>"" And trim(comercial)<>"0" then
	filtroComercial=filtroComercial & " And ( ACCI_SUCOMERCIAL=" & trim(comercial) & " Or ACCI_SUCOMERCIAL2=" & trim(comercial)  & " Or ACCI_SUCOMERCIAL3=" & trim(comercial)  & " Or ACCI_SUCOMERCIAL4=" & trim(comercial) & " ) "
	filtroComercialGeneral=filtroComercialGeneral & " And ( A.ACCI_SUCOMERCIAL=" & trim(comercial) & " Or A.ACCI_SUCOMERCIAL2=" & trim(comercial)  & " Or A.ACCI_SUCOMERCIAL3=" & trim(comercial)  & " Or A.ACCI_SUCOMERCIAL4=" & trim(comercial) & " ) "	
end if

if trim(campana)<>"" And trim(campana)<>"0" then
	filtroCampana=filtroCampana & " And ACCI_SUCAMPANA=" & trim(campana) & "  "
	filtroCampanaGeneral=filtroCampanaGeneral & " And A.ACCI_SUCAMPANA=" & trim(campana) & "  "	
end if
if trim(fechaDesde)<>"" then
'	fitroFechas=fitroFechas & " And Format(ACCI_FECHA,'ddddd')>=Format('" & trim(fechaDesde) & "','ddddd')  "
	fitroFechas=fitroFechas & " And CAST(ACCI_FECHA as datetime)>=CAST('" & trim(fechaDesde) & "'as datetime)  "
	fitroFechasGeneral=fitroFechasGeneral & " And CAST(A.ACCI_FECHA as datetime)>=CAST('" & trim(fechaDesde) & "' as datetime)  "

	filtroAnnoDesde=" And year(ACCI_FECHA)=" & year(fechaDesde) & " "
	filtroAnnoDesdeGeneral=" And year(A.ACCI_FECHA)=" & year(fechaDesde) & " "	
end if
if trim(fechaHasta)<>"" then
'	fitroFechas=fitroFechas & " And Format(ACCI_FECHA,'ddddd')<=Format('" & trim(fechaHasta) & "','ddddd')  "
	fitroFechas=fitroFechas & " And CAST(ACCI_FECHA as datetime)<=CAST('" & trim(fechaHasta) & "' as datetime)  "
	fitroFechasGeneral=fitroFechasGeneral & " And CAST(A.ACCI_FECHA as datetime)<=CAST('" & trim(fechaHasta) & "' as datetime)  "	
end if

filtros=filtroComercial & filtroCampana & fitroFechas
filtrosGeneral=filtroComercialGeneral & filtroCampanaGeneral & fitroFechasGeneral

filtrosSinFechas=filtroComercial & filtroCampana 
filtrosSinFechasGeneral=filtroComercialGeneral & filtroCampanaGeneral

numAreas=0
if conAreas then
	''Guardamos en un recorset los areas de negocio, pero no lo cerramos, porque tenemos que utilizar varias veces.
	sql = " Select TAN_CODIGO,TAN_DESCRIPCION From TIPOS_AREA_NEGOCIO Order by TAN_DESCRIPCION ASC "
	Set rsAreaNegocio = connCRM.AbrirRecordset(sql,3,1)
	numAreas=rsAreaNegocio.recordcount
end if	

numTerritorios=0
if trim(territorios)<>"" then
	sql=" Select PROV_CODIGO,PROV_NOMBRE FROM PROVINCIAS Where PROV_CODIGO in (" & territorios & ") Order by PROV_NOMBRE ASC "
	Set rsTerritorios = connCRM.AbrirRecordset(sql,3,1)
	numTerritorios=rsTerritorios.recordcount
	
else
	sql=" Select PROV_CODIGO,PROV_NOMBRE FROM PROVINCIAS Where PROV_CODIGO=-1 Order by PROV_NOMBRE ASC "
	Set rsTerritorios = connCRM.AbrirRecordset(sql,3,1)
	numTerritorios=rsTerritorios.recordcount		
end if


''''Inicio Bloque datos LLAMADAS
LlamadasTotales=0
LlamadasTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as LlamadasTotales,count(A2.ACCI_CODIGO) as LlamadasTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "      left  join ( Select ACCI_CODIGO From ACCIONES_CLIENTE WHERE ACCI_SUTIPO=1 And ACCI_TERMINADA=1 " & filtros & " ) as A2 on ( A.ACCI_CODIGO=A2.ACCI_CODIGO ) ) "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	LlamadasTotales=rsDatos("LlamadasTotales")
	LlamadasTerminadas=rsDatos("LlamadasTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenLlamadasTerminadas=0
if LlamadasTotales>0 then
	PorcenLlamadasTerminadas=Round( (LlamadasTerminadas*100) / (LlamadasTotales),2) 
end if

LlamadasNuevoCliTotales=0
LlamadasNuevoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as LlamadasNuevoCliTotales,count(A2.ACCI_CODIGO) as LlamadasNuevoCliTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "       left  join ( Select ACCI_CODIGO From ACCIONES_CLIENTE inner join CLIENTES on ( ACCIONES_CLIENTE.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) WHERE ACCI_SUTIPO=1 And ACCI_TERMINADA=1 And CLIE_CLIENTEACTUAL=1 " & filtros & " ) as A2 on ( A.ACCI_CODIGO=A2.ACCI_CODIGO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=1 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	LlamadasNuevoCliTotales=rsDatos("LlamadasNuevoCliTotales")
	LlamadasNuevoCliTerminadas=rsDatos("LlamadasNuevoCliTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenLlamadasNuevoCliTerminadas=0
if LlamadasNuevoCliTotales>0 then
	PorcenLlamadasNuevoCliTerminadas=Round( (LlamadasNuevoCliTerminadas*100) / (LlamadasNuevoCliTotales),2) 
end if

LlamadasViejoCliTotales=0
LlamadasViejoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as LlamadasViejoCliTotales,count(A2.ACCI_CODIGO) as LlamadasViejoCliTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "       left  join ( Select ACCI_CODIGO From ACCIONES_CLIENTE inner join CLIENTES on ( ACCIONES_CLIENTE.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) WHERE ACCI_SUTIPO=1 And ACCI_TERMINADA=1 And CLIE_CLIENTEACTUAL=0 " & filtros & " ) as A2 on ( A.ACCI_CODIGO=A2.ACCI_CODIGO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	LlamadasViejoCliTotales=rsDatos("LlamadasViejoCliTotales")
	LlamadasViejoCliTerminadas=rsDatos("LlamadasViejoCliTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenLlamadasViejoCliTerminadas=0
if LlamadasViejoCliTotales>0 then
	PorcenLlamadasViejoCliTerminadas=Round( (LlamadasViejoCliTerminadas*100) / (LlamadasViejoCliTotales),2) 
end if

if numTerritorios>0 then
	rsTerritorios.movefirst
end if

contBucle=0

while not rsTerritorios.eof 
	
		AuxLlamadasTerminadasProv=0
		AuxPorcProv=0
		AuxLlamadasTotalesProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxLlamadasTerminadasProv,count(A2.ACCI_CODIGO) as AuxLlamadasTotalesProv "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "       left  join ( Select ACCI_CODIGO From ACCIONES_CLIENTE inner join CLIENTES on ( ACCIONES_CLIENTE.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) WHERE ACCI_SUTIPO=1 And ACCI_TERMINADA=1 And CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO") & " " & filtros & " ) as A2 on ( A.ACCI_CODIGO=A2.ACCI_CODIGO ) ) "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=1 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxLlamadasTotalesProv=rsDatos("AuxLlamadasTerminadasProv")
			AuxLlamadasTerminadasProv=rsDatos("AuxLlamadasTotalesProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		AuxPorcProv=0
		if AuxLlamadasTotalesProv>0 then
			AuxPorcProv=Round( (AuxLlamadasTerminadasProv*100) / (AuxLlamadasTotalesProv),2) 
		end if	
		
		Redim Preserve arrProvLlamadasTerminadas(contBucle)		
		arrProvLlamadasTerminadas(contBucle)=AuxLlamadasTerminadasProv
		
		Redim Preserve arrProvPorcLlamadasTerminadas(contBucle)		
		arrProvPorcLlamadasTerminadas(contBucle)=AuxPorcProv
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	

	if trim(territorios)<>"" then
		AuxLlamadasTerminadasProv=0
		AuxPorcProv=0
		AuxLlamadasTotalesProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxLlamadasTerminadasProv,count(A2.ACCI_CODIGO) as AuxLlamadasTotalesProv "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "       left  join ( Select ACCI_CODIGO From ACCIONES_CLIENTE inner join CLIENTES on ( ACCIONES_CLIENTE.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) WHERE ACCI_SUTIPO=1 And ACCI_TERMINADA=1 And CLIE_PROVINCIA not in (" & territorios & ") " & filtros & " ) as A2 on ( A.ACCI_CODIGO=A2.ACCI_CODIGO ) ) "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=1 And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxLlamadasTotalesProv=rsDatos("AuxLlamadasTerminadasProv")
			AuxLlamadasTerminadasProv=rsDatos("AuxLlamadasTotalesProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		AuxPorcProv=0
		if AuxLlamadasTotalesProv>0 then
			AuxPorcProv=Round( (AuxLlamadasTerminadasProv*100) / (AuxLlamadasTotalesProv),2) 
		end if	

		Redim Preserve arrProvLlamadasTerminadas(contBucle)		
		arrProvLlamadasTerminadas(contBucle)=AuxLlamadasTerminadasProv
		
		Redim Preserve arrProvPorcLlamadasTerminadas(contBucle)		
		arrProvPorcLlamadasTerminadas(contBucle)=AuxPorcProv
	end if 'end de if trim(territorios)<>"" then	

''''Fin Bloque datos LLAMADAS


''''Inicio Bloque datos LLAMADAS terminadas por aρo
LlamadasPorAnno=0

sql=" Select count(A.ACCI_CODIGO) as LlamadasPorAnno "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=1  "
sql = sql & filtrosSinFechasGeneral
sql = sql & filtroAnnoDesdeGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	LlamadasPorAnno=rsDatos("LlamadasPorAnno")
end if
rsDatos.cerrar()
Set rsDatos = nothing

LlamadasNuevoCliPorAnno=0

sql=" Select count(A.ACCI_CODIGO) as LlamadasNuevoCliPorAnno "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=1  And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & filtrosSinFechasGeneral
sql = sql & filtroAnnoDesdeGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	LlamadasNuevoCliPorAnno=rsDatos("LlamadasNuevoCliPorAnno")
end if
rsDatos.cerrar()
Set rsDatos = nothing

LlamadasViejoCliPorAnno=0

sql=" Select count(A.ACCI_CODIGO) as LlamadasViejoCliPorAnno "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & filtrosSinFechasGeneral
sql = sql & filtroAnnoDesdeGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	LlamadasViejoCliPorAnno=rsDatos("LlamadasViejoCliPorAnno")
end if
rsDatos.cerrar()
Set rsDatos = nothing



if numTerritorios>0 then
	rsTerritorios.movefirst
end if

contBucle=0

while not rsTerritorios.eof 
	
		AuxLlamadasProvPorAnno=0

		sql=" Select count(A.ACCI_CODIGO) as AuxLlamadasProvPorAnno "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=1  And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & filtrosSinFechasGeneral
		sql = sql & filtroAnnoDesdeGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxLlamadasProvPorAnno=rsDatos("AuxLlamadasProvPorAnno")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		
		Redim Preserve arrProvLlamadasPorAnno(contBucle)		
		arrProvLlamadasPorAnno(contBucle)=AuxLlamadasProvPorAnno
		
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	
	if trim(territorios)<>"" then
		AuxLlamadasProvPorAnno=0

		sql=" Select count(A.ACCI_CODIGO) as AuxLlamadasProvPorAnno "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=1 And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & filtrosSinFechasGeneral
		sql = sql & filtroAnnoDesdeGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxLlamadasProvPorAnno=rsDatos("AuxLlamadasProvPorAnno")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing


		Redim Preserve arrProvLlamadasPorAnno(contBucle)		
		arrProvLlamadasPorAnno(contBucle)=AuxLlamadasProvPorAnno
		
	end if 'end de if trim(territorios)<>"" then	


''''Fin Bloque datos LLAMADAS terminadas por aρo

''''Inicio Bloque datos VISITAS


VisitasTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as VisitasTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=4 And A.ACCI_TERMINADA=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	VisitasTerminadas=rsDatos("VisitasTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

VisitasNuevoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as VisitasNuevoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=4 And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	VisitasNuevoCliTerminadas=rsDatos("VisitasNuevoCliTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

VisitasViejoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as VisitasViejoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=4  And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	VisitasViejoCliTerminadas=rsDatos("VisitasViejoCliTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing


if numTerritorios>0 then
	rsTerritorios.movefirst
end if
contBucle=0
while not rsTerritorios.eof 
	
		AuxVisitasTerminadasProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxVisitasTerminadasProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=4 And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxVisitasTerminadasProv=rsDatos("AuxVisitasTerminadasProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		
		Redim Preserve arrProvVisitasTerminadas(contBucle)		
		arrProvVisitasTerminadas(contBucle)=AuxVisitasTerminadasProv
		
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	
	
	if trim(territorios)<>"" then
		AuxVisitasTerminadasProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxVisitasTerminadasProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=4  And A.ACCI_TERMINADA=1  And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxVisitasTerminadasProv=rsDatos("AuxVisitasTerminadasProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		Redim Preserve arrProvVisitasTerminadas(contBucle)		
		arrProvVisitasTerminadas(contBucle)=AuxVisitasTerminadasProv
		
	end if 'end de if trim(territorios)<>"" then

	'''Inicio Numero de visitas con llamada

VisitasTerminadasConLlamadas=0

sql=" Select count(A.ACCI_CODIGO) as VisitasTerminadasConLlamadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=4 And A.ACCI_TERMINADA=1 "
sql = sql & " And A2.ACCI_SUTIPO=1 And A2.ACCI_TERMINADA=1 "
sql = sql & filtrosSinFechasGeneral
sql = sql & filtroAnnoDesdeGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	VisitasTerminadasConLlamadas=rsDatos("VisitasTerminadasConLlamadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenVisitasTerminadasConLlamadas=0
if LlamadasPorAnno>0 then
	PorcenVisitasTerminadasConLlamadas=Round( (VisitasTerminadasConLlamadas*100) / (LlamadasPorAnno),2) 
end if


VisitasNuevoCliTerminadasConLlamadas=0

sql=" Select count(A.ACCI_CODIGO) as VisitasNuevoCliTerminadasConLlamadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=4 And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & " And A2.ACCI_SUTIPO=1 And A2.ACCI_TERMINADA=1 "
sql = sql & filtrosSinFechasGeneral
sql = sql & filtroAnnoDesdeGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	VisitasNuevoCliTerminadasConLlamadas=rsDatos("VisitasNuevoCliTerminadasConLlamadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenVisitasNuevoCliTerminadasConLlamadas=0
if LlamadasNuevoCliPorAnno>0 then
	PorcenVisitasNuevoCliTerminadasConLlamadas=Round( (VisitasNuevoCliTerminadasConLlamadas*100) / (LlamadasNuevoCliPorAnno),2) 
end if


VisitasViejoCliTerminadasConLlamadas=0

sql=" Select count(A.ACCI_CODIGO) as VisitasViejoCliTerminadasConLlamadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=4  And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & " And A2.ACCI_SUTIPO=1 And A2.ACCI_TERMINADA=1 "
sql = sql & filtrosSinFechasGeneral
sql = sql & filtroAnnoDesdeGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	VisitasViejoCliTerminadasConLlamadas=rsDatos("VisitasViejoCliTerminadasConLlamadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenVisitasViejoCliTerminadasConLlamadas=0
if LlamadasViejoCliPorAnno>0 then
	PorcenVisitasViejoCliTerminadasConLlamadas=Round( (VisitasViejoCliTerminadasConLlamadas*100) / (LlamadasViejoCliPorAnno),2) 
end if



if numTerritorios>0 then
	rsTerritorios.movefirst
end if
contBucle=0
while not rsTerritorios.eof 
	
		AuxVisitasTerminadasProvConLlamadas=0

		sql=" Select count(A.ACCI_CODIGO) as AuxVisitasTerminadasProvConLlamadas "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=4 And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & " And A2.ACCI_SUTIPO=1 And A2.ACCI_TERMINADA=1 "
		sql = sql & filtrosSinFechasGeneral
		sql = sql & filtroAnnoDesdeGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxVisitasTerminadasProvConLlamadas=rsDatos("AuxVisitasTerminadasProvConLlamadas")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		AuxPorcProv=0
		if arrProvLlamadasPorAnno(contBucle)>0 then
			AuxPorcProv=Round( (AuxVisitasTerminadasProvConLlamadas*100) / (arrProvLlamadasPorAnno(contBucle)),2) 
		end if	
		
		Redim Preserve arrProvPorcVisitasTerminadasConLlamadas(contBucle)		
		arrProvPorcVisitasTerminadasConLlamadas(contBucle)=AuxPorcProv
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	
	
	if trim(territorios)<>"" then
		AuxVisitasTerminadasProv=0


		sql=" Select count(A.ACCI_CODIGO) as AuxVisitasTerminadasProvConLlamadas "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=4  And A.ACCI_TERMINADA=1  And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & " And A2.ACCI_SUTIPO=1 And A2.ACCI_TERMINADA=1 "
		sql = sql & filtrosSinFechasGeneral
		sql = sql & filtroAnnoDesdeGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxVisitasTerminadasProvConLlamadas=rsDatos("AuxVisitasTerminadasProvConLlamadas")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		AuxPorcProv=0
		if arrProvLlamadasPorAnno(contBucle)>0 then
			AuxPorcProv=Round( (AuxVisitasTerminadasProvConLlamadas*100) / (arrProvLlamadasPorAnno(contBucle)),2) 
		end if	

		Redim Preserve arrProvPorcVisitasTerminadasConLlamadas(contBucle)		
		arrProvPorcVisitasTerminadasConLlamadas(contBucle)=AuxPorcProv
	end if		
	
	'''Fin  Numero de visitas con llamada
'response.End()

''''Fin Bloque datos VISITAS


''''Inicio Bloque datos Ofertas presentadas

	'''Inicio Numero de ofertas presentadas con visitas
OPresentadasTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadasTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 "
sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasTerminadas=rsDatos("OPresentadasTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenOPresentadasTerminadas=0
if VisitasTerminadas>0 then
	PorcenOPresentadasTerminadas=Round( (OPresentadasTerminadas*100) / (VisitasTerminadas),2) 
end if


OPresentadasNuevoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadasNuevoCliTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasNuevoCliTerminadas=rsDatos("OPresentadasNuevoCliTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenOPresentadasNuevoCliTerminadas=0
if VisitasNuevoCliTerminadas>0 then
	PorcenOPresentadasNuevoCliTerminadas=Round( (OPresentadasNuevoCliTerminadas*100) / (VisitasNuevoCliTerminadas),2) 
end if


OPresentadasViejoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadasViejoCliTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6  And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasViejoCliTerminadas=rsDatos("OPresentadasViejoCliTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenOPresentadasViejoCliTerminadas=0
if VisitasViejoCliTerminadas>0 then
	PorcenOPresentadasViejoCliTerminadas=Round( (OPresentadasViejoCliTerminadas*100) / (VisitasViejoCliTerminadas),2) 
end if



OPresentadasAltaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaAltaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6ESTADOCLIENTE=1  "
sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1  " 'Duda: And A2.ACCI_TIPO6ESTADOCLIENTE=1  
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasAltaTerminadas=rsDatos("OPresentadaAltaTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

'Duda: no se puede calcular el numero de visitas con estado cliente ni area de negocio
PorcenOPresentadasAltaTerminadas=0
if VisitasTerminadas>0 then
	PorcenOPresentadasAltaTerminadas=Round( (OPresentadasAltaTerminadas*100) / (VisitasTerminadas),2) 
end if


OPresentadasMediaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaMediaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6ESTADOCLIENTE=2 "
sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1  " ' Duda: And A2.ACCI_TIPO6ESTADOCLIENTE=2 
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasMediaTerminadas=rsDatos("OPresentadaMediaTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing


PorcenOPresentadasMediaTerminadas=0
if VisitasTerminadas>0 then
	PorcenOPresentadasMediaTerminadas=Round( (OPresentadasMediaTerminadas*100) / (VisitasTerminadas),2) 
end if



OPresentadasBajaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaBajaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6ESTADOCLIENTE=3 "
sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1  " 'Duda: And A2.ACCI_TIPO6ESTADOCLIENTE=3 
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasBajaTerminadas=rsDatos("OPresentadaBajaTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

PorcenOPresentadasBajaTerminadas=0
if VisitasTerminadas>0 then
	PorcenOPresentadasBajaTerminadas=Round( (OPresentadasBajaTerminadas*100) / (VisitasTerminadas),2) 
end if


if numTerritorios>0 then
	rsTerritorios.movefirst
end if
contBucle=0
while not rsTerritorios.eof 
	
		AuxOPresentadasTerminadasProv=0
		AuxPorcProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPresentadasTerminadasProv "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1 " 
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPresentadasTerminadasProv=rsDatos("AuxOPresentadasTerminadasProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing


		AuxPorcProv=0
		if arrProvVisitasTerminadas(contBucle)>0 then
			AuxPorcProv=Round( (AuxOPresentadasTerminadasProv*100) / (arrProvVisitasTerminadas(contBucle)),2) 
		end if	
		
		
'		Redim Preserve arrProvOPresentadasTerminadas(contBucle)		
'		arrProvOPresentadasTerminadas(contBucle)=AuxOPresentadasTerminadasProv
		
		Redim Preserve arrProvPorcOPresentadasTerminadas(contBucle)		
		arrProvPorcOPresentadasTerminadas(contBucle)=AuxPorcProv
		
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	

	if trim(territorios)<>"" then
		AuxOPresentadasTerminadasProv=0
		AuxPorcProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPresentadasTerminadasProv "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1 "		
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPresentadasTerminadasProv=rsDatos("AuxOPresentadasTerminadasProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing
		
		AuxPorcProv=0
		if arrProvVisitasTerminadas(contBucle)>0 then
			AuxPorcProv=Round( (AuxOPresentadasTerminadasProv*100) / (arrProvVisitasTerminadas(contBucle)),2) 
		end if	
		
'		Redim Preserve arrProvOPresentadasTerminadas(contBucle)		
'		arrProvOPresentadasTerminadas(contBucle)=AuxOPresentadasTerminadasProv
		
		Redim Preserve arrProvPorcOPresentadasTerminadas(contBucle)		
		arrProvPorcOPresentadasTerminadas(contBucle)=AuxPorcProv
		
	end if 'end de if trim(territorios)<>"" then

'''''
if numAreas>0 then
	rsAreaNegocio.movefirst
	contBucle=0
	while not rsAreaNegocio.eof
	''''''
		AuxValor=0
		AuxPorc=0		
		AuxCant=0
		AuxMedia=0


		sql=" Select count(A.ACCI_CODIGO) as valorArea "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6SUAREANEGOCIO=" & rsAreaNegocio("TAN_CODIGO")
		sql = sql & " And A2.ACCI_SUTIPO=4 And A2.ACCI_TERMINADA=1 " 'Duda: And A2.ACCI_TIPO6SUAREANEGOCIO=" & rsAreaNegocio("TAN_CODIGO") 		
		sql = sql & filtrosGeneral
		

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxValor=rsDatos("valorArea")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		AuxPorc=0
		if VisitasTerminadas>0 then
			AuxPorc=Round( (AuxValor*100) / (VisitasTerminadas),2) 
		end if

'		Redim Preserve arrValorAreasOPresentadas(contBucle)		
'		arrValorAreasOPresentadas(contBucle)=AuxValor
		
		Redim Preserve arrPorcAreasOPresentadas(contBucle)		
		arrPorcAreasOPresentadas(contBucle)=AuxPorc


		contBucle=contBucle + 1
		rsAreaNegocio.movenext
	wend

end if 'end de if numAreas>0 then	
	'''Fin Numero de ofertas presentadas con visitas


'''''''''''''''''''''''''''''''
OPresentadasTotales=0
CantOPresentadasTotales=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadasTotales,sum(A.ACCI_TIPO6CANTIDAD) as CantOPresentadasTotales "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasTotales=rsDatos("OPresentadasTotales")
	CantOPresentadasTotales=rsDatos("CantOPresentadasTotales")
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOPresentadasTotales) or trim(CantOPresentadasTotales)="" then
	CantOPresentadasTotales=0
end if


MediaCantOPresentadasTotales=0
if OPresentadasTotales>0 then
	MediaCantOPresentadasTotales=CantOPresentadasTotales / OPresentadasTotales 
end if


OPresentadasNuevoCliTotales=0
CantOPresentadasNuevoCliTotales=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadasNuevoCliTotales,sum(A.ACCI_TIPO6CANTIDAD) as CantOPresentadasNuevoCliTotales "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasNuevoCliTotales=rsDatos("OPresentadasNuevoCliTotales")
	CantOPresentadasNuevoCliTotales=rsDatos("CantOPresentadasNuevoCliTotales")	
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOPresentadasNuevoCliTotales) or  trim(CantOPresentadasNuevoCliTotales)="" then
	CantOPresentadasNuevoCliTotales=0
end if

MediaCantOPresentadasNuevoCliTotales=0
if OPresentadasNuevoCliTotales>0 then
	MediaCantOPresentadasNuevoCliTotales=CantOPresentadasNuevoCliTotales / OPresentadasNuevoCliTotales
end if

OPresentadasViejoCliTotales=0
CantOPresentadasViejoCliTotales=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadasViejoCliTotales,sum(A.ACCI_TIPO6CANTIDAD) as CantOPresentadasViejoCliTotales "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6  And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasViejoCliTotales=rsDatos("OPresentadasViejoCliTotales")
	CantOPresentadasViejoCliTotales=rsDatos("CantOPresentadasViejoCliTotales")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPresentadasViejoCliTotales)  or trim(CantOPresentadasViejoCliTotales)="" then
	CantOPresentadasViejoCliTotales=0
end if

MediaCantOPresentadasViejoCliTotales=0
if OPresentadasViejoCliTotales>0 then
	MediaCantOPresentadasViejoCliTotales=CantOPresentadasViejoCliTotales / OPresentadasViejoCliTotales 
end if


OPresentadasAltaTotales=0
CantOPresentadasAltaTotales=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaAltaTotales,sum(A.ACCI_TIPO6CANTIDAD) as CantOPresentadasAltaTotales "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6ESTADOCLIENTE=1  "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasAltaTotales=rsDatos("OPresentadaAltaTotales")
	CantOPresentadasAltaTotales=rsDatos("CantOPresentadasAltaTotales")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPresentadasAltaTotales) or  trim(CantOPresentadasAltaTotales)="" then
	CantOPresentadasAltaTotales=0
end if

MediaCantOPresentadasAltaTotales=0
if OPresentadasAltaTotales>0 then
	MediaCantOPresentadasAltaTotales=CantOPresentadasAltaTotales / OPresentadasAltaTotales 
end if

OPresentadasMediaTotales=0
CantOPresentadasMediaTotales=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaMediaTotales,sum(A.ACCI_TIPO6CANTIDAD) as CantOPresentadasMediaTotales "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6ESTADOCLIENTE=2 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasMediaTotales=rsDatos("OPresentadaMediaTotales")
	CantOPresentadasMediaTotales=rsDatos("CantOPresentadasMediaTotales")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPresentadasMediaTotales) or trim(CantOPresentadasMediaTotales)="" then
	CantOPresentadasMediaTotales=0
end if


MediaCantOPresentadasMediaTotales=0
if OPresentadasMediaTotales>0 then
	MediaCantOPresentadasMediaTotales=CantOPresentadasMediaTotales / OPresentadasMediaTotales 
end if


OPresentadasBajaTotales=0
CantOPresentadasBajaTotales=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaBajaTotales,sum(A.ACCI_TIPO6CANTIDAD) as CantOPresentadasBajaTotales "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6ESTADOCLIENTE=3 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPresentadasBajaTotales=rsDatos("OPresentadaBajaTotales")
	CantOPresentadasBajaTotales=rsDatos("CantOPresentadasBajaTotales")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPresentadasBajaTotales) or trim(CantOPresentadasBajaTotales)="" then
	CantOPresentadasBajaTotales=0
end if


MediaCantOPresentadasBajaTotales=0
if OPresentadasBajaTotales>0 then
	MediaCantOPresentadasBajaTotales=CantOPresentadasBajaTotales / OPresentadasBajaTotales 
end if



'''''

if numTerritorios>0 then
	rsTerritorios.movefirst
end if
contBucle=0
while not rsTerritorios.eof 
	
		AuxOPresentadasTotalesProv=0
		AuxPorcProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPresentadasTotalesProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPresentadasTotalesProv=rsDatos("AuxOPresentadasTotalesProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCantProv) or trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		
		AuxMediaProv=0
		if AuxOPresentadasTotalesProv>0 then
			AuxMediaProv=AuxCantProv / AuxOPresentadasTotalesProv 
		end if
		
		Redim Preserve arrProvOPresentadasTotales(contBucle)		
		arrProvOPresentadasTotales(contBucle)=AuxOPresentadasTotalesProv
		
		Redim Preserve arrProvCantOPresentadasTotales(contBucle)		
		arrProvCantOPresentadasTotales(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOPresentadasTotales(contBucle)		
		arrProvMediaOPresentadasTotales(contBucle)=AuxMediaProv
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	

	if trim(territorios)<>"" then
		AuxOPresentadasTotalesProv=0
		AuxPorcProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPresentadasTotalesProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPresentadasTotalesProv=rsDatos("AuxOPresentadasTotalesProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing
		
		if isnull(AuxCantProv) or  trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		AuxMediaProv=0
		if AuxOPresentadasTotalesProv>0 then
			AuxMediaProv=AuxCantProv / AuxOPresentadasTotalesProv 
		end if
		
		Redim Preserve arrProvOPresentadasTotales(contBucle)		
		arrProvOPresentadasTotales(contBucle)=AuxOPresentadasTotalesProv
		
		Redim Preserve arrProvCantOPresentadasTotales(contBucle)		
		arrProvCantOPresentadasTotales(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOPresentadasTotales(contBucle)		
		arrProvMediaOPresentadasTotales(contBucle)=AuxMediaProv

	end if 'end de if trim(territorios)<>"" then

'''''
if numAreas>0 then
	rsAreaNegocio.movefirst
	contBucle=0
	while not rsAreaNegocio.eof
	''''''
		AuxValor=0
		AuxPorc=0		
		AuxCant=0
		AuxMedia=0


		sql=" Select count(A.ACCI_CODIGO) as valorArea,sum(A.ACCI_TIPO6CANTIDAD) as cantArea "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=6 And A.ACCI_TERMINADA=1 And A.ACCI_TIPO6SUAREANEGOCIO=" & rsAreaNegocio("TAN_CODIGO")
		sql = sql & filtrosGeneral
		

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxValor=rsDatos("valorArea")
			AuxCant=rsDatos("cantArea")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCant) or  trim(AuxCant)="" then
			AuxCant=0
		end if
		
		AuxMedia=0
		if AuxValor>0 then
			AuxMedia=AuxCant / AuxValor 
		end if	



		Redim Preserve arrValorAreasOPresentadasTotales(contBucle)		
		arrValorAreasOPresentadasTotales(contBucle)=AuxValor
		
		Redim Preserve arrCantAreasOPresentadasTotales(contBucle)		
		arrCantAreasOPresentadasTotales(contBucle)=AuxCant

		
		Redim Preserve arrMediaAreasOPresentadasTotales(contBucle)		
		arrMediaAreasOPresentadasTotales(contBucle)=AuxMedia				

		contBucle=contBucle + 1
		rsAreaNegocio.movenext
	wend

end if 'end de if numAreas>0 then


''''Fin Bloque datos Ofertas presentadas




''''Inicio Bloque datos Ofertas aceptadas
OAceptadasTerminadas=0
CantOAceptadasTerminadas=0


sql=" Select count(A.ACCI_CODIGO) as OAceptadasTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOAceptadasTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1 And A.ACCI_TERMINADA=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OAceptadasTerminadas=rsDatos("OAceptadasTerminadas")
	CantOAceptadasTerminadas=rsDatos("CantOAceptadasTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOAceptadasTerminadas) or trim(CantOAceptadasTerminadas)="" then
	CantOAceptadasTerminadas=0
end if


PorcenOAceptadasTerminadas=0
if OPresentadasTotales>0 then
	PorcenOAceptadasTerminadas=Round( (OAceptadasTerminadas*100) / (OPresentadasTotales),2) 
end if

MediaCantOAceptadasTerminadas=0
if OAceptadasTerminadas>0 then
	MediaCantOAceptadasTerminadas=CantOAceptadasTerminadas / OAceptadasTerminadas 
end if


OAceptadasNuevoCliTerminadas=0
CantOAceptadasNuevoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OAceptadasNuevoCliTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOAceptadasNuevoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1  And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OAceptadasNuevoCliTerminadas=rsDatos("OAceptadasNuevoCliTerminadas")
	CantOAceptadasNuevoCliTerminadas=rsDatos("CantOAceptadasNuevoCliTerminadas")	
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOAceptadasNuevoCliTerminadas) or  trim(CantOAceptadasNuevoCliTerminadas)="" then
	CantOAceptadasNuevoCliTerminadas=0
end if

PorcenOAceptadasNuevoCliTerminadas=0
if OPresentadasNuevoCliTotales>0 then
	PorcenOAceptadasNuevoCliTerminadas=Round( (OAceptadasNuevoCliTerminadas*100) / (OPresentadasNuevoCliTotales),2) 
end if

MediaCantOAceptadasNuevoCliTerminadas=0
if OAceptadasNuevoCliTerminadas>0 then
	MediaCantOAceptadasNuevoCliTerminadas=CantOAceptadasNuevoCliTerminadas / OAceptadasNuevoCliTerminadas 
end if

OAceptadasViejoCliTerminadas=0
CantOAceptadasViejoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OAceptadasViejoCliTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOAceptadasViejoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1   And A.ACCI_TERMINADA=1 And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OAceptadasViejoCliTerminadas=rsDatos("OAceptadasViejoCliTerminadas")
	CantOAceptadasViejoCliTerminadas=rsDatos("CantOAceptadasViejoCliTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOAceptadasViejoCliTerminadas)  or trim(CantOAceptadasViejoCliTerminadas)="" then
	CantOAceptadasViejoCliTerminadas=0
end if

PorcenOAceptadasViejoCliTerminadas=0
if OPresentadasViejoCliTotales>0 then
	PorcenOAceptadasViejoCliTerminadas=Round( (OAceptadasViejoCliTerminadas*100) / (OPresentadasViejoCliTotales),2) 
end if

MediaCantOAceptadasViejoCliTerminadas=0
if OAceptadasViejoCliTerminadas>0 then
	MediaCantOAceptadasViejoCliTerminadas=CantOAceptadasViejoCliTerminadas / OAceptadasViejoCliTerminadas 
end if


OAceptadasAltaTerminadas=0
CantOAceptadasAltaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaAltaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOAceptadasAltaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1  And A.ACCI_TERMINADA=1 "
sql = sql & " And A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1 And A2.ACCI_TIPO6ESTADOCLIENTE=1 "
sql = sql & filtrosGeneral


Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OAceptadasAltaTerminadas=rsDatos("OPresentadaAltaTerminadas")
	CantOAceptadasAltaTerminadas=rsDatos("CantOAceptadasAltaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing



if  isnull(CantOAceptadasAltaTerminadas) or  trim(CantOAceptadasAltaTerminadas)="" then
	CantOAceptadasAltaTerminadas=0
end if

PorcenOAceptadasAltaTerminadas=0
if OPresentadasAltaTotales>0 then
	PorcenOAceptadasAltaTerminadas=Round( (OAceptadasAltaTerminadas*100) / (OPresentadasAltaTotales),2) 
end if

MediaCantOAceptadasAltaTerminadas=0
if OAceptadasAltaTerminadas>0 then
	MediaCantOAceptadasAltaTerminadas=CantOAceptadasAltaTerminadas / OAceptadasAltaTerminadas 
end if

OAceptadasMediaTerminadas=0
CantOAceptadasMediaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaMediaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOAceptadasMediaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1  And A.ACCI_TERMINADA=1  "
sql = sql & " And A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1 And A2.ACCI_TIPO6ESTADOCLIENTE=2 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OAceptadasMediaTerminadas=rsDatos("OPresentadaMediaTerminadas")
	CantOAceptadasMediaTerminadas=rsDatos("CantOAceptadasMediaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOAceptadasMediaTerminadas) or trim(CantOAceptadasMediaTerminadas)="" then
	CantOAceptadasMediaTerminadas=0
end if

PorcenOAceptadasMediaTerminadas=0
if OPresentadasMediaTotales>0 then
	PorcenOAceptadasMediaTerminadas=Round( (OAceptadasMediaTerminadas*100) / (OPresentadasMediaTotales),2) 
end if

MediaCantOAceptadasMediaTerminadas=0
if OAceptadasMediaTerminadas>0 then
	MediaCantOAceptadasMediaTerminadas=CantOAceptadasMediaTerminadas / OAceptadasMediaTerminadas 
end if


OAceptadasBajaTerminadas=0
CantOAceptadasBajaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaBajaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOAceptadasBajaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1 And A.ACCI_TERMINADA=1 "
sql = sql & " And A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1 And A2.ACCI_TIPO6ESTADOCLIENTE=3 "
sql = sql & filtrosGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OAceptadasBajaTerminadas=rsDatos("OPresentadaBajaTerminadas")
	CantOAceptadasBajaTerminadas=rsDatos("CantOAceptadasBajaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOAceptadasBajaTerminadas) or trim(CantOAceptadasBajaTerminadas)="" then
	CantOAceptadasBajaTerminadas=0
end if

PorcenOAceptadasBajaTerminadas=0
if OPresentadasBajaTotales>0 then
	PorcenOAceptadasBajaTerminadas=Round( (OAceptadasBajaTerminadas*100) / (OPresentadasBajaTotales),2) 
end if

MediaCantOAceptadasBajaTerminadas=0
if OAceptadasBajaTerminadas>0 then
	MediaCantOAceptadasBajaTerminadas=CantOAceptadasBajaTerminadas / OAceptadasBajaTerminadas 
end if



'''''

if numTerritorios>0 then
	rsTerritorios.movefirst
end if
contBucle=0
while not rsTerritorios.eof 
	
		AuxOAceptadasTerminadasProv=0
		AuxPorcProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOAceptadasTerminadasProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1  And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOAceptadasTerminadasProv=rsDatos("AuxOAceptadasTerminadasProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCantProv) or trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		AuxPorcProv=0
		if arrProvOPresentadasTotales(contBucle)>0 then
			AuxPorcProv=Round( (AuxOAceptadasTerminadasProv*100) / (arrProvOPresentadasTotales(contBucle)),2) 
		end if	
		
		AuxMediaProv=0
		if AuxOAceptadasTerminadasProv>0 then
			AuxMediaProv=AuxCantProv / AuxOAceptadasTerminadasProv 
		end if
		
		Redim Preserve arrProvOAceptadasTerminadas(contBucle)		
		arrProvOAceptadasTerminadas(contBucle)=AuxOAceptadasTerminadasProv
		
		Redim Preserve arrProvPorcOAceptadasTerminadas(contBucle)		
		arrProvPorcOAceptadasTerminadas(contBucle)=AuxPorcProv
		
		Redim Preserve arrProvCantOAceptadasTerminadas(contBucle)		
		arrProvCantOAceptadasTerminadas(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOAceptadasTerminadas(contBucle)		
		arrProvMediaOAceptadasTerminadas(contBucle)=AuxMediaProv
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	

	if trim(territorios)<>"" then
		AuxOAceptadasTerminadasProv=0
		AuxPorcProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOAceptadasTerminadasProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1  And A.ACCI_TERMINADA=1 And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & filtrosGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOAceptadasTerminadasProv=rsDatos("AuxOAceptadasTerminadasProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing
		
		if isnull(AuxCantProv) or  trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		AuxPorcProv=0
		if arrProvOPresentadasTotales(contBucle)>0 then
			AuxPorcProv=Round( (AuxOAceptadasTerminadasProv*100) / (arrProvOPresentadasTotales(contBucle)),2) 
		end if	
		
		AuxMediaProv=0
		if AuxOAceptadasTerminadasProv>0 then
			AuxMediaProv=AuxCantProv / AuxOAceptadasTerminadasProv 
		end if
		
		Redim Preserve arrProvOAceptadasTerminadas(contBucle)		
		arrProvOAceptadasTerminadas(contBucle)=AuxOAceptadasTerminadasProv
		
		Redim Preserve arrProvPorcOAceptadasTerminadas(contBucle)		
		arrProvPorcOAceptadasTerminadas(contBucle)=AuxPorcProv
		
		Redim Preserve arrProvCantOAceptadasTerminadas(contBucle)		
		arrProvCantOAceptadasTerminadas(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOAceptadasTerminadas(contBucle)		
		arrProvMediaOAceptadasTerminadas(contBucle)=AuxMediaProv
	end if 'end if if trim(territorios)<>"" then

'''''
if numAreas>0 then
	rsAreaNegocio.movefirst
	contBucle=0
	while not rsAreaNegocio.eof
	''''''
		AuxValor=0
		AuxPorc=0		
		AuxCant=0
		AuxMedia=0


		sql=" Select count(A.ACCI_CODIGO) as valorArea,sum(A.ACCI_TIPO6CANTIDAD) as cantArea "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "		
		sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TIPO7FINALIZACION=1  And A.ACCI_TERMINADA=1 "
		sql = sql & " And A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1 And A2.ACCI_TIPO6SUAREANEGOCIO=" & rsAreaNegocio("TAN_CODIGO")
		sql = sql & filtrosGeneral
		


		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxValor=rsDatos("valorArea")
			AuxCant=rsDatos("cantArea")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCant) or  trim(AuxCant)="" then
			AuxCant=0
		end if

		AuxPorc=0
		if arrValorAreasOPresentadasTotales(contBucle)>0 then
			AuxPorc=Round( (AuxValor*100) / (arrValorAreasOPresentadasTotales(contBucle)),2) 
		end if

		AuxMedia=0
		if AuxValor>0 then
			AuxMedia=AuxCant / AuxValor 
		end if	

		Redim Preserve arrValorAreasOAceptadas(contBucle)		
		arrValorAreasOAceptadas(contBucle)=AuxValor
		
		Redim Preserve arrPorcAreasOAceptadas(contBucle)		
		arrPorcAreasOAceptadas(contBucle)=AuxPorc

		Redim Preserve arrCantAreasOAceptadas(contBucle)		
		arrCantAreasOAceptadas(contBucle)=AuxCant

		
		Redim Preserve arrMediaAreaOAceptadas(contBucle)		
		arrMediaAreaOAceptadas(contBucle)=AuxMedia				
	''''''

		contBucle=contBucle + 1
		rsAreaNegocio.movenext
	wend

end if 'end de if numAreas>0 then	


''''Fin Bloque datos Ofertas aceptadas



''''Inicio Bloque datos Ofertas pendientes
OPendientesTerminadas=0
CantOPendientesTerminadas=0


sql=" Select count(A.ACCI_CODIGO) as OPendientesTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 "
sql = sql & filtrosSinFechasGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)

if not rsDatos.eof then
	OPendientesTerminadas=rsDatos("OPendientesTerminadas")
	CantOPendientesTerminadas=rsDatos("CantOPendientesTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOPendientesTerminadas) or trim(CantOPendientesTerminadas)="" then
	CantOPendientesTerminadas=0
end if




MediaCantOPendientesTerminadas=0
if OPendientesTerminadas>0 then
	MediaCantOPendientesTerminadas=CantOPendientesTerminadas / OPendientesTerminadas 
end if


OPendientesNuevoCliTerminadas=0
CantOPendientesNuevoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPendientesNuevoCliTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesNuevoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 And C.CLIE_CLIENTEACTUAL=1 "
sql = sql & filtrosSinFechasGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesNuevoCliTerminadas=rsDatos("OPendientesNuevoCliTerminadas")
	CantOPendientesNuevoCliTerminadas=rsDatos("CantOPendientesNuevoCliTerminadas")	
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOPendientesNuevoCliTerminadas) or  trim(CantOPendientesNuevoCliTerminadas)="" then
	CantOPendientesNuevoCliTerminadas=0
end if


MediaCantOPendientesNuevoCliTerminadas=0
if OPendientesNuevoCliTerminadas>0 then
	MediaCantOPendientesNuevoCliTerminadas=CantOPendientesNuevoCliTerminadas / OPendientesNuevoCliTerminadas 
end if

OPendientesViejoCliTerminadas=0
CantOPendientesViejoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPendientesViejoCliTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesViejoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0  And C.CLIE_CLIENTEACTUAL=0 "
sql = sql & filtrosSinFechasGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesViejoCliTerminadas=rsDatos("OPendientesViejoCliTerminadas")
	CantOPendientesViejoCliTerminadas=rsDatos("CantOPendientesViejoCliTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesViejoCliTerminadas)  or trim(CantOPendientesViejoCliTerminadas)="" then
	CantOPendientesViejoCliTerminadas=0
end if


MediaCantOPendientesViejoCliTerminadas=0
if OPendientesViejoCliTerminadas>0 then
	MediaCantOPendientesViejoCliTerminadas=CantOPendientesViejoCliTerminadas / OPendientesViejoCliTerminadas 
end if


OPendientesAltaTerminadas=0
CantOPendientesAltaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaAltaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesAltaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 "
sql = sql & " And  A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1  And A2.ACCI_TIPO6ESTADOCLIENTE=1  "
sql = sql & filtrosSinFechasGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesAltaTerminadas=rsDatos("OPresentadaAltaTerminadas")
	CantOPendientesAltaTerminadas=rsDatos("CantOPendientesAltaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesAltaTerminadas) or  trim(CantOPendientesAltaTerminadas)="" then
	CantOPendientesAltaTerminadas=0
end if


MediaCantOPendientesAltaTerminadas=0
if OPendientesAltaTerminadas>0 then
	MediaCantOPendientesAltaTerminadas=CantOPendientesAltaTerminadas / OPendientesAltaTerminadas 
end if

OPendientesMediaTerminadas=0
CantOPendientesMediaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaMediaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesMediaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 "
sql = sql & " And  A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1  And A2.ACCI_TIPO6ESTADOCLIENTE=2 "
sql = sql & filtrosSinFechasGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesMediaTerminadas=rsDatos("OPresentadaMediaTerminadas")
	CantOPendientesMediaTerminadas=rsDatos("CantOPendientesMediaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesMediaTerminadas) or trim(CantOPendientesMediaTerminadas)="" then
	CantOPendientesMediaTerminadas=0
end if


MediaCantOPendientesMediaTerminadas=0
if OPendientesMediaTerminadas>0 then
	MediaCantOPendientesMediaTerminadas=CantOPendientesMediaTerminadas / OPendientesMediaTerminadas 
end if


OPendientesBajaTerminadas=0
CantOPendientesBajaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaBajaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesBajaTerminadas "
sql = sql & "  From ((  ACCIONES_CLIENTE as A "
sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 "
sql = sql & " And  A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1  And A2.ACCI_TIPO6ESTADOCLIENTE=3  "
sql = sql & filtrosSinFechasGeneral

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesBajaTerminadas=rsDatos("OPresentadaBajaTerminadas")
	CantOPendientesBajaTerminadas=rsDatos("CantOPendientesBajaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesBajaTerminadas) or trim(CantOPendientesBajaTerminadas)="" then
	CantOPendientesBajaTerminadas=0
end if


MediaCantOPendientesBajaTerminadas=0
if OPendientesBajaTerminadas>0 then
	MediaCantOPendientesBajaTerminadas=CantOPendientesBajaTerminadas / OPendientesBajaTerminadas 
end if



'''''

if numTerritorios>0 then
	rsTerritorios.movefirst
end if

contBucle=0
while not rsTerritorios.eof 
	
		AuxOPendientesTerminadasProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPendientesTerminadasProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")		
		sql = sql & filtrosSinFechasGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPendientesTerminadasProv=rsDatos("AuxOPendientesTerminadasProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCantProv) or trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		
		AuxMediaProv=0
		if AuxOPendientesTerminadasProv>0 then
			AuxMediaProv=AuxCantProv / AuxOPendientesTerminadasProv 
		end if
		
		Redim Preserve arrProvOPendientesTerminadas(contBucle)		
		arrProvOPendientesTerminadas(contBucle)=AuxOPendientesTerminadasProv
		
		
		Redim Preserve arrProvCantOPendientesTerminadas(contBucle)		
		arrProvCantOPendientesTerminadas(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOPendientesTerminadas(contBucle)		
		arrProvMediaOPendientesTerminadas(contBucle)=AuxMediaProv
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	

	if trim(territorios)<>"" then
		AuxOPendientesTerminadasProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPendientesTerminadasProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 And (C.CLIE_PROVINCIA not in (" & territorios & ") Or C.CLIE_PROVINCIA is null) "
		sql = sql & filtrosSinFechasGeneral

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPendientesTerminadasProv=rsDatos("AuxOPendientesTerminadasProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing
		
		if isnull(AuxCantProv) or  trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		
		AuxMediaProv=0
		if AuxOPendientesTerminadasProv>0 then
			AuxMediaProv=AuxCantProv / AuxOPendientesTerminadasProv 
		end if
		
		Redim Preserve arrProvOPendientesTerminadas(contBucle)		
		arrProvOPendientesTerminadas(contBucle)=AuxOPendientesTerminadasProv
		
		
		Redim Preserve arrProvCantOPendientesTerminadas(contBucle)		
		arrProvCantOPendientesTerminadas(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOPendientesTerminadas(contBucle)		
		arrProvMediaOPendientesTerminadas(contBucle)=AuxMediaProv
	end if ' end de if trim(territorios)<>"" then		

'''''
if numAreas>0 then
	rsAreaNegocio.movefirst
	contBucle=0
	while not rsAreaNegocio.eof
	''''''
		AuxValor=0
		AuxCant=0
		AuxMedia=0


		sql=" Select count(A.ACCI_CODIGO) as valorArea,sum(A.ACCI_TIPO6CANTIDAD) as cantArea "
		sql = sql & "  From ((  ACCIONES_CLIENTE as A "
		sql = sql & "  		inner join ACCIONES_CLIENTE as A2 on (A.ACCI_SUCLIENTE=A2.ACCI_SUCLIENTE And A.ACCI_SUCAMPANA=A2.ACCI_SUCAMPANA And A.ACCI_PROYECTO=A2.ACCI_PROYECTO ) ) "		
		sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
		sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=0 "		
		sql = sql & " And  A2.ACCI_SUTIPO=6 And A2.ACCI_TERMINADA=1  "		
		sql = sql & " And A2.ACCI_TIPO6SUAREANEGOCIO=" & rsAreaNegocio("TAN_CODIGO")
		sql = sql & filtrosSinFechasGeneral
		

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxValor=rsDatos("valorArea")
			AuxCant=rsDatos("cantArea")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCant) or  trim(AuxCant)="" then
			AuxCant=0
		end if
		

		AuxMedia=0
		if AuxValor>0 then
			AuxMedia=AuxCant / AuxValor 
		end if	

		Redim Preserve arrValorAreasOPendientes(contBucle)		
		arrValorAreasOPendientes(contBucle)=AuxValor
		

		Redim Preserve arrCantAreasOPendientes(contBucle)		
		arrCantAreasOPendientes(contBucle)=AuxCant

		
		Redim Preserve arrMediaAreasOPendientes(contBucle)		
		arrMediaAreasOPendientes(contBucle)=AuxMedia				
	''''''

		contBucle=contBucle + 1
		rsAreaNegocio.movenext
	wend
	
end if 'end de if numAreas>0 then	

''''Fin Bloque datos Ofertas Pendientes

%>




<br>
<div id="destino"></div>
<!--<div id="divCargandoIndicadores" style=" background-color:gray; border:none; margin:0px; padding:0px; position:absolute; top:0px; left:0px; width:100%; height:100%; float:left; clear:both;">Cargando</div>-->
           <form id="formIndicadores" name="formIndicadores" runat="server" method="post"  action="ReporteComercial.asp?buscar=1"  >
            <input type="hidden" id="hidTerritorios" name="hidTerritorios" value="<%=territorios%>" />
            <div id="contenidoIndicadores" class="contenidoIndicadores" >
	            <div id="filtro" class="filtro" style="top:0;">
                	&nbsp;&nbsp;Filtros por acciones:
                </div>
						<div class="bloqueColumna" >                
	                    	<div class="columnaTablaDerecha" style="width:100px;" >
		                        Comercial:&nbsp;
        	                </div>
    	                    <div class="columnaTablaIzquierda" style="width:120px;" >
	            	            <select id="cboComercial" name="cboComercial" class="cboFiltros" >
	                	            <option value="0" <% if trim(comercial)="" then %> selected="selected" <% end if %> ></option>
	                        	<%	'sql=" Select *, iif(ANA_EMAIL='" & trim(session("emailAdministracion")) & "',True,False) as usuActual From ANALISTAS Where ANA_DIRECTORCUENTA=True "
									sql=" Select * From ANALISTAS Where ANA_DIRECTORCUENTA=1 Order by ANA_NOMBRE ASC, ANA_APELLIDOS ASC "
									Set rsComerciales = connCRM.AbrirRecordSet(sql,0,1)
									while not rsComerciales.eof
											%><!-- <option <%'if trim(comercial)=trim(rsComerciales("ANA_CODIGO")) then %> selected="selected"  <%'elseif rsComerciales("usuActual") And trim(comercial)="" then%> selected="selected"  <%'end if%> value="<%'=rsComerciales("ANA_CODIGO")%>" ><%'=rsComerciales("ANA_NOMBRE")%></option>--><%										
											%> <option <%if trim(comercial)=trim(rsComerciales("ANA_CODIGO")) then %> selected="selected" <%end if%> value="<%=rsComerciales("ANA_CODIGO")%>" ><%=rsComerciales("ANA_NOMBRE")& " " & rsComerciales("ANA_APELLIDOS") %></option><%																					

'										if rsComerciales("usuActual") then
'											codComercialActual=rsComerciales("ANA_CODIGO")
'										end if
										rsComerciales.movenext
									wend
									rsComerciales.cerrar()
									set rsComerciales=nothing 
								%>
    		        	    	</select>
            	            </div>
						</div>

						<div class="bloqueColumna" >                
	                    	<div class="columnaTablaDerecha" style="width:80px;" >
		                        Campaρa:&nbsp;
        	                </div>
    	                    <div class="columnaTablaIzquierda" style="width:300px;" >
   	            	            <select id="cboCampanna" name="cboCampanna" class="cboFiltros" >
	                	            <option value="0" <%if trim(campana)="0" then%> selected="selected" <%end if%> ></option>
    	                    	<%	sql=" Select * From CAMPANAS Order By CAMP_FINICIO DESC  "
									Set rsCampannas = connCRM.AbrirRecordSet(sql,0,1)
									while not rsCampannas.eof
										%><option <% if trim(campana)=trim(rsCampannas("CAMP_CODIGO")) then %>  selected="selected" <% end if%>  value="<%=rsCampannas("CAMP_CODIGO")%>" ><%=rsCampannas("CAMP_NOMBRE")%></option><%
										rsCampannas.movenext
									wend
									rsCampannas.cerrar()
									set rsCampannas=nothing 
								%>
   				            	</select>
            	            </div>
						</div>    
						<div class="bloqueColumna" >                
	                    	<div class="columnaTablaDerecha" style="width:90px;" >
		                        Fecha desde:&nbsp;
        	                </div>
    	                    <div class="columnaTablaIzquierda" style="width:90px;" >
	    		            	<input id="txtFDesde" name="txtFDesde" type="text" class="cajaTextoFecha"  style="width:70%;"   maxlength="10"  value="<%=fechaDesde%>" />
            	            </div>
						</div>
						<div class="bloqueColumna" >                        
	                    	<div class="columnaTablaDerecha" style="width:70px;" >
		                        Fecha hasta:&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:90px;" >
    		    	        	<input id="txtFHasta" name="txtFHasta" type="text" class="cajaTextoFecha" style="width:70%;"   maxlength="10"  value="<%=fechaHasta%>"  />
	                        </div>                        
						</div>
						<div class="bloqueColumna" >                        
	                    	<div class="columnaTablaDerecha" style="width:150px;" >
		                        <label for="chkConAreas" style="cursor:pointer;">Mostrar areas de negocio:</label>&nbsp;
        	                </div>
            	            <div class="columnaTablaIzquierda" style="width:40px;" >
    		    	        	<input id="chkConAreas" name="chkConAreas" type="checkbox"  style=" cursor:pointer" <%if conAreas then%> checked="checked"<%end if%>    />
	                        </div>                        
						</div>                        
				     <style type="text/css">
							.claseListaDatos { list-style:none; float:left; margin:5px 0 0 0; padding:0; text-align:left; width:300px; height:270px; overflow:auto; }
							.elemListaDatos { border-top:solid 1px #C0C0C0/*#ff6600*/;  width:100%; float:left; cursor:pointer; }
							.elemListaDatosSeleccion { border-top:solid 1px #C0C0C0/*#ff6600*/;  width:100%; float:left; cursor:pointer; background-color:green; color:#fff; }
							.elemListaDatos:hover { background-color: #C0C0C0; color:#fff;}
							.elemListaDatosSeleccion:hover { border-top:solid 1px #C0C0C0/*#ff6600*/;  width:100%; float:left; cursor:pointer;  }							
							.imgCerrarCapaDatos { background-image:url(/dmcrm/APP/imagenes/cerrar.gif); cursor:pointer; width:16px; border:0; margin:0; padding:0; float:right; position:absolute; top:0; right:0;   }
							.imgCerrarCapaDatos:hover { background-image:url(/dmcrm/APP/imagenes/cerrarHover.gif);  }			
							.verDatos { border:solid 1px #C0C0C0; background-color:#F1F1F1; width:180px; text-align:center;}
							.verDatos:hover {  background-color:#C0C0C0;}
					</style>
						<div class="bloqueColumna" >                        
	                    	<div class="columnaTablaDerecha" style="width:150px; position:relative;" >
				                <a style="float:left;" id="lnkVerProvincias" class="verDatos"  >Seleccionar provincias</a>
                                <div id="capaVerProvincias" style=" border:solid 1px #808080; font-family:Arial; text-align:left; width:178px; background-color:#F1F1F1; position:relative; padding-left:2px; padding-top:2px; margin-top:17px; display:none;">
                                
                                </div>
                                <div id="capaDatos" style="border:solid 1px gray; width:300px; height:300px; float:left; display:none; position:absolute; top:0; left:0; background-color:#F5F5F5; ">
                                	<input type="text" id="filtroCapaDatos"  style="width:278px; float:left;" />
<!--                                    <img style="float:right;" id="cerrarCapaDatos" src="/dmcrm/APP/imagenes/cerrar.gif" />-->
                                    <a id="cerrarCapaDatos" href="#" class="imgCerrarCapaDatos" title="Cerrar panel"  >&nbsp;</a>
                                    <ul id="listaDatos" class="claseListaDatos" >

                                    </ul>
                                </div>                                                                                    
        	                </div>
						</div>                                                                    
	                        
                        

                        <style type="text/css">
							.tabIndices { }
							.tabIndices:td { }
							.colCab { font-weight:bold;}	
							.colCont {}
							.colContMed { background-color:#F5F5F5;}
							.colCabImp { background-color:#000000; color:#FFF; font-weight:bold;}
							
					</style>
                        
                        <input type="submit" name="btnBuscarIndicadores" id="btnBuscarIndicadores" onclick="mostrarCarga();"  value="Buscar"  class="boton" style="float:right; margin:0 50px 0 0; "  />


						<div style=" width:98%; float:left; clear:both; border:none; margin:20px 0 0 0; padding:10px;">

							<%'Tabla Llamadas.%>
							<table class="tabIndices" border="0" rules="all" frame="border"  cellspacing="0" cellpadding="0" bordercolor="#C0C0C0" >
                            	<tr>
                                	<td rowspan="2" colspan="2" style="border-left:none; border-top:none;" >&nbsp;</td>
                                	<td class="colCab" rowspan="2" align="center" valign="top"  width="65px"  style="background-color:#eeeeee;"  >TOTALES</td>
                                	<td class="colCab" colspan="2" align="center" valign="middle" style="background-color:#eeeeee;"  >TIPO CLIENTE</td> 
                                    <%if numTerritorios>0 then%>
                                	<td class="colCab" colspan="<%=numTerritorios + 1%>" align="center" valign="middle" style="background-color:#eeeeee;" >TERRITORIO</td>
                                    <%end if%>
                                </tr>
                            	<tr>
                                	<td  align="center" valign="middle"  width="65px"  style="background-color:#F5F5F5;"  >ANTIGUO</td> 
                                	<td  align="center" valign="middle" width="65px"   style="background-color:#F5F5F5;"  >NUEVO</td>
                                     <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										while not rsTerritorios.eof
										%>
											<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  ><%=ucase(rsTerritorios("PROV_NOMBRE"))%></td>                                                                                                            
            	                        <%rsTerritorios.movenext
										wend%>
                    	            	<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  >OTROS</td>
                                     <%end if   %>
                                </tr>                                
                            	<tr>
                                	<td class="colCab" rowspan="3"  width="100px" align="center" valign="middle"  style=" background-color:#ff6600; color:white;" >LLAMADAS</td> 
                                </tr>                                                                
                            	<tr>
                                	<td align="right" valign="middle"  width="290px"   >NΊ Llamadas realizadas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(LlamadasTerminadas,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(LlamadasNuevoCliTerminadas,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(LlamadasViejoCliTerminadas,0)%></td> 
                                     <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvLlamadasTerminadas(contBucle),0)%></td>                	                    <%contBucle=contBucle + 1
										rsTerritorios.movenext
										wend%>
	                                	<td align="center" valign="middle"  ><%=formatnumber(arrProvLlamadasTerminadas(contBucle),0)%></td> 
									 <%end if %>
                                </tr>                                                                                                
                            	<tr>
                                	<td align="right" valign="middle" width="290px"  >% cumplimientos sobre total</td> 
                                	<td align="center" valign="middle"  ><%=PorcenLlamadasTerminadas & "%"%></td>                                     
                                	<td align="center" valign="middle"  ><%=PorcenLlamadasNuevoCliTerminadas & "%"%></td>                                     
                                	<td align="center" valign="middle"  ><%=PorcenLlamadasViejoCliTerminadas & "%"%></td>                                     
                                     <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=arrProvPorcLlamadasTerminadas(contBucle) & "%"%></td>                	                    <%contBucle=contBucle + 1
										rsTerritorios.movenext
										wend%>
                            	    	<td align="center" valign="middle"  ><%=arrProvPorcLlamadasTerminadas(contBucle) & "%"%></td> 
                                     <%end if%>   
                                </tr>                                   
                            </table>
                            
                            <br/><br/>
                            
							<%'Tabla Visitas.%>
							<table class="tabIndices" border="0" rules="all" frame="border"  cellspacing="0" cellpadding="0" bordercolor="#C0C0C0" >
                            	<tr>
                                	<td rowspan="2" colspan="2" style="border-left:none; border-top:none;" >&nbsp;</td>
                                	<td class="colCab" rowspan="2" align="center" valign="top"  width="65px"  style="background-color:#eeeeee;"  >TOTALES</td>
                                	<td class="colCab" colspan="2" align="center" valign="middle" style="background-color:#eeeeee;"  >TIPO CLIENTE</td> 
                                    <%if numTerritorios>0 then%>
                                	<td class="colCab" colspan="<%=numTerritorios + 1%>" align="center" valign="middle" style="background-color:#eeeeee;" >TERRITORIO</td>
                                    <%end if%>
                                </tr>
                            	<tr>
                                	<td  align="center" valign="middle"  width="65px"  style="background-color:#F5F5F5;"  >ANTIGUO</td> 
                                	<td  align="center" valign="middle" width="65px"   style="background-color:#F5F5F5;"  >NUEVO</td>
                                     <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										while not rsTerritorios.eof
										%>
											<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  ><%=UCASE(rsTerritorios("PROV_NOMBRE"))%></td>            		                        <%rsTerritorios.movenext
										wend%>
                        	        	<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  >OTROS</td>
									<%end if	%>                                        
                                </tr>                                
                            	<tr>
                                	<td class="colCab" rowspan="3"  width="100px" align="center" valign="middle"  style=" background-color:#ff6600; color:white;" >VISITAS</td> 
                                </tr>                                                                
                            	<tr>
                                	<td align="right" valign="middle"  width="290px"   >NΊ de visitas realizadas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(VisitasTerminadas,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(VisitasNuevoCliTerminadas,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(VisitasViejoCliTerminadas,0)%></td>                                     
                                     <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvVisitasTerminadas(contBucle),0)%></td>                	                    <%contBucle=contBucle + 1
										rsTerritorios.movenext
										wend%>
                            	    	<td align="center" valign="middle"  ><%=formatnumber(arrProvVisitasTerminadas(contBucle),0)%></td> 
                                      <%end if%>  
                                </tr>                                                                                                
                            	<tr>
                                	<td align="right" valign="middle" width="290px"  >% logro de visitas sobre llamadas realizadas  <%if trim(filtroAnnoDesde)<>"" then response.Write("en " & year(FechaDesde)) end if%></td> 
                                	<td align="center" valign="middle"  ><%=PorcenVisitasTerminadasConLlamadas & "%"%></td>                                     
                                	<td align="center" valign="middle"  ><%=PorcenVisitasNuevoCliTerminadasConLlamadas & "%"%></td>                                     
                                	<td align="center" valign="middle"  ><%=PorcenVisitasViejoCliTerminadasConLlamadas & "%"%></td>                                     
                                     <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=arrProvPorcVisitasTerminadasConLlamadas(contBucle) & "%"%></td>                	    	                <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
    	                            	<td align="center" valign="middle"  ><%=arrProvPorcVisitasTerminadasConLlamadas(contBucle) & "%"%></td> 
                                       <%end if%> 
                                </tr>                                   
                            </table>
                            <br/><br/>
							<%'Tabla Ofertas.%>
							<table class="tabIndices" border="0" rules="all" frame="border"  cellspacing="0" cellpadding="0" bordercolor="#C0C0C0" >
                            	<tr>
                                	<td rowspan="2" colspan="2" style="border-left:none; border-top:none;" >&nbsp;</td>
                                	<td class="colCab" rowspan="2" align="center" valign="top"  width="65px"  style="background-color:#eeeeee;"  >TOTALES</td>
                                	<td class="colCab" colspan="2" align="center" valign="middle" style="background-color:#eeeeee;"  >TIPO CLIENTE</td> 
                                    <%if numTerritorios>0 then%>
                                	<td class="colCab" colspan="<%=numTerritorios + 1%>" align="center" valign="middle" style="background-color:#eeeeee;" >TERRITORIO</td>
                                    <%end if%>
                                    <%if numAreas>0 then%>
	                                    <td class="colCab" colspan="<%=numAreas%>" align="center" valign="middle" style="background-color:#eeeeee;" >POR AREA</td>
                                    <%end if%>
                                    <td class="colCab" colspan="3" align="center" valign="middle" style="background-color:#eeeeee;" >PROBABILIDAD</td>
                                </tr>
                            	<tr>
                                	<td  align="center" valign="middle"  width="65px"  style="background-color:#F5F5F5;"  >ANTIGUO</td> 
                                	<td  align="center" valign="middle" width="65px"   style="background-color:#F5F5F5;"  >NUEVO</td>
									<%
									if numTerritorios>0 then
										rsTerritorios.movefirst
										while not rsTerritorios.eof
										%>
											<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  ><%=UCASE(rsTerritorios("PROV_NOMBRE"))%></td>            	                        
											<%rsTerritorios.movenext
										wend%>
                    	            	<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  >OTROS</td>
									<%end if%>
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										while not rsAreaNegocio.eof
										%>
											<td  align="center" valign="middle"  width="65px" style="background-color:#F5F5F5;"  ><%=UCASE(rsAreaNegocio("TAN_DESCRIPCION"))%></td>
	                                    <%rsAreaNegocio.movenext
										wend
									end if%>
                                    <td  align="center" valign="middle" width="65px"   style="background-color:#F5F5F5;"  >ALTA</td>
                                    <td  align="center" valign="middle" width="65px"   style="background-color:#F5F5F5;"  >MEDIA</td>
                                    <td  align="center" valign="middle" width="65px"   style="background-color:#F5F5F5;"  >BAJA</td>                                                                        
                                </tr> 
                                <%'Ofertas presentadas%>                               
                            	<tr>
                                	<td class="colCab" rowspan="5"  width="100px" align="center" valign="middle"  style=" background-color:#ff6600; color:white;" >OFERTAS PRESENTADAS</td> 
                                </tr>                                                                
                            	<tr>
                                	<td align="right" valign="middle"  width="290px" >% de ofertas presentadas por vistas realizadas</td> 
                                	<td align="center" valign="middle"  ><%=PorcenOPresentadasTerminadas & "%"%></td>                                     
                                	<td align="center" valign="middle"  ><%=PorcenOPresentadasNuevoCliTerminadas & "%"%></td>                                     
                                	<td align="center" valign="middle"  ><%=PorcenOPresentadasViejoCliTerminadas & "%"%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=arrProvPorcOPresentadasTerminadas(contBucle) & "%"%></td>                		                    
											<%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                                		<td align="center" valign="middle"  ><%=arrProvPorcOPresentadasTerminadas(contBucle) & "%"%></td> 
                                      <%end if%>  
                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	                                    <td align="center" valign="middle"  ><%=arrPorcAreasOPresentadas(contBucle) & "%"%></td>  
	                                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if	%>    
                                	<td align="center" valign="middle"  ><%=PorcenOPresentadasAltaTerminadas & "%"%></td>                                                   
                                	<td align="center" valign="middle"  ><%=PorcenOPresentadasMediaTerminadas & "%"%></td>
                                	<td align="center" valign="middle"  ><%=PorcenOPresentadasBajaTerminadas & "%"%></td>                                                                          
                                </tr>     
                            	<tr>
                                	<td align="right" valign="middle"width="290px"  >NΊ ofertas presentadas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(OPresentadasTotales,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(OPresentadasNuevoCliTotales,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(OPresentadasViejoCliTotales,0)%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvOPresentadasTotales(contBucle),0)%></td>
	            	                        <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td align="center" valign="middle"  ><%=formatnumber(arrProvOPresentadasTotales(contBucle),0)%></td> 
                                     <%end if%>  
                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td align="center" valign="middle"  ><%=arrValorAreasOPresentadasTotales(contBucle)%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>         
                                	<td align="center" valign="middle"  ><%=formatnumber(OPresentadasAltaTotales,0)%></td>                                     
                                  	<td align="center" valign="middle"  ><%=formatnumber(OPresentadasMediaTotales,0)%></td>
                                    <td align="center" valign="middle"  ><%=formatnumber(OPresentadasBajaTotales,0)%></td>    
                                </tr>                                   
                            	<tr>
                                	<td align="right" valign="middle" width="290px" >Importe acumulado ofertas presentadas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPresentadasTotales,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPresentadasNuevoCliTotales,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPresentadasViejoCliTotales,0) & ""%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvCantOPresentadasTotales(contBucle),0) & ""%></td>
	                                    <%contBucle=contBucle + 1
										rsTerritorios.movenext
										wend%>
            	                    	<td align="center" valign="middle"  ><%=formatnumber(arrProvCantOPresentadasTotales(contBucle),0) & ""%></td> 
									<%end if%>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	                	                    <td align="center" valign="middle"  ><%=formatnumber(arrCantAreasOPresentadasTotales(contBucle),0) & ""%></td>  
                        	            <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend                         
									end if%>                                        
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPresentadasAltaTotales,0) & ""%></td>
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPresentadasMediaTotales,0) & ""%></td>
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPresentadasBajaTotales,0) & ""%></td>                                                                                                                             
                                </tr>         
                                <tr>
                                	<td align="right" valign="middle" width="290px" >Importe medio ofertas presentadas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPresentadasTotales,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPresentadasNuevoCliTotales,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPresentadasViejoCliTotales,0) & ""%></td>                                     
									 <%
									 if numTerritorios>0 then
									 	rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvMediaOPresentadasTotales(contBucle),0) & ""%></td>
		                                    <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td align="center" valign="middle"  ><%=formatnumber(arrProvMediaOPresentadasTotales(contBucle),0) & ""%></td> 
									<%end if%>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td align="center" valign="middle"  ><%=formatnumber(arrMediaAreasOPresentadasTotales(contBucle),0) & ""%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>	
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPresentadasAltaTotales,0) & ""%></td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPresentadasMediaTotales,0) & ""%></td>
                                    <td align="center" valign="middle"  ><%=formatnumber(MediaCantOPresentadasBajaTotales,0) & ""%></td>
                                </tr> 
                                <%'ofertas aceptadas%>   
                            	<tr  > <!--style="border-top:solid #C0C0C0 2px;"-->
                                	<td class="colCab" rowspan="5"   width="100px" align="center" valign="middle"  style=" background-color:#ff6600; color:white;" >OFERTAS ACEPTADAS</td> 
                                </tr>                                                                
                            	<tr>
                                	<td class="colContMed" align="right" valign="middle"  width="290px" >% de ofertas aceptadas sobre presentadas</td> 
                                	<td class="colContMed" align="center" valign="middle"  ><%=PorcenOAceptadasTerminadas & "%"%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=PorcenOAceptadasNuevoCliTerminadas & "%"%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=PorcenOAceptadasViejoCliTerminadas & "%"%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td class="colContMed" align="center" valign="middle"  ><%=arrProvPorcOAceptadasTerminadas(contBucle) & "%"%></td>
		                                    <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td class="colContMed" align="center" valign="middle"  ><%=arrProvPorcOAceptadasTerminadas(contBucle) & "%"%></td> 
									<%end if%>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
											%>
	                            	        <td class="colContMed" align="center" valign="middle"  ><%=arrPorcAreasOAceptadas(contBucle) & "%"%></td>  
            	            	            <%contBucle=contBucle + 1
											rsAreaNegocio.movenext
										wend
									end if%>	    
                                	<td class="colContMed" align="center" valign="middle"  ><%=PorcenOAceptadasAltaTerminadas & "%"%></td>   
                                	<td class="colContMed" align="center" valign="middle"  ><%=PorcenOAceptadasMediaTerminadas & "%"%></td>
                                	<td class="colContMed" align="center" valign="middle"  ><%=PorcenOAceptadasBajaTerminadas & "%"%></td>                                                                      
                                </tr>                                                                                                
                            	<tr>
                                	<td class="colContMed" align="right" valign="middle" width="290px" >NΊ de propuestas aceptadas</td> 
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(OAceptadasTerminadas,0)%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(OAceptadasNuevoCliTerminadas,0)%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(OAceptadasViejoCliTerminadas,0)%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrProvOAceptadasTerminadas(contBucle),0)%></td>
		                                    <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrProvOAceptadasTerminadas(contBucle),0)%></td> 
									<%end if%>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td class="colContMed" align="center" valign="middle"  ><%=arrValorAreasOAceptadas(contBucle)%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(OAceptadasAltaTerminadas,0)%></td>                                     
                                  	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(OAceptadasMediaTerminadas,0)%></td>
                                    <td class="colContMed" align="center" valign="middle"  ><%=formatnumber(OAceptadasBajaTerminadas,0)%></td>    
                                </tr>                                   
                            	<tr>
                                	<td class="colContMed" align="right" valign="middle" width="290px" >Importe acumulado ofertas aceptadas</td> 
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(CantOAceptadasTerminadas,0) & ""%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(CantOAceptadasNuevoCliTerminadas,0) & ""%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(CantOAceptadasViejoCliTerminadas,0) & ""%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrProvCantOAceptadasTerminadas(contBucle),0) & ""%></td>
		                                    <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrProvCantOAceptadasTerminadas(contBucle),0) & ""%></td> 
									<%end if %>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
											%><td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrCantAreasOAceptadas(contBucle),0) & ""%></td>  
	            	                        <%contBucle=contBucle + 1
											rsAreaNegocio.movenext
										wend
									end if%>	
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(CantOAceptadasAltaTerminadas,0) & ""%></td>
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(CantOAceptadasMediaTerminadas,0) & ""%></td>
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(CantOAceptadasBajaTerminadas,0) & ""%></td>                                                                                                                             
                                </tr>         
                                <tr>
                                	<td class="colContMed" align="right" valign="middle" width="290px" >Importe medio ofertas aceptadas</td> 
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(MediaCantOAceptadasTerminadas,0) & ""%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(MediaCantOAceptadasNuevoCliTerminadas,0) & ""%></td>                                     
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(MediaCantOAceptadasViejoCliTerminadas,0) & ""%></td>                                     
									 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrProvMediaOAceptadasTerminadas(contBucle),0) & ""%></td>
	                                    <%contBucle=contBucle + 1
										rsTerritorios.movenext
										wend%>
            	                    	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrProvMediaOAceptadasTerminadas(contBucle),0) & ""%></td> 
                                     <%end if%>   
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td class="colContMed" align="center" valign="middle"  ><%=formatnumber(arrMediaAreaOAceptadas(contBucle),0) & ""%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>	
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(MediaCantOAceptadasAltaTerminadas,0) & ""%></td> 
                                	<td class="colContMed" align="center" valign="middle"  ><%=formatnumber(MediaCantOAceptadasMediaTerminadas,0) & ""%></td>
                                    <td class="colContMed" align="center" valign="middle"  ><%=formatnumber(MediaCantOAceptadasBajaTerminadas,0) & ""%></td>
                                </tr>  
                                <%'ofertas pendientes%>   
                            	<tr > <!--style="border-top:solid #C0C0C0 2px;"-->
                                	<td class="colCab" rowspan="4"   width="100px" align="center" valign="middle"  style=" background-color:#ff6600; color:white; " >OFERTAS PENDIENTES</td> 
                                </tr>                                                                
                            	<tr>
                                	<td align="right" valign="middle" width="290px" >NΊ de ofertas vivas
									<a id="cResumenOfertas" style="display:none;" href="/dmcrm/APP/Comercial/ReporteComercialResumen.asp?comercial=<%=trim(comercial)%>&campana=<%=trim(campana)%>"></a>
									</td> 
                                	<td align="center" valign="middle" class="linkResumen"  ><%=formatnumber(OPendientesTerminadas,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(OPendientesNuevoCliTerminadas,0)%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(OPendientesViejoCliTerminadas,0)%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvOPendientesTerminadas(contBucle),0)%></td>
	                                    <%contBucle=contBucle + 1
										rsTerritorios.movenext
										wend%>
            	                    	<td align="center" valign="middle"  ><%=formatnumber(arrProvOPendientesTerminadas(contBucle),0)%></td> 
                                     <%end if%>   
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td align="center" valign="middle"  ><%=arrValorAreasOPendientes(contBucle)%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>	
                                	<td align="center" valign="middle"  ><%=formatnumber(OPendientesAltaTerminadas,0)%></td>                                     
                                  	<td align="center" valign="middle"  ><%=formatnumber(OPendientesMediaTerminadas,0)%></td>
                                    <td align="center" valign="middle"  ><%=formatnumber(OPendientesBajaTerminadas,0)%></td>    
                                </tr>                                   
                            	<tr>
                                	<td align="right" valign="middle" width="290px" >Importe acumulado ofertas vivas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPendientesTerminadas,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPendientesNuevoCliTerminadas,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPendientesViejoCliTerminadas,0) & ""%></td>                                     
                                	 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvCantOPendientesTerminadas(contBucle),0) & ""%></td>
		                                    <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td align="center" valign="middle"  ><%=formatnumber(arrProvCantOPendientesTerminadas(contBucle),0) & ""%></td> 
									<%end if%>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td align="center" valign="middle"  ><%=formatnumber(arrCantAreasOPendientes(contBucle),0) & ""%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>	
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPendientesAltaTerminadas,0) & ""%></td>
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPendientesMediaTerminadas,0) & ""%></td>
                                	<td align="center" valign="middle"  ><%=formatnumber(CantOPendientesBajaTerminadas,0) & ""%></td>                                                                                                                             
                                </tr>         
                                <tr>
                                	<td align="right" valign="middle" width="290px" >Importe medio ofertas vivas</td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPendientesTerminadas,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPendientesNuevoCliTerminadas,0) & ""%></td>                                     
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPendientesViejoCliTerminadas,0) & ""%></td>                                     
									 <%
									 if numTerritorios>0 then
										 rsTerritorios.movefirst
										 contBucle=0
										while not rsTerritorios.eof
										%>
											<td align="center" valign="middle"  ><%=formatnumber(arrProvMediaOPendientesTerminadas(contBucle),0) & ""%></td>
		                                    <%contBucle=contBucle + 1
											rsTerritorios.movenext
										wend%>
                    	            	<td align="center" valign="middle"  ><%=formatnumber(arrProvMediaOPendientesTerminadas(contBucle),0) & ""%></td> 
									<%end if%>                                    
                                    <%
									if numAreas>0 then
										rsAreaNegocio.movefirst
										contBucle=0
										while not rsAreaNegocio.eof
										%>
	        	                            <td align="center" valign="middle"  ><%=formatnumber(arrMediaAreasOPendientes(contBucle),0) & ""%></td>  
                	                    <%contBucle=contBucle + 1
										rsAreaNegocio.movenext
										wend
									end if%>	
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPendientesAltaTerminadas,0) & ""%></td> 
                                	<td align="center" valign="middle"  ><%=formatnumber(MediaCantOPendientesMediaTerminadas,0) & ""%></td>
                                    <td align="center" valign="middle"  ><%=formatnumber(MediaCantOPendientesBajaTerminadas,0) & ""%></td>
                                </tr>                                                                                             
                            </table>                                                      
                        
                        </div>
            

				<div style="clear:both; height:5px;  width:100%;"></div>

                <br/>
                
				<%                
				sql=" Select Distinct A.ANA_CODIGO,A.ANA_NOMBRE + ' ' + A.ANA_APELLIDOS as NombreCompleto "
				sql=sql & " From ANALISTAS A "
				sql=sql & " Where A.ANA_DIRECTORCUENTA=1 "
				if trim(comercial)<>"" And trim(comercial)<>"0" then
					sql=sql & " And A.ANA_CODIGO=" & trim(comercial) & " "
				end if
				sql=sql & " Order by A.ANA_NOMBRE + ' ' + A.ANA_APELLIDOS ASC "

				Set rsAnalistas = connCRM.AbrirRecordset(sql,0,1)
				while not rsAnalistas.eof 
					%><h1><%=rsAnalistas("NombreCompleto")%></h1>                    
		            <table class="tabIndices" border="0" rules="all" frame="border"  cellspacing="1" cellpadding="0" bordercolor="#C0C0C0" >
						<tr>
    		            	<td class="colCabImp"   align="center" valign="middle" width="600px"   >Cliente - Campaρa - Proyecto</td>
        		            <td class="colCabImp"   align="center" valign="middle" width="90px"   >Cantidad Aprobada</td>
            		        <td class="colCabImp"   align="center" valign="middle" width="90px"   >NΊ Comercial Visitas</td>
	            	    	<td class="colCabImp"   align="center" valign="middle" width="90px"   >Cantidad Visita</td>                            
            		        <td class="colCabImp"   align="center" valign="middle" width="90px"   >NΊ Comercial Ofertas</td>  
	            	    	<td class="colCabImp"   align="center" valign="middle" width="90px"   >Cantidad Oferta</td>                            
            		        <td class="colCabImp"   align="center" valign="middle" width="90px"   >NΊ Comercial Cierres</td>                                                      
	            	    	<td class="colCabImp"   align="center" valign="middle" width="90px"   >Cantidad Cierres</td>
                        </tr>   
                        <%
				
					tipoAnterior=""
					sql= " 	Select A.ACCI_SUCLIENTE,C.CLIE_NOMBRE,A.ACCI_SUCAMPANA,CAM.CAMP_NOMBRE,A.ACCI_PROYECTO,sum(A.ACCI_TIPO6CANTIDAD) as cantAprobada "
					sql=sql & " 	From (( Acciones_Cliente A "
					sql=sql & " 	inner join Clientes C on (A.ACCI_SUCLIENTE=C.CLIE_CODIGO) ) "
					sql=sql & " 	inner join Campanas CAM on (A.ACCI_SUCAMPANA=CAM.CAMP_CODIGO) ) "
					sql=sql & " 	Where (A.ACCI_SUTIPO=7 And ACCI_TIPO7FINALIZACION=1 )  And A.ACCI_Terminada=1  "
'					sql=sql & " 	Where ((A.ACCI_SUTIPO=7 And ACCI_TIPO7FINALIZACION=1 ) Or A.ACCI_SUTIPO=6 Or A.ACCI_SUTIPO=4 ) And A.ACCI_Terminada=true  "					
					sql = sql &  filtroCampana & fitroFechas						
					sql=sql & " group by A.ACCI_SUCLIENTE,C.CLIE_NOMBRE,A.ACCI_SUCAMPANA,CAM.CAMP_NOMBRE,A.ACCI_PROYECTO"					
					sql=sql & " order by sum(A.ACCI_TIPO6CANTIDAD) DESC,C.CLIE_NOMBRE,CAM.CAMP_NOMBRE,A.ACCI_PROYECTO "	

					Set rsAcciones = connCRM.AbrirRecordset(sql,0,1)
					contDes=1
					TotalComercial=0
					totalVisita=0	
					totalOferta=0	
					totalCierre=0						


					while not rsAcciones.eof 					
						if trim(rsAcciones("cantAprobada"))<>"0" And trim(rsAcciones("cantAprobada"))<>"" And not isnull(trim(rsAcciones("cantAprobada"))) then
							ciclo=rsAcciones("CLIE_NOMBRE") & " | " & rsAcciones("CAMP_NOMBRE") & " | " & rsAcciones("ACCI_PROYECTO")

							sql= " Select '4' as tipo,(CASE WHEN ACCI_SUCOMERCIAL IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL))<>'' And LTRIM(RTRIM(ACCI_SUCOMERCIAL))<>0 THEN (CASE WHEN ACCI_SUCOMERCIAL2 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL2))<>'' And ACCI_SUCOMERCIAL2 IS NOT NULL THEN (CASE WHEN ACCI_SUCOMERCIAL3 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL3))<>'' And ACCI_SUCOMERCIAL3 IS NOT NULL THEN (CASE WHEN ACCI_SUCOMERCIAL4 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL4))<>'' And ACCI_SUCOMERCIAL4 IS NOT NULL THEN 4 ELSE 3 END) ELSE 2 END) ELSE 1 END) ELSE 0 END) as contComer THEN (CASE WHEN ACCI_SUCOMERCIAL=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL2=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL3=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL4=" & rsAnalistas("ANA_CODIGO") & " THEN 'true' ELSE 'false' END) as existeComercial " ',1 as finalizacion,ACCI_TERMINADA "
							sql=sql & " From ACCIONES_CLIENTE "
							sql=sql & " Where ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' "
							sql=sql & " And ACCI_TERMINADA = 1 And ACCI_SUTIPO=4 "
							sql=sql & " And   ( Select count(A2.ACCI_CODIGO) as contTipo7 From Acciones_cliente A2 Where A2.ACCI_SUTIPO=7 And A2.ACCI_TIPO7FINALIZACION=1 And A2.ACCI_TERMINADA=1 And A2.ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And A2.ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And A2.ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' " &  filtroCampana & fitroFechas	 & " )>0 "
'							sql = sql & filtrosComunes 
							
							sql = sql & " Union All "

							sql= sql & " Select '6' as tipo,(CASE WHEN ACCI_SUCOMERCIAL IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL))<>'' And LTRIM(RTRIM(ACCI_SUCOMERCIAL))<>0 THEN (CASE WHEN ACCI_SUCOMERCIAL2 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL2))<>'' And ACCI_SUCOMERCIAL2 IS NOT NULL  THEN (CASE WHEN ACCI_SUCOMERCIAL3 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL3))<>'' And ACCI_SUCOMERCIAL3 IS NOT NULL THEN (CASE WHEN ACCI_SUCOMERCIAL4 IS NOT NULL And ACCI_SUCOMERCIAL4 IS NOT NULL<>'' And ACCI_SUCOMERCIAL4 IS NOT NULL THEN 4 ELSE3 END) ELSE 2 END) ELSE 1 END) ELSE 0 END) as contComer,iif(ACCI_SUCOMERCIAL=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL2=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL3=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL4=" & rsAnalistas("ANA_CODIGO") & ",1,false) as existeComercial " ',1 as finalizacion,ACCI_TERMINADA "
							sql=sql & " From ACCIONES_CLIENTE "
							sql=sql & " Where ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' "
							sql = sql & " And ACCI_TERMINADA = 1 And ACCI_SUTIPO=6 "
							sql=sql & " And   ( Select count(A2.ACCI_CODIGO) as contTipo7 From Acciones_cliente A2 Where A2.ACCI_SUTIPO=7 And A2.ACCI_TIPO7FINALIZACION=1 And A2.ACCI_TERMINADA=1 And A2.ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And A2.ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And A2.ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' " &  filtroCampana & fitroFechas	 & " )>0 "							
'							sql = sql & filtrosComunes 							
							
							sql = sql & " Union All "

							sql=sql & " Select '7' as tipo,(CASE WHEN ACCI_SUCOMERCIAL IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL))<>'' And LTRIM(RTRIM(ACCI_SUCOMERCIAL))<>0 THEN (CASE WHEN ACCI_SUCOMERCIAL2 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL2))<>'' And ACCI_SUCOMERCIAL2 IS NOT NULL THEN (CASE WHEN ACCI_SUCOMERCIAL3 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL3))<>'' And ACCI_SUCOMERCIAL3 IS NOT NULL, (CASE WHEN ACCI_SUCOMERCIAL4 IS NOT NULL And LTRIM(RTRIM(ACCI_SUCOMERCIAL4))<>'' And ACCI_SUCOMERCIAL4 IS NOT NULL THEN 4 ELSE 3 END) ELSE 2 END) ELSE 1 END) ELSE 0 END) as contComer THEN(CASE WHEN ACCI_SUCOMERCIAL=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL2=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL3=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL4=" & rsAnalistas("ANA_CODIGO") & " THEN true ELSE false END) as existeComercial "',ACCI_TIPO7FINALIZACION as finalizacion,ACCI_TERMINADA "
							sql=sql & " From ACCIONES_CLIENTE "
							sql=sql & " Where ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' "
							sql = sql & " And ACCI_TERMINADA = 1 And ACCI_SUTIPO=7 "							
							sql = sql &  filtroCampana & fitroFechas	




							'sql= " Select '4' as tipo,iif( not isnull(ACCI_SUCOMERCIAL) And trim(ACCI_SUCOMERCIAL)<>'' And trim(ACCI_SUCOMERCIAL)<>0, iif(not isnull(ACCI_SUCOMERCIAL2) And trim(ACCI_SUCOMERCIAL2)<>'' And trim(ACCI_SUCOMERCIAL2), iif(not isnull(ACCI_SUCOMERCIAL3) And trim(ACCI_SUCOMERCIAL3)<>'' And trim(ACCI_SUCOMERCIAL3), iif(not isnull(ACCI_SUCOMERCIAL4) And trim(ACCI_SUCOMERCIAL4)<>'' And trim(ACCI_SUCOMERCIAL4),4 ,3)  , 2)  , 1)  ,0) as contComer,iif(ACCI_SUCOMERCIAL=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL2=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL3=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL4=" & rsAnalistas("ANA_CODIGO") & ",true,false) as existeComercial " ',1 as finalizacion,ACCI_TERMINADA "
							'sql=sql & " From ACCIONES_CLIENTE "
							'sql=sql & " Where ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' "
							'sql=sql & " And ACCI_TERMINADA And ACCI_SUTIPO=4 "
							'sql=sql & " And   ( Select count(A2.ACCI_CODIGO) as contTipo7 From Acciones_cliente A2 Where A2.ACCI_SUTIPO=7 And A2.ACCI_TIPO7FINALIZACION=1 And A2.ACCI_TERMINADA=1 And A2.ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And A2.ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And A2.ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' " &  filtroCampana & fitroFechas	 & " )>0 "
'							sql = sql & filtrosComunes 
							
							'sql = sql & " Union All "

							'sql= sql & " Select '6' as tipo,iif( not isnull(ACCI_SUCOMERCIAL) And trim(ACCI_SUCOMERCIAL)<>'' And trim(ACCI_SUCOMERCIAL)<>0, iif(not isnull(ACCI_SUCOMERCIAL2) And trim(ACCI_SUCOMERCIAL2)<>'' And trim(ACCI_SUCOMERCIAL2), iif(not isnull(ACCI_SUCOMERCIAL3) And trim(ACCI_SUCOMERCIAL3)<>'' And trim(ACCI_SUCOMERCIAL3), iif(not isnull(ACCI_SUCOMERCIAL4) And trim(ACCI_SUCOMERCIAL4)<>'' And trim(ACCI_SUCOMERCIAL4),4 ,3)  , 2)  , 1)  ,0) as contComer,iif(ACCI_SUCOMERCIAL=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL2=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL3=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL4=" & rsAnalistas("ANA_CODIGO") & ",1,false) as existeComercial " ',1 as finalizacion,ACCI_TERMINADA "
							'sql=sql & " From ACCIONES_CLIENTE "
							'sql=sql & " Where ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' "
							'sql = sql & " And ACCI_TERMINADA And ACCI_SUTIPO=6 "
							'sql=sql & " And   ( Select count(A2.ACCI_CODIGO) as contTipo7 From Acciones_cliente A2 Where A2.ACCI_SUTIPO=7 And A2.ACCI_TIPO7FINALIZACION=1 And A2.ACCI_TERMINADA=1 And A2.ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And A2.ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And A2.ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' " &  filtroCampana & fitroFechas	 & " )>0 "							
'							sql = sql & filtrosComunes 							
							
							'sql = sql & " Union All "

							'sql=sql & " Select '7' as tipo,iif( not isnull(ACCI_SUCOMERCIAL) And trim(ACCI_SUCOMERCIAL)<>'' And trim(ACCI_SUCOMERCIAL)<>0, iif(not isnull(ACCI_SUCOMERCIAL2) And trim(ACCI_SUCOMERCIAL2)<>'' And trim(ACCI_SUCOMERCIAL2), iif(not isnull(ACCI_SUCOMERCIAL3) And trim(ACCI_SUCOMERCIAL3)<>'' And trim(ACCI_SUCOMERCIAL3), iif(not isnull(ACCI_SUCOMERCIAL4) And trim(ACCI_SUCOMERCIAL4)<>'' And trim(ACCI_SUCOMERCIAL4),4 ,3)  , 2)  , 1)  ,0) as contComer,iif(ACCI_SUCOMERCIAL=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL2=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL3=" & rsAnalistas("ANA_CODIGO") & " Or ACCI_SUCOMERCIAL4=" & rsAnalistas("ANA_CODIGO") & ",true,false) as existeComercial "',ACCI_TIPO7FINALIZACION as finalizacion,ACCI_TERMINADA "
							'sql=sql & " From ACCIONES_CLIENTE "
							'sql=sql & " Where ACCI_SUCLIENTE=" & rsAcciones("ACCI_SUCLIENTE") & " And ACCI_SUCAMPANA=" & rsAcciones("ACCI_SUCAMPANA") & " And ACCI_PROYECTO='" & rsAcciones("ACCI_PROYECTO") & "' "
							'sql = sql & " And ACCI_TERMINADA And ACCI_SUTIPO=7 "							
							'sql = sql &  filtroCampana & fitroFechas	

							
							NComercialVisita=0
							cantidadVisita=0
							
							NComercialOferta=0							
							cantidadOferta=0							
							
							NComercialCierre=0
							cantidadCierre=0																
							evitarlinea=false

							tieneVisita=false
							tieneOferta=false
							tieneCierre=false														

							colorSelVisita="color:red;"											
							colorSelOferta="color:red;"																								
							colorSelCierre="color:red;"																					
							Set rsCiclo = connCRM.AbrirRecordset(sql,0,1)
							while not rsCiclo.eof 
'								if trim(rsCiclo("finalizacion"))<>"1" or not rsCiclo("ACCI_TERMINADA")  then
'									evitarlinea=true										
'								end if
								Select Case trim(rsCiclo("tipo"))
									Case "4":
										NComercialVisita=rsCiclo("contComer")
										if rsCiclo("existeComercial") then
											colorSelVisita=" color:green; "																								
											tieneVisita=true
										end if
									Case "6":
										NComercialOferta=rsCiclo("contComer")
										if rsCiclo("existeComercial") then
											colorSelOferta=" color:green; "																																		
											tieneOferta=true
										end if										
									Case "7":
										NComercialCierre=rsCiclo("contComer")
										if rsCiclo("existeComercial") then
											colorSelCierre=" color:green; "																																		
											tieneCierre=true
										end if										
								end Select
								rsCiclo.movenext
							wend
							'response.End()
							rsCiclo.cerrar()
							Set rsCiclo = nothing   
						
if  (tieneVisita Or tieneOferta Or tieneCierre) then'and not evitarlinea then

							porcentajeVisita=35
							porcentajeOferta=35
							porcentajeCierre=30
							if NComercialVisita=0 And NComercialOferta=0 then
								porcentajeVisita=0
								porcentajeOferta=0
								porcentajeCierre=100
							elseif NComercialVisita>0 And NComercialOferta=0  then
								porcentajeVisita=35 + 20
								porcentajeOferta=0
								porcentajeCierre=30 + 15						
							elseif NComercialVisita=0 And NComercialOferta>0  then
								porcentajeVisita=0
								porcentajeOferta=35 + 20
								porcentajeCierre=30 + 15														
							end if
							
							
							if NComercialVisita>0  then
								if tieneVisita then
									cantidadVisita= (((rsAcciones("cantAprobada") * porcentajeVisita)/100))  /NComercialVisita
								end if	
							end if
							
							if NComercialOferta>0 then
								if tieneOferta then								
									cantidadOferta= (((rsAcciones("cantAprobada") * porcentajeOferta)/100))  /NComercialOferta
								end if									
							end if
							
							if NComercialCierre>0 then
								if tieneCierre then							
									cantidadCierre= (((rsAcciones("cantAprobada") * porcentajeCierre)/100))  /NComercialCierre
								end if	
							end if		
'							response.Write(cantidadVisita & " | " & cantidadOferta & " | " & cantidadCierre)
'							response.End()
							totalVisita=totalVisita + cantidadVisita
							totalOferta=totalOferta + cantidadOferta
							totalCierre=totalCierre + cantidadCierre
							TotalComercial=TotalComercial + cantidadVisita + cantidadOferta + cantidadCierre											
								%>
			                  	<tr style="background-color:<%if (contDes mod 2 <> 0) then%>#FFF;<%else%>#F6F6F6;<%end if%>">
    	    	            	  	<td align="left" valign="top"  style=" "  ><%=ciclo%></td>
        		        	      	<td align="right" valign="top"  style=""  ><%=formatnumber(rsAcciones("cantAprobada"),0) & " "%></td>
            			          	<td align="right" valign="top"  style="<%=colorSelVisita%> "  ><%=NComercialVisita%></td>
        	        	      		<td align="right" valign="top"  style=" "  ><%=formatnumber(cantidadVisita,0) & " "%></td>                                               
            		    	      	<td align="right" valign="top"  style="<%=colorSelOferta%> "  ><%=NComercialOferta%></td>
        	        	      		<td align="right" valign="top"  style=" "  ><%=formatnumber(cantidadOferta,0) & " "%></td>                                               
            		        	  	<td align="right" valign="top"  style="<%=colorSelCierre%> "  ><%=NComercialCierre%></td>                                                        
        	        	      		<td align="right" valign="top"  style=" "  ><%=formatnumber(cantidadCierre,0) & " "%></td>                                                                        
		                    	</tr>                                            
	                        	<%
								contDes=1=contDes + 1
end if							
						end if
						
						rsAcciones.movenext
					wend
					
					rsAcciones.cerrar()
					Set rsAcciones = nothing 
					   		
						%>
	                  	<tr style="background-color:#F1F1F1; border-top: solid #ff6600 2px; ">
   		        	      	<td colspan="4" align="right" valign="top"  style=" font-size:12px;" >Total Visita:&nbsp;<strong><%=formatnumber(totalVisita,0) & " "%></strong></td>
   	        	      		<td colspan="2" align="right" valign="top"   style=" font-size:12px;"  >Total Oferta:&nbsp;<strong><%=formatnumber(totalOferta,0) & " "%></strong></td>                                               
   	        	      		<td colspan="2" align="right" valign="top"   style=" font-size:12px;"  >Total Cierre:&nbsp;<strong><%=formatnumber(totalCierre,0) & " "%></strong></td>                                               
                    	</tr>                                            
	                  	<tr style="background-color:#F1F1F1; border-top:solid 2px #C0C0C0;">
   	        	      		<td colspan="8" align="right" valign="top"   style=" font-size:14px;"  >Total Comercial:&nbsp;<strong><%=formatnumber(TotalComercial,0) & " "%></strong></td>                                               
                    	</tr>                                                                    
  					</table><%
					rsAnalistas.movenext
				wend
				rsAnalistas.cerrar()
				Set rsAnalistas = nothing                
				%>
                
				                              
                

			</div>
            </form>
<%
'if trim(territorios)<>"" then
	rsTerritorios.cerrar()
	Set rsTerritorios = nothing
'end if	

if numAreas>0 then
	rsAreaNegocio.cerrar()
	Set rsAreaNegocio = nothing
end if	
	
%>            

<script type="text/javascript">
	//pantallaCargada();
</script>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->





