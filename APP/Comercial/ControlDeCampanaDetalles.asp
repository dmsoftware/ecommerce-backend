<%'El nombre del fichero de idiomas es, por ejemplo, Idiomas_Comercial.xml, en la variable ponemos s�lo el men� al que pertenece la p�gina
'S�lo usamos esta variable si la p�gina no est� enganchada en ning�n men� (app_menus)
nomFicheroIdiomas="Comercial"%>
<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%
Sub FormatoEuros(valor)
    IF not isnull(valor) then response.Write FormatNumber(valor, 2) & " �"
end sub

'Arrays para las estad�sticas
'arrLlamadores(1)=nombre
'arrLlamadores(2)=visitas
'arrLlamadores(3)=propuestas
'arrLlamadores(4)=aprobadas
'arrLlamadores(5)=comentarios visitas sin propuesta
'arrLlamadores(6)=visitas acabadas
dim arrLlamadores(200,6)
'arrComerciales(1)=nombre
'arrComerciales(2)=visitas
'arrComerciales(3)=propuestas
'arrComerciales(4)=aprobadas
'arrComerciales(5)=visitas acabadas
dim arrComerciales(200,5)
'Relleno los nombres de los llamadores
sql="select * from analistas where ana_email<>''"
sql=sql & " order by ana_codigo" 
Set rstPers= connCRM.AbrirRecordset(sql,0,1)
while not rstPers.eof
    arrLlamadores(rstPers("ana_codigo"),0)=rstPers("ana_nombre")
    arrComerciales(rstPers("ana_codigo"),0)=rstPers("ana_nombre")
    rstPers.movenext()
wend
rstPers.cerrar
%>

<!-- CABECERA DE LA SELECCION -->
<h1><%=objIdioma.getIdioma("ControlDeCampanas_Texto22")%> <%=" " & request("nom")%></h1>
<p><%=objIdioma.getIdioma("ControlDeCampanas_Texto23")%></p>

<br>

			  <TABLE cellSpacing="2" cellPadding="2" width="100%" border="0" ID="Table2">

					<td height="20" class="titularT">#</td>
		            <td class="titularT" height="25"> <%=objIdioma.getIdioma("ControlDeCampanas_Texto24")%><br /><i><%=objIdioma.getIdioma("ControlDeCampanas_Texto25")%></i></td>
		            <td height="25" colspan=2 align =center class="titularT"><%=objIdioma.getIdioma("ControlDeCampanas_Texto26")%></td>
		            <td height="25" colspan=3 align =center class="titularT"><%=objIdioma.getIdioma("ControlDeCampanas_Texto27")%></td>
		            <td height="25" colspan=6 align =center class="titularT"><%=objIdioma.getIdioma("ControlDeCampanas_Texto28")%></td>
		        
		        </tr> 


<%'Seleccionamos los registros VISITA, PROPUESTA Y SEGUIMIENTO PROPUESTA
sql="select distinct(CAST(acci_sucliente as varchar) + acci_proyecto),acci_sucliente,clie_nombre,acci_proyecto from acciones_cliente inner join clientes on acciones_cliente.acci_sucliente=clientes.clie_codigo"
sql=sql & " where  acci_sucampana=" & request("cod")
sql=sql & " and (acci_sutipo=4 or acci_sutipo=6 or acci_sutipo=7)"
sql=sql & " order by clie_nombre" 
Set rstCamp= connCRM.AbrirRecordset(sql,0,1)

cont=0
while not rstCamp.eof
     cont=cont+1%>
      
     <tr align="left" class="celdaBlanca">


	   	<td VALIGN="TOP" id="celda1" class="titular"><%=cont%></td>
	  	<td VALIGN="TOP" id="celda1" class="celdas">
	  		<strong><%=rstCamp("clie_nombre") %><br /></strong>
            <i>[<%=rstCamp("acci_proyecto") %>]</i>
	  	</td>	  	

	    <%'Tengo todas las acciones del cliente para esa campa�a y proyecto
	    sql="select * "
		sql=sql & " ,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then tipos_aux_propuesta_finalizacion.TAPF_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TAPF_DESCRIPCION_IDI "		
		sql=sql & " ,(case when LTRIM(RTRIM(CAST(D2.IDID_DESCRIPCION AS varchar(max))))='' Or D2.IDID_DESCRIPCION is null then tipos_aux_propuesta_estado_cliente.TAPEC_DESCRIPCION else CAST(D2.IDID_DESCRIPCION AS varchar(max)) end) as TAPEC_DESCRIPCION_IDI "
		sql=sql & " from (((((acciones_cliente "
		sql=sql & " inner join analistas on acciones_cliente.acci_sucomercial=analistas.ana_codigo) "
		sql=sql & " left join tipos_aux_propuesta_finalizacion on acciones_cliente.acci_tipo7finalizacion=tipos_aux_propuesta_finalizacion.tapf_codigo) "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (tipos_aux_propuesta_finalizacion.TAPF_CODIGO=D.IDID_INDICE And upper(D.IDID_TABLA)='TIPOS_AUX_PROPUESTA_FINALIZACION' And upper(D.IDID_CAMPO)='TAPF_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "')) "				
		sql=sql & " left join tipos_aux_propuesta_estado_cliente on acciones_cliente.acci_tipo6estadocliente=tipos_aux_propuesta_estado_cliente.tapec_codigo) "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D2 on (tipos_aux_propuesta_estado_cliente.TAPEC_CODIGO=D2.IDID_INDICE And upper(D2.IDID_TABLA)='TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE' And upper(D2.IDID_CAMPO)='TAPEC_DESCRIPCION' And D2.IDID_SUIDIOMA='" & Idioma & "')) "		
		
		sql=sql & " where  acci_sucliente=" & rstCamp("acci_sucliente")
		sql=sql & " and  acci_sucampana=" & request("cod")
        sql=sql & " and  acci_proyecto='" & rstCamp("acci_proyecto") & "'"
		Set rstAcci= connCRM.AbrirRecordset(sql,3,3)%>

		<%'Llamada
		rstAcci.filter="acci_sutipo=1"
		if not rstAcci.eof then%>
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%=rstAcci("acci_fecha")%>
	  		</td>	
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%=rstAcci("ana_nombre")%>
	  		</td>
	  	<%else%>
	  		<td align="center" class="celdas">&nbsp;</td><td align="center" class="celdas">&nbsp;</td>
	  	<%end if%>
	  	<%'Hallamos el codigo del llamador
	  	if not rstAcci.eof then
	  	   codLlamador=rstAcci("ana_codigo")
	  	else
	  	   codLlamador=null
	  	end if
	  	%>
		
<%'Visita
		rstAcci.filter="acci_sutipo=4"	
		if not rstAcci.eof then%>
	   		<td align="center" VALIGN="TOP" class="celdas" id="celda1">
	  		<%=rstAcci("acci_fecha")%>
	  		</td>	
	   		<td align="center" VALIGN="TOP" class="celdas" id="celda1">
	  		<%=rstAcci("ana_nombre")%>
	  		</td>
	   		<td align="center" VALIGN="TOP" class="celdas" id="celda1">
  			  <%if rstAcci("acci_terminada") then
	  			      response.Write objIdioma.getIdioma("ControlDeCampanas_Texto29")
	  			      'Contador llamador
	  			      if not isnull(codllamador) then
	  					arrLlamadores(codLlamador,5)=arrLlamadores(codLlamador,5)+1
	  			      end if
	  			      arrComerciales(rstAcci("acci_sucomercial"),4)=arrComerciales(rstAcci("acci_sucomercial"),4)+1
	  			  else
	  			      response.write objIdioma.getIdioma("ControlDeCampanas_Texto30")
	  			  end if%>
	  			  &nbsp;
	  		</td>	  		
	  	<%else%>
	  		<td align="center" class="celdas">&nbsp;</td><td align="center" class="celdas">&nbsp;</td><td align="center" class="celdas">&nbsp;</td>	  		
        <%end if

	  	'A�ado el contador del llamador  	
	  	if not rstAcci.eof then
	  		if not isnull(codLlamador) then
	  			arrLlamadores(codLlamador,1)=arrLlamadores(codLlamador,1)+1
	  		end if
	  	end if
		'A�ado el contador del comercial 	
	  	if not rstAcci.eof then
	  		if not isnull(rstAcci("acci_sucomercial")) then
	  			arrComerciales(rstAcci("acci_sucomercial"),1)=arrComerciales(rstAcci("acci_sucomercial"),1)+1
	  		end if
	  	end if
	  	'A�ado los comentarios de esta visita si es que NO tiene una propuesta asociada
	    sql="select * from acciones_cliente "
		sql=sql & " where  acci_sucliente=" & rstCamp("acci_sucliente")
		sql=sql & " and  acci_sucampana=" & request("cod")
		sql=sql & " and  acci_sutipo=6"
		Set rstComentarios= connCRM.AbrirRecordset(sql,3,3)
		if rstComentarios.eof then
		   if not rstAcci.eof then
		    if not isnull(codLlamador) then
		        'contNoPropuesta=contNoPropuesta + 1
				arrLlamadores(codLlamador,4)=arrLlamadores(codLlamador,4) & cont & " <b>" & rstCamp("clie_nombre") & "</b> (" & arrComerciales(rstAcci("acci_sucomercial"),0) & ").- " & rstAcci("acci_comentario") & "<br><br>"
		    end if
		   end if
		end if
        rstComentarios.cerrar
	  	%>	  			  	

		<%'Propuesta
		rstAcci.filter="acci_sutipo=6"
		if not rstAcci.eof then%>
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%=rstAcci("acci_fecha")%>
	  		</td>	
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%=rstAcci("ana_nombre")%>
	  		</td>
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%FormatoEuros(rstAcci("acci_tipo6cantidad"))%>
	  		</td>			  		  	
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%=rstAcci("TAPEC_DESCRIPCION_IDI")%>&nbsp;
	  		</td>	
	  	<%else%>
	  		<td align="center" class="celdas2">&nbsp;</td>
	  		<td align="center" class="celdas2">&nbsp;</td>
	  		<td align="center" class="celdas2">&nbsp;</td>
	  		<td align="center" class="celdas2">&nbsp;</td>	  		
<%end if	
	  	'A�ado el contador del llamador
	  	if not rstAcci.eof then
	  		if not isnull(codLlamador) then
	  			arrLlamadores(codLlamador,2)=arrLlamadores(codLlamador,2)+1
	  		end if
	  	end if
		'A�ado el contador del comercial 	
	  	if not rstAcci.eof then
	  		if not isnull(rstAcci("acci_sucomercial")) then
	  			arrComerciales(rstAcci("acci_sucomercial"),2)=arrComerciales(rstAcci("acci_sucomercial"),2)+1
	  		end if
	  	end if	  	
	  	%>	  	  	
	  		  	
		<%'Seguimiento Propuesta
		rstAcci.filter="acci_sutipo=7"
		if not rstAcci.eof then%>  		  	
	   		<td align="center" VALIGN="TOP" class="celdas2" id="celda1">
	  		<%=rstAcci("TAPF_DESCRIPCION_IDI")%>&nbsp;
	  		</td>	
	  	<%else%>
	  		<td align="center" class="celdas2">&nbsp;</td>	  		
	  	<%end if	
	  	'A�ado el contador del llamador
	  	if not rstAcci.eof then
	  		if not isnull(codLlamador) then
	  		    if rstAcci("tapf_codigo")=1 or rstAcci("tapf_codigo")=3 then
	  			   arrLlamadores(codLlamador,3)=arrLlamadores(codLlamador,3)+1
	  			end if
	  		end if
	  	end if
		'A�ado el contador del comercial 	
	  	if not rstAcci.eof then
	  		if not isnull(rstAcci("acci_sucomercial")) then
	  			if rstAcci("tapf_codigo")=1 or rstAcci("tapf_codigo")=3 then
	  				arrComerciales(rstAcci("acci_sucomercial"),3)=arrComerciales(rstAcci("acci_sucomercial"),3)+1
	  			end if
	  		end if
	  	end if
        
        rstAcci.cerrar	  	
	  	%>		  	
	</tr>     
	
    <%
    
    rstCamp.movenext
wend
rstCamp.cerrar
set rstCamp=nothing%>



	</table>
	

<br><br>


<%'ESTADISTICAS%>

<table cellpadding=1 cellspacing=1 border=0>
    <tr>
       <td colspan=7 align=center class="titularT"><%=objIdioma.getIdioma("ControlDeCampanas_Texto31")%></td>
    </tr>
	<tr class="celdas">
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto32")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto33")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto34")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto35")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto36")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto37")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto38")%><br><%=objIdioma.getIdioma("ControlDeCampanas_Texto39")%></td>
	</tr>
<%
for intI=1 to ubound(arrLlamadores)
	'Hallamos las llamadas realizadas por el llamador
	sql="select distinct(acci_sucliente) from acciones_cliente "
	sql=sql & " where  acci_sucomercial=" & intI
	sql=sql & " and  acci_sucampana=" & request("cod")
	sql=sql & " and  acci_sutipo=1 and acci_terminada=1"
	Set rstLlama= connCRM.AbrirRecordset(sql,3,3)
	if rstLlama.recordcount>0 then%>
		<tr class="celdaBlanca">
		<td align=righ ><%=arrLlamadores(intI,0)%>&nbsp;</td>
		<td align=right><%=rstLlama.recordcount%>&nbsp;</td>
		<td align=right><%=arrLlamadores(intI,1) & "(" & arrLlamadores(intI,5) &")"%>&nbsp;</td>
		<td align=right><%=arrLlamadores(intI,2)%>&nbsp;</td>
		<td align=right><%=arrLlamadores(intI,3)%>&nbsp;</td>
		<td align=right><a href="ControlDeCampanaDetalles.asp?cod=<%=request("cod")%>&nom=<%=request("nom")%>&Com=<%=intI%>"><font color=000000><img src="/dmcrm/app/imagenes/ampliar.png" width=20 border=0/>Ver</font></a></td>
		<td class="celdas" align=right>
		<%'Si existe alguna visita
		if arrLlamadores(intI,5)<>0 then%>
			<%=round(arrLlamadores(intI,2)*100/arrLlamadores(intI,5),0)%> %
		<%else%>
			&nbsp;
		<%end if%>
		</td>
		</tr>
  <%end if
  rstLlama.cerrar
next
%>
</table>

<br><br>

<table cellpadding=2 cellspacing=1 border=0 ID="Table3">
    <tr>
       <td colspan=7 align=center class="titularT"><%=objIdioma.getIdioma("ControlDeCampanas_Texto40")%></td>
    </tr>
	<tr class="celdas">
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto32")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto34")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto35")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto36")%></td>
         <td><%=objIdioma.getIdioma("ControlDeCampanas_Texto38")%><br><%=objIdioma.getIdioma("ControlDeCampanas_Texto39")%></td>         
	</tr>
<%
for intI=1 to ubound(arrComerciales)
	if arrComerciales(intI,1)<>"" or arrComerciales(intI,2)<>"" or arrComerciales(intI,3)<>"" then%>
		<tr class="celdaBlanca">
		<td><%=arrComerciales(intI,0)%>&nbsp;</td>
		<td align="right"><%=arrComerciales(intI,1) & "(" & arrComerciales(intI,4) &")"%>&nbsp;</td>
		<td align="right"><%=arrComerciales(intI,2)%>&nbsp;</td>
		<td align="right"><%=arrComerciales(intI,3)%>&nbsp;</td>
		<td align="right">
		<%'Si existe alguna visita
		if arrComerciales(intI,4)<>0 then%>
			<%=round(arrComerciales(intI,2)*100/arrComerciales(intI,4),0)%> %
		<%else%>
			&nbsp;
		<%end if%>		
		</tr>
  <%end if
next
%>
</table>

<p>
  <%'Comentarios
if request("Com")<>"" then
	%>
	<br>
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span><%=objIdioma.getIdioma("ControlDeCampanas_Texto41")%></span></p>
<br />
<table width="931" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="923" height="23" class="celdas" ><span><%=ucase(arrLlamadores(request("Com"),0))%></span></td>
  </tr>
  <tr class="celdas2">
    <td height="51" valign="middle" bgcolor="#FFFFFF" ><%
	response.Write arrLlamadores(request("Com"),4)
end if
%>
    <%'############################################################################################%>
    <%'############################################################################################%></td>
  </tr>
</table>
<p><br>
  <!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->
</p>
