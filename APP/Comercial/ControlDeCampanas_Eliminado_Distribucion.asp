
<%'***********************************************************************************************%>
<%'***********************************************************************************************%>
<%'****************************** DISTRIBUCI�N             ***************************************%>
<%'***********************************************************************************************%>
<%'***********************************************************************************************%>
<%' SELECCIONAMOS LOS DISTRIBUIDORES QUE HAN TENIDO ALGUNA PROPUESTA EL A�O EN CURSO. LA FECHA V�LIDA ES LA DE LAS ACCIONES DE TIPO6.
sql="select * from CLIENTES INNER JOIN ACCIONES_CLIENTE ON ACCIONES_CLIENTE.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO"
sql=sql & " where ACCI_SUTIPO=6"
if request("an")<>"" and request("an")<>0 then
   sql=sql & " and  year(ACCI_FECHA)=" & request("an")
end if
sql=sql & " AND CLIE_DISTRIBUIDOR=TRUE"
sql=sql & " AND (NOT ISNULL(ACCI_PROYECTO) OR ACCI_PROYECTO<>'')"
sql=sql & " order by CLIE_NOMBRE" 
Set rstCamp= connCRM.AbrirRecordset(sql,3,3)

while not rstCamp.eof
   
  'eliminamos los registros de clientes ya tratados
  if rstCamp("clie_codigo")<>clienteActual then
    
   clienteActual=rstCamp("clie_codigo")
   
   'Inicializamos contadores generales
   TotalCantAprob=0
   TotalNumAprob=0
   TotalCant=0
   TotalNum=0
   
    %>
      
        <tr align="left" bgColor="#6A7C8D">
	   	
	   	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;">
	  		<font face="verdana" size="1" color="ffffff">
	  	    DISTRI.
	  	</font>&nbsp;
	  	</td>	
	  	
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;">
	  		<font face="verdana" size="1" color="ffffff">
	  	    <b><%=ucase(rstCamp("CLIE_nombre"))%></b><br>
	  	    <a href="Campa�aDistriDetalles.asp?codD=<%=rstCamp("CLIE_codigo")%>&nom=<%=rstCamp("CLIE_nombre")%>&an=<%=request("an")%>">DETALLES</a>
	  	</font>&nbsp;
	  	</td>

	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;"  align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos NUM REGISTROS, BASADOS EN PROPUESTAS					     
				sql="select count(*) as numReg from acciones_cliente where acci_suCLIENTE= " & rstCamp("CLIE_codigo")
				sql = sql & " and acci_sutipo=6"
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if				
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				response.Write rstEst("numReg")
				rstEst.cerrar
				%>
	  	</font>&nbsp;
	  	</td>
					  
	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;"  align=right>
			&nbsp;
	  	</td>					  

	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;"  align=right>
			&nbsp;
	  	</td>


        <%'PROPUESTAS GRADO 1: NUM, cantidad presupuestada, num aprobadas, cantidad aprobada%>
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right >
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos los datos estad�sticos de cada campa�a					     
				sql="select count(*)as numReg,sum(acci_tipo6cantidad) as total from acciones_cliente where acci_sucliente= " & rstCamp("clie_codigo")
				sql = sql & " and acci_sutipo=6 and acci_tipo6Estadocliente=1"
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if					
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				response.Write rstEst("numReg")
				TotalNum=TotalNum+rstEst("numReg")				
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                formatoEuros(rstEst("total"))
                if not isnull(rstEst("total")) then  TotalCant=TotalCant+rstEst("total")
				rstEst.cerrar
				%>	  				 
	  	</font>&nbsp;
	  	</td>

	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos LOS PRESUPUESTOS APROBADOS Y SU CANTIDAD					     
				sql="select * from acciones_cliente where acci_sucliente= " & rstCamp("clie_codigo")
				sql = sql & " and acci_sutipo=7 and (acci_tipo7finalizacion = 1 or acci_tipo7finalizacion=3)" 'Aprobada o preaprobada
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if					
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				AuxReg=0
				AuxTotal=0
				While not rstEst.eof
				   	sql="select * from acciones_cliente where acci_proyecto='" & rstEst("acci_proyecto") & "'"
				    sql = sql & " and acci_sutipo=6 and acci_tipo6EstadoCliente=1 " 'Grado 1
				    sql = sql & " and acci_sucliente=" & rstEst("acci_sucliente")
					if request("an")<>"" and request("an")<>0 then
						sql=sql & " AND year(ACCI_FECHA)=" & request("an")
					end if					    
				    Set rstAux= connCRM.AbrirRecordset(sql,3,3) 
				    if not rstAux.eof then
				       AuxReg=AuxReg+1
				       AuxTotal=AuxTotal+rstEst("acci_tipo6Cantidad")-rstEst("acci_tipo7Gastos")
				    end if
				    rstAux.cerrar
				    rstEst.movenext
				wend
				response.Write AuxReg
				TotalNumAprob=TotalNumAprob+AuxReg				
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                if not isnull(AuxTotal) then TotalCantAprob=TotalCantAprob+AuxTotal                
                formatoEuros(AuxTotal) 
				rstEst.cerrar
				%>	   
	  	</font>&nbsp;
	  	</td>

        <%'PROPUESTAS GRADO 2: NUM, cantidad presupuestada, num aprobadas, cantidad aprobada%>
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos los datos estad�sticos de cada campa�a					     
				sql="select count(*)as numReg,sum(acci_tipo6cantidad) as total from acciones_cliente where acci_sucliente= " & rstCamp("clie_codigo")
				sql = sql & " and acci_sutipo=6 and acci_tipo6Estadocliente=2"
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if	
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				response.Write rstEst("numReg")
				TotalNum=TotalNum+rstEst("numReg")				
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                formatoEuros(rstEst("total"))
                if not isnull(rstEst("total")) then  TotalCant=TotalCant+rstEst("total")               
				rstEst.cerrar
				%>	   
	  	</font>&nbsp;
	  	</td>

	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos LOS PRESUPUESTOS APROBADOS Y SU CANTIDAD					     
				sql="select * from acciones_cliente where acci_sucliente= " & rstCamp("clie_codigo")
				sql = sql & " and acci_sutipo=7 and (acci_tipo7finalizacion = 1 or acci_tipo7finalizacion=3)" 'Aprobada o preaprobada	
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if								
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				AuxReg=0
				AuxTotal=0
				While not rstEst.eof
				   	sql="select * from acciones_cliente where acci_proyecto='" & rstEst("acci_proyecto") & "'"
				    sql = sql & " and acci_sutipo=6 and acci_tipo6EstadoCliente=2 " 'Grado 2
				    sql = sql & " and acci_sucliente=" & rstEst("acci_sucliente")
					if request("an")<>"" and request("an")<>0 then
						sql=sql & " AND year(ACCI_FECHA)=" & request("an")
					end if					    
				    Set rstAux= connCRM.AbrirRecordset(sql,3,3) 
				    if not rstAux.eof then
				       AuxReg=AuxReg+1
				       AuxTotal=AuxTotal+rstAux("acci_tipo6Cantidad")-rstEst("acci_tipo7Gastos")
				    end if
				    rstAux.cerrar
				    rstEst.movenext
				wend
				response.Write AuxReg
				TotalNumAprob=TotalNumAprob+AuxReg
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                if not isnull(AuxTotal) then TotalCantAprob=TotalCantAprob+AuxTotal                
                formatoEuros(AuxTotal)
				rstEst.cerrar
				%>	   
	  	</font>&nbsp;
	  	</td>
	  	
        <%'PROPUESTAS GRADO 3: NUM, cantidad presupuestada, num aprobadas, cantidad aprobada%>
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos los datos estad�sticos de cada campa�a					     
				sql="select count(*)as numReg,sum(acci_tipo6cantidad) as total from acciones_cliente where acci_sucliente= " & rstCamp("clie_codigo")
				sql = sql & " and acci_sutipo=6 and acci_tipo6Estadocliente=3"
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if					
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				response.Write rstEst("numReg")
				TotalNum=TotalNum+rstEst("numReg")
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                formatoEuros(rstEst("total"))
                if not isnull(rstEst("total")) then TotalCant=TotalCant+rstEst("total")                
				rstEst.cerrar
				%>	   
	  	</font>&nbsp;
	  	</td>

	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%'Hallamos LOS PRESUPUESTOS APROBADOS Y SU CANTIDAD					     
				sql="select * from acciones_cliente where acci_sucliente= " & rstCamp("clie_codigo")
				sql = sql & " and acci_sutipo=7 and (acci_tipo7finalizacion = 1 or acci_tipo7finalizacion=3)" 'Aprobada o preaprobada	
				if request("an")<>"" and request("an")<>0 then
					sql=sql & " AND year(ACCI_FECHA)=" & request("an")
				end if								
				Set rstEst= connCRM.AbrirRecordset(sql,3,3)
				AuxReg=0
				AuxTotal=0
				While not rstEst.eof
				   	sql="select * from acciones_cliente where acci_proyecto='" & rstEst("acci_proyecto") & "'"
				    sql = sql & " and acci_sutipo=6 and acci_tipo6EstadoCliente=3 " 'Grado 3
				    sql = sql & " and acci_sucliente=" & rstEst("acci_sucliente")
					if request("an")<>"" and request("an")<>0 then
						sql=sql & " AND year(ACCI_FECHA)=" & request("an")
					end if					    
				    Set rstAux= connCRM.AbrirRecordset(sql,3,3) 
				    if not rstAux.eof then
				       AuxReg=AuxReg+1
				       AuxTotal=AuxTotal+rstAux("acci_tipo6Cantidad")-rstEst("acci_tipo7Gastos")
				    end if
				    rstAux.cerrar
				    rstEst.movenext
				wend
				TotalNumAprob=TotalNumAprob+AuxReg
				response.Write AuxReg
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                if not isnull(AuxTotal) then TotalCantAprob=TotalCantAprob+AuxTotal
                formatoEuros(AuxTotal)
				rstEst.cerrar
				%>	   
	  	</font>&nbsp;
	  	</td>	  	

        <%'PROPUESTAS total: NUM, cantidad presupuestada, num aprobadas, cantidad aprobada%>
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%
				response.Write TotalNum
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#6a7c8d;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                formatoEuros(TotalCant)
                if isnull(TotalCant) then TotalCant=0
                contPropuestasDIS=contPropuestasDIS+TotalCant
				%>	   
	  	</font>&nbsp;
	  	</td>

	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff">
				<%
				response.Write TotalNumAprob
				%>	  	    
	  	</font>&nbsp;
	  	</td>
			
	  	<td VALIGN="TOP" id="celda1" style="background-color:#7A8B9C;" align=right>
	  		<font face="verdana" size="1" color="ffffff"> 
                <%
                formatoEuros(TotalCantAprob)
                if isnull(TotalCantAprob) then TotalCantAprob=0
                contAprobadasDIS=contAprobadasDIS+TotalCantAprob
				%>	   
	  	</font>&nbsp;
	  	</td>
	  		  	
	</tr>     
	
    <%
   end if
   rstCamp.movenext
wend%>

<%'***********************************************************************************************%>
<%'***********************************************************************************************%>
<%'****************************** FIN DISTRIBUCI�N         ***************************************%>
<%'***********************************************************************************************%>
<%'***********************************************************************************************%>

