<% 'Response.Buffer=false 
'response.Charset="ISO-8859-1"
'Response.AddHeader "Content-Type", "text/xml; charset=utf-8"%>
<!-- include virtual="/dmcrm/includes/permisos.asp"-->

<%'El nombre del fichero de idiomas es, por ejemplo, Idiomas_Comercial.xml, en la variable ponemos s�lo el men� al que pertenece la p�gina
'S�lo usamos esta variable si la p�gina no est� enganchada en ning�n men� (app_menus)
nomFicheroIdiomas="Comercial"%>
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!--#include virtual="/dmcrm/APP/comun2/Validaciones.asp"  -->
<html>
	<head>
		<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
		<link rel="stylesheet" href="/dmcrm/css/estilos.css">
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">
  
    <!-- #include virtual="/dmcrm/includes/jquery.asp"-->  
        <script type="text/javascript" src="/dmcrm/js/jtables/js/jquery.jeditable.mini.js"></script>    
    <script type="text/javascript" src="/dmcrm/APP/Comun2/FormChek2.js"></script>            
    <!--Includes Pesta�as -->
	 <script type="text/javascript">

        
        function paginaCargada()
        {
    //		document.getElementById("divCargando").style.display="none";
        }
        
		function quitarScroolPadre()
		{
			window.parent.document.body.style.overflow="hidden";
			redimensionarAltosPantalla();

		}
		
		function redimensionarAltosPantalla()
		{
//			alert(document.getElementById("frmRelacionados").style.top);

			//var altoDoc=document.documentElement.clientHeight;
			var altoDoc=document.body.clientHeight;			
			if (altoDoc!="")
			{
				if (document.getElementById("idTab_container")!=null)
				{
					var altoDivPorcen=document.getElementById("idTab_container").style.height;
					altoDivPorcen=altoDivPorcen.replace("%","");			

					var altoDivPX=((altoDivPorcen * altoDoc )/ 100);
					document.getElementById("idTab_container").style.height=altoDivPX + "px";
				}

				if (document.getElementById("frmRelacionados")!=null)
				{
					var altoFramePorcen=document.getElementById("frmRelacionados").style.height;
					altoFramePorcen=altoFramePorcen.replace("%","");

					var altoFramePX=((altoFramePorcen * altoDoc )/ 100);
					document.getElementById("frmRelacionados").style.height=altoFramePX + "px";
				}
				
			}
		}		
		
		function borrarContacto(idReg)
		{
			if (confirm("�Esta seguro que desea borrar el contacto? Esta operaci�n no se puede deshacer.")) {										
				$.ajax({
					type: "POST",
					url: "/dmcrm/APP/Comercial/ContactosDuplicados/contactosDuplicadosAjaxBorrar.asp",
					data: "idReg=" + idReg,					
					success: function(datos) {
   						 oTable.fnDraw();			
						}
					});
			}
		}
		
     </script>
     <% 
	 
		tablaOrig=request("tabla")
		codigoOrig=request("codigo")

		if trim(lcase(tablaOrig))="socios" then
			tabla="corregirSocios"
		else
			tabla="corregircontactos"
		end if


		codCliente="0"		
		if trim(lcase(tablaOrig))<>"socios" then
			if trim(lcase(tablaOrig))="contactos"  then
				sqlCodigoSuCliente=" Select CON_SUCLIENTE From Contactos Where CON_CODIGO=" & codigoOrig
				Set rsCodigoSuCliente = connCRM.AbrirRecordset(sqlCodigoSuCliente,3,1)
				if not rsCodigoSuCliente.eof then 
					codCliente = rsCodigoSuCliente("CON_SUCLIENTE") 
				end if
				rsCodigoSuCliente.cerrar()
				Set rsCodigoSuCliente = nothing
			else
				codCliente=codigoOrig	
			end if
		

			codigo=codCliente
		
			filtro = "CON_SUCLIENTE"
			valorFiltro = codCliente
		end if
				
'	    estadoColumnas="null,null;"
'		cabeceraColumnas="CON_CODIGO,CON_NOMBRE;"
		
	 	numRegistrosTabla=10	
		paginaTabla=0
		busquedaTabla=""
		campoOrdenacionDefecto=0
		direccionOrdenacion="desc"				
		
''''''''''''''''''''''''''''''Funciones de seleccion
	numero1N = 0
	numeroImagenes = 0
	tituloTabla = ""
	numFormularios = 0


	query = "SELECT clave, (CASE WHEN tipo='' Or tipo is null THEN 'G' ELSE tipo END) as Tipo, visibleSel, (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & "='' THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario, claveExt,tablaExt,CampoExt0 as CampoExt, CampoExt1, ordenacion, campo,OcultarOpciones,PaginaAmedidaAlta FROM maestra WHERE tabla in ('" & tabla & "','" & tabla & "G') ORDER BY orden"
'		response.Write(query)
'		response.End()
	Set rsCamposMaestra = connCRM.AbrirRecordset(query,3,1)
	opciones=""
	aMedidaAlta=""
	'***** OBTENEMOS EL TITULO *****
	rsCamposMaestra.filter = "tipo = 'G'"
	if not rsCamposMaestra.eof then 
		tituloTabla = rsCamposMaestra("nomUsuario") 
		opciones=rsCamposMaestra("OcultarOpciones")
		aMedidaAlta=rsCamposMaestra("PaginaAmedidaAlta")
	end if

	'***** OBTENEMOS LOS CAMPOS CLAVE *****
	rsCamposMaestra.filter = "clave = -1"
	Redim camposClave(3, rsCamposMaestra.recordCount() - 1)
	for i = 0 to rsCamposMaestra.recordCount() - 1
		camposClave(0,i) = rsCamposMaestra("nomUsuario")
		camposClave(1,i) = rsCamposMaestra("visibleSel")
		camposClave(2,i) = rsCamposMaestra("campo")	
		rsCamposMaestra.movenext
	next
	
	
	'***** CALCULAMOS EL N�MERO DE CAMPOS FICHERO *****
	rsCamposMaestra.filter = "tipo = 'X'"
	numeroImagenes = rsCamposMaestra.recordCount()
	
	'***** CALCULAMOS EL N�MERO DE RELACIONES 1<->N *****
	query = "SELECT count(tabla) FROM maestra WHERE tablaExt = '" & tabla & "'"
	Set rsRelaciones1N = connCRM.abrirRecordSet(query,3,1)
	numero1N = rsRelaciones1N(0)
	rsRelaciones1N.cerrar()
	Set rsRelaciones1N = nothing
	
	'***** OBTENEMOS EL N�MERO DE RELACIONES M<->N *****
	numeroMN = NumeroDeMNS(tabla)		
	
	'***** OBTENER LA ORDENACION POR DEFECTO *****
	rsCamposMaestra.filter = "clave = -1 OR visibleSel = -1"
	for i = 0 to rsCamposMaestra.recordCount() - 1 
		if rsCamposMaestra("ordenacion") then
			campoOrdenacionDefecto = i - 1
			i = rsCamposMaestra.recordCount()
		end if
		rsCamposMaestra.movenext
	next		
	if isnumeric(campoOrdenacionDefecto) then
		if cint(campoOrdenacionDefecto)<=0 then
			campoOrdenacionDefecto=0
		end if
	end if
	'***** OBTENEMOS LOS CAMPOS GENERALES *****
	rsCamposMaestra.filter = "clave = 0 AND tipo <> 'G'" 
	Redim camposGenerales(3, rsCamposMaestra.recordCount()- 1)
	for i = 0 to rsCamposMaestra.recordCount() - 1
		camposGenerales(0,i) = rsCamposMaestra("nomUsuario")
		camposGenerales(1,i) = rsCamposMaestra("visibleSel")
'			response.Write(rsCamposMaestra("CampoExt"))
		if rsCamposMaestra("claveExt") And rsCamposMaestra("visibleSel") then
			alias=rsCamposMaestra("CampoExt")
			if instr(rsCamposMaestra("CampoExt"),"&")>0 then
				arrAlias=split(rsCamposMaestra("CampoExt")," ")
				if instr(arrAlias(0),"&")>0 then
					arrAlias2=split(arrAlias(0)," ")				
					alias=arrAlias2(0)
				else
					alias=arrAlias(0)	
				end if
				
			end if
			
			if not (instr(alias,rsCamposMaestra("tablaExt") & ".")>0) then
				alias=rsCamposMaestra("tablaExt") & "." & alias
			end if
			camposGenerales(2,i) = alias 
			tablaExtAux=""
			campoExtAux=""
			claveExternaAux=""
			FiltroExtAux=""
			ValorFiltroExtAux=""

			if ClaveExt(Tabla,rsCamposMaestra("campo"),tablaExtAux,campoExtAux,claveExternaAux,FiltroExtAux,ValorFiltroExtAux) then
					fromAnterior=fromAnterior&"("
				
					fromPosterior=fromPosterior&" left Join " & tablaExtAux & " on ("
					if instr(rsCamposMaestra("campo"),tabla & ".")>0 then
						fromPosterior=fromPosterior & rsCamposMaestra("campo") & "="
					else
						fromPosterior=fromPosterior & " " & tabla & "." & rsCamposMaestra("campo") & "="				
					end if			
				
					if instr(claveExternaAux,tablaExtAux & ".")>0 then
						fromPosterior=fromPosterior & claveExternaAux
					else
						fromPosterior=fromPosterior & " " & tablaExtAux & "." & claveExternaAux			
					end if							
					fromPosterior=fromPosterior	& ")) "			
			end if	
		else
			camposGenerales(2,i) = rsCamposMaestra("campo")		
		end if
		
		camposGenerales(3,i) = rsCamposMaestra("tipo")		
		rsCamposMaestra.movenext
	next		
	
	'***** OBTENEMOS EL N�MERO DE BOTONES RELACIONADOS CON FORMULARIOS *****
	select case lcase(tabla)
		case "entidades"
				numFormularios = 1
		case "formularios"
				numFormularios = 2
	end select
	
	rsCamposMaestra.cerrar()
	Set rsCamposMaestra = nothing
	
	'***** CALCULAMOS LAS CARACTER�STICAS DE LAS COLUMNAS PARA EL JTABLE *****
	numColumnas=0	
	Redim numColumnasCheck(0)
	estadoColumnas = ""
	cabeceraColumnas = ""
	for i = 0 to ubound(camposClave,2)
		if camposClave(1,i) then
				estadoColumnas = estadoColumnas & "null,"
		else
			estadoColumnas = estadoColumnas & "{""bVisible"": false},"
		end if
			cabeceraColumnas = cabeceraColumnas & camposClave(2,i)  & ";"
	next
		
	for i = 0 to ubound(camposGenerales,2)
		if trim(ucase(camposGenerales(3,i)))="B" then
			redim preserve numColumnasCheck(ubound(numColumnasCheck)+1)
			numColumnasCheck(ubound(numColumnasCheck))=i
		end if	
		if camposGenerales(1,i) then
			estadoColumnas = estadoColumnas & "null,"
			cabeceraColumnas = cabeceraColumnas & camposGenerales(2,i)  & ";"
		end if
	next
	numColumnas=i
	estadoColumnas = "[" & mid(estadoColumnas,1,len(estadoColumnas)-1) & ",{""sClass"": ""tdOpciones"",""bSearchable"": false,""bSortable"": false}]"
	cabeceraColumnas = mid(cabeceraColumnas,1,len(cabeceraColumnas)-1)
''''''''''''''''''''''''''''''Fin funciones de seleccion		
				

		
	 
	 %>
		<script type="text/javascript">
			/***** Funci�n para generar un id aleatorio para evitar la cache *****/
			function generarGUID()
			{
				var result, i, j;
				result = '';
				for(j=0; j<32; j++)
				{
					if( j == 8 || j == 12 || j == 16 || j == 20)
						result = result + '-';
					i = Math.floor(Math.random()*16).toString(16).toUpperCase();
					result = result + i;
				}
				return result;
			}
			

			
			/** Variables de la tabla **/
			var oTable;
			var gaiSelected =  [];
var posCol;

			$(document).ready(function() {
				/** Objeto tabla **/
				oTable = $('#tablaContenido:first').dataTable({
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bFilter": true,
					"bProcessing": true,
					"bServerSide": true,
					"sAjaxSource": "/dmcrm/APP/Comercial/ContactosDuplicados/contactosDuplicadosAjax.asp",
					"aoColumns": <%=estadoColumnas%>,
					"aaSorting": [[ <%=campoOrdenacionDefecto%>, "<%=direccionOrdenacion%>" ]],
					"iDisplayLength": <%=numRegistrosTabla%>,
					"iDisplayStart":"<%=paginaTabla%>",
					"oSearch": {"sSearch": "<%=busquedaTabla%>"},
					<%if isnumeric(numColumnas) then%>
					"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
							$('td:eq(<%=numColumnas%>)', nRow).attr("class","colSinEdicion");
							<%For contchecks = 0 To ubound(numColumnasCheck)
								if isnumeric(numColumnasCheck(contchecks)) then%>
					               $('td:eq(<%=numColumnasCheck(contchecks)%>)', nRow).attr("class","colSinEdicion");
								 <%end if  
							Next%>
					     return nRow;
					},
					<%end if%>
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						var valorBuscador=$(".cajaBuscador").val().replace("�","|NN|").replace("�","|nn|");
						<%if trim(filtrosBusqueda)<>"" then%>
						var filtro = <%=filtrosBusqueda%>;
						if (filtro != '')
						{
							var arrFiltro = filtro.split("**");
							for (var i=0;i<arrFiltro.length;i++)
							{
								aoData.push(eval('(' + arrFiltro[i] + ')'));
							}
						}					
						<%end if%>
						aoData.push({ "name": "filtro", "value":  "<%=filtro & ";" & filtro2%>" } ); 
						aoData.push({ "name": "valFiltro", "value":  "<%=valorFiltro & ";" & valorFiltro2%>" } ); 
						aoData.push({ "name": "tablaOrig", "value":  "<%=tablaOrig%>" } ); 
						aoData.push({ "name": "codCliente", "value":  "<%=codCliente%>" } ); 						
						aoData.push({ "name": "codigoOrig", "value":  "<%=codigoOrig%>" } ); 												
						aoData.push({ "name": "columnas", "value":  "<%=cabeceraColumnas%>" } );
						aoData.push({ "name": "cabecera", "value":  "" } );
						aoData.push({ "name": "fromAnterior", "value":  "<%=fromAnterior%>" } );						
						aoData.push({ "name": "fromPosterior", "value":  "<%=fromPosterior%>" } );																		
						aoData.push({ "name": "buscadorTabla", "value":  valorBuscador } );																								
						aoData.push({ "name": "nocache", "value":  generarGUID() } ); 
						/*La siguiente llamada, la hace por GET, por lo que pasa todos los parametros por URL. Incluida la SQL, por lo que si es demasiado grande, dependiendo del servidor, puede fallar.
						$.getJSON( sSource, aoData, function (json) { 
							fnCallback(json)
						} );*/
						$.ajax( {
							"dataType": 'json', 
							"type": "POST", 
							"url": sSource, 
							"data": aoData, 
							"success": fnCallback
						} );						
					},
					"fnDrawCallback": function () {
						$('td:not(.colSinEdicion)', oTable.fnGetNodes()).editable('/dmcrm/APP/Comercial/ContactosDuplicados/contactosDuplicadosAjaxGuardar.asp', {
							"callback"  : function( sValue, y ) {if(sValue == '-1'){oTable.fnDraw()}},
							"height"    : "20px",
							"width"     : "100%",
							"margin"    : "0px",							
							"padding"   : "0px",	
//							"font-family": "Arial, Helvetica, sans-serif",
//							"size": "6px",
							"onblur"    : "submit",
							"submitdata": function(value, settings) {
								posCol = (oTable.fnGetPosition( this )[1]) + 1; //Le a�adimos 1, porque el codigo tambien va en la variable columnas
								posFila = (oTable.fnGetPosition( this )[0]); 
								var idReg=$('#idFila_' + posFila).val();
								return {
									"idReg" : idReg,
									"regCampoClave" : "<%=camposClave(2,0)%>",
									"regPosCol" : posCol,
									"columnas" : "<%=cabeceraColumnas%>",
									"success": $.jGrowl("<%=objIdiomaGeneral.getIdioma("GuardadoCorrectamente_Texto1")%>")
            					}; 
								//$.jGrowl("�GUARDADO CORRECTAMENTE!");
							}
						} );
						////////////
						$('td:.colSinEdicion .checksContactos').live('change',function(){
							var valor="0";
							if (this.checked)
							{valor="1";}
							var nomCol=$(this).attr('rel');
							var idRegistro=this.name;
							var nomCampoClave='<%=camposClave(2,0)%>';
							
							$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Comercial/ContactosDuplicados/contactosDuplicadosAjaxGuardar.asp",
								data: "idReg=" + idRegistro + "&regCampoClave=" + nomCampoClave + "&nomColumna=" + nomCol + "&value=" + encodeURIComponent(valor),//+ idReg,					
								success: function(datos) {
   											$.jGrowl("<%=objIdiomaGeneral.getIdioma("GuardadoCorrectamente_Texto1")%>");											 
											oTable.fnDraw();	
											
//											 $.jGrowl("�GUARDADO CORRECTAMENTE!")
											}
								});
						} );						
					},		
					"bSort": true
				});
				
/*				function RecargarEdicionTabla()
				{
					$.jGrowl("�GUARDADO CORRECTAMENTE!"); 
					oTable.fnDraw();
				}*/

				/** Mouseover de los botos de opciones **/
				$('.selBtoMod').live('mouseover',function(){
					$('img', this).attr('src','imagenes/c1.gif');
				}).live('mouseout',function(){
					$('img', this).attr('src','imagenes/c0.gif');
				});
				
				$('.selBtoBor').live('mouseover',function(){
					$('img', this).attr('src','imagenes/b1.gif');
				}).live('mouseout',function(){
					$('img', this).attr('src','imagenes/b0.gif');
				});
				

				$(".cEmergente").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'titleShow'		        : false,					
						'onClosed'		        : function() {
							    oTable.fnDraw();	
						}
					}).trigger("click"); 
					return false; 	

				});
				
			});
			

	
		</script>     
      <%if trim(request("modificado"))="1" then%>  
		 <script type="text/javascript">
    	    $(document).ready(function() {
				$.jGrowl("<%=objIdiomaGeneral.getIdioma("GuardadoCorrectamente_Texto1")%>");
	        });
	     </script>        
     <%end if%>
     <style type="text/css">
body { margin:0; padding:0;}

</style> 

	</head>
	<body class="fondoPagIntranet" onLoad="quitarScroolPadre();"  style=" overflow-y:auto; overflow-x:hidden;"   > <!--onLoad="javascript:paginaCargada();"-->

		<%
		estiloBordeSeparados=""
		if trim(lcase(tablaOrig))<>"socios" then	
				estiloBordeSeparados="margin:0 0 10px 0;  border-top:solid 1px #ff6600;"	 
			    tabla="corregirclientes"
				codigo=codCliente
				filtro=""
				valorFiltro=""
				retorno=server.URLEncode("/dmcrm/APP/Comercial/ContactosDuplicados/contactosDuplicados.asp?tabla=" & tablaOrig & "&CODIGO="&codigoOrig) ' & "&ret=" & retorno
				%>
		<form id="mtoContactosDuplicados" name="mtoContactosDuplicados" runat="server"  method="post"  action="/dmcrm/APP/Comun2/modificaciones.asp?tabla=<%=tabla%>&modo=modificacion&codigo=<%=codigo%>&nomForm=mtoContactosDuplicados&ret=<%=retorno%>"  > 
				<h1 style="width:100%; margin:0; padding:0 0 10px 0;  clear:both; font-size:15px;"><%=objIdioma.getIdioma("contactosDuplicados_Titulo")%></h1>        
                  
                <!-- #include virtual="/dmcrm/APP/Comun2/formMantenimientoCamposEditar.asp"  -->                    
                <br/>                
               	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton" id="B1" name="B1" onClick="ValidaFormularioAlta(mtoContactosDuplicados);" style="float:right; margin:5px 10px 30px 0;"  />                                                
		<%end if%>                
                
   				<h1 style="width:100%;  clear:both; padding:10px 0 0 0; <%=estiloBordeSeparados%> font-size:15px;" ><%=objIdioma.getIdioma("contactosDuplicados_Titulo2")%></h1>        

<div style="position:relative; top:-40px;">
   	    		<a class="cEmergente" id="anadirRegistro" href="<%=carpetaInterna%>/APP/comun2/formMantenimiento.asp?refrescarReg=1&modo=alta&tabla=CONTACTOS&filtro=CON_SUCLIENTE&valorfiltro=<%=codCliente%>&cabecera=<%="Contacto"%>" title="<%=objIdioma.getIdioma("contactosDuplicados_Texto1")%>" style="position:relative; top:40px; z-index:2;" ></a>                
        	    <table id="tablaContenido" name="tablaContenido" width="100%" border="0"  cellspacing="1" cellpadding="0"  style="z-index:1; padding:0px; margin:0px;" >
			        <thead> 
				        <tr class="cabeceraTabla">
                    	<%
							'***** PINTAMOS LA CABECERA DE LAS CLAVES. SI NO SON VISIBLES SE OCULTAN EN EL JTABLE *****
							for i = 0 to ubound(camposClave,2)
								%><th width="10px"><%=camposClave(0,i)%></th><%
							next
							'***** PINTAMOS LA CABECERA DE LOS CAMPOS *****
								
							for i = 0 to ubound(camposGenerales,2)
								if camposGenerales(1,i) then
									%><th width="10px"><%=camposGenerales(0,i)%></th><%
								end if
							next					
						%>
                        <th class="thOpciones" width="30px" align="center" style="margin:0; padding:0 0px 0 2px;" >&nbsp;</th>
    				    </tr>
			        </thead>
		        <tbody>
                
		        </tbody>
		        </table>
</a>                
        </form>

<!-- Funciones de validacion-->
	<script language="Javascript">
	
	function ValidaFormularioAlta(theForm)
	{
	  //Escribimos las funciones de validacion de los campos obligatorios
	  <%=strValidacionObligatorio%>
	  //Escribimos las funciones de validacion de los campos de texto
	  <%=strValidacionCampoDeTexto%>
	  //Escribimos las funciones de validacion de los campos de email
	  <%=strValidacionCampoIP%>
	  //Escribimos las funciones de validacion de los campos de email
	  <%=strValidacionCampoEmail%>
	  //Escribimos las funciones de validacion de los campos num�ricos
	  <%=strValidacionCampoNumerico%> 
	  //Escribimos las funciones de validacion de los campos num�ricos decimales
	  <%=strValidacionCampoNumericoDecimal%> 
	  //Escribimos las funciones de validacion de los campos fecha
	  <%=strValidacionCampoFecha%> 
	  //Escribimos las funciones de validacion de los campos hora
	  <%=strValidacionCampoHora%>   
	  //si se ha validado todo submitimos el formulario con los datos
	  document.mtoContactosDuplicados.submit();	   
	} 
	
/*	function recargarAlCerrar()
	{
		if (document.forms[0]!=null)
		 { document.forms[0].submit(); }
		 else { window.location.reload(); }		
	}	*/
	</script>    
	
	</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->