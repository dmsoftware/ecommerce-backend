<%response.Charset="ISO-8859-1"
'Response.AddHeader "Content-Type", "text/xml; charset=utf-8"%>
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<% 

tabla = "CONTACTOS"'request("tabla")
'codigo = request("codigo")
codCliente = request("codCliente")

tablaOrig=request("tablaOrig")
if trim(lcase(tablaOrig))="contactos" Or trim(lcase(tablaOrig))="socios" then
	codigoOrig=request("codigoOrig")
end if


'tabla = request("tabla")
arrFiltro = split(request("filtro"),";")
arrValFiltro = split(request("valFiltro"),";")
arrColumnas = split(request("columnas"),";")

datosSelect = replace(request("columnas"),";",",")
arrDatosSelectCampoClave = split(request("columnas"),";")

fromAnterior=request("fromAnterior")
fromPosterior=request("fromPosterior")


''''''' Hasta aqui la parte temporal
filtro = trim(arrFiltro(0))
valorFiltro = trim(arrValFiltro(0))
filtro2 = trim(arrFiltro(1))
valorFiltro2 = trim(arrValFiltro(1))
'response.Write("@"& request("iSortCol_0") & "@<br/>")
cOrdenacion = request("iSortCol_0")
cabecera = request("cabecera")
sSearch = Request("sSearch") 
'Machacamos la variable sSearch propia de la tabla, para poder controlar la letra �
sSearch=request("buscadorTabla")
sSearch=replace(sSearch,"|NN|","�")
sSearch=replace(sSearch,"|nn|","�")

dirOrdenacion="desc"
'if request("sSortDir_0") then
'	dirOrdenacion="asc"
'end if



'*********************************

'***** FUNCIONES MAESTRA *****
'*****************************
function crearFiltro(tabla,campo,valor)
	cfQuery = "SELECT tipo FROM maestra WHERE tabla = '" & tabla & "' AND campo = '" & campo & "'"
  	Set rsFiltro = connCRM.AbrirRecordset(cfQuery,3,1)
	if not rsFiltro.eof then
		fTipo = rsFiltro("tipo")
		rsFiltro.cerrar
		Set rsFiltro = nothing
		select case fTipo
			case "T", "P"
				cCondicion = " AND " & trim(arrFiltro(i)) & " = '" & trim(arrValFiltro(i)) & "'"
			case "F"
'				cCondicion = " AND DateValue(" & trim(arrFiltro(i)) & ",'ddddd') = DateValue(" & trim(arrValFiltro(i)) & ",'ddddd')"
'				cCondicion = " AND DateValue(" & trim(arrFiltro(i)) & ") = DateValue('" & trim(arrValFiltro(i)) & "')"
				cCondicion = " AND Format(" & trim(arrFiltro(i)) & ",'ddddd') = Format('" & trim(arrValFiltro(i)) & "','ddddd')"
			case else
				cCondicion = " AND " & trim(arrFiltro(i)) & " = " & trim(arrValFiltro(i))
		end select 
		crearFiltro = cCondicion
	end if
end function

function crearConsultaBusqueda(camposBusqueda,valorBusqueda,convertirTildes)
	resultado = ""
	if convertirTildes then
		valorBusqueda=sustitucion_vocales(valorBusqueda)	
	end if

	if valorBusqueda <> "" then
		for i = 0 to ubound(camposBusqueda)
			if i = 0 then operador = " AND ( " else operador = " OR " end if
				resultado = resultado & operador & camposBusqueda(i) & " LIKE '%" & valorBusqueda & "%'" 	
		next	
		resultado = resultado & ")"
	end if		
'	response.Write(resultado)
'	response.End()
	crearConsultaBusqueda = resultado
end function

function escaparCaracteres(str,codificar)
	dim out

	if isnull(str) = false then
		out = Replace(str, """", "\""")		
'		out = Replace(out, Chr(11), " ")
'		out = Replace(out, Chr(13), " ")
'		out = Replace(out, Chr(0), " ")
'		out = Replace(out, Chr(26), " ")
'		out = Replace(out, Chr(10), " ")
		if 	codificar then
			out = Replace(out, "\""", "''")				
			out = Replace(out, "/""", "''")						
			out = Replace(out, "\", "\\")		
'			out = Replace(out, "&#61664;", " ")					
		end if	
		
		dim contAscii
		For contAscii = 0 To 31 
			out = Replace(out, Chr(contAscii), " ")				
		Next
		out = Replace(out, Chr(127), " ")				
		
	end if
	if codificar then out = server.HTMLEncode(out)
	escaparCaracteres = out
end function

function obtenerDatosCampo(tabla,campo)

	Redim arrAux(7)
	
	odcQuery = "SELECT M1.tipo AS m1Tipo,M1.Campo as m1Campo, M1.ClaveExt AS m1ClaveExt, M1.TablaExt AS m1TablaExt, M1.CampoExt0 AS m1CampoExt, (CASE WHEN M1.NomUsuario_" & Idioma & "='' Or M1.NomUsuario_" & Idioma & "='' THEN M1.NomUsuario_" & DEFAULT_LANGUAGE & " ELSE M1.NomUsuario_" & Idioma & " END) AS m1Nom, M2.tipo AS m2Tipo, M2.campo AS m2Campo " &_
			   "FROM maestra AS M1 LEFT JOIN (SELECT tabla,tipo,campo FROM maestra WHERE clave=-1) AS M2 ON M1.TablaExt = M2.Tabla " &_
			   "WHERE M1.Tabla='" & Tabla & "' AND M1.Campo ='" & Campo &"'"

'response.write odcquery & "<br><br>"
	Set rsDatos = connCRM.abrirRecordSet(odcQuery,3,1)
	if not rsDatos.eof then
		arrAux(0) = rsDatos("m1ClaveExt") '�Es clave externa?    
		arrAux(1) = rsDatos("m1TablaExt") 'Tabla externa
		arrAux(2) = rsDatos("m1CampoExt") 'Campo externo
		arrAux(3) = rsDatos("m1Tipo") 	  'Tipo del campo
		arrAux(4) = rsDatos("m2Campo")    'Clave externa
		arrAux(5) = rsDatos("m2Tipo")     'Tipo clave externa
		arrAux(6) = rsDatos("m1Nom")	  'Descripcion del campo		
		arrAux(7) = rsDatos("m1Campo")	  'Descripcion del campo		

	end if
    obtenerDatosCampo = arrAux
end function

function valorClaveExterna (tabla, campo, clave, tipoClave, codigo)
	resultado = ""
  	if Codigo <> "" then
		vceQuery = "SELECT " & campo  & " FROM " & tabla & " WHERE " & Clave & "=" 
	  	if tipoClave = "T" then
	    	vceQuery = vceQuery & "'" & codigo & "'"
	  	else
	    	vceQuery = vceQuery & codigo
	  	end if
	  	Set rsResultado = connCRM.abrirRecordset(vceQuery,3,1)

	  	if not rsResultado.eof then
	 		resultado =  rsResultado(0)
	  	end if
	  	rsResultado.cerrar
	  	Set rsResultado = nothing
  	end if
  	valorClaveExterna = resultado
end function

function vistaFecha(fecha,tipo)
	'**** Tipo = 1 -> Fecha ; Tipo = 2 -> Hora *****
	'***********************************************
  if isdate(fecha) then 
  	 if isnull(fecha) then
	 	vistaFecha = ""
	 else
	 	if tipo = 1 then
		 	vistaFecha = right("00" & day(fecha),2) & "/" & right("00" & month(fecha),2) & "/" & right("0000" & year(fecha),4)
		else
			vistaFecha = right("00" & hour(fecha),2) & ":" & right("00" & minute(fecha),2)
		end if
	 end if
  end if
end function  

function relaciones1N(tabla)
	Redim arrRelaciones(-1)
	Redim strutRelacion(3) '0 -> denominaci�n de la tabla ; 1-> tabla de la realaci�n ; 2 -> campo de la relaci�n ; 3--Mostrar Relacion
	query = "SELECT tabla, campo FROM maestra WHERE tablaExt = '" & tabla & "' AND ClaveExt=-1 ORDER BY orden"
'	response.Write(query)
'	response.End()
	Set rsResul = connCRM.AbrirRecordset(query,3,1)
	i = 0
	while not rsResul.eof
		redim preserve arrRelaciones(i)
		denominacionTabla = ""
		MostrarEnTrabla=False
'		query = "SELECT iif(trim(NomUsuario_" & Idioma & ")='' Or IsNull(NomUsuario_" & Idioma & "),NomUsuario_" & DEFAULT_LANGUAGE & ",NomUsuario_" & Idioma & ") as nomUsuario FROM maestra WHERE Tabla='" & Tabla & "G'"
		query = "SELECT (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & "='' THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario FROM maestra WHERE Tabla='" &  rsResul("tabla") & "G'"
		set rsNombre = connCRM.abrirRecordSet(query,3,1)
		if not rsNombre.eof then
			denominacionTabla = rsNombre("nomUsuario")
		end if
		rsNombre.cerrar
		Set rsNombre = nothing
		
		query = "SELECT MostrarEnTablaExt FROM maestra WHERE Tabla='" &  rsResul("tabla") & "' And ClaveExt=1 And TablaExt='"&tabla&"' "
		set rsMostrar = connCRM.abrirRecordSet(query,3,1)
		if not rsMostrar.eof then
			MostrarEnTrabla = rsMostrar("MostrarEnTablaExt")
		end if
		rsMostrar.cerrar
		Set rsMostrar = nothing
		
		strutRelacion(0) = denominacionTabla
		strutRelacion(1) = rsResul("tabla")
		strutRelacion(2) = rsResul("campo")
		strutRelacion(3) = MostrarEnTrabla
		arrRelaciones(i) = strutRelacion		
		i = i + 1
		rsResul.moveNext()
	wend	
	rsResul.cerrar()
	Set rsResul = nothing
	relaciones1N = arrRelaciones
end function

function relacionesMN(tabla)
	Redim arrRelaciones(-1)
	Redim strutRelacion(1) '0 -> c�digo 1-> nombre
	query = "SELECT mamn_codigo, MAMN_TEXTO0 as mamn_textomn FROM maestra_mn WHERE mamn_tabla1 = '" & tabla & "'"
	Set rsResul = connCRM.AbrirRecordset(query,3,1)
	i = 0
	while not rsResul.eof
		redim preserve arrRelaciones(i)
		strutRelacion(0) = rsResul("mamn_codigo")
		strutRelacion(1) = rsResul("mamn_textomn")
		arrRelaciones(i) = strutRelacion		
		i = i + 1
		rsResul.moveNext()
	wend	
	rsResul.cerrar()
	Set rsResul = nothing
	relacionesMN = arrRelaciones
end function

function camposFichero(tabla)
	Redim arrFicheros(-1)
	Redim strutFicheros(1) '0 -> nombre del campo 1-> indice del campo
	query = "SELECT Campo FROM maestra WHERE tabla = '" & tabla & "' and tipo='X' order by orden"
	indice = 97 'El 97 corresponde a la 'a'
	i = 0
	Set rsResul = connCRM.AbrirRecordset(query,3,1)
	while not rsResul.eof
		redim preserve arrFicheros(i)
		strutFicheros(0) = rsResul("campo")
		strutFicheros(1) = Chr(97)
		arrFicheros(i) = strutFicheros
		indice = indice + 1
		i = i + 1
		rsResul.movenext
	wend
	rsResul.cerrar()
	Set rsResul = nothing
	camposFichero = arrFicheros
end function
'*****************************


'***** CREAMOS LA CONSULTA *****
'*******************************
if trim(lcase(tablaOrig))="socios" then

	emailSocio=""
	movilSocio=""
	dniSocio=""
	nombreSocio=""
	
	sqlComparableSocio=" Select CON_CODIGO,CON_NOMBRE + ' ' + CON_APELLIDO1 as nombreCompleto,CON_EMAIL,CON_DNI,CON_MOVIL From SOCIOS Where CON_CODIGO=" & codigoOrig
	Set rsComparablesSocio = connCRM.AbrirRecordset(sqlComparableSocio,0,1)
	if not rsComparablesSocio.eof then 
		nombreSocio=rsComparablesSocio("nombreCompleto")
		movilSocio=rsComparablesSocio("CON_MOVIL")
		dniSocio=rsComparablesSocio("CON_DNI")
		emailSocio=rsComparablesSocio("CON_EMAIL")						
	end if
	rsComparablesSocio.cerrar()
	Set rsComparablesSocio = nothing

	consultaWhere=consultaWhere & " And (TDev.ContDuplicadosMail>1 or TDev.ContDuplicadosMovil>1 Or TDev.ContDuplicadosDNI>1 Or TDev.ContDuplicadosNombre>1 ) "
	
	datosSelect=datosSelect & ",TDev.ContDuplicadosMail,TDev.ContDuplicadosMovil,TDev.ContDuplicadosDNI,TDev.ContDuplicadosNombre "
	fromAnterior=fromAnterior & "("
	fromPosterior=fromPosterior & " left join ( Select C2.CON_CODIGO as codigoSocio,TDevMail.ContDuplicadosMail,TDevMovil.ContDuplicadosMovil ,TDevDNI.ContDuplicadosDNI,TDevNombre.ContDuplicadosNombre "
	fromPosterior=fromPosterior & " From (((( CONTACTOS C2 "	
	fromPosterior=fromPosterior & " left join ( "
	fromPosterior=fromPosterior & " Select C3.CON_EMAIL,Count(C3.CON_EMAIL) as ContDuplicadosMail From CONTACTOS C3 "
	fromPosterior=fromPosterior & " Where not C3.CON_EMAIL is null And C3.CON_EMAIL<>'' And C3.CON_EMAIL<>'-' "
	fromPosterior=fromPosterior & " group by C3.CON_EMAIL "
	fromPosterior=fromPosterior & " Having count(C3.CON_EMAIL)>1 "
	fromPosterior=fromPosterior & " ) as TDevMail on (C2.CON_EMAIL=TDevMail.CON_EMAIL) ) "
	fromPosterior=fromPosterior & " left join ( "
	fromPosterior=fromPosterior & " Select C3.CON_MOVIL,Count(C3.CON_MOVIL) as ContDuplicadosMovil From CONTACTOS C3 "
	fromPosterior=fromPosterior & " Where C3.CON_MOVIL is not null And C3.CON_MOVIL<>'' "
	fromPosterior=fromPosterior & " group by C3.CON_MOVIL "
	fromPosterior=fromPosterior & " Having count(C3.CON_MOVIL)>1 "
	fromPosterior=fromPosterior & " ) as TDevMovil on (C2.CON_MOVIL=TDevMovil.CON_MOVIL) ) "
	fromPosterior=fromPosterior & " left join ( "
	fromPosterior=fromPosterior & " Select C3.CON_DNI,Count(C3.CON_DNI) as ContDuplicadosDNI From CONTACTOS C3 "
	fromPosterior=fromPosterior & " Where C3.CON_DNI is not null And C3.CON_DNI<>'' "
	fromPosterior=fromPosterior & " group by C3.CON_DNI "
	fromPosterior=fromPosterior & " Having count(C3.CON_DNI)>1 "
	fromPosterior=fromPosterior & " ) as TDevDNI on (C2.CON_DNI=TDevDNI.CON_DNI) ) "
	fromPosterior=fromPosterior & " left join ( "
	fromPosterior=fromPosterior & " Select C3.CON_NOMBRE + ' ' + C3.CON_APELLIDO1 as nombreCompleto,Count(C3.CON_NOMBRE + ' ' + C3.CON_APELLIDO1) as ContDuplicadosNombre From CONTACTOS C3 "
	fromPosterior=fromPosterior & " group by C3.CON_NOMBRE + ' ' + C3.CON_APELLIDO1 "
	fromPosterior=fromPosterior & " Having count(C3.CON_NOMBRE + ' ' + C3.CON_APELLIDO1)>1 "
	fromPosterior=fromPosterior & " ) as TDevNombre on ((C2.CON_NOMBRE + ' ' + C2.CON_APELLIDO1)=TDevNombre.nombreCompleto) ) "
	fromPosterior=fromPosterior & " Where ( (C2.CON_NOMBRE + ' ' + C2.CON_APELLIDO1) like '%" & trim(nombreSocio) & "%' "
	if trim(movilSocio)<>"" then
		fromPosterior=fromPosterior & " Or C2.CON_MOVIL like '%" & trim(movilSocio) & "%' "
	end if
	if trim(dniSocio)<>"" then
		fromPosterior=fromPosterior & " Or C2.CON_DNI like '%" & trim(dniSocio) & "%' "
	end if
	if trim(emailSocio)<>"" then
		fromPosterior=fromPosterior & " Or C2.CON_EMAIL like '%" & trim(emailSocio) & "%' "
	end if		
	fromPosterior=fromPosterior & " ) ) as TDev on (" & tabla & ".CON_CODIGO=TDev.codigoSocio) ) "
end if



if consulta = "" then

	inicioQuery =  "SELECT " & datosSelect 
	inicioQueryCampoClave =   "SELECT " & arrDatosSelectCampoClave(0)
	 
	query = " FROM " & fromAnterior & tabla & fromPosterior & " WHERE (1=1"
	queryFrom = " FROM " & fromAnterior & tabla & fromPosterior & " "
	
	for i = 0 to ubound(arrFiltro)
		if trim(arrFiltro(i)) <> "" then	    
			query = query & crearFiltro(tabla,trim(arrFiltro(i)),trim(arrValFiltro(i)))
		end if
	next

	'***** BUSCADOR NATIVO DE LA TABLA *****
	query = query & crearConsultabusqueda(arrColumnas,sSearch,false)
'	queryMemoria=queryMemoria & crearConsultabusqueda(arrColumnas,sSearch,false)
	'***************************************
	query = query & consultaWhere & " ) "
'	queryMemoria = queryMemoria & consultaWhere & " ) "	
	ordenColumna=0
	

	if cint(cOrdenacion)<=cint(ubound(arrColumnas)) then
		queryOrdenamiento= " ORDER BY " & arrColumnas(cOrdenacion) & " " & request("sSortDir_0")
		inicioQueryCampoClave=inicioQueryCampoClave & "," & arrColumnas(cOrdenacion)
	else
		queryOrdenamiento= " ORDER BY " & arrColumnas(0) & " " & request("sSortDir_0")	
		inicioQueryCampoClave=inicioQueryCampoClave & "," & arrColumnas(0)		
	end if	
else
	query = consulta
'	queryMemoria = consulta	
end if 
'*******************************
	

	
 	query = inicioQuery & query & queryOrdenamiento

'response.Write(query)
'response.End()


	'''''''''''''''''''''''''''''''''''''''''''''''

'***** OBTENEMOS LA INFORMACION ADICIONAL DE LOS CAMPOS (CLAVES EXTERNAS, .....) *****
'*************************************************************************************
Redim infoCampo(ubound(arrColumnas))
for i= 0 to ubound(arrColumnas)
	infoCampo(i) = obtenerDatosCampo(tabla,arrColumnas(i))
next
'*************************************************************************************		


'query=" select * "
'query=query & " from ( "
'query=query & "SELECT ROW_NUMBER() OVER(ORDER BY CLIE_CODIGO) AS num, "
'query=query & "CLIE_CODIGO,CLIE_NOMBRE,CLIE_LOCALIDAD,CLIE_EMAIL,CLIE_CONTACTO,CLIE_TELEFONO,CLIE_COMERCIAL,CLIE_EMAILCONTDESARROLLO "
'query=query & " FROM clientes "
'query=query & "  WHERE 1=1 "
'query=query & "  ) as TDer "
'query=query & " WHERE (num >= 30 AND num <= 40) "

query=replace(query,"|NN|","�")
query=replace(query,"|nn|","�")

if trim(cstr(session("codigoUsuario")))="49" then
'	response.Write(query)
'	response.End()
end if	

Set rsResultados = connCRM.AbrirRecordset(query,3,1)

numCampos = rsResultados.recordcount()

if isnumeric(request("iDisplayLength")) And cint(trim(request("iDisplayLength")))>0 then

	tamPagina = cint(request("iDisplayLength"))

	if tamPagina>1 then
		rsResultados.PageSize = tamPagina
	else
		rsResultados.PageSize = 1
	end if	

	pagina = request("iDisplayStart") / tamPagina
	pagina = pagina + 1
else
	tamPagina=numCampos
	rsResultados.PageSize = 1	
	pagina = 1
end if	

if numCampos > 0 then
	rsResultados.AbsolutePage = pagina
end if	

session(denominacion & Tabla & "_numRegistrosTabla")=tamPagina
session(denominacion & Tabla & "_paginaTabla")=pagina-1
session(denominacion & Tabla & "_busquedaTabla")=sSearch
session(denominacion & Tabla & "_campoOrdenacion")=cOrdenacion
session(denominacion & Tabla & "_direccionOrdenacion")=request("sSortDir_0")	


resultado =  "{""sEcho"":" & request("sEcho") & ","
resultado = resultado & """iTotalRecords"":" & numCampos & ","
resultado = resultado & """iTotalDisplayRecords"":" & numCampos & ","
resultado = resultado & """aaData"": ["

contador = 0

while contador < tamPagina and not rsResultados.eof 
	resultado = resultado & "["
	for i=0 to ubound(arrColumnas)

		arrInfoCampo = infoCampo(i)
'		response.Write(arrInfoCampo(0))
'response.Write(arrInfoCampo(5))
'response.End()
		if arrInfoCampo(0) then 'Es clave externa
			resultado = resultado & """" & escaparCaracteres(valorClaveExterna(arrInfoCampo(1), arrInfoCampo(2), arrInfoCampo(4), arrInfoCampo(5),rsResultados(i).value),true) & """," 
		else
			'************ ESCRIBIMOS EL VALOR TENIENDO EN CUENTA EL TIPO DE CAMPO ************
			'***** P -> Password ; B -> Booleano ; F -> Fecha ; H -> Hora ; X -> Fichero *****
			'******** T -> Texto (Valor por defecto); LN -> Link ; SM,SM,SE -> Nodos  ********
			'*********************************************************************************
			codificacion = true
			noEscapar=false			
'			response.Write(arrInfoCampo(7) & "<br/>")
				select case arrInfoCampo(3)
					case "P"
						datosColumna = "***"				
					case "B" 

						seleccionCheck=" value=""0"" "
						if rsResultados(i).value then
							seleccionCheck=" checked=""checked"" value=""1"" "
						end if	
						datosColumna = "<input id=""" & i & "_" & rsResultados(0) & """ name=""" & rsResultados(0) & """ rel=""" & arrInfoCampo(7) & """ class=""checksContactos""  type=""checkbox"" " & seleccionCheck & " />"

						codificacion = false	
					case "F"
						datosColumna = vistaFecha(rsResultados(i).value,1)
					case "H"
						datosColumna = vistaFecha(rsResultados(i).value,2)
					case "X"
						datosColumna = rsResultados(i).value				
					case "LN"
						datosColumna = "<a href=""" & rsResultados(i).value & """><img src=""/dmcrm/APP/Comun2/imagenes/l0.gif"" class=""imgLn"" title=""" & escaparCaracteres(arrInfoCampo(6),true) & """>" & escaparCaracteres(arrInfoCampo(6),true) & "</a>"
						codificacion = false
					case "SN","SM","SE"	
						datosColumna = rsResultados(i).value		
					case "M"
'						datosColumna=""'len(rsResultados(i).value)
'						if len(rsResultados(i).value)>58 then
'							for contCarac=58 to len(rsResultados(i).value)
'								datosColumna=datosColumna & "|" & asc(Mid(rsResultados(i).value,contCarac,1))
'							next						
'						end if
						datosColumna=escaparCaracteres(rsResultados(i).value,codificacion)						

					case else 
						datosColumna = rsResultados(i).value '& "<p id=\""" & rsResultados(0).value & "\"" class=\""ordenacion\""></p>" '"<p id=\""" & rsResultados(0).value & "\"" class=\""ordenacion\"" rel=\""" & arrInfoCampo(7) & "\"">" & rsResultados(i).value & "</p>"	
'						response.Write(contador & "|" & i&"<br/>")
						
'						datosColumna = "<p id=\""" & rsResultados(0).value & "\"" class=\""ordenacion\"" rel=\""nomCol\"">" & rsResultados(i).value & "</p>"						
						noEscapar = true									
				end select						

			if noEscapar then		
				resultado = resultado & """" & datosColumna & ""","
			else
				if arrInfoCampo(3)<>"M" then
					resultado = resultado & """" & escaparCaracteres(datosColumna,codificacion) & ""","
				else
					resultado = resultado & """" & datosColumna & ""","			
				end if	
			end if
			
			'Las siguientes lineas son para poner el html de una imagen, cuando la relaci�n de 1 a N es una imagen
			resultado=replace(resultado,"&lt;","<")
			resultado=replace(resultado,"&gt;",">")				
'			resultado=replace(resultado,"\","/")							
		end if
	next
	datosColumna = ""

	''Comprobamos si el contacto tiene algun registro en Acciones o Proyectos
	tieneRelaciones=false
'	descripcionRelacionesInicial=" No se puede borrar el contacto, porque est� asociado con las siguientes entidades: "
	descripcionRelacionesInicial=objIdiomaGeneral.getIdioma("Contacto_Texto1")

	descripcionRelaciones=""
	sqlRelaciones = " Select (Select (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & "='' THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario From maestra Where tabla='Acciones_ClienteG')  as tipoTabla,count(ACCI_CODIGO) as contador "
	sqlRelaciones = sqlRelaciones & " From Acciones_Cliente "
	sqlRelaciones = sqlRelaciones & " Where ACCI_SUCONTACTO=" & rsResultados(0)
	sqlRelaciones = sqlRelaciones & " Union "
	sqlRelaciones = sqlRelaciones & " Select (Select (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & "='' THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario From maestra Where tabla='ProyectosG') as tipoTabla,count(PROY_CODIGO) as contador "
	sqlRelaciones = sqlRelaciones & " From Proyectos "
	sqlRelaciones = sqlRelaciones & " Where PROY_SUCONTACTO=" & rsResultados(0)			
	'response.Write(sqlRelaciones&"<br/>")
  	Set rsRelaciones = connCRM.AbrirRecordset(sqlRelaciones,3,1)
	while not rsRelaciones.eof 
		if isnumeric(rsRelaciones("contador")) then
			if cint(rsRelaciones("contador"))>0 then
				if tieneRelaciones then
					descripcionRelaciones= descripcionRelaciones &  ", "
				end if
				descripcionRelaciones=descripcionRelaciones &  rsRelaciones("tipoTabla")				
				tieneRelaciones=true
			end if
		end if
		rsRelaciones.movenext
	wend
	rsRelaciones.cerrar()
	Set rsRelaciones = nothing
	



	if tieneRelaciones then
		datosColumna = datosColumna & "<img src=""/dmcrm/APP/Comun2/imagenes/relaciones.png"" title=""" & descripcionRelacionesInicial & descripcionRelaciones & """ />"			
		datosColumna = datosColumna & "<img src=""/dmcrm/APP/Comun2/imagenes/deleteRelacionados.png"" title=""" & descripcionRelacionesInicial & descripcionRelaciones & """ />"	
	else
		datosColumna = datosColumna & "<img src=""/dmcrm/app/imagenes/3_trans.png"" width=""23"" title=""" & descripcionRelacionesInicial & descripcionRelaciones & """ />"				
		datosColumna = datosColumna & "<a  onClick=""borrarContacto('" & rsResultados(0) & "')""   ><img src=""/dmcrm/APP/Comun2/imagenes/deleteReg.png"" title=""" & objIdiomaGeneral.getIdioma("Contacto_Texto2") & """ /></a>"	
	end if
	
	datosColumna = datosColumna & "<a class=""cEmergente"" href=""/dmcrm/APP/comun2/formMantenimiento.asp?codigo=" & rsResultados(0) & "&amp;modo=modificacion&amp;tabla=CONTACTOS""><img src=""/dmcrm/APP/Comun2/imagenes/c0.gif"" title=""" & objIdiomaGeneral.getIdioma("SeleccionAjax_4") & """ /></a>"

	datosColumna = datosColumna & "<input type=""hidden"" id=""idFila_" & contador & """ value=""" & rsResultados(0) & """ />"
	
	
	datosColumnaCompelto=datosColumna	
'	if trim(codigoOrig)<>"" And trim(codigoOrig)=trim(rsResultados(0)) then
'		datosColumnaCompelto="<span style=""background:red;"">" & datosColumna & "</span>"	
'	end if


	resultado = resultado & """" & escaparCaracteres(datosColumnaCompelto,false) & """"
'	resultado = resultado & """" & datosColumna & """"
	resultado = resultado & "],"	

	contador = contador + 1
	rsResultados.movenext
wend




if contador <> 0 then
	resultado = mid(resultado,1,len(resultado) - 1)
end if
resultado = resultado & "]}"
response.Write(resultado)

rsResultados.cerrar()
Set rsResultados = nothing
%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->