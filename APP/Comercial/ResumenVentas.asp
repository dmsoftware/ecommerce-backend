<%Response.buffer=false %>

<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%
anoActual=request("an")
if anoActual="" then anoActual=year(date)

strIdentificador="Gastos" & cstr(second(now())) & cstr(minute(now()))

'Variables
boolValue="1"

ProbabilidadPedido=100

ProbabilidadPreaprobado=90
ProbabilidadGrado1=0
ProbabilidadGrado2=0
ProbabilidadGrado3=0

nomProbAlta=""
nomProbMedia=""
nomProbBaja=""
sqlPorcProb=" Select TAPEC_CODIGO,TAPEC_PORC, TAPEC_DESCRIPCION  as nomProb " 
sqlPorcProb=sqlPorcProb & " From TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE E "
'sqlPorcProb=sqlPorcProb & " Left Join IDIOMAS_DESCRIPCIONES D on (E.TAPEC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE' And D.IDID_CAMPO='TAPEC_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "
sqlPorcProb=sqlPorcProb & " Where TAPEC_PORC is not null  "

Set rsPorcProb = connCRM.AbrirRecordset(sqlPorcProb,3,1)

while not rsPorcProb.eof 
	if trim(rsPorcProb("TAPEC_CODIGO"))="1" then
		ProbabilidadGrado1=cdbl(rsPorcProb("TAPEC_PORC"))
		nomProbAlta=rsPorcProb("nomProb")
	elseif trim(rsPorcProb("TAPEC_CODIGO"))="2" then
		ProbabilidadGrado2=cdbl(rsPorcProb("TAPEC_PORC"))
		nomProbMedia=rsPorcProb("nomProb")		
	elseif trim(rsPorcProb("TAPEC_CODIGO"))="3" then
		ProbabilidadGrado3=cdbl(rsPorcProb("TAPEC_PORC"))	
		nomProbBaja=rsPorcProb("nomProb")		
	end if
	
	rsPorcProb.movenext
wend
rsPorcProb.cerrar()
Set rsPorcProb = nothing

contObjAreas=2
objAnual=0.0
cabObjAreas=""
colObjAreas=""
colObjAreasMensual=""
sqlObjetivos=" Select O.TANO_CODIGO,O.TANO_SUAREANEGOCIO,O.TANO_ANNO,O.TANO_OBJETIVO "
sqlObjetivos=sqlObjetivos & ", N.TAN_DESCRIPCION  as nomAreaNegocio  "
sqlObjetivos=sqlObjetivos & " From TIPOS_AREA_NEGOCIO_OBJETIVOSMENSUALES O "
sqlObjetivos=sqlObjetivos & " Inner Join TIPOS_AREA_NEGOCIO N On (O.TANO_SUAREANEGOCIO=N.TAN_CODIGO) "
'sqlObjetivos=sqlObjetivos & " Left Join IDIOMAS_DESCRIPCIONES D on (N.TAN_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AREA_NEGOCIO' And D.IDID_CAMPO='TAN_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "
sqlObjetivos=sqlObjetivos & " Where TANO_ANNO='" & anoActual & "' "
sqlObjetivos=sqlObjetivos & " Order by  N.TAN_DESCRIPCION "

Set rsObjetivos = connCRM.AbrirRecordset(sqlObjetivos,3,1)

while not rsObjetivos.eof 
	cabObjAreas=cabObjAreas & "<th>" & rsObjetivos("nomAreaNegocio") & "</th>"
	colObjAreas=colObjAreas & "<td>" & formatnumber(rsObjetivos("TANO_OBJETIVO"),2) & " � </td>"	
'	TANO_OBJETIVO_MENSUAL=0.0
	colObjAreasMensual=colObjAreasMensual & "<td>" & formatnumber((cdbl(rsObjetivos("TANO_OBJETIVO")) / 12),2) & " � </td>"	
	
	if not isnull(rsObjetivos("TANO_OBJETIVO")) then
		if trim(rsObjetivos("TANO_OBJETIVO"))<>"" then
			objAnual=objAnual + cdbl(rsObjetivos("TANO_OBJETIVO"))
		end if
	end if
	
	contObjAreas=contObjAreas + 1
	
	rsObjetivos.movenext
wend
rsObjetivos.cerrar()
Set rsObjetivos = nothing

GastoPrevistoMes=objAnual / 12.0
if GastoPrevistoMes=0 then
	GastoPrevistoMes=1
end if
response.Write("#"&objAnual&"#")

function Mes(Valor)
	select case Valor
	case 1
		Mes=ucase("ENE")
	case 2
		Mes=ucase("FEB")
	case 3
		Mes=ucase("MAR")
	case 4
		Mes=ucase("ABR")
	case 5
		Mes=ucase("MAY")
	case 6
		Mes=ucase("JUN")
	case 7
		Mes=ucase("JUL")
	case 8
		Mes=ucase("AGO")
	case 9
		Mes=ucase("SEP")
	case 10
		Mes=ucase("OCT")
	case 11
		Mes=ucase("NOV")
	case 12
		Mes=ucase("DIC")
	end select   
end function

Sub FormatoEuros(valor)
    if isnull(valor) then
        response.write "0 �"
    else
        response.Write FormatNumber(valor, 2) & " �"
    end if
end sub


Sub cargarTablaObjetivos(strTipo)

		'Seleccionamos los pedidos indicados
		''strTipo: 1:Neto ; 2:Bruto
		textoTitTabObj="BRUTO"
		esObjNeto=false
		if strTipo=1 then
			textoTitTabObj="NETO"
			esObjNeto=true
		end if

		sql=" Select SUM(A.ACCI_TIPO7GASTOS) AS GASTOS,SUM(A.ACCI_TIPO6CANTIDAD) AS VENTAS, SUM(A.ACCI_TIPO6CANTIDAD) - SUM(A.ACCI_TIPO7GASTOS) AS TOTAL,MONTH(A.ACCI_FECHA) AS MES, TAN_DESCRIPCION "
		sql=sql & " from (( ACCIONES_CLIENTE as A Inner Join ACCIONES_CLIENTE as B On (A.ACCI_CODIGO=B.ACCI_CODIGO) ) "
		sql=sql & " LEFT JOIN TIPOS_AREA_NEGOCIO ON (B.ACCI_TIPO6SUAREANEGOCIO=TIPOS_AREA_NEGOCIO.TAN_CODIGO) ) "
		sql=sql & " where "
		if anoActual<>"" and anoActual<>0 then
		   sql=sql & " year(A.ACCI_FECHA)=" & anoActual
		end if
		sql=sql & " AND A.ACCI_SUTIPO=7 "
		sql=sql & " AND A.ACCI_TERMINADA=" & boolValue
		sql=sql & " AND A.ACCI_TIPO7FINALIZACION=" & boolValue
		
		sql=sql & " and B.ACCI_SUCLIENTE=A.ACCI_SUCLIENTE "
		sql=sql & " and B.ACCI_SUCAMPANA=A.ACCI_SUCAMPANA "
		sql=sql & " and B.ACCI_PROYECTO=A.ACCI_PROYECTO "
		sql=sql & " and B.ACCI_SUTIPO=6 "
		sql=sql & " and B.ACCI_TERMINADA=" & boolValue
		
		sql=sql & " GROUP BY MONTH(A.ACCI_FECHA),TAN_DESCRIPCION "
		sql=sql & " order by MONTH(A.ACCI_FECHA),TAN_DESCRIPCION "

		Set rstVentas= connCRM.AbrirRecordset(sql,3,1)
		
		'Seleccionamos las �reas de negocio
		sql="select TAN_DESCRIPCION from TIPOS_AREA_NEGOCIO "
		sql=sql & " UNION SELECT top 1 null From TIPOS_AREA_NEGOCIO  "
		sql=sql & " order by TAN_DESCRIPCION" 
		Set rstComerciales= connCRM.AbrirRecordset(sql,3,1)
		
		'contadores
		TotalMes=0
		TotalMesIsp=0
		totalAnoEsfuerzo=0
		arrCantPedidos=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
		
		
		'OFERTAS PREAPROBADAS
		'Apartado de los SEGUIMIENTOS, tipo 7
		sql="select * from acciones_cliente inner join clientes on clientes.clie_codigo=acciones_cliente.acci_sucliente where acci_sutipo=7"
		sql=sql & " and acci_tipo7Finalizacion=3 "	
		sql=sql & " and year(acci_fecha)= " & cint(anoActual) 	 
		Set rstSeg= connCRM.AbrirRecordset(sql,0,1)	
		
		arrCantPreAprobado=array(0,0,0,0,0,0,0,0,0,0,0,0,0)	
		arrCantPreAprobadoGasto=array(0,0,0,0,0,0,0,0,0,0,0,0,0)		    
		while not rstSeg.eof 
		''Response.Write rstSeg("clie_nombre") & " - " & rstSeg("acci_tipo6Cantidad")	& "<br>"
			mesIndice=month(rstSeg("acci_fecha"))
			'Ingresos
			arrCantPreAprobado(mesIndice)=arrCantPreAprobado(mesIndice)+rstSeg("acci_tipo6Cantidad")
			TotalPreAprobado=TotalPreAprobado+rstSeg("acci_tipo6Cantidad")
			'Gastos
			if not isnull(rstSeg("acci_tipo7Gastos")) then
				arrCantPreAprobadoGasto(mesIndice)=arrCantPreAprobadoGasto(mesIndice)+rstSeg("acci_tipo7Gastos")
				TotalPreAprobadoGasto=TotalPreAprobadoGasto+rstSeg("acci_tipo7Gastos")
			end if	
			rstSeg.movenext()
		wend
			
		
		'OFERTAS grado 1, 2 , 3
		'HALLO AQUELLAS QUE TIENEN YA CREADA UN SEGUIMIENTO (TIPO 7) Y NO EST� CERRADO (bien en aprobado o no aprobado) O EN PREAPROBADO (aunque est� cerrado)
		'Y AQELLLAS QUE NO TIENEN TODAV�A UN SEGUIMEINTO
		sql="select TIPO7.ACCI_FECHA,TIPO6.ACCI_TIPO6CANTIDAD,TIPO6.ACCI_TIPO6ESTADOCLIENTE from acciones_cliente as tipo6,acciones_cliente as tipo7  "
		sql = sql & " where TIPO6.acci_sutipo=6 and TIPO7.acci_sutipo=7" 
		sql=sql & " and TIPO7.acci_sucampana=TIPO6.Acci_Sucampana "
		sql=sql & " and TIPO7.acci_sucliente=TIPO6.Acci_suCliente "	
		sql=sql & " and TIPO7.acci_proyecto=TIPO6.Acci_proyecto "			 	
		sql=sql & " and year(TIPO7.acci_fecha)= " & cint(anoActual)
		sql=sql & " and not TIPO7.acci_terminada=" & boolValue
		sql=sql & " and (TIPO7.acci_tipo7Finalizacion<>3 or TIPO7.acci_tipo7Finalizacion is null)"
		Set rstNC= connCRM.AbrirRecordset(sql,0,1)
		
		arrCantGrado1=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
		arrCantGrado2=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
		arrCantGrado3=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
		
		
		'Vamos a rellenar un contador por mes y por grado 
		while not rstNC.eof
		 
		  Select case rstNC("ACCI_TIPO6ESTADOCLIENTE")
		  
			 case 3
				TotalGrado3=TotalGrado3+rstNC("acci_tipo6Cantidad")
				mesIndice=MONTH(rstNC("ACCI_FECHA"))
				arrCantGrado3(mesIndice)=arrCantGrado3(mesIndice)+rstNC("acci_tipo6Cantidad")
			 case 2
				TotalGrado2=TotalGrado2+rstNC("acci_tipo6Cantidad")
				mesIndice=MONTH(rstNC("ACCI_FECHA"))
				arrCantGrado2(mesIndice)=arrCantGrado2(mesIndice)+rstNC("acci_tipo6Cantidad")  
			 case 1	
				TotalGrado1=TotalGrado1+rstNC("acci_tipo6Cantidad")     
				mesIndice=MONTH(rstNC("ACCI_FECHA"))
				arrCantGrado1(mesIndice)=arrCantGrado1(mesIndice)+rstNC("acci_tipo6Cantidad")
		   end select	
		   rstNC.movenext()
			
		wend
		
		'Hallamos el mes actual 
		if cint(anoActual)=year(date) then
		  mesActual=month(date) 
		else
		  mesActual=12
		end if%>
		

			<h1 class="titBloquesH1"> - Desglose <%=textoTitTabObj%> de los objetivos de <%=anoActual%></h1>                
			<table class="tabObjetivos" widht="100%" border="0" rules="all" frame="border"  cellspacing="0" cellpadding="0" bordercolor="#C0C0C0" >
				<tr >
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<%for i=1 to 12%>
						 <th class="colNeg colDer" >
						   <%=Mes(i)%>
						 </th>
					<%next%>
					<th class="colSep" >
						Total
					</th>
					<th class="colSep" >
						Total
					</th>
					<th class="colSep" >
						% A�o
					</th>
					<th class="colSep">
					  %<%=mes(mesActual)%>
					</th>
				</tr>
				
				<tr>
					<td class="colIzq" >
					  Pedidos ERP<br><%=ProbabilidadPedido%>%
					</td>
					<td class="tabSinPadding" style="width:auto;" >
						<%'Pintamos los comerciales
						rstComerciales.movefirst
						intCont=0
						while not rstComerciales.eof
							intCont=intCont+1
							claseFilaArea="capaAreaInterior1"
							if (intCont mod 2 = 0) then
								claseFilaArea="capaAreaInterior2"
							end if%>
                            <div class="capaAreaInterior <%=claseFilaArea%>">&nbsp;
							<%if isnull(rstComerciales("TAN_DESCRIPCION")) then
								Response.write "-"
							else
								response.write rstComerciales("TAN_DESCRIPCION")
							end if%>
							</div>
							<%rstComerciales.movenext
						wend%>
						<div class="lineaSepCelda" ></div>                        
                        <div class="capaAreaInterior">&nbsp;&nbsp;Totales:</div>
					</td>
					<%
					'A�adimos los pedidos mes a mes
					for i=1 to 12
						 intTotalPedidosMes=0%>
						 <td class="tabSinPadding" >
							<%rstComerciales.movefirst
							intCont=0
							while not rstComerciales.eof
								intCont=intCont+1
								claseFilaArea="capaAreaInterior1"								
								if (intCont mod 2 = 0) then
									claseFilaArea="capaAreaInterior2"
								end if
								 'Filtro por el mes actual y el �REA 
								 if isnull(rstComerciales("TAN_DESCRIPCION")) then
									rstVentas.filter="MES=" & i & " and TAN_DESCRIPCION=NULL" 
								 else
									rstVentas.filter="MES=" & i & " and TAN_DESCRIPCION='" & rstComerciales("TAN_DESCRIPCION") & "'"
								 end if
								
								 %><div class="capaAreaInterior <%=claseFilaArea%>">&nbsp;
                                     <div class="subCapaIntIzq" ><%
                                         if not rstVentas.eof then 
                                            if esObjNeto then
                                                FormatoEuros(rstVentas("TOTAL"))
                                                intTotalPedidosMes=intTotalPedidosMes + rstVentas("TOTAL")
                                                totalAnoEsfuezo=totalAnoEsfuezo+rstVentas("TOTAL")
                                            else
                                                FormatoEuros(rstVentas("VENTAS"))
                                                intTotalPedidosMes=intTotalPedidosMes + rstVentas("VENTAS")
                                                totalAnoEsfuezo=totalAnoEsfuezo+rstVentas("VENTAS")									
        
                                            end if								 
        
                                         else
                                            response.write "-"
                                         end if%>
                                      </div>
                                      <div class="subCapaIntDer"  >0%</div>

                                 </div>
								<%rstComerciales.movenext
							wend
							arrCantPedidos(mesIndice)=intTotalPedidosMes%>
							<%ColorLetra=""
							IF intTotalPedidosMes<GastoPrevistoMes THEN
								ColorLetra=" ColorLetra"
							  'ColorLetra="#cc0000"
							END IF%>
								<div class="lineaSepCelda" ></div> 
                                <div class="capaAreaInterior<%=ColorLetra%>">                                                   
                                      <div class="subCapaIntIzq" >
                                        <%FormatoEuros(intTotalPedidosMes)%>
                                      </div>
                                      <div class="subCapaIntDer"  ><%=round((intTotalPedidosMes*100)/(GastoPrevistoMes),0)%>%</div>                                                              

                                  </div>
						 </td>
					<%next%>
					<td class="tabSinPadding" >
						<%rstComerciales.movefirst
						intCont=0
						while not rstComerciales.eof
							intCont=intCont+1
							claseFilaArea="capaAreaInterior1"															
							if (intCont mod 2 = 0) then
								claseFilaArea="capaAreaInterior2"																						
							end if
							intTotalPedidosComercial=0
							if isnull(rstComerciales("TAN_DESCRIPCION")) then
								rstVentas.filter="TAN_DESCRIPCION=NULL" 
							else
								rstVentas.filter="TAN_DESCRIPCION='" & rstComerciales("TAN_DESCRIPCION") & "'"
							end if
                            %><div class="capaAreaInterior <%=claseFilaArea%>">&nbsp;
                                   <div class="subCapaIntIzq" ><%
										while not rstVentas.eof
											if esObjNeto then
												intTotalPedidosComercial=intTotalPedidosComercial + rstVentas("TOTAL")
											else
												intTotalPedidosComercial=intTotalPedidosComercial + rstVentas("VENTAS")								
											end if
											rstVentas.movenext
										wend%>
										<%FormatoEuros(intTotalPedidosComercial)%>
                                  </div>
                                  <div class="subCapaIntDer"  >0%</div>                            

                            </div>
							<%rstComerciales.movenext
						wend
					
						TotalPedidos=totalAnoEsfuezo%>
					
						<%ColorLetra=""
						IF TotalPedidos<GastoPrevistoMes*12 THEN
							ColorLetra=" ColorLetra"
						END IF%>
							<div class="lineaSepCelda" ></div>                        
                          <span class="ColorLetra"><%FormatoEuros(TotalPedidos)%>&nbsp;|&nbsp;
                          <%=round((TotalPedidos*100)/(GastoPrevistoMes*12),2)%>%</span>
					</td>
					<td class="colDer1" >
						  <% TotalPedidos=totalAnoEsfuezo%>
						  <%FormatoEuros(totalAnoEsfuezo) %>  
					</td>
					<td class="colDer2" >
						<%'IF TotalPedidos<GastoPrevistoMes*12 THEN
						  'ColorLetra="#cc0000"
						'END IF%>
					  <%FormatoEuros(TotalPedidos)%>
					  <br /><%=round((TotalPedidos*100)/(GastoPrevistoMes*12),2)%>%
					</td>
					<td class="colDer3">
						<%'IF TotalPedidos<GastoPrevistoMes*mesActual THEN
						  'ColorLetra=" style=""color:#FF6600!important;"" "
						'END IF%>
					  	<%=round((TotalPedidos*100)/(GastoPrevistoMes*mesActual),2)%>%
					</td>
				</tr>
			
				<tr>
					<td class="colIzq" >
					  Ofertas pre-aprobadas<br><%=ProbabilidadPreAprobado%>%
					</td>
					
					<td>&nbsp;</td>
						<%
						'A�adimos los pre aprobados mes a mes
						for i=1 to 12%>
						 <td align=right>
							N: <%FormatoEuros(arrCantPreAprobado(i)) - arrCantPreAprobadoGasto(i)%>
							<br>
							B: <%FormatoEuros(arrCantPreAprobado(i))%>
							<br>
							<%=round(((arrCantPreAprobado(i)- arrCantPreAprobadoGasto(i) + arrCantPedidos(i))*100)/(GastoPrevistoMes),2)%> %
						 </td>
						<%next%>
					<td>&nbsp;</td>
					<td class="colDer1" >
					  N: <%FormatoEuros(TotalPreAprobado-TotalPreAprobadoGasto)%><br />
					  B: <%FormatoEuros(TotalPreAprobado)%>
					</td>
					<td class="colDer2" >
						<%
						TotalHastaPreaprobados=TotalPedidos+((TotalPreAprobado-TotalPreAprobadoGasto)*ProbabilidadPreaprobado/100)
						'IF TotalHastaPreaprobados<GastoPrevistoMes*12 THEN
						'  ColorLetra=" style=""color:#FF6600!important;"" "
						'END IF%>
					  	<%FormatoEuros(TotalHastaPreaprobados)%>
					  	<br /><%=round(((TotalHastaPreaprobados)*100)/(GastoPrevistoMes*12),2)%>%
					</td>
					<td class="colDer3" >
						<%'IF TotalHastaPreaprobados<GastoPrevistoMes*mesActual THEN
						  'ColorLetra=" style=""color:#FF6600!important;"" "
						'END IF%>
					  	<%=round(((TotalHastaPreaprobados)*100)/(GastoPrevistoMes*mesActual),2)%>%
					</td>
				</tr>
				
				<tr>
					<td class="colIzq" >
					  <%=nomProbAlta%><br><%=ProbabilidadGrado1%>%
					</td>
					<td>&nbsp;</td>
					<%
					'A�adimos los grados 1 mes a mes
					for i=1 to 12%>
					 <td align=right>
						<%FormatoEuros(arrCantGrado1(i))%>
						<br>
						&nbsp;<%'=round(((arrCantGrados1(i))*100)/(GastoPrevistoMes*mesActual),2)%>
					 </td>
					<%next%>
					<td>&nbsp;</td>
					<td class="colDer1">
					  <%FormatoEuros(TotalGrado1)%>
					</td>
					<td class="colDer2" >
						<%
						TotalHastaGrado1=TotalHastaPreaprobados+(TotalGrado1*ProbabilidadGrado1/100)
						'IF TotalHastaGrado1<GastoPrevistoMes*12 THEN
						'  ColorLetra="#cc0000"
						'END IF%>
						<%FormatoEuros(TotalHastaGrado1)%>
					  	<br><%=round((TotalHastaGrado1*100)/(GastoPrevistoMes*12),2)%>%
					</td>
					<td class="colDer3" >
						<%'IF TotalHastaGrado1<GastoPrevistoMes*mesActual THEN
						  'ColorLetra="#cc0000"
						'END IF%>
                        <%=round((TotalHastaGrado1*100)/(GastoPrevistoMes*mesActual),2)%>%
					</td>
				</tr>
			
				<tr>
			
					<td class="colIzq" >
					  <%=nomProbMedia%><br><%=ProbabilidadGrado2%>%
					</td>
					<td>&nbsp;</td>
					<%'A�adimos los grados 2 mes a mes
					for i=1 to 12%>
					 <td align=right>
						<%FormatoEuros(arrCantGrado2(i))%>
						<br>
						&nbsp;<%'=round(((arrCantGrados1(i))*100)/(GastoPrevistoMes*mesActual),2)%>
					 </td>
					<%next%>
					<td>&nbsp;</td>
					<td class="colDer1">
					  <%FormatoEuros(TotalGrado2)%>
					</td>
					<td class="colDer2" >
						<%
						TotalHastaGrado2=TotalHastaGrado1+(TotalGrado2*ProbabilidadGrado2/100)
						'IF TotalHastaGrado2<GastoPrevistoMes*12 THEN
						'  ColorLetra=" style=""color:#FF6600!important;"" "
						'END IF%>
					  	<% FormatoEuros(TotalHastaGrado2)%>
					  	<br><%=round((TotalHastaGrado2*100)/(GastoPrevistoMes*12),2)%>%
					</td>
					<td class="colDer3" >
						<%'IF TotalHastaGrado2<GastoPrevistoMes*mesActual THEN
						  'ColorLetra="#cc0000"
						'END IF%>
					  	<%=round((TotalHastaGrado2*100)/(GastoPrevistoMes*mesActual),2)%>%
					</td>
				</tr>
			
				<tr>
			
					<td class="colIzq" >
					  <%=nomProbBaja%><br><%=ProbabilidadGrado3%>%
					</td>
					<td>&nbsp;</td>
					<%'A�adimos los grados 3 mes a mes
					for i=1 to 12%>
					 <td align=right>
						<%FormatoEuros(arrCantGrado3(i))%>
						<br>
						&nbsp;<%'=round(((arrCantGrado3(i))*100)/(GastoPrevistoMes*mesActual),2)%>
					 </td>
					<%next%>
					<td>&nbsp;</td>
					<td class="colDer1">
					  <%FormatoEuros(TotalGrado3)%>
					</td>
					<td  class="colDer2" >
						<%
						TotalHastaGrado3=TotalHastaGrado2+(TotalGrado3*ProbabilidadGrado3/100)
						'IF TotalHastaGrado3<GastoPrevistoMes*12 THEN
						'  ColorLetra="#cc0000"
						'END IF%>
						  <%
                          FormatoEuros(TotalHastaGrado3)%>
                          <br><%=round((TotalHastaGrado3*100)/(GastoPrevistoMes*12),2)%>%
					</td>
					<td  class="colDer3">
						<%'IF TotalHastaGrado3<GastoPrevistoMes*mesActual THEN
						  'ColorLetra="#cc0000"
						'END IF%>
					  	<%=round((TotalHastaGrado3*100)/(GastoPrevistoMes*mesActual),2)%>%
					</td>
				</tr>
			</table> <%
			
		rstVentas.cerrar
		rstComerciales.cerrar			
end sub 'Fin de Sub cargarTablaObjetivos(strTipo)


%>

<script language=javascript>
    function recarga() {
        tmp2 = document.forms.fPaginacion2.comboVerAnos;
        document.forms.fPaginacion2.comboVerAnos.value = tmp2.options[tmp2.selectedIndex].value;
        document.location = 'ResumenVentas.asp?an=' + tmp2.options[tmp2.selectedIndex].value;
        //}
    }
</script>


	<style type="text/css" >
		.tabObjAreas {  margin-left:10px; clear:both; margin-bottom:20px; }
		.tabObjAreas th { background:#CDCDCD; height:25px; padding:2px 10px 2px 10px; text-align:center;  }
		.tabObjAreas td { text-align:center; height:25px; min-width:160px; padding:2px 10px 2px 10px;}		
		
		.titBloquesH1 { font-size:13px;  }
		
		.litPantObj {/* font-weight:bold;*/ font-family:Verdana, Geneva, sans-serif; margin-left:30px;}
		
		.tabObjetivos { font-family:Verdana, Geneva, sans-serif; font-size:11px; }
		.tabObjetivos th { text-align:center; font-size:12px; font-weight:bold; background:#EEEEEE; height:20px; }
		.tabObjetivos td { vertical-align:top;    }
		.tabSinPadding { padding-left:0; padding-right:0;  /*min-width:40px; width:auto; max-width:100px;*/}

		
		.colSep { /*border-left:solid 2px gray!important;*/}
		
		.colNeg { font-weight:bold;}
		.colDer { text-align:right;}
		
		.colIzq { color:#ffffff;  text-align:left; background:#FF6600; vertical-align:middle!important; /*width:200px;*/ padding-right:10px; padding-left:10px; font-weight:bold; min-width:160px;   }		 
		.colDer1 { color:#0; text-align:right; background:#CAD5DF;  padding-right:10px; padding-left:10px;  border-left:solid 2px gray!important; min-width:80px;}		 
		.colDer2 {color:#0;  text-align:right; background:#CACAFF;  padding-right:10px; padding-left:10px;  border-left:solid 2px gray!important; min-width:80px; }		 
		.colDer3 {color:#0;  text-align:right; background:#FFE8FF!important;  padding-right:10px; padding-left:10px;  border-left:solid 2px gray!important; border-right:solid 1px #CDCDCD; min-width:80px;}		 
		
		.lineaSepCelda {width:100%; float:left; clear:both; margin:4px 0 0 0; padding:0; height:2px; border-top:solid 2px gray;}
		
		.capaFiltrosIzq {float:left;   padding:10px 40px 10px 20px; }
		.capaFiltrosDer {float:left;  padding:10px 20px 10px 20px; border-left:dashed 1px #CDCDCD; }		
		
		.capaAreaInterior { padding:0; margin:0; float:left; width:100%; clear:both;  padding:2px 0 2px 0; }
		.capaAreaInterior1 { background:#ffffff; }
		.capaAreaInterior2 { background:#CDCDCD; }	
		

		/*.subCapaIntIzqPeq { width:40px; }*/

/*		.subCapaIntIzq { float:left; text-align:right; }*/

		.subCapaIntIzq {  float:left;  font-size:9px; margin:0; padding:0; text-align:right;  }
		.subCapaIntDer { float:right; width:30px; border-left:solid 1px black; font-size:8px; margin:0; padding:0 2px 0 0; text-align:right; }

		
		.ColorLetra { color:red!important;}
		
	</style>
    

    
    

    <!-- CABECERA DE LA SELECCION -->
    <h1>Resumen OBJETIVO DE VENTAS SIN CONTAR ISP</h1>


                <div class="DatosResumen">
                
                	<div class="capaFiltrosIzq" >
						<%'############################################################################################%>
                        <%'Filtro de A�os%>
                        <form name="fPaginacion2" method="POST" ID="Form1">
                        <strong>Seleccionar a�o para obtener los objetivos:</strong><br /><br />
                        <span class="litPantObj">A�o:</span>
                        <select name="comboVerAnos" onchange="javascript:recarga();" class="combos" ID="Select1" style="min-width:100px;">
                            <%sql="Select distinct(year(ACCI_FECHA)) as Ano from ACCIONES_CLIENTE " 		
                            sql=sql & " order by year(ACCI_FECHA) desc "
                            Set rstAnos= connCRM.AbrirRecordset(sql,3,1)%>
                            <%while not rstAnos.eof%>
                                <option value="<%=rstAnos("Ano")%>"<%if cint(anoActual) = rstAnos("Ano") then response.Write(" selected")%>><%=rstAnos("Ano")%></option>
                                <%rstAnos.movenext
                            wend
                            rstAnos.cerrar%>
                            <option value="<%=year(date())+1%>" <%if cint(anoActual) = year(date())+1 then response.Write(" selected")%>><%=year(date())+1%></option>	
                            
                        </select>
                        </form>
                        <%'############################################################################################%>                       
                    </div>
                    <div class="capaFiltrosDer"  >
                        <strong>Objetivos de <%=anoActual%> por �reas de negocio:</strong><br /><br />                    
                        <table class="tabObjAreas" cellspacing="0" cellpadding="0" bordercolor="#C0C0C0" border="0" frame="border" rules="all">
                            <!--<tr><th colspan="<%=contObjAreas%>">Objetivos de <%=anoActual%> por �reas de negocio</th></tr>-->
                            <tr>
                            	<th>Periodo</th>
								<%=cabObjAreas%>
                                <th>TOTAL</th>
                            </tr>
                            <tr>
                            	<td>Objetivo ANUAL</td>
	                            <%=colObjAreas%>
                                <td><%=formatnumber(objAnual,2)%> �</td>
                            </tr>        
                            <tr>
                            	<td>Objetivo MENSUAL</td>                            	
	                            <%=colObjAreasMensual%>
                                <td><%=formatnumber(GastoPrevistoMes,2)%> �</td>
                            </tr>                                    
                        </table>                       
                    </div>
                
                </div>



				<div class="separador" style="height:20px;">&nbsp;</div> 
    
                <%'Tabla de objetivos (NETA)
					cargarTablaObjetivos(1)	%>		
                    
                    
				<div class="separador" style="height:20px;">&nbsp;</div> 
                                    
                <%'Tabla de objetivos (BRUTO)
					cargarTablaObjetivos(2)	%>		                    

        
		<div class="separador">&nbsp;</div>

<%
 %>
<%'############################################################################################%>
<%'############################################################################################%>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->