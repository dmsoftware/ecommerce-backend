<% Response.Buffer =false %>
<%'El nombre del fichero de idiomas es, por ejemplo, Idiomas_Comercial.xml, en la variable ponemos s�lo el men� al que pertenece la p�gina
'S�lo usamos esta variable si la p�gina no est� enganchada en ning�n men� (app_menus)
nomFicheroIdiomas="Comercial"%>
<!--include virtual="/dmcrm/includes/permisos.asp"-->
<!--#include virtual="/dmcrm/conf/conf.asp"-->
<!--#include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!--#include virtual="/dmcrm/APP/comun2/Validaciones.asp"  -->

<html>
	<head>
		<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
		<link rel="stylesheet" href="/dmcrm/css/estilos.css">
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">

        
	</head>
	<body  class="fondoPagIntranet" onLoad="quitarScroolPadre();">


<!--#include virtual="/dmcrm/includes/jquery.asp"  -->
<script type="text/javascript">

			$(document).ready(function() {
				
					$(".cEmergente").live('click', function(){
						$(this).fancybox({
							'imageScale'			: true,
							'zoomOpacity'			: true,
							'overlayShow'			: true,
							'overlayOpacity'		: 0.7,
							'overlayColor'			: '#333',
							'centerOnScroll'		: true,
							'zoomSpeedIn'			: 600,
							'zoomSpeedOut'			: 500,
							'transitionIn'			: 'elastic',
							'transitionOut'			: 'elastic',
							'type'					: 'iframe',
							'frameWidth'			: '100%',
							'frameHeight'			: '100%',
							'titleShow'		        : false
						
						}).trigger("click"); 
						return false; 	
					});
					
					$(".cEmergenteContactoRapido").live('click', function(){
						$(this).fancybox({
							'imageScale'			: true,
							'zoomOpacity'			: true,
							'overlayShow'			: true,
							'overlayOpacity'		: 0.7,
							'overlayColor'			: '#333',
							'centerOnScroll'		: true,
							'zoomSpeedIn'			: 600,
							'zoomSpeedOut'			: 500,
							'transitionIn'			: 'elastic',
							'transitionOut'			: 'elastic',
							'type'					: 'iframe',
							'frameWidth'			: '100%',
							'frameHeight'			: '100%',
							'titleShow'		        : false,
							'onClosed'		        : function() {
							    recargariFrame();
						}							
						
						}).trigger("click"); 
						return false; 	
					});					
					
			
					

					$('.VerDesc').click(function () { 
						$('#DescripCampaAct').toggle('slow');
					});
					$('.VerAnt').click(function () { 
						$('#DescripCampaAnt').toggle('slow');
					});
					
					/*movimiento del circulo de los pasos*/					
					$('li.paso').mouseenter(function(){
						var valorP = $(this).attr('rel');
						$('#'+valorP).css('display','block');
					}).mouseleave(function(){
						var valorP = $(this).attr('rel');
						$('#'+valorP).css('display','none');
					});
					
					/*click en los pasos*/
					$('li.paso').click(function () { 
						$('li.paso').removeClass('activo');
						$(this).addClass('activo');
					});
					
					$('#btnMostrarEdicionCamp').click(function () { 
						$('#divEditarCampana').toggle('slow');
					});
					$('#btnCancelarCampana').click(function () { 
						$('#divEditarCampana').toggle('slow');
					});					
					$('#btnMostrarEdicionProy').click(function () { 
						$('#divEditarProyecto').toggle('slow');
					});
					$('#btnCancelarProyecto').click(function () { 
						$('#divEditarProyecto').toggle('slow');
					});					
			
			

			});


function anadirContacto(codCliente,posCodigo)
{
	var j1 = document.getElementById("mostrarContactoRapido");  
//	 j1.href = "/dmcrm//APP/comun2/formMantenimiento.asp?modo=alta&posCodigo=" + posCodigo + "&tabla=CONTACTOS&filtro=CON_SUCLIENTE&valorfiltro=" + codCliente;  


	 j1.href = "/dmcrm/APP/comun2/formMantenimiento.asp?modo=alta&tabla=CONTACTOS&insertarContacto=1&filtro=CON_SUCLIENTE&valorfiltro=" + codCliente;  

//Y:\APP\Comercial\Empresas
	$('#mostrarContactoRapido').trigger('click'); 
}

/*function imputarHorasAccion(codAccion)
{
		var enlaceOculto = document.getElementById("mostrarContactoRapido");  
		enlaceOculto.href = "/dmcrm/app/produccion/inicio/SeleccionHorasAccionesComerciales.asp?conexion=conexionHoras&tabla=HORAS_ACCIONESCOMERCIALES&filtro=HAC_SUACCIONCOMERCIAL&valorfiltro=" + codAccion;  
		$('#mostrarContactoRapido').trigger('click'); 
}*/


function recargariFrame()
{
	var srcFrame=document.getElementById("datosAccion").src;
	document.getElementById("datosAccion").src=srcFrame;
}

function recargarPagina(modo,codAccion,codCliente,codCampana,tipoAccion,posCodigo,tabla,filtro,valorFiltro,existe,accionAsociadaA)
{
	if (existe=="1")
	{
		document.getElementById("datosAccion").src="/dmcrm/APP/Comercial/Empresas/clientes_AccionesDatos.asp?posCodigo=" + posCodigo + "&codAccion=" + codAccion + "&codCliente=" + codCliente + "&codCampana=" + codCampana + "&modo=" + modo + "&tabla=" + tabla + "&filtro=" + filtro + "&valorFiltro=" + valorFiltro + "&tipoAccion=" + tipoAccion + "&accionAsociadaA=" + accionAsociadaA;	
	}
	else
	{
		document.getElementById("datosAccion").src="/dmcrm/APP/Comercial/Empresas/clientes_AccionesDatos.asp?posCodigo=" + posCodigo + "&codCliente=" + codCliente + "&codCampana=" + codCampana + "&modo=alta&tabla=" + tabla + "&filtro=" + filtro + "&valorFiltro=" + valorFiltro + "&tipoAccion=" + tipoAccion + "&accionAsociadaA=" + accionAsociadaA;
		
     

	}

	
}

function editarCampanaProyecto(codigo,modo,tabla,filtro,valorfiltro,codCampanaViejo,proyectoViejo,tipoAccion,codCliente,tipoEdicion,posCodigo)
{

	document.mtoAccionesCliente.action="clientes_Acciones.asp?posCodigo=" + posCodigo + "&codigo=" + codigo + "&modo=" + modo + "&tabla=" + tabla + "&filtro=" + filtro + "&valorFiltro=" + valorfiltro + "&codCliente=" + codCliente + "&codCampana=" + codCampanaViejo + "&proyecto=" + proyectoViejo + "&tipoAccion=" + tipoAccion + "&" + tipoEdicion + "=1";
	

	document.mtoAccionesCliente.submit();	
	
}

		function quitarScroolPadre()
		{
			window.parent.document.body.style.overflow="hidden";
		}


function cargarTipoAccion()
{
	if (document.getElementById("cboNuevoCliente")!=null && document.getElementById("cboNuevoContacto")!=null)
	{
		if (document.getElementById("cboNuevoCliente").value=="" && document.getElementById("cboNuevoContacto").value=="")
		{
			alert("<%=objIdioma.getIdioma("clientes_Acciones_Texto6")%>");
		}
		else
		{
			document.mtoElegirModoAccion.submit();	
		}

	}

}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
	  return text;
}						   
</script>
<style type="text/css">
.recuadroTitulo {float:left; width:100%; clear:both; display:inline-block; }
.tituloCuadro {float:left; width:100%; margin:0px; padding:0px; height:20px;  background-color:#C0C0C0; font-weight:bold; margin-bottom:5px;}
.Contenidocuadro {}
</style>
<%
'PARAMETROS DE LA PAGINA
codigo=REQUEST("CODIGO")
tabla=REQUEST("TABLA")
modo=REQUEST("MODO")

posCodigo=request("posCodigo")
'conexion=request("conex")'session(denominacion & "_" & request("conex"))
retorno= REQUEST("RET") 'PAGINA DE RETORNO DEL MANTENIMIENTO, SI NO SE PONE, VUELVE A SELECCION.ASP

'el caracter & para retorno:
retorno = replace(retorno,"&",server.URLEncode("&"))


Filtro=REQUEST("FILTRO") 'SI ME ENVIAN CAMPO DE FILTRO, NO CREO CAJA DE TEXTO PARA EL.
ValorFiltro=REQUEST("VALORFILTRO")
Filtro2=REQUEST("FILTRO2") 'SI ME ENVIAN CAMPO DE FILTRO, NO CREO CAJA DE TEXTO PARA EL.
ValorFiltro2=REQUEST("VALORFILTRO2")
NumMax=REQUEST("NumMax")
NumMin=REQUEST("NumMin")
esPrimero=request("esPrimero")

esCliente=false
esContacto=false
if trim(lcase(tabla))="clientes"  Or ( trim(lcase(tabla))="acciones_cliente" And trim(filtro)="ACCI_SUCLIENTE" ) then
	esCliente=true
	esContacto=false
elseif 	 trim(lcase(tabla))="contactos"  Or ( trim(lcase(tabla))="acciones_cliente" And trim(filtro)="ACCI_SUCONTACTO" )  then
	esCliente=false
	esContacto=true
elseif  trim(lcase(tabla))="acciones_cliente" And trim(filtro)="" then
	if trim(request.Form("cboNuevoCliente"))<>"" And trim(request.Form("cboNuevoContacto"))="" then
		esCliente=true
		esContacto=false
		%><input type="hidden" name="cboNuevoClienteDatos" id="cboNuevoClienteDatos"  value="<%=trim(request.Form("cboNuevoCliente"))%>" /><%
	elseif trim(request.Form("cboNuevoCliente"))="" And trim(request.Form("cboNuevoContacto"))<>"" then
		esCliente=false
		esContacto=true
		%><input type="hidden" name="cboNuevoContactoDatos" id="cboNuevoContactoDatos"  value="<%=trim(request.Form("cboNuevoContacto"))%>" /><%		
	end if
end if



if trim(lcase(tabla))="acciones_cliente" And trim(filtro)="" And trim(lcase(modo))="alta" And esPrimero="1" then

%>	
	<form id="mtoElegirModoAccion" name="mtoElegirModoAccion" runat="server" action="clientes_Acciones.asp?modo=<%=modo%>&tabla=<%=tabla%>&filtro=<%=filtro%>&valorfiltro=<%=valorfiltro%>&filtro2=<%=filtro2%>&valorfiltro2=<%=valorfiltro2%>&nummax=<%=nummax%>&nummin=<%=nummin%>&cabecera=<%=Request("Cabecera")%>" method="post"  >
    
	
    	<table  cellpadding="0" cellspacing="0" border="0" style="width:100%; height: 100%; float:left; top:0px; left:0px; position:absolute; "><tr><td valign="middle" align="center">
        	<div style="width:500px; height:350px; border:solid 1px #ff6600; font-family:Arial; font-size:14px; padding:50px;">
            	<span style=" float:left; font-weight:bold;"><%=objIdioma.getIdioma("clientes_Acciones_Texto1")%></span><br/><br/><br/>
                <span style=" float:left; "><%=objIdioma.getIdioma("clientes_Acciones_Texto2")%></span><br/><br/>
                
                <span style="width:100%; clear:both; float:left; margin:0; margin-bottom:30px; margin-top:30px; padding:0; height:1px; border-top:dashed 1px #ff6600;"></span>
                
                <span style=" width:100px; text-align:right; float:left; margin-right:10px;"><img src="/dmcrm/APP/imagenes/imgEmpresa.gif" title="<%=objIdiomaGeneral.getIdioma("AccionA_Texto2")%>"  />&nbsp;&nbsp;<%=objIdioma.getIdioma("clientes_Acciones_Texto3")%></span>
                              <input tabindex="2" type="hidden" name="cboNuevoCliente" id="cboNuevoCliente" size=5 value="" class="cajaTextoModal relleno InpVerde"/>
                              <img style="float:left;" src="/dmcrm/APP/Comun2/imagenes/iconos/dmayuda.gif" width="15" title="<%=objIdiomaGeneral.getIdioma("InformacionAutocompletar_Texto1")%>" />
                              <input tabindex="1" type="text" style="float:left; width:60%;" name="txtDescripNuevoCliente_query" id="txtDescripNuevoCliente_query"  value="" class="cajaTextoModal relleno InpRojo" />
                              <img class="pepelera" style="float:left; margin:0 0 0 5px;" src="/dmcrm/APP/Comun2/imagenes/delete.gif" width="15" />

	                          <script language="javascript">
	                                $(document).ready(function () {							  
	                                jQuery(function () {									
	                                    $('#txtDescripNuevoCliente_query').autocomplete({
	                                        serviceUrl: '<%="/dmcrm/APP/Comun2/ajaxAutocompletar.asp?tabla=CLIENTES&claveExterna=CLIE_CODIGO&DescripcionExterna=CLIE_NOMBRE&id=" & encode(year(date) & "935" & month(date) & day(date))%>',
	                                        minChars: 2,
	                                        delimiter: /(,|;)\s*/, // regex or character
	                                        maxHeight: 400,
	                                        width: 300,
	                                        zIndex: 9999,
											onChangeInterval: 300,
	                                        deferRequestBy: 0, //miliseconds
	                                        noCache: false, //default is false, set to true to disable caching
	                                        onSelect: function (value, data) {
														$('#cboNuevoCliente').val(data);
														$('#txtDescripNuevoCliente_query').val(value);
														$('#cboNuevoContacto').val("");
														$('#txtDescripNuevoContacto_query').val("");														
														$('.relleno').removeClass('InpRojo');
														$('.relleno').addClass('InpVerde');
	                    		                    }
	                                    });
										$('#txtDescripNuevoCliente_query').blur(
											function () {
												setTimeout(function(){
													$.ajax({
															type: "POST",
															url: "/dmcrm/APP/Comun2/ajaxAutocompletarObtenerCodigo.asp",
															data: "tabla=CLIENTES&claveExterna=CLIE_CODIGO&DescripcionExterna=CLIE_NOMBRE&id=<%=encode(year(date) & "935" & month(date) & day(date))%>&contenidoDescripcion=" + escape(replaceAll($('#txtDescripNuevoCliente_query').val(),"+","|plus|")),
															success: function(datos) {
																if (datos==(-1))
																{
																	$('#cboNuevoCliente').val("");
																	$('#txtDescripNuevoCliente_query').val("");
																	$('.relleno').removeClass('InpVerde');
																	$('.relleno').addClass('InpRojo');
																}else 
																{
																	$('#cboNuevoCliente').val(datos);
																	$('.relleno').removeClass('InpRojo');
																	$('.relleno').addClass('InpVerde');
																}
																			
															}
													});
													},1000)
											});		
	                                });
									

									$('.pepelera').click(function () { 
										$('#cboNuevoCliente').val("");
										$('#txtDescripNuevoCliente_query').val("");
										$('.relleno').removeClass('InpVerde');
										$('.relleno').addClass('InpRojo');
									});
									$(".relleno").keypress(function() {
									  	$('.relleno').removeClass('InpVerde');
										$('.relleno').addClass('InpRojo');
										$('#cboNuevoCliente').val("");
									});
                				});						
	                             </script>  	                            	
                <br/><br/>
                
                <span style="width:100%; clear:both; float:left; margin:0; margin-bottom:30px; margin-top:30px; padding:0; height:1px; border-top:dashed 1px #ff6600;"></span>                
                <span style=" width:100px; text-align:right; float:left; margin-right:10px;"><img src="/dmcrm/APP/imagenes/imgPersona.gif" title="<%=objIdiomaGeneral.getIdioma("AccionA_Texto1")%>"  />&nbsp;&nbsp;<%=objIdioma.getIdioma("clientes_Acciones_Texto4")%></span>
                              <input tabindex="4" type="hidden" name="cboNuevoContacto" id="cboNuevoContacto" size=5 value="" class="cajaTextoModal relleno InpVerde"/>
                              <img style="float:left;" src="/dmcrm/APP/Comun2/imagenes/iconos/dmayuda.gif" width="15" title="<%=objIdiomaGeneral.getIdioma("InformacionAutocompletar_Texto1")%>" />
                              <input tabindex="3" type="text" style="float:left; width:60%;" name="txtDescripNuevoContacto_query" id="txtDescripNuevoContacto_query"  value="" class="cajaTextoModal relleno InpRojo" />
                              <img class="pepelera" style="float:left; margin:0 0 0 5px;" src="/dmcrm/APP/Comun2/imagenes/delete.gif" width="15" />

	                          <script language="javascript">
	                                $(document).ready(function () {							  
	                                jQuery(function () {
	                                    var a = $('#txtDescripNuevoContacto_query').autocomplete({
	                                        serviceUrl: '<%="/dmcrm/APP/Comun2/ajaxAutocompletar.asp?tabla=CONTACTOS&claveExterna=CON_CODIGO&DescripcionExterna=CON_NOMBRE&DescripcionExterna2=CON_APELLIDO1&DescripcionExterna3=CON_EMAIL&NumDescripciones=3&id=" & encode(year(date) & "935" & month(date) & day(date))%>',
	                                        minChars: 2,
	                                        delimiter: /(,|;)\s*/, // regex or character
	                                        maxHeight: 400,
	                                        width: 300,
	                                        zIndex: 9999,
	                                        deferRequestBy: 0, //miliseconds
	                                        noCache: false, //default is false, set to true to disable caching
	                                        onSelect: function (value, data) {
														$('#cboNuevoContacto').val(data);
														$('#txtDescripNuevoContacto_query').val(value);
														$('#cboNuevoCliente').val("");
														$('#txtDescripNuevoCliente_query').val("");
														$('.relleno').removeClass('InpRojo');
														$('.relleno').addClass('InpVerde');
	                    		                    }
	                                    });
										$('#txtDescripNuevoContacto_query').blur(
											function () {
												setTimeout(function(){
													$.ajax({
															type: "POST",
															url: "/dmcrm/APP/Comun2/ajaxAutocompletarObtenerCodigo.asp",
															data: "tabla=CONTACTOS&claveExterna=CON_CODIGO&DescripcionExterna=CON_NOMBRE&DescripcionExterna2=CON_APELLIDO1&DescripcionExterna3=CON_EMAIL&NumDescripciones=3&id=<%=encode(year(date) & "935" & month(date) & day(date))%>&contenidoDescripcion=" + escape(replaceAll($('#txtDescripNuevoContacto_query').val(),"+","|plus|")),
															success: function(datos) {
																if (datos==(-1))
																{
																	$('#cboNuevoContacto').val("");
																	$('#txtDescripNuevoContacto_query').val("");
																	$('.relleno').removeClass('InpVerde');
																	$('.relleno').addClass('InpRojo');
																}else 
																{
																	$('#cboNuevoContacto').val(datos);
																	$('.relleno').removeClass('InpRojo');
																	$('.relleno').addClass('InpVerde');
/*																	$('#cboNuevoCliente').val("");										
																	$('#txtDescripNuevoCliente_query').val("");				*/
																}
																			
															}
													});
													},1000)
											});		
	                                });
									

									$('.pepelera').click(function () { 
										$('#cboNuevoContacto').val("");
										$('#txtDescripNuevoContacto_query').val("");
										$('.relleno').removeClass('InpVerde');
										$('.relleno').addClass('InpRojo');
									});
									$(".relleno").keypress(function() {
									  	$('.relleno').removeClass('InpVerde');
										$('.relleno').addClass('InpRojo');
										$('#cboNuevoContacto').val("");
										$('#cboNuevoCliente').val("");										
										//$('#txtDescripNuevoCliente_query').val("");										
//										document.getElementById("txtDescripNuevoCliente_query").value="";
										
									});
                				});									
	                             </script> 
                                 
                   <br/><br/>
                <span style="width:100%; clear:both; float:left; margin:0; margin-bottom:30px; margin-top:30px; padding:0; height:1px; border-top:dashed 1px #ff6600;"></span>                   
                   <input type="button" id="btnCargarNuevo" name="btnCargarNuevo" value="<%=objIdioma.getIdioma("clientes_Acciones_Texto5")%>" class="boton" style="float:right;" onClick="cargarTipoAccion();" />                                            
            </div>
        </td></tr></table>
	</form>
<%

else 'else de if trim(lcase(tabla))="acciones_cliente" And trim(lcase(modo)))="alta" then


'if Instr(lcase(conexion),"access")>0 then
'	set conn = new DMWConexion
'	connCRM.abrir ("" & conexion)
'else
'	set conn = new DMWConexion
'	connCRM.abrirSQL ("" & conexion)
'end if

if trim(lcase(modo))="modificacion" then





	sql=" Select AC.ACCI_CODIGO,AC.ACCI_SUCAMPANA,AC.ACCI_SUCLIENTE,AC.ACCI_SUCONTACTO,AC.ACCI_PROYECTO,AC.ACCI_SUTIPO "
    sql=sql & " ,C.CAMP_NOMBRE,C.CAMP_DESCRIPCION "
    sql=sql & " From ( ACCIONES_CLIENTE AC "
    sql=sql & " Inner join CAMPANAS C ON (AC.ACCI_SUCAMPANA=C.CAMP_CODIGO) ) "
    sql=sql &  " Where ACCI_CODIGO=" & codigo

	Set rsAcciones = connCRM.AbrirRecordSet(sql,3,1)
	while not rsAcciones.eof

		if isnull(rsAcciones("ACCI_SUCLIENTE")) Or trim(rsAcciones("ACCI_SUCLIENTE"))="" then
			esCliente=false
			esContacto=true		
		else
			esCliente=true
			esContacto=false				
		end if

		campana=rsAcciones("CAMP_NOMBRE")
		proyecto=rsAcciones("ACCI_PROYECTO")	
		codCampana=rsAcciones("ACCI_SUCAMPANA")
		if esCliente then
			codCliente=rsAcciones("ACCI_SUCLIENTE")
		elseif esContacto then
			codCliente=rsAcciones("ACCI_SUCONTACTO")		
		end if	
		tipoAccion=rsAcciones("ACCI_SUTIPO")
		DescripCampana=rsAcciones("CAMP_DESCRIPCION")

	rsAcciones.movenext
	wend
	rsAcciones.cerrar()
	set rsAcciones=nothing 

	if esCliente then
		sqlTipoRegistro=" Select CL.CLIE_CODIGO,CL.CLIE_NOMBRE,CL.CLIE_TELEFONO,TCR_DESCRIPCION "
    	sqlTipoRegistro=sqlTipoRegistro & " From ( CLIENTES CL "
	    sqlTipoRegistro=sqlTipoRegistro & " Left Join TIPOS_CALIDADREGISTRO CR ON (CL.CLIE_CALIDADREGISTRO=CR.TCR_CODIGO) ) "
    	sqlTipoRegistro=sqlTipoRegistro &  " Where CLIE_CODIGO=" & codCliente
		Set rsTipoRegistro = connCRM.AbrirRecordSet(sqlTipoRegistro,3,1)
		if not rsTipoRegistro.eof then
			cliente=rsTipoRegistro("CLIE_NOMBRE")
			telefonoCli=rsTipoRegistro("CLIE_TELEFONO")
			'Modificamos el texto de la imagen, para poner el terxto como title.
			if instr(rsTipoRegistro("TCR_DESCRIPCION"),">")>0 then
				arrCalidadRegistro=split(rsTipoRegistro("TCR_DESCRIPCION"),">")
				calidadRegistro=arrCalidadRegistro(0) & " title=""" & arrCalidadRegistro(1) & """" & ">"
			else
				calidadRegistro=rsTipoRegistro("TCR_DESCRIPCION")
			end if		
		end if
		rsTipoRegistro.cerrar()
		set rsTipoRegistro=nothing 
	elseif esContacto then
		sqlTipoRegistro=" Select CON.CON_CODIGO,CON.CON_NOMBRE + ' ' + CON.CON_APELLIDO1 as CON_NOMBRE,CON.CON_EMAIL "
    	sqlTipoRegistro=sqlTipoRegistro & " From  CONTACTOS CON "
    	sqlTipoRegistro=sqlTipoRegistro &  " Where CON_CODIGO=" & codCliente
		Set rsTipoRegistro = connCRM.AbrirRecordSet(sqlTipoRegistro,3,1)
		if not rsTipoRegistro.eof then
			telefonoCli=rsTipoRegistro("CON_EMAIL")
			cliente=rsTipoRegistro("CON_NOMBRE")
		end if
		rsTipoRegistro.cerrar()
		set rsTipoRegistro=nothing 	
	end if
	

	if trim(request("editarCampana"))="1" then
		campanaNueva=request.Form("cboEditarCampana")
		sql=" Update ACCIONES_CLIENTE Set ACCI_SUCAMPANA=" & campanaNueva 
		sql=sql & " Where ACCI_SUCAMPANA=" & codCampana & " And ACCI_PROYECTO='" & proyecto & "' "
		if esCliente then
			sql=sql & " And ACCI_SUCLIENTE=" & codCliente		
		elseif esContacto then
			sql=sql & " And ACCI_SUCONTACTO=" & codCliente		
		end if
		

		connCRM.execute sql,intNumRegistros
		codCampana=campanaNueva
		
			sql=" Select C.CAMP_CODIGO,C.CAMP_NOMBRE,C.CAMP_DESCRIPCION "
		    sql=sql & " From CAMPANAS C "
		    sql=sql &  " Where C.CAMP_CODIGO=" & campanaNueva

			Set rsDescripCampana = connCRM.AbrirRecordSet(sql,3,1)
			while not rsDescripCampana.eof
				campana=rsDescripCampana("CAMP_NOMBRE")
				DescripCampana=rsDescripCampana("CAMP_DESCRIPCION")
				rsDescripCampana.movenext
			wend
			rsDescripCampana.cerrar()
			set rsDescripCampana=nothing
			
		%>	<script type="text/javascript">
			<%'if request("recarga") = "parent" then%>
				// parent.window.location.reload();
		         //parent.$.fancybox.close();
			<%'end if%>
			</script>		<%
	
	elseif trim(request("editarProyecto"))="1" then
		proyectoNuevo=request.Form("txtEditaProyecto")	
	
		sql=" Update ACCIONES_CLIENTE Set ACCI_PROYECTO='" & proyectoNuevo  & "'"
		sql=sql & " Where ACCI_SUCAMPANA=" & codCampana & " And ACCI_PROYECTO='" & proyecto & "' "
		if esCliente then
			sql=sql & " And ACCI_SUCLIENTE=" & codCliente
		elseif esContactto then
			sql=sql & " And ACCI_SUCONTACTO=" & codCliente		
		end if	
		
		connCRM.execute sql,intNumRegistros
		proyecto=proyectoNuevo
		
	end if	
	

	accionesMismaCampana=""
	sql=" Select AC.ACCI_CODIGO,AC.ACCI_Fecha,AC.ACCI_Comentario,AC.ACCI_SUTipo,AC.ACCI_SuCampana,AC.ACCI_PROYECTO,AC.ACCI_SUCONTACTO,AC.ACCI_CONTACTO,ANA.ANA_NOMBRE"
	sql=sql&" ,C.Camp_Nombre,T.TIAC_Codigo "
	sql=sql&" ,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TIAC_NOMBRE else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TIAC_NOMBRE "	
	sql=sql&" ,CON.CON_NOMBRE,CON.CON_APELLIDO1 "		
	sql=sql&" From ((((( ACCIONES_CLIENTE AC Inner Join CAMPANAS C on (AC.ACCI_SUCampana=C.CAMP_CODIGO)) "
	sql=sql&" Inner Join TIPOS_ACCION T on (AC.ACCI_SUTipo=T.TIAC_CODIGO)) "
	sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "')) "	
	sql=sql&" Left Join CONTACTOS CON on (AC.ACCI_SUCONTACTO=CON.CON_CODIGO)) "		
	sql=sql&" Left Join ANALISTAS ANA on (AC.ACCI_SUCOMERCIAL=ANA.ANA_CODIGO)) "			
	sql=sql&" Where  AC.ACCI_SuCampana=" & codCampana & " And AC.ACCI_PROYECTO='" & proyecto & "' "
	if esCliente then
		sql=sql&" And AC.ACCI_SuCliente=" & codCliente 
	elseif esContacto then 
		sql=sql&" And AC.ACCI_SuContacto=" & codCliente 	
	end if	
	
	sql=sql&" Order by C.CAMP_FINICIO DESC,C.Camp_Nombre ASC,ACCI_SUTipo ASC,AC.ACCI_Fecha DESC,T.TIAC_Nombre ASC "


	Set rsAccionesCampana = connCRM.AbrirRecordSet(sql,3,1)
	campannaAnterior=""
	proyectoAnterior=""
	while not rsAccionesCampana.eof
		estiloLink="color:dimgray;"
		if cstr(trim(rsAccionesCampana("ACCI_CODIGO")))=cstr(trim(codigo)) then
			estiloLink="color:green;"
		end if
		
			
		if (campannaAnterior="" Or rsAccionesCampana("ACCI_SuCampana")<>campannaAnterior) And  (proyectoAnterior="" Or rsAccionesCampana("ACCI_PROYECTO")<>campannaAnterior) then
			campannaAnterior=rsAccionesCampana("ACCI_SuCampana")
			proyectoAnterior=rsAccionesCampana("ACCI_PROYECTO")
			accionesMismaCampana2=accionesMismaCampana2 & "<span class=""TitCampa""><strong>" & objIdioma.getIdioma("clientes_Acciones_Texto7") & "</strong> " & rsAccionesCampana("Camp_Nombre") & "&nbsp;&nbsp;<strong>-</strong>&nbsp;&nbsp;<strong>" & objIdioma.getIdioma("clientes_Acciones_Texto8") & "</strong>&nbsp;" & rsAccionesCampana("ACCI_PROYECTO") & "</span>"
		end if
		
accionesMismaCampana=accionesMismaCampana & "<div id=""" & rsAccionesCampana("ACCI_SUTipo") & """ class=""datSegimiento"" >"
			parametros="'" & modo & "','" & rsAccionesCampana("ACCI_CODIGO") & "','" & codCliente & "','" & codCampana & "','" & rsAccionesCampana("TIAC_CODIGO") & "'"
			accionesMismaCampana=accionesMismaCampana & "<div class=""datoCampa"">" 
			if lcase(trim(modo))="alta" then
				accionesMismaCampana=accionesMismaCampana & accionesMismaCampana("TIAC_NOMBRE") 
			else
				accionesMismaCampana=accionesMismaCampana & "<span class=""FondoProp"">" & rsAccionesCampana("TIAC_NOMBRE") & "</span>"
			end if
		
			if esCliente then		
				if isnumeric(trim(rsAccionesCampana("ACCI_SUCONTACTO"))) And trim(rsAccionesCampana("ACCI_SUCONTACTO"))<>"0" And   trim(rsAccionesCampana("ACCI_SUCONTACTO"))<>"-1" then
					accionesMismaCampana=accionesMismaCampana & "<strong>" & objIdioma.getIdioma("clientes_Acciones_Texto9") & "</strong>&nbsp;" & rsAccionesCampana("CON_NOMBRE") & " " & rsAccionesCampana("CON_APELLIDO1") & ""			
				else
					if  trim(rsAccionesCampana("ACCI_CONTACTO"))<>"" then
						accionesMismaCampana=accionesMismaCampana & "<strong>" & objIdioma.getIdioma("clientes_Acciones_Texto9") & "</strong>&nbsp;" & rsAccionesCampana("ACCI_CONTACTO") & ""				
					end if
				end if		
			end if	
			
			accionesMismaCampana=accionesMismaCampana & "<br/><strong>" & objIdioma.getIdioma("clientes_Acciones_Texto18") & "</strong>&nbsp;" & rsAccionesCampana("ANA_NOMBRE") & "<br/>"			
			accionesMismaCampana=accionesMismaCampana & "<strong>" & objIdioma.getIdioma("clientes_Acciones_Texto10") & "</strong>&nbsp;" & rsAccionesCampana("ACCI_Comentario") & "<br/><br/></div>"
accionesMismaCampana=accionesMismaCampana & "</div>"						

	rsAccionesCampana.movenext
	wend
	rsAccionesCampana.cerrar()
	set rsAccionesCampana=nothing 
else 'else de if modificacion



		codCliente=ValorFiltro	
		if trim(codCliente)<>"" then
		
			if esCliente then
				sql=" Select CLIE_NOMBRE as NOMBRE "
		    	sql=sql & " From Clientes "
    			sql=sql &  " Where CLIE_CODIGO=" & codCliente
			elseif esContacto then
				sql=" Select CON_NOMBRE + ' ' + CON_APELLIDO1 as NOMBRE "
		    	sql=sql & " From Contactos "
    			sql=sql &  " Where CON_CODIGO=" & codCliente			
			end if	

			if trim(sql)<>"" then
				Set rsClientes = connCRM.AbrirRecordSet(sql,3,1)
				while not rsClientes.eof
					cliente=rsClientes("NOMBRE")
				rsClientes.movenext
				wend
				rsClientes.cerrar()
				set rsClientes=nothing 		
			end if	
		end if	
		
end if	

if trim(codCliente)<>"" then
	accionesRestoCampanas=""
	sql=" Select AC.ACCI_CODIGO,AC.ACCI_Fecha,AC.ACCI_Comentario,AC.ACCI_SUTipo,AC.ACCI_SuCampana,AC.ACCI_PROYECTO,AC.ACCI_SUCONTACTO,AC.ACCI_CONTACTO, ANA.ANA_NOMBRE "
	sql=sql&" ,C.Camp_Nombre,T.TIAC_Codigo  "
	sql=sql&" ,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TIAC_NOMBRE else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TIAC_NOMBRE "		
	sql=sql&" ,CON.CON_NOMBRE,CON.CON_APELLIDO1 "	
	sql=sql&" From ((((( ACCIONES_CLIENTE AC Inner Join CAMPANAS C on (AC.ACCI_SUCampana=C.CAMP_CODIGO)) "
	sql=sql&" Inner Join TIPOS_ACCION T on (AC.ACCI_SUTipo=T.TIAC_CODIGO)) "
	sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "')) "		
	sql=sql&" Left Join CONTACTOS CON on (AC.ACCI_SUCONTACTO=CON.CON_CODIGO)) "	
	sql=sql&" Left Join ANALISTAS ANA on (AC.ACCI_SUCOMERCIAL=ANA.ANA_CODIGO)) "				
	if esCliente then
		sql=sql&" Where AC.ACCI_SuCliente=" & codCliente 
	elseif esContacto then
		sql=sql&" Where AC.ACCI_SuContacto=" & codCliente 	
	end if
	if trim(cstr(codCampana))<>"" then
		sql=sql& " And AC.ACCI_SuCampana<>" & codCampana
	end if	
	sql=sql&" Order by C.CAMP_FINICIO DESC,C.Camp_Nombre ASC,ACCI_SUTipo ASC,AC.ACCI_Fecha DESC,T.TIAC_Nombre ASC "
	Set rsAccionesRestoCampanas = connCRM.AbrirRecordSet(sql,3,1)
	campannaAnterior=""
	proyectoAnterior=""

	while not rsAccionesRestoCampanas.eof

			if (campannaAnterior="" Or rsAccionesRestoCampanas("ACCI_SuCampana")<>campannaAnterior) And  (proyectoAnterior="" Or rsAccionesRestoCampanas("ACCI_PROYECTO")<>campannaAnterior) then			
				campannaAnterior=rsAccionesRestoCampanas("ACCI_SuCampana")
				proyectoAnterior=rsAccionesRestoCampanas("ACCI_PROYECTO")			
				accionesRestoCampanas=accionesRestoCampanas & "<br/><span class=""TitGris""><strong>" & objIdioma.getIdioma("clientes_Acciones_Texto7") & "</strong>&nbsp;" & rsAccionesRestoCampanas("Camp_Nombre") & "&nbsp;&nbsp;<strong>-</strong>&nbsp;&nbsp;<strong>" & objIdioma.getIdioma("clientes_Acciones_Texto8") & "</strong>&nbsp;" & rsAccionesRestoCampanas("ACCI_PROYECTO") & "</span>"			
			end if

			accionesRestoCampanas=accionesRestoCampanas & "<div class=""LineaGris""><span style=""font-weight:bold; "">" &  rsAccionesRestoCampanas("TIAC_NOMBRE") & "</span>&nbsp;(" & rsAccionesRestoCampanas("ACCI_Fecha") & ")" 

			if esCliente then
				if isnumeric(trim(rsAccionesRestoCampanas("ACCI_SUCONTACTO"))) And trim(rsAccionesRestoCampanas("ACCI_SUCONTACTO"))<>"0" And   trim(rsAccionesRestoCampanas("ACCI_SUCONTACTO"))<>"-1" then
					accionesRestoCampanas=accionesRestoCampanas & "<br/><strong>" & objIdioma.getIdioma("clientes_Acciones_Texto9") & "</strong>&nbsp;" & rsAccionesRestoCampanas("CON_NOMBRE") & " " & rsAccionesRestoCampanas("CON_APELLIDO1") & ""			
				else
					if  trim(rsAccionesRestoCampanas("ACCI_CONTACTO"))<>"" then
						accionesRestoCampanas=accionesRestoCampanas & "<br/><strong>" & objIdioma.getIdioma("clientes_Acciones_Texto9") & "</strong>&nbsp;" & rsAccionesRestoCampanas("ACCI_CONTACTO") & ""				
					end if
				end if
			end if	
			accionesRestoCampanas=accionesRestoCampanas & "<br/><strong>" & objIdioma.getIdioma("clientes_Acciones_Texto18") & "</strong>&nbsp;" & rsAccionesRestoCampanas("ANA_NOMBRE")  & "<br/>"			
			accionesRestoCampanas=accionesRestoCampanas & "<strong>" & objIdioma.getIdioma("clientes_Acciones_Texto10") & "</strong>&nbsp;" & rsAccionesRestoCampanas("ACCI_Comentario")  & "<br/></div>"

	rsAccionesRestoCampanas.movenext
	wend
	rsAccionesRestoCampanas.cerrar()
	set rsAccionesRestoCampanas=nothing 	
end if
	
	%>
<form id="mtoAccionesCliente" name="mtoAccionesCliente" runat="server" action="clientes_Acciones.asp" method="post"  >

<span class="TituGeneral"></span>

    			<!-- include virtual="/dmcrm/APP/Comercial/cargarNavegadorCodigos2.asp"-->
<%'else%>    
  <div style="width:100%; clear:both; float:left; text-align:left; padding:0px; margin:0 0 0 0; overflow:auto; ">
    	<div style="margin:0px; padding:0px; width:80%; float:left; text-align:left;"></div>
<div style="margin:0px; padding:0px; width:18%; float:right; text-align:left;">                    
    			<!-- #include virtual="/dmcrm/APP/Comun2/cargarNavegadorCodigos.asp"-->
</div>
</div>
<%'end if%>
 <input id="tipoAccion" name="tipoAccion" style="width:1px; height:1px; display:none;" value="tipoAccion" type="text" />
 <div  class="recuadroTitulo">
 	<div style="width:32.2%; float:left; display:inline; border:solid 1px #f60; padding:5px;   "> <!--border-right:none;-->
    	<%if esCliente then%>
			<img src="/dmcrm/APP/imagenes/imgEmpresa.gif"  title="<%=objIdiomaGeneral.getIdioma("AccionA_Texto2")%>"  />        
		    <strong><%=objIdioma.getIdioma("clientes_Acciones_Texto3")%>&nbsp;</strong>
        <%elseif esContacto then%>
			<img src="/dmcrm/APP/imagenes/imgPersona.gif"  title="<%=objIdiomaGeneral.getIdioma("AccionA_Texto1")%>"  />        
		    <strong><%=objIdioma.getIdioma("clientes_Acciones_Texto4")%>&nbsp;</strong>        
        <%end if%>
		
		<%if isnumeric(codCliente) then		
		
			nomTablaFormMant=""
			if esCliente then
				nomTablaFormMant="CLIENTES"			
			elseif esContacto then
				nomTablaFormMant="CONTACTOS"			
			end if
			if trim(nomTablaFormMant)<>"" then
				%>&nbsp;<a class="cEmergente" href="/dmcrm/APP/comun2/formMantenimiento.asp?codigo=<%=codCliente%>&modo=modificacion&tabla=<%=nomTablaFormMant%>&soloCampos=1&filtro=<%=Filtro%>&valorfiltro=<%=ValorFiltro%>&filtro2=<%=Filtro2%>&valorfiltro2=<%=ValorFiltro2%>&nummax=<%=NumMax%>&nummin=<%=NumMin%>&cabecera=<%=cabecera%>&recarga=parent" style="float:right; text-decoration:none;"><input type="button" id="btnMostrarEdicionCli" name="btnMostrarEdicionCli"  value="<%=objIdiomaGeneral.getIdioma("BotonEditar_Texto1")%>" class="boton" style="float:right;"   /></a>  	<%						
			end if	
		end if	

		'else
		if trim(codCliente)="" then

				if esCliente then
			        sql="select CLIE_CODIGO,CLIE_NOMBRE from Clientes "
                    sql=sql & " WHERE CLIE_CODIGO=" & trim(request.Form("cboNuevoCliente"))
				elseif esContacto then
			        sql="select CON_CODIGO as CLIE_CODIGO,CON_NOMBRE + ' ' + CON_APELLIDO1 as CLIE_NOMBRE from Contactos "
                    sql=sql & " WHERE CON_CODIGO=" & trim(request.Form("cboNuevoContacto"))
				end if

				if trim(sql)<>"" then
                    Set rsCombo = connCRM.AbrirRecordset(sql,0,1)
                    if not rsCombo.eof then
						codCliente=rsCombo("CLIE_CODIGO")
                        cliente=rsCombo("CLIE_NOMBRE")
                    end if				
				end if
				rsCombo.cerrar()
				set rsCombo=nothing	
		end if

		response.Write(cliente)	
		if 	trim(telefonoCli)<>"" then
			response.Write("(" & telefonoCli & ")")	
		end if	
		if esCliente then
			response.Write(" " & calidadRegistro)			
		end if	
		%>	
		<input  class="cajaTexto" type="text" value="" style=" visibility:hidden; width:1px;" />
    </div>
 	<div style="width:32.2%; float:left; display:inline; border:solid 1px #f60; padding:5px; position:relative; z-index:0;   "><!--border-right:none;-->
<%
				sql=" Select * "
				sql=sql&" From Campanas "
				sql=sql&" Order by CAMP_FINICIO DESC, CAMP_NOMBRE ASC "
				Set rsCampanas = connCRM.AbrirRecordSet(sql,3,1)				
%>	
	
    	<strong><%=objIdioma.getIdioma("clientes_Acciones_Texto7")%>&nbsp;</strong>
		<%if trim(lcase(modo))="modificacion" then		
			response.Write(campana)
			%>		&nbsp;<input type="button" id="btnMostrarEdicionCamp" name="btnMostrarEdicionCamp"  value="<%=objIdiomaGeneral.getIdioma("BotonEditar_Texto1")%>" class="boton" style="float:right;"   />  	
			 <input id="cboCampana" name="cboCampana" style="display:none;" type="text" style=" width:1px;"  value="<%=codCampana%>" /><%			
		else
			%>
            <select id="cboCampana" name="cboCampana" style="width:70%;">
	            <option value="0"  selected="false" ></option>
            	<%
				while not rsCampanas.eof
            	%><option value="<%=rsCampanas("CAMP_CODIGO")%>"    ><%=rsCampanas("CAMP_NOMBRE")%></option><%
					rsCampanas.movenext
				wend
				%>
            </select>
            <%
		end if%>
		<div id="divEditarCampana"  class="divEditar" >
            <select id="cboEditarCampana" name="cboEditarCampana" style="width:100%;">
            	<%
				while not rsCampanas.eof
            	%><option value="<%=rsCampanas("CAMP_CODIGO")%>"  <% if trim(cstr(codCampana))=trim(cstr(rsCampanas("CAMP_CODIGO"))) then%> selected="selected"<%end if%>  ><%=rsCampanas("CAMP_NOMBRE")%></option><%
					rsCampanas.movenext
				wend
				%>
            </select>
				<br/><br/>
				<%paramEditar="'"&codigo&"','"&modo&"','"&tabla&"','"&filtro&"','"&valorFiltro&"','"&codCampana&"','"&proyecto&"','"&tipoAccion&"','"&codCliente&"','editarCampana','" & posCodigo & "'"%>
			   <input type="button" id="btnEditarCampana" name="btnEditarCampana"  value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton"  onClick="javascript:editarCampanaProyecto(<%=paramEditar%>);"  />         
			   <input type="button" id="btnCancelarCampana" name="btnCancelarCampana"  value="<%=objIdiomaGeneral.getIdioma("BotonCancelar_Texto1")%>" class="boton"  />         
		</div>
<%
				rsCampanas.cerrar()
				set rsCampanas=nothing 	
%>											
    </div>    
 	<div style="width:32.1%; float:left; display:inline; border:solid 1px #f60; padding:5px; position:relative; z-index:0;">
    	<strong><%=objIdioma.getIdioma("clientes_Acciones_Texto8")%>&nbsp;</strong>
		<%if trim(lcase(modo))="modificacion" then		
			response.Write(proyecto)
			%>		&nbsp;<input type="button" id="btnMostrarEdicionProy" name="btnMostrarEdicionProy"  value="<%=objIdiomaGeneral.getIdioma("BotonEditar_Texto1")%>" class="boton"  style="float:right;" />  	
			           <input id="txtProyecto" name="txtProyecto" style="display:none;" type="text" style=" width:1px;"  value="<%=proyecto%>" /><%
		else
			%>
           <input id="txtProyecto" name="txtProyecto"  class="cajaTexto" type="text" value="" style=" width:70%;" />
            <%
		end if%>	
		<div id="divEditarProyecto"  class="divEditar"  >
			<input id="txtEditaProyecto" name="txtEditaProyecto"  class="cajaTexto" type="text" value="<%=proyecto%>" style=" width:100%;" />
				<br/><br/>
				<%paramEditar="'"&codigo&"','"&modo&"','"&tabla&"','"&filtro&"','"&valorFiltro&"','"&codCampana&"','"&proyecto&"','"&tipoAccion&"','"&codCliente&"','editarProyecto','" & posCodigo & "'"%>				
			   <input type="button" id="btnEditarProyecto" name="btnEditarProyecto"  value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton" onClick="javascript:editarCampanaProyecto(<%=paramEditar%>);"   />         
			   <input type="button" id="btnCancelarProyecto" name="btnCancelarProyecto"  value="<%=objIdiomaGeneral.getIdioma("BotonCancelar_Texto1")%>" class="boton"  />         
		</div>
				
    </div>    
    <br/>
 </div>
 <br/>


<%if trim(lcase(modo))="modificacion" then %>
<div style="width:49%; float:left; margin:0 10px 0 0;">

    <div class="grafico">
		<%=accionesMismaCampana2%>
        <p style="text-align:right; margin:0; padding:0 20px;"><br><span class="VerDesc"><%=objIdioma.getIdioma("clientes_Acciones_Texto11")%></span> | <span class="VerAnt"><%=objIdioma.getIdioma("clientes_Acciones_Texto12")%></span></p>
        
        <ul id="pasosGrafico">    
        <%'listado de poisiciones posibles
        'class="paso cerrado"--> Verde No Activo
        'class="paso cerrado"--> Naranja No Activo
        'class="paso cerrado activo" --> Verde Activo
        'class="paso pendiente activo" --> Naranja Activo
    
		tieneRepetidos=false
		Pintados=""
        tipoAnterior=""
        sql=" Select TIAC_CODIGO,ACCI_CODIGO,ACCI_FECHA,ACCI_Terminada,ACCI_SUTIPO,ACCI_Codigo "	
		sql=sql&" ,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then  TA.TIAC_NOMBRE else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TIAC_NOMBRE "			
        sql=sql&" From ((TIPOS_ACCION TA "
        sql=sql&" Inner Join ACCIONES_CLIENTE AC on  TA.TIAC_CODIGO=AC.ACCI_SUTIPO) "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (TA.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "')) "			
		if esCliente then
	        sql=sql&" Where ACCI_SUCLIENTE=" & codCliente
		elseif esContacto then
	        sql=sql&" Where ACCI_SUCONTACTO=" & codCliente		
		end if	
        sql=sql&" and ACCI_SUCAMPANA=" & codCampana
        if trim(proyecto)<>"" and not isnull(proyecto)  then
            sql=sql&" and ACCI_PROYECTO='" & proyecto & "'" 
        else
            sql=sql&" and (LTRIM(RTRIM(ACCI_PROYECTO))='' Or ACCI_PROYECTO is null ) "
        end if
        sql=sql&" Order by ACCI_SUTIPO ASC, ACCI_FECHA ASC "
		
		
        Set rsTiposAcciones = connCRM.AbrirRecordSet(sql,3,1)
        while not rsTiposAcciones.eof
		    accionRepetida=""


            if trim(tipoAnterior)="" Or trim(tipoAnterior)<>rsTiposAcciones("ACCI_SUTIPO") then

				'''Obtener si la accion esta repetida
				sqlRepetido=" Select count(ACCI_SUTIPO) as contRepetida"	
		        sqlRepetido=sqlRepetido&" From (( TIPOS_ACCION TA "
			    sqlRepetido=sqlRepetido&" Inner Join ACCIONES_CLIENTE AC on  TA.TIAC_CODIGO=AC.ACCI_SUTIPO) "
				sqlRepetido=sqlRepetido & " Left Join IDIOMAS_DESCRIPCIONES D on (TA.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "')) "					
				if esCliente then
				    sqlRepetido=sqlRepetido&" Where ACCI_SUCLIENTE=" & codCliente
				elseif esContacto then
					sqlRepetido=sqlRepetido&" Where ACCI_SUCONTACTO=" & codCliente
				end if	
		        sqlRepetido=sqlRepetido&" and ACCI_SUCAMPANA=" & codCampana
		        if trim(proyecto)<>"" and not isnull(proyecto)  then
        		    sqlRepetido=sqlRepetido&" and ACCI_PROYECTO='" & proyecto & "'" 
		        else
        		    sqlRepetido=sqlRepetido&" and (ltrim(rtrim(ACCI_PROYECTO))='' Or ACCI_PROYECTO is null ) "
		        end if
				sqlRepetido=sqlRepetido&" And ACCI_SUTIPO=" & rsTiposAcciones("ACCI_SUTIPO")
        		sqlRepetido=sqlRepetido&" Group by acci_sutipo "
        		sqlRepetido=sqlRepetido&" Order by ACCI_SUTIPO ASC "

    		    Set rsAccRepetidos = connCRM.AbrirRecordSet(sqlRepetido,3,1)				
				if not rsAccRepetidos.eof then
					if isnumeric(rsAccRepetidos("contRepetida")) then
						if cint(rsAccRepetidos("contRepetida"))>1 then
							accionRepetida="*"
							tieneRepetidos=true
						end if
					end if
				end if
				rsAccRepetidos.cerrar()
		        set rsAccRepetidos=nothing 	
				'''Hasta aqui  Obtener si la accion esta repetida

                tipoAnterior=rsTiposAcciones("ACCI_SUTIPO")
                claseTipo="paso oculto"
                if rsTiposAcciones("ACCI_Terminada") then
                    claseTipo="paso cerrado"
                else
                    claseTipo="paso pendiente"						
                end if			
                        
                if cstr(trim(rsTiposAcciones("ACCI_SUTIPO")))=cstr(trim(tipoAccion)) then
                    claseTipo=claseTipo&" activo"
                end if				
    
				if trim(Pintados)<>"" then
					Pintados=Pintados & ","
				end if
				Pintados=Pintados & rsTiposAcciones("ACCI_SUTIPO")
                parametros="'" & modo & "','" & rsTiposAcciones("ACCI_Codigo")  & "','" &  codCliente  & "','" &  codCampana   & "','" &  rsTiposAcciones("ACCI_SUTIPO") & "','" & posCodigo & "','" & tabla & "','" & filtro & "','" & valorFiltro & "','1'"
				if esCliente then
					parametros=parametros & ",'CLIE'"
				elseif esContacto then
					parametros=parametros & ",'CON'"				
				else
					parametros=parametros & ",''"				
				end if
                %>
                <li onClick="recargarPagina(<%=parametros%>);" id="paso<%=rsTiposAcciones("ACCI_SUTIPO")%>" rel="<%=rsTiposAcciones("ACCI_SUTIPO")%>" class="<%=claseTipo%>">
                    <span class="no"><%=rsTiposAcciones("ACCI_SUTIPO")%></span>
                    <span class="nom"><%=rsTiposAcciones("TIAC_NOMBRE")%></span>
                    <span class="fecha"><%=rsTiposAcciones("ACCI_FECHA")%></span>
                    <%'accionRepetida="*"
'					tieneRepetidos=true%>
                    <% if trim(accionRepetida)<>"" then  %>
                    	<%if rsTiposAcciones("ACCI_SUTIPO")<=4 then%>
		                    <span class="repetidoIZ"><%=accionRepetida%></span>
                        <%else%>    
		                    <span class="repetidoDE"><%=accionRepetida%></span>                        
                        <%end if%>     
                    <% end if %>
                </li>
                <%		
                
            end if
            rsTiposAcciones.movenext
        wend
        rsTiposAcciones.cerrar()
        set rsTiposAcciones=nothing 	
        
		'Pintamos los que no se han pintado antes
		sql=" Select TIAC_CODIGO,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then TA.TIAC_NOMBRE else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TIAC_NOMBRE "	
        sql=sql&" From TIPOS_ACCION TA "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (TA.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "') "			
        sql=sql&" Where TIAC_CODIGO not in (" & Pintados & ")"
        sql=sql&" Order by TIAC_CODIGO ASC "

		Set rsTiposNoPintados = connCRM.AbrirRecordSet(sql,3,1)

        while not rsTiposNoPintados.eof
                parametros="'" & modo & "','" & rsTiposNoPintados("TIAC_CODIGO")  & "','" &  codCliente  & "','" &  codCampana   & "','" &  rsTiposNoPintados("TIAC_CODIGO") & "','" & posCodigo & "','" & tabla & "','" & filtro & "','" & valorFiltro & "','0'"
				if esCliente then
					parametros=parametros & ",'CLIE'"
				elseif esContacto then
					parametros=parametros & ",'CON'"				
				else
					parametros=parametros & ",''"				
				end if			
                %>
                <li onClick="recargarPagina(<%=parametros%>,this);" id="paso<%=rsTiposNoPintados("TIAC_CODIGO")%>" rel="<%=rsTiposNoPintados("TIAC_CODIGO")%>" class="paso oculto">
                    <span class="no"><%=rsTiposNoPintados("TIAC_CODIGO")%></span>
                    <span class="nom"><%=rsTiposNoPintados("TIAC_NOMBRE")%></span>
<!--                    <span class="fecha"><%'=""%></span>-->
                    <%'accionRepetida=""
					'tieneRepetidos=false%>
                    <%' if trim(accionRepetida)<>"" then  %>
                    	<%'if rsTiposNoPintados("TIAC_CODIGO")<=4 then%>
<!--		                    <span class="repetidoIZ"><%'=accionRepetida%></span>-->
                        <%'else%>    
<!--		                    <span class="repetidoDE"><%'=accionRepetida%></span>  -->
                        <%'end if%>    
                    <%' end if %>
                </li>
                <%				
			 rsTiposNoPintados.movenext
        wend
        rsTiposNoPintados.cerrar()
        set rsTiposNoPintados=nothing 			
		
        %>
        </ul>
        
        <% if tieneRepetidos then%>
			<span style=" color:#F60; margin-bottom:20px; padding-left:10px;"><%=objIdioma.getIdioma("clientes_Acciones_Texto13")%><span style=" font-size:30px; color:#F60;  vertical-align:middle; ">*</span><%=objIdioma.getIdioma("clientes_Acciones_Texto14")%></span>
            
        <% end if %>
        <!-- line-height:13px;-->
	<!--li.paso .repetido { position:absolute; top:5px; right:8px; font-size:30px; color:#F60; line-height:13px; }	-->
        
        <div style="display:block; clear:both;">
		<%'datos de campa�as%>
        <%=accionesMismaCampana%>
        
		</div>
        
    </div>
</div>    
<%end if%> 


<div id="DatCapana">
		<%
			accionAsociadaA=""
			if esCliente then
				accionAsociadaA="CLIE"
			elseif esContacto then
				accionAsociadaA="CON"
			end if
		%>
    	<span class="TitCampa"><strong><%=objIdioma.getIdioma("clientes_Acciones_Texto15")%></strong><span style="float:right; "><span id="bloqueIdAccion"><%=codigo%></span></span></span>
	  <iframe style="overflow:none;" id="datosAccion" name="datosAccion" frameborder="0" width="100%" height="470" src="/dmcrm/APP/Comercial/Empresas/clientes_AccionesDatos.asp?posCodigo=<%=posCodigo%>&codAccion=<%=codigo%>&codCliente=<%=codCliente%>&codCampana=<%=codCampana%>&modo=<%=modo%>&tipoAccion=<%=tipoAccion%>&tabla=<%=tabla%>&filtro=<%=filtro%>&accionAsociadaA=<%=accionAsociadaA%>" ></iframe>
</div>


<div style=" display:block; clear:both;">    
    <div id="DescripCampaAct">
        <span class="TitCampa"><strong><%=objIdioma.getIdioma("clientes_Acciones_Texto16")%></strong></span>
        <%=DescripCampana%>
    </div>

	<div id="DescripCampaAnt"><!--height:350px;--> 
       	<span class="TitCampa"><strong><%=objIdioma.getIdioma("clientes_Acciones_Texto17")%></strong></span>
    	<%=accionesRestoCampanas%>
    </div>  
</div>

<style type="text/css">
.TituGeneral { display:block; clear:both;color:#ff6600; font-size:18px;}
#DatCapana {width:49%; float:left;  border:solid 1px #f60; border-top:none; height:460px; min-width:540px; margin:10px 0 10px 0;}
.datSegimiento { display:none;}

.grafico:after, #pasosGrafico:after {display:block; clear:both; content:""; height1px;}
	.grafico {display:block; width:100%; min-width:540px; height:460px; float:left; overflow:hidden; margin:10px 10px 0 0; border:solid 1px #f60;}
	#pasosGrafico { width:530px;height:270px; position:relative; z-index:1; 
background:url(/dmcrm/APP/imagenes/graficoPasos.gif) no-repeat 110px 0; 
list-style:none; margin:0 auto; padding:0 0 0 0;
}
	
	li.paso { 
position:absolute; top:0px; left:0px; z-index:2; 
background:url(/dmcrm/APP/imagenes/graficoSombra.png) bottom right; 
display:block; width:190px; height:47px; cursor:pointer;
}
	li.paso:hover { background-color:#f1f1f1;}
	li.paso .no { position:absolute; top:0px; right:10px; height:39px; width:175px; 
overflow:hidden; border-left:solid 4px #eee; text-align:right; z-index:-1; 
font-family:georgia; font-size:50px; line-height:30px; color:#e5e5e5; 
}
	li.paso .nom { font-size:12px; color:#f60; clear:both; display:block; 
line-height:11px; margin:3px 0 0 10px;
}
	li.paso .fecha { margin:0 0 0 10px; font-size:10px;}
/*	li.paso .repetido { position:absolute; top:30px; right:8px; font-size:30px; color:#F60; line-height:13px;  }	*/
	li.paso .repetidoIZ {  font-size:30px; color:#F60;  position:absolute; top:30px; left:35px; line-height:13px;  }
	li.paso .repetidoDE {  font-size:30px; color:#F60;  position:absolute; top:30px; right:35px; line-height:13px;  }	
	
	li#paso1 { top:20px; left:310px;}
	li#paso2 { top:90px; left:330px;}
	li#paso3 { top:150px; left:340px;}
	li#paso4 { top:210px; left:310px;}
	li#paso5 { top:210px; left:50px;}
	li#paso6 { top:150px; left:10px;}
	li#paso7 { top:90px; left:0px;}
	li#paso8 { top:30px; left:50px;}
	
	
	li.cerrado .no {border-left:solid 4px #28bc24;}
	li.pendiente .no {border-left:solid 4px #ff6600;}
	
	li.activo {background:url(/dmcrm/APP/imagenes/graficoSombra2.png) bottom right;}
	
	li.oculto {opacity:0.4;filter:alpha(opacity=40); cursor:default;}
	li.oculto:hover { background-color:none!important;}
	li.oculto .nom { color:#333;}
	

li#paso1 .no, li#paso2 .no, li#paso3 .no, li#paso4 .no {text-align:left;}
li#paso1 .nom , li#paso2 .nom , li#paso3 .nom , li#paso4 .nom {margin:3px 0 0 35px;}
li#paso1 .fecha , li#paso2 .fecha , li#paso3 .fecha, li#paso4 .fecha {margin:0px 0 0 35px;}
	

#datoCampa:after {display:block; clear:both; content:""; height1px;}
#datoCampa { float:left; margin:0 10px 0 0; width:100%; min-width:540px; border:solid 1px #f60;}
.TitCampa {background:#f60; display:block; padding:5px; font-size:12px; color:#fff;}
.datoCampa {border-bottom: dotted 2px #f1f1f1; padding:0 10px;}
.datSegimiento .FondoProp { display:block; clear:both; width:99%; color:#fff; background:#f60; padding:3px 2px; margin:0 0 10px -5px; font-size:13px;}


#DescripCampaAct { width:49%; min-width:540px; height:250px; overflow:auto; display:block; float:left; border: solid 1px #f60; display:none; margin:0 10px 10px 0;}
#DescripCampaAct p { padding:10px;}

#DescripCampaAnt { width:49%; min-width:540px; height:250px; overflow:auto; display:block; float:left; border: solid 1px #f60; display:none; margin:0 0 0 0;}
#DescripCampaAnt p { padding:10px;}

.VerDesc, .VerAnt { cursor:pointer;}
.VerDesc:hover, .VerAnt:hover { cursor:pointer; color:#f60;}

.LineaGris { border-bottom: dotted 2px #f1f1f1; padding:5px 10px;}
.TitGris { background:#f1f1f1; display:block; width:98%; padding:3px;}

a.linkPro {margin-top:10px;display:inline-block; font-size:14px; text-decoration:none;}
a.linkPro:hover { color:#f60!important; text-decoration:underline;}
.fecha1 { color:#ccc}
.FondoProp { display:block; clear:both; width:99%; background:#f1f1f1;}

.divEditar {  float:left; clear:both; width:80%;  border:solid 1px #f60; padding:5px; text-align:right; display:none; position:absolute; top:25px; left:5px; z-index:999; background:#fff;  }
</style>

<%'Enlace oculto para poder lanzar Fancybox desde Javascript%>
<a id="mostrarContactoRapido" class="cEmergenteContactoRapido" href="#" style="display:none;" ></a>



</form>


<%end if ' end de if trim(lcase(tabla))="acciones_cliente" And trim(lcase(modo)))="alta"%>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->

	</body>
</html>

