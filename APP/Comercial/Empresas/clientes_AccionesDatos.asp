<% Response.Buffer =false %>
<%'El nombre del fichero de idiomas es, por ejemplo, Idiomas_Comercial.xml, en la variable ponemos s�lo el men� al que pertenece la p�gina
'S�lo usamos esta variable si la p�gina no est� enganchada en ning�n men� (app_menus)
nomFicheroIdiomas="Comercial"%>

<!--#include virtual="/dmcrm/includes/permisos.asp"-->
<!--#include virtual="/dmcrm/conf/conf.asp"-->
<!--#include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!--#include virtual="/dmcrm/APP/comun2/Validaciones.asp"  -->

<html>
	<head>
        <!-- #include virtual="/dmcrm/includes/jquery.asp"-->            

	    <script type="text/javascript" src="/dmcrm/APP/Comun2/FormChek2.js"></script>                

			<script type="text/javascript" >
            $(function()
            {
				$('.cajaTextoFecha').datePicker({startDate:'01/01/1900',clickInput:true});
				$('.cajaTextoFecha').dpSetOffset(20, 0);
            });

			
			////////////////////
            $(document).ready(function() {
                $('#txtHoraInicio').timepicker({
                    showPeriodLabels: false
                });
                $('#txtHoraFin').timepicker({
                    showPeriodLabels: false
                });				
				
					$(".botonImputar").live('click', function(){
						$(this).fancybox({
							'imageScale'			: true,
							'zoomOpacity'			: true,
							'overlayShow'			: true,
							'overlayOpacity'		: 0.7,
							'overlayColor'			: '#333',
							'centerOnScroll'		: true,
							'zoomSpeedIn'			: 600,
							'zoomSpeedOut'			: 500,
							'transitionIn'			: 'elastic',
							'transitionOut'			: 'elastic',
							'type'					: 'iframe',
							'frameWidth'			: '100%',
							'frameHeight'			: '100%',
							'titleShow'		        : false,
							'onClosed'		        : function() {
							    recargarHoras();
							}							
						}).trigger("click"); 
						return false; 	
					});	
					
					function recargarHoras()
					{
                        var esCliente="1";
						$.ajax({
							type: "POST",
							url: "/dmcrm/APP/Comercial/Empresas/clientes_Acciones_Horas.asp",
							data: "idAccion=<%=request("codAccion")%>",
							success: function(datos) {
								if (datos=="")
								{ document.getElementById("bloqueHorasImputadas").innerHTML="0"; }
								else { document.getElementById("bloqueHorasImputadas").innerHTML=datos; }
							}

						});
						

						
						//alert("aa");
					}
              });

		</script>
		<script type="text/javascript">

/*			function abrirImputacionDeHoras(codAccion)
			{
				window.parent.imputarHorasAccion(codAccion);				
			}*/
												 
							
			function refrescarCodAccion(codAccion)				
			{
				if (window.parent.document.getElementById("bloqueIdAccion")!=null)
				{window.parent.document.getElementById("bloqueIdAccion").innerHTML=codAccion;}
			}

			function anadirNuevoContacto(codCliente,posCodigo)
			{
					 window.parent.anadirContacto(codCliente,posCodigo);
			}
			function quitarCargando()
			{
				document.getElementById("divCargandoAccion").style.display="none";
			}
			
			
			
			function guardarDatos(modo,codAccion,codCliente,codCampana,tipoAccion,posCodigo,tabla,filtro,valorFiltro,terminada,accionAsociadaA)
			{
				if (!validadores())
				{
					var respuesta=false;
					var introducirNueva=false;
					sinAprobar=false
					if (document.getElementById("cboFinalizacion")!=null)
					{
						if (document.getElementById("cboFinalizacion").value=="2")
						{ sinAprobar=true; }
					}
					if (document.getElementById("chkTerminado").checked && terminada=="0" && ( parseInt(document.getElementById("cboTipo").value)<7 || (document.getElementById("cboTipo").value=="7" && sinAprobar ) ) )
					{
						var introducirNueva=window.confirm("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto1")%>");
						respuesta=true;
					}
					else
					{
						respuesta=true;				
					}

					if (modo=="alta")
					{
						var salir=false;
						if (parent.document.getElementById("cboCliente")!=null)
						{
							document.getElementById("clienteOculto").value=parent.document.getElementById("cboCliente").value;						
							if (document.getElementById("clienteOculto").value=="0")// || document.getElementById("campanaOculta").value=="")
							{
								respuesta=false;
								alert("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto2")%>")
								salir=true;
							}
						}
						if (!salir)
						{

							document.getElementById("campanaOculta").value=parent.document.getElementById("cboCampana").value;
							document.getElementById("proyectoOculto").value=parent.document.getElementById("txtProyecto").value;
							if (document.getElementById("campanaOculta").value=="0" || document.getElementById("proyectoOculto").value=="")
							{
								respuesta=false;
								alert("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto3")%>")
							}
						}
					
					}
					if (respuesta)
					{
						if (document.getElementById("txtFechaAlta").value=="")
						{
							alert("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto4")%>");
							respuesta=false
						}
					}
					if (respuesta)
					{
						if (document.getElementById("cboTipo").value=="6" || document.getElementById("cboTipo").value=="7")
						{
							if (document.getElementById("chkTerminado").checked && (document.getElementById("cboEstadoCliente").value=="" || document.getElementById("cboEstadoCliente").value=="0"))							
							{
								alert("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto5")%>");
								respuesta=false
							}
							if (respuesta)
							{
								if (document.getElementById("chkTerminado").checked && (document.getElementById("txtCantidad").value=="" || (document.getElementById("txtCantidad").value=="0" && document.getElementById("cboFinalizacion")!=null  && document.getElementById("cboFinalizacion").value!="2") ))							
								{
									alert("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto6")%>");
									respuesta=false
								}
							}
						}
					}
					if (respuesta)
					{
						if (document.getElementById("cboTipo").value=="7")
						{
							if (document.getElementById("cboEstadoCliente").value=="" || document.getElementById("cboEstadoCliente").value=="0")
							{
								alert("<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto7")%>");
								respuesta=false
							}
						}
					}
					if (respuesta)
					{
						var nuevaAccion="0";
						if (introducirNueva)
						{
							nuevaAccion="1";
						}
						if (tipoAccion=="4" || tipoAccion=="6" || tipoAccion=="7" )
						{actualizarComerciales();}
						
						document.mtoAcciones.action="clientes_AccionesDatos.asp?posCodigo=" + posCodigo + "&codAccion=" + codAccion  + "&codCliente=" + codCliente + "&codCampana=" + codCampana + "&modo=" + modo + "&tipoAccion=" + tipoAccion + "&guardar=1&nuevaAccion=" + nuevaAccion + "&tabla=" + tabla + "&filtro=" + filtro + "&valorFiltro=" + valorFiltro + "&accionAsociadaA=" + accionAsociadaA;
						document.mtoAcciones.submit();	
					}
				}
			}



			function validadores()
			{
				
				var errorValidador=false;
				var mensaje="";
				if (document.getElementById("txtGastos")!=null)
				{
					if (isFloat(document.getElementById("txtGastos").value,true) == false & isSignedFloat (document.getElementById("txtGastos").value,true)==false )
					{
						errorValidador=true;
						mensaje="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto8")%>";
					}
					
				}
				if (document.getElementById("txtCantidad")!=null & (!errorValidador))
				{
					if (isFloat(document.getElementById("txtCantidad").value,true) == false & isSignedFloat (document.getElementById("txtCantidad").value,true)==false )
					{
						errorValidador=true;
						mensaje="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto9")%>";						
					}
				}
				
				if (document.getElementById("txtHoraInicio")!=null & document.getElementById("txtHoraFin")!=null & (!errorValidador))
				{
					if (document.getElementById("txtHoraInicio").value=="" && document.getElementById("txtHoraFin").value!="" )
					{
						errorValidador=true;
						mensaje="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto10")%>";						
					}
					
					if (document.getElementById("txtHoraInicio").value!="" && document.getElementById("txtHoraFin").value!="" )
					{
						
						var textoHoraInicio=document.getElementById("txtHoraInicio").value;
						var inicioDesglose=textoHoraInicio.split(":");
						var HoraInicio="";
						var MinutosInicio="";
						if (inicioDesglose.length==1)
						{	HoraInicio=inicioDesglose[0];
							MinutosInicio="0";
						}else if (inicioDesglose.length==2)
						{
							HoraInicio=inicioDesglose[0];
							MinutosInicio=inicioDesglose[1];
						}
						
						var textoHoraFin=document.getElementById("txtHoraFin").value;
						var finDesglose=textoHoraFin.split(":");
						var HoraFin="";
						var MinutosFin="";
						if (finDesglose.length==1)
						{	HoraFin=finDesglose[0];
							MinutosFin="0";
						}else if (finDesglose.length==2)
						{
							HoraFin=finDesglose[0];
							MinutosFin=finDesglose[1];
						}
						
						if (parseInt(HoraInicio)==parseInt(HoraFin))
						{ 
							if (parseInt(MinutosInicio)>parseInt(MinutosFin))
							{
								errorValidador=true;
								mensaje="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto11")%>";			
							}
						}else if (parseInt(HoraInicio)>parseInt(HoraFin))
						{
							errorValidador=true;
							mensaje="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto11")%>";										
						}
									
					}
				}
				
				
				
				//txtHoraFin
				/*if (document.getElementById("txtFechaAlta")!=null & (!errorValidador))
				{
					var cadena=document.getElementById("txtFechaAlta").value;
					var pos1=cadena.indexOf(""/"",1);
					var pos2=cadena.indexOf(""/"",pos1+1);
					 if (pos1==-1 || pos2==-1) { 
						 errorValidador=true;
						mensaje="El campo fecha debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�.3";						
					 }
					  
					 var dia=cadena.substring(0,pos1)
					 for (k=0;k<dia.length;k++) { 
						 if (dia.charAt(k) < '0' || dia.charAt(k)>'9') {
								 errorValidador=true;
								mensaje="El campo fecha debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�.4";		
						} 
					 }
						
					 if (dia>31 || dia<1) { 
						 errorValidador=true;
						mensaje="El campo fecha debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�5.";			
					} 

					var mes=cadena.substring(pos1+1,pos2)
					 for (k=0;k<mes.length;k++) { 
						 if (mes.charAt(k) < '0' || mes.charAt(k)>'9') {   
							 errorValidador=true;
							mensaje="El campo fecha debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�.6";			
							} 
					 }
					 if (mes>12 || mes<1) { 
							 errorValidador=true;
							mensaje="El campo fecha debe contener una fecha l�gica. Corrija el mes: "+mes+".7";			
						} 		
					var ano=cadena.substring(pos2+1,cadena.length)
					 for (k=0;k<ano.length;k++) { 
						 if (ano.charAt(k) < '0' || ano.charAt(k)>'9') {    
							 errorValidador=true;
							mensaje="El campo fecha debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�.8";									
						} 	
					 }

					 if (ano>3000 || ano<0) { 
							 errorValidador=true;
							mensaje="El campo fecha  debe contener una fecha l�gica. Corrija el a�o: "+ano+".8";									
						} 



				}*/
				
				
				if (errorValidador)
				{
					alert(mensaje);
				}

				return errorValidador;	
				
			}

			function mostrarCapasSegunTipo(valor)
			{
				
				if (valor=="6")
				{
					document.getElementById("divBloqueEstadoCliente").style.display="inline";
					document.getElementById("divBloquefinalizacion").style.display="none";
					document.getElementById("divBloqueGastos").style.display="none";					
					document.getElementById("divBloqueCantidad").style.display="inline";										
				}
				else if (valor=="7") 
				{
					document.getElementById("divBloqueEstadoCliente").style.display="none";
					document.getElementById("divBloquefinalizacion").style.display="inline";
					document.getElementById("divBloqueGastos").style.display="inline";					
					document.getElementById("divBloqueCantidad").style.display="inline";															
				}
				else
				{
					document.getElementById("divBloqueEstadoCliente").style.display="none";
					document.getElementById("divBloquefinalizacion").style.display="none";
					document.getElementById("divBloqueGastos").style.display="none";					
					document.getElementById("divBloqueCantidad").style.display="none";															
				}
			
				
			}


			function controlarMultiplesComerciales(comercial,comercial2,comercial3,comercial4,usuCodigo)
			{

				//document.getElementById("enlaceAddComercial2").style.display="none";	
				//document.getElementById("enlaceAddComercial3").style.display="none";	
				if (document.getElementById("lblComercial2")!=null && document.getElementById("cboComercial2")!=null && document.getElementById("enlaceDelComercial1")!=null && document.getElementById("enlaceAddComercial2")!=null )
				{
					if (comercial2=="" || comercial2=="0")
					{
						document.getElementById("lblComercial2").style.display="none";
						document.getElementById("cboComercial2").style.display="none";
						document.getElementById("enlaceDelComercial1").style.display="none";	
						document.getElementById("enlaceAddComercial2").style.display="none";										
					}
					else
					{
						document.getElementById("enlaceAddComercial1").style.display="none";					
						document.getElementById("enlaceDelComercial1").style.display="";										
					}
				}
				
				if (document.getElementById("lblComercial3")!=null && document.getElementById("cboComercial3")!=null && document.getElementById("enlaceDelComercial2")!=null && document.getElementById("enlaceAddComercial3")!=null )
				{				
					if (comercial3=="" || comercial3=="0")
					{				
						document.getElementById("lblComercial3").style.display="none";
						document.getElementById("cboComercial3").style.display="none";
						document.getElementById("enlaceDelComercial2").style.display="none";
						document.getElementById("enlaceAddComercial3").style.display="none";															
					}
					else
					{
						document.getElementById("enlaceAddComercial2").style.display="none";	
						document.getElementById("enlaceDelComercial2").style.display="";	
						
						document.getElementById("enlaceDelComercial1").style.display="none";
					}
				}
				
				if (document.getElementById("lblComercial4")!=null && document.getElementById("cboComercial4")!=null && document.getElementById("enlaceDelComercial3")!=null  )
				{								
					if (comercial4=="" || comercial4=="0")
					{				
						document.getElementById("lblComercial4").style.display="none";
						document.getElementById("cboComercial4").style.display="none";
						document.getElementById("enlaceDelComercial3").style.display="none";
//						document.getElementById("enlaceAddComercial3").style.display="";															
					}
					else
					{
						document.getElementById("enlaceAddComercial3").style.display="none";
						document.getElementById("enlaceDelComercial3").style.display="";	
						
						document.getElementById("enlaceDelComercial1").style.display="none";
						document.getElementById("enlaceDelComercial2").style.display="none";						
					}
				}


			}
			
			function mostrarOcultarComercial(mostrarOcultar,numComercial,usuCodigo)
			{
				if (numComercial=="1")
				{
					if (mostrarOcultar=="M")
					{
						if (document.getElementById("hidComercial2").value!="" && document.getElementById("hidComercial2").value!="0" )
						{document.getElementById("cboComercial2").value=document.getElementById("hidComercial2").value;}
						else
						{ document.getElementById("cboComercial2").value=usuCodigo }
						
						document.getElementById("lblComercial2").style.display="";
						document.getElementById("cboComercial2").style.display="";
						

						document.getElementById("enlaceAddComercial1").style.display="none";
						document.getElementById("enlaceDelComercial1").style.display="";	
						document.getElementById("enlaceAddComercial2").style.display="";
						document.getElementById("enlaceDelComercial2").style.display="none";	
					}
					else
					{
						document.getElementById("lblComercial2").style.display="none";
						document.getElementById("cboComercial2").style.display="none";
						
						document.getElementById("lblComercial3").style.display="none";
						document.getElementById("cboComercial3").style.display="none";
						
						document.getElementById("lblComercial4").style.display="none";
						document.getElementById("cboComercial4").style.display="none";				

						document.getElementById("enlaceAddComercial1").style.display="";
						document.getElementById("enlaceDelComercial1").style.display="none";	

						document.getElementById("enlaceAddComercial2").style.display="none";
						document.getElementById("enlaceDelComercial2").style.display="none";	
						
						document.getElementById("enlaceAddComercial3").style.display="none";
						document.getElementById("enlaceDelComercial3").style.display="none";	
						
						
					}
					
				}
				else if (numComercial=="2")
				{
					if (mostrarOcultar=="M")
					{
						if (document.getElementById("hidComercial3").value!="" && document.getElementById("hidComercial3").value!="0" )
						{document.getElementById("cboComercial3").value=document.getElementById("hidComercial3").value;}						
						else
						{ document.getElementById("cboComercial3").value=usuCodigo }
						
						document.getElementById("lblComercial3").style.display="";
						document.getElementById("cboComercial3").style.display="";

						document.getElementById("enlaceDelComercial1").style.display="none";

						document.getElementById("enlaceAddComercial2").style.display="none";
						document.getElementById("enlaceDelComercial2").style.display="";
						document.getElementById("enlaceAddComercial3").style.display="";
						document.getElementById("enlaceDelComercial3").style.display="none";							
					}
					else
					{
						document.getElementById("lblComercial3").style.display="none";
						document.getElementById("cboComercial3").style.display="none";
						
						document.getElementById("lblComercial4").style.display="none";
						document.getElementById("cboComercial4").style.display="none";

						document.getElementById("enlaceAddComercial1").style.display="none";
						document.getElementById("enlaceDelComercial1").style.display="";

						document.getElementById("enlaceAddComercial2").style.display="";
						document.getElementById("enlaceDelComercial2").style.display="none";
						
						document.getElementById("enlaceAddComercial3").style.display="none";
						document.getElementById("enlaceDelComercial3").style.display="none";	
						
					}
					
				}
				else if (numComercial=="3")
				{
					if (mostrarOcultar=="M")
					{
						if (document.getElementById("hidComercial4").value!="" && document.getElementById("hidComercial4").value!="0" )
						{document.getElementById("cboComercial4").value=document.getElementById("hidComercial4").value;}						
						else
						{ document.getElementById("cboComercial4").value=usuCodigo }
						
						document.getElementById("lblComercial4").style.display="";
						document.getElementById("cboComercial4").style.display="";
						
						document.getElementById("enlaceDelComercial1").style.display="none";						
						document.getElementById("enlaceDelComercial2").style.display="none";												

						document.getElementById("enlaceAddComercial3").style.display="none";
						document.getElementById("enlaceDelComercial3").style.display="";	

					}
					else
					{
						document.getElementById("lblComercial4").style.display="none";
						document.getElementById("cboComercial4").style.display="none";	
						
						document.getElementById("enlaceAddComercial2").style.display="none";
						document.getElementById("enlaceDelComercial2").style.display="";						
						
						document.getElementById("enlaceAddComercial3").style.display="";
						document.getElementById("enlaceDelComercial3").style.display="none";	

					}
					
				}				
				
			}

			function actualizarComerciales()
			{
				if (document.getElementById("cboComercial2")!=null && document.getElementById("hidComercial2")!=null  )
				{		
					if (document.getElementById("cboComercial2").style.display!="none" && document.getElementById("cboComercial3").value!="0" )
					{document.getElementById("hidComercial2").value=document.getElementById("cboComercial2").value;}
					else
					{	document.getElementById("hidComercial2").value=""}
				}

				if (document.getElementById("cboComercial3")!=null && document.getElementById("hidComercial3")!=null  )
				{				
					if (document.getElementById("cboComercial3").style.display!="none" && document.getElementById("cboComercial3").value!="0")
					{document.getElementById("hidComercial3").value=document.getElementById("cboComercial3").value;}
					else
					{document.getElementById("hidComercial3").value=""}
				}
				
				if (document.getElementById("cboComercial4")!=null && document.getElementById("hidComercial4")!=null  )
				{								
					if (document.getElementById("cboComercial4").style.display!="none" &&  document.getElementById("cboComercial4").value!="0")
					{document.getElementById("hidComercial4").value=document.getElementById("cboComercial4").value;	}
					else
					{document.getElementById("hidComercial4").value=""}
				}
			}

		</script>
 
 
<style type="text/css">
body { margin:0; padding:0;}
#divCargandoAccion {position:absolute; width:100%; height:99%; padding:0px; margin:0px; background:WhiteSmoke; text-align:center; vertical-align:middle; padding-top:100px;}

#gestionclientes { padding:10px;}
#gestionclientes label { display:inline-block; padding:0 5px 0 2px; background:#f1f1f1; font-size:12px;}
#gestionclientes input, #gestionclientes select { margin:0 15px 0 0; display:inline-block;}
#gestionclientes select { padding:2px 0;}


.ui-timepicker td, .jCalendar td { font-size:11px; }

.elementoControl { white-space:nowrap; float:left; }
/*.bloqueImgComercial { float:left; top:-6px; left:-10px; position:relative;}*/

.imgComercialAdd {   }
.imgComercialDel {  }

.imgComercialAddEnlace {cursor:pointer; left:-10px; position:relative; }
.imgComercialDelEnlace {cursor:pointer; left:-10px; position:relative; }
.imgComercialAddEnlace:hover {background-color:silver;}
.imgComercialDelEnlace:hover {background-color:silver;}

.imgComercialAddTexto { font-size:9px; font-family:Arial; color:green; cursor:pointer; margin-left:2px; }
.imgComercialDelTexto { font-size:9px; font-family:Arial; color:red; cursor:pointer; margin-left:2px;  }
/*.imgComercialAddTexto:hover { color:gray;}
.imgComercialDelTexto:hover { color:gray;*/

</style> 
        
	</head>
	<body   onLoad="javascript:quitarCargando();">


<div id="divCargandoAccion">
	cargando...
</div>

	<form id="mtoAcciones" name="mtoAcciones" runat="server" action="clientes_AccionesDatos.asp" method="post"  >

<%

codCliente=REQUEST("codCliente")
codCampana=REQUEST("codCampana")
codAccion=REQUEST("codAccion")
modo=REQUEST("MODO")
tabla=REQUEST("tabla")
filtro=REQUEST("filtro")
tipoAccion=REQUEST("TIPOACCION")
posCodigo=request("posCodigo")

accionAsociadaA=request("accionAsociadaA")

esCliente=false
esContacto=false
if trim(ucase(accionAsociadaA))="CLIE" then
	esCliente=true
	esContacto=false
elseif trim(ucase(accionAsociadaA))="CON" then
	esCliente=false
	esContacto=true
else
	if trim(lcase(tabla))="clientes"  Or ( trim(lcase(tabla))="acciones_cliente" And trim(filtro)="ACCI_SUCLIENTE" ) then
		esCliente=true
		esContacto=false
	elseif 	 trim(lcase(tabla))="contactos"  Or ( trim(lcase(tabla))="acciones_cliente" And trim(filtro)="ACCI_SUCONTACTO" )  then
		esCliente=false
		esContacto=true
	elseif  trim(lcase(tabla))="acciones_cliente" And trim(filtro)="" then
		if trim(request.Form("cboNuevoClienteDatos"))<>"" And trim(request.Form("cboNuevoContactoDatos"))="" then
			esCliente=true
			esContacto=false
		elseif trim(request.Form("cboNuevoClienteDatos"))="" And trim(request.Form("cboNuevoContactoDatos"))<>"" then
			esCliente=false
			esContacto=true
		end if
	end if
end if


 function vistaFecha(fecha,tipo)
	'**** Tipo = 1 -> Fecha ; Tipo = 2 -> Hora *****
	'***********************************************
  if isdate(fecha) then 
  	 if isnull(fecha) then
	 	vistaFecha = ""
	 else
	 	if tipo = 1 then
		 	vistaFecha = right("00" & day(fecha),2) & "/" & right("00" & month(fecha),2) & "/" & right("0000" & year(fecha),4)
		else
			vistaFecha = right("00" & hour(fecha),2) & ":" & right("00" & minute(fecha),2)
		end if
	 end if
  end if
end function  



		proyecto=""
'		fecha=FormatearFechaVisualizacion(date(),false)
		fecha=vistaFecha(date(),1)
        horaInicio=""
        horaFin=""
		terminada=false
		comercial="0"
		comercial2="0"
		comercial3="0"
		comercial4="0"		
		comentario=""
		contacto=""
		cantidad="0"
		estado="0"
		finalizacion="0"
		gastos="0"
		

if trim(lcase(modo))="modificacion" then
	sql=" Select * "
    sql=sql & " From ACCIONES_CLIENTE AC "
	if isnumeric(codAccion) then
		sql=sql &  " Where ACCI_CODIGO=" & codAccion 
	else
		sql=sql &  " Where ACCI_SUCAMPANA=" & codCampana & " And ACCI_SUTIPO=" & tipoAccion	
		if esCliente then
			sql=sql & " And ACCI_SUCLIENTE=" & codCliente 
		elseif esContacto then
			sql=sql & " And ACCI_SUCONTACTO=" & codCliente 		
		end if	
	end if	

	sql=sql &  " Order by ACCI_FECHAMODIFICACION DESC "
	Set rsDatosAccion = connCRM.AbrirRecordSet(sql,3,1)
	if not rsDatosAccion.eof then
		if not esCliente And not esContacto then
			if isnull(rsDatosAccion("ACCI_SUCLIENTE")) Or trim(rsDatosAccion("ACCI_SUCLIENTE"))="" then
				esCliente=false
				esContacto=true		
			else
				esCliente=true
				esContacto=false				
			end if	
		end if	
		codAccion=rsDatosAccion("ACCI_CODIGO")
		proyecto=rsDatosAccion("ACCI_PROYECTO")		
'		fecha=FormatearFechaVisualizacion(rsDatosAccion("ACCI_FECHA"),false)
		fecha=vistaFecha(rsDatosAccion("ACCI_FECHA"),1)
		horaInicio=FormatearHora(rsDAtosAccion("ACCI_HORAINICIO"))
        horaFin=FormatearHora(rsDAtosAccion("ACCI_HORAFIN"))
		terminada=rsDatosAccion("ACCI_TERMINADA")
		comercial=rsDatosAccion("ACCI_SUCOMERCIAL")		
		comercial2=rsDatosAccion("ACCI_SUCOMERCIAL2")		
		comercial3=rsDatosAccion("ACCI_SUCOMERCIAL3")		
		comercial4=rsDatosAccion("ACCI_SUCOMERCIAL4")		
		comentario=rsDatosAccion("ACCI_COMENTARIO")		
		contacto=rsDatosAccion("ACCI_SUCONTACTO")	
		contactoRapido=rsDatosAccion("ACCI_CONTACTO")								
		cantidad=rsDatosAccion("ACCI_TIPO6CANTIDAD")								
		areaNegocio=rsDatosAccion("ACCI_TIPO6SUAREANEGOCIO")
		estado=rsDatosAccion("ACCI_TIPO6ESTADOCLIENTE")		
		finalizacion=rsDatosAccion("ACCI_TIPO7FINALIZACION")								
		gastos=rsDatosAccion("ACCI_TIPO7GASTOS")								
		fechaModificacion=rsDatosAccion("ACCI_FECHAMODIFICACION")								
	end if
	rsDatosAccion.cerrar()
	set rsDatosAccion=nothing 
end if	
fechaModificacion=FormatearFechaSQLSever(date()) & " " & time()

if trim(codCliente)="" then
	if trim(request.Form("clienteOculto"))<>"" then
		codCliente=request.Form("clienteOculto")
	end if
end if

		'Si la accion es de tipo 7, vamos a buscar el ACCI_TIPO6ESTADOCLIENTE y ACCI_TIPO6SUAREANEGOCIO
		if trim(tipoAccion)="7" And trim(codCampana)<>"" And trim(codCliente)<>""  then
			sqlObtenerEstado=" Select Top 1 ACCI_TIPO6ESTADOCLIENTE,ACCI_TIPO6SUAREANEGOCIO From ACCIONES_CLIENTE Where ACCI_SUTIPO=6 "
'			if trim(lcase(modo))="modificacion" then
				sqlObtenerEstado=sqlObtenerEstado & "And ACCI_PROYECTO='" & proyecto & "' And ACCI_SUCAMPANA=" & codCampana 
'			else
'				sqlObtenerEstado=sqlObtenerEstado & "And ACCI_PROYECTO='" & request.Form("proyectoOculto") & "' And ACCI_SUCAMPANA=" & request.Form("campanaOculta") 			
'			end if	
			if esCliente then
				sqlObtenerEstado=sqlObtenerEstado & " And ACCI_SUCLIENTE=" & codCliente 
			elseif esContacto then
				sqlObtenerEstado=sqlObtenerEstado & " And ACCI_SUCONTACTO=" & codCliente 		
			end if	
			sqlObtenerEstado=sqlObtenerEstado & " Order by ACCI_FECHAMODIFICACION DESC "
			Set rsObtenerEstado = connCRM.AbrirRecordSet(sqlObtenerEstado,3,1)
			if not rsObtenerEstado.eof then
				if not isnull(rsObtenerEstado("ACCI_TIPO6ESTADOCLIENTE")) then
					if trim(cstr(rsObtenerEstado("ACCI_TIPO6ESTADOCLIENTE")))<>"" And trim(cstr(rsObtenerEstado("ACCI_TIPO6ESTADOCLIENTE")))<>"0" then
						estado=rsObtenerEstado("ACCI_TIPO6ESTADOCLIENTE")
					end if		
				end if
				if not isnull(rsObtenerEstado("ACCI_TIPO6SUAREANEGOCIO")) then
					if trim(cstr(rsObtenerEstado("ACCI_TIPO6SUAREANEGOCIO")))<>"" And trim(cstr(rsObtenerEstado("ACCI_TIPO6SUAREANEGOCIO")))<>"0" then
						areaNegocio=rsObtenerEstado("ACCI_TIPO6SUAREANEGOCIO")						
					end if		
				end if				
			else
				estado="-1"	
				areaNegocio="-1"		
			end if
			rsObtenerEstado.cerrar()
			set rsObtenerEstado=nothing 			
		end if

if request("guardar")="1" then

	if trim(lcase(modo))="alta" then
		'insertar registro
			tipoAccion=trim(request.Form("cboTipo"))
		sql=" Insert into ACCIONES_CLIENTE(ACCI_SUCAMPANA,ACCI_SUCLIENTE,ACCI_PROYECTO,ACCI_FECHA,ACCI_SUTIPO,ACCI_TERMINADA,ACCI_SUCOMERCIAL,ACCI_COMENTARIO,ACCI_SUCONTACTO,ACCI_CONTACTO,ACCI_FECHAMODIFICACION"
		
		'Si existe comercial 2
		if trim(request.Form("hidComercial2"))<>"" And trim(request.Form("hidComercial2"))<>"0" then
			sql=sql & ",ACCI_SUCOMERCIAL2"
		end if
		'Si existe comercial 3
		if trim(request.Form("hidComercial3"))<>"" And trim(request.Form("hidComercial3"))<>"0" then
			sql=sql & ",ACCI_SUCOMERCIAL3"
		end if		
		'Si existe comercial 4
		if trim(request.Form("hidComercial4"))<>"" And trim(request.Form("hidComercial4"))<>"0" then
			sql=sql & ",ACCI_SUCOMERCIAL4"
		end if				
		
        if not request.Form("txtHoraInicio")="" then sql=sql & ",ACCI_HORAINICIO"
        if not request.Form("txtHoraFin")="" then sql=sql & ",ACCI_HORAFIN"
		if tipoAccion="6" then
			sql=sql & ",ACCI_TIPO6ESTADOCLIENTE,ACCI_TIPO6CANTIDAD,ACCI_TIPO6SUAREANEGOCIO"
		elseif tipoAccion="7" then
			sql=sql & ",ACCI_TIPO7FINALIZACION,ACCI_TIPO6CANTIDAD,ACCI_TIPO7GASTOS"
		else
			sql=sql & ",ACCI_TIPO6ESTADOCLIENTE,ACCI_TIPO6CANTIDAD,ACCI_TIPO7FINALIZACION,ACCI_TIPO7GASTOS,ACCI_TIPO6SUAREANEGOCIO"	
		end if
		
'		hidComercial4
		
		sql=sql & ") Values ('" & request.Form("campanaOculta") & "'"
		if esCliente then
			if trim(codCliente)="" then
				sql=sql & ",'" & request.Form("cboCliente") & "'"
			else
				sql=sql & ",'" & codCliente & "'"
			end if
		elseif esContacto then
			sql=sql & ",null"
		end if	
		
		fechaAlta = FormatearFechaSQLSever(cdate(request.Form("txtFechaAlta")))
		sql=sql & ",'" & request.Form("proyectoOculto")  & "','" & fechaAlta & "','" & request.Form("cboTipo")  & "'"
		if trim(lcase(request.Form("chkTerminado")))="on" then
			sql=sql & ",1"
		else
			sql=sql & ",0"
		end if
		sql=sql & ",'" & request.Form("cboComercial") & "','" & request.Form("txtComentario") & "'" 
		if esCliente then
			if trim(request.Form("cboContacto"))<>"0" And trim(request.Form("cboContacto"))<>"" then
				sql=sql & ",'" & request.Form("cboContacto") & "'"		
			else
				sql=sql & ",null"		
			end if
		elseif esContacto then
				sql=sql & ",'" & codCliente & "'"				
		end if	
		if esCliente then
			sql=sql & ",'"&request.Form("txtContactoRapido")&"'"
		elseif esContacto then
			sql=sql & ",null"		
		end if	
		sql=sql&",'" & fechaModificacion & "'"
		
		'Si existe comercial 2
		if trim(request.Form("hidComercial2"))<>"" And trim(request.Form("hidComercial2"))<>"0" then
			sql=sql & ",'" & request.Form("hidComercial2") & "'"
		end if
		'Si existe comercial 3
		if trim(request.Form("hidComercial3"))<>"" And trim(request.Form("hidComercial3"))<>"0" then
			sql=sql & ",'" & request.Form("hidComercial3") & "'"
		end if		
		'Si existe comercial 4
		if trim(request.Form("hidComercial4"))<>"" And trim(request.Form("hidComercial4"))<>"0" then
			sql=sql & ",'" & request.Form("hidComercial4") & "'"
		end if			
		
        if not request.Form("txtHoraInicio")="" then sql=sql &  ",'" & FormatearHora(request.Form("txtHoraInicio")) & "'"
        if not request.Form("txtHoraFin")="" then sql=sql &  ",'" & FormatearHora(request.Form("txtHoraFin")) & "'"

		if tipoAccion="6" then
			if trim(request.Form("cboEstadoCliente"))="0" Or trim(request.Form("cboEstadoCliente"))="" then
				sql=sql & ",null"
			else
				sql=sql & ",'" & request.Form("cboEstadoCliente") & "'"
			end if				
			
			if  trim(request.Form("txtCantidad"))="" then
				sql=sql & ",'0'"
			else
				sql=sql & "," & replace(request.Form("txtCantidad"),",",".") 
			end if				
			
			if trim(request.Form("cboAreaNegocio"))="0" Or trim(request.Form("cboAreaNegocio"))="" then
				sql=sql & ",null"
			else
				sql=sql & ",'" & request.Form("cboAreaNegocio") & "'"
			end if							
			
		elseif tipoAccion="7" then
			if trim(request.Form("cboFinalizacion"))="0" or trim(request.Form("cboFinalizacion"))="" then
				sql=sql & ",null"
			else
				sql=sql & ",'" & request.Form("cboFinalizacion") & "'"
			end if				

			if  trim(request.Form("txtCantidad"))="" then
				sql=sql & ",'0'"
			else
				sql=sql & "," & replace(request.Form("txtCantidad"),",",".") 
			end if				
			
			if trim(request.Form("txtGastos"))="" then
				sql=sql & ",'0'"
			else
				sql=sql & "," & replace(request.Form("txtGastos"),",",".") 
			end if										

		else
			sql=sql & ",null,null,null,null,null"
		end if		
		sql=sql & ")"
				

		connCRM.execute sql,intNumRegistros



		'obtener el codigo insertado		
		accionInsertada=""
		sql=" Select Max(ACCI_CODIGO) as Codigo From ACCIONES_CLIENTE Where ACCI_FECHAMODIFICACION='" & fechaModificacion & "'"
		if esCliente then
			sql=sql & " And ACCI_SUCLIENTE=" & codCliente 
		elseif esContacto then	
			sql=sql & " And ACCI_SUCONTACTO=" & codCliente 		
		end if
		sql=sql & " And ACCI_SUCAMPANA=" & request.Form("campanaOculta") & " And ACCI_SUTIPO=" & request.Form("cboTipo")
		

		Set rsCodigoInsertado = connCRM.AbrirRecordSet(sql,3,1)
		if not rsCodigoInsertado.eof then
			accionInsertada=rsCodigoInsertado("Codigo")							
		end if
		rsCodigoInsertado.cerrar()
		set rsCodigoInsertado=nothing 
		
		
			
'			if trim(request.Form("cboAreaNegocio"))="0" Or trim(request.Form("cboAreaNegocio"))="" then
'			sql=sql & ",ACCI_TIPO6SUAREANEGOCIO=" & request.Form("cboAreaNegocio") 
		''Si es tipo 7, modificamos el campo ACCI_TIPO6ESTADOCLIENTE del tipo 6
		if tipoAccion="7" And ((trim(request.Form("cboEstadoCliente"))<>"" And trim(request.Form("cboEstadoCliente"))<>"0") Or (trim(request.Form("cboAreaNegocio"))<>"" And trim(request.Form("cboAreaNegocio"))<>"0") )  And trim(accionInsertada)<>"" then
			sqlActualizarEstadoCliente = " Update ACCIONES_CLIENTE A Set A.ACCI_TIPO6ESTADOCLIENTE=" 
			if trim(request.Form("cboEstadoCliente"))<>"" And trim(request.Form("cboEstadoCliente"))<>"0" then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & trim(request.Form("cboEstadoCliente"))
			else
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & "null"
			end if							

			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & ", ACCI_TIPO6SUAREANEGOCIO="			
			if trim(request.Form("cboAreaNegocio"))<>"" And trim(request.Form("cboAreaNegocio"))<>"0" then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & trim(request.Form("cboAreaNegocio"))
			else
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & "null"
			end if				
			 
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " Where A.ACCI_CODIGO="
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " ( Select A2.ACCI_CODIGO From ACCIONES_CLIENTE A2 "
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " Inner Join ACCIONES_CLIENTE A3 on ( A2.ACCI_PROYECTO=A3.ACCI_PROYECTO And A2.ACCI_SUCAMPANA=A3.ACCI_SUCAMPANA "
			if esCliente then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " And A2.ACCI_SUCLIENTE=A3.ACCI_SUCLIENTE "						
			elseif esContacto then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " And A2.ACCI_SUCONTACTO=A3.ACCI_SUCONTACTO "						
			end if				
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " ) "			
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " Where  A3.ACCI_CODIGO=" & accionInsertada & " And A2.ACCI_SUTIPO=6) "

			connCRM.execute sqlActualizarEstadoCliente,intNumRegistros
		end if
		''end de Si es tipo 7, modificamos el campo ACCI_TIPO6ESTADOCLIENTE del tipo 6			
		
		'Insertamos en la tabla log
        sql=" Insert into ACCIONES_CLIENTE_LOG (ACCILOG_SUACCION,ACCILOG_SUCOMERCIAL,ACCILOG_FECHAMODIFICACION,ACCILOG_HORAMODIFICACION) values (" & accionInsertada & "," & session("codigoUsuario") & ",'" & fechaModificacion & "','" & time() & "')"

		'Si la accion insertada, est� terminada, se a�ade la siguiente, siempre que la accion no sea la ultima(tipo 7). Y el usuario ha aceptado insertar la nueva
		if trim(lcase(request.Form("chkTerminado")))="on" And request("nuevaAccion")="1" then
			if (cint(request.Form("cboTipo"))<7) Or ( trim(request.Form("cboTipo"))="7" And trim(request.Form("cboFinalizacion"))="2" ) then
				nuevaFechaAlta=FormatearFechaSQLSever(date())
				nuevoTipo=cstr((cint(request.Form("cboTipo"))+1))
				sql=" Insert into ACCIONES_CLIENTE(ACCI_SUCAMPANA,ACCI_SUCLIENTE,ACCI_PROYECTO,ACCI_FECHA,ACCI_SUTIPO,ACCI_TERMINADA,ACCI_SUCOMERCIAL,ACCI_CONTACTO,ACCI_SUCONTACTO,ACCI_FECHAMODIFICACION) "
				sql=sql & " Values ('" & request.Form("campanaOculta") & "'," 
				if esCliente then
					sql=sql& "'" & codCliente & "'"
				elseif esContacto then
					sql=sql& "null"				
				end if	
				
				auxfechaAlta = FormatearFechaSQLSever(cdate(request.Form("txtFechaAlta")))
				sql=sql & ",'" & request.Form("proyectoOculto")  & "','" & auxfechaAlta & "','" &  nuevoTipo & "',0,'" & session("codigoUsuario") & "','" & request.Form("txtContactoRapido") & "','" 
				if esCliente then
					sql=sql & request.Form("cboContacto") 
				elseif esContacto then
					sql=sql & codCliente
				end if	
				sql=sql&"','" & fechaModificacion & "')"

				connCRM.execute sql,intNumRegistros		
				
				'obtener el codigo insertado		
'				accionInsertada=""
				sql=" Select Max(ACCI_CODIGO) as Codigo From ACCIONES_CLIENTE Where ACCI_FECHAMODIFICACION='" & fechaModificacion & "'"
				if esCliente then
					sql=sql & " And ACCI_SUCLIENTE=" & codCliente 
				elseif esContacto then 	
					sql=sql & " And ACCI_SUCONTACTO=" & codCliente 
				end if
				sql=sql & " And ACCI_SUCAMPANA=" & request.Form("campanaOculta") & " And ACCI_SUTIPO=" & nuevoTipo
		'		response.Write(sql&"<br/>")
				Set rsCodigoInsertado = connCRM.AbrirRecordSet(sql,3,1)
				if not rsCodigoInsertado.eof then
					accionInsertada=rsCodigoInsertado("Codigo")							
				end if
				rsCodigoInsertado.cerrar()
				set rsCodigoInsertado=nothing 
				
				

				
				'Insertamos en la tabla log
                sql=" Insert into ACCIONES_CLIENTE_LOG (ACCILOG_SUACCION,ACCILOG_SUCOMERCIAL,ACCILOG_FECHAMODIFICACION,ACCILOG_HORAMODIFICACION) values (" & accionInsertada & "," & session("codigoUsuario") & ",'" & fechaModificacion & "','" & time() & "')"
				connCRM.execute sql,intNumRegistros						
				
			end if	
		end if

	%><script type="text/javascript">
		parent.location="/dmcrm/APP/comercial/Empresas/clientes_Acciones.asp?posCodigo=<%=posCodigo%>&codigo=<%=accionInsertada%>&modo=modificacion&tabla=<%=tabla%>&filtro=<%=filtro%>&valorFiltro=<%=codCliente%>";
	</script><%		
	elseif trim(lcase(modo))="modificacion" then	
	


		auxfechaAlta = FormatearFechaSQLSever(cdate(request.Form("txtFechaAlta")))
		'" & day(auxfechaAlta) & "/" & month(auxfechaAlta) & "/" & year(auxfechaAlta) & "'
			
		sql=" Update ACCIONES_CLIENTE "
		sql=sql & " Set ACCI_FECHA='" & auxfechaAlta & "',ACCI_SUTIPO=" & request.Form("cboTipo") & ",ACCI_SUCOMERCIAL=" & request.Form("cboComercial") & ",ACCI_COMENTARIO='" & request.Form("txtComentario") & "',ACCI_FECHAMODIFICACION='" & fechaModificacion & "'"
		
		'Si existe comercial 2
		if trim(request.Form("hidComercial2"))<>"" And trim(request.Form("hidComercial2"))<>"0" then
			sql=sql & ",ACCI_SUCOMERCIAL2=" & trim(request.Form("hidComercial2"))	
		else			
			sql=sql & ",ACCI_SUCOMERCIAL2=null"
		end if
		
		'Si existe comercial 3
		if trim(request.Form("hidComercial3"))<>"" And trim(request.Form("hidComercial3"))<>"0" then
			sql=sql & ",ACCI_SUCOMERCIAL3=" & trim(request.Form("hidComercial3"))	
		else			
			sql=sql & ",ACCI_SUCOMERCIAL3=null"
		end if
		
		'Si existe comercial 4
		if trim(request.Form("hidComercial4"))<>"" And trim(request.Form("hidComercial4"))<>"0" then
			sql=sql & ",ACCI_SUCOMERCIAL4=" & trim(request.Form("hidComercial4"))	
		else			
			sql=sql & ",ACCI_SUCOMERCIAL4=null"
		end if
		
        if isnull(request.Form("txtHoraInicio")) or trim(request.Form("txtHoraInicio"))="" then
            sql=sql & " ,ACCI_HORAINICIO=null"
        else
            sql=sql & " ,ACCI_HORAINICIO='" & FormatearHora(request.Form("txtHoraInicio")) & "'"
        end if
        if isnull(request.Form("txtHoraFin")) or trim(request.Form("txtHoraFin"))="" then
            sql=sql & " ,ACCI_HORAFIN=null"
        else
            sql=sql & " ,ACCI_HORAFIN='" & FormatearHora(request.Form("txtHoraFin")) & "'"
        end if
		if esCliente then
			if trim(request.Form("cboContacto"))="0" then
				sql=sql & ",ACCI_SUCONTACTO=null"
			else
				sql=sql & ",ACCI_SUCONTACTO=" & request.Form("cboContacto")
			end if		
			sql=sql & ",ACCI_CONTACTO='" & request.Form("txtContactoRapido") & "'"
		end if	 
		if trim(lcase(request.Form("chkTerminado")))="on" then
			sql=sql & ",ACCI_TERMINADA=1 "
		else
			sql=sql & ",ACCI_TERMINADA=0 "
		end if
		
		

		
		if tipoAccion="6" then
			if trim(request.Form("cboEstadoCliente"))="0" Or trim(request.Form("cboEstadoCliente"))="" then
			
				sql=sql & ",ACCI_TIPO6ESTADOCLIENTE=null"
			else

				sql=sql & ",ACCI_TIPO6ESTADOCLIENTE=" & request.Form("cboEstadoCliente") 
			end if		


			if  trim(request.Form("txtCantidad"))="" then
			
				sql=sql & ",ACCI_TIPO6CANTIDAD=0"
			else

				sql=sql & ",ACCI_TIPO6CANTIDAD=" & replace(request.Form("txtCantidad"),",",".")
			end if	
			
			if trim(request.Form("cboAreaNegocio"))="0" Or trim(request.Form("cboAreaNegocio"))="" then
			
				sql=sql & ",ACCI_TIPO6SUAREANEGOCIO=null"
			else

				sql=sql & ",ACCI_TIPO6SUAREANEGOCIO=" & request.Form("cboAreaNegocio") 
			end if		
				

		elseif tipoAccion="7" then
			if trim(request.Form("cboFinalizacion"))="0" Or trim(request.Form("cboFinalizacion"))="" then
				sql=sql & ",ACCI_TIPO7FINALIZACION=null"
			else
				sql=sql & ",ACCI_TIPO7FINALIZACION=" & request.Form("cboFinalizacion") 
			end if				
			
			if  trim(request.Form("txtCantidad"))="" then
				sql=sql & ",ACCI_TIPO6CANTIDAD=0"
			else
				sql=sql & ",ACCI_TIPO6CANTIDAD=" & replace(request.Form("txtCantidad"),",",".")
			end if				
			
			if  trim(request.Form("txtGastos"))="" then
				sql=sql & ",ACCI_TIPO7GASTOS=0"
			else
				sql=sql & ",ACCI_TIPO7GASTOS=" & replace(request.Form("txtGastos"),",",".")
			end if		
			

			
		end if		
		sql=sql & " Where ACCI_CODIGO=" & codAccion

		connCRM.execute sql,intNumRegistros
		
'			if trim(request.Form("cboAreaNegocio"))="0" Or trim(request.Form("cboAreaNegocio"))="" then
'			sql=sql & ",ACCI_TIPO6SUAREANEGOCIO=" & request.Form("cboAreaNegocio") 		
		''Si es tipo 7, modificamos el campo ACCI_TIPO6ESTADOCLIENTE del tipo 6
		if tipoAccion="7" And ((trim(request.Form("cboEstadoCliente"))<>"" And trim(request.Form("cboEstadoCliente"))<>"0") Or (trim(request.Form("cboAreaNegocio"))<>"" And trim(request.Form("cboAreaNegocio"))<>"0")) And trim(codAccion)<>"" then
		
			sqlActualizarEstadoCliente = " Update ACCIONES_CLIENTE Set [ACCIONES_CLIENTE].ACCI_TIPO6ESTADOCLIENTE="
			if trim(request.Form("cboEstadoCliente"))<>"" And trim(request.Form("cboEstadoCliente"))<>"0" then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & trim(request.Form("cboEstadoCliente"))
			else
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & "null"
			end if										
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & ",[ACCIONES_CLIENTE].ACCI_TIPO6SUAREANEGOCIO="			
			if trim(request.Form("cboAreaNegocio"))<>"" And trim(request.Form("cboAreaNegocio"))<>"0" then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & trim(request.Form("cboAreaNegocio"))	
			else
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & "null"
			end if					
			
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " Where [ACCIONES_CLIENTE].ACCI_CODIGO="
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " ( Select A2.ACCI_CODIGO From ACCIONES_CLIENTE A2 "
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " Inner Join ACCIONES_CLIENTE A3 on ( A2.ACCI_PROYECTO=A3.ACCI_PROYECTO And A2.ACCI_SUCAMPANA=A3.ACCI_SUCAMPANA "
			if esCliente then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " And A2.ACCI_SUCLIENTE=A3.ACCI_SUCLIENTE "						
			elseif esContacto then
				sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " And A2.ACCI_SUCONTACTO=A3.ACCI_SUCONTACTO "						
			end if				
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " ) "			
			sqlActualizarEstadoCliente = sqlActualizarEstadoCliente & " Where  A3.ACCI_CODIGO=" & codAccion & " And A2.ACCI_SUTIPO=6) "

			connCRM.execute sqlActualizarEstadoCliente,intNumRegistros
		end if
		''end de Si es tipo 7, modificamos el campo ACCI_TIPO6ESTADOCLIENTE del tipo 6

		'Insertamos en la tabla log
		sql=" Insert into ACCIONES_CLIENTE_LOG (ACCILOG_SUACCION,ACCILOG_SUCOMERCIAL,ACCILOG_FECHAMODIFICACION,ACCILOG_HORAMODIFICACION) values (" & codAccion & "," & session("codigoUsuario") & ",'" & fechaModificacion  & "','" & time() & "')"
		connCRM.execute sql,intNumRegistros				
		if trim(lcase(request.Form("chkTerminado")))="on"  And request("nuevaAccion")="1"  then
			'insertar nuevo registro
'			if cint(request.Form("cboTipo"))<7 then
			if (cint(request.Form("cboTipo"))<7) Or ( trim(request.Form("cboTipo"))="7" And trim(request.Form("cboFinalizacion"))="2" ) then
				nuevaFechaAlta=FormatearFechaSQLSever(date())
				nuevoTipo=cstr((cint(request.Form("cboTipo"))+1))
				sql=" Insert into ACCIONES_CLIENTE(ACCI_SUCAMPANA,ACCI_SUCLIENTE,ACCI_PROYECTO,ACCI_FECHA,ACCI_SUTIPO,ACCI_TERMINADA,ACCI_SUCOMERCIAL,ACCI_CONTACTO,ACCI_SUCONTACTO,ACCI_FECHAMODIFICACION) "
				sql=sql & " Values ('" & codCampana & "',"
				if esCliente then
					 sql=sql & "'" & codCliente & "'"
				elseif esContacto then
					 sql=sql & "null"				
				end if	 
								
				
'				 sql=sql& ",'" & proyecto  & "','" & right(nuevaFechaAlta,2) & "/" & mid(nuevaFechaAlta,5,2) & "/" & left(nuevaFechaAlta,4) & "','" &  nuevoTipo & "',0,'" & session("codigoUsuario")  & "',"  
				 sql=sql& ",'" & proyecto  & "','" & nuevaFechaAlta & "','" &  nuevoTipo & "',0,'" & session("codigoUsuario")  & "',"  
				 if esCliente then
					 sql=sql & "'" & request.Form("txtContactoRapido") & "','" & request.Form("cboContacto") & "'"				 
				 elseif esContacto then
					 sql=sql & "null,'" & codCliente & "'"				 				 
				 end if
				sql = sql & ",'" & fechaModificacion & "')"

				connCRM.execute sql,intNumRegistros		
				
				'obtener el codigo insertado		
'				accionInsertada=""
				sql=" Select Max(ACCI_CODIGO) as Codigo From ACCIONES_CLIENTE Where ACCI_FECHAMODIFICACION='" & fechaModificacion & "'"
				if esCliente then
					sql=sql& " And ACCI_SUCLIENTE=" & codCliente  
				elseif esContacto then
					sql=sql& " And ACCI_SUCONTACTO=" & codCliente  				
				end if	
				 sql=sql&" And ACCI_SUCAMPANA=" & codCampana & " And ACCI_SUTIPO=" & nuevoTipo

				Set rsCodigoInsertado = connCRM.AbrirRecordSet(sql,3,1)
				if not rsCodigoInsertado.eof then
					accionInsertada=rsCodigoInsertado("Codigo")							
				end if
				rsCodigoInsertado.cerrar()
				set rsCodigoInsertado=nothing 		
				

				
				'Insertamos en la tabla log
                sql=" Insert into ACCIONES_CLIENTE_LOG (ACCILOG_SUACCION,ACCILOG_SUCOMERCIAL,ACCILOG_FECHAMODIFICACION,ACCILOG_HORAMODIFICACION) values (" & accionInsertada & "," & session("codigoUsuario") & ",'" & fechaModificacion & "','" & time() & "')"
				connCRM.execute sql,intNumRegistros	
				%><script type="text/javascript">
					parent.location="/dmcrm/APP/comercial/Empresas/clientes_Acciones.asp?posCodigo=<%=posCodigo%>&codigo=<%=accionInsertada%>&modo=modificacion&tabla=<%=tabla%>&filtro=<%=filtro%>&valorFiltro=<%=codCliente%>";
				</script><%											
				
			end if				
		end if
		
				%><script type="text/javascript">
					parent.location="/dmcrm/APP/comercial/Empresas/clientes_Acciones.asp?posCodigo=<%=posCodigo%>&codigo=<%=codAccion%>&modo=modificacion&tabla=<%=tabla%>&filtro=<%=filtro%>&valorFiltro=<%=codCliente%>";
				</script><%			

	end if	
end if 'end de guardar=1

codigoUsuPorDefecto=session("codigoUsuario")

%>
 
    	<input id="clienteOculto" name="clienteOculto" type="text" style="width:1px; display:none;" />
    	<input id="campanaOculta" name="campanaOculta" type="text" style="width:1px; display:none;" />
    	<input id="proyectoOculto" name="proyectoOculto" type="text" style="width:1px; display:none;" />  
        <div id="gestionclientes" class="contenidoSeccion">
        	<div  class="elementoControl" >
		          <label for="cboTipo"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto12")%></label>  
                  <select id="cboTipo" name="cboTipo"  class="cboFiltros" style="width:220px;"  onChange="mostrarCapasSegunTipo(this.value);">
						<%
						sql=" Select T.TIAC_CODIGO,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TIAC_NOMBRE else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TIAC_NOMBRE "
						sql=sql & "From TIPOS_ACCION T "
						sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "') "
						sql=sql & " ORDER BY T.TIAC_CODIGO "	
						Set rsTipoAccion = connCRM.AbrirRecordSet(sql,3,1)
						while not rsTipoAccion.eof
							%><option value="<%=rsTipoAccion("TIAC_CODIGO")%>"  <%if trim(cstr(rsTipoAccion("TIAC_CODIGO")))=trim(cstr(tipoAccion)) then response.Write("selected") end if%> ><%=rsTipoAccion("TIAC_NOMBRE")%></option><%
							rsTipoAccion.movenext
						wend
						rsTipoAccion.cerrar()
						set rsTipoAccion=nothing 
						%>
		            </select> 
             </div>      
           	<%	sql=" Select *, (case when ANA_EMAIL='" & trim(session("emailAdministracion")) & "' then 1 else 0 end) as usuActual From ANALISTAS Where ANA_DIRECTORCUENTA=1 "
				Set rsComerciales = connCRM.AbrirRecordSet(sql,3,1)
                    ''Inicio de comercial 1%>                     
		        	<div class="elementoControl" >              
    	        			<label id="lblComercial" for="cboComercial" style="float:left;"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto13")%></label>
							<select id="cboComercial" name="cboComercial"  class="cboFiltros" style="width:110px; float:left;">
		        	                	<%	sql=" Select *, (case when ANA_EMAIL='" & trim(session("emailAdministracion")) & "' then 1 else 0 end) as usuActual From ANALISTAS Where ANA_DIRECTORCUENTA=1 "
											Set rsComerciales = connCRM.AbrirRecordSet(sql,3,1)
											while not rsComerciales.eof
												%><option <%if  trim(comercial)=trim(rsComerciales("ANA_CODIGO"))  then %> selected="selected" <%elseif rsComerciales("usuActual") And trim(request.Form("cboComercial"))<>"0" And trim(comercial)="0" then %>selected="selected" <%end if%> value="<%=rsComerciales("ANA_CODIGO")%>" ><%=rsComerciales("ANA_NOMBRE")%></option><%
												rsComerciales.movenext
											wend
										%>
				            </select> 
    		        </div>                            
						<%if tipoAccion="4" Or tipoAccion="6" Or tipoAccion="7" then%>
	    	                   <a  id="enlaceAddComercial1"  class="imgComercialAddEnlace" title="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto14")%>"  onClick="mostrarOcultarComercial('M','1','<%=codigoUsuPorDefecto%>');" > <img id="imgAnnadirComercial1" class="imgComercialAdd" src="/dmcrm/APP/imagenes/comercialAnnadir.png" /><span class="imgComercialAddTexto"  ><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto14")%></span></a>

                        <%end if%>
                        

                    <%''Fin de comercial 1%> 
					<%if tipoAccion="4" Or tipoAccion="6" Or tipoAccion="7" then %>

                    <%''Inicio de comercial 2%>
		        	<div  class="elementoControl" >   
                    		<input type="hidden" id="hidComercial2" name="hidComercial2" value="<%=comercial2%>" />                               
	    	        		<label  id="lblComercial2" for="cboComercial2" style="float:left;"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto15")%></label>
							<select id="cboComercial2" name="cboComercial2"  class="cboFiltros" style="width:110px; float:left;">
<!--        	                		<option value="0" <%'if trim(comercial2)="" Or trim(comercial2)="0" then%> selected <%'end if%> ></option>-->
	        		                	<%	rsComerciales.movefirst
											while not rsComerciales.eof
												%><option <%if  trim(comercial2)=trim(rsComerciales("ANA_CODIGO"))  then %> selected="selected" <%elseif rsComerciales("usuActual") And trim(request.Form("cboComercial2"))<>"0" And trim(comercial2)="0" then %>selected="selected" <%end if%> value="<%=rsComerciales("ANA_CODIGO")%>" ><%=rsComerciales("ANA_NOMBRE")%></option><%
												rsComerciales.movenext
											wend
										%>
				            </select> 
  		                   <a  id="enlaceDelComercial1"  class="imgComercialDelEnlace" title="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto16")%>"  onClick="mostrarOcultarComercial('O','1','<%=codigoUsuPorDefecto%>');"> <img id="imgAnnadirComercial1" class="imgComercialDel" src="/dmcrm/APP/imagenes/comercialQuitar.png" /></a>  
                                                     
    		        </div>              
  		        	<div class="elementoControl" >                              
	                       <a  id="enlaceAddComercial2"  class="imgComercialAddEnlace" title="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto17")%>" onClick="mostrarOcultarComercial('M','2','<%=codigoUsuPorDefecto%>');" > <img id="imgAnnadirComercial2" class="imgComercialAdd" src="/dmcrm/APP/imagenes/comercialAnnadir.png" /><span class="imgComercialAddTexto" ><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto17")%></span></a>
					</div>                        

                    <%''Fin de comercial 2%>  					
					
					                                      
                    <%''Inicio de comercial 3%>
		        	<div  class="elementoControl" >   
                    		<input type="hidden" id="hidComercial3" name="hidComercial3" value="<%=comercial3%>" />                               
	    	        		<label  id="lblComercial3" for="cboComercial3" style="float:left;"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto18")%></label>
							<select id="cboComercial3" name="cboComercial3"  class="cboFiltros" style="width:110px; float:left;">
<!--        	                		<option value="0" <%'if trim(comercial3)="" Or trim(comercial3)="0" then%> selected <%'end if%> ></option>-->
	        		                	<%	rsComerciales.movefirst
											while not rsComerciales.eof
												%><option <%if  trim(comercial3)=trim(rsComerciales("ANA_CODIGO"))  then %> selected="selected" <%elseif rsComerciales("usuActual") And trim(request.Form("cboComercial3"))<>"0" And trim(comercial3)="0" then %>selected="selected" <%end if%> value="<%=rsComerciales("ANA_CODIGO")%>" ><%=rsComerciales("ANA_NOMBRE")%></option><%
												rsComerciales.movenext
											wend
										%>
				            </select> 
    	                   <a  id="enlaceDelComercial2"  class="imgComercialDelEnlace"title="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto19")%>"  onClick="mostrarOcultarComercial('O','2','<%=codigoUsuPorDefecto%>');"> <img id="imgAnnadirComercial2" class="imgComercialDel" src="/dmcrm/APP/imagenes/comercialQuitar.png" /></a>       
                                                
    		        </div>  
  		        	<div class="elementoControl" >                                                                               
	                       <a  id="enlaceAddComercial3"  class="imgComercialAddEnlace" title="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto20")%>" onClick="mostrarOcultarComercial('M','3','<%=codigoUsuPorDefecto%>');" > <img id="imgAnnadirComercial3" class="imgComercialAdd" src="/dmcrm/APP/imagenes/comercialAnnadir.png" /><span class="imgComercialAddTexto" ><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto20")%></span></a>
					</div>                           

                        

                    <%''Fin de comercial 3%>    

                    <%''Inicio de comercial 4%>
		        	<div class="elementoControl" >              
                    		<input type="hidden" id="hidComercial4" name="hidComercial4" value="<%=comercial4%>" />                    
    	        			<label  id="lblComercial4" for="cboComercial4" style="float:left;"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto21")%></label>
							<select id="cboComercial4" name="cboComercial4"  class="cboFiltros" style="width:110px; float:left;">
<!--    	                    		<option value="0" <%'if trim(comercial4)="" Or trim(comercial4)="0" then%> selected <%'end if%> ></option>-->
	    	    	                	<%	rsComerciales.movefirst
											while not rsComerciales.eof
												%><option <%if  trim(comercial4)=trim(rsComerciales("ANA_CODIGO"))  then %> selected="selected" <%elseif rsComerciales("usuActual") And trim(request.Form("cboComercial4"))<>"0" And trim(comercial4)="0" then %>selected="selected" <%end if%> value="<%=rsComerciales("ANA_CODIGO")%>" ><%=rsComerciales("ANA_NOMBRE")%></option><%
												rsComerciales.movenext
											wend
										%>
				            </select> 
    	                   <a  id="enlaceDelComercial3"  class="imgComercialDelEnlace" title="<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto22")%>"  onClick="mostrarOcultarComercial('O','3','<%=codigoUsuPorDefecto%>');"> <img id="imgAnnadirComercial3" class="imgComercialDel" src="/dmcrm/APP/imagenes/comercialQuitar.png" /></a>                            
                        
    		        </div>   
                    <%''Fin de comercial 4%>                                                                             
					<script type="text/javascript" >
						controlarMultiplesComerciales('<%=comercial%>','<%=comercial2%>','<%=comercial3%>','<%=comercial4%>','<%=codigoUsuPorDefecto%>')
					</script>                    
            <%end if 
				rsComerciales.cerrar()
				set rsComerciales=nothing 			
			%>
            
	     <div style="clear:both; width:100%; height:10px;">&nbsp;</div> 	
         
        	<div class="elementoControl" >             
				<label for="txtFechaAlta"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto23")%></label> 
<!--                <input id="txtFechaAlta" name="txtFechaAlta"  class="cajaTextoFecha" type="text" value="<%'=cdate(left(fecha,4) & "/" & mid(fecha,5,2) & "/" & right(fecha,2))%>" style=" width:75px;" />-->
                <input id="txtFechaAlta" name="txtFechaAlta"  class="cajaTextoFecha" type="text" value="<%=fecha%>" style=" width:75px;" />

             </div>   

        	<div  class="elementoControl" >	
				<label for="txtHoraInicio"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto24")%></label>
	            <input id="txtHoraInicio" maxlength="0" name="txtHoraInicio"  class="cajaTextoHora" type="text" value="<%if not isnull(horaInicio) and horaInicio<>"" then response.write FormatDateTime(horaInicio,4)%>" style=" width:60px;" />
            </div>    
            
        	<div  class="elementoControl" >
				<label for="txtHoraFin"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto25")%></label> 
            	<input id="txtHoraFin" name="txtHoraFin" maxlength="0"  class="cajaTextoHora" type="text" value="<%if not isnull(horaFin) and horaFin<>"" then response.write FormatDateTime(horaFin,4)%>" style=" width:60px;" />
			</div>                

	    
			<%if trim(codCliente)<>"" And not esContacto then %>
        
			 <div style="clear:both; width:100%; height:10px;">&nbsp;</div> 	            
					<div class="elementoControl" >	             
            		<label for="cboContacto"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto26")%></label>
            		<select id="cboContacto" name="cboContacto"  class="cboFiltros" style="width:350px;">
                    	<option value="0" ></option>
						<%sql=" Select C.CON_CODIGO,C.CON_SUCLIENTE,C.CON_NOMBRE,C.CON_APELLIDO1,C.CON_TELEFONO,C.CON_MOVIL,C.CON_EMAIL,(case when C.CON_CONTACTOCOMERCIAL=1 then ' | " & objIdioma.getIdioma("clientes_AccionesDatos_Texto27") & "' else '' end) as contactoComercial, (case when con_noactivo = 1 then ' - [" & objIdioma.getIdioma("clientes_AccionesDatos_Texto40") & "]' else '' end) as textoNoActivo  "
					    sql=sql & " From CONTACTOS C "
						sql=sql & " Where C.CON_SUCLIENTE=" & codCliente
					    sql=sql &  " Order by C.CON_NOMBRE ASC, C.CON_APELLIDO1 ASC "

						Set rsContactos = connCRM.AbrirRecordSet(sql,3,1)
						while not rsContactos.eof
							regDatos=rsContactos("CON_NOMBRE") & " " & rsContactos("CON_APELLIDO1")
							if trim(rsContactos("CON_TELEFONO"))<>"" then
								regDatos=regDatos & " | " & rsContactos("CON_TELEFONO")
							end if
							if trim(rsContactos("CON_MOVIL"))<>"" then
								regDatos=regDatos & " | " & rsContactos("CON_MOVIL")
							end if							
							if trim(rsContactos("CON_EMAIL"))<>"" then
								regDatos=regDatos & " | " & rsContactos("CON_EMAIL")
							end if		
							regDatos=regDatos & rsContactos("contactoComercial")							
							
							%><option value="<%=rsContactos("CON_CODIGO")%>" <%if trim(contacto)=trim(rsContactos("CON_CODIGO")) then%> selected <%end if%> ><%=regDatos & rsContactos("textoNoActivo")%></option><%
							rsContactos.movenext
						wend
						rsContactos.cerrar()
						set rsContactos=nothing %>
                        <option value="-1" onClick="javascript:anadirNuevoContacto('<%=codCliente%>','<%=posCodigo%>');"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto28")%></option>                                
		            </select>
                    </div>
					<div class="elementoControl" >	                    
						<label for="txtContactoRapido"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto29")%></label> 
                        <input id="txtContactoRapido" name="txtContactoRapido"  class="cajaTexto" type="text" value="<%=contactoRapido%>" style="width:150px" />
                    </div>    
             <%end if%>
            
            
            <div style="clear:both; width:100%; height:10px;"></div> 
            
             <label for="txtComentario"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto30")%></label> <textarea id="txtComentario" name="txtComentario" class="cboFiltros" style="width:100%; height:180px;" ><%=comentario%></textarea>

			<div style="clear:both; width:100%; height:10px;"></div>
             
             
             <div id="divBloqueEstadoCliente" <%if (tipoAccion<>"6" And tipoAccion<>"7") Or trim(estado)="-1" then %> style=" display:none;"<%else%> style=" display:inline;"<%end if%>>
	             <div class="elementoControl" >        
                 <label for="cboEstadoCliente"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto31")%></label>
                    <select id="cboEstadoCliente" name="cboEstadoCliente"  class="cboFiltros" style="width:110px">
                            <option value="0"  ></option>                    
                        <%
						sql=" Select T.TAPEC_CODIGO,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TAPEC_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TAPEC_DESCRIPCION "
						sql=sql & "From TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE T "
						sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TAPEC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE' And D.IDID_CAMPO='TAPEC_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "
						sql=sql & " ORDER BY (case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TAPEC_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) "	
                        Set rsEstados = connCRM.AbrirRecordSet(sql,3,1)
                        while not rsEstados.eof
                            %><option value="<%=rsEstados("TAPEC_CODIGO")%>"  <%if trim(cstr(rsEstados("TAPEC_CODIGO")))=trim(estado) then response.Write("selected") end if%> ><%=rsEstados("TAPEC_DESCRIPCION")%></option><%
                            rsEstados.movenext
                        wend
                        rsEstados.cerrar()
                        set rsEstados=nothing 
                        %>                    
                    </select>
                    </div>
                    <div class="elementoControl" >        
                 <label for="cboAreaNegocio"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto32")%></label>
                    <select id="cboAreaNegocio" name="cboAreaNegocio"  class="cboFiltros" style="width:110px">
                            <option value="0"  ></option>                    
                        <%
						sql=" Select T.TAN_CODIGO,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TAN_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TAN_DESCRIPCION "
						sql=sql & "From TIPOS_AREA_NEGOCIO T "
						sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TAN_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AREA_NEGOCIO' And D.IDID_CAMPO='TAN_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "
						sql=sql & " ORDER BY (case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TAN_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) "	

                        Set rsAreasNegocio = connCRM.AbrirRecordSet(sql,3,1)
                        while not rsAreasNegocio.eof
                            %><option value="<%=rsAreasNegocio("TAN_CODIGO")%>"  <%if trim(cstr(rsAreasNegocio("TAN_CODIGO")))=trim(areaNegocio) then response.Write("selected") end if%> ><%=rsAreasNegocio("TAN_DESCRIPCION")%></option><%
                            rsAreasNegocio.movenext
                        wend
                        rsAreasNegocio.cerrar()
                        set rsAreasNegocio=nothing 
                        %>                    
                    </select>
                    </div>                    
              </div >       
             <div id="divBloquefinalizacion" <%if tipoAccion<>"7" then %> style=" display:none;"<%else%> style=" display:inline;"<%end if%>>                 
	             <div class="elementoControl" >        
            	<label for="cboFinalizacion"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto33")%></label>
                	<select id="cboFinalizacion" name="cboFinalizacion"  class="cboFiltros" style="width:110px">
           	            <option value="0"  ></option>
						<%
			
						sql=" Select T.TAPF_CODIGO,(case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TAPF_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) as TAPF_DESCRIPCION "
						sql=sql & "From TIPOS_AUX_PROPUESTA_FINALIZACION T "
						sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TAPF_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AUX_PROPUESTA_FINALIZACION' And D.IDID_CAMPO='TAPF_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "
						sql=sql & " ORDER BY (case when LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION is null then T.TAPF_DESCRIPCION else CAST(D.IDID_DESCRIPCION AS varchar(max)) end) "	

						Set rsFinalizacion = connCRM.AbrirRecordSet(sql,3,1)
						while not rsFinalizacion.eof
							%><option value="<%=rsFinalizacion("TAPF_CODIGO")%>"  <%if trim(cstr(rsFinalizacion("TAPF_CODIGO")))=trim(finalizacion) then response.Write("selected") end if%> ><%=rsFinalizacion("TAPF_DESCRIPCION")%></option><%
							rsFinalizacion.movenext
						wend
						rsFinalizacion.cerrar()
						set rsFinalizacion=nothing 
						%>                    
		            </select> 
                    </div>
              </div>             
             <div id="divBloqueCantidad" <%if tipoAccion<>"6" and  tipoAccion<>"7"  then %> style=" display:none;"<%else%> style=" display:inline;"<%end if%>>                    
	             <div class="elementoControl" >        
	                <label for="txtCantidad"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto34")%></label> <input id="txtCantidad" name="txtCantidad"  class="cajaTexto" type="text" value="<%=cantidad%>" style=" width:75px;" />
                </div>
             </div >   
             <div id="divBloqueGastos" <%if tipoAccion<>"7" then %> style=" display:none;"<%else%> style=" display:inline;"<%end if%>>                    
				<div class="elementoControl" >                     
					<label for="txtGastos"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto35")%></label> <input id="txtGastos" name="txtGastos" class="cajaTexto" type="text" value="<%=gastos%>" style="width:75px;" />
                </div>    
             </div>   
            <div style="clear:both; width:100%; height:10px;"></div>
            <div style="float:right; margin:0 15px 0 0;">
                <input style="cursor:pointer; margin:0; padding:0;" type="checkbox" id="chkTerminado" name="chkTerminado" <% if cbool(terminada) then%> checked <%end if%> />
				<label for="chkTerminado" style="cursor:pointer; background:none; margin:0; padding:0;"><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto36")%></label>
                
                &nbsp;&nbsp;&nbsp;
                <%parametros="'" & modo & "','" & codAccion & "','" & codCliente & "','" & codCampana & "','" & tipoAccion & "','" & posCodigo &  "','" & tabla & "','" & filtro & "','" & valorFiltro & "'"
					if terminada then
						parametros=parametros & ",'1'"
					else
						parametros=parametros & ",'0'"						
					end if
					parametros=parametros & ",'" & accionAsociadaA & "'"


				%>
			    <input type="button" id="btnGuardarAccion" name="btnGuardarAccion"  value="<%if trim(lcase(modo))="modificacion" then response.Write(objIdiomaGeneral.getIdioma("BotonModificar_Texto1")) else response.Write(objIdiomaGeneral.getIdioma("BotonInsertar_Texto1")) end if%>" class="boton" onClick="javascrip:guardarDatos(<%=parametros%>);"  />         
			</div>  
            <%if trim(lcase(modo))="modificacion" And trim(codAccion)<>"" then %>
	            <div style="clear:both; width:100%; height:10px;"></div>            

    	        <div style="float:right; margin:0 30px 0 0;">
					<a  href="/dmcrm/app/produccion/inicio/SeleccionHorasAccionesComerciales.asp?conexion=conexionHoras&tabla=HORAS_ACCIONESCOMERCIALES&filtro=HAC_SUACCIONCOMERCIAL&valorfiltro=<%=codAccion%>"  class="botonImputar"  ><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto37")%></a> <!--onClick="abrirImputacionDeHoras('<%'=codAccion%>');" href="#" -->
            	</div>    
                
                <%
				descripComercial=""
				if trim(codAccion)<>"" And trim(session("codigoUsuario"))<>"" then
					'Horas imputadas a esta acci�n
                    sql=" Select sum(HAC_HORAS) as numHoras From HORAS_ACCIONESCOMERCIALES Where HAC_SUACCIONCOMERCIAL=" & codAccion 
					Set rsHorasAccion = connCRM.AbrirRecordSet(sql,3,1)
                    numHorasAccion=0
					if not rsHorasAccion.eof then
						if not isnull(rsHorasAccion("numHoras")) then numHorasAccion=rsHorasAccion("numHoras")
					end if
					rsHorasAccion.cerrar()
					set rsHorasAccion=nothing
                    'Horas imputadas a este ciclo
					sql=" Select sum(HAC_HORAS) as numHoras From HORAS_ACCIONESCOMERCIALES inner join ACCIONES_CLIENTE on ACCIONES_CLIENTE.ACCI_CODIGO=HORAS_ACCIONESCOMERCIALES.HAC_SUACCIONCOMERCIAL"
		            sql=sql &  " Where ACCI_SUCAMPANA=" & codCampana 
                    sql=sql &  " and ACCI_PROYECTO='" & proyecto & "'"
		            if esCliente then
			            sql=sql & " And ACCI_SUCLIENTE=" & codCliente 
		            elseif esContacto then
			            sql=sql & " And ACCI_SUCONTACTO=" & codCliente 		
		            end if	
					Set rsHorasCiclo = connCRM.AbrirRecordSet(sql,3,1)
                    numHorasCiclo=0
					if not rsHorasCiclo.eof then
                        if not isnull(rsHorasCiclo("numHoras")) then numHorasCiclo=rsHorasCiclo("numHoras")
					end if
					rsHorasCiclo.cerrar()
					set rsHorasCiclo=nothing
        
				end if	
				%>
                <div  style=" text-align:right; float:right;; margin:0 10px 0 0;  height:18px; border:none;" >
                	<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto38")%>&nbsp;<b><%=objIdioma.getIdioma("clientes_AccionesDatos_Texto39")%></b>&nbsp;<%=objIdioma.getIdioma("clientes_AccionesDatos_Texto41")%>&nbsp;&nbsp;<span id="bloqueHorasImputadas" style="font-weight:bold; font-size:14px;"><%=round(numHorasAccion,2)%>/<%=round(numHorasCiclo,2)%></span>
                </div>                
            <%end if %>            
    		
		<script type="text/javascript" >
			//en esta funci�n recargamos el codigo de la aci�n, del padre del frame donde se carga esta pantalla.
			refrescarCodAccion('<%=codAccion%>');
		</script>            

                  
        </div>    
    
    </form>

    <%'Alberto prueba%>


<style type="text/css">
.divBloquesOcultos { float:none; clear:none; display:inline;}
.botonImputar { background:silver; color:black; cursor:pointer; border:none; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; padding:3px 10px; text-decoration:none; float:right; }
.botonImputar:hover { background:#282828; color:#fff; cursor:pointer;}
</style>
   <!--include virtual="/dmcrm/APP/comun2/formMantenimientoCamposEditar.asp"  -->

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->




	</body>
</html>

