<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%
Function FormatoEuros(valor)
   if not isnull(valor) then 
        FormatoEuros = FormatNumber(valor, 2) & " �"
   end if
end function
%>

<script language=javascript>
$(document).ready(function() {
	$('#fechainicio').datePicker({startDate:'01/01/1900',clickInput:true});
	$('#fechainicio').dpSetOffset(20, 0);
	
	$('#fechaFin').datePicker({startDate:'01/01/1900',clickInput:true});
	$('#fechaFin').dpSetOffset(20, -50);	
});
			
function recarga(){
	tmp2=document.forms.fPaginacion2.comboVerComercial;
	tmp3=document.forms.fPaginacion2.comboVerCampanas;
	tmp4=document.forms.fPaginacion2.comboVerEstadosCliente;
	tmp5=document.forms.fPaginacion2.comboVerEstadosPropuestas;
	tmp6=document.forms.fPaginacion2.comboVerFinalizadas;
	tmp7=document.forms.fPaginacion2.comboVerAnos;
	tmp8=document.forms.fPaginacion2.fechainicio;
	tmp9=document.forms.fPaginacion2.fechafin;	
	document.forms.fPaginacion2.comboVerComercial.value=tmp2.options[tmp2.selectedIndex].value;
	document.forms.fPaginacion2.comboVerCampanas.value=tmp3.options[tmp3.selectedIndex].value;
	document.forms.fPaginacion2.comboVerEstadosCliente.value=tmp4.options[tmp4.selectedIndex].value;
	document.forms.fPaginacion2.comboVerEstadosPropuestas.value=tmp5.options[tmp5.selectedIndex].value;
	document.forms.fPaginacion2.comboVerFinalizadas.value=tmp6.options[tmp6.selectedIndex].value;
	document.forms.fPaginacion2.comboVerAnos.value=tmp7.value;
	document.forms.fPaginacion2.fechainicio.value=tmp8.value;
	document.forms.fPaginacion2.fechafin.value=tmp9.value;		
	document.location='ControlDeOfertas.asp?co=' + tmp2.options[tmp2.selectedIndex].value +'&ca=' + tmp3.options[tmp3.selectedIndex].value  +'&gr=' + tmp4.options[tmp4.selectedIndex].value +'&te=' + tmp5.options[tmp5.selectedIndex].value + '&fi=' + tmp6.options[tmp6.selectedIndex].value + '&an=' + tmp7.options[tmp7.selectedIndex].value + '&fin=' + tmp8.value + '&ffn=' + tmp9.value;
	//}
}

				$(".cEmergente").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'onClosed'		        : function() {
							    recarga();
						}												
					}).trigger("click"); 
					return false; 	
				});
</script>


<!-- CABECERA DE LA SELECCION -->

<h1><%=objIdioma.getIdioma("CControlDeOfertas_Titulo")%></h1>


<%'############################################################################################%>
<form name="fPaginacion2" method="POST" ID="Form1">



<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeOfertas_Texto1")%></span><br />
	<div>
        <select name="comboVerComercial" class="combos" ID="Select1">
            <%sql="Select distinct(analistas.ana_nombre),ana_codigo  from (acciones_cliente inner join analistas on acciones_cliente.acci_sucomercial=analistas.ana_codigo)"
              sql = sql & " where acci_sutipo=6 and ana_directorcuenta = 1" 		
            sql=sql & " order by analistas.ana_nombre"
            Set rstComerciales= connCRM.AbrirRecordset(sql,3,3)
            %><option value="0"<%if cint(request("co")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option><%
            while not rstComerciales.eof%>
                <option value="<%=rstComerciales("ana_codigo")%>"<%if cint(request("co")) = rstComerciales("ana_codigo") then response.Write(" selected")%>><%=rstComerciales("ana_nombre")%></option>
                <%rstComerciales.movenext
            wend
            rstComerciales.cerrar
            set rstComerciales=nothing%>
        </select>
	</div>
</div>

<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeOfertas_Texto2")%></span><br />
	<div>
    <select name="comboVerCampanas"  class="combos" ID="Select1">
        <%sql="Select distinct(campanas.camp_nombre),camp_codigo  from (acciones_cliente inner join campanas on acciones_cliente.acci_sucampana=campanas.camp_codigo)"
          sql = sql & " where acci_sutipo=6 " 	
        sql=sql & " order by campanas.camp_nombre"
        Set rstCampanas= connCRM.AbrirRecordset(sql,3,3)
        %><option value="0"<%if cint(request("ca")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto2")%></option><%
        while not rstCampanas.eof%>
            <option value="<%=rstCampanas("camp_codigo")%>"<%if cint(request("ca")) = rstCampanas("camp_codigo") then response.Write(" selected")%>><%=rstCampanas("camp_nombre")%></option>
            <%rstCampanas.movenext
        wend
        rstCampanas.cerrar
        set rstCampanas=nothing    %>
    </select>
	</div>
</div>

<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeOfertas_Texto10")%></span><br />
	<div>
            <span class="cab2"><%=objIdioma.getIdioma("ControlDeOfertas_Texto3")%></span>
            <select name="comboVerEstadosCliente" class="combos" ID="Select1">
                <%
					sql=" Select T.TAPEC_CODIGO,(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN T.TAPEC_DESCRIPCION ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TAPEC_DESCRIPCION "
					sql=sql & " From TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE T "
					sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TAPEC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE' And D.IDID_CAMPO='TAPEC_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "

                Set rstEstadosCliente= connCRM.AbrirRecordset(sql,0,1)
                %><option value="0"<%if cint(request("gr")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option><%
                while not rstEstadosCliente.eof%>
                    <option value="<%=rstEstadosCliente("TAPEC_CODIGO")%>"<%if cint(request("gr")) = rstEstadosCliente("TAPEC_CODIGO") then response.Write(" selected")%>><%=rstEstadosCliente("TAPEC_DESCRIPCION")%></option>
                    <%rstEstadosCliente.movenext
                wend
                rstEstadosCliente.cerrar
                set rstEstadosCliente=nothing%>                
            </select>
        
            <span class="cab2"><%=objIdioma.getIdioma("ControlDeOfertas_Texto4")%></span>
            <select name="comboVerAnos" class="combos" ID="Select4">
                <%sql="Select distinct(year(acci_fecha)) as Ano from acciones_cliente " 		
                sql=sql & " order by year(acci_fecha) desc "
                Set rstAnos= connCRM.AbrirRecordset(sql,0,1)
                %><option value="0"<%if cint(request("an")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option><%
                while not rstAnos.eof%>
                    <option value="<%=rstAnos("Ano")%>"<%if cint(request("an")) = rstAnos("Ano") then response.Write(" selected")%>><%=rstAnos("Ano")%></option>
                    <%rstAnos.movenext
                wend
                rstAnos.cerrar
                set rstAnos=nothing%>
            </select>
	</div>
</div>


<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeOfertas_Texto5")%></span><br />
	<div>
            <span class="cab2"><%=objIdioma.getIdioma("ControlDeOfertas_Texto6")%></span>
            <select name="comboVerEstadosPropuestas" class="combos" ID="Select2">
				<%sql=" Select T.TAPF_CODIGO,(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN T.TAPF_DESCRIPCION ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TAPF_DESCRIPCION "
				  sql=sql & " From TIPOS_AUX_PROPUESTA_FINALIZACION T "
				  sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TAPF_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_AUX_PROPUESTA_FINALIZACION' And D.IDID_CAMPO='TAPF_DESCRIPCION' And D.IDID_SUIDIOMA='" & Idioma & "') "
                 Set rstEstadosCliente= connCRM.AbrirRecordset(sql,0,1)%>
	             <option value="0"<%if request("te") = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto3")%></option>
                 <%while not rstEstadosCliente.EOF   %>
	                <option value="<%=rstEstadosCliente("TAPF_CODIGO")%>" <%if trim(cstr(request("te"))) = trim(cstr(rstEstadosCliente("TAPF_CODIGO"))) then response.Write(" selected")%>><%=rstEstadosCliente("TAPF_DESCRIPCION")%></option>
                 <%rstEstadosCliente.movenext
				 wend
				 rstEstadosCliente.cerrar
                 set rstEstadosCliente=nothing%>   
            </select>
        
            <span class="cab2"><%=objIdioma.getIdioma("ControlDeOfertas_Texto7")%></span>
            <select name="comboVerFinalizadas" class="combos" ID="Select3">
                <option value="0"<%if request("fi") = 0 or request("fi")="" then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("EstadoAsistencias_Texto1")%></option>
                <option value="1"<%if request("fi") = 1 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("EstadoAsistencias_Texto2")%></option>
                <option value="2"<%if request("fi") = 2 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto3")%></option>
            </select>
            
            <span class="cab2"><%=objIdioma.getIdioma("ControlDeOfertas_Texto8")%></span><input type=text id="fechainicio" value="<%=request("fin")%>" name="fechainicio" size=12>
            <span class="cab2"><%=objIdioma.getIdioma("ControlDeOfertas_Texto9")%></span><input type=text id="fechaFin" value="<%=request("ffn")%>" NAME="fechafin" size=12>
	</div>
</div>
<div class="FiltradoCab">
<span class="cab1"></span><br />
	<div>
    	<input class="boton" type="button" onclick="javascript:recarga();" value="<%=objIdiomaGeneral.getIdioma("BotonBuscar_Texto1")%>">
    </div>
</div>

</form>
<%'############################################################################################%>

<%
sql="select * "
sql=sql & " ,(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN tipos_campanas.TICA_TIPO ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TICA_TIPO_IDI "	 
sql=sql & " ,(CASE WHEN LTRIM(RTRIM(CAST(D2.IDID_DESCRIPCION AS varchar(max))))='' Or D2.IDID_DESCRIPCION IS NULL THEN EPC.TAPEC_DESCRIPCION ELSE CAST(D2.IDID_DESCRIPCION AS varchar(max)) END) as TAPEC_DESCRIPCION_IDI "	 
sql=sql & " from (((((((acciones_cliente as A "
sql=sql & " inner join analistas on A.acci_sucomercial=analistas.ana_codigo) "
sql=sql & " inner join clientes on clientes.clie_codigo=A.acci_sucliente) "
sql=sql & " inner join campanas on campanas.camp_codigo=A.acci_sucampana) "
sql=sql & " left join tipos_campanas on campanas.camp_sutipo=tipos_campanas.tica_codigo)  "
sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (tipos_campanas.TICA_CODIGO=D.IDID_INDICE And UPPER(D.IDID_TABLA)='TIPOS_CAMPANAS' And UPPER(D.IDID_CAMPO)='TICA_TIPO' And D.IDID_SUIDIOMA='" & Idioma & "')) "	
sql=sql & " left join TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE EPC on A.acci_tipo6EstadoCliente=EPC.TAPEC_CODIGO ) "
sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D2 on (EPC.TAPEC_CODIGO=D2.IDID_INDICE And UPPER(D2.IDID_TABLA)='TIPOS_AUX_PROPUESTA_ESTADO_CLIENTE' And UPPER(D2.IDID_CAMPO)='TAPEC_DESCRIPCION' And D2.IDID_SUIDIOMA='" & Idioma & "')) "	
sql = sql & " where acci_sutipo=6 "
if request("an")<>"" and request("an")<>"0" then	 	
    sql=sql & " and year(acci_fecha)=" & request("an") 
end if 
if request("co")<>"" and request("co")<>"0" then
  sql = sql & " and A.acci_sucomercial=" & request("co")
end if
if request("ca")<>"" and request("ca")<>"0" then
  sql = sql & " and A.acci_sucampana=" & request("ca")
end if
if request("gr")<>"" and request("gr")<>"0" then
  sql = sql & " and A.acci_tipo6EstadoCliente=" & request("gr")
end if
if (request("te")<>"" and request("te")<>"0") or (request("fi")<>"2") or (request("an")<>"" and request("an")<>0) or (request("fin")<>"" or request("ffn")<>"") then

	  	'Apartado de los SEGUIMIENTOS, tipo 7
		sql=sql & " and EXISTS (select * from acciones_cliente as B where B.acci_sutipo=7 "
		sql=sql & " and B.acci_sucampana=A.Acci_Sucampana "
		sql=sql & " and B.acci_sucliente=A.Acci_suCliente "		
		sql=sql & " and B.acci_proyecto=A.Acci_proyecto "	
		if request("te")<>"" and request("te")<>"0" then	 	
            sql=sql & " and B.acci_tipo7Finalizacion=" & request("te") 
        end if
		if request("fi")<>"2" then
		    select case request("fi")
		        case 0
		            sql=sql & " and B.acci_terminada=0" 
		        case 1	 	
                    sql=sql & " and B.acci_terminada=1"
            end select
        end if
		if request("fin")<>"" then	 	
            sql=sql & " and B.acci_fecha>='" & FormatearFechaSQLSever(request("fin")) & "'"
        end if
		if request("ffn")<>"" then	 	
            sql=sql & " and B.acci_fecha<='" & FormatearFechaSQLSever(request("ffn")) & "'"
        end if        
        sql=sql & ") "

end if

sql=sql & " order by ana_nombre,clie_nombre,A.acci_fecha,A.acci_tipo6EstadoCliente " 
'ContadorPosCodigo=0
obtenerCodigos=""

Set rstNC= connCRM.AbrirRecordset(sql,0,1)


'Creamos un array de campa�as con los datos que pintaremos en el resumen
'1-codigo campa�a,2-nombre campa�a, 3-num propuestas, 4-Cantidad presupuestada,5-Num Aprobados,6-Cantidad aprobada
    dim arrCampanas(300,7)
	sql="select (CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN tipos_campanas.TICA_TIPO ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TICA_TIPO_IDI,* "
	sql=sql & " from ((campanas "
	sql=sql & " left join tipos_campanas on campanas.camp_sutipo=tipos_campanas.tica_codigo) "
	sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (tipos_campanas.TICA_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_CAMPANAS' And D.IDID_CAMPO='TICA_TIPO' And D.IDID_SUIDIOMA='" & Idioma & "')) "	
	sql=sql & " order by 1 " 'camp_finicio desc" 
	Set rstCamp= connCRM.AbrirRecordset(sql,0,1)
	cont=0
    while not rstCamp.eof
        arrCampanas(cont,1)=rstCamp("Camp_codigo")
        arrCampanas(cont,2)=rstCamp("Camp_nombre")
        arrCampanas(cont,7)=rstCamp("TICA_TIPO_IDI")
        cont=cont+1
        rstCamp.movenext
    wend
    rstCamp.cerrar
    set rstCamp=nothing

Comercial=""
totalRealizado=0
totalAprobado=0
totalRealizadoComercial=0
PropuestasComercial=0



while not rstNC.eof
    
    if Comercial<>rstNC("ana_codigo") then 'Ponemos la cabecera

        'Inicializamos contadores y c�digo comercial
        Comercial=rstNC("ana_codigo") 
		TotalRealizadoComercial=0
		TotalPropuestasComercial=0    
		TotalAprobadoComercial=0  

        %>

		<TABLE cellSpacing="1" cellPadding="2" width="100%" border="0" style="margin:20px 0 0 0;">
              <tr class="nombre">
              	<td colspan="13"><%=rstNC("ana_nombre")%></td>
              </tr>
		      <tr class="titularT"> 
					<td height="25" colspan="2" class="arriba">&nbsp;
                    	
		            </td>
		            
					<td height="25" colspan="4" align="center" class="arriba">
			            <%=objIdioma.getIdioma("ControlDeOfertas_Texto13")%>
		            </td>
		     	   
					<td height="25" colspan="5" align="center" class="arriba">
                    	<%=objIdioma.getIdioma("ControlDeOfertas_Texto14")%>
		            </td>
		            
					<td height="25" class="arriba derecha" colspan=2>&nbsp;</td>
			   </tr> 
		      <tr class="titularT"> 
		       
                    <td height="25"><%=objIdioma.getIdioma("ControlDeOfertas_Texto15")%><br /><i><%=objIdioma.getIdioma("ControlDeOfertas_Texto16")%></i></td>
                    <td height="25"><%=objIdioma.getIdioma("ControlDeOfertas_Texto17")%><br /><i><%=objIdioma.getIdioma("ControlDeOfertas_Texto18")%></i></td>
                    <td height="25" align="center"><%=objIdioma.getIdioma("ControlDeOfertas_Texto19")%></td>
                    <!--<td height="25"><%=objIdioma.getIdioma("ControlDeOfertas_Texto20")%></td>-->
					<td height="25" align="right" class="cantidad"><%=objIdioma.getIdioma("ControlDeOfertas_Texto21")%></td>
		  			<td height="25" align="center"><%=objIdioma.getIdioma("ControlDeOfertas_Texto22")%></td>
					<td height="25"><%=objIdioma.getIdioma("ControlDeOfertas_Texto23")%></td>
					<td height="25" align="center"><%=objIdioma.getIdioma("ControlDeOfertas_Texto19")%></td>
					<!--<td height="25"><%=objIdioma.getIdioma("ControlDeOfertas_Texto20")%></td>-->
		 			<td height="25" align="right" class="cantidad"><%=objIdioma.getIdioma("ControlDeOfertas_Texto21")%></td>
		 			
		 			<td height="25" align="center">
		             <%=objIdioma.getIdioma("ControlDeOfertas_Texto24")%>
		            </td>
		 			
                    <td height="25" align="center">
		             <%=objIdioma.getIdioma("ControlDeOfertas_Texto25")%>
		            </td>
		     	   
					<td height="25" align="center">
		             <%=objIdioma.getIdioma("ControlDeOfertas_Texto23")%>
		            </td>
		            
					<td height="25" class="derecha" colspan=2>&nbsp;</td>		     
			   </tr> 
	   
       <%end if%>
       <%
        'Totales generales y del comercial
		totalPropuestas=totalPropuestas+1
        TotalPropuestasComercial=TotalPropuestasComercial+1
        if not isnull(rstNC("acci_tipo6Cantidad")) then
            totalRealizado=totalRealizado+rstNC("acci_tipo6Cantidad")
            TotalRealizadoComercial=TotalRealizadoComercial+rstNC("acci_tipo6Cantidad")
        end if
        
        'Introduzco los datos en el array de Campa�as
        for intI=0 to ubound(arrCampanas)
           if arrCampanas(intI,1)=rstNC("camp_codigo") then
              arrCampanas(intI,3)=arrCampanas(intI,3)+1
              arrCampanas(intI,4)=arrCampanas(intI,4)+rstNC("acci_tipo6Cantidad")
           end if
        next
        %>       
        
        
        <%
		if trim(obtenerCodigos)<>"" then
			obtenerCodigos=obtenerCodigos & ","
		end if
		obtenerCodigos=obtenerCodigos & rstNC("acci_codigo")
'  		ContadorPosCodigo=ContadorPosCodigo+1
		
	  	'Apartado de los SEGUIMIENTOS, tipo 7
        sql="select * from acciones_cliente where acci_sutipo=7 "
		sql=sql & " and acci_proyecto='" & rstNC("acci_proyecto") & "'"
		sql=sql & " and acci_sucliente= " & rstNC("acci_suCliente")
		sql=sql & " and acci_sucampana= " & rstNC("acci_suCampana") 			 
		Set rstSeg= connCRM.AbrirRecordset(sql,3,3)

	  	%>
        
     <%'S�LO PINTO SEG�N EL FILTRO request("fi")  
     if request("fi")=0 then 'abierta
        blnTerminada=false 
     else
        if request("fi")=1 then
          blnTerminada=true
        end if
     end if
     
     if not rstSeg.eof then
   ''  Response.Write "ter=" & rstSeg("ACCI_TERMINADA")
      if blnTerminada=rstSeg("ACCI_TERMINADA") or  request("fi")="" or request("fi")=2  then 
        strComentario6=rstNC("acci_comentario")%>
	   
        <tr class="celdaBlanca2">
	   
	  	<td valign="top">  		
	  	    <b><%=ucase(rstNC("clie_nombre"))%></b><br>
            <i>[<%=rstNC("ACCI_PROYECTO")%>]</i>
	  	 </td>
         <td valign="top">
	  	    <%'Pinto el nombre de la campa�a s�lo si no es de distribuidor
	  	       Response.write ucase(rstNC("camp_nombre"))%>	  	    
	  	    <i><br />
            [<%=rstNC("TICA_TIPO_IDI") %>]
	  	    <%if instr(UCASE(rstNC("tica_tipo")),"DISTRIBU")>0 then
	  	        'si la campa�a es de distribuci�n pinto tambi�n el DISTRIBUIDOR del cliente
	  	        'Hallamos el distribuidor si corresponde
				sql="select clie_nombre from clientes  "
				sql=sql & " where  clie_codigo=" & rstNC("clie_sudistribuidor")
				Set rstDistribuidor= connCRM.AbrirRecordset(sql,0,1)
				if not rstDistribuidor.eof then
					Response.Write "<br><b>" & rstDistribuidor("clie_nombre") & "</b>"
				end if
				rstDistribuidor.cerrar
                set rstDistribuidor=nothing
	  	    end if%>
            </i>&nbsp;
	  	</td>
					   
	  	<td align="center" valign="top">
	  	    <%=rstNC("acci_fecha")%>&nbsp;
	  	</td>					  
				  
	  	<td align="right" valign="top">
	  	    <%=FormatoEuros(rstNC("acci_tipo6Cantidad"))%>&nbsp;
	  	</td>					  

	  	<td align="center" valign="top">
	  	    <strong><%=rstNC("TAPEC_DESCRIPCION_IDI")%></strong>&nbsp;
	  	</td>
			
	  	<td valign="top" class="BorderDcha">
	  	    <% if rstNC("acci_terminada") then%>
            	<img src="../imagenes/green.png" width="30">
            <%else%>
            	<img src="../imagenes/red.png" width="30">
           	<%end if%>
	  	</td>
			
            <%'ZONA SEGUIMIENTOS%>
	  	<%if not rstSeg.eof then	
	  		
	  		strComentario7=rstSeg("acci_comentario")

	  		'Introduzco los datos en el array de Campa�as
			for intI=0 to ubound(arrCampanas)
				if arrCampanas(intI,1)=rstNC("camp_codigo") then
				    if rstSeg("acci_tipo7finalizacion")=1 or rstSeg("acci_tipo7finalizacion")=3 then
						arrCampanas(intI,5)=arrCampanas(intI,5)+1
						arrCampanas(intI,6)=arrCampanas(intI,6)+rstSeg("acci_tipo6Cantidad")-rstSeg("acci_tipo7Gastos")
					end if
				end if
			next
	  			
	  		'Si est� aprobada cuento la cantidad
            if not isnull(rstSeg("acci_tipo6Cantidad")) then
	  		    if rstSeg("acci_tipo7finalizacion")=1 or rstSeg("acci_tipo7finalizacion")=3 then
	  			    TotalAprobadoComercial=TotalAprobadoComercial+rstSeg("acci_tipo6Cantidad")
	  			    TotalAprobado=TotalAprobado+rstSeg("acci_tipo6Cantidad")
                    if not isnull(rstSeg("acci_tipo7Gastos")) then
	  			        TotalAprobadoComercial=TotalAprobadoComercial - rstSeg("acci_tipo7Gastos")
	  			        TotalAprobado=TotalAprobado - rstSeg("acci_tipo7Gastos")
                    end if
	  	        end if
            end if
	  	    'Comprobamos si esta campa�a termin� para el cliente para el contador final de cantidades ofertadas
	  	    if rstSeg("acci_terminada") then
                if not isnull(rstNC("acci_tipo6Cantidad")) then
	  	            TotalRealizado=TotalRealizado-rstNC("acci_tipo6Cantidad")
                    if not isnull(rstSeg("acci_tipo7Gastos")) then
	  			        TotalRealizado=TotalRealizado - rstSeg("acci_tipo7Gastos")
                    end if
                end if
		    end if%>					   
		    
	  		<td align="center" valign="top">
	  		    <%=rstSeg("acci_fecha")%>&nbsp;
	  		</td>					  
 	
	  		<td align="right" valign="top">
	  			<%=FormatoEuros(rstSeg("acci_tipo6Cantidad"))%><br>
	  			<span color="ff0000">
	  			<%
	  			if not isnull(rstSeg("acci_tipo7Gastos")) then
	  			   response.write FormatoEuros(rstSeg("acci_tipo7Gastos"))
	  			end if%></span>&nbsp;
	  		</td>
	  		<td align="center" valign="top">
	  	              <% if rstSeg("acci_tipo7Finalizacion")=1 then%>
                        <img src="../imagenes/ok_bola.png" width="30">
                      <%elseif rstSeg("acci_Tipo7finalizacion")=3 then%>
						PRE<br /><img src="../imagenes/ok_bola.png" width="30">
                      <%elseif rstSeg("acci_Tipo7finalizacion")=2 then%>
                        <img src='/dmcrm/app/imagenes/nook1.gif'>
                      <%end if%>
            </td>
	  	
	  		<td align="center" valign="top">
                <%'Horas imputadas a este ciclo
	            sql=" Select sum(HAC_HORAS) as numHoras From HORAS_ACCIONESCOMERCIALES inner join ACCIONES_CLIENTE on ACCIONES_CLIENTE.ACCI_CODIGO=HORAS_ACCIONESCOMERCIALES.HAC_SUACCIONCOMERCIAL"
	            sql=sql &  " Where ACCI_SUCAMPANA=" & rstSeg("acci_sucampana") 
                sql=sql &  " and ACCI_PROYECTO='" & rstSeg("acci_proyecto")  & "'"
	            if isnull(rstSeg("acci_sucliente")) then
		            sql=sql & " And ACCI_SUCONTACTO=" & rstSeg("acci_sucontacto") 
	            else
		            sql=sql & " And ACCI_SUCLIENTE=" & rstSeg("acci_sucliente") 		
	            end if	
	            Set rsHorasCiclo = connCRM.AbrirRecordSet(sql,3,1)
                numHorasCiclo=0
	            if not rsHorasCiclo.eof then
                    if not isnull(rsHorasCiclo("numHoras")) then numHorasCiclo=rsHorasCiclo("numHoras")
	            end if
	            rsHorasCiclo.cerrar()
	            set rsHorasCiclo=nothing%>
                <%=numHorasCiclo%> h.
            </td>
			
	  		<td align="center" valign="top">
			  <% if rstSeg("acci_terminada") then%>
                  <img src="../imagenes/green.png" width="30">
              <%else%>
                  <img src="../imagenes/red.png" width="30">
			  <%end if%>
	  		</td>	
           <%'PINTAMOS EL ESTADO GLOBAL DE LA PROPUESTA, ES DECIR, SI SE HA PASADO LA FECHA DE INTERESARSE POR LA ACCION%>
	  	   <td align="center" valign="top" class="derecha">
	  		  <%datFechaNC= rstNC("acci_fecha")'month(rstNC("acci_fecha"))& "/" & day(rstNC("acci_fecha"))& "/" &year(rstNC("acci_fecha"))
	  		    datFechaSeg=rstSeg("acci_fecha") 'month(rstSeg("acci_fecha"))& "/" & day(rstSeg("acci_fecha"))& "/" &year(rstSeg("acci_fecha"))
	  		    datHoy= date() 'month(date())& "/" & day(date()) & "/" & year(date())
	  		  if (datediff("d",datFechaNC,datHoy)>0 and not rstNC("acci_terminada")) OR (datediff("d",datFechaSeg,datHoy)>0 and not rstSeg("acci_terminada")) then%>
                            <img src="../imagenes/alert.png" width="30">
              <%end if%>
	  		<%if rstSeg.recordcount>1 then
	  		   Response.Write objIdioma.getIdioma("ControlDeOfertas_Texto29")
	  		end if%>
            </td>
            <%'Edici�n de la acci�n %>
            <td style="vertical-align:top;">
            <a style="top:0px;" class="cEmergente" href="/dmcrm/APP/Comercial/Empresas/clientes_Acciones.asp?codigo=<%=rstSeg("ACCI_CODIGO")%>&modo=modificacion&tabla=ACCIONES_CLIENTE&posCodigo=<%=ContadorPosCodigo%>"><img src="/dmcrm/APP/Comun2/imagenes/c0.gif" title="<%=objIdioma.getIdioma("ControlDeOfertas_Texto40")%>" /></a>
	  	   </td>	
  	  <%else%>
	  		<td valign="top">&nbsp;</td>					  
	  		<td valign="top">&nbsp;</td>
	  		<td valign="top">&nbsp;</td>	
	  		<td valign="top">&nbsp;</td>	
	  		<td valign="top">&nbsp;</td>
             <%'PINTAMOS EL ESTADO GLOBAL DE LA PROPUESTA, ES DECIR, SI SE HA PASADO LA FECHA DE INTERESARSE POR LA ACCION%>
	  	    <td valign="top" class="derecha">
	  		    <%if (rstNC("acci_fecha")<=date() and not rstNC("acci_terminada"))  then%>
                            <img src="/dmcrm/app/imagenes/atencion1.gif">
                <%end if%>
	  	     </td>	
             <td valign="top">&nbsp;</td>										  
	  	<%end if%>
							 
		
	</tr>  
    <%'Pintamos los cometarios del tipo 6 y 7%>
    <tr class="celdaBlanca">
        <td colspan=2>&nbsp;</td>
        <td colspan=4 valign=top <%if strComentario6<>"" then response.write "style=""background: #fde8da"""%>><%=strComentario6%></td>
        <td colspan=5 valign=top <%if strComentario7<>"" then response.write "style=""background: #fdddc7"""%>><%=strComentario7%></td>
        <td colspan=2>&nbsp;</td>
    </tr>   
	
    <%
     end if  'FILTRO REQUEST("FI")
    end if    
    rstNC.movenext
    
    'Para eliminar el error de eof
    on error resume next
    if rstNC.eof or rstNC("ana_codigo")<>Comercial then 'Ponemos el pie de la tabla pues ha acabado el comercial%>
				     <tr class="titular22"> 
			       		<td colspan="3"><%=TotalPropuestasComercial & " " & objIdioma.getIdioma("ControlDeOfertas_Texto26")%></td>
						<td colspan="2" align="right"><%=objIdioma.getIdioma("ControlDeOfertas_Texto27") & " " & FormatoEuros(TotalRealizadoComercial)%></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td colspan="2" align="right"><%=objIdioma.getIdioma("ControlDeOfertas_Texto28") & " " & FormatoEuros(TotalAprobadoComercial)%></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
                        <td align="right" colspan=3>&nbsp;</td>
                    </tr>  

			    </table>
	<%
		end if
		'end if%>
	
	         
    <%  
        rstSeg.cerrar
        set rstSeg=nothing
wend%>

<br><br>

 	<font face="verdana" size="1">
      <%=objIdioma.getIdioma("ControlDeOfertas_Texto30")%>
     </font>
 	<font face="verdana" size="1">
 	<b><%=TotalPropuestas%></b>
 	</font>
 	<br>
 	<font face="verdana" size="1">
      <%=objIdioma.getIdioma("ControlDeOfertas_Texto31")%>
     </font>
 	<font face="verdana" size="1">
 	<b><%=FormatoEuros(TotalRealizado)%></b>
 	</font>
 	<br>
 	<font face="verdana" size="1">
      <%=objIdioma.getIdioma("ControlDeOfertas_Texto32")%>
     </font>
 	<font face="verdana" size="1">
 	<b><%=FormatoEuros(TotalAprobado)%></b>
 	</font>

<%'############################################################################################%>
<%'############################################################################################%>

<%'Pintamos el resumen de campa�as%>
<br><br>
<table width="800" border="0" cellpadding="0" cellspacing="1">
	<tr class="nombre">
		<td><%=objIdioma.getIdioma("ControlDeOfertas_Texto33")%></td>
        <td align="center"><%=objIdioma.getIdioma("ControlDeOfertas_Texto34")%></td>
		<td align="center"><%=objIdioma.getIdioma("ControlDeOfertas_Texto35")%></td>
		<td align="right"><%=objIdioma.getIdioma("ControlDeOfertas_Texto36")%></td>
		<td align="center"><%=objIdioma.getIdioma("ControlDeOfertas_Texto37")%></td>
		<td align="right"><%=objIdioma.getIdioma("ControlDeOfertas_Texto38")%></td>
	</tr>
  
<%'if Idioma="es" then strTipoAnterior="Distribuidores" else strTipoAnterior ="Banatzaileak"
strTipoAnterior=""
for intI=0 to ubound(arrCampanas)
    if arrCampanas(intI,3)<>"" then%>
        <%'Ponemos una fila de subtotal al finalizar un tipo de campa�a
        if strTipoAnterior<>arrCampanas(intI,7) then
            if strTipoAnterior<>"" then%>
            <tr bgcolor=cccccc>
                <td colspan=2 align=right><b>Subtotal <%=strTipoAnterior%></td>
                <td align=center><b><%=SubNumProp%></td>
                <td align=right><b><%=formatoEuros(SubCantPres)%></td>
                <td align=center><b><%=SubNumApr%></td>
                <td align=right><b><%=formatoEuros(SubCantApro)%></td>
            </tr>
            <%end if
            'Inicializamos las variables
            strTipoAnterior=arrCampanas(intI,7)
            SubNumProp=0
            SubCantPres=0
            SubNumApr=0
            SubCantApro=0%>
        <%
        end if%>
        <%'A�adimos a subtotales
        SubNumProp=SubNumProp+arrCampanas(intI,3)
        SubCantPres=SubCantPres+arrCampanas(intI,4)
        SubNumApr=SubNumApr+arrCampanas(intI,5)
        SubCantApro=SubCantApro+arrCampanas(intI,6)%>
        <tr class="celdaBlanca">
            <td><%=arrCampanas(intI,2)%></td>
            <td align="RIGHT"><%=arrCampanas(intI,7)%></td>
            <td align=center ><%=arrCampanas(intI,3)%></td>
            <%totalColumna1=totalColumna1+arrCampanas(intI,3)%>
            <td align=right ><%=FormatoEuros(arrCampanas(intI,4))%></td>
            <%totalColumna2=totalColumna2+arrCampanas(intI,4)%>
            <td align=center ><%=arrCampanas(intI,5)%></td>
            <%totalColumna3=totalColumna3+arrCampanas(intI,5)%>
            <td align=right ><%=formatoEuros(arrCampanas(intI,6))%></td>
            <%totalColumna4=totalColumna4+arrCampanas(intI,6)%>
        </tr>
    <%end if
next
%>
    <%'Pintamos el �ltimo subtotal %>
    <tr bgcolor=cccccc>
        <td colspan=2 align=right><b>Subtotal <%=strTipoAnterior%></td>
        <td align=center><b><%=SubNumProp%></td>
        <td align=right><b><%=formatoEuros(SubCantPres)%></td>
        <td align=center><b><%=SubNumApr%></td>
        <td align=right><b><%=formatoEuros(SubCantApro)%></td>
    </tr>
	<tr class="nombre">
		<td colspan=2 align=right><%=objIdioma.getIdioma("ControlDeOfertas_Texto39")%></td>
		<td align=center><%=totalColumna1%></td>
		<td align="right"><%=formatoEuros(totalColumna2)%></td>
		<td align=center><%=totalColumna3%></td>
		<td align="right"><%=formatoEuros(totalColumna4)%></td>
	</tr>
</table>


<%

session(denominacion & "ACCIONES_CLIENTE" & "_codigosObtenido")=obtenerCodigos
rstNC.cerrar
%>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->

