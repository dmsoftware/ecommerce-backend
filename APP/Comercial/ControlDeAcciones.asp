<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%Sub FormatoEuros(valor)
 IF not isnull(valor) then response.Write FormatNumber(valor, 2) & " �"
end sub%>


<h1><%=objIdioma.getIdioma("ControlDeAcciones_Titulo")%></h1>
<p><%=objIdioma.getIdioma("ControlDeAcciones_Texto1")%></p>
<br />

<script language=javascript>
    function recargaAcciones() {
	tmp2=document.forms.fPaginacion2.comboVerTipos;
	document.forms.fPaginacion2.comboVerTipos.value=tmp2.options[tmp2.selectedIndex].value;
	tmp3=document.forms.fPaginacion2.comboVerComercial;
	document.forms.fPaginacion2.comboVerComercial.value=tmp3.options[tmp3.selectedIndex].value;
	tmp4 = document.forms.fPaginacion2.comboTerminada;
	document.forms.fPaginacion2.comboTerminada.value = tmp4.options[tmp4.selectedIndex].value;
	document.location = 'ControlDeAcciones.asp?t=' + tmp2.options[tmp2.selectedIndex].value + '&per=' + tmp3.options[tmp3.selectedIndex].value + '&ter=' + tmp4.options[tmp4.selectedIndex].value;
	//}
}
</script>



<%'############################################################################################%>
<form name="fPaginacion2" method="POST" ID="Form1">
<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeAcciones_Texto2")%></span><br />
	<div>
<select name="comboVerTipos" onchange="javascript:recargaAcciones();" class="combos" ID="Select1">
	<option value="0"<%if cint(request("t")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto2")%></option>
	<%sql=" Select T.TIAC_CODIGO,(CASE WHEN LTRIM(RTRIM(CAST(D.IDID_DESCRIPCION AS varchar(max))))='' Or D.IDID_DESCRIPCION IS NULL THEN T.TIAC_NOMBRE ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TIAC_NOMBRE "
		sql=sql & "From TIPOS_ACCION T "
		sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (T.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "') "
		sql=sql & " ORDER BY T.TIAC_CODIGO "	
		Set rsTipoAccion = connCRM.AbrirRecordSet(sql,3,1)
		while not rsTipoAccion.eof
			%><option value="<%=rsTipoAccion("TIAC_CODIGO")%>"  <%if cint(rsTipoAccion("TIAC_CODIGO"))=cint(request("t")) then response.Write("selected") end if%> ><%=rsTipoAccion("TIAC_NOMBRE")%></option><%
			rsTipoAccion.movenext
		wend
		rsTipoAccion.cerrar()
		set rsTipoAccion=nothing %>
</select>
	</div>
</div>

<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeAcciones_Texto3")%></span><br />
	<div>
    <select name="comboVerComercial" onchange="javascript:recargaAcciones();" class="combos" ID="Select1">
	    <%
	    sql="select distinct(ana_nombre),ana_codigo from acciones_cliente inner join analistas on acciones_cliente.acci_sucomercial=analistas.ana_codigo where ana_directorcuenta = 1"
	    sql=sql & " order by ana_nombre"
	    Set rstComercial= connCRM.AbrirRecordset(sql,3,3)
        %><option value="0"<%if cint(request("per")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option><%
	    while not rstComercial.eof%>
		    <option value="<%=rstComercial("ana_codigo")%>"<%if cint(request("per")) = rstComercial("ana_codigo") then response.Write(" selected")%>><%=rstComercial("ana_nombre")%></option>
	        <%rstComercial.movenext
	    wend
        rstComercial.cerrar
        set rstComercial=nothing%>
    </select>
	</div>
</div>

<div class="FiltradoCab">
<span class="cab1"><%=objIdioma.getIdioma("ControlDeAcciones_Texto4")%></span><br />
	<div>
    <select name="comboTerminada" onchange="javascript:recargaAcciones();" class="combos" ID="Select1">
	    <option value="0"<%if cint(request("ter")) = 0 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto2")%></option>
        <option value="1"<%if cint(request("ter")) = 1 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("ValorAfirmativo")%></option>
        <option value="2"<%if cint(request("ter")) = 2 then response.Write(" selected")%>><%=objIdiomaGeneral.getIdioma("ValorNegativo")%></option>
    </select>
	</div>
</div>
</form>

  <%'############################################################################################%>
  
  
<div style="clear:both;">&nbsp;</div>
  

			  <TABLE cellSpacing="1" cellPadding="1" width="100%" border="0" ID="Table2">

		      <tr> 

					<td class="titularT" height="20">
		              #
		            </td>
		            
					<td class="titularT" height="20">
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto5")%>
		            </td>	

					<td class="titularT" height="20">
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto6")%>
		            </td>
		            		            
					<td class="titularT" height="20">
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto7")%>
		            </td>	

					<td class="titularT" height="20">
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto8")%>
		            </td>
		            
		            <td class="titularT" height="25">
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto9")%>
		            </td>

					<td class="titularT" height="25"  align =center>
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto10")%>
		            </td>

					<td class="titularT" height="25"  align =center>
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto11")%>
		            </td>
		            
					<td height="20" align="center" class="titularT">
		              <%=objIdioma.getIdioma("ControlDeAcciones_Texto12")%>
		            </td>		            
		            	            		            		 		            
			   </tr> 

<%
sql="select top 200 acci_sucliente,clie_nombre,camp_nombre,acci_comentario,acci_fecha,acci_sutipo,acci_terminada,acci_fechamodificacion,ana_nombre,clie_fuente"
sql=sql&" ,(CASE WHEN LTRIM(RTRIM(CAST(CAST(D.IDID_DESCRIPCION AS varchar(max)) AS varchar)))='' Or D.IDID_DESCRIPCION IS NULL THEN tipos_accion.TIAC_NOMBRE ELSE CAST(D.IDID_DESCRIPCION AS varchar(max)) END) as TIAC_NOMBRE "	
sql=sql & " from (((((acciones_cliente "
sql=sql & " inner join clientes on acciones_cliente.acci_sucliente=clientes.clie_codigo) "
sql=sql & " inner join campanas on campanas.camp_codigo=acciones_cliente.acci_sucampana) "
sql=sql & " inner join analistas on analistas.ana_codigo=acciones_cliente.acci_sucomercial) "
sql=sql & " inner join tipos_accion on tipos_accion.tiac_codigo=acciones_cliente.acci_sutipo) " 
sql=sql & " Left Join IDIOMAS_DESCRIPCIONES D on (tipos_accion.TIAC_CODIGO=D.IDID_INDICE And D.IDID_TABLA='TIPOS_ACCION' And D.IDID_CAMPO='TIAC_NOMBRE' And D.IDID_SUIDIOMA='" & Idioma & "')) "	
sql=sql & " where acci_FECHAMODIFICACION IS NOT NULL"
'Comercial
if request("per")<>"" AND request("per")<>0 then
    sql=sql & " and acci_sucomercial=" & request("per")
end if
'Tipo acciones
if request("t")<>"" AND request("t")<>0 then
    sql=sql & " and acci_sutipo=" & request("t")
end if
'Terminadas
if request("ter")<>"" AND request("ter")<>0 then
    if request("ter")="1" then
        sql=sql & " and acci_terminada = 1" 
    else
        sql=sql & " and acci_terminada = 0" 
    end if
end if
sql=sql & " order by acci_fechamodificacion desc" 
Set rstCamp= connCRM.AbrirRecordset(sql,0,1)
cont=0
while not rstCamp.eof
     cont=cont+1%>
      
     <tr align="left" class="celdaBlanca">

	   	<td valign="top" id="celda1">
	  		<%=cont%>
	  	</td>
	  	
	   	<td valign="top" id="celda1">
        	<%=FormatearFechaVisualizacion(rstCamp("acci_fechamodificacion"),true)%>
	  	</td>

	   	<td valign="top" id="celda1" >
	  		<%=rstCamp("tiac_nombre")%>
	  	</td>

	   	<td valign="top" id="celda1">
	  		<%=rstCamp("ana_nombre")%>
	  	</td>
	  		  	
	   	<td valign="top" id="celda1">
	  		<%=rstCamp("camp_nombre")%>
	  	</td>
	    
	   	<td valign="top" id="celda1" class="celdas" style="background:#f1f1f1!important;">
			<strong><%=rstCamp("clie_nombre") %></strong>
            <%'Si es amaia, controlamos la fuente del registro
            if instr(lcase(rstCamp("ana_nombre")),"amaia")>0 then%>
                <br /><%=rstCamp("clie_fuente")%>
            <%end if %>
	  	</td>	  	

	   	<td valign="top" id="celda1">
	  		<%=rstCamp("acci_fecha")%>
	  	</td>
	  		
	   	<td valign="top" id="celda1">
	  		<%=rstCamp("acci_comentario")%>

	  	</td>

	   	<td align="center" valign="top" id="celda1">
            <% if rstCamp("acci_terminada")=true then%>
            	<img src="../imagenes/green.png" width="30">
            <%else%>
            	<img src="../imagenes/red.png" width="30">
           	<%end if%>
	  	</td>
		
	</tr>     
	
    <%
    
    rstCamp.movenext
wend%>



	</table>


<br><br>



<%'############################################################################################%>
<%'############################################################################################%>


<%'Estad�sticas del log del comercial%>

<table border=0 width=100% style='border:0 solid #445E7A' cellpadding=2 cellspacing=1>
    <tr>
	<td width=100% align=center>
	    <font face=Verdana size=2 >
        <%=objIdioma.getIdioma("ControlDeAcciones_Texto13")%> <br>
        <%=objIdioma.getIdioma("ControlDeAcciones_Texto14")%>
        </font>
    </td>
    </tr>
</table>

<table>
	<tr>
		<%'Hallamos los 4 d�as laborales anteriores a hoy y el d�a de hoy y sacamos el n�mero de horas rellenadas
		for i=0 to 3%>

			<%aux=dateadd("d",-i,date)
			if WeekDay(aux)<>1 and WeekDay(aux)<>7 then%>	
			   	<td><%PintarEstadisticaDia (aux)%></td>
			<%end if%>	
			   
		<%next%>	
			
	</tr>
</table>	


<%
'1=domingo ... 7=sabado
function diaTexto(fecha)
   Select case weekDay(fecha)
     case 2
        diaTexto=ucase(objIdiomaGeneral.getIdioma("DiasCorto_2"))
     case 3
        diaTexto=ucase(objIdiomaGeneral.getIdioma("DiasCorto_3"))
     case 4
        diaTexto=ucase(objIdiomaGeneral.getIdioma("DiasCorto_4"))
     case 5
        diaTexto=ucase(objIdiomaGeneral.getIdioma("DiasCorto_5"))
     case 6
        diaTexto=ucase(objIdiomaGeneral.getIdioma("DiasCorto_6"))
   end select
end function%>
			
<%function PintarEstadisticaDia (datFecha)

	strHoras="8,9,10,11,12,13,14,15,16,17,18"
	arrHoras=split(strHoras,",")
	TotalDia1=0
	TotalDia2=0
	TotalDia3=0
	TotalDia4=0%>

	<table><tr><td>
	<table cellpadding=2 cellspacing=1 border=0>
	  <tr>
			<td><%=diaTexto(datFecha)%></td>
			<td colspan=4><%=datFecha%></td>
	  </tr>
	  <TR>
	    <TD>&nbsp;</TD>
	    <TD COLSPAN=2 ALIGN=CENTER><%=objIdioma.getIdioma("ControlDeAcciones_Texto15")%></TD>
	    <TD COLSPAN=1 ALIGN=CENTER><%=objIdioma.getIdioma("ControlDeAcciones_Texto16")%></TD>
	    <TD COLSPAN=1 ALIGN=CENTER><%=objIdioma.getIdioma("ControlDeAcciones_Texto17")%></TD>
	    <!--<TD COLSPAN=5 ALIGN=CENTER>LLAMADAS</TD>    -->
	  </tr>
	  <tr>
	    <td><%=objIdioma.getIdioma("ControlDeAcciones_Texto18")%></td>
	    <td><%=objIdioma.getIdioma("ControlDeAcciones_Texto19")%></td>
	    <td><%=objIdioma.getIdioma("ControlDeAcciones_Texto20")%></td>
	    <td><%=objIdioma.getIdioma("ControlDeAcciones_Texto21")%></td>
	    <td><%=objIdioma.getIdioma("ControlDeAcciones_Texto22")%></td>
	    <!--<td>MAX FIN</td>
	    <td>MAX NoFin</td>
	    <td>MEDIA Fin</td>
	    <td>MEDIA NoFin</td>
	    <td># dias est.</td>-->
	  </tr>
	
	<%
	'Llamadas terminadas
	sql="select count(*),DATEPART(HOUR, accilog_horamodificacion) as par from acciones_cliente_log inner join acciones_cliente on acciones_cliente_log.accilog_suaccion=acciones_cliente.acci_codigo " 
	sql=sql & " where "
    if request("per")<>"" and request("per")<>0 then sql=sql & " accilog_sucomercial=" & request("per") & " and"
	sql=sql & " acci_sutipo=1 "
	sql=sql & " and acci_terminada=1"
	sql=sql & " and day(accilog_fechamodificacion)=" & day(datFecha)
	sql=sql & " and month(accilog_fechamodificacion)=" & month(datFecha)
	sql=sql & " and year(accilog_fechamodificacion)=" & year(datFecha)
	sql=sql & " GROUP BY DATEPART(HOUR, accilog_horamodificacion) "
	Set rstLlamadasTerminadas= connCRM.AbrirRecordset(sql,0,1)	
	
	'Llamadas no terminadas
	sql="select count(*),DATEPART(HOUR, accilog_horamodificacion) as par from acciones_cliente_log inner join acciones_cliente on acciones_cliente_log.accilog_suaccion=acciones_cliente.acci_codigo " 
	sql=sql & " where "
    if request("per")<>"" and request("per")<>0 then sql=sql & " accilog_sucomercial=" & request("per") & " and"
	sql=sql & " acci_sutipo=1 "
	sql=sql & " and acci_terminada=0"
	sql=sql & " and day(accilog_fechamodificacion)=" & day(datFecha)
	sql=sql & " and month(accilog_fechamodificacion)=" & month(datFecha)
	sql=sql & " and year(accilog_fechamodificacion)=" & year(datFecha) 
	sql=sql & " GROUP BY DATEPART(HOUR, accilog_horamodificacion) " 
	Set rstLlamadasNoTerminadas= connCRM.AbrirRecordset(sql,0,1)	

    'Envio de informacion
	sql="select count(*),DATEPART(HOUR, accilog_horamodificacion) as par from acciones_cliente_log inner join acciones_cliente on acciones_cliente_log.accilog_suaccion=acciones_cliente.acci_codigo " 
	sql=sql & " where "
    if request("per")<>"" and request("per")<>0 then sql=sql & " accilog_sucomercial=" & request("per") & " and"
	sql=sql & " acci_sutipo=2 "
	sql=sql & " and day(accilog_fechamodificacion)=" & day(datFecha)
	sql=sql & " and month(accilog_fechamodificacion)=" & month(datFecha)
	sql=sql & " and year(accilog_fechamodificacion)=" & year(datFecha)
	sql=sql & " GROUP BY DATEPART(HOUR, accilog_horamodificacion) "    
	Set rstEnvios= connCRM.AbrirRecordset(sql,0,1)
	
    'Otras acciones
	sql="select count(*),DATEPART(HOUR, accilog_horamodificacion) as par from acciones_cliente_log inner join acciones_cliente on acciones_cliente_log.accilog_suaccion=acciones_cliente.acci_codigo " 
	sql=sql & " where "
    if request("per")<>"" and request("per")<>0 then sql=sql & " accilog_sucomercial=" & request("per") & " and"
	sql=sql & " acci_sutipo<>1 and acci_sutipo<>2 "
	sql=sql & " and day(accilog_fechamodificacion)=" & day(datFecha)
	sql=sql & " and month(accilog_fechamodificacion)=" & month(datFecha)
	sql=sql & " and year(accilog_fechamodificacion)=" & year(datFecha)
	sql=sql & " GROUP BY DATEPART(HOUR, accilog_horamodificacion) "  
	Set rstOtros= connCRM.AbrirRecordset(sql,0,1)	
	
	for intI=0 to ubound(arrHoras)
		intAuxValor1=0
		intAuxValor2=0
		intAuxValor3=0
		intAuxValor4=0
		intMedia1=0
		intMedia2=0

		Response.Write "<tr bgcolor=ffffff><td align=right><font face=verdana size=1>" & arrHoras(intI)  & ":00 - " & arrHoras(intI)  & ":59" & "</td>" 

		rstLlamadasTerminadas.filter="par='" & arrHoras(intI)  & "'"
		if rstLlamadasTerminadas.eof then
			intNumero=0
		else
			intNumero=rstLlamadasTerminadas(0)
		end if
		TotalDia1=TotalDia1+intNumero
		Response.Write "<td align=center><font face=verdana size=1>" & intNumero & "</td>"
		
		rstLlamadasNoTerminadas.filter="par='" & arrHoras(intI)  & "'"
		if rstLlamadasNoTerminadas.eof then
			intNumero=0
		else
			intNumero=rstLlamadasNoTerminadas(0)
		end if
		TotalDia2=TotalDia2+intNumero		
		Response.Write "<td align=center><font face=verdana size=1>" & intNumero & "</td>"		
		
		rstEnvios.filter="par='" & arrHoras(intI)  & "'"
		if rstEnvios.eof then
			intNumero=0
		else
			intNumero=rstEnvios(0)
		end if
		TotalDia3=TotalDia3+intNumero
		Response.Write "<td align=center><font face=verdana size=1>" & intNumero & "</td>"

		rstOtros.filter="par='" & arrHoras(intI)  & "'"
		if rstOtros.eof then
			intNumero=0
		else
			intNumero=rstOtros(0)
		end if
		TotalDia4=TotalDia4+intNumero
		Response.Write "<td align=center><font face=verdana size=1>" & intNumero & "</td>"		

		Response.Write "</tr>"			
	next
    rstOtros.cerrar
    rstEnvios.cerrar
    rstLlamadasNoTerminadas.cerrar
    rstLlamadasTerminadas.cerrar%>
	<tr>
		<td align=right><%=objIdioma.getIdioma("ControlDeAcciones_Texto23")%></td>
		<td align=center><%=TotalDia1%></td>
		<td align=center><%=TotalDia2%></td>
		<td align=center><%=TotalDia3%></td>
		<td align=center><%=TotalDia4%></td>
	</tr>
	<tr>
		<td align=right><%=objIdioma.getIdioma("ControlDeAcciones_Texto23")%></td>
		<td colespan=4 align=left><b><%=TotalDia1+TotalDia2+TotalDia3+TotalDia4%></b></td>
	</tr>
	</table></td></tr></table>

<%end function%>

<%
rstCamp.cerrar
%>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->

