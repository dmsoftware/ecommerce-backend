<%
''''Inicio Bloque datos Ofertas pendientes
OPendientesTerminadas=0
CantOPendientesTerminadas=0


sql=" Select count(A.ACCI_CODIGO) as OPendientesTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
sql = sql & " Where A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false "
sql = sql & filtros

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesTerminadas=rsDatos("OPendientesTerminadas")
	CantOPendientesTerminadas=rsDatos("CantOPendientesTerminadas")
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOPendientesTerminadas) or trim(CantOPendientesTerminadas)="" then
	CantOPendientesTerminadas=0
end if




MediaCantOPendientesTerminadas=0
if OPendientesTerminadas>0 then
	MediaCantOPendientesTerminadas=CantOPendientesTerminadas / OPendientesTerminadas 
end if

'response.Write(MediaCantOPendientesTerminadas&"@")
'response.End()

OPendientesNuevoCliTerminadas=0
CantOPendientesNuevoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPendientesNuevoCliTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesNuevoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false And C.CLIE_CLIENTEACTUAL=true "
sql = sql & filtros

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesNuevoCliTerminadas=rsDatos("OPendientesNuevoCliTerminadas")
	CantOPendientesNuevoCliTerminadas=rsDatos("CantOPendientesNuevoCliTerminadas")	
end if
rsDatos.cerrar()
Set rsDatos = nothing

if isnull(CantOPendientesNuevoCliTerminadas) or  trim(CantOPendientesNuevoCliTerminadas)="" then
	CantOPendientesNuevoCliTerminadas=0
end if


MediaCantOPendientesNuevoCliTerminadas=0
if OPendientesNuevoCliTerminadas>0 then
	MediaCantOPendientesNuevoCliTerminadas=CantOPendientesNuevoCliTerminadas / OPendientesNuevoCliTerminadas 
end if

OPendientesViejoCliTerminadas=0
CantOPendientesViejoCliTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPendientesViejoCliTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesViejoCliTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  And C.CLIE_CLIENTEACTUAL=false "
sql = sql & filtros

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesViejoCliTerminadas=rsDatos("OPendientesViejoCliTerminadas")
	CantOPendientesViejoCliTerminadas=rsDatos("CantOPendientesViejoCliTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesViejoCliTerminadas)  or trim(CantOPendientesViejoCliTerminadas)="" then
	CantOPendientesViejoCliTerminadas=0
end if


MediaCantOPendientesViejoCliTerminadas=0
if OPendientesViejoCliTerminadas>0 then
	MediaCantOPendientesViejoCliTerminadas=CantOPendientesViejoCliTerminadas / OPendientesViejoCliTerminadas 
end if


OPendientesAltaTerminadas=0
CantOPendientesAltaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaAltaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesAltaTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  And A.ACCI_TIPO6ESTADOCLIENTE=1  "
sql = sql & filtros

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesAltaTerminadas=rsDatos("OPresentadaAltaTerminadas")
	CantOPendientesAltaTerminadas=rsDatos("CantOPendientesAltaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesAltaTerminadas) or  trim(CantOPendientesAltaTerminadas)="" then
	CantOPendientesAltaTerminadas=0
end if


MediaCantOPendientesAltaTerminadas=0
if OPendientesAltaTerminadas>0 then
	MediaCantOPendientesAltaTerminadas=CantOPendientesAltaTerminadas / OPendientesAltaTerminadas 
end if

OPendientesMediaTerminadas=0
CantOPendientesMediaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaMediaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesMediaTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  And A.ACCI_TIPO6ESTADOCLIENTE=2 "
sql = sql & filtros

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesMediaTerminadas=rsDatos("OPresentadaMediaTerminadas")
	CantOPendientesMediaTerminadas=rsDatos("CantOPendientesMediaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesMediaTerminadas) or trim(CantOPendientesMediaTerminadas)="" then
	CantOPendientesMediaTerminadas=0
end if


MediaCantOPendientesMediaTerminadas=0
if OPendientesMediaTerminadas>0 then
	MediaCantOPendientesMediaTerminadas=CantOPendientesMediaTerminadas / OPendientesMediaTerminadas 
end if


OPendientesBajaTerminadas=0
CantOPendientesBajaTerminadas=0

sql=" Select count(A.ACCI_CODIGO) as OPresentadaBajaTerminadas,sum(A.ACCI_TIPO6CANTIDAD) as CantOPendientesBajaTerminadas "
sql = sql & "  From (  ACCIONES_CLIENTE as A "
sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  And A.ACCI_TIPO6ESTADOCLIENTE=3  "
sql = sql & filtros

Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
if not rsDatos.eof then
	OPendientesBajaTerminadas=rsDatos("OPresentadaBajaTerminadas")
	CantOPendientesBajaTerminadas=rsDatos("CantOPendientesBajaTerminadas")		
end if
rsDatos.cerrar()
Set rsDatos = nothing

if  isnull(CantOPendientesBajaTerminadas) or trim(CantOPendientesBajaTerminadas)="" then
	CantOPendientesBajaTerminadas=0
end if


MediaCantOPendientesBajaTerminadas=0
if OPendientesBajaTerminadas>0 then
	MediaCantOPendientesBajaTerminadas=CantOPendientesBajaTerminadas / OPendientesBajaTerminadas 
end if



'''''


rsTerritorios.movefirst
contBucle=0
while not rsTerritorios.eof 
	
		AuxOPendientesTerminadasProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPendientesTerminadasProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  And C.CLIE_PROVINCIA=" & rsTerritorios("PROV_CODIGO")
		sql = sql & filtros

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPendientesTerminadasProv=rsDatos("AuxOPendientesTerminadasProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCantProv) or trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		
		AuxMediaProv=0
		if AuxOPendientesTerminadasProv>0 then
			AuxMediaProv=AuxCantProv / AuxOPendientesTerminadasProv 
		end if
		
		Redim Preserve arrProvOPendientesTerminadas(contBucle)		
		arrProvOPendientesTerminadas(contBucle)=AuxOPendientesTerminadasProv
		
		
		Redim Preserve arrProvCantOPendientesTerminadas(contBucle)		
		arrProvCantOPendientesTerminadas(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOPendientesTerminadas(contBucle)		
		arrProvMediaOPendientesTerminadas(contBucle)=AuxMediaProv
	
	contBucle=contBucle + 1
	rsTerritorios.movenext
wend	

		AuxOPendientesTerminadasProv=0
		AuxCantProv=0
		AuxMediaProv=0

		sql=" Select count(A.ACCI_CODIGO) as AuxOPendientesTerminadasProv,sum(A.ACCI_TIPO6CANTIDAD) as AuxCantProv "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES C on ( A.ACCI_SUCLIENTE=C.CLIE_CODIGO ) ) "
		sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  And C.CLIE_PROVINCIA not in (" & territorios & ") "
		sql = sql & filtros

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxOPendientesTerminadasProv=rsDatos("AuxOPendientesTerminadasProv")
			AuxCantProv=rsDatos("AuxCantProv")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing
		
		if isnull(AuxCantProv) or  trim(AuxCantProv)="" then
			AuxCantProv=0
		end if

		
		AuxMediaProv=0
		if AuxOPendientesTerminadasProv>0 then
			AuxMediaProv=AuxCantProv / AuxOPendientesTerminadasProv 
		end if
		
		Redim Preserve arrProvOPendientesTerminadas(contBucle)		
		arrProvOPendientesTerminadas(contBucle)=AuxOPendientesTerminadasProv
		
		
		Redim Preserve arrProvCantOPendientesTerminadas(contBucle)		
		arrProvCantOPendientesTerminadas(contBucle)=AuxCantProv
		
		Redim Preserve arrProvMediaOPendientesTerminadas(contBucle)		
		arrProvMediaOPendientesTerminadas(contBucle)=AuxMediaProv

'''''

rsAreaNegocio.movefirst
contBucle=0
while not rsAreaNegocio.eof
	''''''
		AuxValor=0
		AuxCant=0
		AuxMedia=0


		sql=" Select count(A.ACCI_CODIGO) as valorArea,sum(A.ACCI_TIPO6CANTIDAD) as cantArea "
		sql = sql & "  From (  ACCIONES_CLIENTE as A "
		sql = sql & " 		inner join CLIENTES on ( A.ACCI_SUCLIENTE=CLIENTES.CLIE_CODIGO ) ) "
		sql = sql & " Where  A.ACCI_SUTIPO=7 And A.ACCI_TERMINADA=false  "
		sql = sql & " And A.ACCI_TIPO6SUAREANEGOCIO=" & rsAreaNegocio("TAN_CODIGO")
		sql = sql & filtros
		

		Set rsDatos = connCRM.AbrirRecordset(sql,0,1)
		if not rsDatos.eof then
			AuxValor=rsDatos("valorArea")
			AuxCant=rsDatos("cantArea")
		end if
		rsDatos.cerrar()
		Set rsDatos = nothing

		if  isnull(AuxCant) or  trim(AuxCant)="" then
			AuxCant=0
		end if
		

		AuxMedia=0
		if AuxValor>0 then
			AuxMedia=AuxCant / AuxValor 
		end if	

		Redim Preserve arrValorAreas(contBucle)		
		arrValorAreas(contBucle)=AuxValor
		

		Redim Preserve arrCantAreas(contBucle)		
		arrCantAreas(contBucle)=AuxCant

		
		Redim Preserve arrMediaAreas(contBucle)		
		arrMediaAreas(contBucle)=AuxMedia				
	''''''

	contBucle=contBucle + 1
	rsAreaNegocio.movenext
wend

''''Fin Bloque datos Ofertas Pendientes

%>