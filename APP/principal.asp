<!-- #include virtual="/dmcrm/includes/permisos.asp"-->
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->

<%function QuitoNulos(Valor)

    'Si viene un valor nulo pongo 0
    if isnull(Valor) then
        QuitoNulos=0
    else
        QuitoNulos=Valor
    end if
end function%>

<%'P�GINA DE COMIENZO CON LAS REDIRECCIONES PERTINENTES Y COMPROBACIONES Y ACCIONES DIARIAS

'ACCIONES DIARIAS
'Leemos la fecha en la que se ha realizado por �ltima vez las acciones diarias.  Si es la de hoy no hago nada.
sql="Select CONF_FECHAACCIONESDIARIAS FROM CONFIGURACION"
Set rstFecha= connCRM.AbrirRecordset(sql,3,3)

if datediff("d",rstFecha("CONF_FECHAACCIONESDIARIAS"),date)<>0 or isnull(rstFecha("CONF_FECHAACCIONESDIARIAS")) then
'if true  then
    '*****************************************************************************************************************************************************************
    '1/ Borrado diario de logs de entrada (tabla RH_LOGENTRADAS) *****************************************************************************************************
    sql="delete from RH_LOGENTRADAS Where RHL_CODIGO in ( Select RHL_CODIGO from RH_LOGENTRADAS where DATEDIFF(d,RHL_FECHALOG,getdate())>365 ) " 
    connCRM.execute sql,intNumRegistros
    '*****************************************************************************************************************************************************************

    '*****************************************************************************************************************************************************************
    '2/ Borrado diario de logs de acciones (tabla ACCIONES_CLIENTE_LOG) **********************************************************************************************
    sql="delete from ACCIONES_CLIENTE_LOG Where ACCILOG_CODIGO in ( Select ACCILOG_CODIGO from ACCIONES_CLIENTE_LOG where DATEDIFF(d,ACCILOG_FECHAMODIFICACION,getdate())>365 ) " 
    connCRM.execute sql,intNumRegistros
    '*****************************************************************************************************************************************************************

    '*****************************************************************************************************************************************************************
    '3/ Borrado diario de logs LOG_ACTUALIZACION_OK y LOG_ACTUALIZACON_FAIL ******************************************************************************************
    'Dejamos un registro en LGO_aCTUALIZACION_OK para saber la fecha de �ltima actualizacion de Platinum
    sql="Select LOG_CLIENTE,LOG_DOMINIO,LOG_FECHA FROM LOG_ACTUALIZACIONES_OK WHERE DATEDIFF(d,LOG_FECHA,getdate())>365"
    sql=sql & " ORDER BY log_cliente,log_dominio,log_fecha"
    Set rstLog= connCRM.AbrirRecordset(sql,3,3)
    while not rstLog.eof
        if RegistroAnterior=rstLog("LOG_CLIENTE")&rstLog("LOG_DOMINIO")&year(rstLog("LOG_FECHA"))&"/"&month(rstLog("LOG_FECHA"))&"/"&day(rstLog("LOG_FECHA")) then
           rstLog.delete
        else
            RegistroAnterior=rstLog("LOG_CLIENTE")&rstLog("LOG_DOMINIO")&year(rstLog("LOG_FECHA"))&"/"&month(rstLog("LOG_FECHA"))&"/"&day(rstLog("LOG_FECHA"))
        end if    
        rstLog.MOVENEXT
    wend
    rstLog.cerrar

    sql="delete from LOG_ACTUALIZACIONes_FAIL Where LOG_FAILCODIGO in ( Select LOG_FAILCODIGO from LOG_ACTUALIZACIONes_FAIL where DATEDIFF(d,LOG_FECHA,getdate())>365 ) " 
    connCRM.execute sql,intNumRegistros
    '*****************************************************************************************************************************************************************

    '*****************************************************************************************************************************************************************
    '4/ Guardamos los cambios en los proyectos en la tabla PROYECTOS_HISTORICO ***************************************************************************************
    'Listamos los proyectos que:
    '  * Se han modificado desde la �ltima vez que se ha ejecutado
    '  * Se han imputado horas
    '  * Se han modificado la planificacion de sus tareas

    'Hallamos los proyectos y las diferentes fechas de modificacion (del propio proyecto, la fecha m�s alta del hist�rico, la fecha m�s alta de imputaci�n de tareas, la fecha m�s alta de modificaci�n de tareas)
    sql="SELECT proy_codigo,PROY_HORASAMEDIDA,MAX(PROYH_FECHA) as FechaUltHistProyecto, proy_fechamodificacion, MAX(TAR_FECHAMODIFICACIONO) as FechaModTareas, MAX(HPR_SUDIA) as FechaModImputacion"
    sql = sql & " FROM ((proyectos LEFT JOIN proyectos_historico ON proyectos.proy_codigo=proyectos_historico.proyh_suproyecto)"
    sql = sql & " LEFT JOIN tareas ON proyectos.proy_codigo=tareas.tar_suproyecto )"
    sql = sql & " LEFT JOIN horas_proyecto ON proyectos.proy_codigo=horas_proyecto.hpr_suproyecto"
    sql = sql & " GROUP BY proy_codigo,proy_fechamodificacion,PROY_HORASAMEDIDA"
    Set rstProyectosFecha= connCRM.AbrirRecordset(sql,0,1)
    while not rstProyectosFecha.eof

         'Filtramos los proyectos que han tenido modificaciones
         blnModificado=false
         if datediff("d",rstProyectosFecha("FechaUltHistProyecto"),rstProyectosFecha("proy_fechamodificacion"))>0 then blnModificado=true
         if datediff("d",rstProyectosFecha("FechaUltHistProyecto"),rstProyectosFecha("FechaModTareas"))>0 then blnModificado=true
         if datediff("d",rstProyectosFecha("FechaUltHistProyecto"),rstProyectosFecha("FechaModImputacion"))>0 then blnModificado=true
         if isnull(rstProyectosFecha("FechaUltHistProyecto")) or blnModificado then
            
            'Calculamos las horas imputadas del proyecto
            sql="SELECT SUM(HPR_HORAS)as HorasImputadas FROM horas_proyecto WHERE hpr_suproyecto=" & rstProyectosFecha("proy_codigo")
            Set rstHorasImputadas= connCRM.AbrirRecordset(sql,0,1) 
            intHorasImputadas = rstHorasImputadas("HorasImputadas")
            rstHorasImputadas.cerrar

            'Calculamos las horas planificadas del proyecto
            sql="SELECT SUM(TAR_HORASPLANIFICACION)as HorasPlanificadas FROM tareas WHERE tar_suproyecto=" & rstProyectosFecha("proy_codigo") & " and tar_tipotarea=2 and tar_fechafinalizacion is null "
            Set rstHorasPlanificadas= connCRM.AbrirRecordset(sql,0,1) 
            intHorasPlanificadas =rstHorasPlanificadas("HorasPlanificadas")
            rstHorasPlanificadas.cerrar

            'Comprobamos si la fecha ya existe en la tabla PROYECTOS_HISTORICO
            diaHistorico=dateadd("d",-1,date)
            sql="SELECT * "
            sql = sql & " FROM proyectos_historico "
            sql = sql & " WHERE convert(varchar,proyh_fecha," & obtenerFormatoFechaSql() & ")=convert(varchar,'" & year(diaHistorico) & "-" &  month(diaHistorico) & "-" & day(diaHistorico)  & "'," & obtenerFormatoFechaSql() & ") "
            sql = sql & " and proyh_suproyecto=" & rstProyectosFecha("proy_codigo") 
            Set rstHistorico= connCRM.AbrirRecordset(sql,3,3)

            'Comprobamos si los valores que vamos a introducir para la fecha DIAHISTORICO coincide con los de la �ltima fecha que tenemos en la tabla PROYECTOS_HISTORICO.  Si coinciden no insertamos nada, quiere decir, que realmente no ha habido cambios
            blnDatosDistintos=false
            sql="SELECT * FROM proyectos_historico WHERE proyh_suproyecto=" & rstProyectosFecha("proy_codigo") & " ORDER BY proyh_fecha desc"
            Set rstComprobacion= connCRM.AbrirRecordset(sql,0,1)
            if rstComprobacion.eof then 
                blnDatosDistintos=true
            else
                if QuitoNulos(rstComprobacion("PROYH_HPRESUPUESTADAS"))<>QuitoNulos(rstProyectosFecha("PROY_HORASAMEDIDA"))  or QuitoNulos(rstComprobacion("PROYH_HIMPUTADAS"))<>QuitoNulos(intHorasImputadas) or QuitoNulos(rstComprobacion("PROYH_HPLANIFICADAS"))<>QuitoNulos(intHorasPlanificadas) then blnDatosDistintos=true
            end if
            rstComprobacion.cerrar

            if blnDatosDistintos then

                'Si existe el registro lo updateamos, si no existe creamos uno nuevo, s�lo si ha habido cambios del �ltimo d�a al que deseamos guardar
                if rstHistorico.eof then
                   rstHistorico.addnew
                   rstHistorico("PROYH_FECHA")= FormatearFechaSQLSever(diaHistorico)
                   rstHistorico("PROYH_SUPROYECTO")= rstProyectosFecha("proy_codigo") 
                end if
                'A�adimos los valores
                rstHistorico("PROYH_HPRESUPUESTADAS")=rstProyectosFecha("PROY_HORASAMEDIDA") 
                rstHistorico("PROYH_HIMPUTADAS")=intHorasImputadas
                rstHistorico("PROYH_HPLANIFICADAS")=intHorasPlanificadas
                rstHistorico.update
            end if
            rstHistorico.cerrar
         end if
         rstProyectosFecha.movenext
    wend
    rstProyectosFecha.cerrar
    '*****************************************************************************************************************************************************************

    '*****************************************************************************************************************************************************************
    '5/ Borrado diario de los ficheros de IMPORTACI�N (/dmcrm/APP/SISTEMA/IMPORTADORES/TMP) **********************************************************************
	Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder(Server.MapPath("\dmcrm\app\sistema\importadores\tmp"))
	For Each objItem In objFolder.Files
        objItem.delete
	Next 'objItem	
    set objFolder=nothing
    set objFSO=nothing
    '*****************************************************************************************************************************************************************
       		
	'*****************************************************************************************************************************************************************
	'Si tenemos una parte publica con DMCorporative, hacemos un proceso, para traer los email anotados en la lista de bajas del emailing y los que existan en la entidad
	'socios del CRM y no tengan checkeada la opcion de "CON_NORECIBIREMAILS", se la marcamos como chequeada.
'	conDMCorporative=false
'	if session("codigoUsuario")="3" then
'		conDMCorporative=true
'	end if

	if conDMCorporative and false then

		conexionCorporativa = "Provider=SQLNCLI10.1;Server=;database=;uid=;Password="
		set connCorporativa = new DMWConexion
		connCorporativa.AbrirSQL ("" & conexionCorporativa)

			listaNegraMailing=""
			sql=" Select CFG_CODIGO,CFG_LISTANEGRAMAILING From Configuraciones Where ltrim(rtrim(upper(CFG_DOMINIO)))='" & trim(ucase(dominioCRM)) & "' "	
			Set rsConfCorporativa = connCorporativa.AbrirRecordSet(sql,3,1) 
			if not rsConfCorporativa.eof then
				listaNegraMailing=rsConfCorporativa("CFG_LISTANEGRAMAILING")
			end if
			rsConfCorporativa.cerrar()
			set rsConfCorporativa=nothing

			listaNegraMailing=replace(listaNegraMailing,";","','")
			listaNegraMailing=replace(listaNegraMailing,"''","")
			listaNegraMailing=replace(listaNegraMailing,",,",",")
			listaNegraMailing=ucase(listaNegraMailing)

			if trim(listaNegraMailing)<>"" then
				sql=" Select CON_CODIGO,CON_EMAIL,CON_NORECIBIREMAILS From SOCIOS "
				sql=sql & " Where CON_NORECIBIREMAILS=0 And ( (ltrim(rtrim(CON_NTARJETA))<>'' And CON_NTARJETA is not null ) Or (ltrim(rtrim(CON_CODIGOEXTERNO))<>'' And CON_CODIGOEXTERNO is not null And CON_CODIGOEXTERNO<>0) ) "
				sql=sql & " And upper(CON_EMAIL)  IN ('" & listaNegraMailing & "') "

				Set rsDarBaja = connCRM.AbrirRecordSet(sql,3,3) 
				while not rsDarBaja.eof 
					if instr(rsDarBaja("CON_EMAIL"),"@")>0 then
						rsDarBaja("CON_NORECIBIREMAILS")=true			
					end if
					rsDarBaja.update
					
					rsDarBaja.movenext
				wend
				rsDarBaja.cerrar()
				set rsDarBaja=nothing		
			end if

    	connCorporativa.cerrar()
	    set connCorporativa=nothing		
	
	end if

	sql="Update CONFIGURACION set CONF_FECHAACCIONESDIARIAS='" & FormatearFechaSQLSever(date) & "'"
    connCRM.execute sql,intNumRegistros

end if
rstFecha.cerrar    


'Dirigimos a la pantalla de inicio
rutaPantallaInicio="/dmcrm/APP/Produccion/inicio/Inicio.asp"
if not isnull(pagInicioNormal) And trim(pagInicioNormal)<>"" then
	rutaPantallaInicio=pagInicioNormal
end if			
	
'Si la contrase�a del usuaria es vac�a (es la primera vez que entra el usuario) le llevamos al formulario de cambio de contrase�a
if session("ContrasenaVacia") then
	rutaPantallaInicio="/dmcrm/APP/rrhh/CambiarContrasena.asp"
else
		if session("esDirectorCuenta") then
			if not isnull(pagInicioDirectorCuenta) And trim(pagInicioDirectorCuenta)<>"" then
				rutaPantallaInicio=pagInicioDirectorCuenta
			end if
		else
			if session("TipoUsuario")="comercios" then	
				if not isnull(pagInicioComercios) And trim(pagInicioComercios)<>"" then		
					rutaPantallaInicio=pagInicioComercios		
				end if			
			end if
				
		end if		
end if

response.redirect rutaPantallaInicio


%>
