<%response.Charset="ISO-8859-1"%>
<!--#include virtual="/dmcrm/APP/comun2/val_comun.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/dmcrm/css/estilos.css">
	<title>Ayuda</title>
</head>
<body class="fondoPagIntranet">
	<%
		imagen = "dmayuda.gif"
		nombre = " Ayuda"
		descripcion = ""
	%>

    <p>
		En la lista que encontramos al escoger una entidad se muestran todos los registros insertados hasta el momento dicha entidad.  <br><br>Las acciones que pueden ejecutarse sobre la entidad seleccionada se enumeran a continuaci�n:
    </p>
    <div style="border:1px solid #C5C5C5;" class="fondoPagIntranet">
    	<dl id="dlAyuda">
        	<dd><img src="imagenes/detalle.png" align="absmiddle" />&nbsp;&nbsp;Ampliar datos de un registro</dd>
            <dt>Si s�lo desea VER el contenido de un registro clicke en cualquier dato de los que se muestra en el listado. Se presentar�n todos los datos del registro.</dt>
            
            <dd><img src="imagenes/anadir.gif" align="absmiddle" />&nbsp;&nbsp;A�adir un registro</dd>
            <dt>
            	Si desea A�ADIR uno nuevo registro clicke el icono CARPETA. Se le presentar� un formulario para introducir los datos del nuevo registro.&nbsp; Una vez rellenados todos los datos (por lo menos los obligatorios), clicke el bot�n Enviar Datos para guardarlos. Si desea cancelar el alta clicke el bot�n Cancelar, y si desea comenzar de nuevo clicke el bot�n Restablecer para vaciar los campos del formulario.
            </dt>
            
            <dd><img src="imagenes/c0.gif" align="absmiddle">&nbsp;&nbsp;Modificar un registro</dd>
            <dt>
            	Si desea MODIFICAR un registro de la lista clicke en el icono LAPIZ. 
                Se le presentar� un formulario con los datos ya introducidos del registro.
                Una vez modificados los datos deseados puede guardar los datos clickando el bot�n Enviar Datos, cancelar la operaci�n (bot�n Cancelar) o bien restaurar los datos originales (bot�n Restablecer).
            </dt>
            
            <dd><img src="imagenes/delete.gif" align="absmiddle">&nbsp;&nbsp;Eliminar un registro</dd>
            <dt>
            	Si desea BORRAR un registro clicke en el bot�n BORRAR.
                Se le pedir� que confirme la eliminaci�n del registro. La operaci�n de borrado no puede reestablecerse, con lo cual se recomienda m�xima atenci�n a la hora de eliminar un registro. 
                T�ngase en cuenta, adem�s, que borrar un registro e introducir otro nuevo con los mismos datos puede tener consecuencias indeseadas si el registro borrado estaba asociado a otros registros de otras tablas. Internamente tendr�n c�digos distintos, por lo cual las relaciones existentes se pierden.
            </dt>
            
            <dd><img src="imagenes/a0.gif" align="absmiddle">&nbsp;&nbsp;Asociar un archivo a un registro</dd>
            <dt>
            	Si desea ASOCIAR UN ARCHIVO a un registro clicke en el bot�n correspondiente CARPETA. Aparecer� una nueva pantalla pidi�ndole que busque el archivo a asociar en su disco duro.
                Una vez escogido clicke el bot�n Subir Archivo para proceder a llevarlo al servidor. El tiempo en realizar esta operaci�n es funci�n del tama�o del archivo a subir y de la conexi�n a internet disponible.
                Si deseamos borrar un fichero asociado a un registro clickaremos el bot�n Borrar el Archivo de la ventana emergente. Una vez realizada una u otra operaci�n
cerraremos la ventana emergente clickando en el enlace [Cerrar ventana].
            </dt>
            
            <dd><img src="imagenes/i0.gif" align="absmiddle">&nbsp;&nbsp;Asociar registros de otra tabla a un registro</dd>
            <dt>
            	Si desea ASOCIAR REGISTROS de otra tabla a un registro clicke en el bot�n correspondiente a las HOJAS.
                Aparecer� una nueva pantalla pidi�ndole que seleccione los registros de la tabla que desea asociar al registros. Una vez esten seleccionados todos los registros que desee asociar, pulse el bot�n aceptar para guardar los cambios.
                Si se situa con el cursos sobre el icono de las hojas se mostrara una etiqueta que le indicara con los registros de que tabla.
            </dt>
        </dl>
        <p style="text-align:center"><input class="botones" type="button" name="atras" value="  volver  " ID="atras" onClick="parent.$.fancybox.close();"></p>
    </div> 
</body>
</html>