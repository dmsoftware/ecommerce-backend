<!-- De este fichero cogemos varias funciones de validaci�n-->
<script language="JavaScript" src="FormChek2.js"></script>

<%
'*****************************************************************************************
'*************  VALIDACIONES DE LOS CAMPOS ***********************************************
'*****************************************************************************************
ColorObligatorio="000000" 'color de los campos obligatorios de rellenar por el usuario
HayObligatorios=false 'variable para poner el mensaje de campos obligatorios

strValidacionObligatorio ="" 'variable donde se acumulan las validaciones de los campos obligatorios
strValidacionCampoDeTexto ="" 'variable donde se acumulan las validaciones de los campos de texto
strValidacionCampoIP="" 'Variable donde se acumulan las validaciones de los campos de IP
strValidacionCampoEmail ="" 'variable donde se acumulan las validaciones de los campos de email
strValidacionCampoNumerico ="" 'variable donde se acumulan las validaciones de los campos num�ricos
strValidacionCampoNumericoDecimal=""
strValidacionCampoFecha ="" 'variable donde se acumulan las validaciones de los campos fecha
strValidacionCampoHora ="" 'variable donde se acumulan las validaciones de los campos hora
strValidacionCampoMemo ="" 'variable donde se acumulan las validaciones de los campos memo


'FUNCION QUE CREA LOS STRINGS DE VALIDACION DE CADA CAMPO.
Sub PrepararValidacionCampo()

    'VALIDACION DE CAMPO OBLIGATORIO.  ADEM�S ESCRIBIMOS EN DISTINTO COLOR.
	if rsCampos("Obligatorio") then%>
	    <%HayObligatorios=true%>
	    <font color="#<%=ColorObligatorio%>" face="verdana" size="1">* </font>
	    <%ValidacionObligatorio rsCampos("campo"),rsCampos("nomUsuario")%>
	<%else%>
	    <font color="#000000" face="verdana" size="1"></font>						
	<%end if
	
	'VALIDACION DE CAMPO DE TEXTO (comillas simples)
	if rsCampos("Tipo")="T" or rsCampos("Tipo")="M" or rsCampos("Tipo")="ME" then
	   ValidacionCampoDeTexto rsCampos("campo"),rsCampos("nomUsuario")
	end if
	
	if rsCampos("Tipo")="ME" then	
		ValidacionCampoEditorMemo rsCampos("campo")
	end if	

	if rsCampos("Tipo")="MI" then	
		ValidacionCampoEditorMemo rsCampos("campo")
	end if	
	
'lcase(tabla) & lcase(rscampos("campo")) & rsIdi("IDI_CODIGO") & codigo	
	if rsCampos("Tipo")="MEI" then	
		sqlIdiMemo=" SELECT * FROM IDIOMAS ORDER BY IDI_ORDEN ASC,IDI_IDIOMA ASC "
		if trim(request("conex"))<>"" then  
			Set rsIdiMemo = connMaestra.abrirRecordSet(sqlIdiMemo,3,1)
		else
			Set rsIdiMemo = connCRM.abrirRecordSet(sqlIdiMemo,3,1)
		end if							

		while not rsIdiMemo.EOF
			ValidacionCampoEditorMemo "idi_" & lcase(tabla) & lcase(rsCampos("campo")) & lcase(rsIdiMemo("IDI_CODIGO")) & codigo	
			rsIdiMemo.movenext
		wend
		rsIdiMemo.cerrar()
		Set rsIdiMemo = nothing
	end if			

	'VALIDACION DE CAMPO DE IP
	if rsCampos("Tipo")="IP" then
	   ValidacionCampoIP rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO DE Email (comillas simples)
	if rsCampos("Tipo")="E" then
	   ValidacionCampoEmail rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO NUMERICO (s�lo d�gitos) (no valido las combos)
	if (rsCampos("Tipo")="N" OR rsCampos("Tipo")="SN" OR rsCampos("Tipo")="SM" OR rsCampos("Tipo")="SE" OR rsCampos("Tipo")="US") and rsCampos("ClaveExt")=0 then
	   ValidacionCampoNumerico rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO NUMERICO DECIMAL (s�lo d�gitos y coma) 
	if rsCampos("Tipo")="ND" then
	   ValidacionCampoNumericoDecimal rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO FECHA (fecha L�GICA)
	if rsCampos("Tipo")="F" Or rsCampos("Tipo")="FH" then
	   ValidacionCampoFecha rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO HORA (fecha L�GICA)
	if rsCampos("Tipo")="H" then
	   ValidacionCampoHora rsCampos("campo"),rsCampos("nomUsuario")
	end if
		
end sub


'FUNCION QUE CREA LOS STRINGS DE VALIDACION DE CADA CAMPO PARA LA PAGINA DE BUSQUEDAS
Sub PrepararValidacionCampoBusqueda()

     %><font color="#000000" face="verdana" size="1"><%
     
	'VALIDACION DE CAMPO DE TEXTO (comillas simples)
	if rsCampos("Tipo")="T" or rsCampos("Tipo")="M" or rsCampos("Tipo")="ME" then
	   ValidacionCampoDeTexto rsCampos("campo"),rsCampos("nomUsuario")
	end if
	
'	'VALIDACION DE ASIGNACION DE LOS CAMPOS MEMO
'	if rsCampos("Tipo")="M" then
'		ValidacionCampoEditorMemo rsCampos("campo")
'	end if

	'VALIDACION DE CAMPO EMAIL (comillas simples)
	if rsCampos("Tipo")="E" then
	   ValidacionCampoEmail rsCampos("campo"),rsCampos("nomUsuario")
	end if

	if rsCampos("Tipo")="IP" then
	   ValidacionCampoIP rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO NUMERICO (s�lo d�gitos) (no valido las combos)
	if rsCampos("Tipo")="N" and rsCampos("ClaveExt")=0 then
	   ValidacionCampoNumerico rsCampos("campo")&"desde",rsCampos("nomUsuario")
	   ValidacionCampoNumerico rsCampos("campo")&"hasta",rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO NUMERICO (s�lo d�gitos) (no valido las combos)
	if (rsCampos("Tipo")="SN" OR rsCampos("Tipo")="SM" OR rsCampos("Tipo")="SE" OR rsCampos("Tipo")="US") then
	   ValidacionCampoNumerico rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO NUMERICO DECIMAL (s�lo d�gitos y coma) 
	if rsCampos("Tipo")="ND" then
	   ValidacionCampoNumericoDecimal rsCampos("campo"),rsCampos("nomUsuario")
	end if

	'VALIDACION DE CAMPO FECHA (fecha L�GICA)
	if rsCampos("Tipo")="F" Or rsCampos("Tipo")="FH" then
	   ValidacionCampoFecha rsCampos("campo")&"desde",rsCampos("nomUsuario")& " desde"
	   ValidacionCampoFecha rsCampos("campo")&"hasta",rsCampos("nomUsuario")& " hasta"
	end if

	'VALIDACION DE CAMPO HORA (fecha L�GICA)
	if rsCampos("Tipo")="H" then
	   ValidacionCampoHora rsCampos("campo"),rsCampos("nomUsuario")
	end if
		
end sub

'Procedimiento para a�adir validaci�n de campo obligatorio
'Devuelve codigo javascrip de la forma:
'    if (theForm.INMUEBLE.value == "" or theForm.INMUEBLE.value == null)
'    {
'      alert("Escriba un valor para el campo \"INMUEBLE\".");
'      theForm.INMUEBLE.focus();
'      return(false);
'    }
sub ValidacionObligatorio(campo,nombreUsuario)

		if rsCampos("Tipo")="TI" then
			sql=" SELECT * FROM IDIOMAS ORDER BY IDI_ORDEN ASC,IDI_IDIOMA ASC "
			if trim(request("conex"))<>"" then  
				Set rsIdms = connMaestra.abrirRecordSet(sql,3,1)
			else
				Set rsIdms = connCRM.abrirRecordSet(sql,3,1)
			end if		

			while not rsIdms.eof
				strAux= "if (stripInitialWhitespace(theForm.idi_" & lcase(tabla) & lcase(campo) & rsIdms("IDI_CODIGO") & codigo & ".value)=="""") {"
				strAux=StrAux & " alert(""Escriba un valor para el campo \""" & nombreUsuario & "\"" en " & rsIdms("IDI_IDIOMA") & "."");"
				strAux=strAux & " theForm.idi_" & lcase(tabla) & lcase(campo) & rsIdms("IDI_CODIGO") & codigoTI & ".focus();  "
				strAux=strAux & " return(false); } "
				strValidacionObligatorio=strValidacionObligatorio & strAux
				rsIdms.movenext
			wend
		else
		  'A�adimos la funcion javascript stripInitialWhitespace (en js de iFormMantenimiento)
		  strAux= "if (stripInitialWhitespace(theForm." & campo & ".value)=="""") {"
		  strAux=StrAux & " alert(""Escriba un valor para el campo \""" & nombreUsuario & "\""."");"
		  strAux=strAux & " theForm." & campo & ".focus();  "
		  strAux=strAux & " return(false); } "
		  strValidacionObligatorio=strValidacionObligatorio & strAux
		end if
end sub



sub ValidacionCampoDeTexto(campo,nombreUsuario)
  'strAux= "if (theForm." & campo & ".value.lastIndexOf(""'"")!==-1) { "
  'strAux=StrAux & " alert(""No se pueden escribir comillas simples. Elim�nelas del campo \""" & nombreUsuario & "\""."");"
  'strAux=strAux & " theForm." & campo & ".focus();  "
  'strAux=strAux & " return(false); } "
  'strAux=strAux & "if (theForm." & campo & ".value.lastIndexOf('""')!==-1) { "
  'strAux=StrAux & " alert(""No se pueden escribir comillas. Elim�nelas del campo \""" & nombreUsuario & "\"" sustituy�ndolas, si desea, por \""�\"" (Alt+174) y \""�\"" (Alt+175)."");"
  'strAux=strAux & " theForm." & campo & ".focus();  "
  'strAux=strAux & " return(false); } "
  'strValidacionCampoDeTexto=strValidacionCampoDeTexto & strAux
end sub

sub ValidacionCampoIP(campo,nombreUsuario)
  strAux=strAux & "  Valor=theForm." & campo & ".value " & chr(13) & chr(10)
  strAux=strAux & "  if (  isValidIPAddress(Valor,true) == false ) { " & chr(13) & chr(10)
  strAux=StrAux & "    alert(""El campo \""" & nombreUsuario & "\"" debe contener una direcci�n IP. (Ej. 111.111.111.111)"");" & chr(13) & chr(10)
  strAux=strAux & "    theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "    return(false); } " & chr(13) & chr(10) & chr(13) & chr(10)
  strValidacionCampoIP=strValidacionCampoIP & strAux
end sub

sub ValidacionCampoEmail(campo,nombreUsuario)
  strAux=strAux & "  Valor=theForm." & campo & ".value " & chr(13) & chr(10)
  strAux=strAux & "  if (  isEmail(Valor,true) == false ) { " & chr(13) & chr(10)
  strAux=StrAux & "    alert(""El campo \""" & nombreUsuario & "\"" debe contener un email. (Ej. email@dominio.com)"");" & chr(13) & chr(10)
  strAux=strAux & "    theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "    return(false); } " & chr(13) & chr(10) & chr(13) & chr(10)
  strValidacionCampoEmail=strValidacionCampoEmail & strAux
end sub

sub ValidacionCampoNumerico(campo,nombreUsuario)
  strAux= " for (k=0;k<theForm." & campo & ".value.length;k++) { " & chr(13) & chr(10)
  strAux=strAux & "  if (theForm." & campo & ".value.charAt(k) < '0' || theForm." & campo & ".value.charAt(k)>'9') { " & chr(13) & chr(10)
  strAux=StrAux & "    alert(""El campo \""" & nombreUsuario & "\"" s�lo puede contener caracteres num�ricos. Elimine los caracteres no num�ricos."");" & chr(13) & chr(10)
  strAux=strAux & "    theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "    return(false); } } " & chr(13) & chr(10) & chr(13) & chr(10)
  strValidacionCampoNumerico=strValidacionCampoNumerico & strAux
end sub

sub ValidacionCampoNumericoDecimal(campo,nombreUsuario)

  strAux=chr(13) & chr(10) & "  Valor=theForm." & campo & ".value; " & chr(13) & chr(10)
  strAux=strAux & "  if (  isFloat(theForm." & campo & ".value,true) == false & isSignedFloat (theForm." & campo & ".value,true)==false ) { " & chr(13) & chr(10)
  strAux=StrAux & "    alert(""El campo \""" & nombreUsuario & "\"" debe tener un contenido decimal. Si ha introducido un punto, sustit�yalo por una coma. (Ej. 10,45)"");" & chr(13) & chr(10)
  strAux=strAux & "    theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "    return(false); } " & chr(13) & chr(10) & chr(13) & chr(10)
  strValidacionCampoNumericoDecimal=strValidacionCampoNumericoDecimal & strAux
end sub

sub ValidacionCampoFecha (campo,nombreUsuario)
  strAux= " cadena= theForm." & campo & ".value; " & chr(13) & chr(10)
  'si el la fecha no est� rellenada no haecemos nada.
  strAux=strAux & " if (cadena!=="""") { " & chr(13) & chr(10)
  'posiciones de los /
  strAux=strAux & " pos1=cadena.indexOf(""/"",1);" & chr(13) & chr(10)
  strAux=strAux & " pos2=cadena.indexOf(""/"",pos1+1);" & chr(13) & chr(10)
  'campo sin / 
  strAux=strAux & " if (pos1==-1) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } " & chr(13) & chr(10) 
  'campo sin dos / 
  strAux=strAux & " if (pos2==-1) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } " & chr(13) & chr(10) 
  strAux=strAux & " dia=cadena.substring(0,pos1)" & chr(13) & chr(10)
  'el d�a es no num�rico
  strAux=strAux & " for (k=0;k<dia.length;k++) { " & chr(13) & chr(10)
  strAux=strAux & " if (dia.charAt(k) < '0' || dia.charAt(k)>'9') { "
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10) 
  'el d�a no entra en el rango 1-31
  strAux=strAux & " if (dia>31 || dia<1) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica. Corrija el d�a: ""+dia+""."");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } " & chr(13) & chr(10) 
  strAux=strAux & " mes=cadena.substring(pos1+1,pos2)" & chr(13) & chr(10)
  'el mes es no num�rico
  strAux=strAux & " for (k=0;k<mes.length;k++) { " & chr(13) & chr(10)
  strAux=strAux & " if (mes.charAt(k) < '0' || mes.charAt(k)>'9') { "
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } " & chr(13) & chr(10) 
  'el mes no entra en el rango 1-12
  strAux=strAux & " if (mes>12 || mes<1) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica. Corrija el mes: ""+mes+""."");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10)   
  strAux=strAux & " ano=cadena.substring(pos2+1,cadena.length)" & chr(13) & chr(10)
  'el a�o es no num�rico
  strAux=strAux & " for (k=0;k<ano.length;k++) { " & chr(13) & chr(10)
  strAux=strAux & " if (ano.charAt(k) < '0' || ano.charAt(k)>'9') { "
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica siguiendo el formato �dd/mm/aaaa�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10) 
  'el a�o no entra en el rango 0-3000
  strAux=strAux & " if (ano>3000 || ano<0) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una fecha l�gica. Corrija el a�o: ""+ano+""."");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10)   
  strValidacionCampoFecha=strValidacionCampoFecha & strAux
end sub

sub ValidacionCampoHora (campo,nombreUsuario)
  strAux= " cadena= theForm." & campo & ".value; " & chr(13) & chr(10)
  'si el la hora no est� rellenada no haecemos nada.
  strAux=strAux & " if (cadena!=="""") { " & chr(13) & chr(10)
  'posiciones del :
  strAux=strAux & " pos1=cadena.indexOf("":"",1);" & chr(13) & chr(10)
  'campo sin :
  strAux=strAux & " if (pos1==-1) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una hora l�gica siguiendo el formato �hh:mm�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } " & chr(13) & chr(10) 
  'la hora es no num�rico
  strAux=strAux & " hora=cadena.substring(0,pos1)" & chr(13) & chr(10)
  strAux=strAux & " for (k=0;k<hora.length;k++) { " & chr(13) & chr(10)
  strAux=strAux & " if (hora.charAt(k) < '0' || hora.charAt(k)>'9') { "
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una hora l�gica siguiendo el formato �hh:mm�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10) 
  'la hora no entra en el rango 0-24
  strAux=strAux & " if (hora>24 || hora<0) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una hora l�gica. Corrija la hora: ""+hora+""."");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } " & chr(13) & chr(10)   
  'los minutos es no num�rico
  strAux=strAux & " minutos=cadena.substring(pos1+1,cadena.length)" & chr(13) & chr(10)
  strAux=strAux & " for (k=0;k<minutos.length;k++) { " & chr(13) & chr(10)
  strAux=strAux & " if (minutos.charAt(k) < '0' || minutos.charAt(k)>'9') { "
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una hora l�gica siguiendo el formato �hh:mm�"");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10) 
  'los minutos no entra en el rango 0-60
  strAux=strAux & " if (minutos>59 || minutos<0) { " & chr(13) & chr(10)
  strAux=StrAux & "   alert(""El campo \""" & nombreUsuario & "\"" debe contener una hora l�gica. Corrija los minutos: ""+minutos+""."");" & chr(13) & chr(10)
  strAux=strAux & "   theForm." & campo & ".focus();  " & chr(13) & chr(10)
  strAux=strAux & "   return(false); } } " & chr(13) & chr(10)   
  strValidacionCampoHora=strValidacionCampoHora & strAux
end sub

sub ValidacionCampoEditorMemo(campo)
	strAux="if (document.getElementById('" & campo & "')!=null && document.getElementById('" & campo & "_Editor" & "')!=null)"
	strAux=strAux & "{var nicEditoDescripcion = new nicEditors.findEditor('" & campo & "_Editor" & "');"							
	strAux=strAux & "document.getElementById('" & campo & "').value = nicEditoDescripcion.getContent();}"	
    strValidacionCampoMemo=strValidacionCampoMemo & strAux
end sub
'*****************************************************************************************
'*************  FIN VALIDACIONES DE LOS CAMPOS *******************************************
'*****************************************************************************************
%>