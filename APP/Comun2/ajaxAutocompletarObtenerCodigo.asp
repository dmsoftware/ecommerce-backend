<%'Indicamos que los datos van en ISO
response.Charset="ISO-8859-1"
'**********************************%>

<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->

<%

Function URLDecode(sConvert)
    Dim aSplit
    Dim sOutput
    Dim I
    If IsNull(sConvert) Then
       URLDecode = ""
       Exit Function
    End If

    ' convert all pluses to spaces
    sOutput = REPLACE(sConvert, "+", " ")

    ' next convert %hexdigits to the character
    aSplit = Split(sOutput, "%")

    If IsArray(aSplit) Then
      sOutput = aSplit(0)
      For I = 0 to UBound(aSplit) - 1
        sOutput = sOutput & _
          Chr("&H" & Left(aSplit(i + 1), 2)) &_
          Right(aSplit(i + 1), Len(aSplit(i + 1)) - 2)
      Next
    End If

    URLDecode = sOutput
End Function

tabla=request("tabla")
claveExterna=request("claveExterna")
DescripcionExterna=request("DescripcionExterna")
contenidoDescripcion=replace(replace(request("contenidoDescripcion"),"|plus|","+"),"|amper|","&")

codigoObtenido="-1"

esMemo=false
castingMemno1=""
castingMemno2=""
if trim(request("esMemo"))="1" then
	esMemo=true
	castingMemno1="CAST("
	castingMemno2=" as varchar(max)) "	
end if

NumDescripciones=request("NumDescripciones")
tieneVariasDescripciones=false
descripcionesSelect=""
if trim(request("NumDescripciones"))<>"" And isnumeric(trim(request("NumDescripciones"))) then
	tieneVariasDescripciones=true
	for contDescripciones=2 to NumDescripciones 
    	 descripcionesSelect=descripcionesSelect & " + ' ' + " &  castingMemno1 & request("DescripcionExterna" + cstr(contDescripciones)) & castingMemno2
    next 
end if  

''A�adimos ## a los dos lados de la where, porque el sustitucion vocales solo funciona con el like (no con =), por lo que a�adimos las almoadillas para evitar coincidencias.
sql="select distinct(" & claveExterna & ") as codigoObtenido from " & tabla
sql=sql & " WHERE (" & castingMemno1 & DescripcionExterna & castingMemno2 
if tieneVariasDescripciones then
	sql=sql & descripcionesSelect 
end if
sql=sql & " + ' ##' "
''Si la primera letra es un corchete de apertura, lo quitamos para a�adirle un porcentaje.
''Sino hacemos esto, no encuentra la coincidencia con like (el [ es un caracter reservado del sql server).
contenidoAuxSinCorchete=trim(contenidoDescripcion)
if left(trim(contenidoDescripcion),1)="[" then
	contenidoAuxSinCorchete="" & right(contenidoAuxSinCorchete,len(contenidoAuxSinCorchete)-1)
end if
sql=sql & "like '%" & sustitucion_vocales(trim(contenidoAuxSinCorchete)) & " ##" & "' ) "

sql=sql & " Or (" & castingMemno1 & DescripcionExterna & castingMemno2 
if tieneVariasDescripciones then
	sql=sql & descripcionesSelect 
end if
sql=sql & "= '" & trim(contenidoDescripcion) & "' ) "
'response.Write(sql)
if trim(request("conex"))<>"" then  
	Set rstDatos = connMaestra.AbrirRecordset(sql,0,1)
else
	Set rstDatos = connCRM.AbrirRecordset(sql,0,1)
end if

if not rstDatos.eof then
	if isnumeric(rstDatos("codigoObtenido")) And trim(cstr(rstDatos("codigoObtenido")))<>"0" then 
		codigoObtenido=rstDatos("codigoObtenido")
	end if
end if
rstDatos.cerrar()
Set rstDatos = nothing

response.Write(codigoObtenido)

%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->

