<%@ Language=VBScript %>
<!--#include virtual="/dmcrm/APP/comun2/val_comun.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->


<!--#include file="c_uploader.asp"-->

<%
'PARAMETROS DE LA PAGINA 
Tabla=Request("Tabla")
Conexion=session(denominacion & "_" & request("conexion"))
Filtro=request("Filtro")
ValorFiltro=request("ValorFiltro")
Filtro2=request("Filtro2")
ValorFiltro2=request("ValorFiltro2")

intModo = cInt(request.QueryString("modo"))
'Directorio donde se alojar� temporalmente el archivo
directorio=strlocal & carpetaInterna & "/usuariosFtp/" & left(request("conexion"),8)
'FUNCIONES Y PROCEDIMIENTOS

'funci�n que dada una tabla de la maestra, una tabla de memoria y una conexion, nos dice si los campos de la tabla
'de la maestra son los mismos que los de la tabla en memoria. Estos ultimos han de estar en el orden de la maestra
function MismaEstructuraEnMaestra(tablaMaestra,tablaMemoria,connLocal)
	dim intIndiceTabla
	intIndiceTabla = 0
	sql = "SELECT NomUsuario FROM Maestra WHERE Tabla ='" & TablaMaestra & "' AND lcase(Campo)<>'suportal' ORDER BY Orden ASC"
	Set rsCamposMaestra = connLocal.AbrirRecordset(sql,3,3)
	
	MismaEstructuraEnMaestra = true 'Inicializaci�n
	while not rsCamposMaestra.EOF
		if trim(lcase(rsCamposMaestra("NomUsuario"))) <> trim(lcase(tablaMemoria(intIndiceTabla))) then
			MismaEstructuraEnMaestra = false
			exit function
		end if 'trim(lcase(rsCamposMaestra("campo"))) <> trim(lcase(tablaMemoria(intIndiceTabla)))
		intIndiceTabla = intIndiceTabla + 1
		rsCamposMaestra.movenext()
	wend 'not rsCamposMaestra.EOF
	
	rsCamposMaestra.cerrar()
	set rsCamposMaestra = nothing
end function

'Funci�n que detecta si una l�nea tiene todos los caracteres vac�os
'Ocurre mucho cuando se parte de un excell original con varias l�neas y se borra alguna.
'El programa mete todo ; pero campos vac�os
function LineaVacia (arrayLinea)
	dim blnLineaVacia
	dim i
	blnLineaVacia = true
	
	for i = 0 to ubound(arrayLinea)
		if arrayLinea(i) <> "" then blnLineaVacia = false
	next
	LineaVacia = blnLineaVacia
end function

'funci�n que dado un fichero abierto, y un tama�o de la cabecera (n�mero de campos),
'nos acumula posibles errores en strError con los que ya hab�a, y nos devuelve
'una tabla bidimensional [campos x registros] con los datos del fichero cargados
sub CargarTablaFichero (ts, intTamCabecera, modo, byref strError, byref arrTablaDatos)
	dim intLinea
	dim strLinea
	dim arrLinea
	dim i
	'inicializaci�n
	strError = "" 
	intLinea = 0
	redim arrTablaDatos(intTamCabecera,0)
	Do While Not ts.AtEndOfStream
		strLinea=""
		strLinea = ts.ReadLine
		arrLinea = split(strLinea,";")
		if not LineaVacia(arrLinea) then 'Ignoro las l�neas vac�as
			if ubound(arrLinea) <> intTamCabecera then 'la linea no tiene los mismos campos que la cabecera. Puede ser por que existe alg�n ";" en el texto del usuario.
				strError = strError & " - Linea " & intLinea + 2 & " Err�nea o no tiene el formato correcto. Compruebe que tiene el mismo n�mero de campos que la cabecera y que no existen caracteres �;�.<br>"
			else 'linea correcta
				if not isNumeric(trim(arrLinea(0))) and modo<>1 then
					strError = strError & " - Linea " & intLinea + 2 & " No tiene un c�digo num�rico.<br>"
				else 'el c�digo es un n�mero
					redim preserve arrTablaDatos(intTamCabecera,intLinea)
					for i = 0 to ubound(arrLinea)
						arrTablaDatos(i,intLinea) = trim(arrLinea(i))
					next
				end if 'not isNumeric(trim(arrLinea(0)))
			end if 'ubound(arrLinea)<>intTamCabecera
		end if 'not LineaVacia(arrLinea)
		intLinea = intLinea + 1
	loop
end sub
%>
<HTML>
<HEAD>
<TITLE>Mantenimiento masivo de registros</TITLE>
<link rel="stylesheet" href="/dmcrm/css/estilos.css"></link>
</HEAD>
<BODY class="fondoPagIntranet">
<div style="background-color:#FFFFFF;height:100%;border:1px solid #c5c5c5">
<%
if var_sgbd = 1 then
	almohadiFecha = "#"
else
	almohadiFecha = "'"
end if

if request.QueryString("h")="" then 'se ha subido el fichero
	Set ObjUpload = new uploader
	ObjUpload.MaxFileBytes = 5000000
	ObjUpload.FileExtensionList = "csv"
	'El nombre del archivo es "importacion" concatenado con el nombre de la tabla, la fecha y la hora
	strNombreArchivo = "importacion" & tabla & replace(date(),"/","") & replace(time(),":","")
	ObjUpload.NombreEnServidor = strNombreArchivo
	ObjUpload.UpLoadPath = Directorio
	if componenteUpload=1 then
		intUploadCorrecto=ObjUpLoad.Upload_Arsys
	else
		intUploadCorrecto=ObjUpLoad.Upload_Dmacroweb
	end if
%>

<table width="93%" height=90% cellspacing="0" cellpadding="0" align=center ID="Table1">
	<tr><td height=15></td></tr>
	<tr><td valign=top><b>RESULTADO DE LA ACTUALIZACI�N:</b></td></tr>
	<tr><td height=5></td></tr>
	<tr>
		<td>
<%
	Select case intUploadCorrecto
		case -1
			strError = "" 'Variable para acumular el error
			Set fs = Server.CreateObject("Scripting.FileSystemObject")
			strPathFich = directorio & "/" & ObjUpLoad.NombreEnServidor
			If fs.FileExists(strPathFich) Then
				Set fichero = fs.GetFile(strPathFich)
				Set ts = fichero.OpenAsTextStream(1)
				'Primero leemos la primera l�nea para sacar los nombres de los campos
				if ts.AtEndOfStream then 'fichero vac�o
					response.Write " - El fichero est� vac�o<br>"
				else 'hay algo
					strLinea=""
					strCabecera = ts.ReadLine
					arrCabecera = split(strCabecera,";") 'pasamos la linea con los nombres de los campos a tabla
					intTamCabecera = ubound(arrCabecera)
					if intTamCabecera = 0 then 'no tenemos cosas separadas por ";"
						strError = strError & " - Formato del fichero err�neo<br>"
					else
'						set conn = new DMWConexion
'						connCRM.abrir ("" & conexion)
						if trim(request("conexion"))<>"" then  
							connMaestra.beginTrans
						else
							connCRM.beginTrans
						end if

		 				if not MismaEstructuraEnMaestra(Tabla,arrCabecera,conn) then
		 					response.Write " - La estructura de la cabecera no es correcta<br>"
		 				else 'estructura correcta
		 					'Cargamos en una tabla todas las lineas
		 					CargarTablaFichero ts, intTamCabecera,intModo, strError, tablaDatos
							if strError<>"" then 'Hay alg�n error
								response.Write "No se ejecut� ninguna operaci�n, se han producido los siguientes ERRORES:<br><br>" & strError
							else 'no se han detectado errores en el fichero
								'miramos qu� tenemos que hacer
								Clave=ClaveTabla (Tabla,TipoClave)
								select case intModo
									'----------------------------------------------- ALTAS ---------------------------------------------------------------------------------------------------
									case 1: 'Alta masiva
										'Sacamos el c�digo m�s alto de la tabla para hacer las inserciones a partir de ah�
										sql="SELECT Max(" & Clave & ") as Maximo FROM " & Tabla   			    		       	
										if trim(request("conexion"))<>"" then  
											Set rsMaximo = connMaestra.AbrirRecordset(sql,3,3)
										else
											Set rsMaximo = connCRM.AbrirRecordset(sql,3,3)
										end if										

										if isnull(rsMaximo("Maximo")) then
											intCodigo = 1
										else
											intCodigo = rsMaximo("Maximo")+1
										end if 'isnull(rsMaximo("Maximo"))
										rsMaximo.cerrar()
										set rsMaximo = nothing
										'Saco todos los campos menos el c�digo
										sql = "SELECT Campo,Tipo FROM Maestra WHERE Tabla ='" & tabla & "' AND Campo<>'" & clave & "' AND lcase(Campo)<>'suportal' ORDER BY Orden ASC"
										if trim(request("conexion"))<>"" then  
											Set rsCampos = connMaestra.AbrirRecordset(sql,3,3)
										else
											Set rsCampos = connCRM.AbrirRecordset(sql,3,3)
										end if										

										'Para cada registro del archivo:
										for intRegistroEnCurso = 0 to ubound(tablaDatos,2)
											sql = "INSERT INTO " & Tabla & " (" & clave
											sqlAux = ""
											intCampoEnCurso = 1
											if not rsCampos.bof then rsCampos.moveFirst
											while not rsCampos.eof and intCampoEnCurso<= ubound(tablaDatos,1)
												sql = sql & "," & rsCampos("Campo")
												select case rsCampos("Tipo")
													case "T","M","ME","E","IP","CP","X","XI","XA":
														sqlAux = sqlAux & ",'" &  replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),"'","''") & "'"
													case "F","FO","FM":     
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))="" then
															sqlAux = sqlAux & ",null"
														else
															sqlAux = sqlAux & "," & almohadiFecha & FormatearFecha(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) & almohadiFecha
														end if 'trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))=""
													case "H":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))="" then
															sqlAux = sqlAux & ",null"
														else
															sqlAux = sqlAux & "," & almohadiFecha & FormatearHora(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) & almohadiFecha
														end if
													case "B":
														if lcase(trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))) = "verdadero" then
															sqlAux = sqlAux & ",-1"
														else 'vale "falso"
															sqlAux = sqlAux & ",0"
														end if 'lcase(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "verdadero"
													case "N","ND","SN","SM","SE","US":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "" then
															sqlAux = sqlAux & ",0"
														else 'vale "falso"
														    if var_sgbd = 2 then 'SQLSERVER
															    sqlAux = sqlAux & "," & replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),",",".") 
														    else 'access 
															    sqlAux = sqlAux & "," & tablaDatos(intCampoEnCurso,intRegistroEnCurso)
															end if
														end if 'trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = ""
													case else:
														sqlAux = sqlAux & "," &  replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),"'","''")
												end select 'rsCampos("Tipo")
												intCampoEnCurso = intCampoEnCurso + 1
												rsCampos.moveNext()
											wend 'not rsCampos.eof
											'el siguiente if es para insertar en la tabla mai_contactos, mai_listasDistribucion, usuarios y grupos, el codigo del portal que est� en uso																						
											if lcase(Tabla)="mai_contactos" OR lcase(Tabla)="mai_listasdistribucion" then
											    sql=sql&",suportal"
											    sqlAux=sqlAux&","&session(denominacion & "_portalactual")
											end if
											sql = sql & ") VALUES (" & intCodigo & sqlAux & ")"								
											intCodigo = intCodigo + 1
											if trim(request("conexion"))<>"" then  
												connMaestra.execute sql
											else
												connCRM.execute sql
											end if											

										next 'intRegistroEnCurso = 0 to ubound(tablaDatos,2)
										ResultadoUpload= "Actualizaci�n realizada correctamente..."
									'----------------------------------------------- MODIFICACIONES ------------------------------------------------------------------------------------------
									case 2: 'Modificaci�n masiva
										'Saco todos los campos menos el c�digo
										sql = "SELECT Campo,Tipo FROM Maestra WHERE Tabla ='" & tabla & "' AND Campo<>'" & clave & "' AND lcase(Campo)<>'suportal' ORDER BY Orden ASC"
										if trim(request("conexion"))<>"" then  
											Set rsCampos = connMaestra.AbrirRecordset(sql,3,3)
										else
											Set rsCampos = connCRM.AbrirRecordset(sql,3,3)
										end if										

										'Para cada registro del archivo:
										for intRegistroEnCurso = 0 to ubound(tablaDatos,2)
											sql = "UPDATE " & Tabla & " SET "
											intCampoEnCurso = 1
											if not rsCampos.bof then rsCampos.moveFirst
											while not rsCampos.eof and intCampoEnCurso<= ubound(tablaDatos,1)
												sql = sql & rsCampos("Campo") & "="
												select case rsCampos("Tipo")
													case "T","M","ME","E","IP","CP","X","XI","XA":
														sql = sql & "'" &  replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),"'","''") & "'"
													case "F","FO","FM":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))="" then
															sql = sql & "null"
														else
															sql = sql & almohadiFecha & FormatearFechaSQLSever(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) & almohadiFecha
														end if 'trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))=""
													case "H":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))="" then
															sql = sql & "null"
														else
															sql = sql & almohadiFecha & FormatearHora(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) & almohadiFecha
														end if
													case "B":
														if lcase(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "verdadero" then
															sql = sql & "-1"
														else 'vale "falso"
															sql = sql & "0"
														end if 'lcase(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "verdadero"
													case "N","ND","SN","SM","SE","US":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "" then
															sql = sql & "0"
														else 'vale "falso"
															sql = sql& "" & tablaDatos(intCampoEnCurso,intRegistroEnCurso)
														end if 'trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = ""
													case else:
														sql = sql &  replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),"'","''")
												end select 'rsCampos("Tipo")
												intCampoEnCurso = intCampoEnCurso + 1
												rsCampos.moveNext()
												if not rsCampos.eof and intCampoEnCurso<= ubound(tablaDatos,1) then sql = sql & ","
											wend 'not rsCampos.eof
											sql = sql & " WHERE " & clave & "=" & tablaDatos(0,intRegistroEnCurso)
											if trim(request("conexion"))<>"" then  
												connMaestra.execute sql
											else
												connCRM.execute sql
											end if											

											intCodigo = intCodigo + 1
										next 'intRegistroEnCurso = 0 to ubound(tablaDatos,2)
										ResultadoUpload= "Actualizaci�n realizada correctamente"
									'----------------------------------------------- BAJAS ---------------------------------------------------------------------------------------------------
									case 3: 'Borrado masivo									
										for intRegistroEnCurso = 0 to ubound(tablaDatos,2)
											sql = "DELETE FROM " & Tabla & " WHERE " & Clave & " = " & tablaDatos(0,intRegistroEnCurso)
											if trim(request("conexion"))<>"" then  
												connMaestra.execute sql
											else
												connCRM.execute sql
											end if											

											'lanzar borrado exec
										next 'intRegistroEnCurso = 0 to ubound(tablaDatos,2)
										ResultadoUpload= "Actualizaci�n realizada correctamente"
									case else: 'Desconocido
										strError = strError & " - Modo de operaci�n desconocido<br>"
								end select 'cInt(request.QueryString("modo"))
							end if 'strError<>""
							if trim(request("conexion"))<>"" then  
								connMaestra.CommitTrans
							else
								connCRM.CommitTrans
							end if							

'							connCRM.cerrar 
'							set conn = nothing
						end if 'not MismaEstructuraEnMaestra(Tabla,arrCabecera,conn)
					end if 'intTamCabecera = 0
				end if 'rs.AtEndOfStream
				ts.Close()
				Set ts = nothing
				fs.DeleteFile(strPathFich)
			else 'El fichero no existe
				strError = strError & " - El fichero no se ha subido correctamente o se ha producido un error en la subida.<br>" & strPathFich & "<br>"
			end if 'fs.FileExists(strPathFich)
			set fs = nothing
		case 1
		    ResultadoUpload= "NO SE ENCUENTRA EL NOMBRE DE LA TEXT TIPO FILE �FICHERO�"
		case 2
		    ResultadoUpload= "caja de texto vacia o no existe el fichero."
		case 3
		   	ResultadoUpload= "LA LONGITUD DEL ARCHIVO A SUBIR [" & ObjUpload.TamanoReal & "bytes] ES MAYOR QUE LA PERMITIDA [" & ObjUpLoad.MaxFileBytes & " bytes]."
		case 4
		   	ResultadoUpload= "LA CONFIGURACION ACTUAL NO PERMITE EXTENSIONES DEL TIPO �" & ObjUpload.ExtensionFichero & "�"
		case 10
		   	ResultadoUpload= "Error no controlado: [" & ObjUpload.Error & "]"
	end select 'intUploadCorrecto
	
	set ObjUpload=nothing
	response.Write ResultadoUpload
%>
		</td>
	</tr>
	<tr><td height=100%></td></tr>
</table>
<%
else 'primera vez
%>
<!--FORM DE SUBIDA -->
<script language=javascript>
	function CambiarAction(intModo){
		document.form1.action="mantenimientoRegistrosExcel.asp?tabla=<%=tabla%>&conexion=<%=request("conexion")%>&filtro=<%=filtro%>&valorFiltro=<%=valorFiltro%>&filtro2=<%=filtro2%>&valorFiltro2=<%=valorFiltro2%>&modo="+intModo;
	}
</script>
<form method="POST" id="form1" name="form1" ENCTYPE="multipart/form-data" action="mantenimientoRegistrosExcel.asp?tabla=<%=tabla%>&conexion=<%=request("conexion")%>&filtro=<%=filtro%>&valorFiltro=<%=valorFiltro%>&filtro2=<%=filtro2%>&valorFiltro2=<%=valorFiltro2%>&modo=1">
<table width="93%" cellspacing="0" cellpadding="0" align=center ID="Table2">
	<tr><td height=15></td></tr>
	<tr>
		<td style="text-align: justify;">
			<font size="1" face="Verdana"><b><u>INSTRUCCIONES:</u></b><br><br>Para hacer altas, modificaciones o borrados de una gran cantidad de registros, puede hacerlo de una manera c�moda y sencilla trabajando en formato Excel.<br>
			Primero desc�rguese el excel de la tabla con la que desea trabajar, pulsando el icono de "Exportar datos en formato Excel". Trabaje sobre esta estructura manteniendo la primera l�nea en la que se indican los nombres de los campos, y haga lo siguiente dependiendo de la funci�n que desea realizar:<br><br>
			<b><u>A�adir registros:</u></b> Si desea a�adir muchos registros a la vez, deje en el excel <b>SOLO</b> los registros que desea a�adir (y la cabecera con los nombres). Borre el resto de l�neas/registros.<br><br>
			<b><u>Modificar registros:</u></b> Si desea mofificar muchos registros a la vez, deje en el excel <b>SOLO</b> los registros que desea modificar (y la cabecera con los nombres). Borre el resto de l�neas/registros.<br><br>
			<b><u>Borrar registros:</u></b> Si desea borrar muchos registros a la vez, deje en el excel <b>SOLO</b> los registros que desea borrar (y la cabecera con los nombres). Borre el resto de l�neas/registros.<br><br>
			Una vez preparado el excel con los registros deseados, debe guardarlo en su disco seleccionando "guardar como..." y eligiendo el formato "CSV (delimitado por comas)(*.csv)". Una vez guardado, seleccionelo y pulse "Lanzar"<br><br>
			</font>
		</td>
	</tr>
</table>
<table width="93%" cellspacing="0" cellpadding="1" style="border:1px solid #c5c5c5;" align=center ID="Table3">
  <tr>
    <td>
      <table border="0" cellspacing="0" align="center" width="100%" ID="Table4">
        <tr><td height=5 width=100%></td></tr>
        <tr><td align=center><input type="file" size=45 name="FICHERO" ID="File1"></td></tr>
        <tr><td height=10 width=100%></td></tr>
        <tr><td height=5 width=100%></td></tr>
        <tr><td width=100% align=center>Seleccione si lo que desea hacer es un alta, una modificaci�n o un borrado masivo de registros, y luego elija el fichero en formato CSV:</td></tr>
		<tr>
			<td align=center>
				<input class=radios type=radio name=modo onClick="CambiarAction(1);" checked ID="Radio1" VALUE="Radio1">Alta&nbsp;&nbsp;&nbsp;<input class=radios type=radio name=modo  onclick="CambiarAction(2)" ID="Radio2" VALUE="Radio2">Modificaci�n&nbsp;&nbsp;&nbsp;<input class=radios type=radio name=modo onClick="CambiarAction(3);" ID="Radio3" VALUE="Radio3">Borrado
			</td>
		</tr>
		<tr><td align=center><input type="submit" value=" Lanzar " name="B1" ID="Submit1"></td></tr>
        <tr><td height=10 width=100%></td></tr>
      </table>
    </td>
  </tr>
</table>
</form>
<!--FIN FORM DE SUBIDA -->

<%end if 'request.QueryString("h")=""%>

<p align="center" ><a class="cerrar" href="javascript:parent.$.fancybox.close();">[C e r r a r&nbsp;&nbsp; v e n t a n a]</a></p>
</div>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->
</BODY>
</HTML>
