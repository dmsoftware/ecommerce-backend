<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<%response.Charset="ISO-8859-1"%>
<!-- #include virtual="/dmcrm/includes/permisos.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<%
	'***** PARAMETROS DE LA PAGINA *****
	'***** 1.- TABLA     
	'***** 2.- CONEXION      
	'***** 3.- NumMax LIMITA EL DAR DE ALTA UN REGISTRO SI SE HA SOBREPASADO EL NUMMAX
	'***** 4.- Filtro CAMPO POR EL QUE SE VAN A FILTRAR LOS REGISTROS
	'***** 5.- ValorFiltro VALOR POR EL QUE SE VAN A FILTRAR.
	'***********************************
	

%><script type="text/javascript">
//	alert(window.parent.location);
 </script>
<%
    if trim(tabla)="" then tabla = request("tabla")
    if trim(Filtro)="" then filtro = request("Filtro")
	if trim(ValorFiltro)="" then valorFiltro = request("ValorFiltro")
	if trim(Filtro2)="" then filtro2 = request("Filtro2")
	if trim(ValorFiltro2)="" then valorFiltro2 = request("ValorFiltro2")
	if trim(numMin)="" then numMin = request("NumMin")
	if trim(numMax)="" then numMax = request("NumMax")

	if session(denominacion & tabla & "_numMax") <> "" then
		numMax = session(denominacion & tabla & "_numMax")
	end if

	if trim(filtrosAplicados)="" then
		filtrosAplicados=request("filtrosAplicados")
		if trim(filtrosAplicados)<>"" then
			session(denominacion & tabla & "_filtrosAplicados")=filtrosAplicados
		end if
	end if	
	
	if trim(consultaFiltro)="" then
		consultaFiltro=request("consultaFiltro")
		if trim(consultaFiltro)<>"" then
			session(denominacion & tabla & "_consulta")=consultaFiltro
		end if
	end if	

'	if CInt("0" & request("quitarfiltro")) = 1 then 
'		session(denominacion & tabla & "_consulta") = ""
'		session(denominacion & tabla & "_filtrosAplicados") = ""
'	end if
	consulta = session(denominacion & Tabla & "_consulta")
	filtrosAplicados = session(denominacion & Tabla & "_filtrosAplicados")

	'***** OBTENEMOS LOS DATOS NECESARIOS PARA CONSTRUIR LA TABLA *****
	'******************************************************************
	numero1N = 0
	numeroImagenes = 0
	tituloTabla = ""
	numFormularios = 0

	query = "SELECT clave, (CASE WHEN tipo='' Or tipo is null THEN 'G' ELSE tipo END) as Tipo, visibleSel, (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario, claveExt,tablaExt,CampoExt0 as CampoExt, CampoExt1, ordenacion, campo,OcultarOpciones,PaginaAmedidaAlta,PaginaAmedidaBorrar,LongitudPresentacion FROM maestra WHERE tabla in ('" & tabla & "','" & tabla & "G') ORDER BY orden"

	if trim(request("conex"))<>"" then  
		Set rsCamposMaestra = connMaestra.AbrirRecordset(query,3,1)
	else
		Set rsCamposMaestra = connCRM.AbrirRecordset(query,3,1)
	end if

	opciones=""
	aMedidaAlta=""
	aMedidaBorrar=""
	longPresentacion="'100%';'100%'"
	campoGeneralVisible=false
	'***** OBTENEMOS EL TITULO *****
	rsCamposMaestra.filter = "tipo = 'G'"
	if not rsCamposMaestra.eof then 
		tituloTabla = rsCamposMaestra("nomUsuario") 
		opciones=rsCamposMaestra("OcultarOpciones")
		aMedidaAlta=rsCamposMaestra("PaginaAmedidaAlta")
		aMedidaBorrar=rsCamposMaestra("PaginaAmedidaBorrar")	
		if not isnull(rsCamposMaestra("LongitudPresentacion")) And trim(rsCamposMaestra("LongitudPresentacion"))<>"" then
			longPresentacion=rsCamposMaestra("LongitudPresentacion")				
		end if
		campoGeneralVisible=rsCamposMaestra("visibleSel")
	end if

	anchoEdicionSeleccion="'100%'"
	altoEdicionSeleccion="'100%'"	
	if instr(longPresentacion,";")>0 then
		arrLongitudPresentacion=split(longPresentacion,";")
		anchoEdicionSeleccion=arrLongitudPresentacion(0)
		altoEdicionSeleccion=arrLongitudPresentacion(1)
	end if


	'***** OBTENEMOS LOS CAMPOS CLAVE *****
	rsCamposMaestra.filter = "clave = -1"
	Redim camposClave(2, rsCamposMaestra.recordCount() - 1)
	for i = 0 to rsCamposMaestra.recordCount() - 1
		camposClave(0,i) = rsCamposMaestra("nomUsuario")
		camposClave(1,i) = rsCamposMaestra("visibleSel")
		camposClave(2,i) = rsCamposMaestra("campo")		
		rsCamposMaestra.movenext
	next
	
	
	'***** CALCULAMOS EL N�MERO DE CAMPOS FICHERO *****
	rsCamposMaestra.filter = "tipo = 'X'"
	numeroImagenes = rsCamposMaestra.recordCount()
	
	'***** CALCULAMOS EL N�MERO DE RELACIONES 1<->N *****
	query = "SELECT count(tabla) FROM maestra WHERE tablaExt = '" & tabla & "'"
	if trim(request("conex"))<>"" then  
		Set rsRelaciones1N = connMaestra.abrirRecordSet(query,3,1)
	else
		Set rsRelaciones1N = connCRM.abrirRecordSet(query,3,1)
	end if	

	numero1N = rsRelaciones1N(0)
	rsRelaciones1N.cerrar()
	Set rsRelaciones1N = nothing
	
	'***** OBTENEMOS EL N�MERO DE RELACIONES M<->N *****
	numeroMN = NumeroDeMNS(tabla)		
	
	'***** OBTENER LA ORDENACION POR DEFECTO *****
	rsCamposMaestra.filter = "clave = 1 OR visibleSel = 1"
	for i = 0 to rsCamposMaestra.recordCount() - 1 
		if rsCamposMaestra("ordenacion") then
			restarSegunGenralParaOrdenacion=0
			if campoGeneralVisible then
				restarSegunGenralParaOrdenacion=1
			end if
			campoOrdenacionDefecto = i - restarSegunGenralParaOrdenacion
			i = rsCamposMaestra.recordCount()
		end if
		rsCamposMaestra.movenext
	next		
	if isnumeric(campoOrdenacionDefecto) then
		if cint(campoOrdenacionDefecto)<=0 then
			campoOrdenacionDefecto=0
		end if
	end if
	'***** OBTENEMOS LOS CAMPOS GENERALES *****
	rsCamposMaestra.filter = "clave = 0 AND tipo <> 'G'" '& " And tipo<>'M' " 'El ultimo filtro de  And tipo<>'M' , es temporal para evitar los campos memos.
	Redim camposGenerales(6, rsCamposMaestra.recordCount()- 1)
	for i = 0 to rsCamposMaestra.recordCount() - 1
		esMultiIdioma=false	
		nombreTabla=""
		camposGenerales(0,i) = rsCamposMaestra("nomUsuario")
		camposGenerales(1,i) = rsCamposMaestra("visibleSel")
'			response.Write(rsCamposMaestra("CampoExt"))
		if rsCamposMaestra("claveExt") And rsCamposMaestra("visibleSel") then
			alias=rsCamposMaestra("CampoExt")
			if instr(rsCamposMaestra("CampoExt"),"&")>0 then
				arrAlias=split(rsCamposMaestra("CampoExt")," ")
				if instr(arrAlias(0),"&")>0 then
					arrAlias2=split(arrAlias(0)," ")				
					alias=arrAlias2(0)
				else
					alias=arrAlias(0)	
				end if
				
			end if
			
			if not (instr(alias,rsCamposMaestra("tablaExt") & ".")>0) then
				alias=rsCamposMaestra("tablaExt") & "." & alias
			end if
'alias=alias&"," & rsCamposMaestra("campo")		
			camposGenerales(2,i) = alias ' rsCamposMaestra("CampoExt") '**** REVISAR ****'
			tablaExtAux=""
			campoExtAux=""
			claveExternaAux=""
			FiltroExtAux=""
			ValorFiltroExtAux=""
	

			if ClaveExt(Tabla,rsCamposMaestra("campo"),tablaExtAux,campoExtAux,claveExternaAux,FiltroExtAux,ValorFiltroExtAux) then
			
					fromAnterior=fromAnterior&"("
				
					fromPosterior=fromPosterior&" left Join " & tablaExtAux & " on ("
					if instr(rsCamposMaestra("campo"),tabla & ".")>0 then
						fromPosterior=fromPosterior & rsCamposMaestra("campo") & "="
					else
						fromPosterior=fromPosterior & " " & tabla & "." & rsCamposMaestra("campo") & "="				
					end if			
				
					if instr(claveExternaAux,tablaExtAux & ".")>0 then
						fromPosterior=fromPosterior & claveExternaAux
					else
						fromPosterior=fromPosterior & " " & tablaExtAux & "." & claveExternaAux			
					end if							
					
					
					fromPosterior=fromPosterior	& ")) "	
					

						if instr(campoExtAux,"&")>0 Or instr(campoExtAux,"+")>0 then
							nomDescCampo=""
						else
							nomDescCampo=campoExtAux					
							if instr(campoExtAux,".")>0 then
								arrNomDescCampo=split(campoExtAux,".")
								nomDescCampo=arrNomDescCampo(1)
							end if
						end if	

						if trim(tablaExtAux)<>"" and trim(nomDescCampo)<>"" then
							tipoColumna=Tipo(tablaExtAux,nomDescCampo)
'response.Write(tablaExtAux & "." & nomDescCampo & "|" & tipoColumna & "<br/>")							
						end if

						if ucase(trim(tipoColumna))="TI" Or ucase(trim(tipoColumna))="MEI" Or ucase(trim(tipoColumna))="MI" then ' si es multiidoma
							if trim(tablaExtAux)<>"" And trim(claveExternaAux)<>"" And trim(nomDescCampo)<>"" And trim(Idioma)<>"" then

								fromAnterior=fromAnterior&"("
					
								fromPosterior=fromPosterior	& " Left Join IDIOMAS_DESCRIPCIONES ID_" & tablaExtAux & " on (ID_" & tablaExtAux & ".IDID_INDICE=" & tablaExtAux & "." & claveExternaAux & " And upper(ID_" & tablaExtAux & ".IDID_TABLA)=upper('" & tablaExtAux & "') And upper(ID_" & tablaExtAux & ".IDID_CAMPO)=upper('" & nomDescCampo & "') And upper(ID_" & tablaExtAux & ".IDID_SUIDIOMA)=upper('" & Idioma & "') )) "	

								esMultiIdioma=true
								camposGenerales(4,i)=true							
							end if
							nombreTabla=tablaExtAux
						end if	
															

 			end if	
			
		else
				if trim(ucase(rsCamposMaestra("tipo")))="TI" Or trim(ucase(rsCamposMaestra("tipo")))="MEI" Or trim(ucase(rsCamposMaestra("tipo")))="MI" then
					camposGenerales(2,i) = rsCamposMaestra("campo")					
					esMultiIdioma=true
					nombreTabla=rsCamposMaestra("campo")
					fromAnterior=fromAnterior&"("

					fromPosterior=fromPosterior&" left Join IDIOMAS_DESCRIPCIONES ID_" & nombreTabla & " on (ID_" & nombreTabla & ".IDID_INDICE=" & tabla & "." & ClaveTabla(tabla,tipoTab) & " And upper(ID_" & nombreTabla & ".IDID_TABLA)=upper('" & tabla & "') And upper(ID_" & rsCamposMaestra("campo") & ".IDID_CAMPO)=upper('" & rsCamposMaestra("campo") & "') And upper(ID_" & nombreTabla & ".IDID_SUIDIOMA)=upper('" & Idioma & "')) )"

					
				else 'else de if trim(ucase(rsCamposMaestra("tipo")))="TI" then
					camposGenerales(2,i) = rsCamposMaestra("campo")									
				end if 'end de if trim(ucase(rsCamposMaestra("tipo")))="TI" then

		end if
		
		camposGenerales(3,i) = rsCamposMaestra("tipo")		
		camposGenerales(4,i) = esMultiIdioma
		camposGenerales(5,i) = nombreTabla
		camposGenerales(6,i) = rsCamposMaestra("campo")
		
		rsCamposMaestra.movenext
	next		


	'***** OBTENEMOS EL CAMPO DE TIPO ORDENACION (PUEDE QUE NO EXISTA) *****
	rsCamposMaestra.filter = "tipo = 'OR'"
	if rsCamposMaestra.recordCount > 0 then
		campoTipoOrdenacion = rsCamposMaestra("campo") 
	end if
	
	'***** OBTENEMOS EL N�MERO DE BOTONES RELACIONADOS CON FORMULARIOS *****
	select case lcase(tabla)
		case "entidades"
				numFormularios = 1
		case "formularios"
				numFormularios = 2
	end select
	
	rsCamposMaestra.cerrar()
	Set rsCamposMaestra = nothing
	
	
	'***** CALCULAMOS LAS CARACTER�STICAS DE LAS COLUMNAS PARA EL JTABLE *****
	estadoColumnas = ""
	cabeceraColumnas = ""
	cabeceraColumnas_Excel = ""
	cabeceraColumnas_Excel2 = ""
	tiposColumnas = ""
	for i = 0 to ubound(camposClave,2)
			if camposClave(1,i) then
'				estadoColumnas = estadoColumnas & "null,"
				estadoColumnas = estadoColumnas & "{""sClass"": ""elemCodigoPK""},"
				tiposColumnas = tiposColumnas & "A;"
			else
				estadoColumnas = estadoColumnas & "{""bVisible"": false},"
				tiposColumnas = tiposColumnas & "-1;"				
			end if
			cabeceraColumnas = cabeceraColumnas & camposClave(2,i)  & ";"			
			cabeceraColumnas_Excel = cabeceraColumnas_Excel & camposClave(2,i)  & ";"			
			cabeceraColumnas_Excel2 = cabeceraColumnas_Excel2 & camposClave(2,i)  & ";"			

	next
		
	contadorAlias=1	
	for i = 0 to ubound(camposGenerales,2)

		if camposGenerales(1,i) then
		
			if camposGenerales(3,i)="F" Or camposGenerales(3,i)="FH" Or camposGenerales(3,i)="FO" Or camposGenerales(3,i)="FM" Or camposGenerales(3,i)="B" then
				estadoColumnas = estadoColumnas & "{""sClass"": ""elemCentrado""},"			
			elseif camposGenerales(3,i)="XA" Or camposGenerales(3,i)="XI" then
				estadoColumnas = estadoColumnas & "{""sClass"": ""elemCentradoMed""},"							
			elseif   camposGenerales(3,i)="ND" then	
				estadoColumnas = estadoColumnas & "{""sClass"": ""elemDerecha""},"						
			elseif  camposGenerales(3,i)="OR" then
				estadoColumnas = estadoColumnas & "{""sClass"": ""dtCentrado""},"
			elseif camposGenerales(3,i)="M" Or camposGenerales(3,i)="ME" Or camposGenerales(3,i)="MI" Or camposGenerales(3,i)="MEI"  then
				estadoColumnas = estadoColumnas & "{""sClass"": ""elemMemo""},"
			else
				estadoColumnas = estadoColumnas & "null,"			
			end if

			if camposGenerales(4,i) then
				cabeceraColumnas = cabeceraColumnas & "CAST(ID_" & camposGenerales(5,i) & ".IDID_DESCRIPCION AS VARCHAR(MAX)) AS " & camposGenerales(5,i) & "_" & contadorAlias & ";"
				cabeceraColumnas_Excel = cabeceraColumnas_Excel & "CAST(ID_" & camposGenerales(5,i) & ".IDID_DESCRIPCION AS VARCHAR(MAX)) AS " & camposGenerales(6,i) & ";"
				cabeceraColumnas_Excel2 = cabeceraColumnas_Excel2 & camposGenerales(6,i) & ";"								
				
				contadorAlias=contadorAlias + 1
			else 'else de if camposGenerales(4,i) then
				cabeceraColumnas = cabeceraColumnas & camposGenerales(2,i)  & ";"
				cabeceraColumnas_Excel = cabeceraColumnas_Excel & camposGenerales(6,i)  & ";"
				cabeceraColumnas_Excel2 = cabeceraColumnas_Excel2 & camposGenerales(6,i)  & ";"
			end if 'end de if camposGenerales(4,i) then
				
			tiposColumnas = tiposColumnas & camposGenerales(3,i) & ";"
		end if
	next
	
	estadoColumnas = "[" & mid(estadoColumnas,1,len(estadoColumnas)-1) & ",{""sClass"": ""tdOpciones"",""bSearchable"": false,""bSortable"": false}]"
	cabeceraColumnas = mid(cabeceraColumnas,1,len(cabeceraColumnas)-1)
	cabeceraColumnas_Excel = mid(cabeceraColumnas_Excel,1,len(cabeceraColumnas_Excel)-1)
	cabeceraColumnas_Excel2 = mid(cabeceraColumnas_Excel2,1,len(cabeceraColumnas_Excel2)-1)	

	tiposColumnas = mid(tiposColumnas,1,len(tiposColumnas)-1)	
	
	
	'''''''''''''''''''''''''''''''''''''''''''''''
	'********** Obtenemos las opciones Personalizables de la maestra
	'********** A�adir-->A ; Eliminar-->B ; Importar-->I ; Relacionados-->R ; Editar-->E ; Seleccionar/Desseleccionar -->S
	quitarAnadir=False
	quitarEliminar=False
	quitarSeleccionar=False
	quitarImportar=False
	quitarExportar=False
	quitarBuscar=False
	quitarAyuda=False
	
'	sqlPermisosMaestra = " Select CodTabla,ucase(Tabla) as Tabla,OcultarOpciones From maestra Where Tabla='" & ucase(tabla) & "G' "
'	opciones=""
'	Set rsPermisosMaestra = connCRM.abrirRecordSet(sqlPermisosMaestra,3,1)
'	if not rsPermisosMaestra.eof then
'		opciones=rsPermisosMaestra("OcultarOpciones")
		if trim(opciones)<>"" then
			cadaOpcion=split(opciones,",")
			for contOpcion = 0 to ubound(cadaOpcion)
				  Select Case trim(ucase(cadaOpcion(contOpcion)))
				      Case "A" 
					  		quitarAnadir=True
				      Case "B" 
					  		quitarEliminar=True					  
 					  Case "S"
							quitarSeleccionar=True
				      Case "I" 
					  		quitarImportar=True					  
					  Case "X"		
							quitarExportar=True
					  Case "F"
							quitarBuscar=True
					  Case "Y"
							quitarAyuda=True					  	
				  End Select

			next				
		end if

'	end if
'	rsPermisosMaestra.cerrar()
'	Set rsPermisosMaestra = nothing
	''''''''''''''''''''''''''''''''''''''''

numRegistrosTabla=10	
if session(denominacion & Tabla & "_numRegistrosTabla")<>"" And isnumeric(session(denominacion & Tabla & "_numRegistrosTabla")) then
	numRegistrosTabla=session(denominacion & Tabla & "_numRegistrosTabla")
end if	

paginaTabla=0
if session(denominacion & Tabla & "_paginaTabla")<>"" then
	paginaTabla=session(denominacion & Tabla & "_paginaTabla")
end if	

'response.Write("@"&session(denominacion & Tabla & "_paginaTabla")&"@")
busquedaTabla=session(denominacion & Tabla & "_busquedaTabla")

'session(denominacion & Tabla & "_campoOrdenacion")=""
if session(denominacion & Tabla & "_campoOrdenacion")<>"" then
campoOrdenacionDefecto=session(denominacion & Tabla & "_campoOrdenacion")
end if

direccionOrdenacion="desc"
if session(denominacion & Tabla & "_direccionOrdenacion")<>"" then
	direccionOrdenacion=session(denominacion & Tabla & "_direccionOrdenacion")
end if



'	response.Write(paginaTabla&"@")
	
	if instr(conexionBd,"formulario") > 0 then
		imagen = "dmFormulario.gif"
	else
		imagen = tabla & ".gif"
	end if
	nombre = tituloTabla & Request("Cabecera")
	descripcion = ""
%>

<!--<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<link rel="stylesheet" href="/dmcrm/css/estilos.css"/>-->
		<!-- CARGAMOS JTABLES -->
		
		<script type="text/javascript">
			/***** Funci�n para generar un id aleatorio para evitar la cache *****/
			function generarGUID()
			{
				var result, i, j;
				result = '';
				for(j=0; j<32; j++)
				{
					if( j == 8 || j == 12 || j == 16 || j == 20)
						result = result + '-';
					i = Math.floor(Math.random()*16).toString(16).toUpperCase();
					result = result + i;
				}
				return result;
			}
			

			
			/** Variables de la tabla **/
			var oTable;
			var gaiSelected =  [];

			$(document).ready(function() {

				/** Objeto tabla **/
				oTable = $('#tablaContenido:first').dataTable({
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bFilter": true,
					"bProcessing": true,
					"bServerSide": true,
					"sAjaxSource": "/dmcrm/APP/Comun2/seleccionAjax.asp",					
					"aoColumns": <%=estadoColumnas%>,
					"aaSorting": [[ <%=campoOrdenacionDefecto%>, "<%=direccionOrdenacion%>" ]],
					"iDisplayLength": <%=numRegistrosTabla%>,
					"iDisplayStart":<%=cint(paginaTabla)%>,
					"oSearch": {"sSearch": "<%=busquedaTabla%>"},
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						var valorBuscador=$(".cajaBuscador").val().replace("�","|NN|").replace("�","|nn|");
						<%if trim(filtrosBusqueda)<>"" then%>
						var filtro = <%=filtrosBusqueda%>;
//						filtro=filtro.replace("�","|N|")
						if (filtro != '')
						{
							var arrFiltro = filtro.split("**");
							for (var i=0;i<arrFiltro.length;i++)
							{
								aoData.push(eval('(' + arrFiltro[i] + ')'));
							}
						}					
						<%end if%>
						aoData.push({ "name": "filtro", "value":  "<%=filtro & ";" & filtro2%>" } ); 
						aoData.push({ "name": "valFiltro", "value":  "<%=valorFiltro & ";" & valorFiltro2%>" } ); 
						aoData.push({ "name": "tabla", "value":  "<%=tabla%>" } ); 
						aoData.push({ "name": "columnas", "value":  "<%=cabeceraColumnas%>" } );
						aoData.push({ "name": "cabecera", "value":  "<%=request("cabecera")%>" } );
						aoData.push({ "name": "fromAnterior", "value":  "<%=fromAnterior%>" } );						
						aoData.push({ "name": "fromPosterior", "value":  "<%=fromPosterior%>" } );																		
						aoData.push({ "name": "buscadorTabla", "value":  valorBuscador } );																								
						aoData.push({ "name": "conex", "value":  "<%=request("conex")%>" } );																														
						aoData.push({ "name": "nocache", "value":  generarGUID() } ); 
						/*La siguiente llamada, la hace por GET, por lo que pasa todos los parametros por URL. Incluida la SQL, por lo que si es demasiado grande, dependiendo del servidor, puede fallar.
						$.getJSON( sSource, aoData, function (json) { 
							fnCallback(json)
						} );*/
						$.ajax( {
							"dataType": 'json', 
							"type": "POST", 
							"url": sSource, 
							"data": aoData, 
							"success": fnCallback
						} );						
					},
					"fnDrawCallback": function () {
						
						//Funcionalidad cuando la tabla tiene campo de ordenamiento
						$('p.ordenacion').editable('/dmcrm/APP/comun2/ordenacionAjax.asp', {
							"callback"  : function( sValue, y ) {if(sValue == '-1'){oTable.fnDraw()}},
							"height"    : "14px",
							"width"     : "35px",
							"onblur"    : "submit",
							"submitdata": function(value, settings) {
								return {
               						"regTabla" : "<%=tabla%>",
									"regCampoClave" : "<%=camposClave(2,0)%>",
									"regCampoOrdenacion" : "<%=campoTipoOrdenacion%>",
									"conexion" : "<%=request("conex")%>",
									"filtro"   : "<%=filtro & ";" & filtro2%>",
									"valFiltro" : "<%=valorFiltro & ";" & valorFiltro2%>"
            					}; 
							}
						} );
						
						var nomTabla=$('#tablaContenido:first').attr('rel');
						//Funcionalidad para cargar el pie con los totales
						$.ajax({
								type: "POST",
								url: "/dmcrm/APP/comun2/seleccionAjaxPie.asp",
								data: "tabla=<%=tabla%>&tiposColumnas=<%=tiposColumnas%>&cabeceraColumnas=<%=cabeceraColumnas%>",
								success: function(datos) {
									$('#pieTabla').html(datos)
								}
						});
						
						
					},					
					"bSort": true
				});



			/*	function borrarInputs() {
					$('#filtro').find(':input').each(function() {
						switch(this.type) {
							case 'password':
							case 'select-multiple':
							case 'select-one':
							case 'text':
							case 'textarea':
								$(this).val('');
								break;
							case 'checkbox':
							case 'radio':
								this.checked = false;
							case 'hidden':
								$(this).val(0);
						}
					});
				}			*/	


				/** Mouseover de los botos de opciones **/
				$('.selBtoMod').live('mouseover',function(){
					$('img', this).attr('src','imagenes/c1.gif');
				}).live('mouseout',function(){
					$('img', this).attr('src','imagenes/c0.gif');
				});
				
				$('.selBtoBor').live('mouseover',function(){
					$('img', this).attr('src','imagenes/b1.gif');
				}).live('mouseout',function(){
					$('img', this).attr('src','imagenes/b0.gif');
				});
				
				/** Boton de anular registro  **/
				$('#anularSeleccionados').click( function() {
					var bTabla = $(this).attr('rel');
					var anSelected = fnGetSelected( oTable );
					//parametros para enviar al borrado por AJAX
					var borradoOK = false;
					var ids = '';
					for (var i=0;i<anSelected.length;i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}					
					
					if (ids!="")					
					{
						var mensajeConfirm="<%=objIdiomaGeneral.getIdioma("Seleccion_29")%>";
						
						if (document.getElementById("hidRelacionados").value!="")
						{mensajeConfirm="<%=objIdiomaGeneral.getIdioma("Seleccion_16")%>" + document.getElementById("hidRelacionados").value;	}
						if (confirm(mensajeConfirm)) {							
						
							$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Ventas/Pedidos/anularPedidos.asp",
								data: "tabla="+bTabla+"&ids="+ids+"&conex=<%=request("conex")%>",
								success: function(datos) {
									if (datos!="")
									{ 	alert(datos); }
									/*for (var i=0;i<anSelected.length;i++)
									{ oTable.fnDeleteRow(anSelected[i]); }*/
									oTable.fnDraw();
								}
							});
						}
					}
					else
					{ alert("<%=objIdiomaGeneral.getIdioma("Seleccion_17")%>"); }
				} );							
				
				/** Boton de borrado **/
				$('#borrarSeleccionados').click( function() {
					var bTabla = $(this).attr('rel');
					var anSelected = fnGetSelected( oTable );
					//parametros para enviar al borrado por AJAX
					var borradoOK = false;
					var ids = '';
					for (var i=0;i<anSelected.length;i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}					
//						alert(ids)					
					if (ids!="")					
					{
						
						var esBorradoMaestra=true;
						var rutaBorrado="/dmcrm/APP/comun2/bajas.asp";
						if ($(this).hasClass('claseBorrarMedi')) {
							rutaBorrado="<%=aMedidaBorrar%>";
							esBorradoMaestra=false;
						}
						
						var mensajeConfirm="<%=objIdiomaGeneral.getIdioma("Seleccion_15")%>";

						<%if session("codigoUsuario")="4999" then%>
								var entidadesHijas="";
								//Primero se lanza un ajax sincrono para obtener las entidades hijas de la tabla y mostrarlo en el mensaje de confirmacion
								$.ajax({
									type: "POST",
									url: "/dmcrm/APP/comun2/bajasObtenerRelaciones.asp",
									data: "tabla="+bTabla,
									async: false,																
									success: function(datos) {
										entidadesHijas=datos;
									}
								});		
								//////
								//Si entidadesHijas es vacio, no tiene tablas relacionadas en maestra.
								alert(entidadesHijas);
								
								
								rutaBorrado="/dmcrm/APP/comun2/bajas_Pruebas.asp";

						<%end if%>


						if (document.getElementById("hidRelacionados").value!="")
						{mensajeConfirm="<%=objIdiomaGeneral.getIdioma("Seleccion_16")%>" + document.getElementById("hidRelacionados").value;	}
						if (confirm(mensajeConfirm)) {							

							$.ajax({
								type: "POST",
								url: rutaBorrado,
								data: "tabla="+bTabla+"&ids="+ids+"&conex=<%=request("conex")%>",
								success: function(datos) {
									if (datos!="")
									{ 	alert(datos); 
										/*for (var i=0;i<anSelected.length;i++)
										{ 
											var arrPosIds=datos.split("|||");
											if (arrPosIds.length>=1)
											{
												var arrCadaPos=arrPosIds[1].split(".");
												for (contPos=0;contPos<arrCadaPos.length;contPos++)
												{
													if (arrCadaPos[contPos]!="")
													{
														oTable.fnDeleteRow(anSelected[i]); 
													}													
												}
											}
										}	*/								
									}/* else
									{
										for (var i=0;i<anSelected.length;i++)
										{ oTable.fnDeleteRow(anSelected[i]); }										
									}*/
										for (var i=0;i<anSelected.length;i++)
										{ oTable.fnDeleteRow(anSelected[i]); }						
								}
							});
						}

					}
					else
					{ alert("<%=objIdiomaGeneral.getIdioma("Seleccion_17")%>"); }
				} );	
				
				$('#cerrarCampanas').click( function() {
					var bTabla = $(this).attr('rel');
					
						var anSelected = fnGetSelected( oTable );
						var borradoOK = false;
						var ids = '';
						for (var i=0;i<anSelected.length;i++)
						{
							if(i == 0)
							{
								ids = oTable.fnGetData(anSelected[i])[0];
							}
							else
							{
								ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
							}
						}
					if (ids=="")
					{
						alert("<%=objIdiomaGeneral.getIdioma("Seleccion_18")%>" );
					}
					else
					{
						if (ids.indexOf(",") != -1)
						{
							alert("<%=objIdiomaGeneral.getIdioma("Seleccion_19")%>");
						}
						else
						{
							if (confirm("<%=objIdiomaGeneral.getIdioma("Seleccion_20")%>")) {							


								$.ajax({
									type: "POST",
									url: "/dmcrm/APP/Comercial/cierreCampana.asp",
									data: "tabla="+bTabla+"&ids="+ids,
									success: function(datos) {
										 recargarAlCerrar();
									}
								});
							}
						}
					}
				} );					
				



				//********Dentro de esta funcion ponemos las acciones javascript a medida**************
				function funcionesJSaMedida() {
						//Si existe el combo cboEstadosOferta, y el valor es 0 o 1, ocultamos la columna ordenacion
					var bVis=false;
					if ( $('#cboEstadosOferta').val()=="" || $('#cboEstadosOferta').val()=="0" || $('#cboEstadosOferta').val()=="1" ) {
							bVis=true;
					}
					
					oTable.fnSetColumnVis(1, bVis);
				}
				//********

				
				$('#btnBuscarFiltros').click( function() {
						diferenciarFiltro();
														
						$('#txtSociosDuplicados').val('');															
				   		oTable.fnDraw();
						<%if trim(lcase(tabla))="club_promociones" then%>
							funcionesJSaMedida();
						<%end if%>

				});				
				
				$('#buscarDuplicados').click( function() {
//					inicializarFiltros();		
					diferenciarFiltro();					
					$('#txtSociosDuplicados').val('1');
			   		oTable.fnDraw();																				   
				});																					   
				
				$('#btnEnviarPedidos').click( function() {
					var bTabla = $(this).attr('rel');
					var anSelected = fnGetSelected( oTable );
					//parametros para enviar al borrado por AJAX
					var borradoOK = false;
					var ids = '';
					for (var i=0;i<anSelected.length;i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}
					var valoresSel=ids;						
					if (valoresSel!="") {
						$.ajax({
							type: "POST",
							url: "/dmcrm/APP/Ventas/pedidos/cambiarEstadoPedido.asp",
							data: "ids=" + valoresSel + "&est=2",
							success: function(datos) {
								oTable.fnDraw();																				   
							}
						});						
					} else {
						alert("<%=objIdiomaGeneral.getIdioma("Seleccion_30")%>");
					}
				});					
				
				$('#btnRecibirPedidos').click( function() {
					var bTabla = $(this).attr('rel');
					var anSelected = fnGetSelected( oTable );
					//parametros para enviar al borrado por AJAX
					var borradoOK = false;
					var ids = '';
					for (var i=0;i<anSelected.length;i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}
					var valoresSel=ids;						
					if (valoresSel!="") {
						$.ajax({
							type: "POST",
							url: "/dmcrm/APP/Ventas/pedidos/cambiarEstadoPedido.asp",
							data: "ids=" + valoresSel + "&est=3",
							success: function(datos) {
								oTable.fnDraw();																				   
							}
						});						
					} else {
						alert("<%=objIdiomaGeneral.getIdioma("Seleccion_31")%>");
					}
				});									
				
				$('#btnNotificarPedidos').click( function() {
					if ( confirm("<%=objIdiomaGeneral.getIdioma("Seleccion_31")%>") ) {											  
						var bTabla = $(this).attr('rel');
						var anSelected = fnGetSelected( oTable );
						//parametros para enviar al borrado por AJAX
						var borradoOK = false;
						var ids = '';
						for (var i=0;i<anSelected.length;i++)
						{
							if(i == 0)
							{
								ids = oTable.fnGetData(anSelected[i])[0];
							}
							else
							{
								ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
							}
						}
						var valoresSel=ids;						
						if (valoresSel!="") {
							$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Ventas/Pedidos/notificarPedidos.asp",
								data: "ids=" + valoresSel,
								success: function(datos) {
									//oTable.fnDraw();
									switch(datos) {
										case "0":
											alert("<%=objIdiomaGeneral.getIdioma("Seleccion_44")%>");
											break;
										case "-1":
											alert("<%=objIdiomaGeneral.getIdioma("Seleccion_32")%>");
											break;
										default:
											alert("<%=objIdiomaGeneral.getIdioma("Seleccion_33")%>");
									}
								}
							});						
						} else {
							alert("<%=objIdiomaGeneral.getIdioma("Seleccion_34")%>");
						}
					} 
				});									
				
				
				$('#quitarFiltro').click( function() {
					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/Comun2/BusquedaLimpiar.asp",
						data: "tabla=<%=tabla%>",
						success: function(datos) {
							 recargarAlCerrar();
						}
					});
			   });
				
				$('#verFiltrosAplicados').mouseover( function() {				
					$('#capaVerFiltros').show('slow');
			   });				
				
				$('#verFiltrosAplicados').mouseout( function() {				
					$('#capaVerFiltros').hide('slow');
			   });								
				
				$('#generarCampanaMarqueting').click( function() {
						var bTabla = $(this).attr('rel');
						var anSelected = fnGetSelected( oTable );
						//parametros para enviar al borrado por AJAX
						var borradoOK = false;
						var ids = '';
						for (var i=0;i<anSelected.length;i++)
						{
							if(i == 0)
							{
								ids = oTable.fnGetData(anSelected[i])[0];
							}
							else
							{
								ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
							}
						}
						document.getElementById("txtIdsOculto").value=ids;
													
				} );		
				
				$('#sociosEnviarTarjetasSeleccionados').click( function() {
					var bTabla = $(this).attr('rel');
					var anSelected = fnGetSelected( oTable );
					var borradoOK = false;
					var ids = '';
					for (var i=0;i<anSelected.length;i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}					
					
					if (ids!="")					
					{
						document.getElementById("sociosEnviarTarjetasSeleccionados").href="/dmcrm/APP/Club/socios/exportarSocios.asp?ids=" + ids;
							/*$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Club/socios/exportarSocios.asp",
								data: "ids="+ids,
								success: function(datos) {
									oTable.fnDraw();
								}
							});*/
					}
					else
					{ alert("<%=objIdiomaGeneral.getIdioma("Seleccion_25")%>");
					  return false; }
				} );			
				
				$('#sociosEnviarMailsSeleccionados').click( function() {
					var bTabla = $(this).attr('rel');
					var anSelected = fnGetSelected( oTable );
					var borradoOK = false;
					var ids = '';
					for (var i=0;i<anSelected.length;i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}					
					
					if (ids!="")					
					{
						document.getElementById("sociosEnviarMailsSeleccionados").href="/dmcrm/APP/Club/socios/enviarMailsSocios.asp?ids=" + ids;
							/*$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Club/socios/exportarSocios.asp",
								data: "ids="+ids,
								success: function(datos) {
									oTable.fnDraw();
								}
							});*/
					}
					else
					{ alert("<%=objIdiomaGeneral.getIdioma("Seleccion_26")%>");
					  return false; }
				} );		
				
				$("#pedidosConsultaNOPagados, #pedidosConsultaNOValorados").live('click', function(){															   
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'titleShow'		        : false/*,					
						'onClosed'		        : function() {
							    recargarAlCerrar();
						}*/
					}).trigger("click"); 
					return false; 	
				} );					

                /** Exportar costes **/                
                $("#btoExportarCostes").live('click', function(){															   
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: 620,
						'frameHeight'			: 400,
						'titleShow'		        : false					
					}).trigger("click"); 
					return false; 	
				} );					
				

                $('#generarFicheroCorreos').live('click', function(){	
				    var fcHref = $(this).attr('href');
                    var anSelected = fnGetSelected( oTable );
					var ids = '';
                    var actual = $(this);
					for (var i=0; i<anSelected.length; i++)
					{
						if(i == 0)
						{
							ids = oTable.fnGetData(anSelected[i])[0];
						}
						else
						{
							ids = ids + ',' + oTable.fnGetData(anSelected[i])[0];
						}
					}

                    if (ids != '') {
                         $(this).attr('href',fcHref + '?ids=' + ids);     
                         $(this).fancybox({
						    'imageScale'			: true,
						    'zoomOpacity'			: true,
						    'overlayShow'			: true,
						    'overlayOpacity'		: 0.7,
						    'overlayColor'			: '#333',
						    'centerOnScroll'		: true,
						    'zoomSpeedIn'			: 600,
						    'zoomSpeedOut'			: 500,
						    'transitionIn'			: 'elastic',
						    'transitionOut'			: 'elastic',
						    'type'					: 'iframe',
						    'frameWidth'			: 500,
						    'frameHeight'			: 300,
						    'titleShow'		        : false,
                            'onClosed'		        : function() {
						                                recargarAlCerrar();
						                              }    			
					    }).trigger("click");

                    } else { 
                        alert('Debes seleccionar al menos un pedido');
                    }
                    return false;
                });               
				

				/** Seleccionar fila **/
				$('#tablaContenido tr').live('click', function () {
					if(this['id'] != 'cabeceraTabla')
					{
						var aData = oTable.fnGetData( this );
						var iId = aData[0];
						
						if ( jQuery.inArray(iId, gaiSelected) == -1 )
						{
							gaiSelected[gaiSelected.length++] = iId;
						}
						else
						{
							gaiSelected = jQuery.grep(gaiSelected, function(value) {
								return value != iId;
							} );
						}
						$(this).toggleClass('row_selected');
					}
				} );
				
				/** Obtener seleccionados **/
				function fnGetSelected(oTableLocal)
				{
					var aReturn = new Array();
					var aTrs = oTableLocal.fnGetNodes();
					
					for ( var i=0 ; i<aTrs.length ; i++ )
					{
						if ( $(aTrs[i]).hasClass('row_selected') )
						{
							aReturn.push( aTrs[i] );
						}
					}
					return aReturn;
				}
				
				
				/** Boton seleccionar todos **/
				$('#seleccionar').click( function() {	
					var aTrs = oTable.fnGetNodes();
					for ( var i=0 ; i<aTrs.length ; i++ )
					{
						if ( !$(aTrs[i]).hasClass('row_selected') )
						{
							$(aTrs[i]).addClass('row_selected');
						}
					}
				});
				
				/** Boton deseleccionar todos **/
				$('#deseleccionar').click( function() {
					var aTrs = oTable.fnGetNodes();
					for ( var i=0 ; i<aTrs.length ; i++ )
					{
						if ($(aTrs[i]).hasClass('row_selected') )
						{
							$(aTrs[i]).removeClass('row_selected');
						}
					}	
				});
											
				$(".cEmergente").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: <%=anchoEdicionSeleccion%>,
						'frameHeight'			: <%=altoEdicionSeleccion%>,
						'titleShow'		        : false,					
						'onClosed'		        : function() {
							    recargarAlCerrar();
						}
					}).trigger("click"); 
					return false; 	

				});

				$(".cEmergenteSinRefresco").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'titleShow'		        : false
					}).trigger("click"); 
					return false; 	
				});				
				
				$(".cEmergentePequeno").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '30%',
						'frameHeight'			: '32%',
						'titleShow'		        : false,
						'onClosed'		        : function() {
							    recargarAlCerrar();
						}						
					}).trigger("click"); 
					return false; 	
				});				
				
				$(".cEmergenteMediano").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '60%',
						'frameHeight'			: '60%',
						'titleShow'		: false						
					}).trigger("click"); 
					return false; 	
				});		
				

				$(".cEmergenteIntegra").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: 800,
						'frameHeight'			: '100%',
						'titleShow'		: false						
					}).trigger("click"); 
					return false; 	
				});						

                $(".MRW").live('click', function(){
                    var mrwId = $(this).attr('href');
                    var mrwImg = $(this).find('img');
                    mrwId = mrwId.substring(mrwId.indexOf("codigo=") + 7);
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: 760,
						'frameHeight'			: 540,
						'titleShow'		        : false,					
						'onClosed'		        : function() { 
                                                    $.ajax({
                        								type: "GET",
						                        		url: "/dmcrm/APP/Ventas/Pedidos/MRW-check.aspx",
								                        data: "codigo=" + mrwId,
								                        success: function(datos) {
									                        if(datos == 1){
                                                                mrwImg.attr('src', '/dmcrm/APP/imagenes/mrwok.png');
                                                            } else {
                                                                mrwImg.attr('src', '/dmcrm/APP/imagenes/mrwko.png');
                                                            }
								                        }
							                        });
                                                  }
					}).trigger("click"); 
					return false; 	

				});

/*				$(".cEmergenteSubida").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: 600,
						'frameHeight'			: 280,
						'titleShow'		        : false,					
						'onClosed'		        : function() {
							    recargarAlCerrar();
						}
					}).trigger("click"); 
					return false; 	

				});			*/
				
				
				$(".cEmergenteFoto").live('click', function(){
					$(this).fancybox({
					    'imageScale'		: true,
                        'zoomOpacity'		: true,
                        'overlayShow'		: true,
                        'overlayOpacity'	: 0.7,
                        'overlayColor'		: '#333',
                        'centerOnScroll'	: true,
                        'zoomSpeedIn'		: 600,
                        'zoomSpeedOut'		: 500,
                        'transitionIn'		: 'elastic',
                        'transitionOut'		: 'elastic',
						'titleShow'		    : false
					}).trigger("click"); 
					return false; 	

				});				
				/** Cargar enlaces con fancybox **/
				/*$(".cEmergente").live('click', function(){*/
					/*$(".cEmergente").fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '75%',
						'frameHeight'			: '90%'*/
					/*}).trigger("click"); 
					return false; 	*/
					/*});*/
				/*});*/
			});
			

	
		</script>
        <!-- FIN CARGA JTABLES -->
<!--	</head>
	<body class="fondoPagIntranet">-->
		<!-- CABECERA DE LA SELECCION -->       

		<%

		registrosRelacionados=""
'		sqlRelacionados=" Select MBO_TABLAHIJO From MAESTRA_BORRADOS Where upper(MBO_TABLAPADRE)='" & ucase(trim(tabla)) & "' "
'		if trim(request("conex"))<>"" then  
'	  		Set rsRelacionados = connMaestra.AbrirRecordset(sqlRelacionados,0,1)
'		else
'	  		Set rsRelacionados = connCRM.AbrirRecordset(sqlRelacionados,0,1)
'		end if
'		while not rsRelacionados.eof
'			if not trim(registrosRelacionados)="" then
'				registrosRelacionados=registrosRelacionados & ", "
'			end if
'				registrosRelacionados=registrosRelacionados & lcase(rsRelacionados("MBO_TABLAHIJO"))
'			rsRelacionados.movenext
'		wend
'		rsRelacionados.cerrar
'		Set rsRelacionados = nothing
	

		
		%>

   <input type="hidden" id="hidRelacionados" name="hidRelacionados" value="<%=registrosRelacionados%>" />
<div  class="bloqueSeleccion">
		<div id="capaOpciones" >
            <ul > 
				<%if trim(lcase(tabla))="pedidos_ventas" then%>                         
	                <li><a id="anularSeleccionados" title="<%=objIdiomaGeneral.getIdioma("Seleccion_2")%>" rel="<%=tabla%>"></a></li>                                
            	    <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>                                    
                <%end if%>            
				<%if not quitarAnadir then %>
	               	<li >
    	            <!--class="capaLight posicion"-->
					<% if trim(aMedidaAlta)="" Or isnull(aMedidaAlta)  THEN  %>
            	    		<a class="cEmergente" id="anadirRegistro" href="<%=carpetaInterna%>/APP/comun2/formMantenimiento.asp?modo=alta&conex=<%=request("conex")%>&amp;tabla=<%=tabla%>&amp;filtro=<%=filtro%>&amp;valorfiltro=<%=valorfiltro%>&amp;filtro2=<%=filtro2%>&amp;valorfiltro2=<%=valorfiltro2%>&amp;nummax=<%=nummax%>&amp;nummin=<%=nummin%>&cabecera=<%=Request("Cabecera")%>" title="<%=objIdiomaGeneral.getIdioma("Seleccion_1") & tituloTabla%>" ></a>
        	        <% else  %>
    	            		<a class="cEmergente" id="anadirRegistro" href="<%=aMedidaAlta%>?modo=alta&esPrimero=1&conex=<%=request("conex")%>&amp;tabla=<%=tabla%>&amp;filtro=<%=filtro%>&amp;valorfiltro=<%=valorfiltro%>&amp;filtro2=<%=filtro2%>&amp;valorfiltro2=<%=valorfiltro2%>&amp;nummax=<%=nummax%>&amp;nummin=<%=nummin%>&cabecera=<%=Request("Cabecera")%>" title="<%=objIdiomaGeneral.getIdioma("Seleccion_1") & tituloTabla%>"></a>                    
                	<% end if %>    
	                </li>
				<%end if%> 
                <%if not quitarEliminar then 
					claseBorradoMedida=""
					if not isnull(aMedidaBorrar) And trim(aMedidaBorrar)<>"" then
						claseBorradoMedida=" class=""claseBorrarMedi"" "
					end if%> 
	                <li><a id="borrarSeleccionados" <%=claseBorradoMedida%> title="<%=objIdiomaGeneral.getIdioma("Seleccion_2")%>" rel="<%=tabla%>"></a></li>                
                <%end if%>       
                <% if not quitarSeleccionar then%>
    	            <li><a id="seleccionar" title="<%=objIdiomaGeneral.getIdioma("Seleccion_3")%>"></a></li>
        	        <li ><a id="deseleccionar" title="<%=objIdiomaGeneral.getIdioma("Seleccion_4")%>"></a></li>                                             
                <%end if%>
                <%if not quitarAnadir Or not quitarEliminar then %>                		
            	    <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>                
                <%end if%>                
                <%if not quitarExportar then %>   
	   			    <li>
    	           		<a id="exportarExcel" href="/dmcrm/APP/comun2/seleccionenexcel.asp?tabla=<%=tabla%>&conex=<%=request("conex")%>&columnas=<%=cabeceraColumnas_Excel%>&filtro=<%=filtro%>&valorfiltro=<%=valorfiltro%>&filtro2=<%=filtro2%>&valorfiltro2=<%=valorfiltro2%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_5")%>"></a>
        	        </li>
            	    <li>
               			<a id="exportarExcelVI" href="/dmcrm/APP/comun2/seleccionenexcel.asp?tabla=<%=tabla%>&conex=<%=request("conex")%>&columnas=<%=cabeceraColumnas_Excel2%>&filtro=<%=filtro%>&valorfiltro=<%=valorfiltro%>&filtro2=<%=filtro2%>&valorfiltro2=<%=valorfiltro2%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_6")%>"></a>                    
<!--               			<a id="exportarExcelVI" href="/dmcrm/APP/comun2/seleccionenexcel2.asp?tabla=<%=tabla%>&conex=<%=request("conex")%>&columnas=<%=cabeceraColumnas_Excel2%>&filtro=<%=filtro%>&valorfiltro=<%=valorfiltro%>&filtro2=<%=filtro2%>&valorfiltro2=<%=valorfiltro2%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_6")%>"></a>-->
	                </li>
                <%end if%>
                <%if not quitarImportar then %>
<!--    	            <li >
        	        	<a class="cEmergente" id="importar" href="/dmcrm/APP/comun2/mantenimientoregistrosexcel.asp?tabla=<%=tabla%>&conex=<%=request("conex")%>&filtro=<%=filtro%>&valorfiltro=<%=valorfiltro%>&filtro2=<%=filtro2%>&valorfiltro2=<%=valorfiltro2%>&amp;h=<%=time%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_7")%>"></a>
            	    </li>                   -->
                <%end if%>
                <%if not quitarExportar Or not quitarImportar then %>                		                             
	                <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>                
                <%end if%>
                <%if not quitarBuscar then %>                
	                <li>
    	            	<%'if trim(consulta) = "" then%>
        	        		<a class="cEmergente" id="buscador" href="/dmcrm/APP/comun2/busqueda.asp?tabla=<%=tabla%>&conex=<%=request("conex")%>&filtro=<%=filtro%>&valorfiltro=<%=valorfiltro%>&cabecera=<%=Request("Cabecera")%>&filtro2=<%=filtro2%>&valorfiltro2=<%=valorfiltro2%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_8")%>"></a>
            	        <%'else%>
                	    <!--	<a id="quitarFiltro" href="<%'=request.ServerVariables("SCRIPT_NAME") & "?quitarFiltro=1&conex=" & request("conex") & "&" & request.ServerVariables("QUERY_STRING")%>"  title="<%'=objIdiomaGeneral.getIdioma("Seleccion_9")%>"></a>-->
                    	<%'end if%>
	                </li>      
                    <%if trim(filtrosAplicados)<>"" then%>    
                    <li>
                	    <a id="quitarFiltro"   title="<%=objIdiomaGeneral.getIdioma("Seleccion_9")%>"></a>
                    </li>      
                    <li>
						<a id="verFiltrosAplicados" style="padding:5px 0 0 20px; word-spacing:0.5px; letter-spacing:-0.2px;  color:#8C8C8D; width:118px;" ><%=objIdiomaGeneral.getIdioma("Seleccion_36")%></a>
                        <div id="capaVerFiltros" >
			    	    	<span class="tituloBloqueFiltros"><%=objIdiomaGeneral.getIdioma("Seleccion_35")%></span>
							<div class="bloqueFiltrosAplicados">
               					<%=filtrosAplicados%>
			                </div>
			            </div>
                    </li>
                    <%end if%>
	                <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>                                    
                <%end if%>                    
                <%if not quitarAyuda then %>                                
	            	<li><a id="ayuda" class="cEmergente" href="/dmcrm/APP/comun2/ayuda.asp" title="<%=objIdiomaGeneral.getIdioma("Seleccion_10")%>"></a></li>                
                <%end if%>                 
                
				<%if trim(lcase(tabla))="socios" then%>
	                <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>                                   
					<li style="margin-right:10px;"><a id="buscarDuplicados" title="<%=objIdiomaGeneral.getIdioma("Seleccion_22")%>" style="padding:5px 0 0 25px; color:#8C8C8D; width:118px;" ><%=objIdiomaGeneral.getIdioma("Seleccion_21")%></a></li>    <!--Errepikatuak bilatu-->
					<li style="margin-right:10px;"><a id="sociosEnviarMailsSeleccionados" href="/dmcrm/APP/Club/socios/enviarMailsSocios.asp" class="cEmergenteExportarSocios" title="<%=objIdiomaGeneral.getIdioma("Seleccion_28")%>" style="padding:0px 0 0 30px; color:#8C8C8D; width:118px; line-height:1em;" ><%=objIdiomaGeneral.getIdioma("Seleccion_27")%></a></li>    
					<li style="margin-right:10px;"><a id="sociosEnviarTarjetasSeleccionados" href="/dmcrm/APP/Club/socios/exportarSocios.asp" class="cEmergenteExportarSocios" title="<%=objIdiomaGeneral.getIdioma("Seleccion_24")%>" style="padding:0px 0 0 30px; color:#8C8C8D; width:118px; line-height:1em;" ><%=objIdiomaGeneral.getIdioma("Seleccion_23")%></a></li>    

          	    <%elseif trim(lcase(tabla))="clientes" then %>                    
	                <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>               
    	            <li style="margin-right:10px;"><a id="generarCampanaMarqueting" class="cEmergentePequeno" style="padding:2px 0 0 20px; color:#8C8C8D; width:118px; line-height:10px; "   href="/dmcrm/APP/Comercial/Empresas/AnadirCampanasTelemarqueting.asp" title="<%=objIdiomaGeneral.getIdioma("Seleccion_11")%>" ><%=objIdiomaGeneral.getIdioma("botonesEspecialesCoemrcial_1")%><br/><%=objIdiomaGeneral.getIdioma("botonesEspecialesComercial_2")%></a></li>                    
                <%elseif trim(lcase(tabla))="campanas" then %>                    
					<li style="margin-right:10px;"><a id="cerrarCampanas" title="<%=objIdiomaGeneral.getIdioma("Seleccion_13")%>" style="padding:5px 0 0 20px; color:#8C8C8D; width:118px;" ><%=objIdiomaGeneral.getIdioma("botonesEspecialesComercial_5")%></a></li>   
                               
				<%elseif trim(lcase(tabla))="socios" then%>
	                <li style=" border-left:solid 1px black; padding-right:5px;" >&nbsp;</li>                                   
					<li style="margin-right:10px;"><a id="buscarDuplicados" title="<%=objIdiomaGeneral.getIdioma("Seleccion_22")%>" style="padding:5px 0 0 25px; color:#8C8C8D; width:118px;" ><%=objIdiomaGeneral.getIdioma("Seleccion_21")%></a></li>    <!--Errepikatuak bilatu-->
					<li style="margin-right:10px;"><a id="sociosEnviarMailsSeleccionados" href="/dmcrm/APP/Club/socios/enviarMailsSocios.asp" class="cEmergenteExportarSocios" title="<%=objIdiomaGeneral.getIdioma("Seleccion_28")%>" style="padding:0px 0 0 30px; color:#8C8C8D; width:118px; line-height:1em;" ><%=objIdiomaGeneral.getIdioma("Seleccion_27")%></a></li>    
					<li style="margin-right:10px;"><a id="sociosEnviarTarjetasSeleccionados" href="/dmcrm/APP/Club/socios/exportarSocios.asp" class="cEmergenteExportarSocios" title="<%=objIdiomaGeneral.getIdioma("Seleccion_24")%>" style="padding:0px 0 0 30px; color:#8C8C8D; width:118px; line-height:1em;" ><%=objIdiomaGeneral.getIdioma("Seleccion_23")%></a></li>    

				<%elseif trim(lcase(tabla))="pedidos_ventas" then%>                         
        	        <li style="margin-right:10px;" rel="2" ><a id="btnEnviarPedidos" rel="<%=tabla%>" style="padding:2px 0 0 25px; color:#8C8C8D; width:118px; line-height:10px;"  title="Poner como enviados los pedidos seleccionados" ><%=objIdiomaGeneral.getIdioma("Seleccion_37")%><br/><%=objIdiomaGeneral.getIdioma("Seleccion_38")%></a></li>                                      
           	        <li style="margin-right:10px;" rel="3" ><a id="btnRecibirPedidos" rel="<%=tabla%>" style="padding:2px 0 0 25px; color:#8C8C8D; width:118px; line-height:10px;"  title="Poner como entregados los pedidos seleccionados" ><%=objIdiomaGeneral.getIdioma("Seleccion_39")%><br/><%=objIdiomaGeneral.getIdioma("Seleccion_40")%></a></li>                                      
           	        <li style="margin-right:10px;"><a id="btnNotificarPedidos" rel="<%=tabla%>" title="<%=objIdiomaGeneral.getIdioma("Seleccion_42")%>" style="padding:5px 0 0 30px; color:#8C8C8D; width:118px;" ><%=objIdiomaGeneral.getIdioma("Seleccion_41")%></a></li>  
					<!-- <li style="margin-right:10px;"><a id="pedidosConsultaNOPagados" href="/dmcrm/APP/Ventas/Pedidos/notificarNoPagados.asp"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_45a") & " " & objIdiomaGeneral.getIdioma("Seleccion_45b")%>" style="padding:2px 0 0 32px; color:#8C8C8D; width:118px; line-height:10px;" ><%=objIdiomaGeneral.getIdioma("Seleccion_45a") & "<br />" & objIdiomaGeneral.getIdioma("Seleccion_45b")%></a></li> -->
                    <li style="margin-right:10px;"><a id="pedidosConsultaNOValorados" href="/dmcrm/APP/Ventas/Pedidos/notificarValoracionesTrustedShop.asp"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_46a") & " " & objIdiomaGeneral.getIdioma("Seleccion_46b")%>" style="padding:2px 0 0 32px; color:#8C8C8D; width:118px; line-height:10px;" ><%=objIdiomaGeneral.getIdioma("Seleccion_46a") & "<br />" & objIdiomaGeneral.getIdioma("Seleccion_46b")%></a></li>
                    <li style="margin-right:10px;"><a id="generarFicheroCorreos" href="/dmcrm/APP/Ventas/Pedidos/generarFicheroCorreos.asp"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_47a") & " " & objIdiomaGeneral.getIdioma("Seleccion_47b")%>" style="padding:2px 0 0 32px; color:#8C8C8D; width:118px; line-height:10px;"><%=objIdiomaGeneral.getIdioma("Seleccion_47a") & "<br />" & objIdiomaGeneral.getIdioma("Seleccion_47b")%></a></li>
                    <li style="margin-right:10px;"><a id="btoExportarCostes" href="/dmcrm/APP/Ventas/Pedidos/pedidosweb-sel-exportar-costes.asp"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_48a") & " " & objIdiomaGeneral.getIdioma("Seleccion_48b")%>" style="padding:2px 0 0 32px; color:#8C8C8D; width:118px; line-height:10px;"><%=objIdiomaGeneral.getIdioma("Seleccion_48a") & "<br />" & objIdiomaGeneral.getIdioma("Seleccion_48b")%></a></li>

				<%elseif trim(lcase(tabla))="socios_comer" then%>                                              
	                <a id="anadirRegistro" href="/dmcrm/app/club/socios/altaSocios.asp?modo=alta" title="<%=objIdiomaGeneral.getIdioma("Seleccion_1") & tituloTabla%>" ></a>
				<%'elseif trim(lcase(tabla))="club_liquidaciones" then%>                                                                  
					<!--<li style="margin-right:10px;"><a id="verLiquiMensuales" class="cEmergenteSinRefresco" href="/DMCrm/APP/Club/transacciones/LiquidacionesListado.asp" title="" style="padding:2px 0 0 25px; color:#8C8C8D; width:118px; line-height:10px;" >Liquidaciones menduales</a></li>  --> 
                <%end if%>
                

                
          	    <%'A�adimos otra opcion auxliar al final
				if trim(lcase(tabla))="clientes" or trim(lcase(tabla))="socios" then %>
        	        <li style="margin-right:10px;"><a id="generarCampanaMailing" class="cEmergenteMediano"  style="padding:0px 0 0 32px; color:#8C8C8D; width:118px; line-height:10px;"   href="/dmcrm/APP/Comercial/Empresas/AnadirCampanasComunicacion.asp?tipoCamp=M&tabla=<%=tabla%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_12")%>" ><%=objIdiomaGeneral.getIdioma("botonesEspecialesComercial_3")%><br/><%=objIdiomaGeneral.getIdioma("botonesEspecialesComercial_4")%></a></li>                
        	        <li style="margin-right:10px;"><a id="generarCampanaSMS" class="cEmergenteMediano"  style="padding:0px 0 0 32px; color:#8C8C8D; width:118px; line-height:10px;"   href="/dmcrm/APP/Comercial/Empresas/AnadirCampanasComunicacion.asp?tipoCamp=S&tabla=<%=tabla%>"  title="<%=objIdiomaGeneral.getIdioma("Seleccion_43")%>" ><%=objIdiomaGeneral.getIdioma("botonesEspecialesComercial_6")%><br/><%=objIdiomaGeneral.getIdioma("botonesEspecialesComercial_7")%></a></li>                                    
                <%end if%>    
            </ul>
        	    <input type="text" id="txtIdsOculto" name="txtIdsOculto" style="width:1px; display:none;" />                
            
		</div>     
        
        <div id="contenedorSeleccion">
            <table border="0" width="99%" cellspacing="1" cellpadding="0" id="tablaContenido" rel="<%=tabla%>" style="z-index:1; padding:0px; margin:0px;">
                <thead>
                    <tr id="cabeceraTabla">
                    	<%
							'Sql donde guardaremos las sumas de los pies a mostrar
							sqlTotalesPie=""
						
							'***** PINTAMOS LA CABECERA DE LAS CLAVES. SI NO SON VISIBLES SE OCULTAN EN EL JTABLE *****
							for i = 0 to ubound(camposClave,2)
								%><th><%=camposClave(0,i)%></th><%
							next
							'***** PINTAMOS LA CABECERA DE LOS CAMPOS *****
							for i = 0 to ubound(camposGenerales,2)
								if camposGenerales(1,i) then
									%><th><%=camposGenerales(0,i)%></th><%
								end if
							next					

							session(denominacion & Tabla & "_consultTotalesPie")=sqlTotalesPie
						%>
                        <th  class="thOpciones" >
	                        <%=objIdiomaGeneral.getIdioma("Seleccion_14")%>
                        </th>
                    </tr>
                </thead>
                <tbody>
                	<!--Aqui cargamos el contenido por ajax-->
                </tbody>
                <tfoot id="pieTabla">
                    <tr style="display:none;"><th></th></tr>
                </tfoot>
            </table>
        </div>

</div>        

<script language="Javascript">
	function recargarAlCerrar()
	{
		if (document.forms[0]!=null)
		 { document.forms[0].submit(); }
		 else { window.location.reload(); }		
//		document.location.reload();
	}
	function recargarPaginaPadre()
	{
		//oTable.fnDraw();
	}
</script>    
<!--	</body>
</html>-->


