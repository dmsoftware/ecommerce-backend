<%'Indicamos que los datos van en ISO
response.Charset="ISO-8859-1"
'**********************************%>

<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->

<%
'Validamos que la llamada sea correcta ***********************
validacion=decode(request("id"))
if validacion<>year(date) & "935" & month(date) & day(date) then response.end
'*************************************************************

function escaparCaracteres(str,codificar)
	dim out

	if isnull(str) = false then
		out = Replace(str, """", "\""")		
		if 	codificar then
			out = Replace(out, "\""", "''")				
			out = Replace(out, "/""", "''")						
			out = Replace(out, "\", "\\")		
		end if	
		
		dim contAscii
		For contAscii = 0 To 31 
			out = Replace(out, Chr(contAscii), " ")				
		Next
		out = Replace(out, Chr(127), " ")	

        'M�s limpieza de caracteres
        caracteresMalos  = "��������������������������"
        caracteresBuenos = "aoaeiouaeiouaeiou--aeiouac"
        Aeliminar = "��^~��`""'���" & chr(13) & chr(10) & chr(9) & chr(160)
	    for i = 1 to len(caracteresMalos)
		    out = replace(out, mid(caracteresMalos,i,1),mid(caracteresBuenos,i,1))
	    next
	    for i = 1 to len(Aeliminar)
		    out = replace(out, mid(Aeliminar,i,1),"")
	    next

		
	end if
	if codificar then out = server.HTMLEncode(out)
	escaparCaracteres = out
end function

function Limpiar(texto)
    texto=replace(texto,"'","")
    Limpiar=texto
end function

sinBarraSeparadora=false
caracterSeparador=" | "
if request("sinBarraSeparadora")="1" then
	caracterSeparador=" "
end if

esMemo=false
castingMemno1=""
castingMemno2=""
if trim(request("esMemo"))="1" then
	esMemo=true
	castingMemno1="CAST("
	castingMemno2=" as varchar(max)) "	
end if

NumDescripciones=request("NumDescripciones")
tieneVariasDescripciones=false
strDescripciones=""
if trim(request("NumDescripciones"))<>"" And isnumeric(trim(request("NumDescripciones"))) then
	tieneVariasDescripciones=true
	for contDescripciones=2 to NumDescripciones 
        strDescripciones=strDescripciones & "  + '" & caracterSeparador & "'  + isnull(" & request("DescripcionExterna" + cstr(contDescripciones)) & ",'') "
    next 
end if  

sql="select " & request("ClaveExterna") & "," & castingMemno1 & request("DescripcionExterna") & castingMemno2
if tieneVariasDescripciones then
	sql=sql & " " & strDescripciones & " "
end if
sql=sql & " from " & request("tabla")

if tieneVariasDescripciones then
	sql=sql & " WHERE " & castingMemno1 & request("DescripcionExterna") & castingMemno2 & strDescripciones & " LIKE '%" & request("query") & "%' "
else
    sql=sql & " WHERE " & castingMemno1 & request("DescripcionExterna") & castingMemno2 & " LIKE '%" & request("query") & "%' "
end if
'if trim(request("filtroExt"))<>"" And trim(request("valorFiltroExt"))<>"" And trim(EVAL(request("valorFiltroExt")))<>"" then
'    sql = sql & " And " & trim(request("filtroExt")) & "=" & EVAL(request("valorFiltroExt"))
'end if
sql=sql & " order by " & castingMemno1 & request("DescripcionExterna") & castingMemno2
if tieneVariasDescripciones then
	sql=sql & " " & strDescripciones & " "
end if
'if session("codigoUsuario")="49" then
'	response.Write(sql)
'end if
if trim(request("conex"))<>"" then  
	Set rstDatos = connMaestra.AbrirRecordset(sql,0,1)
else
	Set rstDatos = connCRM.AbrirRecordset(sql,0,1)
end if	

while not rstDatos.eof
    strSuggestions=strSuggestions & "'" & escaparCaracteres(rstDatos(1),false) & "',"
    strData=strData & "'" & rstDatos("" & request("ClaveExterna")) & "',"
    rstDatos.movenext

wend
if len(strSuggestions)>0 then strSuggestions=left(strSuggestions,len(strSuggestions)-1)
if len(strData)>0 then strData=left(strData,len(strData)-1)
rstDatos.cerrar
%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->

{
 query:'<%=request("query")%>',
 suggestions:[<%=strSuggestions%>],
 data:[<%=strData%>]
}
