<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<%' PARAMETROS DE LA APLICACI�N

ids = request("ids")
tabla = request("tabla")
'conexion = session(denominacion & "_" & request("conex"))

clave = claveTabla(tabla, tipoDeClave)


function relaciones1N(tabla)
	denominacionTabla = ""
	Redim arrRelaciones(-1)
	Redim strutRelacion(3) 
	query = "SELECT codTabla,tabla, campo,(CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario,'1' as ordenado FROM maestra WHERE tabla = '" & tabla & "G' "
	query = query & " union "
	query = query & " SELECT codTabla,tabla, campo,(CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario,'2' as ordenado FROM maestra WHERE tablaExt = '" & tabla & "' AND ClaveExt=1  "
	query = query & " order by ordenado,codTabla "

	
	if trim(request("conex"))<>"" then  
		Set rsResul = connMaestra.AbrirRecordset(query,3,1)
	else
		Set rsResul = connCRM.AbrirRecordset(query,3,1)
	end if	
	
	i = 0
	while not rsResul.eof
		redim preserve arrRelaciones(i)
		if trim(denominacionTabla)="" then
			denominacionTabla=rsResul("nomUsuario")
			MostrarEnTrabla=True			
		else
			denominacionTabla=""

			query = "SELECT (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario FROM maestra WHERE Tabla='" & rsResul("tabla") & "G'"
			if trim(request("conex"))<>"" then  
				set rsNombre = connMaestra.abrirRecordSet(query,3,1)
			else
				set rsNombre = connCRM.abrirRecordSet(query,3,1)
			end if			
			
			if not rsNombre.eof then
				denominacionTabla = rsNombre("nomUsuario")
			end if
			rsNombre.cerrar
			Set rsNombre = nothing

			MostrarEnTrabla=False
			query = "SELECT MostrarEnTablaExt FROM maestra WHERE Tabla='" &  rsResul("tabla") & "' And ClaveExt=1  And TablaExt='"&tabla&"'  "
			if trim(request("conex"))<>"" then  
				set rsMostrar = connMaestra.abrirRecordSet(query,3,1)
			else
				set rsMostrar = connCRM.abrirRecordSet(query,3,1)
			end if			
			
			if not rsMostrar.eof then
				MostrarEnTrabla = rsMostrar("MostrarEnTablaExt")
			end if
			rsMostrar.cerrar
			Set rsMostrar = nothing		

			
		end if

		
		strutRelacion(0) = denominacionTabla
		strutRelacion(1) = rsResul("tabla")
		strutRelacion(2) = rsResul("campo")
		strutRelacion(3) = MostrarEnTrabla
		arrRelaciones(i) = strutRelacion		
		i = i + 1
		rsResul.moveNext()
	wend	
	rsResul.cerrar()
	Set rsResul = nothing
	relaciones1N = arrRelaciones
end function

function relacionesMN(tabla)
	Redim arrRelaciones(-1)
	Redim strutRelacion(1) '0 -> c�digo 1-> nombre
	query = "SELECT mamn_codigo, MAMN_TEXTO0 as mamn_textomn FROM maestra_mn WHERE mamn_tabla1 = '" & tabla & "'"
	if trim(request("conex"))<>"" then  
		Set rsResul = connMaestra.AbrirRecordset(query,3,1)
	else
		Set rsResul = connCRM.AbrirRecordset(query,3,1)
	end if	
	
	i = 0
	while not rsResul.eof
		redim preserve arrRelaciones(i)
		strutRelacion(0) = rsResul("mamn_codigo")
		strutRelacion(1) = rsResul("mamn_textomn")
		arrRelaciones(i) = strutRelacion		
		i = i + 1
		rsResul.moveNext()
	wend	
	rsResul.cerrar()
	Set rsResul = nothing
	relacionesMN = arrRelaciones
end function

'***** BORRAMOS LOS FICHEROS ASOCIADOS *****
directorioAr = "/DMCrm/APP/UsuariosFtp/archivos"
directorioIm = "/DMCrm/APP/UsuariosFtp/imagenes"
directorioThn = "/DMCrm/APP/UsuariosFtp/thn"

query = "SELECT Campo FROM maestra WHERE tabla = '" & tabla & "' and (tipo='X' Or tipo='XI' Or tipo='XA') order by orden"
if trim(request("conex"))<>"" then  
	Set rsResul = connMaestra.AbrirRecordset(query,3,1)
else
	Set rsResul = connCRM.AbrirRecordset(query,3,1)
end if


while not rsResul.eof
	sql = "SELECT " & rsResul("campo") & " FROM " & tabla & " WHERE "& clave	
	if trim(ids)<>"" then
		if tipoDeClave = "T" then 
		  sql = sql & " IN ('" & replace(ids,",","','") & "')"
		else
		  sql = sql & " IN (" & ids & ")"
		end if
	end if	

	if trim(request("conex"))<>"" then  
		Set rsFicheros = connMaestra.AbrirRecordset(sql,3,1)
	else
		Set rsFicheros = connCRM.AbrirRecordset(sql,3,1)
	end if	
	

	while not rsFicheros.eof
		if trim(rsFicheros(0)) <> "" then
		   nombreFicheroABorrar = rsFicheros(0)
		   '***** BORRAMOS FISICAMENTE EL FICHERO *****
		   Set fso = CreateObject("scripting.FileSystemObject")
			if not instr(NombreFicheroABorrar,"../") > 0 then		   
			   if fso.FileExists("" & directorioAr & "\" & NombreFicheroABorrar) then	   
			      fso.DeleteFile "" & directorioAr & "\" & NombreFicheroABorrar
			   end if

			   if fso.FileExists("" & directorioIm & "\" & NombreFicheroABorrar) then	   
			      fso.DeleteFile "" & directorioIm & "\" & NombreFicheroABorrar
			   end if
			   
			   nomFicheroAux1=""
			   nomFicheroAux2=""			   
			   arrNombreFicheroABorrar=split(nombreFicheroABorrar,".")
			   if ubound(arrNombreFicheroABorrar)>0 then
				   	nomFicheroAux1=arrNombreFicheroABorrar(0) & "_0_15.jpg"
				   	nomFicheroAux2=arrNombreFicheroABorrar(0) & "_0_15.jpg"					
			   end if
			   if trim(nomFicheroAux1)<>"" then
				   if fso.FileExists("" & directorioThn & "\" & NombreFicheroABorrar & "_0_15.jpg") then	   
					  fso.DeleteFile "" & directorioThn & "\" & NombreFicheroABorrar
				   end if	
				end if
				
			   if trim(nomFicheroAux1)<>"" then				
				   if fso.FileExists("" & directorioThn & "\" & NombreFicheroABorrar & "_200_0.jpg") then	   
					  fso.DeleteFile "" & directorioThn & "\" & NombreFicheroABorrar
				   end if				   		   			   			   
				end if			   		
		   end if
		   Set fso = nothing
	    end if
		rsFicheros.movenext
	wend
	
	rsFicheros.cerrar 
	Set rsFicheros = nothing				
	rsResul.movenext
wend

rsResul.cerrar()
Set rsResul = nothing
'*******************************************


sub obtenerRegistrosBorrar(cadaTabla)

		registrosRelacionados=""
		sqlRelacionados=" Select M.Tabla,M.Campo as CampoClave,M2.Campo as CampoExt "
		sqlRelacionados= sqlRelacionados & " From (( MAESTRA M "
		sqlRelacionados= sqlRelacionados & " inner join MAESTRA_BORRADOS MB on (M.Tabla=MB.MBO_TABLAHIJO) ) "
		sqlRelacionados= sqlRelacionados & " inner join MAESTRA M2 on (ltrim(rtrim(upper(M2.TablaExt)))='" & ucase(trim(cadaTabla)) & "' And M2.ClaveExt=1 And M2.Tabla=M.Tabla ) ) "
		sqlRelacionados= sqlRelacionados & " Where upper(ltrim(rtrim(MB.MBO_TABLAPADRE)))='" & ucase(trim(cadaTabla)) & "' And M.Clave=1 "

'response.Write(sqlRelacionados)

		
  		
		if trim(request("conex"))<>"" then  
			Set rsRelacionados = connMaestra.AbrirRecordset(sqlRelacionados,0,1)
		else
			Set rsRelacionados = connCRM.AbrirRecordset(sqlRelacionados,0,1)
		end if		

		while not rsRelacionados.eof

'		sqlCadaRelacion=" Select M.Tabla,M.Campo as CampoClave,M2.Campo as CampoExt "
'		sqlCadaRelacion= sqlCadaRelacion & " From (( MAESTRA M "
'		sqlCadaRelacion= sqlCadaRelacion & " inner join MAESTRA_BORRADOS MB on (M.Tabla=MB.MBO_TABLAHIJO) ) "
'		sqlCadaRelacion= sqlCadaRelacion & " inner join MAESTRA M2 on (trim(ucase(M2.TablaExt))='" & ucase(trim(cadaTabla)) & "' And M2.ClaveExt=True And M2.Tabla=M.Tabla ) ) "
'		sqlCadaRelacion= sqlCadaRelacion & " Where ucase(trim(MB.MBO_TABLAPADRE))='" & ucase(trim(cadaTabla)) & "' And M.Clave=True "
'response.Write("@"&sqlCadaRelacion&"@<br/>")
'response.End()
'		Set rsCadaRelacionados = connCRM.AbrirRecordset(sqlCadaRelacion,0,1)
''		if not rsCadaRelacionados.eof then
'			response.Write(sqlCadaRelacion & "<br/>")
'			obtenerCadaRegistroBorrar()
''			response.End()		
''		end if
'		rsCadaRelacionados.cerrar
'		Set rsCadaRelacionados = nothing

			obtenerCadaRegistroBorrar(rsRelacionados)	
			
			rsRelacionados.movenext
		wend
		rsRelacionados.cerrar
		Set rsRelacionados = nothing

	'obtenerRegistrosBorrar=respuesta
end sub

sub obtenerCadaRegistroBorrar(rsRelacionados)

		registrosBorrar=""	

		sqlRegistrosBorrar=" Select " & rsRelacionados("CampoClave") & " as Codigo From " & rsRelacionados("tabla") & " Where " & rsRelacionados("CampoExt") & " in (" & ids & ") "
		if trim(request("conex"))<>"" then  
			Set rsRegistrosBorrar = connMaestra.AbrirRecordset(sqlRegistrosBorrar,0,1)
		else
			Set rsRegistrosBorrar = connCRM.AbrirRecordset(sqlRegistrosBorrar,0,1)
		end if		
		
		while not rsRegistrosBorrar.eof
			if trim(registrosBorrar)<>"" then
				registrosBorrar=registrosBorrar & ","
			end if
			registrosBorrar=registrosBorrar & rsRegistrosBorrar("Codigo")
			rsRegistrosBorrar.movenext
		wend
		rsRegistrosBorrar.cerrar
		Set rsRegistrosBorrar = nothing
			
		if trim(registrosBorrar)<>"" then
			sqlcadaBorrado=" Delete From " & rsRelacionados("tabla") & " Where " & rsRelacionados("CampoClave") & " in (" & registrosBorrar & ") " 				
			'response.Write(sqlcadaBorrado & "<br/>")
			if trim(request("conex"))<>"" then  
				connMaestra.execute sqlcadaBorrado,intNumRegistros
			else
				connCRM.execute sqlcadaBorrado,intNumRegistros
			end if			
			
		end if
		
end sub

''''''''''''''

if trim(cstr(session("codigoUsuario")))="49" then 
'	obtenerRegistrosBorrar(tabla)
end if

'''''''''''''

	'****** Los parametros se pasan por sesi�n *******
	session(denominacion & "_codigoelemento")=""
	rutaBorradoExec="/DMCrm/APP/comun2Triggers/borrados/" & trim(lcase(tabla)) & "_BorradoExec.asp"
	
	dim FS
	set FS=Server.CreateObject("Scripting.FileSystemObject")   
	if FS.FileExists(server.MapPath(rutaBorradoExec))=true then
		session(denominacion & "_codigoelemento") = ids
		Server.Execute(rutaBorradoExec)
	end if		
	set fs=nothing	 			
	
	
		
			'Hay que comprobar que los registros no tienen relaciones
			arrIds=split(ids,",")
		
			tieneAlgunaRelacion=false
			idsAux=""	
			descripcionValidacion=""
			posIds=""
			if ubound(arrIds)>0 then
			'Hay mas de un ID
				descripcionValidacion="Existen registros que no se han podido eliminar porque estan relacionados con una de las siguientes entidades: "	
				descripcionEntidades=""
				entidadesEcritas=","
				for contIds = 0 to ubound(arrIds)
					tieneRelacion=false
					arrRelaciones1N = relaciones1N(tabla)
					for i = 0 to ubound(arrRelaciones1N)
						arrRelacion = arrRelaciones1N(i)				
					
						entidad=arrRelacion(0)
						tablaEntidad=arrRelacion(1)
						campoEntidad=arrRelacion(2)		
						
						if arrRelacion(3) then
							if trim(lcase(tabla))<>trim(lcase(tablaEntidad)) And trim(lcase(tabla & "g"))<>trim(lcase(tablaEntidad)) then
								sqlExisteCodigo=" Select count(" & campoEntidad & ") as contCodigo From " & tablaEntidad & " Where " & campoEntidad & "=" & trim(arrIds(contIds))
								if trim(request("conex"))<>"" then  
									Set rsExisteCodigo = connMaestra.AbrirRecordset(sqlExisteCodigo,0,1)
								else
									Set rsExisteCodigo = connCRM.AbrirRecordset(sqlExisteCodigo,0,1)
								end if						
								
								if rsExisteCodigo("contCodigo")>0 then 
									tieneRelacion=true
									if not instr(entidadesEcritas,","& entidad &",")>0 then
										entidadesEcritas=entidadesEcritas & "," & entidad & ","
										if trim(descripcionEntidades)<>"" then
											descripcionEntidades=descripcionEntidades & ", "
										end if
										descripcionEntidades=descripcionEntidades & entidad
										tieneAlgunaRelacion=true
									end if
									
								end if
								rsExisteCodigo.cerrar()
								Set rsExisteCodigo = nothing					
							end if
						end if	
						
					next	
					
					if not tieneRelacion And trim(arrIds(contIds))<>"" then
						if trim(idsAux)<>"" then
							idsAux=idsAux & ","
						end if
						idsAux=idsAux & arrIds(contIds)
						if trim(posIds)<>"" then
							posIds=posIds&","
						end if
						posIds=posIds & contIds				
					end if
					
				next
		
				descripcionValidacion=descripcionValidacion & descripcionEntidades
		
			elseif ubound(arrIds)=0 then
			'Solo hay un ID
				idsAux=ids
				descripcionValidacion="No se puede eliminar el registro, porque esta relacionado con las siguientes entidades: "
				arrRelaciones1N = relaciones1N(tabla)
				for i = 0 to ubound(arrRelaciones1N)
					arrRelacion = arrRelaciones1N(i)				
					
					entidad=arrRelacion(0)
					tablaEntidad=arrRelacion(1)
					campoEntidad=arrRelacion(2)
					
					if arrRelacion(3) then
						if trim(lcase(tabla))<>trim(lcase(tablaEntidad)) And trim(lcase(tabla & "g"))<>trim(lcase(tablaEntidad)) then
								sqlExisteCodigo=" Select count(" & campoEntidad & ") as contCodigo From " & tablaEntidad & " Where " & campoEntidad & "=" & trim(ids)
								if trim(request("conex"))<>"" then  
									Set rsExisteCodigo = connMaestra.AbrirRecordset(sqlExisteCodigo,0,1)
								else
									Set rsExisteCodigo = connCRM.AbrirRecordset(sqlExisteCodigo,0,1)
								end if						
								
								if rsExisteCodigo("contCodigo")>0 then 
									idsAux=""	
									if tieneAlgunaRelacion then
										descripcionValidacion=descripcionValidacion & ", "
									end if
									tieneAlgunaRelacion=true
									descripcionValidacion=descripcionValidacion & entidad										
								end if
								rsExisteCodigo.cerrar()
								Set rsExisteCodigo = nothing
						end if			
					end if
								
				next	
				
				if trim(idsAux)<>"" then
					posIds=0
				end if
				
			end if
		
			ids=idsAux
		
		
			if not tieneAlgunaRelacion then
				descripcionValidacion=""
			else
				descripcionValidacion= descripcionValidacion '& "|||" & posIds	
			end if
		
	
	'*************************************************	



	


	if trim(ids)<>"" then

'		'Borramos las desscripciones de los idiomas, de los registros que van a ser eliminados
'		sql = "SELECT count(*) FROM maestra WHERE tabla = '" & tabla & "' and (tipo = 'TI' Or tipo = 'MI' Or tipo = 'MEI' ) "
'		Set rsMaestra = connCRM.AbrirRecordset(sql,3,1)
		
'		if not rsMaestra.EOF then
'			numRegistros = rsMaestra(0)
'			if numRegistros > 0 then
				sqlDeleteIdiomas="DELETE FROM IDIOMAS_DESCRIPCIONES WHERE upper(IDID_TABLA) = '" & trim(ucase(tabla)) & "' AND IDID_INDICE in (" & ids & ")"
				connCRM.execute sqlDeleteIdiomas,intNumRegistros

'			end if
'		end if
'		rsMaestra.cerrar()
'		Set rsMaestra = nothing


		''Borramos los registros que han superado las restricciones
		sql = "DELETE FROM " & tabla & " WHERE " & clave
		if tipoDeClave = "T" then 
		  sql = sql & " IN ('" & replace(ids,",","','") & "')"
		else
		  sql = sql & " IN (" & ids & ")"
		end if
		if trim(request("conex"))<>"" then  
			connMaestra.execute sql,intNumRegistros
		else
			connCRM.execute sql,intNumRegistros
		end if		
		
	end if	
	
	
	response.Write(descripcionValidacion)


'if not tieneAlgunaRelacion then
'	descripcionValidacion="-1"
'end if





%>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->
