<%
function CampoOrdenacion (Tabla)
'devuelve el campo de ordenacion de la tabla Tabla
  dim sql
  sql="select Campo from maestra where tabla='" & Tabla & "' AND Ordenacion=1"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  CampoOrdenacion=rsResul(0)
  rsResul.cerrar
end function

function EsClave (Tabla, Campo)
  dim sql
  sql="select Clave from Maestra where Tabla='" & Tabla & "' and Campo ='" & Campo &"'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  EsClave=rsResul(0)
  rsResul.cerrar
end function

function tieneCampoMultiIdioma(Tabla)
  dim sql
  sql="select Campo from MAESTRA where UPPER(Tabla) = '" & ucase(Tabla) & "' and Tipo = 'TI'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  
  if not rsResul.eof then
  	tieneCampoMultiIdioma=rsResul(0)
  else
  	tieneCampoMultiIdioma=NULL
  end if
  rsResul.cerrar
end function

function tieneCampoExterno(Tabla)
  dim sql
  sql="select tablaExt from MAESTRA where UPPER(Tabla) = '" & ucase(Tabla) & "' and ClaveExt = 1"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  
  do while not rsResul.eof
  	 if strAux <> "" then
	 	strAux = strAux & "," & rsResul(0)
	 else
	 	strAux = rsResul(0)
	 end if
  	rsResul.movenext
  loop
  if strAux <> "" then
  	tieneCampoExterno=strAux
  else
  	tieneCampoExterno=NULL
  end if
  rsResul.cerrar
end function

function Tipo (Tabla, Campo)
  dim sql
  sql="select Tipo from Maestra where Tabla='" & Tabla & "' and Campo ='" & Campo &"'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,0,1)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,0,1)
	end if  
	if not rsResul.EOF then
	  Tipo=rsResul(0)
    else
	  Tipo=""
    end if
  rsResul.cerrar
end function

function NomUsuario (Tabla, Campo)
  
  sql="select (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario from Maestra where Tabla='" & Tabla & "' and Campo ='" & Campo &"'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  NomUsuario = rsResul(0)
  rsResul.cerrar
end function

function LongitudPresentacion (Tabla, Campo)
  
  sql="select LongitudPresentacion from Maestra where Tabla='" & Tabla & "' and Campo ='" & Campo &"'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  LongitudPresentacion =rsResul(0)
  rsResul.cerrar
end function

function LongitudMax (Tabla, Campo)
  sql="select LongitudMax from Maestra where Tabla='" & Tabla & "' and Campo ='" & Campo &"'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  LongitudMax =rsResul(0)
  rsResul.cerrar
end function

'ClaveExt devuelve true si el campo es de clave externa, y devuelve de qu� tabla, qu� campo es la descripci�n
' y en ClaveExterna devuelve el campo clave de la tabla TablaExt.
'Adem�s devuelve, el campo que act�a como Filtro, si existe, y el valor del filtro.
function ClaveExt (Tabla, Campo, byref TablaExt, byref CampoExt, byref ClaveExterna, byref FiltroExt, byref ValorFiltroExt)
  sql="select ClaveExt, TablaExt, CampoExt0 as CampoExt, FiltroExt, ValorFiltroExt from Maestra where Tabla='" & Tabla & "' and Campo ='" & Campo &"'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  if rsResul(0) then
     TablaExt=rsResul("TablaExt")
     CampoExt=rsResul("CampoExt")
     FiltroExt=rsResul("FiltroExt")
     ValorFiltroExt=rsResul("ValorFiltroExt")
     'necesito saber el campo clave de la tabla "TablaExt"
     sql="select Campo from Maestra where Tabla='" & rsResul("TablaExt") & "' and Clave=1"
		if trim(request("conex"))<>"" then  
		     Set rsClave = connMaestra.AbrirRecordset(sql,3,3)
		else
		     Set rsClave = connCRM.AbrirRecordset(sql,3,3)
		end if	 

     if not rsClave.eof then
       'En rsClave("Campo") tengo el campo clave de la tabla TablaExt
       ClaveExterna=rsClave("Campo")
     end if
     rsClave.cerrar()
     set rsClave=nothing
     ClaveExt =true
  else  
     ClaveExt=false
  end if
  rsResul.cerrar
end function

'ValorClaveExterna devuelve el valor del campo Campo, en la tabla Tabla, sabiendo que la clave es Clave y su tipo de datos es TIPOCLAVE y buscamos el c�digo Codigo
function ValorClaveExterna (Tabla, Campo, Clave, TipoClave, Codigo)

  'si el valor del Codigo es vacio salimos
  if Codigo<>"" then
	  
	  sql="select " & Campo  & " from " & Tabla & " where " & Clave & "=" 
	  'El tipo de datos que es la clave (texto o n�mero). Lo obtenemos de TipoClave
	  if TipoClave="T" then
	    sql=sql & "'" & codigo & "'"
	  else
	    sql=sql & codigo
	  end if
	
		if trim(request("conex"))<>"" then  
		  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
		else
		  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
		end if	

	  'si no devuelve ningun valor, devolvemos una cadena vacia
	  if rsResul.eof then
	     ValorClaveExterna=""
	  else
	     ValorClaveExterna =rsResul(0)
	  end if
	  rsResul.cerrar
  end if
end function  

'ClaveTabla devuelve el camo CLAVE de la tabla Tabla, y en Tipo devuelve el tipo de la misma
function ClaveTabla (Tabla, byref Tipo)
  strsql="select campo,tipo from maestra where tabla='" & tabla & "' and Clave=1" 
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(strsql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(strsql,3,3)
	end if  

  ClaveTabla =rsResul("Campo")
  Tipo=rsResul("Tipo")
  rsResul.cerrar  
end function 
    
'VisibleSel devuelve si el CAMPO de la TABLA es visible en la selecci�n de los mantenimientos
function VisibleSel (Tabla, Campo)
  
  
  sql="select VisibleSel from maestra where tabla='" & tabla & "' and Campo='" & Campo & "'"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  VisibleSel =rsResul("VisibleSel")
  rsResul.cerrar
  
end function 

'HayCampoFichero true si en la tabla hay un campo de tipo FICHERO (x)
'Por ahora permitimos seis ficheros como m�ximo. Quedar�a pendiente una revisi�n para permitir infinitos.
function HayCampoFichero (Tabla, NombreCampoFicheroA, NombreCampoFicheroB, NombreCampoFicheroC, NombreCampoFicheroD, NombreCampoFicheroE, NombreCampoFicheroF) 
  
  dim sql
  sql="select Campo from maestra where tabla='" & tabla & "' and Tipo='XA' order by orden"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  if not rsResul.eof then
    HayCampoFichero = true
    num=1
    NombreCampoFicheroA=rsResul("Campo")
    rsResul.movenext
    if not rsResul.eof then
      num=2
      NombreCampoFicheroB=rsResul("Campo")
      rsResul.movenext
      if not rsResul.eof then
        num=3
        NombreCampoFicheroC=rsResul("Campo")
        rsResul.movenext
        if not rsResul.eof then
          num=4
          NombreCampoFicheroD=rsResul("Campo")
          rsResul.movenext
          if not rsResul.eof then
            num=5
            NombreCampoFicheroE=rsResul("Campo")
            rsResul.movenext
            if not rsResul.eof then
              num=6
              NombreCampoFicheroF=rsResul("Campo")
              rsResul.movenext
            end if
          end if
        end if
      end if
    end if
    rsResul.cerrar
	set rsResul = nothing
  else
    HayCampoFichero = false
  end if
end function

'Funcion que nos devuelve si hay relaciones 1 - N con la tabla, y nos dice que tablas est�n relacionadas,
'por qu� campos se relacionan y cuantas son
function hayRelaciones1N(Tabla,byref tablaRelaciones,byref tablaCamposRelaciones, byref numRelaciones)

  dim sql
  dim strTablas 'String donde vamos a almacenar, separados por #'s las tablas. Luego creamos la tabla
  dim strCampos 'String donde vamos a almacenar, separados por #'s los campos claves extranjeras de las tablas. Luego creamos la tabla
  strTablas = "" 
  strCampos = ""
  sql="SELECT Tabla,Campo FROM maestra WHERE tablaExt='" & tabla & "' AND ClaveExt=1 ORDER BY orden"
	if trim(request("conex"))<>"" then  
	  Set rsResul = connMaestra.AbrirRecordset(sql,3,3)
	else
	  Set rsResul = connCRM.AbrirRecordset(sql,3,3)
	end if  

  if not rsResul.eof then
    hayRelaciones1N = true
    while not rsResul.eof
		strTablas = strTablas & rsResul("Tabla")
		strCampos = strCampos & rsResul("Campo")
		rsResul.moveNext()
		if not rsResul.eof then 
			strTablas = strTablas & "#"
			strCampos = strCampos & "#"
		end if
    wend
  else
	hayRelaciones1N = false
  end if
  tablaRelaciones = split(strTablas,"#")
  tablaCampos = split(strCampos,"#")
  numRelaciones = ubound(tablaRelaciones) + 1
  rsResul.cerrar()
    
end function

'Presenta una hora al modo hh:mm
function VistaHora(hora)
  if not isdate(hora) then exit function
  if isnull(hora) then
     VistaHora=Hora
  else
     VistaHora=hour(hora) & ":" & minute(hora)
  end if
end function

'Presenta una fecha al modo d/m/a�o
function VistaFecha(fecha)
  if not isdate(fecha) then exit function
  if isnull(fecha) then
     VistaFecha=fecha
  else
     VistaFecha=day(fecha) & "/" & month(fecha)& "/" & year(fecha)
  end if
end function

'Formatea una fecha al modo mes/dia/a�o (para UPDATES E INSERTS)
function FormatearFecha(fecha)
	if not isdate(fecha) then exit function
	if isnull(fecha) then
		FormatearFecha=fecha
	else
		diaF = day(fecha)
		if diaF < 10 then diaF = "0" & diaF
		mesF = month(fecha)
		if mesF <10 then mesF = "0" & mesF
		anoF = year(fecha)

		FormatearFecha = anoF & mesF & diaF
		'FormatearFecha = diaF & "/" & mesF & "/" & anoF 
	end if
end function

'Formatea una hora al modo hora:minutos (para UPDATES E INSERTS)
function FormatearHora(hora)
  if not isdate(hora) then exit function
  if isnull(hora) then
     FormatearHora=Hora
  else
     FormatearHora=hour(hora) & ":" & minute(hora)
  end if
end function

function NumeroDeMNS(Tabla)
	sql="select COUNT(MAMN_TABLA1) from MAESTRA_MN where MAMN_TABLA1='" & Tabla & "'"
	if trim(request("conex"))<>"" then  
		Set rsResulMN = connMaestra.AbrirRecordset(sql,0,1)
	else
		Set rsResulMN = connCRM.AbrirRecordset(sql,0,1)
	end if	

	NumeroDeMNS=rsResulMN(0)
	rsResulMN.cerrar
end function

function obtenerRuta(codigo)
	dim salida
	dim salir
	dim cod
	dim sql
	
	salida = ""
	salir = false
	cod = codigo
	sql = "SELECT codM,nombreM,padreM FROM Menus"
	if trim(request("conex"))<>"" then  
		Set rsMenus = connMaestra.AbrirRecordset(sql,3,3)
	else
		Set rsMenus = connCRM.AbrirRecordset(sql,3,3)
	end if	

	
	if rsMenus.eof then
		salir = ">> no existe ese nodo"
	else
		while cod<>-1
			rsMenus.filter = "CodM=" & cod
			if not rsMenus.eof then
				salida = ">>" & rsMenus("nombreM") & salida
				cod = rsMenus("padreM")
			else
				salida = ">>No existe el nodo, o es un nodo aislado. Pueden producirse errores."
				cod = -1 'Para salir
			end if
			rsMenus.filter = 0
		wend
	end if
	rsMenus.cerrar
	obtenerRuta = salida
end function

function numIdiomasWeb()
	sql="SELECT count(*) as total FROM IDIOMAS ORDER BY IDI_ORDEN ASC "
	if trim(request("conex"))<>"" then  
		Set rsNumIdi = connMaestra.abrirRecordSet(sql,3,3)
	else
		Set rsNumIdi = connCRM.abrirRecordSet(sql,3,3)
	end if
	
	totalIdi = rsNumIdi("total")
	rsNumIdi.cerrar
	set rsNumIdi = nothing
	numIdiomasWeb = totalIdi
end function

function idiomasWeb()
	idiomasWeb = ""
	nodosCodigos = ""
	nodosNombres = ""
	if trim(request("conex"))<>"" then  
		Set rsIdi = connMaestra.abrirRecordSet("SELECT * FROM menus WHERE nivelM=-1",3,3)
	else
		Set rsIdi = connCRM.abrirRecordSet("SELECT * FROM menus WHERE nivelM=-1",3,3)
	end if	

	while not rsIdi.eof
		nodosCodigos = nodosCodigos & rsIdi("codM") & ","
		nodosNombres = nodosNombres & rsIdi("nombreM") & ","
		rsIdi.movenext
	wend
	'para quitarles las comas finales
	if len(nodosCodigos)>0 then nodosCodigos = left(nodosCodigos,len(nodosCodigos)-1)
	if len(nodosNombres)>0 then nodosNombres = left(nodosNombres,len(nodosNombres)-1)
	rsIdi.cerrar
	set rsIdi = nothing
	idiomasWeb = nodosCodigos & ";" & nodosNombres
end function

function sustitucion_vocales(palabra)    
'Esta funcion se encarga de pasar la palabra que se pase a todas la vocales posilbles.
    InitialString = palabra    
    Set RegularExpressionObject = New RegExp
    
    for i=0 to 4
        With RegularExpressionObject
            .IgnoreCase = True
            .Global = True
            select case i
                case 0: .Pattern = "[a����]"                
                case 1: .Pattern = "[e����]"
                case 2: .Pattern = "[i����]"
                case 3: .Pattern = "[o����]"
                case 4: .Pattern = "[u����]"
            end select         
        End With        
            select case i
                case 0: ReplacedString = RegularExpressionObject.Replace(InitialString, "[a����]")                 
                case 1: ReplacedString = RegularExpressionObject.Replace(InitialString, "[e����]") 
                case 2: ReplacedString = RegularExpressionObject.Replace(InitialString, "[i����]") 
                case 3: ReplacedString = RegularExpressionObject.Replace(InitialString, "[o����]") 
                case 4: ReplacedString = RegularExpressionObject.Replace(InitialString, "[u����]") 
            end select      
        InitialString  = ReplacedString    
    next    
    
    Set RegularExpressionObject = nothing
    sustitucion_vocales=ReplacedString    
end function

function sustitucion_regular(palabra)    
    if not isnull(palabra) then
        strAux=""
        'Elimino los caracteres que no me interesan
        palabra=replace(palabra," ","")
        palabra=replace(palabra,",","")
        palabra=replace(palabra,".","")
        palabra=replace(palabra,"/","")
        palabra=replace(palabra,"_","")
        palabra=replace(palabra,"-","")
        palabra=replace(palabra,"|","")
        palabra=replace(palabra,";","")
        palabra=replace(palabra,":","")
        palabra=replace(palabra,"&","")
        palabra=replace(palabra,"(","")
        palabra=replace(palabra,")","")
        palabra=replace(palabra,"[","")
        palabra=replace(palabra,"]","")
        for intI=1 to len(palabra)
            strAux=strAux & "%" & mid(palabra,intI,1)
        next
        'Detras s�lo permito un caracter
        sustitucion_regular=strAux & "%"
    else
        sustitucion_regular=""
    end if
end function

%>