
<%

consultaAuxuliar=session(denominacion & tabla & "_consulta")

if trim(cstr(session("codigoUsuario")))="49" then
'fromAnterior="("
end if

'Bloque de filtros de acciones:

'if (trim(request.Form("cboComercial"))<>"" and trim(request.Form("cboComercial"))<>"0") then
'	consultaWhere=consultaWhere & " And CLIE_COMERCIAL=" & request.Form("cboComercial")
'elseif (trim(comercialInicial)<>"" and trim(comercialInicial)<>"0") then
'	consultaWhere=consultaWhere & " And CLIE_COMERCIAL=" & comercialInicial
'end if

	datosSelect=replace(datosSelect,",CLIE_NOMBRE",", CLIE_NOMBRE + ' ' + (Case When CLIE_MARCACOMERCIAL is not null and CLIE_MARCACOMERCIAL<>'' Then ' (' + CLIE_MARCACOMERCIAL + ') ' Else '' End ) as CLIE_NOMBRE ")

estandarAccionesInicio=" And CLIE_CODIGO in ( Select ACCI_SUCLIENTE From ACCIONES_CLIENTE Where (1=1) "
aplicadoFiltroEspecial=false
filtroEspecialAcciones=""

if trim(request("cboTerminado"))="N" Or trim(terminadoIncial)="N" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_TERMINADA=0 " 
	aplicadoFiltroEspecial=True
elseif trim(request("cboTerminado"))="S" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_TERMINADA=1 " 
	aplicadoFiltroEspecial=True
end if


if (trim(request("cboComercial"))<>"" and trim(request("cboComercial"))<>"0") then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ( ACCI_SUCOMERCIAL=" & request("cboComercial") & " Or  ACCI_SUCOMERCIAL2=" & request("cboComercial") & " Or  ACCI_SUCOMERCIAL3=" & request("cboComercial") & " Or  ACCI_SUCOMERCIAL4=" & request("cboComercial") & " ) "
	aplicadoFiltroEspecial=True
elseif (trim(comercialInicial)<>"" and trim(comercialInicial)<>"0") then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ( ACCI_SUCOMERCIAL=" & comercialInicial & " Or  ACCI_SUCOMERCIAL2=" & comercialInicial & " Or  ACCI_SUCOMERCIAL3=" & comercialInicial & " Or  ACCI_SUCOMERCIAL4=" & comercialInicial & " ) "
	aplicadoFiltroEspecial=True	
end if

'response.Write("@"&cfiltroEspecialAcciones&"@")

if trim(request("txtFDesde"))<>"" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And convert(datetime,convert(varchar,ACCI_FECHA," & obtenerFormatoFechaSql() & ")," & obtenerFormatoFechaSql() & ") >= convert(varchar,'" & trim(request("txtFDesde")) & "'," & obtenerFormatoFechaSql() & ")  "
	aplicadoFiltroEspecial=True
end if

if trim(request("txtFHasta"))<>"" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And  convert(datetime,convert(varchar,ACCI_FECHA," & obtenerFormatoFechaSql() & ")," & obtenerFormatoFechaSql() & ") <= convert(varchar,'" & trim(request("txtFHasta")) & "'," & obtenerFormatoFechaSql() & ")  "
	aplicadoFiltroEspecial=True
end if


if trim(request("cboTipoAccion"))<>"" and trim(request("cboTipoAccion"))<>"0" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_SUTIPO=" & trim(request("cboTipoAccion")) 
	aplicadoFiltroEspecial=True
end if

if trim(request("cboCampanna"))<>"" and trim(request("cboCampanna"))<>"0" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_SUCAMPANA=" & trim(request("cboCampanna")) 
	aplicadoFiltroEspecial=True
end if

if trim(request("txtComentario"))<>""  then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ( ACCI_COMENTARIO like '%" & sustitucion_vocales(request("txtComentario")) & "%' ) "
    aplicadoFiltroEspecial=True
end if


if aplicadoFiltroEspecial then
	consultaWhere=consultaWhere & estandarAccionesInicio & filtroEspecialAcciones & " ) "
end if


if trim(request("chkSinAcciones"))="1"  then
'	consultaWhere=consultaWhere & "  And ( CLIE_CODIGO not in ( Select subAcc.ACCI_SUCLIENTE From ACCIONES_CLIENTE  subAcc Where subAcc.ACCI_SUCLIENTE=CLIE_CODIGO ) ) "
	consultaWhere=consultaWhere & "  And ( not ( Select count(subAcc.ACCI_CODIGO) From ACCIONES_CLIENTE  subAcc Where subAcc.ACCI_SUCLIENTE=CLIE_CODIGO ) > 0 ) "
end if

enlaceJoin="left"
if trim(request("chkContactosDuplicados"))="1"  then
'	consultaWhere=consultaWhere & " And (CLIE_CODIGO in ( select C2.CON_SUCLIENTE as conNombre From CONTACTOS C2 Where C2.CON_SUCLIENTE=CON_SUCLIENTE  group by C2.CON_SUCLIENTE,C2.CON_NOMBRE & ' ' & C2.CON_APELLIDO1 having count(C2.CON_NOMBRE & ' ' & C2.CON_APELLIDO1) >1 ) ) "
	enlaceJoin="inner"
end if

datosSelect=datosSelect & ",TDev.contDuplicados as contDuplicados "
fromAnterior=fromAnterior & "("
fromPosterior=fromPosterior & enlaceJoin & " join ( "
fromPosterior=fromPosterior & " Select Distinct C2.CON_SUCLIENTE as conCliente,count(C2.CON_SUCLIENTE) as contDuplicados From CONTACTOS C2  Group By C2.CON_SUCLIENTE,C2.CON_NOMBRE + ' ' + C2.CON_APELLIDO1 Having count(C2.CON_NOMBRE + ' ' + C2.CON_APELLIDO1) > 1 "
fromPosterior=fromPosterior & " Union "
fromPosterior=fromPosterior & " Select Distinct C3.CON_SUCLIENTE as conCliente,count(C3.CON_SUCLIENTE) as contDuplicados From CONTACTOS C3  Group By C3.CON_SUCLIENTE,C3.CON_EMAIL Having count(C3.CON_EMAIL) > 1 "
fromPosterior=fromPosterior & ") as TDev on ( clientes.CLIE_CODIGO=TDev.conCliente )) "


'A partir de aqui filtros de clientes:

if trim(request("cboEsCliente"))="N" Or trim(esClienteActualInicial)="N" then
	consultaWhere=consultaWhere & "  And CLIE_CLIENTEACTUAL=0 "
elseif trim(request("cboEsCliente"))="S" then
	consultaWhere=consultaWhere & "  And CLIE_CLIENTEACTUAL=1 "
end if

if trim(request("txtNombre"))<>""  then
	consultaWhere=consultaWhere & " And ( CLIE_NOMBRE like '%" & sustitucion_vocales(request("txtNombre")) & "%' Or CLIE_MARCACOMERCIAL like '%" & sustitucion_vocales(request("txtNombre")) & "%' Or ( CLIE_NOMBRE + ' | ' + CLIE_MARCACOMERCIAL like '%" & sustitucion_vocales(request("txtNombre")) & "%'  ) ) "
end if

if trim(request("txtCodigosCli"))<>""  then
	consultaWhere=consultaWhere & " And ( CLIE_CODIGO like '%" & sustitucion_vocales(request("txtCodigosCli")) & "%' Or CLIE_CODIGOSPYRO like '%" & sustitucion_vocales(request("txtCodigosCli")) & "%' Or CLIE_CODIGOFACTURAPLUS like '%" & sustitucion_vocales(request("txtCodigosCli")) & "%' ) "
end if

if trim(request("txtCIF"))<>""  then
	consultaWhere=consultaWhere & " And ( CLIE_CIF like '%" & request("txtCIF") & "%' ) "
end if


if trim(request("txtLocalidad"))<>"" then
	consultaWhere=consultaWhere & " And ( CLIE_LOCALIDAD like '%" & sustitucion_vocales(request("txtLocalidad")) & "%' ) "
end if

if trim(request("cboProvincia"))<>""  and trim(request("cboProvincia"))<>"0" then
	consultaWhere=consultaWhere & " And ( CLIE_PROVINCIA=" & request("cboProvincia") & ") "
end if

if trim(request("txtCP"))<>"" then
	consultaWhere=consultaWhere & " And ( CLIE_CP like '%" & request("txtCP") & "%' ) "
end if

if trim(request("txtEMail"))<>"" then
	consultaWhere=consultaWhere & " And ( CLIE_EMAIL like '%" & request("txtEMail") & "%' ) "
end if

if trim(request("txtNtrab"))<>"" and isnumeric(trim(request("txtNtrab"))) then
	consultaWhere=consultaWhere & " And ( CLIE_NUMTRABAJADORES" & request("cboNTrab") & trim(request("txtNtrab")) & " ) "
end if

if trim(request("cboDistribuid"))<>"" and trim(request("cboDistribuid"))<>"0" then
	if trim(request("cboDistribuid"))="S" then
		consultaWhere=consultaWhere & " And ( CLIE_DISTRIBUIDOR=1 ) "	
	elseif trim(request("cboDistribuid"))="N" then
		consultaWhere=consultaWhere & " And ( CLIE_DISTRIBUIDOR=0 ) "
	end if	
end if

if trim(request("cboEsProveedor"))<>"" and trim(request("cboEsProveedor"))<>"0" then
	if trim(request("cboEsProveedor"))="S" then
		consultaWhere=consultaWhere & " And ( CLIE_PROVEEDOR=0 ) "	
	elseif trim(request("cboEsProveedor"))="N" then
		consultaWhere=consultaWhere & " And ( CLIE_PROVEEDOR=0 ) "
	end if	
end if

if trim(request("txtActividad"))<>"" then
	consultaWhere=consultaWhere & " And ( CLIE_ACTIVIDAD like '%" & sustitucion_vocales(request("txtActividad")) & "%' ) "
end if

'if trim(request("txtCategoria"))<>"" then
'	consultaWhere=consultaWhere & " And ( (CLIE_Categoria1 in (Select CATEG1.CATEG_CODIGO From TIPOS_CATEGORIAS CATEG1 Where CATEG1.CATEG_DESCRIPCION like '%" & request("txtCategoria") & "%'))  Or  (CLIE_Categoria2 in (Select CATEG2.CATEG_CODIGO From TIPOS_CATEGORIAS CATEG2 Where CATEG2.CATEG_DESCRIPCION like '%" & request("txtCategoria") & "%')) Or (CLIE_Categoria3 in (Select CATEG3.CATEG_CODIGO From TIPOS_CATEGORIAS CATEG3 Where CATEG3.CATEG_DESCRIPCION like '%" & request("txtCategoria") & "%'))  ) "
'end if

if trim(request("cboCategoria"))<>""  and trim(request("cboCategoria"))<>"0" then
	consultaWhere=consultaWhere & " And ( (CLIE_CATEGORIA1=" & request("cboCategoria") & ") Or (CLIE_CATEGORIA2=" & request("cboCategoria") & ") Or (CLIE_CATEGORIA3=" & request("cboCategoria") & ")  ) "
end if

if	trim(request("cboComercialCli"))="-1" then
	consultaWhere=consultaWhere & " And ( CLIE_COMERCIAL=0 or CLIE_COMERCIAL=-1  Or CLIE_COMERCIAL is null ) "
elseif trim(request("cboComercialCli"))<>""  and trim(request("cboComercialCli"))<>"0" then
	consultaWhere=consultaWhere & " And ( CLIE_COMERCIAL=" & request("cboComercialCli") & ") "
end if

if trim(request("cboDistribuidor"))<>""  and trim(request("cboDistribuidor"))<>"0" then
	consultaWhere=consultaWhere & " And ( CLIE_SUDISTRIBUIDOR=" & request("cboDistribuidor") & ") "
end if

if trim(request("txtTelefonoFax"))<>"" then
	consultaWhere=consultaWhere & " And ( CLIE_TELEFONO like '%" & request("txtTelefonoFax") & "%' Or CLIE_FAX like '%" & request("txtTelefonoFax") & "%' ) "
end if

if trim(request("cboCalidadRegistro"))<>""  and trim(request("cboCalidadRegistro"))<>"0" then
	consultaWhere=consultaWhere & " And ( CLIE_CALIDADREGISTRO=" & request("cboCalidadRegistro") & ") "
end if

'A partir de aqui filtros de contactos:
estandarContactoInicio=" And CLIE_CODIGO in ( Select CON_SUCLIENTE From CONTACTOS Where (1=1) "
aplicadoFiltroEspecialContactos=false
filtroEspecialContactos=""

if trim(request("txtContacto"))<>""  then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_NOMBRE like '%" & sustitucion_vocales(trim(request("txtContacto")))  & "%' Or CON_APELLIDO1 like '%" & sustitucion_vocales(trim(request("txtContacto")))  & "%'  ) "
	aplicadoFiltroEspecialContactos=True
end if

if trim(request("txtTelefonoContacto"))<>""  then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_TELEFONO like '%" & trim(request("txtTelefonoContacto"))  & "%' Or CON_MOVIL like '%" & trim(request("txtTelefonoContacto"))  & "%' Or CON_FAX like '%" & trim(request("txtTelefonoContacto"))  & "%'  ) "
	aplicadoFiltroEspecialContactos=True
end if

if trim(request("txtEMailContacto"))<>""  then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_EMAIL like '%" & trim(request("txtEMailContacto"))  & "%'  ) "
	aplicadoFiltroEspecialContactos=True
end if

if trim(request("txtDireccionContacto"))<>""  then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_DIRECCION like '%" & sustitucion_vocales(trim(request("txtDireccionContacto")))  & "%'  ) "
	aplicadoFiltroEspecialContactos=True
end if

if trim(request("cboEsContactoComercial"))="N" then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_CONTACTOCOMERCIAL=0  ) "
	aplicadoFiltroEspecialContactos=True
elseif trim(request("cboEsContactoComercial"))="S" then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_CONTACTOCOMERCIAL=1  ) "
	aplicadoFiltroEspecialContactos=True
end if

if trim(request("cboEsContactoProduccion"))="N" then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_CONTACTOPRODUCCION=0  ) "
	aplicadoFiltroEspecialContactos=True
elseif trim(request("cboEsContactoProduccion"))="S" then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_CONTACTOPRODUCCION=1  ) "
	aplicadoFiltroEspecialContactos=True
end if

if trim(request("chkContactoMailRepetido"))="1"  then
	filtroEspecialContactos=filtroEspecialContactos & " And ( CON_EMAIL LIKE '%@%@%'  ) "
	aplicadoFiltroEspecialContactos=True	
end if

if aplicadoFiltroEspecialContactos then
	consultaWhere=consultaWhere & estandarContactoInicio & filtroEspecialContactos & " ) "
end if



'A partir de aqui filtros de Productos para DM:
if trim(request("txtProductoDM"))<>""  then
	consultaWhere=consultaWhere & " And CLIE_CODIGO in ( Select DOM_SUCLIENTE From ((( ISP_PRODUCTOS ISP_P Inner join ISP_DOMINIO_PRODUCTO ISP_DP on (ISP_P.PRO_CODIGO=ISP_DP.DP_SUPRODUCTO)) " _
																				& " Inner join ISP_DOMINIOS ISP_D on (ISP_DP.DP_SUDOMINIO=ISP_D.DOM_CODIGO)) " _
																					& " Inner join CLIENTES ISP_C on (ISP_D.DOM_SUCLIENTE=ISP_C.CLIE_CODIGO)) " _				
																		& " Where  ( ISP_P.PRO_DESCRIPCION like '%" & sustitucion_vocales(trim(request("txtProductoDM")))  & "%' ) ) "

end if

'A partir de aqui filtros de Modulo para DM:
if trim(request("txtModuloDM"))<>""  then
	consultaWhere=consultaWhere & " And CLIE_CODIGO in ( Select DOM_SUCLIENTE From ((( ISP_MODULOS ISP_M Inner join ISP_DOMINIO_MODULOS ISP_DM on (ISP_M.MOD_CODIGO=ISP_DM.DM_SUMODULO)) " _
																				& " Inner join ISP_DOMINIOS ISP_D on (ISP_DM.DM_SUDOMINIO=ISP_D.DOM_CODIGO)) " _
																					& " Inner join CLIENTES ISP_C on (ISP_D.DOM_SUCLIENTE=ISP_C.CLIE_CODIGO)) " _				
																		& " Where  ( ISP_M.MOD_DESCRIPCION like '%" & sustitucion_vocales(trim(request("txtModuloDM")))  & "%' ) ) "

end if

'estandarProductoDMInicio=" And CLIE_CODIGO in ( Select DOM_SUCLIENTE From ISP_PRODUCTOS ISP_P Inner join ISP_DOMINIO_PRODUCTO ISP_DP on (ISP_P.PRO_CODIGO=ISP_DP.DP_SUPRODUCTO) " _
'				" Inner join ISP_DOMINIOS ISP_D on (ISP_DP.SP_SUDOMINIO=ISP_D.DOM_CODIGO) " _
'				" Inner join CLIENTES ISP_C on (ISP_D.DOM_SUCLIENTE=ISP_C.CLIE_CODIGO) " _				
'				" Where (1=1) "
'aplicadoFiltroEspecialProductosDM=false
'filtroEspecialProductosDM=""
'if trim(request("txtProductoDM"))<>""  then
'	filtroEspecialProductosDM=filtroEspecialProductosDM & " And ( PRO_DESCRIPCION like '%" & trim(request("txtContacto"))  & "%'  ) "
'	aplicadoFiltroEspecialProductosDM=True
'end if
'if aplicadoFiltroEspecialProductosDM then
'	consultaWhere=consultaWhere & estandarProductoDMInicio & filtroEspecialProductosDM & " ) "
'end if


%>