
<%

consultaAuxuliar=session(denominacion & tabla & "_consulta")



estandarAccionesInicio=" And CON_SUCLIENTE in ( Select ACCI_SUCLIENTE From ACCIONES_CLIENTE Where (1=1) "
aplicadoFiltroEspecial=false
filtroEspecialAcciones=""

if trim(request("cboTerminado"))="N" Or trim(terminadoIncial)="N" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_TERMINADA=0 " 
	aplicadoFiltroEspecial=True
elseif trim(request("cboTerminado"))="S" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_TERMINADA=1 " 
	aplicadoFiltroEspecial=True
end if


if (trim(request("cboComercial"))<>"" and trim(request("cboComercial"))<>"0") then
	filtroEspecialAcciones=filtroEspecialAcciones & " And (ACCI_SUCOMERCIAL=" & request("cboComercial") & " Or ACCI_SUCOMERCIAL2=" & request("cboComercial") & " Or ACCI_SUCOMERCIAL3=" & request("cboComercial") & " Or ACCI_SUCOMERCIAL4=" & request("cboComercial") & " ) "
	aplicadoFiltroEspecial=True
elseif (trim(comercialInicial)<>"" and trim(comercialInicial)<>"0") then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ( ACCI_SUCOMERCIAL=" & comercialInicial & " Or  ACCI_SUCOMERCIAL2=" & comercialInicial & " Or  ACCI_SUCOMERCIAL3=" & comercialInicial & " Or  ACCI_SUCOMERCIAL4=" & comercialInicial & " ) "
	aplicadoFiltroEspecial=True	
end if

'response.Write("@"&cfiltroEspecialAcciones&"@")

if trim(request("txtFDesde"))<>"" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And  convert(datetime,convert(varchar,ACCI_FECHA," & obtenerFormatoFechaSql() & ")," & obtenerFormatoFechaSql() & ") >= convert(varchar,'" & trim(request("txtFDesde")) & "'," & obtenerFormatoFechaSql() & ")  "
	aplicadoFiltroEspecial=True
end if

if trim(request("txtFHasta"))<>"" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And  convert(datetime,convert(varchar,ACCI_FECHA," & obtenerFormatoFechaSql() & ")," & obtenerFormatoFechaSql() & ") <= convert(varchar,'" & trim(request("txtFHasta")) & "'," & obtenerFormatoFechaSql() & ")  "
	aplicadoFiltroEspecial=True
end if


if trim(request("cboTipoAccion"))<>"" and trim(request("cboTipoAccion"))<>"0" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_SUTIPO=" & trim(request("cboTipoAccion")) 
	aplicadoFiltroEspecial=True
end if

if trim(request("cboCampanna"))<>"" and trim(request("cboCampanna"))<>"0" then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ACCI_SUCAMPANA=" & trim(request("cboCampanna")) 
	aplicadoFiltroEspecial=True
end if

if trim(request("txtComentario"))<>""  then
	filtroEspecialAcciones=filtroEspecialAcciones & " And ( ACCI_COMENTARIO like '%" & sustitucion_vocales(request("txtComentario")) & "%' ) "
    aplicadoFiltroEspecial=True
end if


if aplicadoFiltroEspecial then
	consultaWhere=consultaWhere & estandarAccionesInicio & filtroEspecialAcciones & " ) "
end if


if trim(request("chkSinAcciones"))="1"  then
	consultaWhere=consultaWhere & "  And ( CLIE_CODIGO not in ( Select subAcc.ACCI_SUCLIENTE From ACCIONES_CLIENTE  subAcc Where subAcc.ACCI_SUCLIENTE=CLIE_CODIGO ) ) "
end if

enlaceJoin="left"
if trim(request("chkContactosDuplicados"))="1"  then
'	consultaWhere=consultaWhere & " And (CON_SUCLIENTE in ( select C2.CON_SUCLIENTE as conNombre From CONTACTOS C2 Where C2.CON_SUCLIENTE=CON_SUCLIENTE  group by C2.CON_SUCLIENTE,C2.CON_NOMBRE & ' ' & C2.CON_APELLIDO1 having count(C2.CON_NOMBRE & ' ' & C2.CON_APELLIDOS) >1 ) ) "
	enlaceJoin="inner"
end if

datosSelect=datosSelect & ",TDev.contDuplicados as contDuplicados "
fromAnterior=fromAnterior & "("
fromPosterior=fromPosterior & enlaceJoin & " join ( "
fromPosterior=fromPosterior & " Select Distinct C2.CON_SUCLIENTE as conCliente,count(C2.CON_SUCLIENTE) as contDuplicados From CONTACTOS C2  Group By C2.CON_SUCLIENTE,C2.CON_NOMBRE + ' ' + C2.CON_APELLIDO1 Having count(C2.CON_NOMBRE + ' ' + C2.CON_APELLIDO1) > 1 "
fromPosterior=fromPosterior & " Union "
fromPosterior=fromPosterior & " Select Distinct C3.CON_SUCLIENTE as conCliente,count(C3.CON_SUCLIENTE) as contDuplicados From CONTACTOS C3  Group By C3.CON_SUCLIENTE,C3.CON_EMAIL Having count(C3.CON_EMAIL) > 1 "
fromPosterior=fromPosterior & ") as TDev on ( contactos.CON_SUCLIENTE=TDev.conCliente )) "


''''''''''Filtros de clientes

estandarClientesInicio=" And CON_SUCLIENTE in ( Select CLIE_CODIGO From Clientes Where (1=1) "
aplicadoFiltroEspecialClientes=false
filtroEspecialClientes=""

if trim(request("cboEsCliente"))="N" Or trim(esClienteActualInicial)="N" then
	filtroEspecialClientes=filtroEspecialClientes & "  And CLIE_CLIENTEACTUAL=0 "
	aplicadoFiltroEspecialClientes=True
elseif trim(request("cboEsCliente"))="S" then
	filtroEspecialClientes=filtroEspecialClientes & "  And CLIE_CLIENTEACTUAL=1 "
	aplicadoFiltroEspecialClientes=True	
end if

if trim(request("txtNombre"))<>""  then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_NOMBRE like '%" & sustitucion_vocales(request("txtNombre")) & "%' ) "
	aplicadoFiltroEspecialClientes=True	
end if

if trim(request("txtCodigosCli"))<>""  then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_CODIGO like '%" & sustitucion_vocales(request("txtCodigosCli")) & "%' Or CLIE_CODIGOSPYRO like '%" & sustitucion_vocales(request("txtCodigosCli")) & "%' Or  CLIE_CODIGOFACTURAPLUS like '%" & sustitucion_vocales(request("txtCodigosCli")) & "%' ) "
	aplicadoFiltroEspecialClientes=True	
end if

if trim(request("txtCIF"))<>""  then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_CIF like '%" & request("txtCIF") & "%' ) "
	aplicadoFiltroEspecialClientes=True	
end if


if trim(request("txtLocalidad"))<>"" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_LOCALIDAD like '%" & sustitucion_vocales(request("txtLocalidad")) & "%' ) "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("cboProvincia"))<>""  and trim(request("cboProvincia"))<>"0" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_PROVINCIA=" & request("cboProvincia") & ") "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("txtCP"))<>"" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_CP like '%" & request("txtCP") & "%' ) "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("txtEMail"))<>"" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_EMAIL like '%" & request("txtEMail") & "%' ) "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("txtNtrab"))<>"" and isnumeric(trim(request("txtNtrab"))) then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_NUMTRABAJADORES" & request("cboNTrab") & trim(request("txtNtrab")) & " ) "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("cboDistribuid"))<>"" and trim(request("cboDistribuid"))<>"0" then
	if trim(request("cboDistribuid"))="S" then
		filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_DISTRIBUIDOR=1 ) "	
		aplicadoFiltroEspecialClientes=True
	elseif trim(request("cboDistribuid"))="N" then
		filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_DISTRIBUIDOR=0 ) "
		aplicadoFiltroEspecialClientes=True
	end if	
end if

if trim(request("cboEsProveedor"))<>"" and trim(request("cboEsProveedor"))<>"0" then
	if trim(request("cboEsProveedor"))="S" then
		filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_PROVEEDOR=1 ) "	
		aplicadoFiltroEspecialClientes=True
	elseif trim(request("cboEsProveedor"))="N" then
		filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_PROVEEDOR=0 ) "
		aplicadoFiltroEspecialClientes=True
	end if	
end if

if trim(request("txtActividad"))<>"" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_ACTIVIDAD like '%" & sustitucion_vocales(request("txtActividad")) & "%' ) "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("cboCategoria"))<>""  and trim(request("cboCategoria"))<>"0" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( (CLIE_CATEGORIA1=" & request("cboCategoria") & ") Or (CLIE_CATEGORIA2=" & request("cboCategoria") & ") Or (CLIE_CATEGORIA3=" & request("cboCategoria") & ")  ) "
	aplicadoFiltroEspecialClientes=True

end if


if trim(request("cboComercialCli"))="-1" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_COMERCIAL=0  or CLIE_COMERCIAL=-1   Or CLIE_COMERCIAL is null ) "
	aplicadoFiltroEspecialClientes=True
'or   CLIE_COMERCIAL not in ( Select ANA_CODIGO from ANALISTAS   )	
elseif trim(request("cboComercialCli"))<>""  and trim(request("cboComercialCli"))<>"0" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_COMERCIAL=" & request("cboComercialCli") & ") "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("cboDistribuidor"))<>""  and trim(request("cboDistribuidor"))<>"0" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_SUDISTRIBUIDOR=" & request("cboDistribuidor") & ") "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("txtTelefonoFax"))<>"" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_TELEFONO like '%" & request("txtTelefonoFax") & "%' Or CLIE_FAX like '%" & request("txtTelefonoFax") & "%' ) "
	aplicadoFiltroEspecialClientes=True
end if

if trim(request("cboCalidadRegistro"))<>""  and trim(request("cboCalidadRegistro"))<>"0" then
	filtroEspecialClientes=filtroEspecialClientes & " And ( CLIE_CALIDADREGISTRO=" & request("cboCalidadRegistro") & ") "
	aplicadoFiltroEspecialClientes=True
end if


if aplicadoFiltroEspecialClientes then
	consultaWhere=consultaWhere & estandarClientesInicio & filtroEspecialClientes & " ) "
end if

''''''''''''Din de Filtros de clientes


'''Inicio bloque de contactos

if trim(request("txtContacto"))<>""  then
	consultaWhere=consultaWhere & " And ( CON_NOMBRE like '%" & sustitucion_vocales(trim(request("txtContacto")))  & "%' Or CON_APELLIDO1 like '%" & sustitucion_vocales(trim(request("txtContacto")))  & "%'  ) "
end if

if trim(request("txtTelefonoContacto"))<>""  then
	consultaWhere=consultaWhere & " And ( CON_TELEFONO like '%" & trim(request("txtTelefonoContacto"))  & "%' Or CON_MOVIL like '%" & trim(request("txtTelefonoContacto"))  & "%' Or CON_FAX like '%" & trim(request("txtTelefonoContacto"))  & "%'  ) "
end if

if trim(request("txtEMailContacto"))<>""  then
	consultaWhere=consultaWhere & " And ( CON_EMAIL like '%" & trim(request("txtEMailContacto"))  & "%'  ) "
end if

if trim(request("txtDireccionContacto"))<>""  then
	consultaWhere=consultaWhere & " And ( CON_DIRECCION like '%" & sustitucion_vocales(trim(request("txtDireccionContacto")))  & "%'  ) "
end if

if trim(request("cboEsContactoComercial"))="N" then
	consultaWhere=consultaWhere & " And ( CON_CONTACTOCOMERCIAL=0  ) "
elseif trim(request("cboEsContactoComercial"))="S" then
	consultaWhere=consultaWhere & " And ( CON_CONTACTOCOMERCIAL=1  ) "
end if

if trim(request("cboEsContactoProduccion"))="N" then
	consultaWhere=consultaWhere & " And ( CON_CONTACTOPRODUCCION=0  ) "
elseif trim(request("cboEsContactoProduccion"))="S" then
	consultaWhere=consultaWhere & " And ( CON_CONTACTOPRODUCCION=1  ) "
end if

if trim(request("chkContactoMailRepetido"))="1"  then
	consultaWhere=consultaWhere & "  And ( CON_EMAIL LIKE '%@%@%' ) "
end if


'''Fin bloque de contactos
'''''''''''''''''''''''''''''''''''''



'A partir de aqui filtros de Productos para DM:
if trim(request("txtProductoDM"))<>""  then
	consultaWhere=consultaWhere & " And CON_SUCLIENTE in ( Select DOM_SUCLIENTE From ((( ISP_PRODUCTOS ISP_P Inner join ISP_DOMINIO_PRODUCTO ISP_DP on (ISP_P.PRO_CODIGO=ISP_DP.DP_SUPRODUCTO)) " _
																				& " Inner join ISP_DOMINIOS ISP_D on (ISP_DP.DP_SUDOMINIO=ISP_D.DOM_CODIGO)) " _
																					& " Inner join CLIENTES ISP_C on (ISP_D.DOM_SUCLIENTE=ISP_C.CLIE_CODIGO)) " _				
																		& " Where  ( ISP_P.PRO_DESCRIPCION like '%" & sustitucion_vocales(trim(request("txtProductoDM")))  & "%' ) ) "

end if

'A partir de aqui filtros de Modulo para DM:
if trim(request("txtModuloDM"))<>""  then
	consultaWhere=consultaWhere & " And CON_SUCLIENTE in ( Select DOM_SUCLIENTE From ((( ISP_MODULOS ISP_M Inner join ISP_DOMINIO_MODULOS ISP_DM on (ISP_M.MOD_CODIGO=ISP_DM.DM_SUMODULO)) " _
																				& " Inner join ISP_DOMINIOS ISP_D on (ISP_DM.DM_SUDOMINIO=ISP_D.DOM_CODIGO)) " _
																					& " Inner join CLIENTES ISP_C on (ISP_D.DOM_SUCLIENTE=ISP_C.CLIE_CODIGO)) " _				
																		& " Where  ( ISP_M.MOD_DESCRIPCION like '%" & sustitucion_vocales(trim(request("txtModuloDM")))  & "%' ) ) "

end if

'estandarProductoDMInicio=" And CLIE_CODIGO in ( Select DOM_SUCLIENTE From ISP_PRODUCTOS ISP_P Inner join ISP_DOMINIO_PRODUCTO ISP_DP on (ISP_P.PRO_CODIGO=ISP_DP.DP_SUPRODUCTO) " _
'				" Inner join ISP_DOMINIOS ISP_D on (ISP_DP.SP_SUDOMINIO=ISP_D.DOM_CODIGO) " _
'				" Inner join CLIENTES ISP_C on (ISP_D.DOM_SUCLIENTE=ISP_C.CLIE_CODIGO) " _				
'				" Where (1=1) "
'aplicadoFiltroEspecialProductosDM=false
'filtroEspecialProductosDM=""
'if trim(request("txtProductoDM"))<>""  then
'	filtroEspecialProductosDM=filtroEspecialProductosDM & " And ( PRO_DESCRIPCION like '%" & trim(request("txtContacto"))  & "%'  ) "
'	aplicadoFiltroEspecialProductosDM=True
'end if
'if aplicadoFiltroEspecialProductosDM then
'	consultaWhere=consultaWhere & estandarProductoDMInicio & filtroEspecialProductosDM & " ) "
'end if


%>