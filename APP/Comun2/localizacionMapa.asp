<% Response.Buffer=false %>
<!-- #include virtual="/dmcrm/includes/permisos.asp"-->
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<html><head>
		<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
  
		<!-- #include virtual="/dmcrm/includes/jquery.asp"-->  
    
    
	</head>
	<body class="fondoPagIntranet" style="overflow-x:hidden;" > <!--onLoad="javascript:paginaCargada();"-->


		<% 
			tabla=request("tabla")
			campo=request("campo")
			latMapa=request("latMapa")
			longMapa=request("longMapa")
			zoomMapa=request("zoomMapa")
			codRegistro=request("codigo")
			
			tieneLocalizacion=false
			if trim(latMapa)<>"" And trim(longMapa)<>"" And trim(zoomMapa)<>"" then
				tieneLocalizacion=true
			end if
			
			if trim(latMapa)="" Or trim(longMapa)="" Or trim(zoomMapa)="" then
				sqlLocConf="Select Top 1 CONF_CODIGO,CONF_POIS_LATITUD,CONF_POIS_LONGITUD,CONF_POIS_ZOOM From CONFIGURACION  "
				Set rsLocConf = connCRM.AbrirRecordset(sqlLocConf,0,1)
				if not rsLocConf.eof then
					if trim(latMapa)="" then					
						if not isnull(rsLocConf("CONF_POIS_LATITUD")) then
							if trim(rsLocConf("CONF_POIS_LATITUD"))<>"" then
								latMapa=rsLocConf("CONF_POIS_LATITUD")
							end if
						end if
					end if					
					if trim(longMapa)="" then
						if not isnull(rsLocConf("CONF_POIS_LONGITUD")) then
							if trim(rsLocConf("CONF_POIS_LONGITUD"))<>"" then
								longMapa=rsLocConf("CONF_POIS_LONGITUD")
							end if
						end if
					end if					
					if trim(zoomMapa)="" then					
						if not isnull(rsLocConf("CONF_POIS_ZOOM")) then
							if trim(rsLocConf("CONF_POIS_ZOOM"))<>"" then
								zoomMapa=rsLocConf("CONF_POIS_ZOOM")
							end if
						end if
					end if					
				end if
				rsLocConf.cerrar()
				set rsLocConf=nothing
			end if

			tineDireccion=false
			tineLocalidad=false
			campoClave=""
			campoDireccion=""
			contenioDireccion=""
			campoLocalidad=""
			contenidoLocalidad=""
			tituloTabla=""
			sqlDatos=" Select 'G' as tipo,(CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as campo "
			sqlDatos=sqlDatos & " From maestra "
			sqlDatos=sqlDatos  & " where upper(tabla)='" & ucase(tabla) & "G' "
			sqlDatos=sqlDatos  & " union "
			sqlDatos=sqlDatos  & " Select 'D' as tipo, campo as campo "
			sqlDatos=sqlDatos  & " From maestra "
			sqlDatos=sqlDatos  & " where upper(tabla)='" & ucase(tabla) & "' And UPPER(Campo) like '%_DIRECCION%' "
			sqlDatos=sqlDatos  & " union "
			sqlDatos=sqlDatos  & " Select 'L' as tipo, campo as campo "
			sqlDatos=sqlDatos  & " From maestra "
			sqlDatos=sqlDatos  & " where upper(tabla)='" & ucase(tabla) & "' And (UPPER(Campo) like '%_LOCALIDAD%' or UPPER(Campo) like '%_CIUDAD%') "
			sqlDatos=sqlDatos  & " union "
			sqlDatos=sqlDatos  & " Select 'C' as tipo, campo as campo "
			sqlDatos=sqlDatos  & " From maestra "
			sqlDatos=sqlDatos  & " where upper(tabla)='" & ucase(tabla) & "' And Clave=1 "			
			
			Set rsDatos = connCRM.AbrirRecordset(sqlDatos,0,1)
			while not rsDatos.eof 
				Select Case trim(ucase(rsDatos("tipo")))
					Case "G":
						if trim(tituloTabla)="" then
							tituloTabla=rsDatos("campo")
						end if
					Case "D":
						if trim(campoDireccion)="" then
							campoDireccion=rsDatos("campo")
						end if					
					Case "L":										
						if trim(campoLocalidad)="" then
							campoLocalidad=rsDatos("campo")
						end if						
					Case "C":										
						if trim(campoClave)="" then
							campoClave=rsDatos("campo")
						end if												
				end Select
				
				rsDatos.movenext
			wend
			rsDatos.cerrar()
			set rsDatos=nothing	
			
			'response.Write(campoClave & "#" & tituloTabla & "#" & 	campoDireccion & "#" & campoLocalidad)
			'response.End()
			
			if trim(campoClave)<>"" And trim(codRegistro)<>"" And ( trim(campoDireccion)<>"" Or trim(campoLocalidad)<>"" ) then
				sqlDireccion=" Select " & campoClave & " as codigo"
				if trim(campoDireccion)<>"" then
					tineDireccion=true
					sqlDireccion=sqlDireccion & ", " & campoDireccion 
				end if
				if trim(campoLocalidad)<>"" then
					tineLocalidad=true
					sqlDireccion=sqlDireccion & ", " & campoLocalidad 
				end if			
				sqlDireccion=sqlDireccion & " From " & tabla & " Where " & campoClave & "=" & codRegistro	
				Set rsDireccion = connCRM.AbrirRecordset(sqlDireccion,0,1)
				if not rsDireccion.eof then
					if tineDireccion then
						contenioDireccion=rsDireccion(campoDireccion)
					end if
					
					if tineLocalidad then
						contenidoLocalidad=rsDireccion(campoLocalidad)
					end if					
					
					rsDireccion.movenext
				end if
				rsDireccion.cerrar()
				set rsDireccion=nothing	
				
			end if
			
			
			latMapaBBDD=""
			longMapaBBDD=""
			zoomMapaBBDD=""
			if trim(tabla)<>"" And trim(campoClave)<>"" And trim(codRegistro)<>"" And trim(campo)<>"" then
				locBBDD=""
				sqlLocBBDD=" Select " & campo & " as campoLoc From " & tabla & " Where " & campoClave & "=" & codRegistro
				Set rsLocBBDD = connCRM.AbrirRecordset(sqlLocBBDD,0,1)
				if not rsLocBBDD.eof then
					locBBDD=rsLocBBDD("campoLoc")
				end if
				rsLocBBDD.cerrar()
				set rsLocBBDD=nothing					
				
				if trim(locBBDD)<>"" then
					arrLocBBDD=split(locBBDD,"|")
					if ubound(arrLocBBDD)>=0 then
						latMapaBBDD=arrLocBBDD(0)
					end if
					if ubound(arrLocBBDD)>=1 then
						longMapaBBDD=arrLocBBDD(1)
					end if
					if ubound(arrLocBBDD)>=2 then
						zoomMapaBBDD=arrLocBBDD(2)
					end if										
				end if
				
			end if
			
		%>

                    <style type="text/css">
                        .bloqueResumen_Peq { width:23%!important; min-width:20px; height:150px!important; padding-bottom:20px!important; }
						.bloqueResumenTitulo { font-size:12px!important;}
						.boton { float:right; margin-top:5px; margin-right:5px;  }
						
						
						.filaDatos {float:left; clear:both; width:95%; margin:5px 0 0 10px; padding:0;}
						.colEtiq { width:20%; float:left; text-align:right;}
						.colDato { width:75%; float:left; margin-left:5px; }
						
						.separadorTitulo {width:100%; clear:both; padding:10px 0 0 0; margin:0 0 10px 0; border-top:solid 1px #ff6600; font-size:15px;}
						
						.editarMapaLoc {width:60%; height:380px; margin:0; float:right;  }
						
						.apaDescripLoc {float:left; width:38%; min-height:380px; padding-right:10px; border-right:dashed 1px #ff6600;}
						
						.valorLoc {font-weight:bold; }
						.literalLocActual { margin-left:25px; margin-top:5px;}
						.literalLocActualLargo { margin-left:60px;}	
						
						#valorLatitud {font-weight:bold; margin-top:5px;}
						#valorLongitud {font-weight:bold; margin-top:5px;}
						#valorZoom {font-weight:bold; margin-top:5px;}
						
						
						.tituloPosActual {margin:0; padding:5px 0 5px 0; width:100%; border-bottom:solid 3px #CDCDCD; font-size:14px; color:#555; font-weight:bold; background-color:#F7F7F7; text-transform:uppercase;}



						.botonDeshabil { background:#ff6600; color:#fff;  border:none; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; padding:3px 10px; float:right; margin-top:5px; margin-right:5px;}
						
						.capaDeshabil {filter: alpha(opacity=50);  opacity: .5;}
                    </style>

		<h1 ><span style="text-transform:capitalize;"><%=objIdiomaGeneral.getIdioma("localizacionMapa_Titulo1")%><%=" " & lcase(tituloTabla) & " "%></span><%=objIdiomaGeneral.getIdioma("localizacionMapa_Titulo2")%></h1>

              <div class="bloquePestanna" > 
			  
				  <div class="filaDatos" >
						<%'tieneLocalizacion=false%>
						<div class="apaDescripLoc" >
							- <%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto1") & " " & lcase(tituloTabla) & " " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto2")%><br />
							
							<%textoEstadoLoc=objIdiomaGeneral.getIdioma("localizacionMapa_Texto3")
							textoBotonAceptar=objIdiomaGeneral.getIdioma("localizacionMapa_Texto4")
							idBotonAceptar="btnGuardarLoc"
							if not tieneLocalizacion then
								textoEstadoLoc=objIdiomaGeneral.getIdioma("localizacionMapa_Texto5")
								textoBotonAceptar=objIdiomaGeneral.getIdioma("localizacionMapa_Texto6")
								idBotonAceptar="btnAceptarLoc"								
							end if%>
							<%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto7") & " " & textoEstadoLoc & " " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto8")%><br />
							<%="- " &  objIdiomaGeneral.getIdioma("localizacionMapa_Texto9")%><br />
							<%="- " &  objIdiomaGeneral.getIdioma("localizacionMapa_Texto10")%><br />
							<br />
							<%if not tieneLocalizacion then %>
								<%="- "%><strong><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto11")%></strong>.
							<%else%>
								<%="- "%><strong><u><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto12")%></u></strong>	
								<div class="separador" style="height:5px;">&nbsp;</div>
								<span class="literalLocActual"><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto13")%>:</span> <span class="valorLoc"><%=latMapaBBDD%></span>
								<div class="separador" style="height:5px;">&nbsp;</div>								
								<span class="literalLocActual"><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto14")%>:</span> <span class="valorLoc"><%=longMapaBBDD%></span>
								<div class="separador" style="height:5px;">&nbsp;</div>								
								<span class="literalLocActual"><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto15")%>:</span> <span class="valorLoc"><%=zoomMapaBBDD%></span>								
							<%end if %>							

							<div style="float:left; clear:both; width:100%; margin-top:20px; border-top:dashed 1px #ff6600;">&nbsp;</div>
							
                            
                            	<div style="float:left; clear:both; border:solid 1px #CDCDCD; width:95%;">
                                
										 <div  class="tituloPosActual">&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto16")%></div>
                                         
                               			<div class="separador" style="height:10px;">&nbsp;</div>
                                         
                                        <div class="filaDatos" >
                                            <div class="literalLocActual" style="float:left;" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto13")%>:&nbsp;</div>
                                            <input type="text" disabled="true" id="valorLatitud" name="valorLatitud" style="width:20%; float:left;"  class="cajaTextoModal" value="<%=latMapa%>">
                                            <div class="literalLocActual" style="float:left;" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto14")%>:&nbsp;</div>
                                            <input type="text"  disabled="true"  id="valorLongitud" name="valorLongitud" style="width:20%; float:left; "  class="cajaTextoModal" value="<%=longMapa%>">
                                            <div class="literalLocActual" style="float:left;" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto17")%>:&nbsp;</div>
                                            <input type="text"  disabled="true"  id="valorZoom" name="valorZoom" style="width:20%; float:left;"  class="cajaTextoModal" value="<%=zoomMapa%>">
                                        </div>
                                        <center>
                                            <div class="filaDatos" >
                                                    <input id="<%=idBotonAceptar%>" name="<%=idBotonAceptar%>" class="boton" type="button" value="<%=textoBotonAceptar%>" style=" height:25px; margin-right:40px;  margin-bottom:10px; " >
                                            </div>                                                                                                                                   
                                        </center>                                             

                                
                                </div>
                            
                            

						</div>


							<div id="editarMapa" class="editarMapaLoc" ></div>   <!--editarMapaGoogle-->
							<!--<input type="hidden"  id="valorLatitud" name="valorLatitud" value="<%=latMapa%>" />            
							<input type="hidden"  id="valorLongitud" name="valorLongitud" value="<%=longMapa%>" />                            
							<input type="hidden"  id="valorZoom" name="valorZoom" value="<%=zoomMapa%>"  />     -->

	
				  </div>

			  </div>



			<div class="separador" style="height:20px;">&nbsp;</div>

		<h1 class="separadorTitulo"><%=objIdiomaGeneral.getIdioma("localizacionMapa_Titulo3")%></h1>


              <div class="bloquePestanna" > 
                
                	<%estiloCapaDireccion=""
					claseBotonDir="boton"
					visibleDire=""
					oculDire=" display:none; "
					habilitarBotonDire=""
					if not tineDireccion then
						estiloCapaDireccion=" capaDeshabil"
						claseBotonDir="botonDeshabil"
						visibleDire=" display:none; "
						oculDire=""
						habilitarBotonDire=" disables=""true"" "						
					end if %>
                   <div class="bloqueResumen bloqueResumen_Peq<%=estiloCapaDireccion%>">
                        <div  class="bloqueResumenTitulo">&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto18")%></div>
                        
                        <div style="height:100px; <%=visibleDire%>">                        

                            <div class="filaDatos" >
                                  <span id="spanLocaActiv"><%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto35")%></span>
                            </div>                        						

                            <div class="filaDatos" >
                                <div class="colEtiq" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto20")%>:</div>
                                <div class="colDato" ><input type="text"  id="txtLocPosMapa" name="txtLocPosMapa" style="width:90%;"  class="cajaTextoModal" value="<%=contenidoLocalidad%>"></div>
                            </div>                            
													
                            <div class="filaDatos" >
                                <div class="colEtiq" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto19")%>:</div>
                                <div class="colDato" ><input type="text" id="txtDirPosMapa" name="txtDirPosMapa" style="width:90%;"  class="cajaTextoModal" value="<%=contenioDireccion%>"></div>
                            </div>
                            
						</div>
                        
                        <div style="height:70px; padding:10px 0 0 10px; <%=oculDire%>">                        
                        	<%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto21")%><br />
                            <%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto22")%>
                        </div>
                        
                        <div class="filaDatos" >                        
                            <input id="btnDirPosMapa" name="btnDirPosMapa" <%=habilitarBotonDire%> class="<%=claseBotonDir%>" type="button" value="<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto23")%>"  >
                        </div>                        
                        
                   </div>                                                               
    
                   
                   <div class="bloqueResumen bloqueResumen_Peq">
                        <div  class="bloqueResumenTitulo">&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto24")%></div>


                        <div style="height:100px;">
                            <div class="filaDatos" >
                                <div class="colEtiq" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto13")%>:</div>
                                <div class="colDato" ><input type="text" id="txtLatPosMapa" name="txtLatPosMapa" style="width:90%;"  class="cajaTextoModal"></div>
                            </div>
                            <div class="filaDatos" >
                                <div class="colEtiq" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto14")%>:</div>
                                <div class="colDato" ><input type="text" id="txtLongPosMapa" name="txtLongPosMapa" style="width:90%;"  class="cajaTextoModal"></div>
                            </div>       
                            <div class="filaDatos" >
                                <div class="colEtiq" ><%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto17")%>:</div>
                                <div class="colDato" ><input type="text" id="txtZoomPosMapa" name="txtZoomPosMapa" style="width:90%;"  class="cajaTextoModal"></div>
                            </div>                                   
						</div>
                        
                        <div class="filaDatos" >                        
	                        <input id="btnLocPosMapa" name="btnLocPosMapa" class="boton" type="button" value="<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto23")%>"  >
                        </div>                        
                        
                   </div>   
                   
                   <div class="bloqueResumen bloqueResumen_Peq" id="capaPosLocActual">
                        <div  class="bloqueResumenTitulo">&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto25")%></div>

                        <div style="height:100px;">
                            <div class="filaDatos" >
                                  <span id="spanLocaActiv"><%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto26")%><br/>
                                  <%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto27")%></span>
                                  <span id="spanLocaDesActiv"><%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto28")%><br/>
                                  <%="- " & objIdiomaGeneral.getIdioma("localizacionMapa_Texto29")%></span>                                  
                            </div>                        
						</div>
                        
                        <div class="filaDatos" >                        
	                        <input id="btnPosActual" name="btnPosActual" class="boton" type="button" value="<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto23")%>"  >
                        </div>                                                   
                   </div>                       
                   
                   <div class="bloqueResumen bloqueResumen_Peq capaDeshabil" >
                        <div  class="bloqueResumenTitulo">&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto30")%></div>

                        <div style="height:100px;">
                            <div class="filaDatos" >
                                  <%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto31")%>
                            </div>                        
						</div>
                        
                        <div class="filaDatos" >                        
	                        <input id="btnPosGPSActual" disabled="true" name="btnPosGPSActual" class="botonDeshabil" type="button" value="<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto23")%>"  >
                        </div>         


                   </div>                                          
                                     
                
              </div>




  <script type="text/javascript">
  
  
	
	$(document).ready(function () { 
	
		var permitirGeLoc=false;
		if (navigator.geolocation) {			
			var permitirGeLoc=true;
		}
		if (permitirGeLoc) {
			$('#capaPosLocActual').removeClass("capaDeshabil");
			$('#btnPosActual').removeClass('botonDeshabil');
			$('#btnPosActual').addClass('boton');						
			$('#spanLocaActiv').show();
			$('#spanLocaDesActiv').hide();			
			$('#btnPosActual').attr('disabled',false);
			
		} else {
			$('#capaPosLocActual').addClass("capaDeshabil");
			$('#btnPosActual').removeClass('boton');
			$('#btnPosActual').addClass('botonDeshabil');						
			$('#spanLocaActiv').hide();
			$('#spanLocaDesActiv').show();			
			$('#btnPosActual').attr('disabled',true);
		}
	
			$("#btnDirPosMapa").click(function() {
			  	var direccion="";
				if ($('#txtLocPosMapa').val()!="" ) {
					direccion= direccion + " " + $('#txtLocPosMapa').val()
				}
				
				if ($('#txtDirPosMapa').val()!="" ) {
					direccion= direccion + " " + $('#txtDirPosMapa').val()
				}				
				
				if (direccion!="") {
				 	inicializarDireccion(direccion);
				}
			});	
			
			
			$("#btnPosActual").click(function() {
				var latitud="";
				var longitud="";
				if (navigator.geolocation) {											  
					navigator.geolocation.getCurrentPosition(function(position) {
						latitud=position.coords.latitude;
						longitud=position.coords.longitude;					
						if (latitud!="" && longitud!="") {
							posicionarEnMapa(latitud,longitud,$('#valorZoom').val());
						}						
					});		
				}	

			});			
			
			
			$("#btnLocPosMapa").click(function() {
			  	var latPosicionar=$("#txtLatPosMapa").val();
			  	var longPosicionar=$("#txtLongPosMapa").val();	
				var zoomPosicionar=$("#txtZoomPosMapa").val();	
				if (zoomPosicionar=="") {
					zoomPosicionar=$('#valorZoom').val();
				}
				if (latPosicionar=="" || longPosicionar=="") {
						alert("<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto32")%>");
				}else {
					posicionarEnMapa(latPosicionar,longPosicionar,zoomPosicionar);
				}
				 
				 
			});	
						

			
			$("#btnAceptarLoc").click(function() {		
		 	    window.parent.document.getElementById("hidGeoLocUso").value="<%=request("campo")%>";  											   
				var valorLoc=$('#valorLatitud').val() + "|" + $('#valorLongitud').val() + "|" + $('#valorZoom').val();
				window.parent.document.getElementById("hidMap_<%=request("campo")%>").value=valorLoc;
				parent.$.fancybox.close();
			});				
						
			$("#btnGuardarLoc").click(function() {		
		 	    window.parent.document.getElementById("hidGeoLocUso").value="<%=request("campo")%>";  											   
				var valorLoc=$('#valorLatitud').val() + "|" + $('#valorLongitud').val() + "|" + $('#valorZoom').val();
				window.parent.document.getElementById("hidMap_<%=request("campo")%>").value=valorLoc;
				
				//lanzamos ajax para guardar la localizacion
				$.ajax({
					type: "POST",
					url: "/dmcrm/APP/comun2/localizacionMapaGuardar.asp",
					data: "t=<%=tabla%>&c=<%=campo%>&cc=<%=campoClave%>&cr=<%=codRegistro%>&lc=" + valorLoc,
					success: function(resp) {
						if (resp!="") {
							parent.$.fancybox.close();
						} else {
							alert("<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto36")%>");
						}
					}
				});								

			});						
			
			
	});								
  
  
 	  window.parent.document.getElementById("hidGeoLocUso").value="";  
  
      var initListener;
      var marker;
	  <%if trim(latMapa)<>"" And trim(longMapa)<>"" then%>
		  var latlng = new google.maps.LatLng(<%=latMapa%>, <%=longMapa%>);
		  var options = {
			  zoom: <%=zoomMapa%>,
			  center: latlng,
			  mapTypeId: google.maps.MapTypeId.ROADMAP,
			  draggableCursor: "crosshair",
			  streetViewControl: false
			};
		  var map = new google.maps.Map(document.getElementById("editarMapa"), options);
	  <%end if%>
	  posicionarEnMapa($('#valorLatitud').val(),$('#valorLongitud').val(),$('#valorZoom').val());

	  
	  //Funcion para posicionar en mapa
      function posicionarEnMapa(latitud, longitud, zoom)
      {

	  	 if ( isNumber(latitud) && isNumber(longitud) && isNumber(zoom) ) {
			  var latLong = new google.maps.LatLng(latitud, longitud);
			  mostrarPosicion(latLong);
			  map.setCenter(latLong);
			  map.setZoom(parseFloat(zoom))
			  $('#valorLatitud').val(latitud);
			  $('#valorLongitud').val(longitud);
			  $('#valorZoom').val(zoom);
		  } else {
		  	alert("<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto34")%>");
		  }
      }	  
	  
      function mostrarPosicion(latLong)
      {
        // show the lat/long
        if (marker != null) {
          marker.setMap(null);
        }
        marker = new google.maps.Marker({
          position: latLong,
          map: map});
      }
	  
	  
	  google.maps.event.addListener(map,"click", function(location)
      {
		 mostrarPosicion(location.latLng)
		 
		 $('#valorLatitud').val(location.latLng.lat());		
		 $('#valorLongitud').val(location.latLng.lng());		 
		 	 
      });

	  google.maps.event.addListener(map,'zoom_changed', function(oldLevel, newLevel)
      {
         $("#valorZoom").val(map.getZoom());
      });

	  
	  
	  //http://www.ajaxshake.com/es/JS/2410601/como-posicionar-una-direccion-en-el-mapa-con-google-maps-api-3-y-jquery-googlemaps.html
		function inicializarDireccion(address) {
		  var geoCoder = new google.maps.Geocoder(address)
		  var request = {address:address};
		  geoCoder.geocode(request, function(result, status){
		 	if (result.length>0) {
				posicionarEnMapa(result[0].geometry.location.lat(),result[0].geometry.location.lng(),$("#valorZoom").val());				
			} else {
				alert("<%=objIdiomaGeneral.getIdioma("localizacionMapa_Texto33")%>");
			}	

		  })
		}	  
	  
		function isNumber(n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
		}


  </script>


	</body>
	<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->    
</html>