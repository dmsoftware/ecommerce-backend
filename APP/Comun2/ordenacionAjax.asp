<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<%
'***** PARAMETROS NECESARIOS *****
'*********************************

tabla = request("regTabla")
campoClave = request("regCampoClave")
campoOrdenacion = request("regCampoOrdenacion")
if isnumeric(request("id")) then id = request("id") else id = -100 end if
valor = cint(request("value"))
arrFiltro = split(request("filtro"),";")
arrValFiltro = split(request("valFiltro"),";")

'** FUNCION PARA CREAR LOS FILTROS **'
function crearFiltro(tabla,campo,valor)
	cfQuery = "SELECT tipo FROM maestra WHERE tabla = '" & tabla & "' AND campo = '" & campo & "'"
  	Set rsFiltro = connCRM.AbrirRecordset(cfQuery,3,1)
	if not rsFiltro.eof then
		fTipo = rsFiltro("tipo")
		rsFiltro.cerrar
		Set rsFiltro = nothing
		select case fTipo
			case "T", "P"
				cCondicion = " AND " & trim(arrFiltro(i)) & " = '" & trim(arrValFiltro(i)) & "'"
			case "F"
				cCondicion = " AND convert(varchar," & trim(arrFiltro(i)) & "," & obtenerFormatoFechaSql() & ") = convert(varchar," & trim(arrValFiltro(i)) & "," & obtenerFormatoFechaSql() & ")"				
			case else
				cCondicion = " AND " & trim(arrFiltro(i)) & " = " & trim(arrValFiltro(i))
		end select 
		crearFiltro = cCondicion
	end if
end function

'***** OBTENEMOS EL VALOR ORIGINAL DEL CAMPO *****'
query = "SELECT " & campoOrdenacion & " FROM " & tabla & " WHERE " & campoClave & " = " & id
set rsActual = connCRM.AbrirRecordset(query,0,1)
valorActual = rsActual(0)
rsActual.cerrar
Set rsActual = Nothing
if valorActual < valor then
	operadorCondicionMenor = " <= " 
	operadorCondicionMayor = " > "
elseif valorActual > valor then
	operadorCondicionMenor = " < "
	operadorCondicionMayor = " >= "
else
	response.Write(valorActual)
	response.End()
end if

'***** NO PERMITIMOS PONER EL VALOR 0 *****'
if valor = 0 then
	response.Write(valorActual)
	response.End()
end if

'***** MONTAMOS LAS CONSULTAS *****'
qSelect = "SELECT " & campoClave & ", " & campoOrdenacion & " FROM " & tabla & " WHERE "
qCondicionMenores = campoClave & " <> " & id & " AND " & campoOrdenacion & operadorCondicionMenor & valor
qCondicionMayores = campoClave & " <> " & id & " AND " & campoOrdenacion & operadorCondicionMayor & valor
for i = 0 to ubound(arrFiltro)
	if trim(arrFiltro(i)) <> "" then	    
		qWhere = qWhere & crearFiltro(tabla,trim(arrFiltro(i)),trim(arrValFiltro(i)))
	end if
next
qOrder = qOrder & " ORDER BY " & campoOrdenacion & " ASC"

orden = 1

'***** ORDENAMOS LOS MENORES *****'
Set rsMenores = connCRM.AbrirRecordset(qSelect & qCondicionMenores & qWhere & qOrder,3,3)
while not rsMenores.EOF 
	rsMenores(campoOrdenacion) = orden
	rsMenores.Update 
	rsMenores.MoveNext 
	orden = orden + 1
wend 
rsMenores.cerrar()
Set rsMenores = Nothing 

'***** ORDENAMOS EL CAMBIADO *****'
query = "UPDATE " & tabla & " SET " & campoOrdenacion & " = " & orden & " WHERE " & campoClave & " = " & id
'connCRM.execute(query)
connCRM.execute query,intNumRegistros
orden  = orden + 1

'***** ORDENAMOS LOS MAYORES *****'
Set rsMayores = connCRM.AbrirRecordset(qSelect & qCondicionMayores & qWhere & qOrder,3,3)
while not rsMayores.EOF 
	rsMayores(campoOrdenacion) = orden
	rsMayores.Update 
	rsMayores.MoveNext 
	orden = orden + 1
wend 
rsMayores.cerrar()
Set rsMayores = Nothing 

response.Write(-1)
%>