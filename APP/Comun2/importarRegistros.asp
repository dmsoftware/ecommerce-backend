<%@ Language=VBScript %>

<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!--#include file="c_uploader.asp"-->
<!--#include file="funcionesMaestra.inc"-->
<script language="javascript" type="text/javascript">
	function comprobarCombos(){
		var numComb = document.forms.importUsers.numCombos.value;
		var cuantos = 0;
		for (i=0;i<numComb;i++){
			var valorCombo = document.getElementById("campo" + i).value;
			cuantos = cuantos + parseInt(valorCombo);
		}
		if (cuantos>0){
			document.importUsers.submit();
		} else {
			alert("Debe seleccionar al menos un campo para importar");
		}
	}
</script>

<%
'PARAMETROS DE LA PAGINA 
Tabla=Request("Tabla")
Conexion=session(denominacion & "_" & request("conex"))
Filtro=request("Filtro")
ValorFiltro=request("ValorFiltro")
Filtro2=request("Filtro2")
ValorFiltro2=request("ValorFiltro2")
intModo = cInt(request.QueryString("modo"))
intSep = cInt(request.QueryString("sep"))
intSepGrup = cInt(request.QueryString("sepGrup"))
select case intSep
	case 0
		separador = ";"
	case 1
		separador = ","
	case 2
		separador = ":"
	case else
		separador = ";"
end select
select case intSepGrup
	case 0
		separadorGru = ","
	case 1
		separadorGru = ";"
	case 2
		separadorGru = ":"
	case else
		separadorGru = ","
end select
'Directorio donde se alojar� temporalmente el archivo
directorio=strlocal & carpetaInterna & "/usuariosFtp/" & left(request("conex"),8)

'FUNCIONES Y PROCEDIMIENTOS

'funci�n que dada una tabla de la maestra, una tabla de memoria y una conexion, nos dice si los campos de la tabla
'de la maestra son los mismos que los de la tabla en memoria. Estos ultimos han de estar en el orden de la maestra
function MismaEstructuraEnMaestra(tablaMaestra,tablaMemoria,connLocal)
	dim intIndiceTabla
	intIndiceTabla = 0
	sql = "SELECT NomUsuario FROM Maestra WHERE Tabla ='" & TablaMaestra & "' AND lcase(Campo)<>'suportal' ORDER BY Orden ASC"
	Set rsCamposMaestra = connLocal.AbrirRecordset(sql,3,3)
	
	MismaEstructuraEnMaestra = true 'Inicializaci�n
	while not rsCamposMaestra.EOF
		if trim(lcase(rsCamposMaestra("NomUsuario"))) <> trim(lcase(tablaMemoria(intIndiceTabla))) then
			MismaEstructuraEnMaestra = false
			exit function
		end if 'trim(lcase(rsCamposMaestra("campo"))) <> trim(lcase(tablaMemoria(intIndiceTabla)))
		intIndiceTabla = intIndiceTabla + 1
		rsCamposMaestra.movenext()
	wend 'not rsCamposMaestra.EOF
	
	rsCamposMaestra.cerrar()
	set rsCamposMaestra = nothing
end function

'Funci�n que detecta si una l�nea tiene todos los caracteres vac�os
'Ocurre mucho cuando se parte de un excell original con varias l�neas y se borra alguna.
'El programa mete todo ; pero campos vac�os
function LineaVacia (arrayLinea)
	dim blnLineaVacia
	dim i
	blnLineaVacia = true
	
	for i = 0 to ubound(arrayLinea)
		if arrayLinea(i) <> "" then blnLineaVacia = false
	next
	LineaVacia = blnLineaVacia
end function

'funci�n que dado un fichero abierto, y un tama�o de la cabecera (n�mero de campos),
'nos acumula posibles errores en strError con los que ya hab�a, y nos devuelve
'una tabla bidimensional [campos x registros] con los datos del fichero cargados
sub CargarTablaFichero (ts, intTamCabecera, modo, byref strError, byref arrTablaDatos, byref textoAux)
	dim intLinea
	dim strLinea
	dim arrLinea
	dim i
	'inicializaci�n
	strError = "" 
	intLinea = 0
	textoAux = ""
	redim arrTablaDatos(intTamCabecera,0)
	idLinea = 0
	Do While Not ts.AtEndOfStream
		if idLinea mod 2 = 0 then
			colorfila = "#cccccc"
		else
			colorfila = "#ffffff"
		end if
		strLinea=""
		strLinea = ts.ReadLine
		arrLinea = split(strLinea,separador)
		if not LineaVacia(arrLinea) then 'Ignoro las l�neas vac�as
			idLinea = idLinea + 1
			if ubound(arrLinea) <> intTamCabecera then 'la linea no tiene los mismos campos que la cabecera. Puede ser por que existe alg�n ";" en el texto del usuario.
				strError = strError & " - Linea " & intLinea + 2 & " Err�nea o no tiene el formato correcto. Compruebe que tiene el mismo n�mero de campos que la cabecera y que no existen caracteres �;�.<br>"
			else 'linea correcta
				if not isNumeric(trim(arrLinea(0))) and modo<>1 then
					strError = strError & " - Linea " & intLinea + 2 & " No tiene un c�digo num�rico.<br>"
				else 'el c�digo es un n�mero
					redim preserve arrTablaDatos(intTamCabecera,intLinea)
					textoAux = textoAux & "<tr>"
					for i = 0 to ubound(arrLinea)
						textoAux = textoAux & "<td bgcolor='" & colorfila & "'>" & trim(arrLinea(i)) & "</td>"
						arrTablaDatos(i,intLinea) = trim(arrLinea(i))
					next
					textoAux = textoAux & "</tr>"
				end if 'not isNumeric(trim(arrLinea(0)))
			end if 'ubound(arrLinea)<>intTamCabecera
		end if 'not LineaVacia(arrLinea)
		intLinea = intLinea + 1
	loop
end sub
%>
<HTML>
<HEAD>
<TITLE>Mantenimiento masivo de registros</TITLE>
<link rel="stylesheet" href="/administrar/estilos.css"></link>
</HEAD>
<BODY class="fondoPagIntranet">
<div style="background-color:#FFFFFF;height:100%;border:1px solid #c5c5c5">
<%
if var_sgbd = 1 then
	almohadiFecha = "#"
else
	almohadiFecha = "'"
end if

if request.QueryString("h")="" then 'se ha subido el fichero
	Set ObjUpload = new uploader
	ObjUpload.MaxFileBytes = 5000000
	ObjUpload.FileExtensionList = "csv"
	'El nombre del archivo es "importacion" concatenaco con el nombre de la tabla, la fecha y la hora
	strNombreArchivo = "importacion" & tabla & replace(date(),"/","") & replace(time(),":","")
	ObjUpload.NombreEnServidor = strNombreArchivo
	ObjUpload.UpLoadPath = Directorio
	intUploadCorrecto=ObjUpLoad.Upload_Arsys
	
%>

<table width="93%" height=90% cellspacing="0" cellpadding="0" align=center ID="Table1">
	<tr><td height=15></td></tr>
	<tr><td valign=top><b>RESULTADO:</b></td></tr>
    <tr><td height=20></td></tr>
	<tr>
		<td>
<%	Select case intUploadCorrecto
		case -1
			strError = "" 'Variable para acumular el error
			Set fs = Server.CreateObject("Scripting.FileSystemObject")
			strPathFich = directorio & "/" & ObjUpLoad.NombreEnServidor
			If fs.FileExists(strPathFich) Then
				Set fichero = fs.GetFile(strPathFich)
				Set ts = fichero.OpenAsTextStream(1)
				'Primero leemos la primera l�nea para sacar los nombres de los campos
				if ts.AtEndOfStream then 'fichero vac�o
					response.Write " - El fichero est� vac�o<br>"
				else 'hay algo
					strLinea=""
					strCabecera = ts.ReadLine
					arrCabecera = split(strCabecera,separador) 'pasamos la linea con los nombres de los campos a tabla
					intTamCabecera = ubound(arrCabecera)
					if intTamCabecera = 0 then 'no tenemos cosas separadas por ";"
						response.Write " - Formato del fichero err�neo<br>"
					else
						set conn = new DMWConexion
						connCRM.abrir ("" & conexion)
						connCRM.beginTrans
						textoA = ""
						if intModo=1 then
							%><table><tr><td height="15"></td></tr><tr><td height="5">Este es el contenido del archivo que ha exportado. Se a&ntilde;adir&aacute;n los siguientes registros:</td></tr>
                        	<form name="importUsers" id="importUsers" method="post" action="importarRegistros2.asp">
                            	<input type="hidden" name="fichUsers" value="<%=strPathFich%>" />
                                <input type="hidden" name="Tabla" value="<%=Tabla%>" />
                                <input type="hidden" name="Conexion" value="<%=Conexion%>" />
                                <input type="hidden" name="Filtro" value="<%=Filtro%>" />
                                <input type="hidden" name="ValorFiltro" value="<%=ValorFiltro%>" />
                                <input type="hidden" name="Filtro2" value="<%=Filtro2%>" />
                                <input type="hidden" name="ValorFiltro2" value="<%=ValorFiltro2%>" />
                                <input type="hidden" name="intSep" value="<%=intSep%>" />
                                <input type="hidden" name="intSepGrup" value="<%=intSepGrup%>" />
								<table width='92%'>
									<tr><%
							a = 0
							while a<intTamCabecera+1
								Set rsCamposMaes = connCRM.abrirRecordSet("SELECT NomUsuario,orden FROM Maestra WHERE tabla='" & Tabla & "' and clave=0 order by orden asc",3,3)
								%><td><select id="campo<%=a%>" name="campo<%=a%>" style="width:100%"><option value="0">No importar</option><%
								numOpcion = 1
								while not rsCamposMaes.eof
									%><option value="<%=rsCamposMaes("orden")%>"><%=rsCamposMaes("NomUsuario")%></option><%
									numOpcion = numOpcion + 1
									rsCamposMaes.movenext
								wend
								%><option value="999">>> Grupos</option></select></td><%
								a = a + 1
							wend
							%><input type="hidden" name="numCombos" value="<%=a%>" /></tr><%
							'Cargamos en una tabla todas las lineas
		 					CargarTablaFichero ts, intTamCabecera,intModo, strError, tablaDatos, textoA
							textoA = textoA & "</table>"
						else 'intModo=1
							if not MismaEstructuraEnMaestra(Tabla,arrCabecera,conn) then
		 						strError = strError & " La estructura de la cabecera no es correcta<br>"
		 					end if
							CargarTablaFichero ts, intTamCabecera,intModo, strError, tablaDatos, textoA
						end if
							if strError<>"" then 'Hay alg�n error
								response.Write "No se ejecut� ninguna operaci�n, se han producido los siguientes ERRORES:<br><br>" & strError
							else 'no se han detectado errores en el fichero
								'miramos qu� tenemos que hacer
								Clave=ClaveTabla (Tabla,TipoClave)
								select case intModo
									'----------------------------------------------- ALTAS ---------------------------------------------------------------------------------------------------
									case 1: 'Alta masiva
										response.Write(textoA)
										%><br /><br /><table width="100%"><tr><td align="center"><input type="button" onClick="javascript:comprobarCombos();" value="Enviar" /></td></tr></table></form><%
										ResultadoUpload= ""
									'----------------------------------------------- MODIFICACIONES ------------------------------------------------------------------------------------------
									case 2: 'Modificaci�n masiva
										'Saco todos los campos menos el c�digo
										sql = "SELECT Campo,Tipo FROM Maestra WHERE Tabla ='" & tabla & "' AND Campo<>'" & clave & "' AND lcase(Campo)<>'suportal' ORDER BY Orden ASC"
										Set rsCampos = connCRM.AbrirRecordset(sql,3,3)
										'Para cada registro del archivo:
										for intRegistroEnCurso = 0 to ubound(tablaDatos,2)
											sql = "UPDATE " & Tabla & " SET "
											intCampoEnCurso = 1
											if not rsCampos.bof then rsCampos.moveFirst
											while not rsCampos.eof and intCampoEnCurso<= ubound(tablaDatos,1)
												sql = sql & rsCampos("Campo") & "="
												select case rsCampos("Tipo")
													case "T","M","ME","E","IP","CP","X","XI","XA":
														sql = sql & "'" &  replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),"'","''") & "'"
													case "F","FO","FM":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))="" then
															sql = sql & "null"
														else
															sql = sql & almohadiFecha & FormatearFechaSQLSever(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) & almohadiFecha
														end if 'trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))=""
													case "H":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso))="" then
															sql = sql & "null"
														else
															sql = sql & almohadiFecha & FormatearHora(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) & almohadiFecha
														end if
													case "B":
														if lcase(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "verdadero" then
															sql = sql & "-1"
														else 'vale "falso"
															sql = sql & "0"
														end if 'lcase(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "verdadero"
													case "N","ND","SN","SM","SE","US":
														if trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = "" then
															sql = sql & "0"
														else 'vale "falso"
															sql = sql& "" & tablaDatos(intCampoEnCurso,intRegistroEnCurso)
														end if 'trim(tablaDatos(intCampoEnCurso,intRegistroEnCurso)) = ""
													case else:
														sql = sql &  replace(tablaDatos(intCampoEnCurso,intRegistroEnCurso),"'","''")
												end select 'rsCampos("Tipo")
												intCampoEnCurso = intCampoEnCurso + 1
												rsCampos.moveNext()
												if not rsCampos.eof and intCampoEnCurso<= ubound(tablaDatos,1) then sql = sql & ","
											wend 'not rsCampos.eof
											sql = sql & " WHERE " & clave & "=" & tablaDatos(0,intRegistroEnCurso)
											connCRM.execute sql,intNumRegistros
											intCodigo = intCodigo + 1
										next 'intRegistroEnCurso = 0 to ubound(tablaDatos,2)
										ResultadoUpload= "Actualizaci�n realizada correctamente"
										'fs.DeleteFile(strPathFich)
									'----------------------------------------------- BAJAS ---------------------------------------------------------------------------------------------------
									case 3: 'Borrado masivo									
										for intRegistroEnCurso = 0 to ubound(tablaDatos,2)
											sql = "DELETE FROM " & Tabla & " WHERE " & Clave & " = " & tablaDatos(0,intRegistroEnCurso)
											connCRM.execute sql,intNumRegistros
											
											'lanzar borrado exec

										next 'intRegistroEnCurso = 0 to ubound(tablaDatos,2)
										ResultadoUpload= "Actualizaci�n realizada correctamente"
										'fs.DeleteFile(strPathFich)
									case else: 'Desconocido
										strError = strError & " - Modo de operaci�n desconocido<br>"
								end select 'cInt(request.QueryString("modo"))
							end if 'strError<>""
							connCRM.CommitTrans
							connCRM.cerrar 
							set conn = nothing
						'end if 'not MismaEstructuraEnMaestra(Tabla,arrCabecera,conn)
					end if 'intTamCabecera = 0
				end if 'rs.AtEndOfStream
				ts.Close()
				Set ts = nothing
			else 'El fichero no existe
				strError = strError & " - El fichero no se ha subido correctamente o se ha producido un error en la subida.<br>" & strPathFich & "<br>"
			end if 'fs.FileExists(strPathFich)
			set fs = nothing
		case 1
		    ResultadoUpload= "NO SE ENCUENTRA EL NOMBRE DE LA TEXT TIPO FILE �FICHERO�"
		case 2
		    ResultadoUpload= "caja de texto vacia o no existe el fichero."
		case 3
		   	ResultadoUpload= "LA LONGITUD DEL ARCHIVO A SUBIR [" & ObjUpload.TamanoReal & "bytes] ES MAYOR QUE LA PERMITIDA [" & ObjUpLoad.MaxFileBytes & " bytes]."
		case 4
		   	ResultadoUpload= "LA CONFIGURACION ACTUAL NO PERMITE EXTENSIONES DEL TIPO �" & ObjUpload.ExtensionFichero & "�"
		case 10
		   	ResultadoUpload= "Error no controlado: [" & ObjUpload.Error & "]"
	end select 'intUploadCorrecto
	
	set ObjUpload=nothing
	response.Write ResultadoUpload
%>
		</td>
	</tr>
	<tr><td height=100%></td></tr>
</table>
<%
else 'primera vez
%>
<!--FORM DE SUBIDA -->
<script language=javascript>
	function CambiarAction(){
		var intModo;
		for (i=0;i<document.form1.modo.length;i++){
			if (document.form1.modo[i].checked){
				intModo = i;
				break;
			}
		} 
		var intSep = document.form1.separador.value;
		var intSepGrup = document.form1.separadorGru.value;
		if (intModo==0){
			document.getElementById("separadores").style.display = 'block';
		} else {
			document.getElementById("separadores").style.display = 'none';
		}
		document.form1.action="importarRegistros.asp?tabla=<%=tabla%>&conexion=<%=request("conex")%>&filtro=<%=filtro%>&valorFiltro=<%=valorFiltro%>&filtro2=<%=filtro2%>&valorFiltro2=<%=valorFiltro2%>&modo="+(intModo+1)+"&sep="+intSep+"&sepGrup="+intSepGrup;
	}
</script>
<form method="POST" id="form1" name="form1" ENCTYPE="multipart/form-data" action="importarRegistros.asp?tabla=<%=tabla%>&conexion=<%=request("conex")%>&filtro=<%=filtro%>&valorFiltro=<%=valorFiltro%>&filtro2=<%=filtro2%>&valorFiltro2=<%=valorFiltro2%>&modo=1">
<table width="93%" cellspacing="0" cellpadding="0" align=center ID="Table2">
	<tr><td height=15></td></tr>
	<tr>
		<td style="text-align: justify;">
			<font size="1" face="Verdana"><b><u>INSTRUCCIONES:</u></b><br><br>Para hacer altas de una gran cantidad de registros exporte un archivo en formato "CSV (delimitado por comas)(*.csv)" y siga unos sencillos pasos.<br>
			Para modificaciones o borrados, puede hacerlo de una manera c�moda y sencilla trabajando en formato Excel. Primero desc�rguese el excel de la tabla con la que desea trabajar, pulsando el icono de "Exportar datos en formato Excel". Trabaje sobre esta estructura manteniendo la primera l�nea en la que se indican los nombres de los campos, y haga lo siguiente dependiendo de la funci�n que desea realizar:<br><br>
			<b><u>Modificar registros:</u></b> Si desea mofificar muchos registros a la vez, deje en el excel <b>SOLO</b> los registros que desea modificar (y la cabecera con los nombres). Borre el resto de l�neas/registros.<br><br>
			<b><u>Borrar registros:</u></b> Si desea borrar muchos registros a la vez, deje en el excel <b>SOLO</b> los registros que desea borrar (y la cabecera con los nombres). Borre el resto de l�neas/registros.<br><br>
			Una vez preparado el excel con los registros deseados, debe guardarlo en su disco seleccionando "guardar como..." y eligiendo el formato "CSV (delimitado por comas)(*.csv)". Una vez guardado, seleccionelo y pulse "Lanzar"<br><br>
			</font>
		</td>
	</tr>
</table>
<table width="93%" cellspacing="0" cellpadding="1" style="border:1px solid #c5c5c5;" align=center ID="Table3">
  <tr>
    <td>
      <table border="0" cellspacing="0" align="center" width="100%" ID="Table4">
      	<tr><td height=5 width=100%></td></tr>
        <tr><td height=5 width="100%" align="center">Elija el fichero en formato CSV:</td></tr>
        <tr><td align=center><input type="file" size=45 name="FICHERO" ID="File1"></td></tr>
        <tr><td height=5 width=100%></td></tr>
        <tr><td width=100% align=center>Seleccione la acci&oacute;n que desee realizar:</td></tr>
		<tr>
			<td align=center>
				<input class="radios" type="radio" name="modo" onClick="CambiarAction();" checked ID="Radio1" VALUE="Radio1">Alta&nbsp;&nbsp;&nbsp;<input class="radios" type="radio" name="modo" onClick="CambiarAction()" ID="Radio2" VALUE="Radio2">Modificaci�n&nbsp;&nbsp;&nbsp;<input class="radios" type="radio" name="modo" onClick="CambiarAction();" ID="Radio3" VALUE="Radio3">Borrado
			</td>
		</tr>
        <tr><td height=10 width=100%></td></tr>
              	<tr><td>
            <div id="separadores" style="text-align:center">
Escoja el separador de los campos del archivo CSV:<br /><br />Separador&nbsp;<select name="separador" onChange="CambiarAction();">
                    <option value="0">;</option>
                    <option value="1">,</option>
                    <option value="2">:</option>
                </select>&nbsp;&nbsp;Separador de grupos&nbsp;
                <select name="separadorGru" onChange="CambiarAction();">
                    <option value="0">,</option>
                    <option value="1">;</option>
                    <option value="2">:</option>
                </select><br /><br /></div>
        </td></tr>
		<tr><td align=center><input type="submit" value=" Lanzar " name="B1" ID="Submit1"></td></tr>
        <tr><td height=10 width=100%></td></tr>
      </table>
    </td>
  </tr>
</table>
</form>
<!--FIN FORM DE SUBIDA -->

<%end if 'request.QueryString("h")=""%>

<p align="center" ><a class="cerrar" href="javascript:parent.$.fancybox.close();">[C e r r a r&nbsp;&nbsp; v e n t a n a]</a></p>
</div>
</BODY>
</HTML>
