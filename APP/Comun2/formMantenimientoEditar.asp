    
    <!--Includes Pesta�as -->
	 <script type="text/javascript">
        $(document).ready(function() {
			/*$('input:nth(1)').focus();*/
								   
            //Cuando el sitio carga...
            $(".tab_content").hide(); //Esconde todo el contenido
            $("ul.tabs li:first").addClass("active").show(); //Activa la primera tab
            $(".tab_content:first").show(); //Muestra el contenido de la primera tab
            //On Click Event
            $("ul.tabs li").click(function() {
                $("ul.tabs li").removeClass("active"); //Elimina las clases activas
                $(this).addClass("active"); //Agrega la clase activa a la tab seleccionada
                $(".tab_content").hide(); //Esconde todo el contenido de la tab
                var activeTab = $(this).find("a").attr("href"); //Encuentra el valor del atributo href para identificar la tab activa + el contenido
                $(activeTab).fadeIn(); //Agrega efecto de transici�n (fade) en el contenido activo
                return false;
            });
			
			
			$(".cEmergenteFoto").live('click', function(){
				$(this).fancybox({
					'imageScale'		: true,
					'zoomOpacity'		: true,
					'overlayShow'		: true,
					'overlayOpacity'	: 0.7,
					'overlayColor'		: '#333',
					'centerOnScroll'	: true,
					'zoomSpeedIn'		: 600,
					'zoomSpeedOut'		: 500,
					'transitionIn'		: 'elastic',
					'transitionOut'		: 'elastic',
					'titleShow'		    : false
				}).trigger("click"); 
				return false; 	
			});						
			
			$(".cEmergenteSubida").live('click', function(){
				$(this).fancybox({
					'imageScale'			: true,
					'zoomOpacity'			: true,
					'overlayShow'			: true,
					'overlayOpacity'		: 0.7,
					'overlayColor'			: '#333',
					'centerOnScroll'		: true,
					'zoomSpeedIn'			: 600,
					'zoomSpeedOut'			: 500,
					'transitionIn'			: 'elastic',
					'transitionOut'			: 'elastic',
					'type'					: 'iframe',
					'frameWidth'			: 600,
					'frameHeight'			: 180,
					'titleShow'		        : false,					
					'onClosed'		        : function() {
							//recargarAlCerrar();
							window.location.reload();
					}
				}).trigger("click"); 
				return false; 	
			});	
			
	
			$('.enlaceBorrar').click( function() {		
					var paremetros=$(this).attr('rel');					
					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/comun2/borrarAdjuntos.asp",
						data: paremetros,
						success: function(datos) {
							window.location.reload();
						}
					});				
			});
			
			
			
			/*Funciones de los mapas*/
			
			$('.enlEditMapa').click( function() {					
				var campoMapa=$(this).attr('rel');
				var valLocalizacion=$('#hidMap_' + campoMapa).val();
				var rutaPantMapa="/dmcrm/APP/comun2/localizacionMapa.asp?tabla=<%=request("tabla")%>&campo=" + campoMapa + "&codigo=<%=request("codigo")%>";
				var valLatitud="";
				var valLongitud="";
				var valZoom="";
				var localizacion=$('#hidMap_' + campoMapa).val().split("|");							
				if (localizacion.length-1>=0) {
					valLatitud=localizacion[0];								
				}
				if (localizacion.length-1>=1) {
					valLongitud=localizacion[1];
				}
				if (localizacion.length-1>=2) {
					valZoom=localizacion[2];							
				}					
				if (valLocalizacion!="") {
					rutaPantMapa+="&latMapa=" + valLatitud + "&longMapa=" + valLongitud + "&zoomMapa=" + valZoom;
				} 

				$('#enlOculMapa_' + campoMapa).attr('href',rutaPantMapa);	
				$('#enlOculMapa_' + campoMapa).click();
			});	
			
			
			
			$(".cEmergenteEdiMapa").live('click', function(){
				$(this).fancybox({
					'imageScale'			: true,
					'zoomOpacity'			: true,
					'overlayShow'			: true,
					'overlayOpacity'		: 0.7,
					'overlayColor'			: '#333',
					'centerOnScroll'		: true,
					'zoomSpeedIn'			: 600,
					'zoomSpeedOut'			: 500,
					'transitionIn'			: 'elastic',
					'transitionOut'			: 'elastic',
					'type'					: 'iframe',
					'frameWidth'			: '100%',
					'frameHeight'			: '100%',
					'titleShow'		        : false,					
					'onClosed'		        : function() {
						var nomCampoMap=$('#hidGeoLocUso').val();
						if (nomCampoMap!="") {
							$('.lisEdiMapa_' + nomCampoMap).removeClass('bloqueMapaOculto');
							$('.lisAnnMapa_' + nomCampoMap).addClass('bloqueMapaOculto');															
							recargarPluginMapa(nomCampoMap);
						}
						$('#hidGeoLocUso').val('');
					}
				}).trigger("click"); 
				return false; 	
			});	
			
			
			
			$('.enlaceBorrarMapa').click( function() {					
				var campoMapa=$(this).attr('rel');
				$('#hidMap_' + campoMapa).val('');
				$('.lisEdiMapa_' + campoMapa).addClass('bloqueMapaOculto');
				$('.lisAnnMapa_' + campoMapa).removeClass('bloqueMapaOculto');								
			});	
			
			/*$("#capaMapa_COMER_PRU_LOC").gMap({
					markers: [
						{//controls: false,
						//scrollwheel: false,
						latitude: 43.318278,
						longitude: -1.983259,					 
						//html: "direccion, localidad",
						//popup: true,
						icon: {}}],
						zoom: 16,
						//maptype: google.maps.MapTypeId.ROADMAP,
						//panControl: false
				});	
		
        	});*/				
			
		
        });
        

		function recargarPluginMapa(nomCampo) {
			//hidMap_COMER_PRU_LOC
			if ($('#hidMap_' + nomCampo).val()!="") {
				var valLatitud=0;
				var valLongitud=0;
				var valZoom=0;
				var localizacion=$('#hidMap_' + nomCampo).val().split("|");							
				if (localizacion.length-1>=0) {
					valLatitud=localizacion[0];								
				}
				if (localizacion.length-1>=1) {
					valLongitud=localizacion[1];
				}
				if (localizacion.length-1>=2) {
					valZoom=localizacion[2];							
				}	
				$('#capaMapa_' + nomCampo).gMap({
						markers: [
							{ latitude: valLatitud, longitude: valLongitud, }
						],
						zoom: parseInt(valZoom),
						streetViewControl:false
					});	

			}			
		}

		
        function cargarPestannas(tipo,tabla,filtro,valorFiltro,contador,enlace)
        {
			//alert(contador)
            if (contador=="0" )
            {
                if (document.getElementById("frmRelacionados")!=null)
                {
                    document.getElementById("frmRelacionados").src="";
                    document.getElementById("frmRelacionados").style.display="none";
                }
                document.getElementById("divMenuHor").style.display="";
				if (document.getElementById("bloqueImagenes")!=null) {				
					document.getElementById("bloqueImagenes").style.display="";				
				}	
				if (document.getElementById("bloqueArchivos")!=null) {
					document.getElementById("bloqueArchivos").style.display="";
				}	
				if (document.getElementById("bloqueLocalizacion")!=null) {				
					document.getElementById("bloqueLocalizacion").style.display="";				
				}									

            }
            else
            {
                if (document.getElementById("frmRelacionados")!=null)
                {
					if (tipo=="AM")
					{
                        document.getElementById("frmRelacionados").src=enlace + "?cod=" + valorFiltro;											
					}
					else if (tipo=="MN")
                    {
                        document.getElementById("frmRelacionados").src="/dmcrm/APP/Comun2/MN.asp?codigo=" + valorFiltro + "&codigomn=" + tabla + "&titulomn=" + filtro;					
                    }
                    else
                    {
                        document.getElementById("frmRelacionados").src="/dmcrm/APP/Comun2/seleccionAsociados.asp?tabla=" + tabla + "&Filtro=" + filtro + "&ValorFiltro=" + valorFiltro;
                    }
                    document.getElementById("frmRelacionados").style.display="";
					redimensionarAltosPantalla();
                }
                document.getElementById("divMenuHor").style.display="none";
				if (document.getElementById("bloqueImagenes")!=null) {				
					document.getElementById("bloqueImagenes").style.display="none";				
				}	
				if (document.getElementById("bloqueArchivos")!=null) {
					document.getElementById("bloqueArchivos").style.display="none";
				}					
				if (document.getElementById("bloqueLocalizacion")!=null) {				
					document.getElementById("bloqueLocalizacion").style.display="none";				
				}									
            }
        }
        
        function paginaCargada()
        {
    //		document.getElementById("divCargando").style.display="none";
        }
        
		function quitarScroolPadre()
		{
			<%if trim(request("sinRefrescoAuto"))="" then%>
				window.parent.document.body.style.overflow="hidden";
			<%end if%>
			redimensionarAltosPantalla();

		}
		
		function redimensionarAltosPantalla()
		{
//			alert(document.getElementById("frmRelacionados").style.top);

			//var altoDoc=document.documentElement.clientHeight;
			var altoDoc=document.body.clientHeight;			
			if (altoDoc!="")
			{
				if (document.getElementById("idTab_container")!=null)
				{
					var altoDivPorcen=document.getElementById("idTab_container").style.height;
					altoDivPorcen=altoDivPorcen.replace("%","");			

					var altoDivPX=((altoDivPorcen * altoDoc )/ 100);
					document.getElementById("idTab_container").style.height=altoDivPX + "px";
				}

				if (document.getElementById("frmRelacionados")!=null)
				{
					var altoFramePorcen=document.getElementById("frmRelacionados").style.height;
					altoFramePorcen=altoFramePorcen.replace("%","");

					var altoFramePX=((altoFramePorcen * altoDoc )/ 100);
					document.getElementById("frmRelacionados").style.height=altoFramePX + "px";
				}
				
			}
		}		
		

     </script>
     <style type="text/css">
body { margin:0; padding:0;}
.jCalendar td { font-size:11px; }
</style> 

           
<%

'PARAMETROS DE LA PAGINA
    if trim(codigo)="" then codigo = request("CODIGO")
    if trim(tabla)="" then tabla = request("TABLA")
    if trim(posCodigo)="" then posCodigo = request("posCodigo")
    if trim(retorno)="" then retorno = request("RET")	'PAGINA DE RETORNO DEL MANTENIMIENTO, SI NO SE PONE, VUELVE A SELECCION.ASP		

	'el caracter & para retorno:
	retorno = replace(retorno,"&",server.URLEncode("&"))

    if trim(Filtro)="" then Filtro = request("FILTRO") 'SI ME ENVIAN CAMPO DE FILTRO, NO CREO CAJA DE TEXTO PARA EL.
    if trim(ValorFiltro)="" then ValorFiltro = request("VALORFILTRO")
    if trim(Filtro2)="" then Filtro2 = request("FILTRO2")
    if trim(ValorFiltro2)="" then ValorFiltro2 = request("VALORFILTRO2") 'SI ME ENVIAN CAMPO DE FILTRO, NO CREO CAJA DE TEXTO PARA EL.
		
    if trim(NumMax)="" then NumMax = request("NumMax")
    if trim(NumMin)="" then NumMin = request("NumMin")
	
    if trim(soloCampos)="" then soloCampos = request("soloCampos")					



if trim(request("modificado"))="1" then%>
	 <script type="text/javascript">
        $(document).ready(function() {
			$.jGrowl("<%=objIdiomaGeneral.getIdioma("GuardadoCorrectamente_Texto1")%>");
        });

     </script>
<%end if


%>
<!--#include file="FuncionesMaestra.inc"  -->
<!--#include file="Validaciones.asp"  -->
<%



function relaciones1N(tabla)
	denominacionTabla = ""
	Redim arrRelaciones(-1)
	Redim strutRelacion(3) 
	query = "SELECT codTabla,tabla, campo, (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario,'1' as ordenado FROM maestra WHERE tabla = '" & tabla & "G' "
	query = query & " union "
	query = query & " SELECT codTabla,tabla, campo, (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario,'2' as ordenado FROM maestra WHERE tablaExt = '" & tabla & "' AND ClaveExt=1  "
	query = query & " order by ordenado,codTabla "
'response.Write(query&"<br />")
	if trim(request("conex"))<>"" then  
		Set rsResul = connMaestra.AbrirRecordset(query,3,1)
	else
		Set rsResul = connCRM.AbrirRecordset(query,3,1)
	end if
	
	i = 0
	while not rsResul.eof
		redim preserve arrRelaciones(i)
		if trim(denominacionTabla)="" then
			denominacionTabla=rsResul("nomUsuario")
			MostrarEnTrabla=True			
		else
			denominacionTabla=""

			query = "SELECT (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomUsuario FROM maestra WHERE Tabla='" & rsResul("tabla") & "G'"
			if trim(request("conex"))<>"" then  
				set rsNombre = connMaestra.abrirRecordSet(query,3,1)
			else
				set rsNombre = connCRM.abrirRecordSet(query,3,1)
			end if			
			
			if not rsNombre.eof then
				denominacionTabla = rsNombre("nomUsuario")
			end if
			rsNombre.cerrar
			Set rsNombre = nothing

			MostrarEnTrabla=False
			query = "SELECT MostrarEnTablaExt FROM maestra WHERE Tabla='" &  rsResul("tabla") & "' And ClaveExt=1  And TablaExt='"&tabla&"'  "
			if trim(request("conex"))<>"" then  
				set rsMostrar = connMaestra.abrirRecordSet(query,3,1)	
			else
				set rsMostrar = connCRM.abrirRecordSet(query,3,1)	
			end if			

			if not rsMostrar.eof then
				MostrarEnTrabla = rsMostrar("MostrarEnTablaExt")
			end if
			rsMostrar.cerrar
			Set rsMostrar = nothing		

			
		end if

		
		strutRelacion(0) = denominacionTabla
		strutRelacion(1) = rsResul("tabla")
		strutRelacion(2) = rsResul("campo")
		strutRelacion(3) = MostrarEnTrabla
		arrRelaciones(i) = strutRelacion		
		i = i + 1
		rsResul.moveNext()
	wend	
	rsResul.cerrar()
	Set rsResul = nothing
	relaciones1N = arrRelaciones

end function

function relacionesMN(tabla)
	Redim arrRelaciones(-1)
	Redim strutRelacion(1) '0 -> c�digo 1-> nombre
	query = "SELECT mamn_codigo, MAMN_TEXTO0 as mamn_textomn FROM maestra_mn WHERE mamn_tabla1 = '" & tabla & "'"
	if trim(request("conex"))<>"" then  
		Set rsResul = connMaestra.AbrirRecordset(query,3,1)
	else
		Set rsResul = connCRM.AbrirRecordset(query,3,1)
	end if	

	i = 0
	while not rsResul.eof
		redim preserve arrRelaciones(i)
		strutRelacion(0) = rsResul("mamn_codigo")
		strutRelacion(1) = rsResul("mamn_textomn")
		arrRelaciones(i) = strutRelacion		
		i = i + 1
		rsResul.moveNext()
	wend	
	rsResul.cerrar()
	Set rsResul = nothing
	relacionesMN = arrRelaciones
end function




'session(denominacion & "paramRecibidos")=request.QueryString
'response.Write(request.QueryString)
'	session(denominacion & "rutaFormulario")="/modulos/comun/formMantenimiento.asp"
'response.Write(request)
Randomize
	intLength = 10000
	intRandom = CInt((Rnd * 1000)Mod intLength) + 1


    'if lcase(tabla) <> "diseimagenes" then	
'	if request("modo") = "modificacion" then

		arrRelaciones1N = relaciones1N(tabla)
		arrRelacionesMN = relacionesMN(tabla)		
'	end if
	'end if	
	

		tieneAlgunaPestana=false
			esGeneral=0
			for i = 0 to ubound(arrRelaciones1N)
				arrRelacion = arrRelaciones1N(i)
				
				IF arrRelacion(3) then
					if esGeneral=0 then
						esGeneral=1
					else
						tieneAlgunaPestana=true
					end if

				end if
			next


		'Para pintar las pesta�as automaticas. Aparte de estar relacionada, el campo relacionado de la tabla hijo tiene que tener activado el campo MostrarEnTablaExt
		

		sqlContPest=" Select count(MPE_CODIGO) as contPest From MAESTRA_PESTANAS Where upper(MPE_TABLA)='" & trim(ucase(tabla)) & "' "
		if trim(request("conex"))<>"" then  
			Set rsContPest = connMaestra.abrirRecordSet(sqlContPest,0,1)
		else
			Set rsContPest = connCRM.abrirRecordSet(sqlContPest,0,1)
		end if		
		if not rsContPest.eof then
			if rsContPest("contPest")>0 then
				tieneAlgunaPestana=true
			end if
		end if
		rsContPest.cerrar()
		Set rsContPest = nothing
		
		
	'''Obtenemos la lista de formatos disponibles
	sqlFormatos="Select FOR_CODIGO,upper(FOR_DESCRIPCION) as nomExt,FOR_ICONO From FORMATOS Order by FOR_DESCRIPCION "
    Set rsFormatos= connCRM.AbrirRecordset(sqlFormatos,3,1)
	hayFormatos=false
	if not rsFormatos.EOF then
		hayFormatos=true
	end if
	'''''''''''''''''''''''''''		



 if request("modo")="alta" then %>
	<%' if trim(retorno)<>""  then 
		rutaRetornoModif=""
		
		 if trim(retornoGuardar)<>"" then
			 retornoModif=retornoGuardar
		 else
			retornoModif=server.URLEncode("/dmcrm/APP/Comun2/formMantenimiento.asp?tabla=" & tabla & "&filtro=" & request("filtro") & "&valorFiltro=" & request("valorFiltro")  & "&filtro2=" & request("filtro2") & "&valorFiltro2=" & request("valorFiltro2")  & "&modo=modificacion&CODIGO=-1&posCodigo="& posCodigo & "&ret=" & retorno)
		end if		

		
		
	'end if
	'if  trim(soloCampos)="1" then response.Write("&recarga=parent") end if
	%>
    
    <%

	if trim(request("insertarContacto"))="1" then
		retornoCarga="&recarga=soloCerrar"
		retornoModif=""
	elseif  trim(request("refrescarReg"))="1" then
		retornoCarga="&recarga=parent"
		retornoModif=""		
	else
		if  not tieneAlgunaPestana then
			retornoCarga= "&recarga=parent"
			retornoModif=""'server.URLEncode("/dmcrm/APP/Comun2/formMantenimiento.asp?tabla=" & tabla & "&filtro=" & request("filtro") & "&valorFiltro=" & request("valorFiltro")  & "&filtro2=" & request("filtro2") & "&valorFiltro2=" & request("valorFiltro2")  & "&modo=modificacion&CODIGO=-1&posCodigo="& posCodigo & "&ret=" & retorno)
		end if
	end if%>
    <form id="formAlta" name="formAlta" method="post"  action="/dmcrm/app/comun2/inserciones.asp?tabla=<%=tabla%>&conex=<%=request("conex")%>&insertarContacto=<%=request("insertarContacto")%>&refrescarReg=<%=request("refrescarReg")%><%=retornoCarga%>&ret=<%=retornoModif%>" >


	<%		listaLocalizacion="" %>

    <ul class="tabs">
         <li ><a href="#tab"><%=tabla%></a></li>
    </ul>	
    <div class="tab_container" id="idTab_container" style="height:92%;   overflow-y:auto; overflow-x:hidden; "><!--92%-->

            <div id="tab" class="tab_content"  >
                 <div id="divMenuHor"   >
							<!-- #include virtual="/dmcrm/APP/comun2/formMantenimientoCampos.asp"  -->                    
							<div style="clear:both; float:left; text-align:right; width:100%; padding-right:20px; padding-bottom:20px;">
	                             <font color="#000000" face="verdana" size="1"><i><%=objIdiomaGeneral.getIdioma("CamposObligatorios_Texto1")%></i></font>
                                 
                            	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton" id="B1" name="B1" onClick="ValidaFormularioAlta(formAlta);"  />                                
                             	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonCancelar_Texto1")%>"  class="boton" onClick="javascript:parent.$.fancybox.close();" />   

                             </div>
				</div> 
                
				<%
                if trim(listaLocalizacion)<>"" then%>
                    <script type="text/javascript" >
                        $(document).ready(function() {
                            $('#divMenuHor').css('width','78%');
                            $('#divMenuHor').css('float','left');							
                        });            
                    </script>                  
                <%end if%>      
                <%if trim(listaLocalizacion)<>"" then
                    listaLocalizacion=listaLocalizacion & "</ul>"%>
                    <div id="bloqueLocalizacion" class="bloqueAdjuntos">
                        <span class="bloqueAdjuntosTitulo" >&nbsp;&nbsp;&nbsp;Geo-localizaciones:</span>                        
                        <%=listaLocalizacion%>
                    </div>
                    <input type="hidden" id="hidGeoLocUso" name="hidGeoLocUso" value="" >
                    <div class="bloqueAdjuntos" style="height:20px;" ></div>
                <%end if%>                  
            
		    </div> 
	</div>        
    
    </form>
    
	
	<!-- Funciones de validacion-->
	<script language="Javascript">
	
	function ValidaFormularioAlta(theForm)
	{
	  //Escribimos las funciones de validacion de los campos memo
	  <%=strValidacionCampoMemo%>	  		
	  //Escribimos las funciones de validacion de los campos obligatorios
	  <%=strValidacionObligatorio%>
	  //Escribimos las funciones de validacion de los campos de texto
	  <%=strValidacionCampoDeTexto%>
	  //Escribimos las funciones de validacion de los campos de email
	  <%=strValidacionCampoIP%>
	  //Escribimos las funciones de validacion de los campos de email
	  <%=strValidacionCampoEmail%>
	  //Escribimos las funciones de validacion de los campos num�ricos
	  <%=strValidacionCampoNumerico%> 
	  //Escribimos las funciones de validacion de los campos num�ricos decimales
	  <%=strValidacionCampoNumericoDecimal%> 
	  //Escribimos las funciones de validacion de los campos fecha
	  <%=strValidacionCampoFecha%> 
	  //Escribimos las funciones de validacion de los campos hora
	  <%=strValidacionCampoHora%>   
	  //si se ha validado todo submitimos el formulario con los datos
	  document.formAlta.submit();	   
	} 
	</script>    
    
<% else 'es modificacion %>	

<%' if trim(retorno)<>""  then 

	 if trim(retornoGuardar)<>"" then
		 retornoModif=retornoGuardar
	 else
		retornoModif=server.URLEncode("/dmcrm/APP/Comun2/formMantenimiento.asp?tabla=" & tabla & "&filtro=" & request("filtro") & "&valorFiltro=" & request("valorFiltro")  & "&filtro2=" & request("filtro2") & "&valorFiltro2=" & request("valorFiltro2") & "&modo=modificacion&CODIGO="&codigo&"&posCodigo="& posCodigo & "&ret=" & retorno)
	end if
	
    
	
	quitarRelacionados1N=False
	quitarRelacionadosMN=False	
	
	sqlPermisosMaestra = " Select CodTabla,Tabla,OcultarOpciones,PaginaAmedidaModificar From maestra Where Tabla='" & ucase(tabla) & "G' "
	opciones=""
	if trim(request("conex"))<>"" then  
		Set rsPermisosMaestra = connMaestra.abrirRecordSet(sqlPermisosMaestra,3,1)		
	else
		Set rsPermisosMaestra = connCRM.abrirRecordSet(sqlPermisosMaestra,3,1)
	end if	

	if not rsPermisosMaestra.eof then
		opciones=rsPermisosMaestra("OcultarOpciones")
		if trim(opciones)<>"" then
			cadaOpcion=split(opciones,",")
			for contOpcion = 0 to ubound(cadaOpcion)
				  Select Case trim(ucase(cadaOpcion(contOpcion)))
				      Case "R" 
					  		quitarRelacionados1N=True
						Case "M"
							quitarRelacionadosMN=True
				  End Select

			next				
		end if

	end if

	rsPermisosMaestra.cerrar()
	Set rsPermisosMaestra = nothing	
'	response.Write(request.ServerVariables("SERVER_PROTOCOL"))
	%>
    
		<%if  trim(soloCampos)="1" then 
			retornoCarga= "&recarga=soloCerrar"
			retornoModif=""
		elseif not tieneAlgunaPestana then
			retornoCarga= "&recarga=parent"
			retornoModif=""
		end if
        
        


	%>
    <form id="formEdtar" name="formEdtar" method="post"  action="/dmcrm/app/comun2/modificaciones.asp?posCodigo=<%=posCodigo%>&tabla=<%=tabla%>&modo=modificacion&conex=<%=request("conex")%>&codigo=<%=codigo%>&nomForm=formClientes<%=retornoCarga%>&ret=<%=retornoModif%>" >
    
<%' hasta aqui%>    
	
   <% if not trim(soloCampos)<>"" then%>
    <style type="text/css">
		.cEmergenteSubida { float:right;  color:#555; position:relative; top:0px; right:0px; text-decoration:none;  }
		.cEmergenteSubida:hover {    filter: alpha(opacity=70);  opacity: .7;}		
		.cEmergenteSubida img { position:relative; top:5px; }	
		
	</style> 
    <div style="width:100%; clear:both; float:left; text-align:left; padding:0px; margin:0 0 15px 0; border-bottom: solid 2px #f60">
    	<div style="margin:0px; padding:0px; width:80%; float:left; text-align:left;">

           
            <%
            
	            clave=ClaveTabla(Tabla, TipoDeClave)
    	        sql="select * from " & tabla & " WHERE " & clave & "=" 
        	    if TipoDeClave="T" or TipoDeClave="E" or TipoDeClave="IP" then
            	  sql=sql & "'" & codigo & "'"
	            else
    	          sql=sql & codigo
        	    end if

				if trim(request("conex"))<>"" then  
	        	     Set rsDatosMaestra = connMaestra.abrirRecordSet(sql,3,1)	
				else
	        	     Set rsDatosMaestra = connCRM.abrirRecordSet(sql,3,1)
				end if
				


            	if not rsDatosMaestra.eof then
	                sql=" select campo,visiblesel, (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomusuario,tipo,claveExt,tablaExt,campoExt1 as CampoExt "
    	          sql=sql&" from maestra "
        	      sql=sql& " where tabla='"&tabla&"'  "
            	  sql=sql & " and  visiblesel=1 and Tipo<>'M' "
	              sql=sql & " order by orden "
					if trim(request("conex"))<>"" then  
	    	       		  Set rsCamposMaestra = connMaestra.abrirRecordSet(sql,3,1)
					else
    			          Set rsCamposMaestra = connCRM.abrirRecordSet(sql,3,1)
					end if				  

				if tieneAlgunaPestana And ( not quitarRelacionados1N or not quitarRelacionadosMN ) then %>
				 <ul id="idListadico" class="listadico">  <%
				  
        	      while not rsCamposMaestra.eof 
            	        %><li><span class="agrupacionEtiquetas"><span class="cadaEtiqueta"><%=rsCamposMaestra("nomusuario") & ": "%></span><%
						escribirContenido=""
						if not rsCamposMaestra("claveExt") then
							if rsCamposMaestra("tipo")="B" then
								if 	rsDatosMaestra("" & rsCamposMaestra("campo")) then
									escribirContenido=objIdiomaGeneral.getIdioma("ValorAfirmativo")
								else
									escribirContenido=objIdiomaGeneral.getIdioma("ValorNegativo")							
								end if							
							elseif rsCamposMaestra("tipo")="XA" then
								if trim(rsDatosMaestra("" & rsCamposMaestra("campo")))<>"" then

									tamFichero=tamannoArchivo(server.MapPath("/dmcrm/APP/UsuariosFtp/archivos/" & rsDatosMaestra("" & rsCamposMaestra("campo"))))								
								
									indexUltimoPunto=instr(StrReverse(rsDatosMaestra("" & rsCamposMaestra("campo"))),".")
									nomExtension=right(rsDatosMaestra("" & rsCamposMaestra("campo")),indexUltimoPunto-1)
		
									if hayFormatos then
										rsFormatos.filter = " nomExt='" & ucase(nomExtension) & "' "
										icono=rsFormatos("FOR_ICONO")
									end if
									escribirContenido = "<a target=""__blank"" class=""enlaceAdjuntoElem"" href=""" & "/dmcrm/APP/UsuariosFtp/archivos/" & rsDatosMaestra("" & rsCamposMaestra("campo")) & """><img src=""/dmcrm/APP/UsuariosFtp/imagenes/" & icono & """ >" & tamFichero & "</a>"						
								else
									escribirContenido="&nbsp;"
								end if								
							elseif rsCamposMaestra("tipo")="XI" then
								if trim(rsDatosMaestra("" & rsCamposMaestra("campo")))<>"" then
									response.Write("<a class=""cEmergenteFoto"" href=""/dmcrm/APP/UsuariosFtp/imagenes/" & rsDatosMaestra("" & rsCamposMaestra("campo")) & """>" & thumbnail("/dmcrm/APP/UsuariosFtp/imagenes/" & rsDatosMaestra("" & rsCamposMaestra("campo")),0,15,"") & "</a>")
								else
									escribirContenido="&nbsp;"
								end if	
							else							
								escribirContenido=rsDatosMaestra("" & rsCamposMaestra("campo"))
							end if	
						else

						'''''''''''''''''''''
						'claveExterna=ClaveTabla(rsCamposMaestra("tablaExt"), TipoDeClaveExterna)
						tablaExtAux=""
						campoExtAux=""
						claveExternaAux=""
						FiltroExtAux=""
						ValorFiltroExtAux=""

						if ClaveExt(Tabla,rsCamposMaestra("campo"),tablaExtAux,campoExtAux,claveExternaAux,FiltroExtAux,ValorFiltroExtAux) then								
							if trim(rsDatosMaestra("" & rsCamposMaestra("campo")))<>"" then
								if instr(campoExtAux,"&")>0 then
					    	        sqlExterna="select * from " & tablaExtAux 
									sqlExterna=sqlExterna & " WHERE " & claveExternaAux & "=" & rsDatosMaestra("" & rsCamposMaestra("campo"))								
								else 
									arrCampDesc=split(campoExtAux,".")
									CampDesc=campoExtAux
									if ubound(arrCampDesc)>0 then
										CampDesc=arrCampDesc(1)
									end if
									
					    	        sqlExterna="select " & claveExternaAux 
									sqlExterna=sqlExterna & ", (CASE WHEN CAST(IDIOMAS_DESCRIPCIONES.IDID_DESCRIPCION AS varchar(max))='' Or IDIOMAS_DESCRIPCIONES.IDID_DESCRIPCION is null THEN " & tablaExtAux & "." & CampDesc & " ELSE CAST(IDIOMAS_DESCRIPCIONES.IDID_DESCRIPCION AS varchar(max)) END)  as " & CampDesc
									sqlExterna=sqlExterna & " from " & tablaExtAux 
									sqlExterna=sqlExterna & " Left Join IDIOMAS_DESCRIPCIONES on (" & tablaExtAux & "." & claveExternaAux & "=IDIOMAS_DESCRIPCIONES.IDID_INDICE  And IDIOMAS_DESCRIPCIONES.IDID_TABLA='" & tablaExtAux & "' And IDIOMAS_DESCRIPCIONES.IDID_CAMPO='" & CampDesc & "' And IDIOMAS_DESCRIPCIONES.IDID_SUIDIOMA='" & Idioma & "') "				
									sqlExterna=sqlExterna & " WHERE " & claveExternaAux & "=" & rsDatosMaestra("" & rsCamposMaestra("campo"))								
								end if


								if trim(request("conex"))<>"" then  
									Set rsDatosClaveExterna = connMaestra.abrirRecordSet(sqlExterna,3,1)
								else
									Set rsDatosClaveExterna = connCRM.abrirRecordSet(sqlExterna,3,1)
								end if


								if not rsDatosClaveExterna.eof then
'									if tablaExtAux="TIPOS_PRODUCTOS" then
'										response.Write(campoExtAux)
'										response.End()
'									end if

									if instr(campoExtAux,"&")>0 then
										arrAlias=split(campoExtAux," ")
										if instr(arrAlias(0),"&")>0 then
											arrAlias2=split(arrAlias(0)," ")				
											alias=arrAlias2(0)
										else
											alias=arrAlias(0)	
										end if
										escribirContenido=rsDatosClaveExterna("" & replace(alias,tablaExtAux & ".",""))										
									else
										escribirContenido=rsDatosClaveExterna("" & replace(campoExtAux,tablaExtAux & ".",""))											
									end if
			

								end if

								rsDatosClaveExterna.cerrar()
    				        	Set rsDatosClaveExterna = nothing		
							end if	'end de if trim(rsDatosMaestra("" & rsCamposMaestra("campo")))<>"" then	
						end if	'end de if ClaveExt(Tabla,rsCamposMaestra("campo"),tablaExtAux,campoExtAux,claveExternaAux,FiltroExtAux,ValorFiltroExtAux) then									

						''''''''''''	
						end if	'end de if not rsCamposMaestra("claveExt") then
                	    %><span class="cadaContenido"><%=escribirContenido%></span></span></li><%					
	                rsCamposMaestra.movenext
    	          wend 'end de  while not rsCamposMaestra.eof 
				  
        	      rsCamposMaestra.cerrar()
            	  Set rsCamposMaestra = nothing	
				  
			  

				  
				   %></ul><%
					end if 'end de if tieneAlgunaPestana And ( not quitarRelacionados1N or not quitarRelacionadosMN ) then		  

              
            ''mostramos los campos ocultos y filtros.%>
			<ul id="idListadicoOcultos" class="listadicoOcultos">				
                  <% sql=" Select campo, (CASE WHEN NomUsuario_" & Idioma & "='' Or NomUsuario_" & Idioma & " is null THEN NomUsuario_" & DEFAULT_LANGUAGE & " ELSE NomUsuario_" & Idioma & " END) as nomusuario "
				  	 sql=sql & " From maestra "
				     sql=sql & " Where lower(Tabla)='" & lcase(tabla) & "' And (Tipo='FO' Or Tipo='FM' Or Clave=1 ) "
				  	 sql=sql & " Order by orden "	

					if trim(request("conex"))<>"" then  
	    	       		  Set rsCamposMaestraOcultos = connMaestra.abrirRecordSet(sql,3,1)
					else
    			          Set rsCamposMaestraOcultos = connCRM.abrirRecordSet(sql,3,1)
					end if				  


        	        while not rsCamposMaestraOcultos.eof%>
			  	        <li>
    	            		<span class="agrupacionEtiquetas">
        	            		<span class="cadaEtiqueta"><%=rsCamposMaestraOcultos("nomusuario") & ": "%></span>
		    	                <span class="cadaContenido"><%=rsDatosMaestra("" & rsCamposMaestraOcultos("campo"))%></span>
    	        	        </span>
        	        	</li>		

						<%rsCamposMaestraOcultos.movenext
    		         wend
        	      	 rsCamposMaestraOcultos.cerrar()
            	     Set rsCamposMaestraOcultos = nothing%>    
			</ul>
			<%''mostramos los campos ocultos y filtros.%>
<%
			  
			  
        	      end if 'end de if not rsDatosMaestra.eof then
				  
	               rsDatosMaestra.cerrar()
    	          Set rsDatosMaestra = nothing						  
				
             %> 
            
            
        </div>
    	<div style="margin:0px; padding:0px; width:18%; float:right; text-align:left;">        
			<!-- #include virtual="/dmcrm/APP/Comun2/cargarNavegadorCodigos.asp"-->
	    </div>
    </div>


<%' hasta aqui%>    


	<%if trim(retornoGuardar)<>"" then
			quitarRelacionados1N=true
			quitarRelacionadosMN=true
	  end if %>

    <ul class="tabs" style="z-index:1;">
    <%

		if not quitarRelacionados1N then
			for i = 0 to ubound(arrRelaciones1N)
				arrRelacion = arrRelaciones1N(i)
'				response.Write("<br/>"&"@"&arrRelacion(0)&"<br/>")
				IF arrRelacion(3) then
				%>
	             <li onClick="javascript:cargarPestannas('1N','<%=arrRelacion(1)%>','<%=arrRelacion(2)%>','<%=codigo%>','<%=i%>','');" ><a href="#tab"><%=arrRelacion(0)%></a></li>
    	        <%
				end if
			next	
		else
			'Solo cargar pesta�a General
				arrRelacion = arrRelaciones1N(0)
				IF arrRelacion(3) then
				%>
	             <li onClick="javascript:cargarPestannas('1N','<%=arrRelacion(1)%>','<%=arrRelacion(2)%>','<%=codigo%>','<%=i%>','');" ><a href="#tab"><%=arrRelacion(0)%></a></li>
    	        <%
				end if				
		end if	
		conAnterior=i
		if not quitarRelacionadosMN then
			for i = 0 to ubound(arrRelacionesMN)
				arrRelacionMN = arrRelacionesMN(i)
'				IF arrRelacion(3) then
				%>
	             <li onClick="javascript:cargarPestannas('MN','<%=arrRelacionMN(0)%>','<%=arrRelacionMN(1)%>','<%=codigo%>','<%=conAnterior + i%>','');" ><a href="#tab"><%=arrRelacionMN(1)%></a></li>
    	        <%
'				end if
			next	
		end if
		
		sqlPestAMedida=" Select MPE_CODIGO,MPE_TABLA,MPE_MENUENLACEPERMISOS,MPE_PESTANA_" & Idioma & " as MPE_PESTANA,MPE_ENLACE,MPE_ENLACEPESTANA From MAESTRA_PESTANAS Where upper(MPE_TABLA)='" & trim(ucase(tabla)) & "' "
		if trim(request("conex"))<>"" then  
			Set rsPestAMedida = connMaestra.abrirRecordSet(sqlPestAMedida,0,1)
		else
			Set rsPestAMedida = connCRM.abrirRecordSet(sqlPestAMedida,0,1)
		end if		

		'conAnterior=i
		while not rsPestAMedida.eof
			conPantPest=rsPestAMedida("MPE_MENUENLACEPERMISOS")
			enlace=rsPestAMedida("MPE_ENLACE")
			nomPest=rsPestAMedida("MPE_PESTANA")

			pintaPestAMedida=true
			if not isnull(conPantPest) then
				if trim(conPantPest)<>"" then
					'Comprbamos que el usuario tenga permisos para ver la pesta�a
					sqlPermPest=" Select count(MENA_CODIGO) as contPer From APP_MENUS_ANALISTAS Where MENA_SUANALISTA=" & session("codigoUsuario") & " And MENA_SUMENU=" & conPantPest
					if trim(request("conex"))<>"" then  
						Set rsPermPest = connMaestra.abrirRecordSet(sqlPermPest,3,1)
					else
						Set rsPermPest = connCRM.abrirRecordSet(sqlPermPest,3,1)
					end if			
					if rsPermPest.eof then
						pintaPestAMedida=false
					else
						if isnull(rsPermPest("contPer")) then
							pintaPestAMedida=false
						elseif rsPermPest("contPer")=0 then
							pintaPestAMedida=false							
						end if
					end if	
					rsPermPest.cerrar
					Set rsPermPest = nothing								
					
				end if 'end de if trim(codPest)<>"" then
			end if	'end de if not isnull(codPest) then								
				
			if pintaPestAMedida then	
				'Calculamos, si viene un MPE_ENLACEPESTANA no vac�o, lo que debe acompa�ar al nombre
				strTextoMPE_ENLACEPESTANA=""
				if rsPestAMedida("MPE_ENLACEPESTANA")<>"" then
					strUrlMPE_ENLACEPESTANA= pathlocal & "app/comun2Pestanas/" & rsPestAMedida("MPE_ENLACEPESTANA") & "?codigoRegistro=" & codigo
					
'					strTextoMPE_ENLACEPESTANA=getCodigo(strUrlMPE_ENLACEPESTANA)

					'Si el contador de elementos nos devuelve -1, quitamos el enlace para que nos se muestra la pantalla
					if strTextoMPE_ENLACEPESTANA="-1" then
						enlace=""
					end if
				end if 
				if trim(enlace)<>"" then
					%><li onClick="javascript:cargarPestannas('AM','','','<%=codigo%>','-1','<%=enlace%>');" ><a href="#tab"><%=nomPest%> <span id="<%=nomPest%>"><%=strTextoMPE_ENLACEPESTANA%></span></a></li><%			
				end if	
			end if	

			i=i+1
			rsPestAMedida.movenext
		wend
		rsPestAMedida.cerrar
		Set rsPestAMedida = nothing
		
		listaImagenes=""
		listaArchivos=""
		listaLocalizacion=""
		
	%>

    </ul>
	
    <div class="tab_container" id="idTab_container"  style="height:78%;     overflow-y:auto; overflow-x:hidden; "> <!--height:92%;--> <!--height:740px;-->
            <div id="tab" class="tab_content" >
                <div id="divMenuHor"  > <!--style=":100%; float:left;  " border-right:dashed 1px #555;-->
							<!-- #include virtual="/dmcrm/APP/comun2/formMantenimientoCamposEditar.asp"  -->                    
							<div style="clear:both; float:left; text-align:right; width:100%; padding-right:20px; padding-bottom:20px; padding-top:20px;">
	                             <font color="#000000" face="verdana" size="1"><i><%=objIdiomaGeneral.getIdioma("CamposObligatorios_Texto1")%></i></font>                            
                            	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton" id="B1" name="B1" onClick="ValidaFormularioModificacion(formEdtar);"  />
                             	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonCancelar_Texto1")%>" class="boton"  onClick="javascript:parent.$.fancybox.close();" />   
                             </div>
				</div> 
                <%
				
				
				if trim(listaImagenes)<>"" Or trim(listaArchivos)<>"" Or trim(listaLocalizacion)<>""  then%>
					<script type="text/javascript" >
						$(document).ready(function() {
							$('#divMenuHor').css('width','78%');
							$('#divMenuHor').css('float','left');							
						});            
                    </script>                  
				<%end if%>      
				<%if trim(listaLocalizacion)<>"" then
                    listaLocalizacion=listaLocalizacion & "</ul>"%>
                    <div id="bloqueLocalizacion" class="bloqueAdjuntos">
                        <span class="bloqueAdjuntosTitulo" >&nbsp;&nbsp;&nbsp;Geo-localizaciones:</span>                        
                        <%=listaLocalizacion%>
                    </div>
					<input type="hidden" id="hidGeoLocUso" name="hidGeoLocUso" value="" >
                    <div class="bloqueAdjuntos" style="height:20px;" ></div>
                <%end if%>                  
                <%if trim(listaImagenes)<>"" then
					listaImagenes=listaImagenes & "</ul>"%>
                    <div id="bloqueImagenes" class="bloqueAdjuntos">
                        <!--<span class="bloqueAdjuntosTitulo" >&nbsp;&nbsp;<img src="/dmcrm/APP/Comun2/imagenes/m0.gif" />&nbsp;<span style="position:relative; top:-5px;">Imagenes:</span></span>-->
                        <span class="bloqueAdjuntosTitulo" >&nbsp;&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("formMant_Texto7")%></span>                        
                        <%=listaImagenes%>
                    </div>
                    <div class="bloqueAdjuntos" style="height:20px;" ></div>
				<%end if%>
                <%if trim(listaArchivos)<>"" then
					listaArchivos=listaArchivos & "</ul>"%>
                    <div id="bloqueArchivos" class="bloqueAdjuntos" >
                        <!--<span class="bloqueAdjuntosTitulo" >&nbsp;&nbsp;<img src="/dmcrm/APP/Comun2/imagenes/a0.gif" />&nbsp;<span style="position:relative; top:-5px;">Archivos:</span></span>-->
                        <span class="bloqueAdjuntosTitulo" >&nbsp;&nbsp;&nbsp;<%=objIdiomaGeneral.getIdioma("formMant_Texto7")%></span>                        
                        <%=listaArchivos%>				
                    </div>                
				<%end if%>
            
            	<!--El frame solo ser� visible para el resto de pesta�as -->
	    	    <iframe id="frmRelacionados" name="frmRelacionados" frameborder="0"    style="width:100%; height:73%; display:none;red; overflow-x:hidden; overflow-y:auto;" ></iframe><!--height:92%;--><!--height:700px;-->
		    </div> 
	</div>    
    </form>
    
<% else %>    
    <div class="tab_container" id="idTab_container"  style="height:78%; border-top:solid 1px gray;   overflow-y:auto; overflow-x:hidden; "> <!--height:92%; height:560px-->
            <div id="tab" class="tab_content" >
                <div id="divMenuHor"    >
							<!-- #include virtual="/dmcrm/APP/comun2/formMantenimientoCamposEditar.asp"  -->                    
							<div style="clear:both; float:left; text-align:right; width:100%;">
	                             <font color="#000000" face="verdana" size="1"><i><%=objIdiomaGeneral.getIdioma("CamposObligatorios_Texto1")%></i></font>                            
                            	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonGuardar_Texto1")%>" class="boton" id="B1" name="B1" onClick="ValidaFormularioModificacion(formEdtar);"  />
                             	<input type="button" value="<%=objIdiomaGeneral.getIdioma("BotonCancelar_Texto1")%>" class="boton"  onClick="javascript:parent.$.fancybox.close();" />   
                             </div>

				</div> 
            	<!--El frame solo ser� visible para el resto de pesta�as -->
	    	    <iframe id="frmRelacionados" name="frmRelacionados" frameborder="0"    style="width:100%; height:73%; display:none; overflow-x:hidden; overflow-y:auto;" ></iframe><!--height:92%;-->
		    </div> 
	</div>        
    
   <% end if%>    
    
	<!-- Funciones de validacion-->
	<script language="Javascript"> 
	function ValidaFormularioModificacion(theForm)
	{
	  //Escribimos las funciones de validacion de los campos memo
	  <%=strValidacionCampoMemo%>		
	  //Escribimos las funciones de validacion de los campos obligatorios
	  <%=strValidacionObligatorio%>
	  //Escribimos las funciones de validacion de los campos de texto
	  <%=strValidacionCampoDeTexto%>
	  //Escribimos las funciones de validacion de los campos de IP
	  <%=strValidacionCampoIP%>
	  //Escribimos las funciones de validacion de los campos de email
	  <%=strValidacionCampoEmail%>
	  //Escribimos las funciones de validacion de los campos num�ricos
	  <%=strValidacionCampoNumerico%> 
	  //Escribimos las funciones de validacion de los campos num�ricos decimales
	  <%=strValidacionCampoNumericoDecimal%> 
	  //Escribimos las funciones de validacion de los campos fecha
	  <%=strValidacionCampoFecha%> 
	  //Escribimos las funciones de validacion de los campos hora
	  <%=strValidacionCampoHora%> 

	  //si se ha validado todo submitimos el formulario con los datos
	  document.formEdtar.submit();
	} 
	</script>    
    
<% end if %>     


<%''Cerramos la lista de formatos
rsFormatos.cerrar()
Set rsFormatos = nothing%>

