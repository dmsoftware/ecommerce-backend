<%Response.buffer=false %>

<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<%
anoActual=request("an")
if anoActual="" then anoActual=year(date)

strIdentificador="Gastos" & cstr(second(now())) & cstr(minute(now()))

'Variables
ProbabilidadPedido=100
ProbabilidadPreaprobado=90
ProbabilidadGrado1=40
ProbabilidadGrado2=20
ProbabilidadGrado3=10

GastoPrevistoMes_2007=41200
intISPEnero_2007=53500

GastoPrevistoMes_2008=54500
intISPEnero_2008=86000

GastoPrevistoMes_2009=52000
'intISPEnero_2009=135000  'cambiado el d�a 14/04/09 
intISPEnero_2009=142120 

GastoPrevistoMes_2010=52000
intISPEnero_2010=175448

GastoPrevistoMes_2011=57000
intISPEnero_2011=211000

GastoPrevistoMes_2012=60000
intISPEnero_2012=236299.34 'La realidad contando con 1000 euros en rectificadas y con 5.710,20 euros de isp mensual es 241.009 euros

GastoPrevistoMes_2013=60000
intISPEnero_2013=231021.34

intISPEnero=eval("intISPEnero_"&anoActual)
GastoPrevistoMes=eval("GastoPrevistoMes_"&anoActual)

'Si estoy sacando el listado ISP=NO
if request("isp")="no" then
    GastoPrevistoMesTotal=GastoPrevistoMes
	GastoPrevistoMes=GastoPrevistoMes-intISPEnero/12
end if

function Mes(Valor)
	select case Valor
	case 1
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_1"))
	case 2
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_2"))
	case 3
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_3"))
	case 4
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_4"))
	case 5
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_5"))
	case 6
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_6"))
	case 7
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_7"))
	case 8
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_8"))
	case 9
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_9"))
	case 10
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_10"))
	case 11
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_11"))
	case 12
		Mes=ucase(objIdiomaGeneral.getIdioma("MesesCorto_12"))
	end select   
end function

Sub FormatoEuros(valor)
    if isnull(valor) then
        response.write "0 �"
    else
        response.Write FormatNumber(valor, 2) & " �"
    end if
end sub
%>



<script language=javascript>
function recarga(){
	tmp2=document.forms.fPaginacion2.comboVerAnos;
	document.forms.fPaginacion2.comboVerAnos.value=tmp2.options[tmp2.selectedIndex].value;
	document.location='ResumenVentas.asp?isp=<%=request("isp")%>&an=' + tmp2.options[tmp2.selectedIndex].value;
	//}
}
</script>


<!-- CABECERA DE LA SELECCION -->
<%if request("isp")="no" then%>

		<h1><%=objIdioma.getIdioma("ResumenVentas_Titulo1")%></h1>
        <P><FONT SIZE=2><a href="ResumenVentas.asp"><u><%=objIdioma.getIdioma("ResumenVentas_Texto1")%></u></a></FONT></P>
        <p>
		<font face="Verdana" size=1 >[<%=objIdioma.getIdioma("ResumenVentas_Texto2") & " "%> <%FormatoEuros(GastoPrevistoMesTotal)%>]<BR>
		[<%=objIdioma.getIdioma("ResumenVentas_Texto3")%> <b><%FormatoEuros(GastoPrevistoMes)%></b>]<br>
		[<%=objIdioma.getIdioma("ResumenVentas_Texto4") & " "%> <%FormatoEuros(intISPEnero)%>]<br>
		<%=objIdioma.getIdioma("ResumenVentas_Texto5")%><br />
		</font>
        </p>       

<%else%>

		<h1><%=objIdioma.getIdioma("ResumenVentas_Titulo2")%></h1>
        <P><FONT size=2><a href="ResumenVentas.asp?isp=no"><u><%=objIdioma.getIdioma("ResumenVentas_Texto6")%></u></a></FONT></P>
        <p>
		<font face="Verdana" size=1>
		<%
		response.write "[" & objIdioma.getIdioma("ResumenVentas_Texto7") & " <b>" 
		FormatoEuros(GastoPrevistoMes) 
		response.Write "</b>]"
		%>
		<br><%=objIdioma.getIdioma("ResumenVentas_Texto8")%>
		</font>  
        </p>     

<%end if%>


<a href="ResumenVentas_bak.asp?isp=<%=request("isp")%>"><%=objIdioma.getIdioma("ResumenVentas_Texto9")%></a>
<br /><br>

<%'############################################################################################%>
<form name="fPaginacion2" method="POST" ID="Form1">
<font face="Verdana" size=2><B><%=objIdioma.getIdioma("ResumenVentas_Texto10")%>&nbsp;</b></font>
<select name="comboVerAnos" onchange="javascript:recarga();" class="combos" ID="Select1">
	<%sql="Select distinct(year(PED_FECHA)) as Ano from PEDIDOS " 		
	sql=sql & " order by year(PED_FECHA) "
	Set rstAnos= connCRM.AbrirRecordset(sql,0,1)%>
	<%while not rstAnos.eof%>
		<option value="<%=rstAnos("Ano")%>"<%if cint(anoActual) = rstAnos("Ano") then response.Write(" selected")%>><%=rstAnos("Ano")%></option>
	    <%rstAnos.movenext
	wend
    rstAnos.cerrar%>
	<option value="<%=year(date())+1%>" <%if cint(anoActual) = year(date())+1 then response.Write(" selected")%>><%=year(date())+1%></option>	
	
</select>

</form>
<%'############################################################################################%>


<%
'Seleccionamos los pedidos indicados
sql="select sum(PEDLI_PRECIOPAGADO*PEDLI_UNIDADES*100/(100+PEDLI_IVA)) AS CANTIDAD, MONTH(PED_FECHA) AS MES, ANA_NOMBRE "
sql=sql & " from PEDIDOS_LINEAS INNER JOIN PEDIDOS ON PEDIDOS.PED_CODIGO=PEDIDOS_LINEAS.PEDLI_SUPEDIDO "
sql=sql & " left join ANALISTAS ON PEDIDOS.PED_SUCOMERCIAL=ANALISTAS.ANA_CODIGO"
if anoActual<>"" and anoActual<>0 then
   sql=sql & " where year(PED_FECHA)=" & anoActual
end if
sql=sql & " GROUP BY MONTH(PED_FECHA),ANA_NOMBRE "
sql=sql & " order by MONTH(PED_FECHA),ANA_NOMBRE" 
Set rstVentas= connCRM.AbrirRecordset(sql,3,1)

'Seleccionamos los comerciales que han vendido algo en el a�o
sql="select ANA_NOMBRE from PEDIDOS left join ANALISTAS ON PEDIDOS.PED_SUCOMERCIAL=ANALISTAS.ANA_CODIGO"
if anoActual<>"" and anoActual<>0 then
   sql=sql & " where year(PED_FECHA)=" & anoActual
end if
sql=sql & " GROUP BY ANA_NOMBRE "
sql=sql & " order by ANA_NOMBRE" 
Set rstComerciales= connCRM.AbrirRecordset(sql,3,1)

'contadores
TotalMes=0
TotalMesIsp=0
totalAnoEsfuerzo=0
arrCantPedidos=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
%>

<%'OFERTAS PREAPROBADAS
'Apartado de los SEGUIMIENTOS, tipo 7
sql="select * from acciones_cliente inner join clientes on clientes.clie_codigo=acciones_cliente.acci_sucliente where acci_sutipo=7"
sql=sql & " and acci_tipo7Finalizacion=3 "	
sql=sql & " and year(acci_fecha)= " & cint(anoActual) 	 
Set rstSeg= connCRM.AbrirRecordset(sql,0,1)	

arrCantPreAprobado=array(0,0,0,0,0,0,0,0,0,0,0,0,0)	
arrCantPreAprobadoGasto=array(0,0,0,0,0,0,0,0,0,0,0,0,0)		    
while not rstSeg.eof 
''Response.Write rstSeg("clie_nombre") & " - " & rstSeg("acci_tipo6Cantidad")	& "<br>"
    mesIndice=month(rstSeg("acci_fecha"))
    'Ingresos
    arrCantPreAprobado(mesIndice)=arrCantPreAprobado(mesIndice)+rstSeg("acci_tipo6Cantidad")
	TotalPreAprobado=TotalPreAprobado+rstSeg("acci_tipo6Cantidad")
	'Gastos
	if not isnull(rstSeg("acci_tipo7Gastos")) then
		arrCantPreAprobadoGasto(mesIndice)=arrCantPreAprobadoGasto(mesIndice)+rstSeg("acci_tipo7Gastos")
		TotalPreAprobadoGasto=TotalPreAprobadoGasto+rstSeg("acci_tipo7Gastos")
	end if	
	rstSeg.movenext()
wend
	

'OFERTAS grado 1, 2 , 3
'HALLO AQUELLAS QUE TIENEN YA CREADA UN SEGUIMIENTO (TIPO 7) Y NO EST� CERRADO (bien en aprobado o no aprobado) O EN PREAPROBADO (aunque est� cerrado)
'Y AQELLLAS QUE NO TIENEN TODAV�A UN SEGUIMEINTO
sql="select TIPO7.ACCI_FECHA,TIPO6.ACCI_TIPO6CANTIDAD,TIPO6.ACCI_TIPO6ESTADOCLIENTE from acciones_cliente as tipo6,acciones_cliente as tipo7  "
sql = sql & " where TIPO6.acci_sutipo=6 and TIPO7.acci_sutipo=7" 
sql=sql & " and TIPO7.acci_sucampana=TIPO6.Acci_Sucampana "
sql=sql & " and TIPO7.acci_sucliente=TIPO6.Acci_suCliente "	
sql=sql & " and TIPO7.acci_proyecto=TIPO6.Acci_proyecto "			 	
sql=sql & " and year(TIPO7.acci_fecha)= " & cint(anoActual)
sql=sql & " and TIPO7.acci_terminada=0 "
sql=sql & " and (TIPO7.acci_tipo7Finalizacion<>3 or TIPO7.acci_tipo7Finalizacion is null)"
Set rstNC= connCRM.AbrirRecordset(sql,0,1)

arrCantGrado1=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrCantGrado2=array(0,0,0,0,0,0,0,0,0,0,0,0,0)
arrCantGrado3=array(0,0,0,0,0,0,0,0,0,0,0,0,0)


'Vamos a rellenar un contador por mes y por grado 
while not rstNC.eof
 
  Select case rstNC("ACCI_TIPO6ESTADOCLIENTE")
  
     case 3
        TotalGrado3=TotalGrado3+rstNC("acci_tipo6Cantidad")
        mesIndice=MONTH(rstNC("ACCI_FECHA"))
     	arrCantGrado3(mesIndice)=arrCantGrado3(mesIndice)+rstNC("acci_tipo6Cantidad")
     case 2
        TotalGrado2=TotalGrado2+rstNC("acci_tipo6Cantidad")
        mesIndice=MONTH(rstNC("ACCI_FECHA"))
     	arrCantGrado2(mesIndice)=arrCantGrado2(mesIndice)+rstNC("acci_tipo6Cantidad")  
     case 1	
        TotalGrado1=TotalGrado1+rstNC("acci_tipo6Cantidad")     
        mesIndice=MONTH(rstNC("ACCI_FECHA"))
     	arrCantGrado1(mesIndice)=arrCantGrado1(mesIndice)+rstNC("acci_tipo6Cantidad")
   end select	
   rstNC.movenext()
	
wend


'Hallamos el mes actual 
if cint(anoActual)=year(date) then
  mesActual=month(date) 
else
  mesActual=12
end if
 

%>

<P><font face=VERDANA size=1>
<%=objIdioma.getIdioma("ResumenVentas_Texto11") & " " & date()%><br><br></font></P>


<table cellpadding=0 cellspacing=0 border=1 widht=100%><tr>
<td>&nbsp;
  
</td>
<td>&nbsp;
  
</td>
<%for i=1 to 12%>
 <td align=right>
   <font face=VERDANA size=1><B><%=Mes(i)%></B>
 </td>
<%next%>
<td bgcolor=ffffff align=right>
  <font face=VERDANA size=1 color=000000><B><%=objIdioma.getIdioma("ResumenVentas_Texto12")%></B>
</td>
<td bgcolor=4D667D align=right>
  <font face=VERDANA size=1 color=FFFFFF><B><%=objIdioma.getIdioma("ResumenVentas_Texto12")%></B>
</td>
<td bgcolor=#9999ff align=right>
  <font face=VERDANA size=1 color=000000><B><%=objIdioma.getIdioma("ResumenVentas_Texto13")%></B>
</td>
<td bgcolor=#ffccff align=right>
  <font face=VERDANA size=1 color=000000><B>%<%=mes(mesActual)%></B>
</td>

</tr><tr>


<td bgcolor=4D667D>
  <font face=VERDANA size=1 color=FFFFFF>
  <%=objIdioma.getIdioma("ResumenVentas_Texto14")%><br><%=ProbabilidadPedido%>%
</td>
<td valign=top>
    <%'Pintamos los comerciales
    rstComerciales.movefirst
    intCont=0
    while not rstComerciales.eof
        intCont=intCont+1
        if (intCont mod 2 = 0) then%>
            <span style="display:block; background:#cdcdcd; padding:0 3px;">
        <%else %>
            <span style="display:block; background:#fff; padding:0 3px;">
        <%end if%>
        <%if isnull(rstComerciales("ana_nombre")) then
            Response.write objIdioma.getIdioma("ResumenVentas_Texto15")
        else
            response.write rstComerciales("ana_nombre")
        end if%><br />
        </span>
        <%rstComerciales.movenext
    wend%>
</td>
<%
'A�adimos los pedidos mes a mes
for i=1 to 12
     intTotalPedidosMes=0%>
	 <td align=right valign=top>
        <%rstComerciales.movefirst
        intCont=0
        while not rstComerciales.eof
            intCont=intCont+1
            if (intCont mod 2 = 0) then%>
                <span style="display:block; background:#cdcdcd; padding:0 3px;">
            <%else %>
                <span style="display:block; background:#fff; padding:0 3px;">
            <%end if
             'Filtro por el mes actual y el comercial 
             if isnull(rstComerciales("ana_nombre")) then
                rstVentas.filter="MES=" & i & " and ana_nombre=null"
             else
                rstVentas.filter="MES=" & i & " and ANA_NOMBRE='" & rstComerciales("ana_nombre") & "'"
             end if
             if not rstVentas.eof then 
                'Si el comercial es MARIAN y es enero, comprobamos si tenemos que mostrar con ISP o no
                'Si mostramos el listado ISP=NO eliminamos de los pedidos de enero int
                if request("isp")="no" and UCASE(rstVentas("ana_nombre"))="DM_MARIAN" and i=1 then
                    FormatoEuros(rstVentas("cantidad")-intISPEnero)
                    intTotalPedidosMes=intTotalPedidosMes + rstVentas("cantidad") - intISPEnero
                    totalAnoEsfuezo=totalAnoEsfuezo+rstVentas("cantidad")-intISPEnero
                else
                    FormatoEuros(rstVentas("cantidad"))
                    intTotalPedidosMes=intTotalPedidosMes + rstVentas("cantidad")
                    totalAnoEsfuezo=totalAnoEsfuezo+rstVentas("cantidad")
                end if
             else
                response.write "-"
             end if%><br /></span>
            <%rstComerciales.movenext
        wend
        arrCantPedidos(mesIndice)=intTotalPedidosMes%>
	    <%IF intTotalPedidosMes<GastoPrevistoMes THEN
	      ColorLetra="#cc0000"
	    ELSE
	      ColorLetra="#000000"
	    END IF%>
        <hr>
	    <font color=<%=ColorLetra%>><B><%FormatoEuros(intTotalPedidosMes)%></B>
	    <br>
	    <font color=<%=ColorLetra%>><%=round((intTotalPedidosMes*100)/(GastoPrevistoMes),2)%>  %
	 </td>
<%next%>
<td valign=top align=right>
    <%rstComerciales.movefirst
    intCont=0
    while not rstComerciales.eof
        intCont=intCont+1
        if (intCont mod 2 = 0) then%>
            <span style="display:block; background:#cdcdcd; padding:0 3px;">
        <%else %>
            <span style="display:block; background:#fff; padding:0 3px;">
        <%end if
        intTotalPedidosComercial=0
        'Filtro por el comercial para sumar todos los meses
        if isnull(rstComerciales("ana_nombre")) then
            rstVentas.filter="ana_nombre=null"
        else
            rstVentas.filter="ANA_NOMBRE='" & rstComerciales("ana_nombre") & "'"
        end if
        while not rstVentas.eof
            'Si mostramos el listado ISP=NO eliminamos de los pedidos de enero int
            intTotalPedidosComercial=intTotalPedidosComercial + rstVentas("cantidad")
            rstVentas.movenext
        wend
        'Si el comercial es MARIAN comprobamos si tenemos que mostrar con ISP o no
        if request("isp")="no" and UCASE(rstComerciales("ana_nombre"))="DM_MARIAN" then
            intTotalPedidosComercial=intTotalPedidosComercial - intISPEnero
        end if%>
        <b><%FormatoEuros(intTotalPedidosComercial)%></b><br /></span>
        <%rstComerciales.movenext
    wend

    TotalPedidos=totalAnoEsfuezo%>

    <hr />

    <%IF TotalPedidos<GastoPrevistoMes*12 THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font color=<%=ColorLetra%>>
  <b><%FormatoEuros(TotalPedidos)%></b>
  <br><b><%=round((TotalPedidos*100)/(GastoPrevistoMes*12),2)%>%</b>
</td>
<td align=right valign=top BGCOLOR=4D667D>
  <b><font color=ffffff>
  <%
  TotalPedidos=totalAnoEsfuezo%>
  <%FormatoEuros(totalAnoEsfuezo) %>  
  </b></font>
</td>
<td align=right valign=top bgcolor=#9999ff>
    <%IF TotalPedidos<GastoPrevistoMes*12 THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font color=<%=ColorLetra%>>
  <%FormatoEuros(TotalPedidos)%>
  <br><b><%=round((TotalPedidos*100)/(GastoPrevistoMes*12),2)%>%</b>
</td>
<td align=right valign=top bgcolor=#ffccff>
    <%IF TotalPedidos<GastoPrevistoMes*mesActual THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font color=<%=ColorLetra%>>
  <b><%=round((TotalPedidos*100)/(GastoPrevistoMes*mesActual),2)%>%</b>
</td>
</tr><tr>


<td bgcolor=4D667D>
  <font face=VERDANA size=1 color=FFFFFF>
  <%=objIdioma.getIdioma("ResumenVentas_Texto16")%><br><%=ProbabilidadPreAprobado%>%
</td>
<td>&nbsp;
    
</td>
<%
'A�adimos los pre aprobados mes a mes
for i=1 to 12%>
 <td align=right>
    <font face=VERDANA size=1><B>N: <%FormatoEuros(arrCantPreAprobado(i)) - arrCantPreAprobadoGasto(i)%></B>
    <br>
    <font face=VERDANA size=1>B: <%FormatoEuros(arrCantPreAprobado(i))%>
    <br>
    <font face=VERDANA size=1><%=round(((arrCantPreAprobado(i)- arrCantPreAprobadoGasto(i) + arrCantPedidos(i))*100)/(GastoPrevistoMes),2)%> %
 </td>
<%next%>
<td>&nbsp;
  
</td>
<td align=right valign=top  BGCOLOR=4D667D>
  <font face=VERDANA size=1 color=ffffff>
  <b>N: <%FormatoEuros(TotalPreAprobado-TotalPreAprobadoGasto)%></b><br>
  B: <%FormatoEuros(TotalPreAprobado)%>
</td>
<td align=right bgcolor=#9999ff>
    <%
    TotalHastaPreaprobados=TotalPedidos+((TotalPreAprobado-TotalPreAprobadoGasto)*ProbabilidadPreaprobado/100)
    IF TotalHastaPreaprobados<GastoPrevistoMes*12 THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <%FormatoEuros(TotalHastaPreaprobados)%>
  <br><b><%=round(((TotalHastaPreaprobados)*100)/(GastoPrevistoMes*12),2)%>%</b>
</td>
<td align=right valign=top bgcolor=#ffccff>
    <%IF TotalHastaPreaprobados<GastoPrevistoMes*mesActual THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <b><%=round(((TotalHastaPreaprobados)*100)/(GastoPrevistoMes*mesActual),2)%>%</b>
</td>
</tr><tr>


<td bgcolor=4D667D>
  <font face=VERDANA size=1 color=FFFFFF>
  <%=objIdioma.getIdioma("ResumenVentas_Texto17")%><br><%=ProbabilidadGrado1%>%
</td>
<td>&nbsp;
  
</td>
<%
'A�adimos los grados 1 mes a mes
for i=1 to 12%>
 <td align=right>
    <font face=VERDANA size=1><B><%FormatoEuros(arrCantGrado1(i))%></B>
    <br>
    <font face=VERDANA size=1><B>&nbsp;<%'=round(((arrCantGrados1(i))*100)/(GastoPrevistoMes*mesActual),2)%></B>
 </td>
<%next%>
<td>&nbsp;
  
</td>
<td align=right valign=top  BGCOLOR=4D667D>
  <font face=VERDANA size=1 color=ffffff>
  <b><%FormatoEuros(TotalGrado1)%></b>
</td>
<td align=right valign=top bgcolor=#9999ff>
    <%
    TotalHastaGrado1=TotalHastaPreaprobados+(TotalGrado1*ProbabilidadGrado1/100)
    IF TotalHastaGrado1<GastoPrevistoMes*12 THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <%
  FormatoEuros(TotalHastaGrado1)%>
  <br><b><%=round((TotalHastaGrado1*100)/(GastoPrevistoMes*12),2)%>%</b>
</td>
<td align=right valign=top bgcolor=#ffccff>
    <%IF TotalHastaGrado1<GastoPrevistoMes*mesActual THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <b><%=round((TotalHastaGrado1*100)/(GastoPrevistoMes*mesActual),2)%>%</b>
</td>
</tr><tr>


<td bgcolor=4D667D>
  <font face=VERDANA size=1 color=FFFFFF>
  <%=objIdioma.getIdioma("ResumenVentas_Texto18")%><br><%=ProbabilidadGrado2%>%
</td>
<td>&nbsp;
  
</td>
<%'A�adimos los grados 2 mes a mes
for i=1 to 12%>
 <td align=right>
    <font face=VERDANA size=1><B><%FormatoEuros(arrCantGrado2(i))%></B>
    <br>
    <font face=VERDANA size=1><B>&nbsp;<%'=round(((arrCantGrados1(i))*100)/(GastoPrevistoMes*mesActual),2)%></B>
 </td>
<%next%>
<td>&nbsp;
  
</td>
<td align=right valign=top BGCOLOR=4D667D>
  <font face=VERDANA size=1 color=ffffff>
  <b><%FormatoEuros(TotalGrado2)%></b>
</td>
<td align=right valign=top bgcolor=#9999ff>
    <%
    TotalHastaGrado2=TotalHastaGrado1+(TotalGrado2*ProbabilidadGrado2/100)
    IF TotalHastaGrado2<GastoPrevistoMes*12 THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <%
  FormatoEuros(TotalHastaGrado2)%>
  <br><b><%=round((TotalHastaGrado2*100)/(GastoPrevistoMes*12),2)%>%</b>
</td>
<td align=right valign=top bgcolor=#ffccff>
    <%IF TotalHastaGrado2<GastoPrevistoMes*mesActual THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <b><%=round((TotalHastaGrado2*100)/(GastoPrevistoMes*mesActual),2)%>%</b>
</td>
</tr><tr>


<td bgcolor=4D667D>
  <font face=VERDANA size=1 color=FFFFFF>
  <%=objIdioma.getIdioma("ResumenVentas_Texto19")%><br><%=ProbabilidadGrado3%>%
</td>
<td>&nbsp;
  
</td>
<%'A�adimos los grados 3 mes a mes
for i=1 to 12%>
 <td align=right>
    <font face=VERDANA size=1><B><%FormatoEuros(arrCantGrado3(i))%></B>
    <br>
    <font face=VERDANA size=1><B>&nbsp;<%'=round(((arrCantGrado3(i))*100)/(GastoPrevistoMes*mesActual),2)%></B>
 </td>
<%next%>
<td>&nbsp;
  
</td>
<td align=right valign=top BGCOLOR=4D667D>
  <font face=VERDANA size=1 color=ffffff>
  <b><%FormatoEuros(TotalGrado3)%></b>
</td>
<td align=right bgcolor=#9999ff>
    <%
    TotalHastaGrado3=TotalHastaGrado2+(TotalGrado3*ProbabilidadGrado3/100)
    IF TotalHastaGrado3<GastoPrevistoMes*12 THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <%
  FormatoEuros(TotalHastaGrado3)%>
  <br><b><%=round((TotalHastaGrado3*100)/(GastoPrevistoMes*12),2)%>%</b>
</td>
<td align=right valign=top bgcolor=#ffccff>
    <%IF TotalHastaGrado3<GastoPrevistoMes*mesActual THEN
      ColorLetra="#cc0000"
    ELSE
      ColorLetra="#000000"
    END IF%>
  <font face=VERDANA size=1 color=<%=ColorLetra%>>
  <b><%=round((TotalHastaGrado3*100)/(GastoPrevistoMes*mesActual),2)%>%</b>
</td>
</tr>
</table>

<%
rstVentas.cerrar
rstComerciales.cerrar %>
<%'############################################################################################%>
<%'############################################################################################%>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->