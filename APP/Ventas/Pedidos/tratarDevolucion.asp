<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!-- #include virtual="/dmcrm/conf/inc_func_asp.asp"-->

<html>
	<head>
		<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
		<link rel="stylesheet" href="/dmcrm/css/estilos.css">
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">

		<!-- #include virtual="/dmcrm/includes/jquery.asp"-->   
		<script language="Javascript">
		
		$(document).ready(function() {				   		
			$("#btnGuardarDevolucion").click(function() {
				if ($('#txtFechaDevolcion').val()=="") {
					alert("Debe introducir una fecha de resoluci�n de la devoluci�n.")
				} else {
					var expresionDecimal=/^-?[0-9]+([,\.][0-9]*)?$/;
					 if ($('#txtGastoDevolucion').val().match(expresionDecimal)) {
						document.formDevolucion.action="tratarDevolucion.asp?pd=<%=request("pd")%>&lp=<%=request("lp")%>&pl=<%=request("pl")%>&gu=1";
						document.formDevolucion.submit();	
					} else {
						alert("El coste devoluci�n no tiene un formato de n�mero correcto.");
					 }
				}
				
				
			});	
			
		});	
		
		</script>	
	
<style type="text/css">
.etiqDevol { font-size:12px;}
</style>      	
	
	</head>
	<body style="margin:0; padding:0;"  >
    
    <%
	suPedido=request("pd")
	suLinea=request("lp")
	suProducto=request("pl")


	if trim(request("gu"))="1" then
		'PEDD_COSTEDEVOLUCION
		
		valAceptado="0"
		if trim(lcase(request.Form("chkRealizadoDevolucion")))="on" then
			valAceptado="1"		
		end if

		fechaActual=date()
		if trim(request.Form("txtFechaDevolcion"))<>"" then
			fechaActual=request.Form("txtFechaDevolcion")
		end if
		
		comentario=""
		if trim(request.Form("txtComentarioDevolucion"))<>"" then
			comentario=request.Form("txtComentarioDevolucion")
		end if		
		
		costeDevoucion=0
		if isnumeric(trim(request.Form("txtGastoDevolucion"))) then
			costeDevoucion=cdbl(trim(request.Form("txtGastoDevolucion")))
		end if
		
		sql=""
		if trim(lcase(request.Form("cboEstadoDevolucion")))="0" then		
			sql=" Update PEDIDOS_DEVOLUCIONES "
			sql=sql & " Set PEDD_FECHAACEPTACION=NULL, PEDD_REALIZADO=NULL, PEDD_COMENTARIO=NULL, PEDD_ESINTERNO=NULL, PEDD_COSTEDEVOLUCION=NULL "
			sql=sql & " Where PEDD_SUPEDIDO=" & suPedido & " And PEDD_SULINEA=" & suLinea & " And PEDD_SUPRODUCTO=" & suProducto
		elseif trim(lcase(request.Form("cboEstadoDevolucion")))="1" then		
			sql=" Update PEDIDOS_DEVOLUCIONES "
			sql=sql & " Set PEDD_FECHAACEPTACION='" & FormatearFechaSQLSever(fechaActual) & "', PEDD_REALIZADO=1, PEDD_COMENTARIO='" & comentario & "', PEDD_ESINTERNO=0, PEDD_COSTEDEVOLUCION='" & replace(costeDevoucion,",",".") & "' "
			sql=sql & " Where PEDD_SUPEDIDO=" & suPedido & " And PEDD_SULINEA=" & suLinea & " And PEDD_SUPRODUCTO=" & suProducto		
		elseif trim(lcase(request.Form("cboEstadoDevolucion")))="2" then		
			sql=" Update PEDIDOS_DEVOLUCIONES "
			sql=sql & " Set PEDD_FECHAACEPTACION='" & FormatearFechaSQLSever(fechaActual) & "', PEDD_REALIZADO=0, PEDD_COMENTARIO='" & comentario & "', PEDD_ESINTERNO=0, PEDD_COSTEDEVOLUCION='" & replace(costeDevoucion,",",".") & "' "
			sql=sql & " Where PEDD_SUPEDIDO=" & suPedido & " And PEDD_SULINEA=" & suLinea & " And PEDD_SUPRODUCTO=" & suProducto		
		end if		
		
		if sql<>"" then
			connCRM.execute sql,intNumRegistros
		end if
	
		
'		response.Write("error")
		'response.Write("Pedido marcado como devuelto.<br/>")%>
		<!--<input type="button" id="btnSalirDevolucion" name="btnSalirDevolucion"  value="Salir" class="boton" onClick="javascript:parent.$.fancybox.close();" />                 -->
        <script language="javascript" type="text/javascript">
			$(document).ready(function() {
				parent.$.fancybox.close();
			});			
		</script>
         <%
	
	else 'else de if trim(request("gu"))="1" then
	
	
		estadoPedido=0
		comentario=""
		fecha=""
		sql="Select top 1 PEDD_CODIGO,PEDD_REALIZADO,convert(varchar,PEDD_FECHAACEPTACION," & obtenerFormatoFechaSql() & ") as fechaAceptacion,PEDD_COMENTARIO,(CASE WHEN PEDD_COSTEDEVOLUCION is null THEN 0 ELSE PEDD_COSTEDEVOLUCION END) as PEDD_COSTEDEVOLUCION From PEDIDOS_DEVOLUCIONES Where PEDD_SUPEDIDO=" & suPedido & " And PEDD_SULINEA=" & suLinea & " And PEDD_SUPRODUCTO=" & suProducto	& " order by PEDD_CODIGO desc "


        Set rsPedidos = connCRM.AbrirRecordset(sql,3,1)


		if not rsPedidos.eof then
			if not isnull(rsPedidos("PEDD_REALIZADO")) then
				if rsPedidos("PEDD_REALIZADO") then
					estadoPedido=1
				else
					estadoPedido=2				
				end if
			end if

			if not isnull(rsPedidos("PEDD_COMENTARIO")) then
				comentario=rsPedidos("PEDD_COMENTARIO")
			end if

			if not isnull(rsPedidos("fechaAceptacion")) then
				fecha=rsPedidos("fechaAceptacion")			
			end if
			
			if trim(fecha)="" then
				fecha=date()
			end if
			
			costeDevolucion=0
			if not isnull(rsPedidos("PEDD_COSTEDEVOLUCION")) then
				costeDevolucion=rsPedidos("PEDD_COSTEDEVOLUCION")
			end if					
			
		end if
        rsPedidos.cerrar
        Set rsPedidos = nothing				
		
	
		%>

		<form id="formDevolucion" name="formDevolucion" runat="server" method="post"  action="tratarDevolucion.asp?pd=<%=suPedido%>&lp=<%=suLinea%>&pl=<%=suProducto%>&gu=1" >
			<h1>Devolver producto</h1>
			<table style="width:100%;">
				<tr>
					<td align="right"><label class="etiqDevol" for="txtFechaDevolcion">Devoluci�n realizada:</label></td>
					<td align="left"><!--<input type="checkbox" id="chkRealizadoDevolucion" name="chkRealizadoDevolucion" checked />-->
                    <select id="cboEstadoDevolucion" name="cboEstadoDevolucion" style="width:200px;" >
                    	<option value="0" <%if estadoPedido=0 then%> selected<%end if%> >Pendiente</option>
                    	<option value="1" <%if estadoPedido=1 then%> selected<%end if%> >Aceptado</option>
                    	<option value="2" <%if estadoPedido=2 then%> selected<%end if%> >Rechazada</option>                                                
                    </select>

                    </td>                
				</tr>
				<tr>
					<td align="right"><label class="etiqDevol" for="txtGastoDevolucion">Coste devoluci�n:</label></td>
					<td align="left"><input type="text" id="txtGastoDevolucion" name="txtGastoDevolucion" class="cajaTexto" value="<%=costeDevolucion%>" style="width:80px;"  /> </td>                
				</tr>                
				<tr>
					<td align="right"><label class="etiqDevol" for="txtFechaDevolcion">Fecha devoluci�n:</label></td>
					<td align="left"><input type="text" id="txtFechaDevolcion" name="txtFechaDevolcion" class="cajaTextoFecha" value="<%=fecha%>" style="width:80px;"  /></td>                
				</tr>
				<tr>
					<td align="right" valign="top"><label class="etiqDevol" for="txtFechaDevolcion">Comentario:</label></td>
					<td align="left">
						<textarea id="txtComentarioDevolucion" name="txtComentarioDevolucion" style="width:250px; height:120px;"><%=comentario%></textarea>
					</td>                
				</tr>                        
				<tr>
					<td colspan="2" align="right" style="padding-right:60px;">

					<input type="button" id="btnGuardarDevolucion" name="btnGuardarDevolucion"  value="Guardar" class="boton"  />         
					<input type="button" id="btnSalirDevolucion" name="btnSalirDevolucion"  value="Salir" class="boton" onClick="javascript:parent.$.fancybox.close();" />                 
					</td>
				</tr>            
			</table>
	
		</form>
     <%end if 'end de if trim(request("gu"))="1" then%>   
    


	</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->