<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->
 
   	<script type="text/javascript">
 		jQuery(document).ready(function(){
		
				oTable = $('#tablaContenido').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 25,
					"aaSorting": [[ 1, "desc" ],[ 2, "desc" ]],	
					"aoColumns": [ {"bSortable": false},{ "sType": "formatted-num" },{ "sType": "formatted-num" }]
				});
				
				
				
		});
		</script>		
 
 <%
 
		filtroPedidos=" PED_TRATADO=1 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		filtroPedidosCambios=" And ( PED_PADRE_DEVOLUCION is null Or PED_CODIGO in ( Select PCT.PED_PADRE_DEVOLUCION From PEDIDOS PCT Where PCT.PED_PADRE_DEVOLUCION is not null ) ) "				
		
		'Pedidos nulos. 'Falta quitar los que tengan lineas buenas
		sqlNulosSelect=" Select count(P.PED_CODIGO) as numPedidosNulos "
		sqlNulosSelect2=" Select P.PED_CODIGO "		
		sqlNulos=" From Pedidos P "
		sqlNulos=sqlNulos & " Where " & filtroPedidos & filtroPedidosCambios 
		sqlNulos=sqlNulos & " And ((Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHASOLICITUD is not null And PD.PEDD_REALIZADO=1  ) > 0) "
		'sqlNulos=sqlNulos & " And ((Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHASOLICITUD is not null And PD.PEDD_REALIZADO=1) = (Select COUNT(PL.PEDLI_CODIGO) * PL.PEDLI_UNIDADES From PEDIDOS_LINEAS PL Where PL.PEDLI_SUPEDIDO=P.PED_CODIGO And PL.PEDLI_SUPRODUCTO_IDERP<>10 And PL.PEDLI_SUPRODUCTO_IDERP<>20 Group by PL.PEDLI_UNIDADES ) )  "				
		 
%>
		<h1>Top de los clientes m�s activos (100 primeros)</h1>
		
			<%contTopClientes=1
			  sqlTopClientes=" Select top 100 C.CON_CODIGO,C.CON_NOMBRE,C.CON_APELLIDO1,C.CON_APELLIDO2,C.CON_EMAIL,count(P.PED_CODIGO) as numPedidos "
			  sqlTopClientes=sqlTopClientes & ",sum(TLineas.sumaLineas) as cantPedido "
			  sqlTopClientes=sqlTopClientes & " From CONTACTOS C Inner Join PEDIDOS_VENTAS P On (C.CON_CODIGO=P.codPropietario) "
			  sqlTopClientes=sqlTopClientes & " Inner Join ( Select L.PEDLI_SUPEDIDO,sum(L.PEDLI_PRECIOPAGADO*L.PEDLI_UNIDADES) as sumaLineas From PEDIDOS_LINEAS L Group by L.PEDLI_SUPEDIDO ) as TLineas on (P.PED_CODIGO=TLineas.PEDLI_SUPEDIDO)  "
			  sqlTopClientes=sqlTopClientes & " Where " & filtroPedidos & filtroPedidosCambios 	
			  sqlTopClientes=sqlTopClientes & " And P.PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "  			  			  
			  sqlTopClientes=sqlTopClientes & " Group by C.CON_CODIGO, C.CON_NOMBRE, C.CON_APELLIDO1, C.CON_APELLIDO2, C.CON_EMAIL "
			  sqlTopClientes=sqlTopClientes & " Order By count(P.PED_CODIGO) DESC, sum(TLineas.sumaLineas) DESC "
			  Set rsTopClientes = connCRM.AbrirRecordSet(sqlTopClientes,3,1) 	
			  tieneTopClinetesActivos=false
			  if not rsTopClientes.eof then
				if rsTopClientes.recordcount>0 then
					tieneTopClinetesActivos=true
				end if
			  end if
			  'if tieneTopClinetesActivos then%> 
				 	<table id="tablaContenido"  class="tablaResumenGeneral" style="margin:0; width:100%;"  >
						<thead>
							<tr>
								<th style="width:60%;"  class="tablaResumenLectura">Cliente</th>
								<th style="width:20%;"  class="tablaResumenLectura">N� pedidos</th>
								<th style="width:20%;"  class="tablaResumenLectura">Compra</th> 
							</tr>
						</thead>
						<tbody>	
					 <% while not rsTopClientes.EOF And contTopClientes<=100%>
						<tr>
							<td style="text-align:left;" ><%=rsTopClientes("CON_NOMBRE") & " " & rsTopClientes("CON_APELLIDO1") & " " & rsTopClientes("CON_APELLIDO2") & " (" & rsTopClientes("CON_EMAIL") & ") "%></td>
							<td style="text-align:right;"><%=formatnumber(rsTopClientes("numPedidos"),0)%></td>
							<td style="text-align:right;"><%=formatnumber(rsTopClientes("cantPedido"),2) & " �"%></td>										
						</tr>
					<%rsTopClientes.movenext
					contTopClientes=contTopClientes + 1
				  wend	
				  rsTopClientes.cerrar()
				  set rsTopClientes=nothing	%>
						</tbody>
					</table> 
			  <%'end if 'end de  if tieneTopClinetesActivos then%>           

 
 
 <br /><br />


<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->






