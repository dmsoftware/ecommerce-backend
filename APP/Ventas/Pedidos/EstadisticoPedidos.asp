<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<h1><%=objIdioma.getIdioma("EstadisticoPedidos_Titulo1")%></h1>    
    
    
    
<form id="estadisticasPedidos" name="estadisticasPedidos" runat="server"  method="post" action="EstadisticoPedidos.asp"  >    

	<%
	
	
		filtro=request("fil")
		valorFiltro=request("valfil")
		
		filtroComp=request("filtroComp")
		
		
		coloresComp=""
		seriesComp=""
		tieneComparador=false
		numAnnosComp=0
		if trim(filtroComp)<>"" then
			tieneComparador=true
			arrFiltroComp=split(filtroComp,",")
			numAnnosComp=ubound(arrFiltroComp)
		end if
		sacarComparadores=false
		

        'Distinguimos si hay que presentar vista A�OS, A�O, MES
		esAnual=true
		
		Anno=request("anno")
		'if Anno="" then 
		'	Anno=cstr(year(date()))
		'	Mes="0"
		'end if
		if Anno="" or Anno="0" then
			tieneComparador=false
            ModoVista="1ANOS"
            Anno=""
            textoMeses=objIdiomaGeneral.getIdioma("PeriodoTiempo_Texto1")
            'Hallamos el n�mero de a�os disponibles en la tabla pedidos
		    sql=" Select distinct(year(PED_FECHA)) From PEDIDOS order by year(PED_FECHA)"
		    Set rstNumAnos = connCRM.AbrirRecordSet(sql,3,1)
            maximoFilter=rstNumAnos.recordcount
            AnoInicial=rstNumAnos(0)
            rstNumAnos.cerrar
        else
			Anno=cstr(request("anno"))
		    maximoFilter=12
		    Mes=request("mes")
		    if trim(Mes)="" Or trim(Mes)="0" then
                ModoVista="2ANO"
                textoMeses=objIdiomaGeneral.getIdioma("PeriodoTiempo_Texto2")
			    Mes="0"
		    else
				tieneComparador=false			
                ModoVista="3MES"
			    maximoFilter=ultimoDiaMes(Mes,Anno)
			    esAnual=false	
			    textoMeses=objIdiomaGeneral.getIdioma("PeriodoTiempo_Texto3")
		    end if	

		end if
		
		if Anno="" And  not trim(request("anno"))="0" then 
			Anno=cstr(year(date()))
		    maximoFilter=12			
			ModoVista="2ANO"
			textoMeses=objIdiomaGeneral.getIdioma("PeriodoTiempo_Texto2")
			Mes="0"
		end if		
		
		leftGraficos=60
		altoGrafico=100
		altoCapaGrafico=230
		numRowSpan=1
		if tieneComparador then
			maximoFilter=12
			leftGraficos=120
			altoGrafico=160
			altoCapaGrafico=270			
			numRowSpan=numAnnosComp + 2							
		end if

		numNuevosClientes=0
		numPedidos=0
		cantIngresos=0
		cantIngresosVentas=0				
		cantGastosEnvio=0
		cantResumenGastosEnvioDomicilio=0
		cantResumenGastosEnvioDevolucion=0
		cantResumenGastosEnvioCambioTalla=0								


		aplicarIva=""
		if not conIVAIncluido then
			aplicarIva="*100/(100+PEDLI_IVA)"
		end if	


		%> <!-- #include virtual="/DMCrm/APP/Ventas/Pedidos/EstadisticoPedidosConsultas.asp"--> <%
	
		tieneNuevosClientes=false
		tienePedidos=false
		tieneIngresos=false
		tieneGastosEnvioEntrega=false
		tieneGastosEnvioDevolucion=false
		tieneGastosEnvioCambioTalla=false
		
		dim arrNuevosClientes()		
		dim arrPedidos()		
		dim arrIngresos()	
		dim arrGastosEnvioEntrega()	
		dim arrGastosEnvioDevolucion()					
		dim arrGastosEnvioCambioTalla()
		
		for contMes=1 to maximoFilter %>
			<!-- #include virtual="/DMCrm/APP/Ventas/Pedidos/EstadisticoPedidosDatos.asp"-->            
		<%next		


		cantGastosEnvio=cantResumenGastosEnvioDomicilio + cantResumenGastosEnvioDevolucion + cantResumenGastosEnvioCambioTalla
		cantIngresos=cantIngresosVentas + cantGastosEnvio		

		
''Calculamos las medias
		calculadorMedias=0
		tipoCalculador=""
		whereCalculador=""
		Select case modoVista
            case "1ANOS"
				tipoCalculador="YY"
				whereCalculador=""			
            case "2ANO"
				tipoCalculador="M"
                whereCalculador = " And YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
				tipoCalculador="D"
               whereCalculador = " And YEAR(P.PED_FECHA)=" & Anno & " And month(P.PED_FECHA)=" & Mes 
        end select			

		sqlCalculadorMedia=" Select DATEDIFF(""" & tipoCalculador & """,min(P.PED_FECHA),max(P.PED_FECHA)) as numCalculador "
		sqlCalculadorMedia=sqlCalculadorMedia & " From Pedidos P Where " & filtroPedidos & filtroPedidosCambios		
		sqlCalculadorMedia=sqlCalculadorMedia & whereCalculador
				
		Set rsCalculadorMedia = connCRM.AbrirRecordSet(sqlCalculadorMedia,3,1) 	
		if not rsCalculadorMedia.eof then
			if not isnull(rsCalculadorMedia("numCalculador")) then
				calculadorMedias=rsCalculadorMedia("numCalculador") + 1
			end if
		end if
		rsCalculadorMedia.cerrar()
		set rsCalculadorMedia=nothing



		
		mediaNuevosClientes=0
		mediaPedidos=0
		mediaIngresos=0
		mediaIngresosVentas=0
		mediaGastosEnvio=0
		mediaGastosEnvioEntrega=0
		mediaGastosEnvioDevolucion=0
		mediaGastosEnvioCambioTalla=0

		if not calculadorMedias=0 then
			mediaNuevosClientes=round(numNuevosClientes / calculadorMedias,2)
			mediaPedidos=round(numPedidos / calculadorMedias,2)
			mediaIngresos=round(cantIngresos / calculadorMedias,2)
			mediaIngresosVentas=round(cantIngresosVentas / calculadorMedias,2)
			mediaGastosEnvio=round(cantGastosEnvio / calculadorMedias,2)
			mediaGastosEnvioEntrega=round(cantResumenGastosEnvioDomicilio / calculadorMedias,2)
			mediaGastosEnvioDevolucion=round(cantResumenGastosEnvioDevolucion / calculadorMedias,2)
			mediaGastosEnvioCambioTalla=round(cantResumenGastosEnvioCambioTalla / calculadorMedias,2)	
		end if		
		
		
		coloresComp="'#FF6600','#FF9751'"
		seriesComp="1: {type: ""line""}"	
		'series: {1: {type: "line"},3: {type: "line"}}						
		
		if tieneComparador then
			sacarComparadores=true
			
			dim arrNuevosClientesComp()	
			redim arrNuevosClientesComp(maximoFilter,numAnnosComp)
			
			dim arrPedidosComp()	
			redim arrPedidosComp(maximoFilter,numAnnosComp)			
			
			dim arrIngresosComp()	
			redim arrIngresosComp(maximoFilter,numAnnosComp)			
			
			dim arrGastosEnvioEntregaComp()	
			redim arrGastosEnvioEntregaComp(maximoFilter,numAnnosComp)			
			
			dim arrGastosEnvioDevolucionComp()	
			redim arrGastosEnvioDevolucionComp(maximoFilter,numAnnosComp)			
			
			dim arrGastosEnvioCambioTallaComp()	
			redim arrGastosEnvioCambioTallaComp(maximoFilter,numAnnosComp)
			
			
			dim arrNumNuevosClientes()
			redim arrNumNuevosClientes(numAnnosComp)			
			
			dim arrMediaNuevosClientes()
			redim arrMediaNuevosClientes(numAnnosComp)						
			
			dim arrNumPedidos()
			redim arrNumPedidos(numAnnosComp)						
			
			dim arrMediaNumPedidos()
			redim arrMediaNumPedidos(numAnnosComp)									
			
			dim arrCantPedido()
			redim arrCantPedido(numAnnosComp)			
			
			dim arrMediaCantPedido()
			redim arrMediaCantPedido(numAnnosComp)									
			
			dim arrCantGastosEnvioEntrega()
			redim arrCantGastosEnvioEntrega(numAnnosComp)						
			
			dim arrMediaGastosEnvioEntrega()
			redim arrMediaGastosEnvioEntrega(numAnnosComp)									
			
			dim arrCantGastosEnvioDevolucion()
			redim arrCantGastosEnvioDevolucion(numAnnosComp)			
			
			dim arrMediaGastosEnvioDevolucion()
			redim arrMediaGastosEnvioDevolucion(numAnnosComp)									
			
			dim arrCantGastosEnvioCambioTalla()
			redim arrCantGastosEnvioCambioTalla(numAnnosComp)																					

			dim arrMediaGastosEnvioCambioTalla()
			redim arrMediaGastosEnvioCambioTalla(numAnnosComp)									
			
			cantIngresosVentas=0				
			cantGastosEnvio=0
			cantResumenGastosEnvioDomicilio=0
			cantResumenGastosEnvioDevolucion=0
			cantResumenGastosEnvioCambioTalla=0																				
			

			dim arrColoresComp()
			for contCol=0 to (numAnnosComp * 2+1)
				
				cadaColor=""
				select case contCol
					case 0
						cadaColor=",'#6A8EBD'"			
					case 1
						cadaColor=",'#A9D5FF'"	
					case 2
						cadaColor=",'#59B300'"									
					case 3
						cadaColor=",'#D5FFA9'"									
					case 4
						cadaColor=",'#C082FF'"									
					case 5
						cadaColor=",'#DDBBFF'"									
					case 6
						cadaColor=",'#FF79BC'"																																							
					case 7
						cadaColor=",'#FFA9D5'"															
					case else
						cadaColor=",'#CDCDCD'"						
				end select
				redim preserve arrColoresComp(contCol)	
				arrColoresComp(contCol)=cadaColor
			next

			
			
'			if (numAnnosComp * 2) > ubound(arrColoresComp) then
'				for contRestoCol = ubound(arrColoresComp) to (numAnnosComp * 2)
'					redim preserve arrColoresComp(contRestoCol)				
'					arrColoresComp(contRestoCol)=",'#CDCDCD'"
'				next
'			end if

			for contComp=0 to ubound(arrFiltroComp)

				posColor=contComp*2	
				coloresComp=coloresComp & arrColoresComp(posColor)				
				coloresComp=coloresComp & arrColoresComp(posColor+1)
				seriesComp=seriesComp & "," & posColor+3 & ": {type: ""line""}"							
				
				
				cadaAnno=arrFiltroComp(contComp)
				for contMes=1 to maximoFilter		%>
					<!-- #include virtual="/DMCrm/APP/Ventas/Pedidos/EstadisticoPedidosDatos.asp"-->            
				<%next							
                 
				 
				calculadorMedias=0
				sqlCalculadorMedia=" Select DATEDIFF(""M"",min(P.PED_FECHA),max(P.PED_FECHA)) as numCalculador "
				sqlCalculadorMedia=sqlCalculadorMedia & " From Pedidos P Where " & filtroPedidos & filtroPedidosCambios		
				sqlCalculadorMedia=sqlCalculadorMedia & " And YEAR(P.PED_FECHA)=" & cadaAnno
				Set rsCalculadorMedia = connCRM.AbrirRecordSet(sqlCalculadorMedia,3,1) 	
				if not rsCalculadorMedia.eof then
					if not isnull(rsCalculadorMedia("numCalculador")) then
						calculadorMedias=rsCalculadorMedia("numCalculador") + 1
					end if
				end if
				rsCalculadorMedia.cerrar()
				set rsCalculadorMedia=nothing
				 
				arrMediaNuevosClientes(contComp)=round(arrNumNuevosClientes(contComp) / calculadorMedias,2)
				arrMediaNumPedidos(contComp)=round(arrNumPedidos(contComp) / calculadorMedias,2)
				arrMediaCantPedido(contComp)=round(arrCantPedido(contComp) / calculadorMedias,2)
				arrMediaGastosEnvioEntrega(contComp)=round(arrCantGastosEnvioEntrega(contComp) / calculadorMedias,2)
				arrMediaGastosEnvioDevolucion(contComp)=round(arrCantGastosEnvioDevolucion(contComp) / calculadorMedias,2)
				arrMediaGastosEnvioCambioTalla(contComp)=round(arrCantGastosEnvioCambioTalla(contComp) / calculadorMedias,2)																				

			next
		end if		


	%>
    <script type="text/javascript" src="/dmcrm/js/gvChart/jsapi.js"></script>
	<script type="text/javascript" src="/dmcrm/js/gvChart/jquery.gvChart-1.0.1.min.js"></script>	
   	<script type="text/javascript">
		gvChartInit();
		
		jQuery(document).ready(function(){
		
		
				$('#ulCompradorAnos li').live('click', function(){
					if ($(this).hasClass('compAnnoSel')) {
						$(this).removeClass('compAnnoSel');
					}else {
						$(this).addClass('compAnnoSel');						
					}
				});
				
				$('#cboMes').live('change', function(){
					var mesSelec=$(this).val();
					if (mesSelec!="0") {
						$('#hidCompAnno').val('0');		
						$('#capaCompAnnos').hide('fast');
						$('.elemCompAnnos').removeClass('compAnnoSel');				
					} else {
						$('#capaCompAnnos').show('fast');
					}
					
				});	
				
				$('#cboAnno').live('change', function(){
					var annoCargado=$(this).attr('rel');
					var annoSelec=$(this).val();
					var annoAnterior=$('#hidCompAnno').val();
						
					$('#hidCompAnno').val(annoSelec);						

					if (annoSelec=="0") {
						$('#capaCompAnnos').hide('fast');
						$('.elemCompAnnos').removeClass('compAnnoSel');						
					} else {
						$('#capaCompAnnos').show('fast');
						$('#elemComp_' + annoAnterior).show('fast'); 
						
						$('#elemComp_' + annoSelec).hide('fast'); 					
						$('#elemComp_' + annoSelec).removeClass('compAnnoSel');						
					}

					var maxMes=$('#cboMes').attr('rel');
					for (contMes = 1; contMes <= 12; contMes++) {
						if (annoSelec==annoCargado && contMes>maxMes  ) {
							$('#idValorCboMes_' + contMes).hide('fast');							
						}else {
							$('#idValorCboMes_' + contMes).show('fast');
						}
					}
					
				});				
				
				
				$('#btnAplicarFiltros').live('click', function(){
					var pantalla="EstadisticoPedidos.asp?fil=<%=request("fil")%>&valfil=<%=request("valfil")%>";
					var valAnno=$('#cboAnno').val();
					var valMes=$('#cboMes').val();					

					var permitir=true;
					if (valMes!="0" && valAnno=="0" ) {
						var permitir=false;
						alert("<%=objIdioma.getIdioma("EstadisticoPedidos_Texto1")%>");
					}
					
					if (permitir) {
						var tieneComparador=false;
						if (valAnno!="0" && valMes=="0" ) {
							tieneComparador=true;
						}
						
						pantalla=pantalla + "&anno=" + valAnno;
						if (valMes!="0" && valMes!="undefined") {
							pantalla= pantalla + "&mes=" + valMes;
						}

						var filtroComparador="";
						if (tieneComparador) {
							$('.compAnnoSel').each(function (index) {
								if (filtroComparador!="") {
									filtroComparador=filtroComparador + ",";
								}
								filtroComparador=filtroComparador + $(this).attr('rel');
							});
							if (filtroComparador!="") {
								pantalla=pantalla + "&filtroComp=" + filtroComparador							
							}
						}

						document.estadisticasPedidos.action=pantalla;
						document.estadisticasPedidos.submit();						
					}
				
				});																
				
		
				$(".cEmergente").live('click', function(){
					$(this).fancybox({
						'imageScale'			: true,
						'zoomOpacity'			: true,
						'overlayShow'			: true,
						'overlayOpacity'		: 0.7,
						'overlayColor'			: '#333',
						'centerOnScroll'		: true,
						'zoomSpeedIn'			: 600,
						'zoomSpeedOut'			: 500,
						'transitionIn'			: 'elastic',
						'transitionOut'			: 'elastic',
						'type'					: 'iframe',
						'frameWidth'			: '100%',
						'frameHeight'			: '100%',
						'titleShow'		        : false,					
						'onClosed'		        : function() {
							window.document.body.style.overflow="auto";
							   // window.location.reload(); 
						}
					}).trigger("click"); 
					return false; 	

				});
				
		
				oTable = $('#tabProductosVendidos').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 25,
					"aaSorting": [[ 1, "desc" ],[ 2, "desc" ]],	
					"aoColumns": [ {"bSortable": false},{ "sType": "formatted-num" },{ "sType": "formatted-num" }]
				});
				
				oTable = $('#tabClientesActivos').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 25,
					"aaSorting": [[ 1, "desc" ],[ 2, "desc" ]],	
					"aoColumns": [ {"bSortable": false},{ "sType": "formatted-num" },{ "sType": "formatted-num" }]
				});
				
				oTable = $('#tabMarcasActivos').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 25,
					"aaSorting": [[ 1, "desc" ],[ 2, "desc" ]],	
					"aoColumns": [ {"bSortable": false},{ "sType": "formatted-num" },{ "sType": "formatted-num" }]
				});				
				
			<%if tieneNuevosClientes then%>
			jQuery('#tabNuevosClientes').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: '', minValue: 0},
					legend:  {position: 'none'},					
					chartArea:{left:<%=leftGraficos%>,top:20,width:'82%',height:<%=altoGrafico%>},					
					colors:[<%=coloresComp%>],
					series: {<%=seriesComp%>}					
					}
			});	
			<%end if%>
			
			<%if tienePedidos then%>			
			jQuery('#tabNumPedidos').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend:  {position: 'none'},					
					chartArea:{left:<%=leftGraficos%>,top:20,width:'82%',height:<%=altoGrafico%>},					
					colors:[<%=coloresComp%>],
					series: {<%=seriesComp%>}					
					}
			});
			<%end if%>
			
			<%if tieneIngresos then%>			
			jQuery('#tabCantIngresos').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend:  {position: 'none'},					
					chartArea:{left:<%=leftGraficos%>,top:20,width:'82%',height:<%=altoGrafico%>},					
					colors:[<%=coloresComp%>],
					series: {<%=seriesComp%>}					
					}
			});	
			<%end if%>	
			
			<%if tieneGastosEnvioEntrega then%>			
			jQuery('#tabCantGastosEnvioEntrega').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend:  {position: 'none'},					
					chartArea:{left:<%=leftGraficos%>,top:20,width:'82%',height:<%=altoGrafico%>},					
					colors:[<%=coloresComp%>],
					series: {<%=seriesComp%>}					
					}
			});		
			<%end if%>
			
			<%if tieneGastosEnvioDevolucion then%>			
			jQuery('#tabCantGastosEnvioDevolucion').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend:  {position: 'none'},					
					chartArea:{left:<%=leftGraficos%>,top:20,width:'82%',height:<%=altoGrafico%>},					
					colors:[<%=coloresComp%>],
					series: {<%=seriesComp%>}					
					}
			});		
			<%end if%>			
			
			<%if tieneGastosEnvioCambioTalla then%>			
			jQuery('#tabCantGastosEnvioCambioTalla').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend:  {position: 'none'},					
					chartArea:{left:<%=leftGraficos%>,top:20,width:'82%',height:<%=altoGrafico%>},					
					colors:[<%=coloresComp%>],
					series: {<%=seriesComp%>}					
					}
			});	
			<%end if%>							

		});    
		
		</script>
        
		<script type="text/javascript">

		
		function recargarFiltrosTiempo(ModoVista,valor,inicializar) {
			var pantalla="";
			if (inicializar) {
				pantalla="EstadisticoPedidos.asp";
			}
			else {
				pantalla="EstadisticoPedidos.asp?fil=<%=request("fil")%>&valfil=<%=request("valfil")%>";
				if (ModoVista=="1ANOS") {
					pantalla=pantalla + "&anno=0";
				}else if (ModoVista=="2ANO") {
					pantalla=pantalla + "&anno=" + valor;
				}else if (ModoVista=="3MES") {
					var annoMes=document.getElementById("cboAnno").value;					
					pantalla=pantalla + "&anno=" + annoMes + "&mes=" + valor;									
				}
			}

			document.estadisticasPedidos.action=pantalla;
			document.estadisticasPedidos.submit();
			
        }		
		</script>      
		

<style type="text/css">
.DatosResumen:after {display:block; clear:both; content:""; height:1px;}
.DatosResumen {background:#f7f7f7; padding:5px; margin:5px 0 0 0; color:#555; border-top: solid 5px #ff6600;}

/*.descripcionPantalla {float:left; width:75%; text-align:left; padding:15px 20px 0 20px;  }
.resumenNumerico {  color:#ff6600; font-size:16px; font-weight:bold; margin-top:6px;  }*/

/*filtros*/
/*#ulFiltros { float:right; width:15%; text-align:right;  padding-left:30px; padding-right:10px; border-left:dashed 1px gray; }
#ulFiltros li {margin:0; padding:0; list-style:none; padding:3px 0;font-size:12px; }*/
#ulFiltros { float:left; width:80%; text-align:left;  padding-left:30px; padding-right:10px; }
#ulFiltros li {margin:0; padding:0; list-style:none; padding:3px 0;font-size:12px; }
.tituloResumen { font-weight:bold; }

.etiqFiltros {/*float:left;*/  /*margin-left:20px; margin-right:5px;*/}


#ulCompradorAnos {float:left;  width:120px; height:80px; overflow-y:auto; margin:0; padding:0; text-align:left; list-style:none; background:white; border:solid 1px #CDCDCD;}
#ulCompradorAnos li { border-bottom:dashed 1px #CDCDCD; padding:2px 5px 2px 5px; cursor:pointer; }
#ulCompradorAnos li:hover { background-color:#C1C1C1; }

.compAnnoSel { background:#62B150; color:white; }
.compAnnoSel:hover { background:#62B150!important;}


</style>        




    <div class="DatosResumen">
	
         <span  style="font-size:11px; font-weight:normal; font-style:italic; float:right; margin-right:20px;">
            <%if conIVAIncluido then%>
                <%=objIdiomaGeneral.getIdioma("avisoIva_Texto1")%>
            <%else%>
                <%=objIdiomaGeneral.getIdioma("avisoIva_Texto2")%>                                                    
            <%end if%>
        </span>
		

		 
     <ul id="ulFiltros" style="border-left:none;" >
     		<li><%=objIdioma.getIdioma("EstadisticoPedidos_Texto2")%> </li>
            
              <li style=" padding-left:50px; border-bottom:solid 1px #D1D1D1; width:360px; padding-bottom:10px; ">
               <span style="font-weight:bold; margin-left:-50px; "><%=objIdiomaGeneral.getIdioma("filtradoPor_Texto1")%></span>&nbsp;&nbsp;&nbsp;
               	<%
			    Select case modoVista
                    case "1ANOS"
                        response.write (objIdiomaGeneral.getIdioma("todosLosAnnos_Texto1"))
                    case "2ANO"
                        response.Write(objIdiomaGeneral.getIdioma("anno_Texto1") & ": " & Anno)
                    case "3MES"
                        response.Write(objIdiomaGeneral.getIdioma("anno_Texto1") & ": " & Anno)
                        response.Write("   |   " & objIdiomaGeneral.getIdioma("mes_Texto1") & ": " & mesLargoIdioma(Mes))
                end select
				if trim(textoFiltradoPorTitulo)<>"" And trim(textoFiltradoPorContenido)<>"" then
					 response.Write(textoFiltradoPorTitulo & ": " & textoFiltradoPorContenido)				
				end if
				%>&nbsp;&nbsp;&nbsp;
               <a href="#" style="font-weight:normal; float:right;" onClick="javascript:recargarFiltrosTiempo('1ANOS','<%=year(date())%>',true);" >[<%=objIdiomaGeneral.getIdioma("limpiarFiltros_Texto1")%>]</a>                    
               </li>              
            
<!--			<li class="tituloResumen" style="float:left; clear:both; width:100%; text-align:left;">Filtros:</li>	 -->
	         <li style="padding-top:10px;"  >
             	<div style="width:150px; float:left;">
		             <span class="etiqFiltros"><%=objIdiomaGeneral.getIdioma("anno_Texto1")%>:</span><br/>
			        <select  id="cboAnno" name="cboAnno" rel="<%=Anno%>" class="combos" style="width:100%; "  >
                        <option value="0" <%if modoVista="1ANOS" then response.write " selected" %>><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option>						    
                    	<%	sql=""
							sql=" select distinct Year(PED_FECHA) as Anno "
							sql=sql & " from  PEDIDOS "
							sql=sql & " Where (PED_ANULADO=0 Or PED_ANULADO is null ) "																				
							sql=sql& " Order by Year(PED_FECHA) DESC "
							Set rsAnnos = connCRM.AbrirRecordSet(sql,0,1)
							while not rsAnnos.EOF%>	
								<option value="<%=rsAnnos("Anno")%>"  <%if modoVista<>"1ANOS" and ( trim(cstr(rsAnnos("Anno")))=Anno or ( Anno="" And rsAnnos("Anno")=year(now()) )) then %>  selected <% end if %> ><%=rsAnnos("Anno")%></option>						    
                         	    <%rsAnnos.MoveNext 
							wend
							''No lo cerramos porque lo vas a usar mas tarde.
						%>
                    </select>                        

					 <%'No muestro los meses si estoy en la vista general de a�os
                     if modoVista<>"1ANOS" then %>
                     	<div style="clear:both; width:100%; margin:0; padding:0; height:5px;"></div>
                         <span class="etiqFiltros"><%=objIdiomaGeneral.getIdioma("mes_Texto1")%>:</span><br/>
                                <select  id="cboMes" name="cboMes" rel="<%=month(now())%>"   class="combos" style="width:100%; "   >
                                    <option value="0"  <%if trim(Mes)="0" or trim(Mes)="" then %>  selected <% end if %> ><%=objIdiomaGeneral.getIdioma("MostrarTodos_Texto1")%></option>                                                
                                        <%for contMeses=1 to 12
											estiloOcultoMes=""
                                            if month(now())<contMeses And trim(Anno)=trim(cstr(year(now()))) then
												estiloOcultoMes=" style=""display:none;"" "
											end if%>
                                                <option id="idValorCboMes_<%=contMeses%>" <%=estiloOcultoMes%> value="<%=contMeses%>"  <%if trim(cstr(Mes))=trim(cstr(contMeses)) then %>  selected <% end if %> ><%=mesLargoIdioma(contMeses)%></option>
                                        <%next%>
                                </select>             
                     
                     <%end if%>   
                     
                     	<div style="clear:both; width:100%; margin:0; padding:0; height:10px;"></div>
                        
                        <a id="btnAplicarFiltros" name="btnAplicarFiltros"><%=objIdiomaGeneral.getIdioma("aplicarFiltros_Texto1")%></a>

                </div>
                
				 <%'No muestro los meses si estoy en la vista general de a�os
                if modoVista="2ANO" then %>                
                    <div id="capaCompAnnos" style="width:300px; margin-left:50px; float:left;">
                    		<input type="hidden" id="hidCompAnno" name="hidCompAnno" value="<%=Anno%>" />
                             <span class="etiqFiltros"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto3")%></span><br/>
                             <ul id="ulCompradorAnos" >
                                <%rsAnnos.movefirst
                                while not rsAnnos.EOF
									estiloOculto=""
									if trim(cstr(rsAnnos("Anno")))=Anno then
										estiloOculto=" style=""display:none;"" "
									end if
									claseSel=""
									if instr(filtroComp,trim(cstr(rsAnnos("Anno"))))>0 then
										claseSel=" compAnnoSel"
									end if%>	
	                                     <li id="elemComp_<%=rsAnnos("Anno")%>" class="elemCompAnnos<%=claseSel%>" <%=estiloOculto%>  rel="<%=rsAnnos("Anno")%>"  ><%=rsAnnos("Anno")%></li>
                                    <%rsAnnos.MoveNext 
                                wend %>           
                             </ul>                  
                    </div>
                <%end if%>
				<%rsAnnos.cerrar()
				set rsAnnos=nothing   %>
             </li>
         </ul>		
	 </div>
<!--	<div style="height:20px; width:50%; border:solid 1px red;">bb</div>	 -->
	<br/>
    
 
  
    
    
<style type="text/css">
 .tablaResumen { z-index:1; float:left;position:relative; top:-40px; left:0px; margin:0 10px 0 10px;}
 .tablaResumen th { background-color:#5893D5; color:white;  vertical-align:middle; width:80px; text-align:center; padding:4px; } 
 .tablaResumen th:hover { background-color:#ff6600; cursor:pointer; }  
 .tablaResumen th.tablaResumenCabSel { background-color:#015B9F;  }  
 .tablaResumen td { background-color:#F1F1F1; color:#ff6600; text-align:center; width:80px; font-size:14px; font-weight:bold; }  
 
 .bloqueResumen {float:left; border:solid 1px #CDCDCD; /*min-width:595px;*/ height:<%=altoCapaGrafico%>px; margin:0; padding:0; margin:10px 10px 10px 10px; }
 .bloqueResumen_Peq { /*width:595px;*/ width:47%;  }
 .bloqueResumen_Grande {/* width:120px;*/ width:96%; } 
 .bloqueResumenTitulo {margin:0; padding:5px 0 5px 0; width:100%; border-bottom:solid 3px #ff6600; font-size:14px; color:#555; font-weight:bold; background-color:#F7F7F7; text-transform:uppercase;}
 
 .bloqueResumenGraficos {width:100%; float:left; margin:0; padding:0;}

 .capaGraficoPorcentual {position:relative; top:-50px; left:10;}

.tablaGraficos { }
.tablaGraficos th { text-transform:capitalize;}

.capaLeyendas { float:left; margin-left:20px; width:auto;}
.titLeyendas { float:left; }
.colorLeyenda { width:10px; height:10px; float:left; margin-top:5px; margin-left:20px;}
.textoColorLeyenda { float:left; margin-left:5px; margin-top:2px;}
</style>            


<%
cabeceraTablaGrafco="<th>" & textoMeses & "</th>"
for contMes=1 to maximoFilter
	Select case modoVista
        case "1ANOS"
            nomMes=AnoInicial+contMes-1
        case "2ANO"
            nomMes=mesCortoIdioma(contMes)
        case "3MES"
            nomMes=contMes
    end select
	cabeceraTablaGrafco=cabeceraTablaGrafco & "<th >" & nomMes & "</th>"
next%>
    
    <div class="capaLeyendas">
    	<span class="titLeyendas">* <%=objIdiomaGeneral.getIdioma("leyendas_Texto1")%>:</span>
        <div class="colorLeyenda" style="background:#FF6600;"></div>
        <span class="textoColorLeyenda"><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto1") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto5")%></span>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div class="colorLeyenda" style="background:#FF9751;"></div>
        <span class="textoColorLeyenda"><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></span>        
    <%if tieneComparador then
		for contComp=0 to ubound(arrFiltroComp) 
			cadaIndice=contComp * 2
			if cadaIndice mod 2 <> 0 then
				cadaIndice=cadaIndice-1
			end if%>
			 <div class="colorLeyenda" style="background:<%=replace(replace(arrColoresComp(cadaIndice),",",""),"'","")%>;"></div>
			<span class="textoColorLeyenda"><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto1") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto5")%></span>
			&nbsp;&nbsp;&nbsp;&nbsp;        
			 <div class="colorLeyenda" style="background:<%=replace(replace(arrColoresComp(cadaIndice + 1),",",""),"'","")%>;"></div>
			<span class="textoColorLeyenda"><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></span>        
			<%'=arrFiltroComp(contComp)%>
		<%next
	end if%>
    </div>							



		<%claseTamanno=" bloqueResumen_Peq"
		if tieneComparador then
			claseTamanno=" bloqueResumen_Grande"
		end if%>	

	<div style="clear:both; margin:0; padding:0; width:100%; float:left; height:10px;"></div>


	<%if tieneNuevosClientes then%>
        <div class="bloqueResumen<%=claseTamanno%>">
            <div  class="bloqueResumenTitulo">&nbsp;&nbsp;* <%=objIdioma.getIdioma("EstadisticoPedidos_Texto4")%></div>
            
            <table id="tabNuevosClientes"  class="tablaGraficos"  >
                <thead  >
                    <tr><%=cabeceraTablaGrafco%></tr>
                </thead>
                <tbody>
                    <tr>
                        <th><%=Anno%></th>
                        <%for contMes=1 to maximoFilter
                            %><td  ><%=arrNuevosClientes(contMes)%></td><%
                        next%>
                    </tr>
                    <tr>
                        <th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
                        <%for contMes=1 to maximoFilter
                            %><td ><%=replace(mediaNuevosClientes,",",".")%></td><%
                        next%>
                    </tr>                
                    <%if tieneComparador then
                        for contComp=0 to numAnnosComp%>
                            <tr>
                                <th><%=arrFiltroComp(contComp)%></th>
                                <%for contMes=1 to maximoFilter
                                    %><td ><%=arrNuevosClientesComp(contMes,contComp)%></td><%
                                next%>
                            </tr> 
                            <tr>
                                <th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
                                <%for contMes=1 to maximoFilter
                                    %><td ><%=replace(arrMediaNuevosClientes(contComp),",",".")%></td><%
                                next%>
                            </tr>                         
                                                           
                    <%	next
                    end if%>
                </tbody>
            </table>    	        
         </div>
	 <%end if%>
	
	<%if tienePedidos then%>
    <div class="bloqueResumen<%=claseTamanno%>">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* <%=objIdioma.getIdioma("EstadisticoPedidos_Texto5")%>:</div>

			 <table id="tabNumPedidos"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th><%=Anno%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=arrPedidos(contMes)%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaPedidos,",",".")%></td><%
					next%>
	            </tr>				                
                <%if tieneComparador then
					for contComp=0 to numAnnosComp%>
						<tr>
							<th><%=arrFiltroComp(contComp)%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=arrPedidosComp(contMes,contComp)%></td><%
							next%>
						</tr>  
						<tr>
							<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(arrMediaNumPedidos(contComp),",",".")%></td><%
							next%>
						</tr>                                                           
				<%	next
				end if%>                
    	    </tbody>
	    </table>    	        
	 </div>  
	 <%end if%>   
	 
	<%if tieneIngresos then%> 
    <div class="bloqueResumen<%=claseTamanno%>">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* <%=objIdioma.getIdioma("EstadisticoPedidos_Texto6")%>(�):</div>

			 <table id="tabCantIngresos"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th><%=Anno%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrIngresos(contMes)),",",".")%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaIngresos,",",".")%></td><%
					next%>
	            </tr>		                
                <%if tieneComparador then
					for contComp=0 to numAnnosComp%>
						<tr>
							<th><%=arrFiltroComp(contComp)%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(cstr(arrIngresosComp(contMes,contComp)),",",".")%></td><%
							next%>
						</tr>     
						<tr>
							<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(arrMediaCantPedido(contComp),",",".")%></td><%
							next%>
						</tr>                                                        
				<%	next
				end if%>                
    	    </tbody>
	    </table>    	        
	 </div>
	 <%end if%>     
	 
	<%if tieneGastosEnvioEntrega then%> 
    <div class="bloqueResumen<%=claseTamanno%>">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* <%=objIdioma.getIdioma("EstadisticoPedidos_Texto7")%> - <%=objIdioma.getIdioma("EstadisticoPedidos_Texto8")%>(�):</div>

			 <table id="tabCantGastosEnvioEntrega"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th><%=Anno%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrGastosEnvioEntrega(contMes)),",",".")%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaGastosEnvioEntrega,",",".")%></td><%
					next%>
	            </tr>				                
                <%if tieneComparador then
					for contComp=0 to numAnnosComp%>
						<tr>
							<th><%=arrFiltroComp(contComp)%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(cstr(arrGastosEnvioEntregaComp(contMes,contComp)),",",".")%></td><%
							next%>
						</tr>   
						<tr>
							<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(arrMediaGastosEnvioEntrega(contComp),",",".")%></td><%
							next%>
						</tr>                                                          
				<%	next
				end if%>                
    	    </tbody>
	    </table>    	        
	 </div>   
	 <%end if%> 
	 
	<%if tieneGastosEnvioDevolucion then%> 
    <div class="bloqueResumen<%=claseTamanno%>">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* <%=objIdioma.getIdioma("EstadisticoPedidos_Texto7")%> - <%=objIdioma.getIdioma("EstadisticoPedidos_Texto9")%>(�):</div>

			 <table id="tabCantGastosEnvioDevolucion"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th><%=Anno%></th>
					<%					
					for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrGastosEnvioDevolucion(contMes)),",",".")%></td><%
					next
					%>
	            </tr>
				<tr>
    	        	<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaGastosEnvioDevolucion,",",".")%></td><%
					next%>
	            </tr>				                
                <%if tieneComparador then
					for contComp=0 to numAnnosComp%>
						<tr>
							<th><%=arrFiltroComp(contComp)%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(cstr(arrGastosEnvioDevolucionComp(contMes,contComp)),",",".")%></td><%
							next%>
						</tr>     
						<tr>
							<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(arrMediaGastosEnvioDevolucion(contComp),",",".")%></td><%
							next%>
						</tr>                                                        
				<%	next
				end if%>                
    	    </tbody>
	    </table>    	        
	 </div>
	 <%end if%>    	
     
	<%if tieneGastosEnvioCambioTalla then%> 
    <div class="bloqueResumen<%=claseTamanno%>">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* <%=objIdioma.getIdioma("EstadisticoPedidos_Texto7")%> - <%=objIdioma.getIdioma("EstadisticoPedidos_Texto10")%>(�):</div>

			 <table id="tabCantGastosEnvioCambioTalla"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th><%=Anno%></th>
					<%					
					for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrGastosEnvioCambioTalla(contMes)),",",".")%></td><%
					next
					%>
	            </tr>
				<tr>
    	        	<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & Anno & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaGastosEnvioCambioTalla,",",".")%></td><%
					next%>
	            </tr>				                
                <%if tieneComparador then
					for contComp=0 to numAnnosComp%>
						<tr>
							<th><%=arrFiltroComp(contComp)%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(cstr(arrGastosEnvioCambioTallaComp(contMes,contComp)),",",".")%></td><%
							next%>
						</tr>   
						<tr>
							<th><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto2") & " " & arrFiltroComp(contComp) & objIdiomaGeneral.getIdioma("globalEstadisticas_Texto6")%></th>
							<%for contMes=1 to maximoFilter
								%><td ><%=replace(arrMediaGastosEnvioCambioTalla(contComp),",",".")%></td><%
							next%>
						</tr>                                                          
				<%	next
				end if%>                
    	    </tbody>
	    </table>    	        
	 </div>    
	 <%end if%>	      
      	
    
<style type="text/css">
 .tablaResumenGeneral { float:left; clear:both; width:100%; margin:30px 0 40px 0; padding:0;}

 .tablaResumenGeneral th { background-color:#5893D5; color:white;  vertical-align:middle; max-width:1px; text-align:center; padding:4px; text-transform:capitalize; } 
 .tablaResumenGeneral th:hover { background-color:#ff6600;  cursor:pointer; }  
 .tablaResumenGeneral th.tablaResumenCabSel { background-color:#015B9F; }  
 .tablaResumenGeneral th.tablaResumenLectura { background-color:#5893D5; cursor:default; }  

 .tablaResumenGeneral tr { /*background-color:#F1F1F1;*/}
 .tablaResumenGeneral tr:hover { background-color:#CDCDCD;}
 .tablaResumenGeneral td {  color:#555; text-align:right; font-size:11px;}  
 .tablaResumenGeneral td.tituloCeldaCab { color:#555; text-align:left; text-transform:none; width:100px; font-size:12px; font-weight:bold; }  
 .tablaResumenGeneral td.tituloCeldaCabAgrupacion { color:#555; text-align:left; width:100px; font-size:13px; font-weight:bold; text-transform:uppercase;/* background-color:#F1F1F1;*/ }  
 .celdaGris { background:#CDCDCD; }
 .celdaGrisClaro { background:#F1F1F1; }
 .celdaBlnca { background:#FFFFFF: }
 
 
</style>     
    
    <table class="tablaResumenGeneral"  >
        	<tr>
            	<th style="min-width:150px;"  class="tablaResumenLectura"><%=textoMeses%></th>
                <%if tieneComparador then%>
	            	<th style="width:100px;"  class="tablaResumenLectura celdaGrisClaro"><%=objIdiomaGeneral.getIdioma("anno_Texto1")%></th>                
                <%end if%>
				<%For contMes=1 to maximoFilter%>
	                <%Select case modoVista%>
						<%case "1ANOS"%>
								<th style="max-width:100px;" onclick="recargarFiltrosTiempo('2ANO','<%=AnoInicial+contMes-1%>',false);"  ><%=AnoInicial+contMes-1%></th>   
						<%case "2ANO"%>
								 <th style="max-width:100px;" onclick="recargarFiltrosTiempo('3MES','<%=contMes%>',false);" ><%=mesLargoIdioma(contMes)%></th>   	
                        <%case "3MES"
                   			diaDeSemana=Weekday(cdate(contMes & "/" & Mes & "/" & Anno))%>
                            <th style="max-width:100px;"  class="tablaResumenLectura"><%=contMes & "<br />" & diaLetraIdioma(diaDeSemana)%></th>   
                    <%end select%>
                <%next%>
				<th style="min-width:50px;" class="tablaResumenLectura"  ><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto3")%></th>
				<th style="min-width:50px;"  class="tablaResumenLectura"><%=objIdiomaGeneral.getIdioma("globalEstadisticas_Texto4")%></th>  
		  </tr>
			<%if tieneNuevosClientes then%>	  
				<%bordeFila=""
                if not tieneComparador then
                    bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "
                end if%>            
                <tr class="filaTablaResumenGeneral">
                    <td  class="tituloCeldaCabAgrupacion celdaGris" <%=bordeFila%> rowspan="<%=numRowSpan%>"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto4")%></td>            
                    <%if tieneComparador then%>
	                    <td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=Anno%></td>                                
                    <%end if
					numElementosMes=0
                     valorElementosMes=0
                     For contMes=1 to maximoFilter%>
                        <td <%=bordeFila%> >
                            <%if arrNuevosClientes(contMes)=0 then
                                response.Write("-")
                            else
                                response.Write(formatnumber(arrNuevosClientes(contMes),0))
                            end if%>
                        </td>
                    <%numElementosMes=numElementosMes + 1
                    valorElementosMes=valorElementosMes + arrNuevosClientes(contMes)
                    next
                    if numElementosMes=0 then
                        mediaValoresMes=valorElementosMes
                     else
                        mediaValoresMes=valorElementosMes/numElementosMes				 
                     end if   %>
                    <td class="celdaGrisClaro" <%=bordeFila%> >
                        <%=formatnumber(mediaNuevosClientes,2)%>				
                    </td>    	
                    <td class="celdaGrisClaro" <%=bordeFila%>  ><%=formatnumber(valorElementosMes,0)%></td>    			 
                  </tr>    
                  
                  <%if tieneComparador then
						for contComp=0 to numAnnosComp
							bordeFila=""				  
						  if contComp=numAnnosComp then
							bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "					  
						  end if 	%>
						  <tr class="filaTablaResumenGeneral">
							<td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=arrFiltroComp(contComp)%></td>                                                      	
							<%numElementosMes=0
							 valorElementosMes=0
							 For contMes=1 to maximoFilter%>
								<td <%=bordeFila%> >
									<%if arrNuevosClientesComp(contMes,contComp)=0 then
										response.Write("-")
									else
										response.Write(formatnumber(arrNuevosClientesComp(contMes,contComp),0))
									end if%>
								</td>
							<%numElementosMes=numElementosMes + 1
							valorElementosMes=valorElementosMes + arrNuevosClientesComp(contMes,contComp)
							next
							if numElementosMes=0 then
								mediaValoresMes=valorElementosMes
							 else
								mediaValoresMes=valorElementosMes/numElementosMes				 
							 end if   %>
							<td class="celdaGrisClaro" <%=bordeFila%> >
								<%=formatnumber(arrMediaNumPedidos(contComp),2)%>				
							</td>    	
							<td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,0)%></td>    			 
						  </tr>                        
					  <%next
					  end if%>    
			 <%end if%>     
            
			<%if tienePedidos then%>	
				<%bordeFila=""
                if not tieneComparador then
                    bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "
                end if%>                        		
                <tr>
                    <td  class="tituloCeldaCabAgrupacion celdaGris" <%=bordeFila%> rowspan="<%=numRowSpan%>"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto5")%></td>            
                    <%if tieneComparador then%>
	                    <td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=Anno%></td>                                
                    <%end if                    
                    numElementosMes=0
                     valorElementosMes=0
                     For contMes=1 to maximoFilter%>
                        <td <%=bordeFila%> >
                            <%if arrPedidos(contMes)=0 then
                                response.Write("-")
                            else
                                response.Write(formatnumber(arrPedidos(contMes),0))
                            end if%>
                        </td>                    
                     <%numElementosMes=numElementosMes + 1
                    valorElementosMes=valorElementosMes + arrPedidos(contMes)
                    next
                    if numElementosMes=0 then
                        mediaValoresMes=valorElementosMes
                     else
                        mediaValoresMes=valorElementosMes/numElementosMes				 
                     end if   %>        
                    <td class="celdaGrisClaro" <%=bordeFila%>>
                        <%=formatnumber(mediaPedidos,2)%>				
                    </td> 	
                    <td class="celdaGrisClaro" <%=bordeFila%>  ><%=formatnumber(valorElementosMes,0)%></td>    			         
              </tr>   
          
                  <%if tieneComparador then
						for contComp=0 to numAnnosComp
							bordeFila=""				  
						  if contComp=numAnnosComp then
							bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "					  
						  end if 	%>
						  <tr class="filaTablaResumenGeneral">
							<td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=arrFiltroComp(contComp)%></td>                                                      	
							<%numElementosMes=0
							 valorElementosMes=0
							 For contMes=1 to maximoFilter%>
								<td <%=bordeFila%> >
									<%if arrPedidosComp(contMes,contComp)=0 then
										response.Write("-")
									else
										response.Write(formatnumber(arrPedidosComp(contMes,contComp),0))
									end if%>
								</td>
							<%numElementosMes=numElementosMes + 1
							valorElementosMes=valorElementosMes + arrPedidosComp(contMes,contComp)
							next
							if numElementosMes=0 then
								mediaValoresMes=valorElementosMes
							 else
								mediaValoresMes=valorElementosMes/numElementosMes				 
							 end if   %>
							<td class="celdaGrisClaro" <%=bordeFila%> >
								<%=formatnumber(arrMediaNumPedidos(contComp),2)%>				
							</td>    	
							<td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,0)%></td>    			 
						  </tr>                        
					  <%next
					  end if%>              
		  <%end if%>       
			
			<%if tieneIngresos then%>	
				<%bordeFila=""
                if not tieneComparador then
                    bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "
                end if%>                        		
                <tr>
                    <td  class="tituloCeldaCabAgrupacion celdaGris" <%=bordeFila%> rowspan="<%=numRowSpan%>"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto6")%>(�)</td>            
                    <%if tieneComparador then%>
	                    <td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=Anno%></td>                                
                    <%end if                                        
                    numElementosMes=0
                     valorElementosMes=0
                     For contMes=1 to maximoFilter%>
                        <td <%=bordeFila%>>
                            <%if isnull(arrIngresos(contMes)) then
                                response.Write("-")						
                            elseif clng(arrIngresos(contMes))=0 then
                                response.Write("-")
                            else
                                response.Write(formatnumber(arrIngresos(contMes),2))
                            end if%>
                        </td>                                        
                    <%numElementosMes=numElementosMes + 1
                      if isnumeric(arrIngresos(contMes)) then
                        valorElementosMes=valorElementosMes + arrIngresos(contMes)
                      end if
                    next
                    if numElementosMes=0 then
                        mediaValoresMes=valorElementosMes
                     else
                        mediaValoresMes=valorElementosMes/numElementosMes				 
                     end if   %>     
                    <td class="celdaGrisClaro" <%=bordeFila%>>
                        <%=formatnumber(mediaIngresos,2)%>				
                    </td> 		
                    <td class="celdaGrisClaro" <%=bordeFila%>  ><%=formatnumber(valorElementosMes,2)%></td>    			              
                 </tr>  
             
                  <%if tieneComparador then
						for contComp=0 to numAnnosComp
							bordeFila=""				  
						  if contComp=numAnnosComp then
							bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "					  
						  end if 	%>
						  <tr class="filaTablaResumenGeneral">
							<td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=arrFiltroComp(contComp)%></td>                                                      	
							<%numElementosMes=0
							 valorElementosMes=0
							 For contMes=1 to maximoFilter%>
								<td <%=bordeFila%> >
									<%if arrIngresosComp(contMes,contComp)=0 then
										response.Write("-")
									else
										response.Write(formatnumber(arrIngresosComp(contMes,contComp),2))
									end if%>
								</td>
							<%numElementosMes=numElementosMes + 1
							valorElementosMes=valorElementosMes + arrIngresosComp(contMes,contComp)
							next
							if numElementosMes=0 then
								mediaValoresMes=valorElementosMes
							 else
								mediaValoresMes=valorElementosMes/numElementosMes				 
							 end if   %>
							<td class="celdaGrisClaro" <%=bordeFila%> >
								<%=formatnumber(arrMediaCantPedido(contComp),2)%>				
							</td>    	
							<td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,0)%></td>    			 
						  </tr>                        
					  <%next
					  end if%>                 
			 <%end if%>
			
			<%if tieneGastosEnvioEntrega then%>			
				<%bordeFila=""
                if not tieneComparador then
                    bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "
                end if%>                        
                <tr>
                    <td  class="tituloCeldaCabAgrupacion celdaGris" <%=bordeFila%> rowspan="<%=numRowSpan%>"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto7")%> - <%=objIdioma.getIdioma("EstadisticoPedidos_Texto8")%>(�)</td>            
                    <%if tieneComparador then%>
	                    <td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=Anno%></td>                                
                    <%end if                    
                    numElementosMes=0
                     valorElementosMes=0
                     For contMes=1 to maximoFilter%>
                        <td <%=bordeFila%>>
                            <%if isnull(arrGastosEnvioEntrega(contMes)) then
                                response.Write("-")						
                            elseif clng(arrGastosEnvioEntrega(contMes))=0 then
                                response.Write("-")
                            else
                                response.Write(formatnumber(arrGastosEnvioEntrega(contMes),2))
                            end if%>
                        </td>                                        
                     <%numElementosMes=numElementosMes + 1
                    next%>   
                    <td class="celdaGrisClaro" <%=bordeFila%> >
                        <%=formatnumber(mediaGastosEnvioEntrega,2)%>				
                    </td> 	
                    <td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,2)%></td>    				
                </tr>  
            
                  <%if tieneComparador then
						for contComp=0 to numAnnosComp
							bordeFila=""				  
						  if contComp=numAnnosComp then
							bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "					  
						  end if 	%>
						  <tr class="filaTablaResumenGeneral">
							<td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=arrFiltroComp(contComp)%></td>                                                      	
							<%numElementosMes=0
							 valorElementosMes=0
							 For contMes=1 to maximoFilter%>
								<td <%=bordeFila%> >
									<%if arrGastosEnvioEntregaComp(contMes,contComp)=0 then
										response.Write("-")
									else
										response.Write(formatnumber(arrGastosEnvioEntregaComp(contMes,contComp),2))
									end if%>
								</td>
							<%numElementosMes=numElementosMes + 1
							valorElementosMes=valorElementosMes + arrGastosEnvioEntregaComp(contMes,contComp)
							next
							if numElementosMes=0 then
								mediaValoresMes=valorElementosMes
							 else
								mediaValoresMes=valorElementosMes/numElementosMes				 
							 end if   %>
							<td class="celdaGrisClaro" <%=bordeFila%> >
								<%=formatnumber(arrMediaGastosEnvioEntrega(contComp),2)%>				
							</td>    	
							<td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,0)%></td>    			 
						  </tr>                        
					  <%next
					  end if%>                
			<%end if%>
			
			<%if tieneGastosEnvioDevolucion then%>
				<%bordeFila=""
                if not tieneComparador then
                    bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "
                end if%>                        
                <tr>
                    <td  class="tituloCeldaCabAgrupacion celdaGris" <%=bordeFila%> rowspan="<%=numRowSpan%>"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto7")%> - <%=objIdioma.getIdioma("EstadisticoPedidos_Texto9")%>(�)</td>            
                    <%if tieneComparador then%>
	                    <td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=Anno%></td>                                
                    <%end if                             
                    numElementosMes=0
                     valorElementosMes=0
                     For contMes=1 to maximoFilter%>
                        <td <%=bordeFila%> >
                            <%if isnull(arrGastosEnvioDevolucion(contMes)) then
                                response.Write("-")						
                            elseif clng(arrGastosEnvioDevolucion(contMes))=0 then
                                response.Write("-")
                            else
                                response.Write(formatnumber(arrGastosEnvioDevolucion(contMes),2))
                            end if%>
                        </td>                                        
                     <%numElementosMes=numElementosMes + 1
                    next%>   
                    <td class="celdaGrisClaro" <%=bordeFila%>>
                        <%=formatnumber(mediaGastosEnvioDevolucion,2)%>				
                    </td> 
                    <td class="celdaGrisClaro" <%=bordeFila%>  ><%=formatnumber(valorElementosMes,2)%></td>    					
               </tr>  
           
                  <%if tieneComparador then
						for contComp=0 to numAnnosComp
							bordeFila=""				  
						  if contComp=numAnnosComp then
							bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "					  
						  end if 	%>
						  <tr class="filaTablaResumenGeneral">
							<td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=arrFiltroComp(contComp)%></td>                                                      	
							<%numElementosMes=0
							 valorElementosMes=0
							 For contMes=1 to maximoFilter%>
								<td <%=bordeFila%> >
									<%if arrGastosEnvioDevolucionComp(contMes,contComp)=0 then
										response.Write("-")
									else
										response.Write(formatnumber(arrGastosEnvioDevolucionComp(contMes,contComp),2))
									end if%>
								</td>
							<%numElementosMes=numElementosMes + 1
							valorElementosMes=valorElementosMes + arrGastosEnvioDevolucionComp(contMes,contComp)
							next
							if numElementosMes=0 then
								mediaValoresMes=valorElementosMes
							 else
								mediaValoresMes=valorElementosMes/numElementosMes				 
							 end if   %>
							<td class="celdaGrisClaro" <%=bordeFila%> >
								<%=formatnumber(arrMediaGastosEnvioDevolucion(contComp),2)%>				
							</td>    	
							<td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,0)%></td>    			 
						  </tr>                        
					  <%next
					  end if%>               
		   <%end if%>
           
			<%if tieneGastosEnvioCambioTalla then%>		
				<%bordeFila=""
                if not tieneComparador then
                    bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "
                end if%>                           
                <tr>
                    <td  class="tituloCeldaCabAgrupacion celdaGris" <%=bordeFila%> rowspan="<%=numRowSpan%>"><%=objIdioma.getIdioma("EstadisticoPedidos_Texto7")%> - <%=objIdioma.getIdioma("EstadisticoPedidos_Texto10")%>(�)</td>            
                    <%if tieneComparador then%>
	                    <td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=Anno%></td>                                
                    <%end if                                                 
                    numElementosMes=0
                     valorElementosMes=0
                     For contMes=1 to maximoFilter%>
                        <td <%=bordeFila%>>
                            <%if isnull(arrGastosEnvioCambioTalla(contMes)) then
                                response.Write("-")						
                            elseif clng(arrGastosEnvioCambioTalla(contMes))=0 then
                                response.Write("-")
                            else
                                response.Write(formatnumber(arrGastosEnvioCambioTalla(contMes),2))
                            end if%>
                        </td>                                        
                     <%numElementosMes=numElementosMes + 1
                    next%>   
                    <td class="celdaGrisClaro" <%=bordeFila%> >
                        <%=formatnumber(mediaGastosEnvioCambioTalla,2)%>				
                    </td> 	
                    <td class="celdaGrisClaro" <%=bordeFila%>  ><%=formatnumber(valorElementosMes,2)%></td>    				
               </tr>   
           
                  <%if tieneComparador then
						for contComp=0 to numAnnosComp
							bordeFila=""				  
						  if contComp=numAnnosComp then
							bordeFila=" style=""border-bottom:solid 1px #CDCDCD;"" "					  
						  end if 	%>
						  <tr class="filaTablaResumenGeneral">
							<td  class="tituloCeldaCabAgrupacion celdaGrisClaro" <%=bordeFila%> ><%=arrFiltroComp(contComp)%></td>                                                      	
							<%numElementosMes=0
							 valorElementosMes=0
							 For contMes=1 to maximoFilter%>
								<td <%=bordeFila%> >
									<%if arrGastosEnvioCambioTallaComp(contMes,contComp)=0 then
										response.Write("-")
									else
										response.Write(formatnumber(arrGastosEnvioCambioTallaComp(contMes,contComp),2))
									end if%>
								</td>
							<%numElementosMes=numElementosMes + 1
							valorElementosMes=valorElementosMes + arrGastosEnvioCambioTallaComp(contMes,contComp)
							next
							if numElementosMes=0 then
								mediaValoresMes=valorElementosMes
							 else
								mediaValoresMes=valorElementosMes/numElementosMes				 
							 end if   %>
							<td class="celdaGrisClaro" <%=bordeFila%> >
								<%=formatnumber(arrMediaGastosEnvioCambioTalla(contComp),2)%>				
							</td>    	
							<td class="celdaGrisClaro" <%=bordeFila%> ><%=formatnumber(valorElementosMes,0)%></td>    			 
						  </tr>                        
					  <%next
					  end if%>               
		   <%end if%> 
		            
          </table>  


	
        <div style="height:20px; width:100%; clear:both;"></div>	 

</form>	        


<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->




