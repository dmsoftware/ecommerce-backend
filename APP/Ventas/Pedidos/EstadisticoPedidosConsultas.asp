<%

	
		filtroPedidos=" PED_TRATADO=1 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		filtroPedidosCambios=" And ( PED_PADRE_DEVOLUCION is null Or PED_CODIGO in ( Select PCT.PED_PADRE_DEVOLUCION From PEDIDOS PCT Where PCT.PED_TRATADO=1 And (PCT.PED_ANULADO=0 Or PCT.PED_ANULADO is null ) And PCT.PED_PADRE_DEVOLUCION is not null ) ) "				
		

		sqlEnCambioSelect=" Select count(P.PED_CODIGO) as numPedidosEnCambio "
		sqlEnCambioSelect2=" Select P.PED_CODIGO "		
		sqlEnCambio= " From Pedidos P "
		sqlEnCambio=sqlEnCambio & " Where " & filtroPedidos & filtroPedidosCambios ' PED_TRATADO=1 And (PED_ANULADO=0 Or PED_ANULADO is null )  "
		sqlEnCambio=sqlEnCambio & "	And ( (( Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHAACEPTACION is null ) > 0) "
		sqlEnCambio=sqlEnCambio & " Or (( Select count(PC.PEDLI_CODIGO) From PEDIDOS_LINEAS PC Where PEDLI_SUPEDIDO in ( Select PCT.PED_PADRE_DEVOLUCION From PEDIDOS PCT INNER JOIN PEDIDOS_LINEAS PCL on (PCT.PED_CODIGO=PCL.PEDLI_SUPEDIDO) Where PCT.PED_TRATADO=1 And (PCT.PED_ANULADO=0 Or PCT.PED_ANULADO is null ) And PCT.PED_PADRE_DEVOLUCION is not null And PCL.PEDLI_FECHACAMBIO is null And PCL.PEDLI_SUPRODUCTO_IDERP<>10 And PCL.PEDLI_SUPRODUCTO_IDERP<>20 ) And PC.PEDLI_SUPEDIDO=PED_CODIGO ) > 0) "
		sqlEnCambio=sqlEnCambio & " ) "		
		
		'Pedidos nulos. 'Falta quitar los que tengan lineas buenas
		sqlNulosSelect=" Select count(P.PED_CODIGO) as numPedidosNulos "
		sqlNulosSelect2=" Select P.PED_CODIGO "		
		sqlNulos=" From Pedidos P "
		sqlNulos=sqlNulos & " Where " & filtroPedidos & filtroPedidosCambios 
		sqlNulos=sqlNulos & " And ((Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHASOLICITUD is not null And PD.PEDD_REALIZADO=1  ) > 0) "
		'sqlNulos=sqlNulos & " And ((Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHASOLICITUD is not null And PD.PEDD_REALIZADO=1) = (Select COUNT(PL.PEDLI_CODIGO) * PL.PEDLI_UNIDADES From PEDIDOS_LINEAS PL Where PL.PEDLI_SUPEDIDO=P.PED_CODIGO And PL.PEDLI_SUPRODUCTO_IDERP<>10 And PL.PEDLI_SUPRODUCTO_IDERP<>20 Group by PL.PEDLI_UNIDADES ) )  "			
		
		
		sqlNuevosClientes=" select count(P.codPropietario) as contClientes " 
		sqlNuevosClientes=sqlNuevosClientes & " from PEDIDOS_VENTAS  AS P  "
		sqlNuevosClientes=sqlNuevosClientes & " Where " & filtroPedidos & filtroPedidosCambios 		
		sqlNuevosClientes=sqlNuevosClientes & " and not P.codPropietario is null "		
		
		sqlNumPedidos=" Select count(PED_CODIGO) as contPedidos From PEDIDOS P "
		sqlNumPedidos=sqlNumPedidos & " Where " & filtroPedidos & filtroPedidosCambios & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") " 		
		
		sqlCantPedidos=" Select sum(L.PEDLI_PRECIOPAGADO*L.PEDLI_UNIDADES" & aplicarIva & ") as cantPedidos From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantPedidos=sqlCantPedidos & " Where " & filtroPedidos & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") " 		
											
											
		sqlCantGastosEnvioEntrega=" Select sum(L.PEDLI_PRECIOPAGADO" & aplicarIva & ") as cantGastosEnvioEntrega From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		sqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega & " And P.PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "
		sqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega & " And P.PED_PADRE_DEVOLUCION is null "
		
		sqlCantGastosEnvioDevolucion=" Select sum(CASE WHEN PD.PEDD_COSTEDEVOLUCION is null THEN 0 ELSE PD.PEDD_COSTEDEVOLUCION END) as cantGastosEnvioDevolucion "
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " From PEDIDOS P Inner Join PEDIDOS_DEVOLUCIONES PD on (P.PED_CODIGO=PD.PEDD_SUPEDIDO) "
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " Where " & filtroPedidos & filtroPedidosCambios 
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " And PD.PEDD_FECHAACEPTACION is not null And PD.PEDD_REALIZADO=1 " 	
		
		
		sqlCantGastosEnvioCambioTalla=" Select sum(L.PEDLI_PRECIOPAGADO" & aplicarIva & ") as cantGastosEnvioCambioTalla From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " And P.PED_PADRE_DEVOLUCION not in (" & sqlNulosSelect2 & sqlNulos & ") "
		sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " And P.PED_PADRE_DEVOLUCION is not null "																									
		
		''''filtroPedidosAnulados=" And (PED_ANULADO=0 Or PED_ANULADO is null ) "	

			
%>