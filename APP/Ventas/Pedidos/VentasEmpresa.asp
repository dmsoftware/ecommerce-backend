<%'El nombre del fichero de idiomas es, por ejemplo, Idiomas_Comercial.xml, en la variable ponemos s�lo el men� al que pertenece la p�gina
'S�lo usamos esta variable si la p�gina no est� enganchada en ning�n men� (app_menus)
nomFicheroIdiomas="Ventas"%>
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!-- #include virtual="/dmcrm/includes/permisos.asp"-->

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="shortcut icon" href="/dmcrm/favicon.ico">    
    <title><% if trim(nomSubMenu)<>"" then %><%=nomSubMenu%><%else%>DM GESTION<%end if%></title>


    <!-- #include virtual="/dmcrm/includes/jquery.asp"-->  

    <%'Incluimos los gr�ficos JQUERY ************************************************************%>
    <script type="text/javascript" src="/dmcrm/js/gvChart/jsapi.js"></script>
	<script type="text/javascript" src="/dmcrm/js/gvChart/jquery.gvChart-1.0.1.min.js"></script>
	<script type="text/javascript">
		gvChartInit();
		$(document).ready(function () {
		    $('#tabImportes').gvChart({
		        chartType: 'ColumnChart',
		        gvSettings: {
		            vAxis: { title: ''/*,format: {format:'#.##0,00'}*/  },
		            hAxis: { title: '' },
		            legend: "none",
		            colors: ['#83C375', '#3366CC'],
		            width: '98%'/*window.innerWidth-60*/,
		            height: 120,
					top:0,
					tooltipTextStyle: {color: "#83C375", fontName: "Arial", fontSize: 12},
		            chartArea: { top:10, left: 60, width:'98%'/*window.innerWidth-60*/, height: "75%" }
		        }
		    });

		    $('#tabPedidoMedio').gvChart({
		        chartType: 'ColumnChart',
		        gvSettings: {
		            vAxis: { title: '' },
		            hAxis: { title: '' },
		            legend: "none",
		            colors: ['#FF6600', '#3366CC'],
		            width:  '98%'/*(window.innerWidth/2-60)*/,
		            height: 120,
					top:0,
					tooltipTextStyle: {color: "#FF6600", fontName: "Arial", fontSize: 12},					
		            chartArea: { top:10, left: 60, width: '98%'/*(window.innerWidth/2-60)*/, height: "75%" }
		        }
		    });

		    $('#tabNumPedidos').gvChart({
		        chartType: 'ColumnChart',
		        gvSettings: {
		            vAxis: { title: '' },
		            hAxis: { title: '' },
		            legend: "none",
		            colors: ['#5893D5', '#3366CC'],
		             width:  '100%'/*(window.innerWidth/2-60)*/,
		            height: 120,
					top:0,
					tooltipTextStyle: {color: "#5893D5", fontName: "Arial", fontSize: 12},										
		            chartArea: { left: 60, width: '100%'/*(window.innerWidth/2-60)*/, height: "70%" }
		        }
		    });

		});
	</script>
    <%'Fin .- Incluimos los gr�ficos JQUERY ************************************************************%>


</head>
<body style="margin:0; padding:0;">

<%
sql="Select year(ped_fecha) as ano,sum(PEDLI_PRECIOPAGADO*PEDLI_UNIDADES*100/(100+PEDLI_IVA)) as Importe "
sql=sql & " from (PEDIDOS inner join PEDIDOS_LINEAS on PEDIDOS.PED_CODIGO=PEDIDOS_LINEAS.PEDLI_SUPEDIDO ) left join CLIENTES on PEDIDOS.PED_SUCLIENTEEMPRESA=CLIENTES.CLIE_CODIGO"
sql=sql & " where PED_SUCLIENTEEMPRESA=" &  request("cod")
sql=sql & " group by year(ped_fecha) "
sql=sql & " order by year(ped_fecha) asc "
Set rstVentas= connCRM.AbrirRecordset(sql,3,1)
if not rstVentas.eof then%>

	<div style="float:left; width:32%; margin-left:1%;">
            <b><%=objIdioma.getIdioma("VentasGeneral_Texto3")%></b>
            <table id="tabImportes" style="margin:0px; padding:0px; float:left; " >
                <thead >
                    <tr>
                        <th></th>
                        <%while not rstVentas.eof
                            cont=cont+1%>
                            <th><%=rstVentas("ano")%></th>
                            <%rstVentas.movenext
                        wend %>
                    </tr>
                </thead >
                <tbody >
                    <tr>
                        <th ><%=objIdioma.getIdioma("VentasGeneral_Texto7")%></th>
                        <%rstVentas.movefirst
                        while not rstVentas.eof%>
                            <td><%=rstVentas("importe")%></td>
                            <%rstVentas.movenext
                        wend %>
                    </tr>
                </tbody>
            </table> 
      </div>      

    
    <%rstVentas.movefirst%>
    <div id="divImpMedio" style="  float:left; width:32%; margin-left:1%;  ">
    <b><%=objIdioma.getIdioma("VentasGeneral_Texto4")%></b>    
            <table id="tabPedidoMedio" style="  margin:0px; z-index:2; position:relative;  padding:0px; float:left;  " >
                <thead >
                    <tr>
                        <th></th>
                        <%while not rstVentas.eof%>
                            <th><%=rstVentas("ano")%></th>
                            <%rstVentas.movenext
                        wend %>
                    </tr>
                </thead >
                <tbody >
                    <tr>
                        <th ><%=objIdioma.getIdioma("VentasGeneral_Texto4")%></th>
                        <%rstVentas.movefirst
                        strNumPedidos=""
                        while not rstVentas.eof
                            'Hallamos el n�mero de pedidos de ese a�o
                            sql="Select COUNT(PED_CODIGO) from PEDIDOS "
                            sql=sql & " where PED_SUCLIENTEEMPRESA=" &  request("cod")
                            sql=sql & " and year(PED_FECHA)=" & rstVentas("ano")
                            Set rstNumPedidos= connCRM.AbrirRecordset(sql,3,1)
                            'Preparamos el gr�fico siguiente
                            strNumPedidos=strNumPedidos & "<td>" & rstNumPedidos(0) & "</td>"%>
                            <td><%=round(rstVentas("importe")/rstNumPedidos(0),0)%></td>
                            <%rstNumPedidos.cerrar
                            rstVentas.movenext
                        wend %>
                    </tr>
                </tbody>
            </table> 
    </div>   

    
    <%rstVentas.movefirst%>
    <div id="divImpMedio" style="  float:left; width:32%; margin-left:1%; ">
        <b><%=objIdioma.getIdioma("VentasGeneral_Texto5")%></b>    
        <table id="tabNumPedidos" style=" margin:0px;  padding:0px; float:left;" >
            <thead >
                <tr>
                    <th></th>
                    <%while not rstVentas.eof%>
                        <th><%=rstVentas("ano")%></th>
                        <%rstVentas.movenext
                    wend %>
                </tr>
            </thead >
            <tbody >
                <tr>
                    <th ><%=objIdioma.getIdioma("VentasGeneral_Texto5")%></th>
                    <%=strNumPedidos%>
                </tr>
            </tbody>
        </table> 
	</div>


    <!--<div style="width:100%; height:10px; clear:both; float:left;"></div>-->
    <%'*****************************************************************************
    ' Llamada a la maestra *********************************************************
    '*******************************************************************************
    'Par�metros
    tabla = "PEDIDOS_VENTAS" '"ADM_VENTAS"
    filtro = "PED_SUCLIENTEEMPRESA"
    valorFiltro = request("cod")
    filtro2 = ""
    valorFiltro2 = ""%>
    <div id="gestionclientes" class="contenidoSeccion" style="z-index:1;">
		<div style="clear:both; height:5px;  width:100%;"></div>    
	    <!-- #include virtual="/dmcrm/APP/Comun2/seleccion.asp"-->
    </div>
    <%'******************************************************************************
    '******************************************************************************%>
<%else%>
 No hay ventas.
<%end if
rstVentas.cerrar%>

</body>

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->





