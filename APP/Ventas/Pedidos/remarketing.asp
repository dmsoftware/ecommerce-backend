<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<style>
    #remarketing img { cursor:pointer }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#remarketing img').click(function () {
            var isSelected = ($(this).attr('data-selected') == 1);
            var imageSrc = '/dmcrm/APP/imagenes/' + ((isSelected) ? 'red' : 'green') + '.png';

            $(this).attr('data-selected', ((isSelected) ? '0' : '1'));
            $(this).attr('src', imageSrc);
        });
    });
</script>

<h1 style="border-bottom: solid #ff6600 2px; padding-bottom:5px;"><%=objIdiomaGeneral.getIdioma("Remarketing_Texto1")%></h1>
<p><%=objIdiomaGeneral.getIdioma("Remarketing_Texto2a")%></p>
<p><%=objIdiomaGeneral.getIdioma("Remarketing_Texto2b")%></p>
<br /><br />
<table width="100%" cellspacing="1" cellpadding="2" border="0" id="remarketing">
    <tbody>
        <tr class="nombre">
            <td colspan="5">
                <%=objIdiomaGeneral.getIdioma("Remarketing_Texto2c")%>
            </td>
        </tr>
        <tr class="celdaGris noBorder">
            <td height="27">&nbsp;</td>
            <td width="15%" align="center"><%=objIdiomaGeneral.getIdioma("Remarketing_Texto7")%></td>
            <td width="15%" align="center"><%=objIdiomaGeneral.getIdioma("Remarketing_Texto8")%></td>
            <td width="15%" align="center"><%=objIdiomaGeneral.getIdioma("Remarketing_Texto9")%></td>
            <td width="15%" align="center"><%=objIdiomaGeneral.getIdioma("Remarketing_Texto10")%></td>
        </tr>
        <tr class="celdaBlanca">
            <td><%=objIdiomaGeneral.getIdioma("Remarketing_Texto3")%></td>
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="1-1" /></td>               
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="1-2" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/red.png" alt="" data-selected="0" data-id="1-3" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="1-4" /></td>
        </tr>
        <tr class="celdaBlanca">
            <td><%=objIdiomaGeneral.getIdioma("Remarketing_Texto4")%></td>
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="2-1" /></td>               
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/red.png" alt="" data-selected="0" data-id="2-2" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="2-3" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="2-4" /></td>
        </tr>
        <tr class="celdaBlanca">
            <td><%=objIdiomaGeneral.getIdioma("Remarketing_Texto5")%></td>
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="3-1" /></td>               
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="3-2" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="3-3" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/green.png" alt="" data-selected="1" data-id="3-4" /></td>
        </tr>
        <tr class="celdaBlanca">
            <td><%=objIdiomaGeneral.getIdioma("Remarketing_Texto6")%></td>
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/red.png" alt="" data-selected="0" data-id="4-1" /></td>               
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/red.png" alt="" data-selected="0" data-id="4-2" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/red.png" alt="" data-selected="0" data-id="4-3" /></td>                
            <td align="center"><img width="15" src="/dmcrm/APP/imagenes/red.png" alt="" data-selected="0" data-id="4-4" /></td>
        </tr>
    </tbody>
</table>

<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->