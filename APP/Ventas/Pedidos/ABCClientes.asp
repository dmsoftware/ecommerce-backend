<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->



<%Sub FormatoEuros(valor)
	if not isnull(valor) then
		response.Write FormatNumber(valor, 2) & " �"
	else
		response.Write "0 �"	
	end if	
end sub%>
 
<h1><%=objIdioma.getIdioma("ABCClientes_Titulo")%></h1>

        <span style="font-size:11px; font-weight:normal; font-style:italic; float:right; margin-right:20px;">
            <%if conIVAIncluido then%>
                Importes con el IVA incluido.
            <%else%>
                Importes SIN IVA.                                                    
            <%end if%>
        </span>

<p><%=objIdioma.getIdioma("ABCClientes_Texto2")%></p>

<b><%=objIdioma.getIdioma("ABCClientes_Texto3")%></b><br />
<%=objIdioma.getIdioma("ABCClientes_Texto4")%> 
<%'############################################################################################%>
<% %>
<form name="fPaginacion2" method="POST" ID="Form1" action="ABCClientes.asp?ent=si">
    <div class="FiltradoCab">
	    <div>
                <%sql="Select distinct(year(PED_FECHA)) as Ano from PEDIDOS "
                sql=sql & " order by YEAR(PED_FECHA)"
                Set rstAnos= connCRM.AbrirRecordset(sql,0,1)
                while not rstAnos.eof
                    strChecked=""
                    'Si vengo por primera vez
                    if request("ent")<>"si" then
                        if year(date)=rstAnos("Ano") or year(date)-1=rstAnos("Ano") then 
                            strChecked = " checked"
                            sqlW=sqlW & " or year(PED_FECHA)=" & rstAnos("Ano")
                        end if
                    else
                        'Si entro una vez seleccionado los a�os
                        if request(""&rstAnos("Ano"))="on" then 
                            strChecked = " checked"
                            sqlW=sqlW & " or year(PED_FECHA)=" & rstAnos("Ano")
                        end if
                    end if%>
                    <INPUT TYPE=CHECKBOX NAME="<%=rstAnos("Ano")%>" <%=strChecked%>><%=rstAnos("Ano")%>&nbsp;&nbsp;
                    <%rstAnos.movenext
                wend
                rstAnos.cerrar
                set rstAnos=nothing%>
                <input type="SUBMIT" class="boton" value="<%=objIdiomaGeneral.getIdioma("BotonSeleccionar_Texto1")%>"/>
	    </div>
    </div>
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<%'############################################################################################%>



    <%'Incluimos los gr�ficos JQUERY ************************************************************%>
    <script type="text/javascript" src="/dmcrm/js/gvChart/jsapi.js"></script>
	<script type="text/javascript" src="/dmcrm/js/gvChart/jquery.gvChart-1.0.1.min.js"></script>
	<script type="text/javascript">
	    gvChartInit();
	    $(document).ready(function () {
	        $('#tabImportes').gvChart({
	            chartType: 'AreaChart',
	            gvSettings: {
	                vAxis: { title: ''/*,format: {format:'#.##0,00'}*/ },
	                hAxis: { title: '' },
	                legend: "none",
	                colors: ['#83C375', '#3366CC'],
	                width: window.innerWidth - 60,
	                height: 250,
	                top: 0,
	                tooltipTextStyle: { color: "#83C375", fontName: "Arial", fontSize: 12 },
	                chartArea: { top: 10, left: 60, width: window.innerWidth - 60, height: "75%" }
	            }
	        });

	    });
	</script>
    <%'Fin .- Incluimos los gr�ficos JQUERY ************************************************************%>



<div>
<b><%=objIdioma.getIdioma("ABCClientes_Texto5")%></b>
<%'A�ado VEN_CODIGOEMPRESAERP ya que hay pedidos sin empresa en CRM

		aplicarIva=""
		if not conIVAIncluido then
			aplicarIva="*100/(100+PEDLI_IVA)"
		end if	

sql="Select sum(PEDLI_PRECIOPAGADO*PEDLI_UNIDADES" & aplicarIva & ") as Importe, CLIE_NOMBRE as Empresa, CLIE_CODIGO,PED_SUCLIENTEEMPRESA_IDERP "
sql=sql & " from (PEDIDOS inner join PEDIDOS_LINEAS on PEDIDOS.PED_CODIGO=PEDIDOS_LINEAS.PEDLI_SUPEDIDO ) left join CLIENTES on PEDIDOS.PED_SUCLIENTEEMPRESA=CLIENTES.CLIE_CODIGO"
sql=sql & " where "
sql=sql & " not PED_SUCLIENTEEMPRESA IS NULL " 's�LO EMPRESAS
sql=sql & " and ( 1=0 "
sql=sql & sqlW
sql=sql & " ) "
sql=sql & " group by PED_SUCLIENTEEMPRESA_IDERP,CLIE_CODIGO, CLIE_NOMBRE "

'A�adimos el sql para los clientes que no son empresa sino de la tabla CONTACTOS
sql=sql & " UNION ALL "

sql=sql & " Select sum(PEDLI_PRECIOPAGADO*PEDLI_UNIDADES" & aplicarIva & ") as Importe, CON_NOMBRE+' '+CON_APELLIDO1 as Empresa, CON_CODIGO,PED_SUCONTACTO  "
sql=sql & " from (PEDIDOS inner join PEDIDOS_LINEAS on PEDIDOS.PED_CODIGO=PEDIDOS_LINEAS.PEDLI_SUPEDIDO ) " 
sql=sql & " left join CONTACTOS on PEDIDOS.PED_SUCONTACTO=CONTACTOS.CON_CODIGO " 
sql=sql & " where "
sql=sql & " not PED_SUCONTACTO IS NULL " 's�LO PARTICULARES
sql=sql & " and ( 1=0 "
sql=sql & sqlW
sql=sql & " ) "
sql=sql & " group by PED_SUCONTACTO, CON_CODIGO, CON_NOMBRE+' '+CON_APELLIDO1  "
sql=sql & " order by sum(PEDLI_PRECIOPAGADO*PEDLI_UNIDADES" & aplicarIva & ") DESC "

Set rstABC= connCRM.AbrirRecordset(sql,3,1)
'Hallamos el n�mero de clientes y el total vendido a todos los clientes
intNumClientes=rstABC.recordcount
while not rstABC.eof
	if isnumeric(rstABC("importe")) then
	    intTotal=intTotal+rstABC("importe")
	end if
    rstABC.movenext
wend
if intNumClientes>0 then rstABC.movefirst%>

<table id="tabImportes" style="margin:0px; padding:0px; float:left; " >
	<thead >
		<tr>
			<th></th>
            <%while not rstABC.eof
                contC=contC+1%>
                <th><%=contC%></th>
                <%rstABC.movenext
            wend%>
		</tr>
	</thead >
	<tbody >
		<tr>
			<th ><%=objIdioma.getIdioma("ABCClientes_Texto6")%></th>
            <%rstABC.movefirst
            while not rstABC.eof%>
                <td><%=rstABC("Importe")%></td>
                <%rstABC.movenext
            wend %>
		</tr>
	</tbody>
</table> 
</div>


<b><%=objIdioma.getIdioma("ABCClientes_Texto7")%></b>
<table border=1>
    <tr>
        <td>#</td>
        <td><%=objIdioma.getIdioma("ABCClientes_Texto8")%></td>
        <td><%=objIdioma.getIdioma("ABCClientes_Texto9")%></td>
        <td><%=objIdioma.getIdioma("ABCClientes_Texto10")%></td>
        <td><%=objIdioma.getIdioma("ABCClientes_Texto11")%></td>
        <td><%=objIdioma.getIdioma("ABCClientes_Texto12")%></td>
        <td><%=objIdioma.getIdioma("ABCClientes_Texto13")%></td>
    </tr>
<%
if intNumClientes>0 then rstABC.movefirst
while not rstABC.eof
    cont=cont+1
	if isnumeric(rstABC("Importe")) then
	    acumulado=acumulado+rstABC("Importe")
	end if%>
    <%'Seleccionamos el grupo A
    if blnGrupoA<>true then
        blnGrupoA=true%>
        <tr><td colspan=7><b><%=objIdioma.getIdioma("ABCClientes_Texto14")%></b></td></tr>
    <%end if%>
    <%'Seleccionamos el grupo B
    if blnGrupoB<>true and dblAcumulado>=80 then
        blnGrupoB=true%>
        <tr><td colspan=7><b><%=objIdioma.getIdioma("ABCClientes_Texto15")%></b></td></tr>
    <%end if%>
    <%'Seleccionamos el grupo C
    if blnGrupoC<>true and dblAcumulado>=95 then
        blnGrupoC=true%>
        <tr><td colspan=7><b><%=objIdioma.getIdioma("ABCClientes_Texto16")%></b></td></tr>
    <%end if%>
    
    <tr>
        <td>
            <%=cont%>
        </td>
        <td>
            <%if not isnull(rstABC("Empresa")) then
                response.write rstABC("Empresa")
             else
                response.write "<FONT COLOR=777777>" & objIdioma.getIdioma("ABCClientes_Texto19") & " - " & rstABC("PED_SUCLIENTEEMPRESA_IDERP")
             end if%>
        </td>
        <td>
            <%=round((cont*100)/intNumClientes,2)%> %
        </td>
        
        <td>
            <%FormatoEuros(rstABC("Importe"))%>
        </td>
        <td>
        	<%if not isnull(rstABC("importe")) then%>
				<%if isnumeric(rstABC("importe")) then%>
                <%=round((rstABC("importe")*100)/intTotal,2)%> %
                <%else%>
                <%end if%>
            <%end if%>
        </td>
        <td>
            <%FormatoEuros(acumulado)%>
        </td>
        <td>
        	<%if not isnull(acumulado) then%>        
            <%if isnumeric(acumulado) And isnumeric(intTotal) And intTotal>0 then%>
	            <%dblAcumulado=round((acumulado*100)/intTotal,2)
			end if
            Response.write dblAcumulado%> %
            <%end if%>            
        </td>
    </tr>
    <%rstABC.movenext
wend
rstABC.cerrar%>

<tr><td colspan=7><b><%=objIdioma.getIdioma("ABCClientes_Texto17")%></b></td></tr>
<tr>
    <td>
        <%=cont%>
    </td>
    <td>
        <%=objIdioma.getIdioma("ABCClientes_Texto18")%>
    </td>
    <td>
        100 %
    </td>
    <td>
        <%FormatoEuros(acumulado)%>
    </td>
    <td>
        100 %
    </td>
    <td>
        <%FormatoEuros(acumulado)%>
    </td>
    <td>
        100 %
    </td>
</tr>
</table>



<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->





