<!-- #include virtual="/dmcrm/includes/permisos.asp"-->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="/dmcrm/css/estilos.css" />
        <title>Ayuda</title>
        <style type="text/css">
            body { margin:0 }
            p { background-color: #015b9f; color:#FFF; padding:5px 10px; font-size:13px; }
            form { margin:0 0 20px 0}
            #frmExportarCostes { padding:45px 25px; background-color:#f6f6f6}
            label {display:inline-block; width:80px; text-align: right; margin-right: 5px}
            #fechaInicio, #fechaFin {width:80px;}
            #btoExportar { background-color: #FF6600; border: medium none; border-radius: 5px; color: #FFFFFF; cursor: pointer; padding: 3px 20px; margin-left:30px }
            #btoExportar:hover { background: none repeat scroll 0 0 #282828; color: #FFFFFF; cursor: pointer }
            #rdFechaCompra + span, #rdFechaFactura + span { top:-1px; position:relative }
        </style>
        <link rel="stylesheet" type="text/css" media="screen" href="/dmcrm/js/JSCalendar/datePicker.css">

        <script type="text/javascript" src="/dmcrm/js/jquery/jquery.js"></script>
        <script type="text/javascript">
            Date.dayNames = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
            Date.abbrDayNames = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'];
            Date.monthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
            Date.abbrMonthNames = ['Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene', 'Ene'];
            Date.firstDayOfWeek = 1;
            Date.format = 'dd/mm/yyyy';
            Date.fullYearStart = '20';
        </script>

        <script type="text/javascript" src="/dmcrm/js/JSCalendar/date.js"></script>
		<script type="text/javascript" src="/dmcrm/js/JSCalendar/jquery.datePicker.js"></script>
		
        <script type="text/javascript" >
            $(function () {
                $('.cajaTextoFecha').datePicker({ startDate: '01/01/1900', clickInput: true });
                $('.cajaTextoFecha').dpSetOffset(20, 0);
                $('.cajaTextoModalFecha').datePicker({ startDate: '01/01/1900', clickInput: true });
                $('.cajaTextoModalFecha').dpSetOffset(20, 0);

                $('#btoExportarCostes').click(function () {
                    var fechaInicio = $('#fechaInicio').val();
                    var fechaFin = $('#fechaInicio').val();

                    if (fechaInicio == '' || fechaFin == '') {
                        alert('Las fechas seleccionadas no son v�lidas');
                        return false;
                    }
                });

                $('#btoExportarCostes').click(function () {
                    var fechaInicio = $('#fechaInicio').val();
                    var fechaFin = $('#fechaInicio').val();

                    if (fechaInicio == '' || fechaFin == '') {
                        alert('Las fechas seleccionadas no son v�lidas');
                        return false;
                    }
                });
            });        
		</script>
    </head>
    <body>
        <p><strong>Selecciona la fecha inicio y fecha fin de los pedidos facturados a exportar.</strong></p>
        <form id="frmExportarCostes" action="pedidosweb-exportar-costes.asp" method="post">
            
            <input type="radio" value="1" id="rdFechaCompra" name="cbFecha" checked="checked" /><span>Fecha de compra</span>
            <input type="radio" value="2" id="rdFechaFactura" name="cbFecha" /><span>Fecha de factura</span>
            
            <br /><br />

            <input type="checkbox" value="1" id="cbOmitirDevoluciones" name="cbOmitirDevoluciones" /> Omitir pedidos cancelados / devueltos

            <br /><br />

            <label for="fechaInicio">Fecha inicio:</label>
            <input type="text" value="" maxlength="10" class="cajaTextoFecha dp-applied" name="fechaInicio" id="fechaInicio" readonly="readonly" />
            
            <label for="fechaFin">Fecha fin:</label>
            <input type="text" value="" maxlength="10" class="cajaTextoFecha dp-applied" name="fechaFin" id="fechaFin" readonly="readonly" />

            <input id="btoExportar" type="submit" value="Exportar" />
        </form>
    </body>
</html>



