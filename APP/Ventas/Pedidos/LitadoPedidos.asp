<!--#include virtual="/dmcrm/APP/comun2/val_comun.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<%

annoListado=request("anno")
mesListado=request("mes")


'Response.Buffer=false
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment;filename=Pedidos_" & mesListado & "-" & annoListado & ".xls"
%> 
<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>

<style>
<!--
@page
	{margin:.39in .39in .39in .39in;
	mso-header-margin:0in;
	mso-footer-margin:0in;
	mso-page-orientation:landscape;}
br{mso-data-placement:same-cell;}
td{white-space:nowrap;
   mso-ignore:colspan;
   mso-width-source:50;
	}
.t{mso-pattern:auto none;
  white-space:normal;
	}
-->
</style>
	<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
	</head>
<body>
<%
'parametros
if trim(annoListado)<>"" And trim(mesListado)<>"" then
'creamos la l�nea cabecera

%>
<TABLE cellPadding="2" width="100%" border=1 bordercolor="#C0C0C0">
<tr> 
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">C�digo CRM</font>
    </td>
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">C�digo ERP</font>
    </td>    
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">Fecha</font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">P.V.P.</font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">Precio pagado</font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">Gastos env�o</font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#ffffff" face="verdana" size="1">Desuentos</font>
    </td>            
   
</tr>       
<%   

	numPedidos=0
	cantOriginal=0
	cantPagado=0
	cantGastosEnvio=0
	cantDescuentos=0
	sql=" Select P.PED_CODIGO,P.PED_ID_IDERP,P.PED_FECHA, sum(L.PEDLI_PRECIOORIGINAL*L.PEDLI_UNIDADES) as precioOriginal ,sum(L.PEDLI_PRECIOPAGADO*L.PEDLI_UNIDADES) as precioPagado "
	sql=sql & ", ( Select sum(L2.PEDLI_PRECIOPAGADO) From PEDIDOS_LINEAS L2 Where L2.PEDLI_SUPEDIDO=P.PED_CODIGO And L2.PEDLI_SUPRODUCTO_IDERP=10 ) as gastosEnvio "
	sql=sql & ", ( Select sum(L3.PEDLI_PRECIOPAGADO) From PEDIDOS_LINEAS L3 Where L3.PEDLI_SUPEDIDO=P.PED_CODIGO And L3.PEDLI_SUPRODUCTO_IDERP=20 ) as Descuentos "	
	sql=sql & " From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
	sql=sql & " Where YEAR(P.PED_FECHA)=" & annoListado & " And MONTH(P.PED_FECHA)=" & mesListado & " And P.PED_TRATADO=1 And P.PED_PADRE_DEVOLUCION is null "
	sql=sql & " Group by  P.PED_CODIGO,P.PED_ID_IDERP,P.PED_FECHA "	
	sql=sql & " Order by P.PED_FECHA ASC "

	Set rsPedidos = connCRM.AbrirRecordSet(sql,3,1) 

	while not rsPedidos.eof
 	%><tr align="left">
	
				<td VALIGN="middle" height="25" align="center" >
					<font face="verdana" size="1" color="000000"><%=rsPedidos("PED_CODIGO")%></font>
				</td>					  
				<td VALIGN="middle" height=25 align="center">
					<font face="verdana" size="1" color="000000"><%=rsPedidos("PED_ID_IDERP")%></font>
				</td>					                  
				<td VALIGN="middle" height=25 align="center">
					<font face="verdana" size="1" color="000000"><%=rsPedidos("PED_FECHA")%></font>
				</td>					                  
				<td VALIGN="middle" height=25 align="right">
					<font face="verdana" size="1" color="000000">
                    <%if not isnull(rsPedidos("precioOriginal")) then
						response.Write(formatnumber(rsPedidos("precioOriginal"),2) & " ")
					else
						response.Write("0")						
					end if%>
                    </font>
				</td>					                  
				<td VALIGN="middle" height=25 align="right">
					<font face="verdana" size="1" color="000000">
                    <%if  not isnull(rsPedidos("precioPagado")) then
	                    response.Write(formatnumber(rsPedidos("precioPagado"),2) & " ")
					else
						response.Write("0")						
					end if%>						
					</font>
				</td>					                  
				<td VALIGN="middle" height=25 align="right">
					<font face="verdana" size="1" color="000000">
                    <%if  not isnull(rsPedidos("gastosEnvio")) then					
                    	response.Write(formatnumber(rsPedidos("gastosEnvio"),2) & " ")
					else
						response.Write("0")						
					end if%>											
					</font>
				</td>					                  
				<td VALIGN="middle" height=25 align="right">
					<font face="verdana" size="1" color="000000">
                    <%if  not isnull(rsPedidos("Descuentos")) then										
                    	response.Write(formatnumber(rsPedidos("Descuentos"),2) & " ")
					else
						response.Write("0")
					end if%>																
                    </font>
				</td>					                                                                                                  

		
		</tr>   <%	
		numPedidos=numPedidos + 1
		if not isnull(rsPedidos("precioOriginal")) then
			cantOriginal=cantOriginal + cdbl(rsPedidos("precioOriginal"))
		end if
		if cdbl(rsPedidos("precioPagado")) then		
			cantPagado=cantPagado + cdbl(rsPedidos("precioPagado"))
		end if		
		if not isnull(rsPedidos("gastosEnvio")) then		
			cantGastosEnvio=cantGastosEnvio + cdbl(rsPedidos("gastosEnvio"))
		end if		
		if not isnull(rsPedidos("Descuentos")) then		
			cantDescuentos=cantDescuentos + cdbl(rsPedidos("Descuentos"))
		end if		
		
		rsPedidos.movenext
	wend	
	rsPedidos.cerrar()
	set rsPedidos=nothing	
%>
         
      <tr> 
	<td class="titularT" height="25" width="100" bgcolor="#4D667D" colspan="2">
		<font color="#F1F1F1" face="verdana" size="1">Totales:</font>
    </td>
	<td class="titularT" height="25" width="100" bgcolor="#4D667D" align="right" >
		<font color="#F1F1F1" face="verdana" size="1">N�Pedidos:&nbsp;<%=numPedidos%></font>
    </td>    
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#F1F1F1" face="verdana" size="1"><%=formatnumber(cantOriginal,2) & " "%></font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#F1F1F1" face="verdana" size="1"><%=formatnumber(cantPagado,2) & " "%></font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#F1F1F1" face="verdana" size="1"><%=formatnumber(cantGastosEnvio,2) & " "%></font>
    </td>        
	<td class="titularT" height="25" width="100" bgcolor="#4D667D">
		<font color="#F1F1F1" face="verdana" size="1"><%=formatnumber(cantDescuentos,2) & " "%></font>
    </td>            
   
</tr>    

</table>
<%
end if 'end de if trim(annoListado)<>"" And trim(mesListado)<>"" then

%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->
</body>
</html>


