﻿<%'Abrimos la conexion a DMINTEGRA
blnAbrirConexionDMIntegra=true%>

<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->

<style type="text/css">
/*body { font-family:Arial, Helvetica, sans-serif; font-size:12px;}*/
#CampanasPedidos2 { font-size:11px;}
#CampanasPedidos2 tr th { border-bottom:solid 2px #cdcdcd; background:#008bcb; color:#fff;}
#CampanasPedidos2 tr td { border-bottom:solid 1px #cdcdcd;}
#CampanasPedidos2 tr.DatosSubtotales td { border-bottom:solid 5px #fff; border-top:solid 1px #ff6600; font-weight:bold; background:#f1f1f1;}
#CampanasPedidos2 tr.DatosTotales td { border-bottom:solid 5px #fff; border-top:solid 5px #ff6600; font-weight:bold; background:#cdcdcd;}
#CampanasPedidos2 img {height:20px; margin-left:10px;}
tr.seleccionadoTD td { background:#f1f1f1;}
.seleccionadoTD { background:#f1f1f1;}
.CerrarCapaPedidos { float:right; text-decoration:none; color:#999; font-size:20px;}
</style>

<script type="text/javascript">
	/*$(document).ready(function() {            
		$(".ventanaModal").live('click', function(){
			$(this).fancybox({
				'imageScale'			: true,
				'zoomOpacity'			: true,
				'overlayShow'			: true,
				'overlayOpacity'		: 0.7,
				'overlayColor'			: '#333',
				'centerOnScroll'		: true,
				'zoomSpeedIn'			: 600,
				'zoomSpeedOut'			: 500,
				'transitionIn'			: 'elastic',
				'transitionOut'			: 'elastic',
				'type'					: 'iframe',
				'frameWidth'			: 800,
				'frameHeight'			: '100%',
				'titleShow'		        : false,					
				'onClosed'		        : function() {
					
				}
			}).trigger("click"); 
			return false; 	
			$(this).closest('tr').addClass('seleccionadoTD');
		});					
	});					   				*/
</script>
<a class="CerrarCapaPedidos" href="#">X</a>
<b>Listado de pedidos</b><br />
Campa&ntilde;a: 
    <%if request("mailing")<>"" then
        response.write request("mailing")
    else
        response.write request("f")
    end if%><br />
Aportación: <%=request("v")%> %<br />
Fechas: <%=request("fi")%> - <%=request("ff")%><br /><br />  

<%'<div id="ListadoPedidos">%>
    <table border="0" cellpadding="3" cellspacing="1" id="CampanasPedidos2">
    
        <tr>
        	<th align="center">Tienda</th>
            <th align="right">Cod.Pedido</th>
            <th align="right">Importe Ped.</th>
            <th align="right">Importe Ponderado</th>
            <th align="center">Ver visita</th>
            <th align="center">Ver pedido</th>
        </tr>
    
        <%'Hallamos los detalles de cada campaña
        sql= " Select LOCF_VALOR,PED_CODIGO,PED_ID_IDERP,ImporteBruto,PED_SESION,PED_FECHA,TT_NOMBRE,TT_DOMINIO "
        sql=sql & "	From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS P left join LOC_FUENTES as FUENTES ON P.PED_SESION=FUENTES.LOCF_SESION AND P.PED_IP=FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT) "
        select case request("f")
            case "NIN" 'NINGUNA CAMPAÑA
				sql0= "   Select 100 AS LOCF_VALORAUX,PED_CODIGO, AVG(ImporteBruto) as ImporteBruto,PED_SESION,PED_FECHA,TT_NOMBRE,TT_DOMINIO "
				sql0=sql0 & "	From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS P left join LOC_FUENTES ON P.PED_SESION=LOC_FUENTES.LOCF_SESION AND P.PED_IP=LOC_FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT ) "
				sql0=sql0 & "	Where PED_FECHA>='" & FormatearFechaSQLSever(request("fi")) & "' and PED_FECHA<='" & FormatearFechaSQLSever(request("ff")) & " 23:59:59' and iconEstadoPedido <= 5 "
                sql0=sql0 & "	group by PED_CODIGO, PED_SESION,PED_FECHA,TT_NOMBRE,TT_DOMINIO "
                sql0=sql0 & "	having AVG(locf_valor)=0 or AVG(locf_valor) is null "
            case "SEO" 'SEO
                sql=sql & "	Where LEFT(FUENTES.locf_fuente,4)='bus:'"
            case "SOC" 'REDES SOCIALES
                sql=sql & "	Where LEFT(FUENTES.locf_fuente,4)='soc:'"
            case "EXT" 'ENLACES EXTERNOS
                sql=sql & "	Where LEFT(FUENTES.locf_fuente,4)='ext:'" 
            case else
                sql=sql & "	Where FUENTES.locf_fuente='" & request("f") & "' "
        end select
        sql=sql & "	and LOCF_VALOR=" & request("v")
        sql=sql & "	and PED_FECHA>='" & FormatearFechaSQLSever(request("fi")) & "' and PED_FECHA<='" & FormatearFechaSQLSever(request("ff")) & " 23:59:59' "
        'Condición de pedido pagado o pendiente de pago
        sql=sql & " and iconEstadoPedido <= 5 "
        sql=sql & " Order by LOCF_VALOR desc "
    
        if request("f")="NIN" then
            Set rstDetalle = connDMI.AbrirRecordset(sql0,0,1)
        else
            Set rstDetalle = connDMI.AbrirRecordset(sql,0,1)
        end if
    
        while not rstDetalle.eof
            PEDFECHA=DAY(rstDetalle("PED_FECHA")) & "/" & MONTH(rstDetalle("PED_FECHA")) & "/" & YEAR(rstDetalle("PED_FECHA"))
            PEDHORA=hour(rstDetalle("PED_FECHA")) & ":" & minute(rstDetalle("PED_FECHA")) & ":" & second(rstDetalle("PED_FECHA"))
            ImporteBruto=0
            if not isnull(rstDetalle("ImporteBruto")) then ImporteBruto=cdbl(rstDetalle("ImporteBruto"))%>
            <tr>
            	<td align="left"><%=rstDetalle("TT_NOMBRE")%></td>
                <td align="center">
                    <%=rstDetalle("PED_CODIGO")%>
                    <%if not isnull(rstDetalle("PED_ID_IDERP")) then response.write "/" & rstDetalle("PED_ID_IDERP")%>
                </td>
                <td align="right"><%=ImporteBruto%> €</td>
                <td align="right"><%=formatnumber(ImporteBruto*cdbl(request("v"))/100,2)%> €</td>
                <td align="center">
					<a class="SeleccionandoTR" href="http://<%=dominioCRM%>/dmcrm/APP/ventas/pedidos/pedidosLineas.asp?codigo=<%=rstDetalle("PED_CODIGO")%>&posCodigo=0&modo=modificacion&conex=&tabla=PEDIDOS_VENTAS&filtro=&valorfiltro=&filtro2=&valorfiltro2=&nummax=&nummin=&cabecera=" target="_blank"><img src="/DMCRM/app/comun2/imagenes/c0.gif" /></a>
                </td>
            </tr>
            <%TotalNumPedidos=TotalNumPedidos + 1
            TotalCantidadPedidos=TotalCantidadPedidos + ImporteBruto
            TotalCantidadPonderado=TotalCantidadPonderado + (ImporteBruto*cdbl(request("v"))/100)
            rstDetalle.movenext
        wend
        rstDetalle.cerrar%>
        <tr class="DatosTotales">
            <td align="center"><%=TotalNumPedidos%></td>
            <td align="right">&nbsp;</td>
            <td align="right"><%=formatnumber(TotalCantidadPedidos,2)%> €</td>
            <td align="right"><%=formatnumber(TotalCantidadPonderado,2)%> €</td>
            <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
         </tr>
    
    </table>                 

<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->





