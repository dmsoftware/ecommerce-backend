<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!-- #include virtual="/dmcrm/conf/inc_func_asp.asp"-->

<html>
	<head>
		<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
		<link rel="stylesheet" href="/dmcrm/css/estilos.css">
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">

		<!-- #include virtual="/dmcrm/includes/jquery.asp"-->   
		<script language="Javascript">
		
		$(document).ready(function() {				   		
			$("#btnGuardarCambio").click(function() {
				if ($('#txtFechaCambio').val()=="") {
					alert("Debe introducir una fecha de resolución del cambio de talla.")
				} else {
					document.formCambioTalla.action="tratarCambioTalla.asp?plc=<%=request("plc")%>&gu=1";
					document.formCambioTalla.submit();					
				}
			});	
			
		});	
		
		</script>	
	
<style type="text/css">
.etiqDevol { font-size:12px;}
</style>      	
	
	</head>
	<body style="margin:0; padding:0;"  >
    
    <%
	suLineaPedido=request("plc")


	if trim(request("gu"))="1" then
		
		
		fechaActual=date()
		if trim(request.Form("txtFechaCambio"))<>"" then
			fechaActual=request.Form("txtFechaCambio")
		end if
		
		sql=""
		if trim(lcase(request.Form("cboEstadoCambio")))="0" then		
			sql=" Update PEDIDOS_LINEAS "
			sql=sql & " Set PEDLI_FECHACAMBIO=NULL, PEDLI_CAMBIOREALIZADO=NULL "
			sql=sql & " Where PEDLI_CODIGO=" & suLineaPedido 
		elseif trim(lcase(request.Form("cboEstadoCambio")))="1" then		
			sql=" Update PEDIDOS_LINEAS "
			sql=sql & " Set PEDLI_FECHACAMBIO='" & FormatearFechaSQLSever(fechaActual) & "', PEDLI_CAMBIOREALIZADO=1 "
			sql=sql & " Where PEDLI_CODIGO=" & suLineaPedido 
		elseif trim(lcase(request.Form("cboEstadoCambio")))="2" then		
			sql=" Update PEDIDOS_LINEAS "
			sql=sql & " Set PEDLI_FECHACAMBIO='" & FormatearFechaSQLSever(fechaActual) & "', PEDLI_CAMBIOREALIZADO=0 "
			sql=sql & " Where PEDLI_CODIGO=" & suLineaPedido 
		end if		
		
		if sql<>"" then
			connCRM.execute sql,intNumRegistros
		end if
	
		%>
        <script language="javascript" type="text/javascript">
			$(document).ready(function() {
				parent.$.fancybox.close();
			});			
		</script>
         <%
	
	else 'else de if trim(request("gu"))="1" then
	
	
		estadoPedido=0
		fecha=""
		sql="Select PEDLI_CAMBIOREALIZADO,convert(varchar,PEDLI_FECHACAMBIO," & obtenerFormatoFechaSql() & ") as fechaAceptacion From PEDIDOS_LINEAS Where PEDLI_CODIGO=" & suLineaPedido 

        Set rsPedidos = connCRM.AbrirRecordset(sql,3,1)


		if not rsPedidos.eof then
			if not isnull(rsPedidos("PEDLI_CAMBIOREALIZADO")) then
				if rsPedidos("PEDLI_CAMBIOREALIZADO") then
					estadoPedido=1
				else
					estadoPedido=2				
				end if
			end if

			if not isnull(rsPedidos("fechaAceptacion")) then
				fecha=rsPedidos("fechaAceptacion")			
			end if
			
			if trim(fecha)="" then
				fecha=date()
			end if
			
		end if
        rsPedidos.cerrar
        Set rsPedidos = nothing				
		
	
		%>	

		<form id="formCambioTalla" name="formCambioTalla" runat="server" method="post"  action="tratarCambioTalla.asp?plc=<%=suLineaPedido%>&gu=1" >
			<h1>Cambio de talla</h1>
			<table style="width:100%;">
				<tr>
					<td align="right"><label class="etiqDevol" for="cboEstadoCambio">Cambio realizado:</label></td>
					<td align="left"><!--<input type="checkbox" id="chkRealizadoDevolucion" name="chkRealizadoDevolucion" checked />-->
                    <select id="cboEstadoCambio" name="cboEstadoCambio" style="width:200px;" >
                    	<option value="0" <%if estadoPedido=0 then%> selected<%end if%> >Pendiente</option>
                    	<option value="1" <%if estadoPedido=1 then%> selected<%end if%> >Aceptado</option>
                    	<option value="2" <%if estadoPedido=2 then%> selected<%end if%> >Rechazado</option>                                                
                    </select>

                    </td>                
				</tr>
				<tr>
					<td align="right"><label class="etiqDevol" for="txtFechaCambio">Fecha cambio:</label></td>
					<td align="left"><input type="text" id="txtFechaCambio" name="txtFechaCambio" class="cajaTextoFecha" value="<%=fecha%>" style="width:80px;"  /></td>                
				</tr>
				<tr>
					<td colspan="2" align="right" style="padding-right:60px;">

					<input type="button" id="btnGuardarCambio" name="btnGuardarCambio"  value="Guardar" class="boton"  />         
					<input type="button" id="btnSalirCambio" name="btnSalirCambio"  value="Salir" class="boton" onClick="javascript:parent.$.fancybox.close();" />                 
					</td>
				</tr>            
			</table>
	
		</form>
     <%end if 'end de if trim(request("gu"))="1" then%>   
    


	</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->