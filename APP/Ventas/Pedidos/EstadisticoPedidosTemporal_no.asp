    
<form id="estadisticasPedidos" name="estadisticasPedidos" runat="server"  method="post" action="<%=nomPaginaestadisticas%>.asp?modo=<%=request("modo")%>"  >    

	<%
	
Function ultimoDiaMes(iMes, iAnno)
	siguienteMes = DateAdd("m", 1, DateSerial(iAnno, iMes, "01"))
	ultimoDiaMes = Day(DateAdd("d", -1, siguienteMes))
End Function
	
		modoEstadistica=request("modo")
		'if modoEstadistica<>"" then
		'	if modoEstadistica="y" then
		'		modoEstadistica=""
		'	else
		'		modoEstadistica="?anno=" & year(getdate()) & "&mes=" & month(date())
		'	end if
		'	response.Redirect("EstadisticoPedidosTemporal.asp" & modoEstadistica)
		'end if

        'Distinguimos si hay que presentar vista A�OS, A�O, MES
		esAnual=true
		
		Anno=request("anno")

		if (Anno="" or Anno="0") And modoEstadistica="y" then
            ModoVista="1ANOS"
            Anno=""
            textoMeses="A�os"
            'Hallamos el n�mero de a�os disponibles en la tabla pedidos
		    sql=" Select distinct(year(PED_FECHA)) From PEDIDOS order by year(PED_FECHA)"
		    Set rstNumAnos = connCRM.AbrirRecordSet(sql,3,1)
            maximoFilter=rstNumAnos.recordcount
            AnoInicial=rstNumAnos(0)
            rstNumAnos.cerrar
        else
			Anno=cstr(request("anno"))
		    maximoFilter=12
		    Mes=request("mes")

			if modoEstadistica="m" And (trim(Mes)="" Or trim(Mes)="0" ) then
				Mes=month(date())
				Anno=year(date())
			end if
		    if trim(Mes)="" Or trim(Mes)="0" then
                ModoVista="2ANO"
                textoMeses="Meses"
			    Mes="0"
		    else
                ModoVista="3MES"
			    maximoFilter=ultimoDiaMes(Mes,Anno)
			    esAnual=false	
			    textoMeses="D�as"
		    end if	

		end if
		
		if Anno="" And  not trim(request("anno"))="0" then 
			Anno=cstr(year(date()))
		    maximoFilter=12			
			ModoVista="2ANO"
			textoMeses="Meses"
			Mes="0"
		end if		
		
		fechaInicioConf=""
		sqlIniConf=" Select top 1 C.CONF_FINICIO_ESTADISTICAS "
		sqlIniConf=sqlIniConf & " From CONFIGURACION C "		
		Set rsIniConf = connCRM.AbrirRecordSet(sqlIniConf,3,1) 	
		if not rsIniConf.eof then
			if not isnull(rsIniConf("CONF_FINICIO_ESTADISTICAS")) then
				fechaInicioConf=rsIniConf("CONF_FINICIO_ESTADISTICAS")
			end if
		end if
		rsIniConf.cerrar
		set rsIniConf=nothing		

		numNuevosClientes=0
		numNuevosClientesGeneral=0		
		
		numPedidos=0
		numPedidosGeneral=0		
		
		cantIngresos=0
		cantIngresosGeneral=0	
		
		cantVentasProductos=0
		cantVentasProductosGeneral=0
		
		cantIngresosVentas=0				
		cantIngresosVentasGeneral=0						
		
		cantGastosEnvio=0
		cantGastosEnvioGeneral=0		
		
		cantResumenGastosEnvioDomicilio=0
		cantResumenGastosEnvioDomicilioGeneral=0
				
		cantResumenGastosEnvioDevolucion=0
		cantResumenGastosEnvioDevolucionGeneral=0
				
		cantResumenGastosEnvioCambioTalla=0								
		cantResumenGastosEnvioCambioTallaGeneral=0										
		
		'filtroPedidosAnulados=" And (PED_ANULADO=0 Or PED_ANULADO is null ) "	

		''Obtener clientes registrados en el periodo filtrado
		sqlNuevosClientes=" Select count(CON_CODIGO) as contClientes From CONTACTOS C "
        sqlNuevosClientes=sqlNuevosClientes & " Where "
		Select case modoVista
            case "1ANOS"
                sqlNuevosClientes=sqlNuevosClientes & " 1=1 "
            case "2ANO"
                sqlNuevosClientes=sqlNuevosClientes & " YEAR(C.CON_FECHAALTA)=" & Anno
            case "3MES"
                sqlNuevosClientes=sqlNuevosClientes & " YEAR(C.CON_FECHAALTA)=" & Anno & " and month(C.CON_FECHAALTA)=" & Mes
        end select

		Set rsNuevosClientes = connCRM.AbrirRecordSet(sqlNuevosClientes,3,1) 	
		if not rsNuevosClientes.eof then
			numNuevosClientes=rsNuevosClientes("contClientes")
		end if
		rsNuevosClientes.cerrar()
		set rsNuevosClientes=nothing
        	
			
'select PED_SUCLIENTEEMPRESA from PEDIDOS  AS P 
'where year(PED_FECHA)=2012
'and not PED_SUCLIENTEEMPRESA is null 
'and PED_SUCLIENTEEMPRESA not in (select PED_SUCLIENTEEMPRESA from PEDIDOS where year(PED_fecha)<2012 and PED_SUCLIENTEEMPRESA= P.PED_SUCLIENTEEMPRESA)
'order by PED_SUCLIENTEEMPRESA
			
			
		''Obtener clientes registrados en total
		sqlNuevosClientes=" Select count(CON_CODIGO) as contClientes From CONTACTOS C "
		Set rsNuevosClientes = connCRM.AbrirRecordSet(sqlNuevosClientes,3,1) 	
		if not rsNuevosClientes.eof then
			numNuevosClientesGeneral=rsNuevosClientes("contClientes")
		end if
		rsNuevosClientes.cerrar()
		set rsNuevosClientes=nothing		


		filtroPedidos=" PED_TRATADO=1 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		filtroPedidosCambios=" And ( PED_PADRE_DEVOLUCION is null Or PED_CODIGO in ( Select PCT.PED_PADRE_DEVOLUCION From PEDIDOS PCT Where PCT.PED_TRATADO=1 And (PCT.PED_ANULADO=0 Or PCT.PED_ANULADO is null ) And PCT.PED_PADRE_DEVOLUCION is not null ) ) "				
		

		sqlEnCambioSelect=" Select count(P.PED_CODIGO) as numPedidosEnCambio "
		sqlEnCambioSelect2=" Select P.PED_CODIGO "		
		sqlEnCambio= " From Pedidos P "
		sqlEnCambio=sqlEnCambio & " Where " & filtroPedidos & filtroPedidosCambios ' PED_TRATADO=1 And (PED_ANULADO=0 Or PED_ANULADO is null )  "
		sqlEnCambio=sqlEnCambio & "	And ( (( Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHAACEPTACION is null ) > 0) "
		sqlEnCambio=sqlEnCambio & " Or (( Select count(PC.PEDLI_CODIGO) From PEDIDOS_LINEAS PC Where PEDLI_SUPEDIDO in ( Select PCT.PED_PADRE_DEVOLUCION From PEDIDOS PCT INNER JOIN PEDIDOS_LINEAS PCL on (PCT.PED_CODIGO=PCL.PEDLI_SUPEDIDO) Where PCT.PED_TRATADO=1 And (PCT.PED_ANULADO=0 Or PCT.PED_ANULADO is null ) And PCT.PED_PADRE_DEVOLUCION is not null And PCL.PEDLI_FECHACAMBIO is null And PCL.PEDLI_SUPRODUCTO_IDERP<>10 And PCL.PEDLI_SUPRODUCTO_IDERP<>20 ) And PC.PEDLI_SUPEDIDO=PED_CODIGO ) > 0) "
		sqlEnCambio=sqlEnCambio & " ) "		
		
		'Pedidos nulos. 'Falta quitar los que tengan lineas buenas
		sqlNulosSelect=" Select count(P.PED_CODIGO) as numPedidosNulos "
		sqlNulosSelect2=" Select P.PED_CODIGO "		
		sqlNulos=" From Pedidos P "
		sqlNulos=sqlNulos & " Where " & filtroPedidos & filtroPedidosCambios 
		sqlNulos=sqlNulos & " And ((Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHASOLICITUD is not null And PD.PEDD_REALIZADO=1  ) > 0) "
		'sqlNulos=sqlNulos & " And ((Select count(PD.PEDD_CODIGO) From PEDIDOS_DEVOLUCIONES PD Where PD.PEDD_SUPEDIDO=P.PED_CODIGO And PD.PEDD_FECHASOLICITUD is not null And PD.PEDD_REALIZADO=1) = (Select COUNT(PL.PEDLI_CODIGO) * PL.PEDLI_UNIDADES From PEDIDOS_LINEAS PL Where PL.PEDLI_SUPEDIDO=P.PED_CODIGO And PL.PEDLI_SUPRODUCTO_IDERP<>10 And PL.PEDLI_SUPRODUCTO_IDERP<>20 Group by PL.PEDLI_UNIDADES ) )  "			


		''Obtener n� de pedidos en el periodo filtrado.
		sqlNumPedidos=" Select count(PED_CODIGO) as contPedidos From PEDIDOS P "
		sqlNumPedidos=sqlNumPedidos & " Where " & filtroPedidos & filtroPedidosCambios & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "  
		Select case modoVista
            case "1ANOS"
                'No ponemos nuevas condiciones
            case "2ANO"
                sqlNumPedidos=sqlNumPedidos & " and YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
                sqlNumPedidos=sqlNumPedidos & " and YEAR(P.PED_FECHA)=" & Anno & " And month(P.PED_FECHA)=" & Mes 
        end select
		Set rsNumPedidos = connCRM.AbrirRecordSet(sqlNumPedidos,3,1) 	
		if not rsNumPedidos.eof then
			if not isnull(rsNumPedidos("contPedidos")) then		
				if rsNumPedidos("contPedidos")>0 then
					numPedidos=rsNumPedidos("contPedidos")
				end if
			end if
		end if
		rsNumPedidos.cerrar()
		set rsNumPedidos=nothing

		''Obtener n� de pedidos en total.
		sqlNumPedidos=" Select count(PED_CODIGO) as contPedidos From PEDIDOS P "
		sqlNumPedidos=sqlNumPedidos & " Where " & filtroPedidos & filtroPedidosCambios & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") " 
		Set rsNumPedidos = connCRM.AbrirRecordSet(sqlNumPedidos,3,1) 	
		if not rsNumPedidos.eof then
			if rsNumPedidos("contPedidos")>0 then
				numPedidosGeneral=rsNumPedidos("contPedidos")
			end if
		end if
		rsNumPedidos.cerrar()
		set rsNumPedidos=nothing		
	
		''Obtener ingresos acumulados en el periodo filtrado
		sqlCantPedidos=" Select sum(L.PEDLI_PRECIOPAGADO*L.PEDLI_UNIDADES) as cantPedidos From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantPedidos=sqlCantPedidos & " Where " & filtroPedidos & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") And L.PEDLI_SUPRODUCTO_IDERP<>10 " 
		Select case modoVista
            case "1ANOS"
                'No ponemos nuevas condiciones
            case "2ANO"
                sqlCantPedidos=sqlCantPedidos & " and YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
                sqlCantPedidos=sqlCantPedidos & " and YEAR(P.PED_FECHA)=" & Anno & " And month(P.PED_FECHA)=" & Mes 
        end select
		Set rsCantPedidos = connCRM.AbrirRecordSet(sqlCantPedidos,3,1) 	
		if not rsCantPedidos.eof then
			if not isnull(rsCantPedidos("cantPedidos")) then
				if cdbl(rsCantPedidos("cantPedidos"))>0 then		
					cantVentasProductos=cdbl(rsCantPedidos("cantPedidos"))
				end if
			end if	
		end if
		rsCantPedidos.cerrar()
		set rsCantPedidos=nothing
		
		''Obtener ingresos acumulados en general
		sqlCantPedidos=" Select sum(L.PEDLI_PRECIOPAGADO*L.PEDLI_UNIDADES) as cantPedidos From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantPedidos=sqlCantPedidos & " Where " & filtroPedidos & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") And L.PEDLI_SUPRODUCTO_IDERP<>10 " 
		Set rsCantPedidos = connCRM.AbrirRecordSet(sqlCantPedidos,3,1) 	
		if not rsCantPedidos.eof then
			if cdbl(rsCantPedidos("cantPedidos"))>0 then		
				cantVentasProductosGeneral=cdbl(rsCantPedidos("cantPedidos"))
			end if
		end if
		rsCantPedidos.cerrar()
		set rsCantPedidos=nothing		


		sqlCantGastosEnvioDomicilio=" Select sum(L.PEDLI_PRECIOPAGADO) as cantGastosEnvioDomicilio From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " And P.PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "
		sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " And P.PED_PADRE_DEVOLUCION is null "		
		Select case modoVista
            case "1ANOS"
                'No ponemos nuevas condiciones
            case "2ANO"
                sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " and YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
                sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " and YEAR(P.PED_FECHA)=" & Anno &  " And month(P.PED_FECHA)=" & Mes 
        end select		
		Set rsGastosEnvioDomicilio = connCRM.AbrirRecordSet(sqlCantGastosEnvioDomicilio,3,1) 	
		if not isnull(rsGastosEnvioDomicilio("cantGastosEnvioDomicilio")) then
			if cdbl(rsGastosEnvioDomicilio("cantGastosEnvioDomicilio"))>0 then		
				cantResumenGastosEnvioDomicilio=cdbl(rsGastosEnvioDomicilio("cantGastosEnvioDomicilio"))
			end if
		end if
		rsGastosEnvioDomicilio.cerrar()
		set rsGastosEnvioDomicilio=nothing

		sqlCantGastosEnvioDomicilio=" Select sum(L.PEDLI_PRECIOPAGADO) as cantGastosEnvioDomicilio From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " And P.PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "
		sqlCantGastosEnvioDomicilio=sqlCantGastosEnvioDomicilio & " And P.PED_PADRE_DEVOLUCION is null "		
		Set rsGastosEnvioDomicilio = connCRM.AbrirRecordSet(sqlCantGastosEnvioDomicilio,3,1) 	
		if not isnull(rsGastosEnvioDomicilio("cantGastosEnvioDomicilio")) then
			if cdbl(rsGastosEnvioDomicilio("cantGastosEnvioDomicilio"))>0 then		
				cantResumenGastosEnvioDomicilioGeneral=cdbl(rsGastosEnvioDomicilio("cantGastosEnvioDomicilio"))
			end if
		end if
		rsGastosEnvioDomicilio.cerrar()
		set rsGastosEnvioDomicilio=nothing
		
		sqlCantGastosEnvioCambio=" Select sum(L.PEDLI_PRECIOPAGADO) as cantGastosEnvioCambio From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " And P.PED_PADRE_DEVOLUCION not in (" & sqlNulosSelect2 & sqlNulos & ") "
		sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " And P.PED_PADRE_DEVOLUCION is not null "			
		Select case modoVista
            case "1ANOS"
                'No ponemos nuevas condiciones
            case "2ANO"
                sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " and YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
                sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " and YEAR(P.PED_FECHA)=" & Anno &  " And month(P.PED_FECHA)=" & Mes 
        end select				
		Set rsGastosEnvioCambio = connCRM.AbrirRecordSet(sqlCantGastosEnvioCambio,3,1) 	
		if not isnull(rsGastosEnvioCambio("cantGastosEnvioCambio")) then
			if cdbl(rsGastosEnvioCambio("cantGastosEnvioCambio"))>0 then		
				cantResumenGastosEnvioCambioTalla=cdbl(rsGastosEnvioCambio("cantGastosEnvioCambio"))
			end if
		end if
		rsGastosEnvioCambio.cerrar()
		set rsGastosEnvioCambio=nothing		

		
		sqlCantGastosEnvioCambio=" Select sum(L.PEDLI_PRECIOPAGADO) as cantGastosEnvioCambio From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
		sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
		sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " And P.PED_PADRE_DEVOLUCION not in (" & sqlNulosSelect2 & sqlNulos & ") "
		sqlCantGastosEnvioCambio=sqlCantGastosEnvioCambio & " And P.PED_PADRE_DEVOLUCION is not null "			
		Set rsGastosEnvioCambio = connCRM.AbrirRecordSet(sqlCantGastosEnvioCambio,3,1) 	
		if not isnull(rsGastosEnvioCambio("cantGastosEnvioCambio")) then
			if cdbl(rsGastosEnvioCambio("cantGastosEnvioCambio"))>0 then		
				cantResumenGastosEnvioCambioTallaGeneral=cdbl(rsGastosEnvioCambio("cantGastosEnvioCambio"))
			end if
		end if
		rsGastosEnvioCambio.cerrar()
		set rsGastosEnvioCambio=nothing						


		sqlCantGastosEnvioDevolucion=" Select sum(CASE WHEN PD.PEDD_COSTEDEVOLUCION is null THEN 0 ELSE PD.PEDD_COSTEDEVOLUCION END) as cantGastosEnvioDevolucion "
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " From PEDIDOS P Inner Join PEDIDOS_DEVOLUCIONES PD on (P.PED_CODIGO=PD.PEDD_SUPEDIDO) "
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " Where " & filtroPedidos & filtroPedidosCambios 
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " And PD.PEDD_FECHAACEPTACION is not null And PD.PEDD_REALIZADO=1 " 
		Select case modoVista
            case "1ANOS"
                'No ponemos nuevas condiciones
            case "2ANO"
                sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " And YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
                sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " And YEAR(P.PED_FECHA)=" & Anno & " And month(P.PED_FECHA)=" & Mes 
        end select			
		Set rsGastosEnvioDevolucion = connCRM.AbrirRecordSet(sqlCantGastosEnvioDevolucion,3,1) 	

		if not isnull(rsGastosEnvioDevolucion("cantGastosEnvioDevolucion")) then
			if cdbl(rsGastosEnvioDevolucion("cantGastosEnvioDevolucion"))>0 then		
				cantResumenGastosEnvioDevolucion=cdbl(rsGastosEnvioDevolucion("cantGastosEnvioDevolucion"))
			end if
		end if
		rsGastosEnvioDevolucion.cerrar()
		set rsGastosEnvioDevolucion=nothing		
		
		sqlCantGastosEnvioDevolucion=" Select sum(CASE WHEN PD.PEDD_COSTEDEVOLUCION is null THEN 0 ELSE PD.PEDD_COSTEDEVOLUCION END) as cantGastosEnvioDevolucion "
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " From PEDIDOS P Inner Join PEDIDOS_DEVOLUCIONES PD on (P.PED_CODIGO=PD.PEDD_SUPEDIDO) "
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " Where " & filtroPedidos & filtroPedidosCambios 
		sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " And PD.PEDD_FECHAACEPTACION is not null And PD.PEDD_REALIZADO=1 " 
		Set rsGastosEnvioDevolucion = connCRM.AbrirRecordSet(sqlCantGastosEnvioDevolucion,3,1) 	

		if not isnull(rsGastosEnvioDevolucion("cantGastosEnvioDevolucion")) then
			if cdbl(rsGastosEnvioDevolucion("cantGastosEnvioDevolucion"))>0 then		
				cantResumenGastosEnvioDevolucionGeneral=cdbl(rsGastosEnvioDevolucion("cantGastosEnvioDevolucion"))
			end if
		end if
		rsGastosEnvioDevolucion.cerrar()
		set rsGastosEnvioDevolucion=nothing	
		
		
		cantGastosEnvio=cantResumenGastosEnvioDomicilio + cantResumenGastosEnvioCambioTalla
		cantGastosEnvioGeneral=cantResumenGastosEnvioDomicilioGeneral + cantResumenGastosEnvioCambioTallaGeneral
		
		cantIngresosVentas=cantVentasProductos + cantGastosEnvio
		cantIngresosVentasGeneral=cantVentasProductosGeneral + cantGastosEnvioGeneral
		
		cantIngresos=cantIngresosVentas + cantResumenGastosEnvioDevolucion
		cantIngresosGeneral=cantIngresosVentasGeneral + cantResumenGastosEnvioDevolucionGeneral


		
'		numDiasConPedidos=0
'		numDiasConPedidosGeneral=0		
		
'		sqlNumDias=" Select DATEDIFF(""d"",min(P.PED_FECHA),max(GETDATE())) as numDias, DATEDIFF(""d"",min(C.CONF_FINICIO_ESTADISTICAS),max(GETDATE())) as numDiasConf "
'		sqlNumDias=sqlNumDias & " From Pedidos P Left Join CONFIGURACION C on (CONF_CODIGO=1) "		
'		sqlNumDias=sqlNumDias & " Where " & filtroPedidos & filtroPedidosCambios
'		Select case modoVista
'            case "1ANOS"
'                'No ponemos nuevas condiciones
'            case "2ANO"
'                sqlNumDias=sqlNumDias & " and YEAR(P.PED_FECHA)=" & Anno & " and YEAR(C.CONF_FINICIO_ESTADISTICAS)=" & Anno
'            case "3MES"
'                sqlNumDias=sqlNumDias & " and YEAR(P.PED_FECHA)=" & Anno & " and YEAR(C.CONF_FINICIO_ESTADISTICAS)=" & Anno & " And month(P.PED_FECHA)=" & Mes & "  And month(C.CONF_FINICIO_ESTADISTICAS)=" & Mes 
'        end select				
'		Set rsNumDias = connCRM.AbrirRecordSet(sqlNumDias,3,1) 	
'		'if not isnull(rsNumDias("numDias")) then
'			if rsNumDias("numDias")<rsNumDias("numDiasConf") then
'				numDiasConPedidos=rsNumDias("numDiasConf") +1
'			else
'				numDiasConPedidos=rsNumDias("numDias") +1			
'			end if
'		'end if
'		rsNumDias.cerrar()
'		set rsNumDias=nothing
		
'		sqlNumDias="Select DATEDIFF(""d"",min(P.PED_FECHA),max(GETDATE())) as numDias, DATEDIFF(""d"",min(C.CONF_FINICIO_ESTADISTICAS),max(GETDATE())) as numDiasConf "
'		sqlNumDias=sqlNumDias & " From Pedidos P Left Join CONFIGURACION C on (CONF_CODIGO=1) "	
'		sqlNumDias=sqlNumDias & " Where " & filtroPedidos & filtroPedidosCambios
'		Set rsNumDias = connCRM.AbrirRecordSet(sqlNumDias,3,1) 	
''		if not isnull(rsNumDias("numDias")) then
'			if rsNumDias("numDias")<rsNumDias("numDiasConf") then
'				numDiasConPedidosGeneral=rsNumDias("numDiasConf") +1
'			else
'				numDiasConPedidosGeneral=rsNumDias("numDias") +1			
'			end if			
''		end if
'		rsNumDias.cerrar()
'		set rsNumDias=nothing					
		
'		mediaNumPedidosDias=0
'		mediaNumPedidosDiasGeneral=0				

'		mediaCantPedidosDias=0		
'		mediaCantPedidosDiasGeneral=0				
		
'		if numDiasConPedidos>0 then
'			mediaNumPedidosDias=numPedidos / numDiasConPedidos
'			mediaCantPedidosDias=cantIngresosVentas / numDiasConPedidos			
'		end if
'		if numDiasConPedidosGeneral>0 then				
'			mediaNumPedidosDiasGeneral=numPedidosGeneral / numDiasConPedidosGeneral	
'			mediaCantPedidosDiasGeneral=cantIngresosVentasGeneral / numDiasConPedidosGeneral
'		end if	

'		sacarDiasMedia=false
		calculadorMediasDias=0
		
		calculadorMedias=0
		sqlCalculadorMedia=""
		sqlWhereCalculadorMedia=""
		tipoCalculador=""
		Select case modoVista
            case "1ANOS"
                tipoCalculador="y"
'				sacarDiasMedia=true
            case "2ANO"
				tipoCalculador=""
				
				fechaInicioMes="01/01/" & Anno
				if trim(fechaInicioConf)<>"" then
					if cint(year(cdate(fechaInicioConf)))=cint(Anno) then
						fechaInicioMes=fechaInicioConf
					end if
				end if	
				
				if cint(year(date()))=cint(Anno) then
					fechaFinMes=date()
				else
					fechaFinMes="31/12/" & Anno
				end if

				calculadorMedias=DateDiff("m", cdate(fechaInicioMes) ,cdate(fechaFinMes)) + 1				
								
				calculadorMediasDias=DateDiff("d", cdate(fechaInicioMes) ,cdate(date)) + 1						
				
'				sacarDiasMedia=true				
'                sqlWhereCalculadorMedia=sqlCalculadorMedia & " and YEAR(P.PED_FECHA)=" & Anno
            case "3MES"
				tipoCalculador=""
				
fechaInicioMes="01/" & Mes & "/" & Anno
				if trim(fechaInicioConf)<>"" then
					if cint(month(cdate(fechaInicioConf)))=cint(Mes) And cint(year(cdate(fechaInicioConf)))=cint(Anno) then
						fechaInicioMes=fechaInicioConf
					end if
				end if
				
				if cint(month(date()))=cint(Mes) And cint(year(date()))=cint(Anno) then
					fechaFinMes=date()
				else
					fechaFinMes=ultimoDiaMes(Mes,Anno) & "/" & Mes & "/" & Anno
				end if
				
				calculadorMedias=DateDiff("d", cdate(fechaInicioMes) ,cdate(fechaFinMes)) + 1				
				
'                sqlWhereCalculadorMedia=sqlCalculadorMedia & " and YEAR(P.PED_FECHA)=" & Anno & " And month(P.PED_FECHA)=" & Mes 
        end select	
'		if not trim(tipoCalculador)="" then
'			sqlCalculadorMedia=" Select DATEDIFF(""" & tipoCalculador & """,min(P.PED_FECHA),max(P.PED_FECHA)) as numCalculador "
'			sqlCalculadorMedia=sqlCalculadorMedia & " From Pedidos P Where " & filtroPedidos & filtroPedidosCambios		
'			sqlCalculadorMedia=sqlCalculadorMedia & sqlWhereCalculadorMedia	
'		end if
'		Set rsCalculadorMedia = connCRM.AbrirRecordSet(sqlCalculadorMedia,3,1) 	
'		if not rsCalculadorMedia.eof then
'			if not isnull(rsCalculadorMedia("numCalculador")) then
'				calculadorMedias=rsCalculadorMedia("numCalculador") + 1
'			end if
'		end if
'		rsCalculadorMedia.cerrar()
'		set rsCalculadorMedia=nothing
		
'		if sacarDiasMedia then
'			sqlCalculadorMedia=" Select DATEDIFF(""d"",min(P.PED_FECHA),max(P.PED_FECHA)) as numCalculador "
'			sqlCalculadorMedia=sqlCalculadorMedia & " From Pedidos P Where " & filtroPedidos & filtroPedidosCambios		
'			sqlCalculadorMedia=sqlCalculadorMedia & sqlWhereCalculadorMedia				
'			Set rsCalculadorMedia = connCRM.AbrirRecordSet(sqlCalculadorMedia,3,1) 	
'			if not rsCalculadorMedia.eof then
'				if not isnull(rsCalculadorMedia("numCalculador")) then
'					calculadorMediasDias=rsCalculadorMedia("numCalculador") + 1
'				end if
'			end if
'			rsCalculadorMedia.cerrar()
'			set rsCalculadorMedia=nothing			
'		end if

		if calculadorMediasDias=0 then
			calculadorMediasDias=calculadorMedias
		end if
		
		calculadorMediasGeneral=0		
		calculadorMediasGeneral=DateDiff("d", cdate(fechaInicioConf) ,cdate(date)) + 1			
'		calculadorMediasGeneral=0		
'		sqlCalculadorMedia=" Select DATEDIFF(""d"",min(P.PED_FECHA),max(P.PED_FECHA)) as numCalculador "
'		sqlCalculadorMedia=sqlCalculadorMedia & " From Pedidos P Where " & filtroPedidos & filtroPedidosCambios				
'		sqlCalculadorMedia=sqlCalculadorMedia & " "			
'		Set rsCalculadorMedia = connCRM.AbrirRecordSet(sqlCalculadorMedia,3,1) 					
'		if not rsCalculadorMedia.eof then
'			if not isnull(rsCalculadorMedia("numCalculador")) then
'				calculadorMediasGeneral=rsCalculadorMedia("numCalculador") + 1
'			end if		
'		end if
'		rsCalculadorMedia.cerrar()
'		set rsCalculadorMedia=nothing
				
		
		mediaNumPedidosDias=0
		mediaCantPedidosDias=0
		
		mediaNuevosClientes=0
		mediaPedidos=0
		mediaPedidosExp=0
		mediaPedidosGeneral=0		
		mediaIngresos=0
		mediaIngresosExp=0
		mediaIngresosGeneral=0		
		mediaGastosEnvioEntrega=0
		mediaGastosEnvioDevolucion=0
		mediaGastosEnvioCambioTalla=0
		'response.Write(cantIngresosVentas & "@" & calculadorMedias & "@" & numDiasConPedidos)
		if not calculadorMedias=0 then
		
			mediaNumPedidosDias=round(numPedidos / calculadorMedias,2)
		
			mediaNuevosClientes=round(numNuevosClientes / calculadorMedias,2)
			mediaPedidos=round(numPedidos / calculadorMedias,2)
			mediaIngresos=round(cantIngresosVentas / calculadorMedias,2)
			mediaGastosEnvioEntrega=round(cantResumenGastosEnvioDomicilio / calculadorMedias,2)
			mediaGastosEnvioDevolucion=round(cantResumenGastosEnvioDevolucion / calculadorMedias,2)
			mediaGastosEnvioCambioTalla=round(cantResumenGastosEnvioCambioTalla / calculadorMedias,2)	
		end if
		
		if not calculadorMediasDias=0 then
			mediaPedidosExp=round(numPedidos / calculadorMediasDias,2)		
			mediaIngresosExp=round(cantIngresosVentas / calculadorMediasDias,2)		
		end if
		
		if not calculadorMediasGeneral=0 then
			mediaPedidosGeneral=round(numPedidosGeneral / calculadorMediasGeneral,2)
			mediaIngresosGeneral=round(cantIngresosVentasGeneral / calculadorMediasGeneral,2)				
		end if

		
		tieneNuevosClientes=false
		tienePedidos=false
		tieneIngresos=false
		tieneGastosEnvioEntrega=false
		tieneGastosEnvioDevolucion=false
		tieneGastosEnvioCambioTalla=false
		
		dim arrNuevosClientes()		
		dim arrPedidos()		
		dim arrIngresos()	
		dim arrGastosEnvioEntrega()	
		dim arrGastosEnvioDevolucion()					
		dim arrGastosEnvioCambioTalla()		
		
		''Inicializamos los arrays
		for contMes=1 to maximoFilter		
			
			redim preserve arrNuevosClientes(contMes)
			arrNuevosClientes(contMes)=0
			redim preserve arrPedidos(contMes)
			arrPedidos(contMes)=0
			redim preserve arrIngresos(contMes)
			arrIngresos(contMes)=0
			redim preserve arrGastosEnvioEntrega(contMes)
			arrGastosEnvioEntrega(contMes)=0
			redim preserve arrGastosEnvioDevolucion(contMes)
			arrGastosEnvioDevolucion(contMes)=0
			redim preserve arrGastosEnvioCambioTalla(contMes)
			arrGastosEnvioCambioTalla(contMes)=0															
		
		next									


			cadaNuevoCliente=0
			sqlFiltroSelect=""			
			sqlFiltroWhere=""
			sqlFiltroGroup=""			
			Select case modoVista
                case "1ANOS"
					sqlFiltroSelect=",year(C.CON_FECHAALTA) as colFiltro "
'                    sqlFiltroWhere=" YEAR(C.CON_FECHAALTA)=" & AnoInicial + contMes -1
					sqlFiltroGroup=" Group by year(C.CON_FECHAALTA) "			
                case "2ANO"
					sqlFiltroSelect=",month(C.CON_FECHAALTA) as colFiltro "				
                    sqlFiltroWhere=" And YEAR(C.CON_FECHAALTA)=" & anno '& " And month(C.CON_FECHAALTA)=" & contMes
					sqlFiltroGroup=" Group by month(C.CON_FECHAALTA) "			
                case "3MES"
					sqlFiltroSelect=",day(C.CON_FECHAALTA) as colFiltro "
                    sqlFiltroWhere=" And YEAR(C.CON_FECHAALTA)=" & anno & " And month(C.CON_FECHAALTA)=" & Mes '& " And day(C.CON_FECHAALTA)=" & contMes
					sqlFiltroGroup=" Group by day(C.CON_FECHAALTA) "			
            end select	                   		
			sqlNuevosClientes=" Select count(CON_CODIGO) as contClientes " & sqlFiltroSelect & " From CONTACTOS C "
			sqlNuevosClientes=sqlNuevosClientes & " Where 1=1 " & sqlFiltroWhere & sqlFiltroGroup
			Set rsNuevosClientes = connCRM.AbrirRecordSet(sqlNuevosClientes,3,1) 	
			while not rsNuevosClientes.eof 
				tieneNuevosClientes=true
				
				if not isnull(rsNuevosClientes("contClientes")) And trim(rsNuevosClientes("contClientes"))<>"0" then
					arrNuevosClientes(rsNuevosClientes("colFiltro"))=rsNuevosClientes("contClientes")
				end if
				
				rsNuevosClientes.movenext
			wend
			rsNuevosClientes.cerrar()
			set rsNuevosClientes=nothing	
			
			
			''Contador pedidos
			cadaNumPedidos=0	
			sqlFiltroSelect=""			
			sqlFiltroWhere=""
			sqlFiltroGroup=""
			Select case modoVista
                case "1ANOS"
					sqlFiltroSelect=",year(P.PED_FECHA) as colFiltro "
					sqlFiltroGroup=" Group by year(P.PED_FECHA) "			
                case "2ANO"
					sqlFiltroSelect=",month(P.PED_FECHA) as colFiltro "				
                    sqlFiltroWhere=" And YEAR(P.PED_FECHA)=" & anno 
					sqlFiltroGroup=" Group by month(P.PED_FECHA) "			
                case "3MES"
					sqlFiltroSelect=",day(P.PED_FECHA) as colFiltro "
                    sqlFiltroWhere=" And YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes 
					sqlFiltroGroup=" Group by day(P.PED_FECHA) "			
            end select	 			
			sqlNumPedidos=" Select count(PED_CODIGO) as contPedidos " & sqlFiltroSelect & " From PEDIDOS P "
			sqlNumPedidos=sqlNumPedidos & " Where " & filtroPedidos & filtroPedidosCambios & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "  & sqlFiltroWhere & sqlFiltroGroup			
			Set rsNumPedidos = connCRM.AbrirRecordSet(sqlNumPedidos,3,1) 	
			while not rsNumPedidos.eof 
				tienePedidos=true
				
				if not isnull(rsNumPedidos("contPedidos")) And trim(rsNumPedidos("contPedidos"))<>"0" then
					arrPedidos(rsNumPedidos("colFiltro"))=rsNumPedidos("contPedidos")
				end if
					
				rsNumPedidos.movenext		
			wend
			rsNumPedidos.cerrar()
			set rsNumPedidos=nothing
			
			

			''Suma ingresos
			cadaCantPedido=0
			sqlCantPedidos=" Select sum(L.PEDLI_PRECIOPAGADO*L.PEDLI_UNIDADES) as cantPedidos " & sqlFiltroSelect & " From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
			sqlCantPedidos=sqlCantPedidos & " Where " & filtroPedidos & " And PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") And L.PEDLI_SUPRODUCTO_IDERP<>10 " & sqlFiltroWhere & sqlFiltroGroup						
			Set rsCantPedidos = connCRM.AbrirRecordSet(sqlCantPedidos,3,1) 	
			while not rsCantPedidos.eof 
				tieneIngresos=true
				
				if not isnull(rsCantPedidos("cantPedidos")) And trim(rsCantPedidos("cantPedidos"))<>"0"  then
					arrIngresos(rsCantPedidos("colFiltro"))=rsCantPedidos("cantPedidos")
				end if
					
				rsCantPedidos.movenext	
			wend
			rsCantPedidos.cerrar()
			set rsCantPedidos=nothing

			redim preserve arrIngresos(contMes)
			arrIngresos(contMes)=cadaCantPedido
			
			


			''Suma gastos envio
			cadaCantGastosEnvioEntrega=0	
			sqlCantGastosEnvioEntrega=" Select sum(L.PEDLI_PRECIOPAGADO) as cantGastosEnvioEntrega " & sqlFiltroSelect & "  From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
			sqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) " 
			sqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega & " And P.PED_CODIGO not in (" & sqlNulosSelect2 & sqlNulos & ") "
			sqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega & " And P.PED_PADRE_DEVOLUCION is null " & sqlFiltroWhere & sqlFiltroGroup							
			Set rsCantGastosEnvioEntrega = connCRM.AbrirRecordSet(sqlCantGastosEnvioEntrega,3,1) 	
			while not rsCantGastosEnvioEntrega.eof 
				tieneGastosEnvioEntrega=true
				
				if not isnull(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega")) And trim(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega"))<>"0"  then
					arrGastosEnvioEntrega(rsCantGastosEnvioEntrega("colFiltro"))=rsCantGastosEnvioEntrega("cantGastosEnvioEntrega")
				end if
					
				rsCantGastosEnvioEntrega.movenext			
			wend
			rsCantGastosEnvioEntrega.cerrar()
			set rsCantGastosEnvioEntrega=nothing



			cadaCantGastosEnvioDevolucion=0		
			sqlCantGastosEnvioDevolucion=" Select sum(CASE WHEN PD.PEDD_COSTEDEVOLUCION is null THEN 0 ELSE PD.PEDD_COSTEDEVOLUCION END) as cantGastosEnvioDevolucion " & sqlFiltroSelect 
			sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " From PEDIDOS P Inner Join PEDIDOS_DEVOLUCIONES PD on (P.PED_CODIGO=PD.PEDD_SUPEDIDO) "			
			sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " Where " & filtroPedidos & filtroPedidosCambios 
			sqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion & " And PD.PEDD_FECHAACEPTACION is not null And PD.PEDD_REALIZADO=1 " & sqlFiltroWhere & sqlFiltroGroup					

			Set rsCantGastosEnvioDevolucion = connCRM.AbrirRecordSet(sqlCantGastosEnvioDevolucion,3,1) 	
			if not rsCantGastosEnvioDevolucion.eof then
				while rsCantGastosEnvioDevolucion.eof 
					tieneGastosEnvioEntrega=true
					
					if not isnull(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion")) And trim(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion"))<>"0"  then
						arrGastosEnvioDevolucion(rsCantGastosEnvioDevolucion("colFiltro"))=rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion")
					end if
						
					rsCantGastosEnvioDevolucion.movenext	
				wend
			end if
			rsCantGastosEnvioDevolucion.cerrar()
			set rsCantGastosEnvioDevolucion=nothing

			

			cadaCantGastosEnvioCambioTalla=0									
			sqlCantGastosEnvioCambioTalla=" Select sum(L.PEDLI_PRECIOPAGADO) as cantGastosEnvioCambioTalla " & sqlFiltroSelect 
			sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " From PEDIDOS P Inner Join PEDIDOS_LINEAS L ON (P.PED_CODIGO=L.PEDLI_SUPEDIDO) "
			sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " Where " & filtroPedidos &  " And L.PEDLI_SUPRODUCTO_IDERP=10 And (PED_ANULADO=0 Or PED_ANULADO is null ) "
			sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " And P.PED_PADRE_DEVOLUCION not in (" & sqlNulosSelect2 & sqlNulos & ") "
			sqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla & " And P.PED_PADRE_DEVOLUCION is not null " &  sqlFiltroWhere & sqlFiltroGroup				
			Set rsCantGastosEnvioCambioTalla = connCRM.AbrirRecordSet(sqlCantGastosEnvioCambioTalla,3,1) 	
			if not rsCantGastosEnvioCambioTalla.eof then
				while not rsCantGastosEnvioCambioTalla.eof 
					tieneGastosEnvioCambioTalla=true
					
					if not isnull(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla")) And trim(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla"))<>"0" then
						arrGastosEnvioCambioTalla(rsCantGastosEnvioCambioTalla("colFiltro"))=rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla")
					end if
						
					rsCantGastosEnvioCambioTalla.movenext	
				wend
			end if
			rsCantGastosEnvioCambioTalla.cerrar()
			set rsCantGastosEnvioCambioTalla=nothing




	colorLineaGrafico="#5893D5"
	textoLineaGrafico=""
	Select case modoVista
		case "1ANOS"
			colorLineaGrafico="#800080"
			textoLineaGrafico="Media anual"
		case "2ANO"
			colorLineaGrafico="#5893D5"
			textoLineaGrafico="Media mensual"			
		case "3MES"
			colorLineaGrafico="#008040"
			textoLineaGrafico="Media diaria"			
	end select   


	%>
    <script type="text/javascript" src="/dmcrm/js/gvChart/jsapi.js"></script>
	<script type="text/javascript" src="/dmcrm/js/gvChart/jquery.gvChart-1.0.1.min.js"></script>	
   	<script type="text/javascript">
		gvChartInit();
		jQuery(document).ready(function(){
		
				oTable = $('#tabProductosVendidos').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 25,
					"aaSorting": [[ 1, "desc" ],[ 2, "desc" ]],	
					"aoColumns": [ {"bSortable": false},{ "sType": "formatted-num" },{ "sType": "formatted-num" }]
				});
				
				oTable = $('#tabClientesActivos').dataTable({
					"oLanguage": { "sUrl": "/dmcrm/js/jtables/lang/<%=Idioma%>_ES.txt" },
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"bProcessing": true,
					"bSort": true,
					"iDisplayLength": 25,
					"aaSorting": [[ 1, "desc" ],[ 2, "desc" ]],	
					"aoColumns": [ {"bSortable": false},{ "sType": "formatted-num" },{ "sType": "formatted-num" }]
				});
		
			<%if tieneNuevosClientes then%>
			jQuery('#tabNuevosClientes').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: '', minValue: 0},
					legend: {position: 'none'},
					chartArea:{left:60,top:20,width:'82%',height:100},					
					colors:['#FF6600','<%=colorLineaGrafico%>'],
					series: {1: {type: "line"}}
					}
			});	
			<%end if%>
			
			<%if tienePedidos then%>			
			jQuery('#tabNumPedidos').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend: {position: 'none'},
					chartArea:{left:60,top:20,width:'82%',height:100},					
					colors:['#FF6600','<%=colorLineaGrafico%>'],
					series: {1: {type: "line"}}
					}
			});
			<%end if%>
			
			<%if tieneIngresos then%>			
			jQuery('#tabCantIngresos').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend: {position: 'none'},
					chartArea:{left:60,top:20,width:'82%',height:100},					
					colors:['#FF6600','<%=colorLineaGrafico%>'],
					series: {1: {type: "line"}}
					}
			});	
			<%end if%>	
			
			<%if tieneGastosEnvioEntrega then%>			
			jQuery('#tabCantGastosEnvioEntrega').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend: {position: 'none'},
					chartArea:{left:60,top:20,width:'82%',height:100},					
					colors:['#FF6600','<%=colorLineaGrafico%>'],
					series: {1: {type: "line"}}
					}
			});		
			<%end if%>
			
			<%if tieneGastosEnvioDevolucion then%>			
			jQuery('#tabCantGastosEnvioDevolucion').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend: {position: 'none'},
					chartArea:{left:60,top:20,width:'82%',height:100},					
					colors:['#FF6600','<%=colorLineaGrafico%>'],
					series: {1: {type: "line"}}
					}
			});		
			<%end if%>			
			
			<%if tieneGastosEnvioCambioTalla then%>			
			jQuery('#tabCantGastosEnvioCambioTalla').gvChart({
				chartType: 'ColumnChart',
				gvSettings: {
					vAxis: {title: '', minValue: 0},
					hAxis: {title: ''},
					legend: {position: 'none'},
					chartArea:{left:60,top:20,width:'82%',height:100},					
					colors:['#FF6600','<%=colorLineaGrafico%>'],
					series: {1: {type: "line"}}
					}
			});	
			<%end if%>							

		});    
		
		</script>
        
		<script type="text/javascript">
	
		
		function recargarFiltrosTiempo(ModoVista,valor,inicializar) {
			var pantalla="";
			if (inicializar) {
				pantalla="<%=nomPaginaestadisticas%>.asp?modo=<%=request("modo")%>";
			}
			else {
				pantalla="<%=nomPaginaestadisticas%>.asp?modo=<%=request("modo")%>";
				if (ModoVista=="1ANOS") {
					pantalla=pantalla + "&anno=0";
				}else if (ModoVista=="2ANO") {
					pantalla=pantalla + "&anno=" + valor;
				}else if (ModoVista=="3MES") {
					var annoMes=document.getElementById("cboAnno").value;					
					pantalla=pantalla + "&anno=" + annoMes + "&mes=" + valor;									
				}
			}

			document.estadisticasPedidos.action=pantalla;
			document.estadisticasPedidos.submit();
			
        }		
		</script>      
		

<style type="text/css">
.DatosResumen:after {display:block; clear:both; content:""; height:1px;}
.DatosResumen {background:#f7f7f7; padding:5px; margin:5px 0 0 0; color:#555; border-top: solid 5px #ff6600;}

.descripcionPantalla {float:left; width:75%; text-align:left; padding:15px 20px 0 20px; border-right:dashed 1px gray; }
.resumenNumerico {  color:#ff6600; font-size:16px; font-weight:bold; margin-top:6px;  }

/*filtros*/
#ulFiltros { float:right; width:15%; text-align:right;  padding-left:30px; padding-right:10px; border:none; /*border-left:dashed 1px gray;*/ }
#ulFiltros li {margin:0; padding:0; list-style:none; padding:3px 0;font-size:12px; }
</style>        


    <div class="DatosResumen">
	
		<div class="descripcionPantalla" >
		<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
		
		
		 Mediante esta pantalla podr� consultar las estadisticas de los pedidos que se han realizado en la tienda, en el a�o o mes que seleccione. 
		 <br/>

		<style>
			.tabResumenDatos { /*width:100%;*/ border:dashed 1px #555; }
			.tabResumenDatos td { /*width:20%;*/ }
			.datosResumenCabCol { text-transform:uppercase; font-weight:bold; text-align:right; width:200px; border-bottom:dashed 1px #555; font-size:14px;    }
			.datosResumenCabFila { text-align:left; font-weight:bold; width:300px; border-bottom:dashed 1px #555; font-size:14px; }
			.datosResumenContenido { color:#ff6600; font-weight:bold; text-align:right; width:180px; font-size:14px;  border-bottom:dashed 1px #555; }	
			
			.subTotalMediano { margin-left:20px; font-size:13px!important;}
			.subTotalPequeno { margin-left:50px; font-size:11px!important;}		
			.subContMediano { margin-left:20px; font-size:13px!important; }
			.subContPequeno { margin-left:50px; font-size:11px!important;}	
		</style>
		<br/>
		<table class="tabResumenDatos">
			<tr>
				<td class="datosResumenCabFila"></td>
				<td class="datosResumenCabCol">Periodo filtrado</td>
				<td class="datosResumenCabCol">General (atemporal)</td>				
			</tr>
			<tr>
				<td class="datosResumenCabFila">Nuevos clientes: </td>
				<td class="datosResumenContenido"><%=formatnumber(numNuevosClientes,0)%></td>
				<td class="datosResumenContenido"><%=formatnumber(numNuevosClientesGeneral,0)%></td>				
			</tr>
			<tr>
				<td class="datosResumenCabFila">N�mero de pedidos: </td>
				<td class="datosResumenContenido"><%=formatnumber(numPedidos,0)%></td>
				<td class="datosResumenContenido"><%=formatnumber(numPedidosGeneral,0)%></td>				
			</tr>
			<tr>
				<td class="datosResumenCabFila">Ingresos totales:
                	<br/><span class="subTotalMediano">- Ventas</span>
                	<br/><span class="subTotalPequeno">- Productos</span>
                	<br/><span class="subTotalPequeno">- Entrega en domicilio</span>                    
                	<br/><span class="subTotalPequeno">- Cambio de talla.</span>                                                            
                	<br/><span class="subTotalMediano">- Costes de devoluci�n.</span>                    
                </td>
				<td class="datosResumenContenido"><%=formatnumber(cantIngresos,2) & " �"%>
                	<br/><span class="subContMediano"><%=formatnumber(cantIngresosVentas,2) & " �"%></span>
                	<br/><span class="subContPequeno"><%=formatnumber(cantVentasProductos,2) & " �"%></span>       
                	<br/><span class="subContPequeno"><%=formatnumber(cantResumenGastosEnvioDomicilio,2) & " �"%></span>
                	<br/><span class="subContPequeno"><%=formatnumber(cantResumenGastosEnvioCambioTalla,2) & " �"%></span>
                	<br/><span class="subContMediano"><%=formatnumber(cantResumenGastosEnvioDevolucion,2) & " �"%></span>                                                            
                </td>
				<td class="datosResumenContenido"><%=formatnumber(cantIngresosGeneral,2) & " �"%></span>
                	<br/><span class="subContMediano"><%=formatnumber(cantIngresosVentasGeneral,2) & " �"%></span>
                	<br/><span class="subContPequeno"><%=formatnumber(cantVentasProductosGeneral,2) & " �"%></span>
                	<br/><span class="subContPequeno"><%=formatnumber(cantResumenGastosEnvioDomicilioGeneral,2) & " �"%></span>
                	<br/><span class="subContPequeno"><%=formatnumber(cantResumenGastosEnvioCambioTallaGeneral,2) & " �"%></span>
                	<br/><span class="subContMediano"><%=formatnumber(cantResumenGastosEnvioDevolucionGeneral,2) & " �"%></span>
                </td>
			</tr>	
			<tr>
				<td class="datosResumenCabFila" >Media de n� de pedidos/d�a:</td>
				<td class="datosResumenContenido" ><%=formatnumber(mediaPedidosExp,2)%></td>
				<td class="datosResumenContenido" ><%=formatnumber(mediaPedidosGeneral,2)%></td>				
			</tr>		            
			<tr>
				<td class="datosResumenCabFila" style="border:none;">Media de importes de pedidos/d�a:</td>
				<td class="datosResumenContenido" style="border:none;"><%=formatnumber(mediaIngresosExp,2) & " �"%></td>
				<td class="datosResumenContenido" style="border:none;"><%=formatnumber(mediaIngresosGeneral,2) & " �"%></td>				
			</tr>		                        
		</table>
		<br/>
		
	 
		</div>	 
     <ul id="ulFiltros" style="">
			<li class="tituloResumen" style="float:left; clear:both; width:100%; text-align:left;">Filtros:</li>	 
	         <li><span>A�o:</span>
			        <select  id="cboAnno" name="cboAnno" class=combos style="width:100px;" onChange="recargarFiltrosTiempo('2ANO',this.value,false);"  >
                       <!-- <option value="0" <%if trim(cstr(Anno))="0" then response.write " selected" %>>Todos</option>						    -->
                    	<%
	                    	sql=""
							sql=" select distinct Year(PED_FECHA) as Anno "
							sql=sql & " from  PEDIDOS "
							sql=sql & " Where (PED_ANULADO=0 Or PED_ANULADO is null ) "																				
							sql=sql& " Order by Year(PED_FECHA) DESC "
							Set rsAnnos = connCRM.AbrirRecordSet(sql,0,1)
							while not rsAnnos.EOF%>	
								<option value="<%=rsAnnos("Anno")%>"  <%if  trim(cstr(rsAnnos("Anno")))=trim(cstr(Anno))  then %>  selected <% end if %> ><%=rsAnnos("Anno")%></option>						    
                         	    <%rsAnnos.MoveNext 
							wend
							rsAnnos.cerrar()
							set rsAnnos=nothing
						%>
                    </select>                
             </li>
             <%'No muestro los meses si estoy en la vista general de a�os
             if  modoEstadistica="m" then %>
			     <li><span>Mes:</span>
			            <select  id="cboMes" name="cboMes"   class=combos style="width:100px;"  onchange="recargarFiltrosTiempo('3MES',this.value,false);"  >
                                <%for contMeses=1 to 12
                                    if month(now())>=contMeses Or trim(Anno)<>trim(cstr(year(now()))) then%>
	                            	    <option value="<%=contMeses%>"  <%if trim(cstr(Mes))=trim(cstr(contMeses)) then %>  selected <% end if %> ><%=ucase(left(monthname(contMeses),1)) & right(monthname(contMeses),len(monthname(contMeses))-1)%></option>
                                    <%end if
							    next%>
                        </select>             
             
                 </li>
             <%end if%>   

              <li style="font-weight:bold;">
               <span style="font-weight:normal;">Filtrado por:</span><br/>
               
               	<%
			    Select case modoVista
                    case "1ANOS"
                        response.write ("Todos los a�os disponibles")
                    case "2ANO"
                        response.Write("A�o: " & Anno & "<br/>")
                    case "3MES"
                        response.Write("A�o: " & Anno & "<br/>")
                        response.Write("Mes: " & ucase(left(monthname(Mes),1)) & right(monthname(Mes),len(monthname(Mes))-1))
                end select
				if trim(textoFiltradoPorTitulo)<>"" And trim(textoFiltradoPorContenido)<>"" then
					if trim(Mes)<>"0" then 
						response.Write("<br/>")
					end if
					 response.Write(textoFiltradoPorTitulo & ": " & textoFiltradoPorContenido & "<br/>")				
				end if
								
				
				%>
                    <br/> 
                    <a href="#" style="font-weight:normal;" onClick="javascript:recargarFiltrosTiempo('1ANOS','<%=year(date())%>',true);" >[Limpiar filtros]</a>
               </li>            
               

         </ul>		 
	 
	 </div>
	 
<!--	<div style="height:20px; width:50%; border:solid 1px red;">bb</div>	 -->
	<br/>
    
    
<style type="text/css">
 .tablaResumen { z-index:1; float:left;position:relative; top:-40px; left:0px; margin:0 10px 0 10px;}
 .tablaResumen th { background-color:#5893D5; color:white;  vertical-align:middle; width:80px; text-align:center; padding:4px; } 
 .tablaResumen th:hover { background-color:#ff6600; cursor:pointer; }  
 .tablaResumen th.tablaResumenCabSel { background-color:#015B9F;  }  
 .tablaResumen td { background-color:#F1F1F1; color:#ff6600; text-align:center; width:80px; font-size:14px; font-weight:bold; }  
 
 .bloqueResumen {float:left; border:solid 1px #CDCDCD; /*min-width:595px;*/ height:230px; margin:0; padding:0; margin:10px 10px 10px 10px; }
 .bloqueResumen_Peq { /*width:595px;*/ width:47%;  }
 .bloqueResumen_Grande {/* width:120px;*/ width:96%; } 
 .bloqueResumenTitulo {margin:0; padding:5px 0 5px 0; width:100%; border-bottom:solid 3px #ff6600; font-size:14px; color:#555; font-weight:bold; background-color:#F7F7F7; text-transform:uppercase;}
 
 .bloqueResumenGraficos {width:100%; float:left; margin:0; padding:0;}

 .capaGraficoPorcentual {position:relative; top:-50px; left:10;}

.tablaGraficos { }
.tablaGraficos th { text-transform:capitalize;}
</style>            


<%
cabeceraTablaGrafco="<th>" & textoMeses & "</th>"
for contMes=1 to maximoFilter
	Select case modoVista
        case "1ANOS"
            nomMes=AnoInicial+contMes-1
        case "2ANO"
            nomMes=left(monthname(contMes),3)	
        case "3MES"
            nomMes=contMes
    end select
	cabeceraTablaGrafco=cabeceraTablaGrafco & "<th >" & nomMes & "</th>"
next%>


	
	<%if tieneNuevosClientes then%>    
    <div class="bloqueResumen bloqueResumen_Peq">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* N� de nuevos clientes:
        	<div style=" float:right; white-space:nowrap;  z-index:99; ">	
			     <div style=" margin-top:5px; margin-right:2px; float:left; width:15px; height:5px; background:<%=colorLineaGrafico%>; "></div>
    			 <span style="float:left; font-size:12px; font-weight:normal; text-transform:none; margin-right:50px; "><%=textoLineaGrafico%></span>
		    </div>         
        </div>
		
		<table id="tabNuevosClientes"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th>N� de clientes nuevos</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=arrNuevosClientes(contMes)%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th>Media de clientes nuevos</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaNuevosClientes,",",".")%></td><%
					next%>
	            </tr>
    	    </tbody>
	    </table>    	        

	 </div>
     
	 <%end if%>
	
	<%if tienePedidos then%>
    <div class="bloqueResumen bloqueResumen_Peq">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* N� de pedidos:
        	<div style=" float:right; white-space:nowrap;  z-index:99; ">	
			     <div style=" margin-top:5px; margin-right:2px; float:left; width:15px; height:5px; background:<%=colorLineaGrafico%>; "></div>
    			 <span style="float:left; font-size:12px; font-weight:normal; text-transform:none; margin-right:50px; "><%=textoLineaGrafico%></span>
		    </div>         
        </div>        

			 <table id="tabNumPedidos"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th>N� de pedidos</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=arrPedidos(contMes)%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th>Media de n� de pedidos</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaPedidos,",",".")%></td><%
					next%>
	            </tr>				
    	    </tbody>
	    </table>    	        
	 </div>  
	 <%end if%>   
	 
     
	<%if tieneIngresos then%> 
    <div class="bloqueResumen bloqueResumen_Peq">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* Ventas - Productos(�):
        	<div style=" float:right; white-space:nowrap;  z-index:99; ">	
			     <div style=" margin-top:5px; margin-right:2px; float:left; width:15px; height:5px; background:<%=colorLineaGrafico%>; "></div>
    			 <span style="float:left; font-size:12px; font-weight:normal; text-transform:none; margin-right:50px; "><%=textoLineaGrafico%></span>
		    </div>         
        </div>        

			 <table id="tabCantIngresos"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th>Ventas - Productos(�)</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrIngresos(contMes)),",",".")%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th>Media de ventas</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaIngresos,",",".")%></td><%
					next%>
	            </tr>				
    	    </tbody>
	    </table>    	        
	 </div>

	 <%end if%>     
	 
	<%if tieneGastosEnvioEntrega then%> 
    <div class="bloqueResumen bloqueResumen_Peq">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* Ventas - Entrega a domicilio(�):
        	<div style=" float:right; white-space:nowrap;  z-index:99; ">	
			     <div style=" margin-top:5px; margin-right:2px; float:left; width:15px; height:5px; background:<%=colorLineaGrafico%>; "></div>
    			 <span style="float:left; font-size:12px; font-weight:normal; text-transform:none; margin-right:50px; "><%=textoLineaGrafico%></span>
		    </div>         
        </div>        

			 <table id="tabCantGastosEnvioEntrega"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th>Ventas - Entrega a domicilio(�)</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrGastosEnvioEntrega(contMes)),",",".")%></td><%
					next%>
	            </tr>
				<tr>
    	        	<th>Media de Ventas - Entrega a domicilio</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaGastosEnvioEntrega,",",".")%></td><%
					next%>
	            </tr>				
    	    </tbody>
	    </table>    	        
	 </div>   
	 <%end if%> 
	 
	<%if tieneGastosEnvioCambioTalla then%> 
    <div class="bloqueResumen bloqueResumen_Peq">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* Ventas - Cambio de talla(�):
        	<div style=" float:right; white-space:nowrap;  z-index:99; ">	
			     <div style=" margin-top:5px; margin-right:2px; float:left; width:15px; height:5px; background:<%=colorLineaGrafico%>; "></div>
    			 <span style="float:left; font-size:12px; font-weight:normal; text-transform:none; margin-right:50px; "><%=textoLineaGrafico%></span>
		    </div>         
        </div>        

			 <table id="tabCantGastosEnvioCambioTalla"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th>Ventas - Cambio de talla(�)</th>
					<%					
					for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrGastosEnvioCambioTalla(contMes)),",",".")%></td><%
					next
					%>
	            </tr>
				<tr>
    	        	<th>Media de ventas - Cambio de talla</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaGastosEnvioCambioTalla,",",".")%></td><%
					next%>
	            </tr>				
    	    </tbody>
	    </table>    	        
	 </div>    
	 <%end if%>	 
     
	<%if tieneGastosEnvioDevolucion then%> 
    <div class="bloqueResumen bloqueResumen_Peq">
    	<div  class="bloqueResumenTitulo">&nbsp;&nbsp;* Costes de devoluci�n(�):
        	<div style=" float:right; white-space:nowrap;  z-index:99; ">	
			     <div style=" margin-top:5px; margin-right:2px; float:left; width:15px; height:5px; background:<%=colorLineaGrafico%>; "></div>
    			 <span style="float:left; font-size:12px; font-weight:normal; text-transform:none; margin-right:50px; "><%=textoLineaGrafico%></span>
		    </div>         
        </div>        

			 <table id="tabCantGastosEnvioDevolucion"  class="tablaGraficos"  >
    		<thead  >
        		<tr><%=cabeceraTablaGrafco%></tr>
    	    </thead>
        	<tbody>
        		<tr>
    	        	<th>Costes de devoluci�n(�)</th>
					<%					
					for contMes=1 to maximoFilter
						%><td ><%=replace(cstr(arrGastosEnvioDevolucion(contMes)),",",".")%></td><%
					next
					%>
	            </tr>
				<tr>
    	        	<th>Media de costes de devoluci�n</th>
					<%for contMes=1 to maximoFilter
						%><td ><%=replace(mediaGastosEnvioDevolucion,",",".")%></td><%
					next%>
	            </tr>				
    	    </tbody>
	    </table>    	        
	 </div>
	 <%end if%>              
      	
    
<style type="text/css">
 .tablaResumenGeneral { float:left; clear:both; width:100%; margin:30px 0 40px 0; padding:0;}

 .tablaResumenGeneral th { background-color:#5893D5; color:white;  vertical-align:middle; max-width:1px; text-align:center; padding:4px; text-transform:capitalize; } 
 .tablaResumenGeneral th:hover { background-color:#ff6600;  cursor:pointer; }  
 .tablaResumenGeneral th.tablaResumenCabSel { background-color:#015B9F; }  
 .tablaResumenGeneral th.tablaResumenLectura { background-color:#5893D5; cursor:default; }  

 .tablaResumenGeneral tr { background-color:#F1F1F1;}
 .tablaResumenGeneral tr:hover { background-color:#CDCDCD;}
 .tablaResumenGeneral td {  color:#555; text-align:center; font-size:11px;}  
 .tablaResumenGeneral td.tituloCeldaCab { color:#555; text-align:left; text-transform:none; width:100px; font-size:12px; font-weight:bold; }  
 .tablaResumenGeneral td.tituloCeldaCabAgrupacion { color:#555; text-align:left; width:100px; font-size:13px; font-weight:bold; text-transform:uppercase; background-color:#F1F1F1; }  
 
 
</style>     
    
    <table class="tablaResumenGeneral"  >
        	<tr>
            	<th style="min-width:150px;"  class="tablaResumenLectura"><%=textoMeses%></th>
				<%For contMes=1 to maximoFilter%>
	                <%Select case modoVista%>
                        <%case "1ANOS"%>
                            <th style="max-width:100px;" <%if Mes=trim(cstr(contMes)) then%> class="tablaResumenCabSel" <%else%> onclick="recargarFiltrosTiempo('2ANO','<%=AnoInicial+contMes-1%>',false);" <%end if%> ><%=AnoInicial+contMes-1%></th>   
                        <%case "2ANO"%>
							 <th style="max-width:100px;"  class="tablaResumenLectura"><%=monthName(contMes)%></th> 
                            <!--<th style="max-width:100px;" <%if Mes=trim(cstr(contMes)) then%> class="tablaResumenCabSel" <%else%> onclick="recargarFiltrosTiempo('1ANOS','<%=contMes%>',false);" <%end if%> ><%=monthName(contMes)%></th>   	-->
                        <%case "3MES"%>
                            <th style="max-width:100px;"  class="tablaResumenLectura"><%=contMes%></th> 
                    <%end select%>
                <%next%>
				<th style="min-width:50px;" class="tablaResumenLectura"  >Total</th>                
				<th style="min-width:50px;" class="tablaResumenLectura"  >Media</th>
		  </tr>
			<%if tieneNuevosClientes then%>	  
        	<tr class="filaTablaResumenGeneral">
            	<td  class="tituloCeldaCabAgrupacion" >N� de nuevos clientes</td>            
                <%numElementosMes=0
				 valorElementosMes=0
				 For contMes=1 to maximoFilter%>
	            	<td>
						<%if arrNuevosClientes(contMes)=0 then
							response.Write("-")
						else
							response.Write(formatnumber(arrNuevosClientes(contMes),0))
						end if%>
                    </td>
                <%numElementosMes=numElementosMes + 1
				valorElementosMes=valorElementosMes + arrNuevosClientes(contMes)
				next
				if numElementosMes=0 then
					mediaValoresMes=valorElementosMes
                 else
					mediaValoresMes=valorElementosMes/numElementosMes				 
                 end if   %>
				<td >
					<%=formatnumber(valorElementosMes,0)%>				
				</td>    				                  
				<td >
					<%=formatnumber(mediaNuevosClientes,2)%>				
				</td>    				 
			  </tr>      
			 <%end if%>     
            
			<%if tienePedidos then%>			
        	<tr>
            	<td  class="tituloCeldaCabAgrupacion">N� de pedidos</td>            
                <%numElementosMes=0
				 valorElementosMes=0
				 For contMes=1 to maximoFilter%>
	            	<td>
						<%if arrPedidos(contMes)=0 then
							response.Write("-")
						else
							response.Write(formatnumber(arrPedidos(contMes),0))
						end if%>
                    </td>                    
                 <%numElementosMes=numElementosMes + 1
				valorElementosMes=valorElementosMes + arrPedidos(contMes)
				next
				if numElementosMes=0 then
					mediaValoresMes=valorElementosMes
                 else
					mediaValoresMes=valorElementosMes/numElementosMes				 
                 end if   %>        
				<td >
					<%=formatnumber(valorElementosMes,0)%>				
				</td> 	                 
				<td >
					<%=formatnumber(mediaPedidos,2)%>				
				</td> 				         
	      </tr>   
		  <%end if%>       
			
			<%if tieneIngresos then%>			
        	<tr>
            	<td  class="tituloCeldaCabAgrupacion">Ventas Productos(�)</td>            
                <%numElementosMes=0
				 valorElementosMes=0
				 For contMes=1 to maximoFilter%>
	            	<td>
						<%if isnull(arrIngresos(contMes)) then
							response.Write("-")						
						elseif clng(arrIngresos(contMes))=0 then
							response.Write("-")
						else
							response.Write(formatnumber(arrIngresos(contMes),2))
						end if%>
                    </td>                                        
                <%numElementosMes=numElementosMes + 1
				  if not isnull(arrIngresos(contMes)) then
					valorElementosMes=valorElementosMes + cdbl(arrIngresos(contMes))
				  end if
				next
				if numElementosMes=0 then
					mediaValoresMes=valorElementosMes
                 else
					mediaValoresMes=valorElementosMes/numElementosMes				 
                 end if   %>     
				<td >
					<%=formatnumber(valorElementosMes,2)%>				
				</td> 	                 
				<td >
					<%=formatnumber(mediaIngresos,2)%>				
				</td> 					              
		     </tr>  
			 <%end if%>
			
			<%if tieneGastosEnvioEntrega then%>	
        	<tr>
            	<td  class="tituloCeldaCabAgrupacion">Ventas - Entrega a domicilio(�)</td>            
                <%numElementosMes=0
				 valorElementosMes=0
				 For contMes=1 to maximoFilter%>
	            	<td>
						<%if isnull(arrGastosEnvioEntrega(contMes)) then
							response.Write("-")						
						elseif clng(arrGastosEnvioEntrega(contMes))=0 then
							response.Write("-")
						else
							response.Write(formatnumber(arrGastosEnvioEntrega(contMes),2))
						end if%>
                    </td>                                        
                 <%numElementosMes=numElementosMes + 1
					if not isnull(arrGastosEnvioEntrega(contMes)) then
						valorElementosMes=valorElementosMes + cdbl(arrGastosEnvioEntrega(contMes))
				    end if				 
				 
				next%>   
				<td >
					<%=formatnumber(valorElementosMes,2)%>				
				</td> 		                
				<td >
					<%=formatnumber(mediaGastosEnvioEntrega,2)%>				
				</td> 					
		    </tr>  
			<%end if%>
			
			<%if tieneGastosEnvioCambioTalla then%>		   
        	<tr>
            	<td  class="tituloCeldaCabAgrupacion">Ventas - Cambio de talla(�)</td>            
                <%numElementosMes=0
				 valorElementosMes=0
				 For contMes=1 to maximoFilter%>
	            	<td>
						<%if isnull(arrGastosEnvioCambioTalla(contMes)) then
							response.Write("-")						
						elseif clng(arrGastosEnvioCambioTalla(contMes))=0 then
							response.Write("-")
						else
							response.Write(formatnumber(arrGastosEnvioCambioTalla(contMes),2))
						end if%>
                    </td>                                        
                 <%numElementosMes=numElementosMes + 1
					if not isnull(arrGastosEnvioCambioTalla(contMes)) then
						valorElementosMes=valorElementosMes + cdbl(arrGastosEnvioCambioTalla(contMes))
				    end if					 
				next%>   
				<td >
					<%=formatnumber(valorElementosMes,2)%>				
				</td>                 
				<td >
					<%=formatnumber(mediaGastosEnvioCambioTalla,2)%>				
				</td> 					
		   </tr>   
		   <%end if%> 
           
			<%if tieneGastosEnvioDevolucion then%>
        	<tr>
            	<td  class="tituloCeldaCabAgrupacion">Costes de devoluci�n(�)</td>            
                <%numElementosMes=0
				 valorElementosMes=0
				 For contMes=1 to maximoFilter%>
	            	<td>
						<%if isnull(arrGastosEnvioDevolucion(contMes)) then
							response.Write("-")						
						elseif clng(arrGastosEnvioDevolucion(contMes))=0 then
							response.Write("-")
						else
							response.Write(formatnumber(arrGastosEnvioDevolucion(contMes),2))
						end if%>
                    </td>                                        
                 <%numElementosMes=numElementosMes + 1
					if not isnull(arrGastosEnvioDevolucion(contMes)) then
						valorElementosMes=valorElementosMes + cdbl(arrGastosEnvioDevolucion(contMes))
				    end if					 
				next%>   
				<td >
					<%=formatnumber(valorElementosMes,2)%>				
				</td> 		                
				<td >
					<%=formatnumber(mediaGastosEnvioDevolucion,2)%>				
				</td> 					
		   </tr>  
		   <%end if%>           
		            
          </table>  

 
        <div style="height:20px; width:100%; clear:both;"></div>	 

</form>	        





