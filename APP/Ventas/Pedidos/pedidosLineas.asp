<%'El nombre del fichero de idiomas es, por ejemplo, Idiomas_Comercial.xml, en la variable ponemos s�lo el men� al que pertenece la p�gina
'S�lo usamos esta variable si la p�gina no est� enganchada en ning�n men� (app_menus)
nomFicheroIdiomas="Ventas"%>

<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!--#include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<!--#include virtual="/dmcrm/APP/comun2/Validaciones.asp"  -->
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="shortcut icon" href="/dmcrm/favicon.ico">    
    <title><% if trim(nomSubMenu)<>"" then %><%=nomSubMenu%><%else%>DM GESTION<%end if%></title>
    
		<link rel="stylesheet" href="/dmcrm/css/estilos.css">
		<link rel="stylesheet" href="/dmcrm/css/estilosPestanna.css">    
    <!-- #include virtual="/dmcrm/includes/jquery.asp"-->  
	
    <style type="text/css" >
			
			
			/**********************
			cesta de la compra ****/
			#tablaCesta {width:100%; font-size:11px!important; }
			#tablaCesta tr th {background:#999; color:#fff;}
			#tablaCesta tr td {border-bottom:solid 1px #cdcdcd; vertical-align:top;}
			#tablaCesta tr td.center {text-align:center;}
			#tablaCesta tr td.right {text-align:right; padding-right:10px!important;}
			#tablaCesta tr td.top {padding:22px 0 0 0;}
			#tablaCesta tr td .cmbTallas {width:50px;}
			#tablaCesta tr th, #tablaCesta tr td {padding:5px 10px;}
			#tablaCesta tr td.celdaOpciones {text-align:right;}
			#tablaCesta tr.totalCarrito {text-align:right; font-weight:bold; font-size:14px;}
			#tablaCesta tr.totalCarrito td {border:none; padding:0 10px 0 0;}
			#tablaCesta tr.totalCarrito td.totalTxt2, #tablaCesta tr.totalCarrito td span.ccGE {font-size:11px; font-weight:normal;}
			#tablaCesta .carritoImagen { width:100px; margin:0 10px 0 0; float:left}
			#tablaCesta span.carritoMarca {font-weight:bold;}
			.tablaMisPedidos tr td.totalTxt {padding-bottom:30px!important;}
			
			
			
			#tablaCestaDeLaCompra .comboUnidades{width: 40px; margin:0 0 0 5px; text-align:right}
			#tablaCestaDeLaCompra .center, #resumenPedido .center { text-align:center }
			#tablaCestaDeLaCompra .right, #resumenPedido .right { text-align: right }
			#tablaCestaDeLaCompra .top, #resumenPedido .top { vertical-align: top; padding-top:25px }
			
			
			/*Ventas de aviso*/
			.avisoEmer strong {color:#e95a33; border-bottom:solid 1px #e95a33; font-size:14px; margin:15px 0 0 0; padding:0; display:block;}
			.avisoEmer p {margin:0 0 10px 0;}
			
			
			#resumenPedidoDatos:after {display:block; clear:both; content:"";}
			#resumenPedidoDatos {margin:0 0 20px 0;}
			#resumenPedidoDatos p {border:solid 1px #4ace0f; background:#dbf5cf; text-align:center; padding:5px;}
			#resumenPedidoDatos #resumenPedidoIzquierda { width:45%; float:left; margin:0 0 0 10px;}
			#resumenPedidoDatos #resumenPedidoDerecha {width:45%; float:right; margin:0 10px 0 0; text-align:right;}
	
	</style>
    
	    <style type="text/css">
			.agrupacionEtiquetas{  display:inline; clear:both; float:left;  white-space:nowrap; }
			.cadaEtiqueta { margin-left:10px; font-weight:bold;}
			.cadaContenido { margin-right:10px;}
			.listadico { list-style:none; margin:0 0 0 0; padding:0;}
			.listadico li { display:inline-block;  background:#f1f1f1; margin:0 5px 5px 0 ; padding:5px; width:auto; float:left; }
			
			/*Parcehar la tabla para que se vea bien el fiscar y el mostrar*/
			.dataTables_filter { position:relative; top:-30px; }
			.dataTables_length { position:relative; top:-32px; }
			
			.capaImgBoton { position:relative;  width:70px; height:26px; overflow:hidden; display:inline-block; }
			
			.devolucionLinea { float:right; border:solid #555 1px; width:260px; height:40px; color:red; text-align:center; }

			.cambioTallaLinea { float:left; border:solid #555 1px; padding-left:5px; padding-right:5px;  height:30px; color:red; text-align:center; }			
			
			#btnFraudePoner {float:left; padding:5px 0 0 30px;  position:relative; top:0; left:0; line-height:9px;
			 font-weight:bold; color:#8C8C8D; width:118px; display:inline;   }
			 
			#btnFraudeQuitar {float:left; padding:5px 0 0 30px;  position:relative; top:0; left:0; line-height:9px;
			 font-weight:bold; color:#8C8C8D; width:118px; display:inline;   }			
			
		</style>      
    <script language="javascript" type="text/javascript" >
		$(document).ready(function () {	
		     $(".ku_fancybox").fancybox({ 'hideOnContentClick': false, 'zoomSpeedIn': 300, 'zoomSpeedOut': 300, 'overlayShow': true, 'titleShow': false });	
			 
			 $(".linkTratarDevolucion").fancybox({
					'imageScale'			: true,
					'zoomOpacity'			: true,
					'overlayShow'			: true,
					'overlayOpacity'		: 0.7,
					'overlayColor'			: '#333',
					'centerOnScroll'		: true,
					'zoomSpeedIn'			: 600,
					'zoomSpeedOut'			: 500,
					'transitionIn'			: 'elastic',
					'transitionOut'			: 'elastic',
					'type'					: 'iframe',
					'frameWidth'			: 500,
					'frameHeight'			: 320,
					'titleShow'				: false	,
					'onClosed'		        : function() {
							//recargarAlCerrar();
							window.location.reload();
					}						 
			 });	
			 
			 
			 $(".linkTratarCambioTalla").fancybox({
					'imageScale'			: true,
					'zoomOpacity'			: true,
					'overlayShow'			: true,
					'overlayOpacity'		: 0.7,
					'overlayColor'			: '#333',
					'centerOnScroll'		: true,
					'zoomSpeedIn'			: 600,
					'zoomSpeedOut'			: 500,
					'transitionIn'			: 'elastic',
					'transitionOut'			: 'elastic',
					'type'					: 'iframe',
					'frameWidth'			: 500,
					'frameHeight'			: 300,
					'titleShow'				: false	,
					'onClosed'		        : function() {
							//recargarAlCerrar();
							window.location.reload();
					}						 
			 });				 
			 
			 $("#bnEnviarPedido").click(function() {
				var permitirCambio=false;	
				var valorPedido="";											 
				if ($(this).attr('rel')=="1") {
					if ($('#bnRecibirPedido').attr('rel')=="1") {
						alert("<%=objIdioma.getIdioma("PedidosWeb_Texto27")%>");
					} else if ($('#bnRecibirPedido').attr('rel')=="0") {
						$(this).css("margin-left","-40px");									
						$(this).attr('rel','0');
						permitirCambio=true;	
						valorPedido="1";					
					}	
				}else if ($(this).attr('rel')=="0") {
					$(this).css("margin-left","0px");
					$(this).attr('rel','1');
					permitirCambio=true;
					valorPedido="2";						
				}								
				
				if (permitirCambio && valorPedido!="") {
					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/Ventas/pedidos/cambiarEstadoPedido.asp",
						data: "ids=<%=request("codigo")%>&valor=" + valorPedido,
						success: function(respuesta) {
						}
					});				
				}
				 
			 });
					
			 $("#bnRecibirPedido").click(function() {
			 	var permitirCambio=false;	
				var valorPedido="";
			 	if ($(this).attr('rel')=="1") {
					$(this).css("margin-left","-40px");									
					$(this).attr('rel','0');	
					valorPedido="2";
					permitirCambio=true;														
				}else if ($(this).attr('rel')=="0") {
					/*if ($('#bnEnviarPedido').attr('rel')=="0") {
						alert("<%=objIdioma.getIdioma("PedidosWeb_Texto28")%>");				
					} else { */
						
                    $(this).css("margin-left","0px");
					$(this).attr('rel','1');	
					permitirCambio=true;
					valorPedido="3";																	

					/*}*/
				}
  				
				if (permitirCambio && valorPedido!="") {
					$.ajax({
						type: "POST",
						url: "/dmcrm/APP/Ventas/pedidos/cambiarEstadoPedido.asp",
						data: "ids=<%=request("codigo")%>&valor=" + valorPedido,
						success: function(respuesta) {
						}
					});				
				}				
				
			 });	
			 
			 
			$('#btnFraudeQuitar').click( function() {	
					var idPed=$(this).attr('rel');
					if (idPed!="") {
						$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Ventas/Pedidos/cambioFraude.asp",
								data: "ids=" + idPed + "&fraude=0",
								success: function(datos) {
									if (datos=="1") {
										$('#btnFraudeQuitar').hide();
										$('#btnFraudePoner').show();
									}
								}
							});								
					}
			});
			
			$('#btnFraudePoner').click( function() {	
					var idPed=$(this).attr('rel');
					if (idPed!="") {
						$.ajax({
								type: "POST",
								url: "/dmcrm/APP/Ventas/Pedidos/cambioFraude.asp",
								data: "ids=" + idPed + "&fraude=1",
								success: function(datos) {
									if (datos=="1") {									
										$('#btnFraudePoner').hide();
										$('#btnFraudeQuitar').show();
									}
								}
							});								
					}												 
			});											   
						 
			 
			$('#btnNotificarPedidos').click( function() {
				if ( confirm("<%=objIdioma.getIdioma("PedidosWeb_Texto29")%>") ) {											  
					var valoresSel="<%=request("codigo")%>";						
					if (valoresSel!="") {
						$.ajax({
							type: "POST",
							url: "/dmcrm/APP/Ventas/Pedidos/notificarPedidos.asp",
							data: "ids=" + valoresSel,
							success: function(datos) {
								//oTable.fnDraw();																				   
								if (datos=="-1") {
									alert("<%=objIdioma.getIdioma("PedidosWeb_Texto30")%>");								
								} else {
									alert("<%=objIdioma.getIdioma("PedidosWeb_Texto31")%>");
								}
							}
						});						
					} else {
						alert("<%=objIdioma.getIdioma("PedidosWeb_Texto32")%>");
					}
				}
			});					 
		 				
			 								
		});			 
	</script>

</head>
<body style="margin:0; padding:0;">



    	<%	codPed=request("codigo")
		
		strRutaImagenes=""
		tieneDevolu=false
				
		PED_CODIGO=""
		PED_ID_IDERP=""
		PED_SUCONTACTO=""
		PED_FECHA=""
		PED_SUMODOENVIO=""
		PED_SUMODOPAGO=""
		PED_TRATADO=false
		PED_NOTIFICADO=false
		PED_ESTADO="0"
	PED_FRAUDULENTO=false		
		sql=" Select * From PEDIDOS P left Join CONTACTOS C on (P.PED_SUCONTACTO=C.CON_CODIGO) left Join TIPOS_MODOPAGO TP on (P.PED_SUMODOPAGO=TP.MPA_CODIGO) left Join TIPOS_MODOENVIO TE on (P.PED_SUMODOENVIO=TE.MEN_CODIGO) Where PED_CODIGO=" & codPed & " "

		Set rsDatosPedido = connCRM.abrirRecordSet(sql,3,1)					
		if not rsDatosPedido.EOF then	
			PED_CODIGO=rsDatosPedido("PED_CODIGO")
			PED_ID_IDERP=rsDatosPedido("PED_ID_IDERP")
			PED_SUCONTACTO=rsDatosPedido("CON_EMAIL")
			PED_FECHA=rsDatosPedido("PED_FECHA")
            PED_FECHA_FACTURA = rsDatosPedido("PED_FECHA_FACTURA")
			PED_SUMODOENVIO=rsDatosPedido("MEN_DESCRIPCION")
			PED_SUMODOPAGO=rsDatosPedido("MPA_DESCRIPCION")
			PED_TRATADO=rsDatosPedido("PED_TRATADO")
			PED_NOTIFICADO=rsDatosPedido("PED_NOTIFICADO")
			PED_ESTADO=rsDatosPedido("PED_ESTADO")
			PED_FRAUDULENTO=rsDatosPedido("PED_FRAUDULENTO")
		end if
		rsDatosPedido.cerrar()
		Set rsDatosPedido = nothing		
		
		%>

		<h1 style="width:100%; margin:0; padding:0 0 10px 0;  clear:both; font-size:15px;" ><%=objIdioma.getIdioma("PedidosWeb_Titulo2")%></h1>
		
    	<!--<div style="width:100%; clear:both; float:left; text-align:left; padding:0px; margin:0 0 15px 0; border-bottom: solid 2px #f60">
    		<div style="margin:0px; padding:0px; width:80%; float:left; text-align:left;">		-->
				<ul id="idListadico" class="listadico">
					<li>
						<span class="agrupacionEtiquetas">
							<span class="cadaEtiqueta"><%=objIdioma.getIdioma("PedidosWeb_Texto33")%></span>
							<span class="cadaContenido"><%=PED_ID_IDERP%></span>
						</span>
					</li>
					<li>
						<span class="agrupacionEtiquetas">
							<span class="cadaEtiqueta"><%=objIdioma.getIdioma("PedidosWeb_Texto34")%></span>
							<span class="cadaContenido"><%=PED_SUCONTACTO%></span>
						</span>
					</li>
					<li>
						<span class="agrupacionEtiquetas">
							<span class="cadaEtiqueta"><%=objIdioma.getIdioma("PedidosWeb_Texto35")%></span>
							<span class="cadaContenido"><%=PED_FECHA%></span>
						</span>
					</li>			
					<li>
						<span class="agrupacionEtiquetas">
							<span class="cadaEtiqueta"><%=objIdioma.getIdioma("PedidosWeb_Texto36")%></span>
							<span class="cadaContenido"><%=PED_SUMODOENVIO%></span>
						</span>
					</li>	
					<li>
						<span class="agrupacionEtiquetas">
							<span class="cadaEtiqueta"><%=objIdioma.getIdioma("PedidosWeb_Texto37")%></span>
							<span class="cadaContenido"><%=PED_SUMODOPAGO%></span>
						</span>
					</li>	            					
					<li>
						<span class="agrupacionEtiquetas">
							<span class="cadaEtiqueta"><%=objIdioma.getIdioma("PedidosWeb_Texto38")%></span>
							<span class="cadaContenido">
								<%if PED_TRATADO then
									response.Write("Si")
								else
									response.Write("No")								
								end if%> 
							</span>
						</span>
					</li>	            					            
				</ul>
                
				<%visibilidadFraude=" style=""display:none;"" "
				visibilidadFraude2=" "
				if PED_FRAUDULENTO then
					visibilidadFraude=" "
					visibilidadFraude2=" style=""display:none;"" "
                end if%>
                
               <a id="btnFraudeQuitar" title="Quitar como fraudulento" rel="<%=PED_CODIGO%>" <%=visibilidadFraude%> ><%="Quitar como<br />fraudulento"%></a>                                                   
               <a id="btnFraudePoner" title="Poner como fraudulento" rel="<%=PED_CODIGO%>" <%=visibilidadFraude2%> ><%="Marcar como<br />fraudulento"%></a>                  
			<!--</div>-->
			
			<!--<div style="margin:0px; padding:0px; width:18%; float:right; text-align:left;">-->
				<!-- include virtual="/dmcrm/APP/Comun2/cargarNavegadorCodigos.asp"-->
			<!--</div>-->
		</div>				
		
		<%if PED_TRATADO then%>
		<h1 style="width:100%; margin:0; padding:0 0 10px 0;  clear:both; font-size:15px;" ><%=objIdioma.getIdioma("PedidosWeb_Titulo3")%></h1>
		
		<div style="float:left; background:#F1F1F1; border:solid 1px #CDCDCD;">
			<div style=" float:left; width:200px; border-right:dashed 1px #CDCDCD; height:80px; ">
				<span id="textoNotificado" style="margin:10px 0 0 20px; float:left; font-size:12px; font-weight:bold;">
					<%if PED_NOTIFICADO then
						response.Write(objIdioma.getIdioma("PedidosWeb_Texto39"))	
					else
						response.Write(objIdioma.getIdioma("PedidosWeb_Texto40"))							
					end if%>
				</span>
				<a id="btnNotificarPedidos" title="Enviar email del pedido" style=" float:left; padding:5px 0 0 30px; margin:5px 0 0 20px; font-weight:bold; color:#8C8C8D; width:118px;" ><%=objIdioma.getIdioma("PedidosWeb_Texto41")%></a>				
			</div>
            
                <%valorEnviado="0"
				  valorEntregado="0"
				if trim(PED_ESTADO)="2" then
					valorEnviado="1"
					valorEntregado="0"
				elseif trim(PED_ESTADO)="3" then
					valorEnviado="1"
					valorEntregado="1"					
				end if%>                            
			<div style=" float:left; width:300px; height:40px; ">
				<span style=" position:relative; top:-10px; left:45px; font-size:12px; font-weight:bold; ">
					<%=objIdioma.getIdioma("PedidosWeb_Texto42")%>
				</span>	
                <%estiloEstado=" margin-left:0px; "
				if trim(valorEnviado)="0" then
					estiloEstado=" margin-left:-40px; "
				end if%>
				<span class="capaImgBoton" style="margin-top:10px; margin-left:52px; ">
					<img id="bnEnviarPedido" rel="<%=valorEnviado%>" style="cursor:pointer; <%=estiloEstado%>;"  src="/DMCrm/APP/Imagenes/onOff.jpg" />
				</span>		
				<br/>
                <%estiloEstado=" margin-left:0px; "
				if trim(valorEntregado)="0" then
					estiloEstado=" margin-left:-40px; "
				end if%>                
				<span rel="" style=" position:relative; margin-top:0px; top:-8px; left:30px; font-size:12px; font-weight:bold; ">
					<%=objIdioma.getIdioma("PedidosWeb_Texto43")%>
				</span>	
				<span class="capaImgBoton" style="margin-top:5px; margin-left:36px; ">
					<img id="bnRecibirPedido"  style="cursor:pointer; <%=estiloEstado%>" rel="<%=valorEntregado%>"  src="/DMCrm/APP/Imagenes/onOff.jpg" />
				</span>								
			
			</div>		
            <div style="float:left; width:200px; border-left:dashed 1px #CDCDCD; height:80px">
                <span style="margin:10px 0 0 20px; font-size:12px; font-weight:bold;display:block">Fecha Factura:</span>
                <span style="margin-top:5px; margin-left:20px"><%=PED_FECHA_FACTURA%></span>
            </div>	
		</div>		
		<%end if%>
        
 
		
<h1 id="h1Titulo" style="width:100%; margin:0 0 20px 0; padding:20px 0 0 0;  clear:both; font-size:15px;"><%=objIdioma.getIdioma("PedidosWeb_Titulo4")%></h1>  
          

	<div id="tablaCestaDeLaCompra">
		<form id="frmCesta" name="frmCesta" method="post" action="">	
			<table id="tablaCesta">
				<tr>
					<th style="width:43%; text-align:left;"><%=objIdioma.getIdioma("PedidosWeb_Texto44")%></th>
					<th style="width:12%; text-align:center;"><%=objIdioma.getIdioma("PedidosWeb_Texto45")%></th>
					<th style="width:13%; text-align:center;"><%=objIdioma.getIdioma("PedidosWeb_Texto46")%></th>
					<th style="width:17%; text-align:right;"><%=objIdioma.getIdioma("PedidosWeb_Texto47")%></th>
					<th style="width:15%; text-align:right;"><%=objIdioma.getIdioma("PedidosWeb_Texto48")%></th>
				</tr>
				<%
					sql=" Select * "
	'				sql=sql & ",(CASE WHEN PEDLI_PRECIOPAGADO > 0.00 THEN PEDLI_PRECIOPAGADO ELSE PEDLI_PRECIOORIGINAL END) AS PF "
					sql=sql & " From PEDIDOS P Inner Join PEDIDOS_LINEAS PL ON (P.PED_CODIGO=PL.PEDLI_SUPEDIDO) "
					sql=sql & " Where PED_CODIGO=" & codPed

					Set rsZapato = connCRM.AbrirRecordset(sql,3,1)
					
					carritoVacio = false
					totalPrecio = 0
					sumaGastosDevolucion=0
					numProductos=0
					if rsZapato.eof then 
						carritoVacio = true
					end if
					
					while not rsZapato.eof 
						if trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"10" And trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"20" then 
							numProductos=numProductos + 1
							
							
								if tieneDevolu then
											'*******Comprobamos la existencia de devolucion
											sqlDevolucion=" Select count(PEDD_CODIGO) as contDevolucion,sum(PEDD_COSTEDEVOLUCION) as costesDevolucion,convert(varchar,PEDD_FECHAACEPTACION," & obtenerFormatoFechaSql() & ") as fechaDevolucion,PEDD_REALIZADO From PEDIDOS_DEVOLUCIONES Where PEDD_SUPEDIDO=" & codPed & " And PEDD_SULINEA=" & rsZapato("PEDLI_CODIGO") & " And PEDD_SUPRODUCTO=" & rsZapato("PEDLI_SUPRODUCTO_IDERP") & " And PEDD_TALLA='" & rsZapato("PEDLI_TALLA")  & "'  Group by convert(varchar,PEDD_FECHAACEPTACION," & obtenerFormatoFechaSql() & "),PEDD_REALIZADO "

											' PEDD_DESCRIPCION, PEDD_PRECIOORIGINAL, PEDD_PRECIOPAGADO, PEDD_CANTIDAD, PEDD_FECHASOLICITUD) 
											devolucionSolicitada=false
											devolucionRealizada=false		
											devolucionAceptada=false
											fechaDevolucion=""		
				
											Set rsDevolucion = connCRM.AbrirRecordset(sqlDevolucion,3,1)
											numDevolucion=0
											if not rsDevolucion.eof then
												devolucionSolicitada=true
												if isdate(rsDevolucion("fechaDevolucion")) then
													if cdate(rsDevolucion("fechaDevolucion"))<=date() then
														devolucionRealizada=true
														fechaDevolucion=rsDevolucion("fechaDevolucion")
														
														cadaCosteDevolucion=0
														if not isnull(rsDevolucion("costesDevolucion")) then
															cadaCosteDevolucion=cdbl(rsDevolucion("costesDevolucion"))
														end if
														sumaGastosDevolucion=sumaGastosDevolucion + cdbl(cadaCosteDevolucion)
														
														if not isnull(rsDevolucion("PEDD_REALIZADO")) then
															if rsDevolucion("PEDD_REALIZADO") then
																devolucionAceptada=true
															end if
														end if
													end if
												end if
'												numDevolucion=rsDevolucion("contDevolucion")
												numDevolucion=1
											else
												devolucionSolicitada=false
											end if
											rsDevolucion.cerrar
											Set rsDevolucion = nothing									
											'******** Fin Comprobamos la existencia de devolucion		
				
											contUnidInicial=1

											 unidadesLinea=rsZapato("PEDLI_UNIDADES")
											if numDevolucion>0 then
												unidadesLinea=unidadesLinea - numDevolucion 
												
											end if 'end de if numDevolucion>0 then
											
								else											
									contUnidInicial=rsZapato("PEDLI_UNIDADES")	
									unidadesLinea=rsZapato("PEDLI_UNIDADES")	
								end if											
								
								for contUnid=contUnidInicial to cint(unidadesLinea)															
	
							%>
							<tr>
								<td>
                                	<%if trim(strRutaImagenes)<>"" then%>
                                        <a class="ku_fancybox" href="<%=strRutaImagenes%>/item/1000x667/<%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%>_1.jpg" title="<%=rsZapato("PEDLI_DESCRIPCION")%>">
                                            <img  class="carritoImagen" src="<%=strRutaImagenes%>/item/168x112/<%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%>_1.jpg" alt="<%=rsZapato("PEDLI_DESCRIPCION") %>" />
                                        </a>
                                    <%end if%>
									<span class="carritoRef"><%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%></span><br />
									<span class="carritoNombre"><%=rsZapato("PEDLI_DESCRIPCION")%></span>
								</td>
								<td class="top center">
									<%if not isnull(rsZapato("PEDLI_TALLA")) then 
											response.write replace(rsZapato("PEDLI_TALLA"),"M","&#189;")
										else
											response.Write("&nbsp;")
									  end if%>
								</td>
								<td class="top center"><%=unidadesLinea%></td>                        
								<td class="right top">
									<%
										response.Write("<span class=""precioFinalCesta"">" & FormatNumber(rsZapato("PEDLI_PRECIOPAGADO"),2) & "&nbsp;&euro;</span>")
										if cdbl(rsZapato("PEDLI_PRECIOPAGADO")) < cdbl(rsZapato("PEDLI_PRECIOORIGINAL")) then
											response.Write("<br /><span class=""precioAnteriorCesta""><s>" & FormatNumber(rsZapato("PEDLI_PRECIOORIGINAL"),2) & "&nbsp;&euro;</s></span>")
										end if
									%>
								</td>
								<td class="right top"><%= FormatNumber(cdbl(rsZapato("PEDLI_PRECIOPAGADO")) * unidadesLinea,2) %>&nbsp;&euro;</td>
							</tr>
							<%next	'for contUnid=1 to cint(unidadesLinea)	
						else 'else de if trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"10" And trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"20" then  %>
							<tr>
								<td colspan="4">
									<span class="carritoNombre"><%=rsZapato("PEDLI_DESCRIPCION")%></span>
								</td>
								<td class="right"><%= FormatNumber(cdbl(rsZapato("PEDLI_PRECIOPAGADO")),2) %>&nbsp;&euro;</td>
							</tr>                        				
						<%end if 'end de if trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"10" And trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"20" then 
						totalPrecio = totalPrecio + (cdbl(rsZapato("PEDLI_PRECIOPAGADO")) * unidadesLinea)
						
							'**********************Linea devuelta
							if numDevolucion>0 then %>	
								<%if trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"10" And trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"20" then %>									
								<tr>
									<td>
	                                	<%if trim(strRutaImagenes)<>"" then%>                                    
                                            <a class="ku_fancybox" href="<%=strRutaImagenes%>/item/1000x667/<%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%>_1.jpg" title="<%=rsZapato("PEDLI_DESCRIPCION")%>">
                                                <img  class="carritoImagen" src="<%=strRutaImagenes%>/item/168x112/<%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%>_1.jpg" alt="<%=rsZapato("PEDLI_DESCRIPCION") %>" />
                                            </a>
                                        <%end if%>
										<span class="carritoRef"><%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%></span><br />
										<span class="carritoNombre"><%=rsZapato("PEDLI_DESCRIPCION")%></span>
										
                                        <%if devolucionSolicitada then%>
											<%if devolucionRealizada  then%>
                                                <%if devolucionAceptada then%>
                                                    <div class="devolucionLinea">Art�culo devuelto el <%=" " & fechaDevolucion%>
                                                <%else%>                                            
                                                    <div class="devolucionLinea">Devoluci�n rechazada el <%=" " & fechaDevolucion%>                                        
                                                <%end if%>
                                            <%else%>
                                                <div class="devolucionLinea" >En proceso de devoluci�n                                                
                                            <%end if%>
                                         <%end if%>   
										<%if numDevolucion>1 then%>
										(<%=numDevolucion & " "%>unid.)
										<%end if%>
										<br/>
										<a  class="linkTratarDevolucion" href="/dmcrm/APP/Ventas/pedidos/tratarDevolucion.asp?pd=<%=codPed%>&lp=<%=rsZapato("PEDLI_CODIGO")%>&pl=<%=rsZapato("PEDLI_SUPRODUCTO_IDERP")%>"  >Mas info</a>				
										</div>		
													
										
									</td>
									<td class="top center"  ><%=replace(rsZapato("PEDLI_TALLA"),"M","&#189;")%></td>
									<td class="top center"><%=unidadesLinea+1%></td>                        
									<td class="right top">
										<%
											response.Write("<span class=""precioFinalCesta"">" & FormatNumber(rsZapato("PEDLI_PRECIOPAGADO"),2) & "&nbsp;&euro;</span>")
											if cdbl(rsZapato("PEDLI_PRECIOPAGADO")) < cdbl(rsZapato("PEDLI_PRECIOORIGINAL")) then
												response.Write("<br /><span class=""precioAnteriorCesta""><s>" & FormatNumber(rsZapato("PEDLI_PRECIOORIGINAL"),2) & "&nbsp;&euro;</s></span>")
											end if
										%>
									</td>
									<td class="right top"><%= FormatNumber(cdbl(rsZapato("PEDLI_PRECIOPAGADO")) * (unidadesLinea+1),2) %>&nbsp;&euro;</td>
								</tr>
								<%end if 'end de if trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"10" And trim(rsZapato("PEDLI_SUPRODUCTO_IDERP"))<>"20" then 			
								totalPrecio = totalPrecio + (cdbl(rsZapato("PEDLI_PRECIOPAGADO")) * (unidadesLinea+1))									
							else 'else de if numDevolucion>0 then
	
								
							end if 'end de if numDevolucion>0 then 
							'**********************Fin Linea devuelta					
						rsZapato.movenext
					wend 
					
					rsZapato.cerrar
					Set rsZapato = nothing
					
					''''''''''''''''''''''''Tratamos los posibles cambios de talla''''''''''
					sqlCambio=" Select * "
'					sqlCambio=sqlCambio & ""
					sqlCambio=sqlCambio & " From PEDIDOS P Inner Join PEDIDOS PC on (P.PED_CODIGO=PC.PED_PADRE_DEVOLUCION) "
					sqlCambio=sqlCambio & " Inner Join PEDIDOS_LINEAS PLC on (PC.PED_CODIGO=PLC.PEDLI_SUPEDIDO) "
					sqlCambio=sqlCambio & " Where P.PED_CODIGO=" & codPed & " And PLC.PEDLI_TALLA<>'0'"
					sqlCambio=sqlCambio & " And P.PED_TRATADO=1 And (P.PED_ANULADO=0 Or P.PED_ANULADO is null ) "
					sqlCambio=sqlCambio & " And PC.PED_TRATADO=1 And (PC.PED_ANULADO=0 Or PC.PED_ANULADO is null ) "					
					sqlCambio=sqlCambio & " Order by PC.PED_FECHA ASC, PLC.PEDLI_SUPRODUCTO_IDERP DESC, PLC.PEDLI_FECHACAMBIO ASC "															
					Set rsCambios = connCRM.AbrirRecordset(sqlCambio,3,1)
					numCambios=1
					totalCambios=rsCambios.recordcount
					if not rsCambios.eof then
						%>	<tr  >
								<td style="border-bottom: 1px dashed #CDCDCD; "  colspan="5"><span style="font-size:12px; font-weight:bold;">Cambios de tallas:</span></td> <!--border-top:solid 1px black;-->
							</tr>    	
						<%					
					end if
					while not rsCambios.eof 
						bordeCambio="border-bottom: 1px dashed #CDCDCD;"
						if numCambios=totalCambios then
							bordeCambio="border-bottom: 1px solid #CDCDCD;"
						end if		
						numCambios=numCambios+1			
						%>	<tr  >
								<td style=" padding-left:40px; <%=bordeCambio%> "  >
								<%=rsCambios("PEDLI_DESCRIPCION")%>
								</td>
								<td style=" padding-left:40px; <%=bordeCambio%> " colspan="2"  >
								<%if trim(rsCambios("PEDLI_TALLA"))<>"0" then 
									tallaAnterior=""
									sqlTallaAnt=" Select PEDLI_CODIGO, PEDLI_TALLA From PEDIDOS_LINEAS Where PEDLI_SUPEDIDO=" & codPed & " And PEDLI_TALLA<>'" & rsCambios("PEDLI_TALLA") & "' And PEDLI_SUPRODUCTO_IDERP=" & rsCambios("PEDLI_SUPRODUCTO_IDERP")
									Set rsTallaAnt = connCRM.AbrirRecordset(sqlTallaAnt,3,1)
									if not rsTallaAnt.eof then
										tallaAnterior=rsTallaAnt("PEDLI_TALLA")
									end if
									rsTallaAnt.cerrar
									Set rsTallaAnt = nothing
									response.Write(replace(tallaAnterior,"M","&#189;") & " -> " & replace(rsCambios("PEDLI_TALLA"),"M","&#189;") & "&nbsp;&nbsp;")
									%><script language="javascript" type="text/javascript" >
										$(document).ready(function () {	
											// $(".colTalla_<%'=rsCambios("PEDLI_CODIGO")%>").html('<%'=replace(rsCambios("PEDLI_TALLA"),"M","&#189;")%>');									
										});	
									
									
                                    </script>
                                    <%
								end if
								response.Write("(" & escribir_Fecha_Simple(rsCambios("PED_FECHA")) & ")")
								%> 
								</td>
								<td style=" <%=bordeCambio%> "  >
									<%if trim(rsCambios("PEDLI_SUPRODUCTO_IDERP"))<>"10" And trim(rsCambios("PEDLI_SUPRODUCTO_IDERP"))<>"20" then %>
										<%if isnull(rsCambios("PEDLI_FECHACAMBIO")) then%>
											<div class="cambioTallaLinea" >En proceso de cambio
										<%elseif not isnull(rsCambios("PEDLI_CAMBIOREALIZADO")) then%>	
											<%if rsCambios("PEDLI_CAMBIOREALIZADO") then%>
												<div class="cambioTallaLinea">Aceptado el <%=" " & escribir_Fecha_Simple(rsCambios("PEDLI_FECHACAMBIO"))%>											
											<%else%>
												<div class="cambioTallaLinea">Rechazado el <%=" " & escribir_Fecha_Simple(rsCambios("PEDLI_FECHACAMBIO"))%>																							
											<%end if%>
										<%else%>	
											<div class="cambioTallaLinea">Rechazado el <%=" " & escribir_Fecha_Simple(rsCambios("PEDLI_FECHACAMBIO"))%>																																	
										<%end if%>
										<br/>
										<a  class="linkTratarCambioTalla" href="/dmcrm/APP/Ventas/pedidos/tratarCambioTalla.asp?plc=<%=rsCambios("PEDLI_CODIGO")%>"  >Mas info</a>				
										</div>									
									<%else%>
									&nbsp;													
									<%end if%>
								</td>								
								<td style="text-align:right; <%=bordeCambio%>"><%=FormatNumber(rsCambios("PEDLI_PRECIOPAGADO"),2)%>&nbsp;&euro;</td>
							</tr>    	
						<%
						
							totalPrecio = totalPrecio + (cdbl(rsCambios("PEDLI_PRECIOPAGADO")))	
						rsCambios.movenext
					wend						
					rsCambios.cerrar
					Set rsCambios = nothing
					''''''''''''''''''''''''Fin de Tratamos los posibles cambios de talla'''	
	
					if not carritoVacio then %>
						<tr class="totalCarrito">
							<td class="totalTxt" colspan="4"><%=objIdioma.getIdioma("PedidosWeb_Texto48")%></td>
							<td style="text-align:right;"><%=FormatNumber(totalPrecio,2)%>&nbsp;&euro;</td>
						</tr>    
					<% end if 
					'if sumaGastosDevolucion>0 then%>
							<tr>
								<td colspan="4" style="border-top:solid 1px #cdcdcd;">
									<span class="carritoNombre"><%=objIdioma.getIdioma("PedidosWeb_Texto49")%></span>
								</td>
								<td class="right" style="border-top:solid 1px #cdcdcd;"><%= FormatNumber(sumaGastosDevolucion,2) %>&nbsp;&euro;</td>
							</tr>  						
					<%'end if%>
					
					
			</table>




        
        
        <%if numProductos=0 then%>
			<script language="javascript" type="text/javascript" >
                $(document).ready(function () {	
                     $("#tablaCesta").html('<%=objIdioma.getIdioma("PedidosWeb_Texto50")%>');									
                });			 
            </script>        
        <%end if%>


</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->






