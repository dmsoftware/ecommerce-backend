<!-- include virtual="/dmcrm/includes/permisos.asp"-->
<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->

<%
conexion = "Provider=SQLNCLI10;Data Source=;Initial Catalog=;User ID=;Password="
set conn = new DMWConexion
conn.AbrirSQL ("" & conexion)

'***** FUNCIONES *****'
function quitarTildes(texto)
    res = texto
    res = replace(replace(res, "�", "a"), "�", "A")
    res = replace(replace(res, "�", "e"), "�", "E")
    res = replace(replace(res, "�", "i"), "�", "I")
    res = replace(replace(res, "�", "o"), "�", "O")
    res = replace(replace(res, "�", "u"), "�", "U")
    quitarTildes = res
end function

'***** PAR�METROS *****'
fechaInicio = request("fechaInicio")
fechaFin = request("fechaFin")
tipoFecha = request("cbFecha")

periodo = fechaInicio & " - " & fechaFin

nombreExcel = fechaInicio & "-" & fechaFin
if tipoFecha = 1 then nombreExcel = "Fecha-Compra-" & nombreExcel else nombreExcel = "Fecha-Factura-" & nombreExcel end if

arrEstados = Array("Pendiente", "��??", "Enviado", "��??", "Entregado", "Proceso de cambio / devoluci�n", "Cancelado / Devuelto", "No pagado")

'***** CABECERA *****'
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "content-disposition", " filename=Costes-" & nombreExcel & ".xls"

'***** OBTENEMOS LOS DATOS *****'
query = "SELECT P1.PED_SUTIENDA,TT.TT_NOMBRE,TT.TT_DOMINIO,P1.PED_CODIGO,P1.PED_SESION,P1.PED_IP, P1.PED_ID_IDERP, DateDiff(d,'2014-04-23', P1.PED_FECHA) AS SEAPLICADESCUENTO, CONVERT(varchar(10), P1.PED_FECHA, 103) AS FECHA, CONVERT(varchar(10), P1.PED_FECHA_FACTURA, 103) AS FECHAFACTURA, P1.PED_SUCONTACTO, P1.PED_SUMODOENVIO, P1.PED_SUMODOPAGO, P1.PED_PADRE_DEVOLUCION, " &_
        "P1.PED_FRAUDULENTO, P1.PED_TRATADO, P1.PED_MRW, P1.PED_DIRECCIONENVIO, P1.PED_NOTIFICADO, P1.PED_ESTADO,  P1.PED_SUCLIENTEEMPRESA, P1.PED_SUCLIENTEEMPRESA_IDERP, P1.PED_SUCOMERCIAL, " &_
        "CAST(P1.PED_OBSERVACIONES AS varchar) AS PED_OBSERVACIONES, P1.PED_SUDEPARTAMENTO, P1.PED_SUCOMERCIO, isnull(P1.PED_ANULADO,0) as PED_ANULADO, C.CON_EMAIL AS emailPropietario, C.CON_EMAIL + ' (' + CON_NOMBRE + ISNULL(' ' + CON_APELLIDO1,'') + ')' AS nomPropietario, " &_
		"TabImportes.importeBruto, TabImportes.importeNeto, " &_
	    "stuff ((SELECT cast('|' AS varchar(max)) + U.PEDLI_DESCRIPCION " &_
        "FROM PEDIDOS_LINEAS U " &_
        "WHERE U.PEDLI_SUPEDIDO = P1.PED_CODIGO And (U.PEDLI_SUPRODUCTO_IDERP>100) " &_
        "GROUP BY U.PEDLI_DESCRIPCION " &_
		"ORDER BY U.PEDLI_DESCRIPCION FOR xml path('')), 1, 1, '' " &_
        ") AS lineasPedido " &_
		", (CASE WHEN TabLineas.numLinPed=TabDevol.numDevol THEN 6 " &_
        "ELSE CASE WHEN isnull(TabProcDev.numProcDev,0)>0 THEN 5 " &_
        "ELSE CASE WHEN (P1.PED_ESTADO is null or P1.PED_ESTADO=1) And P1.PED_TRATADO=1 THEN 0 " &_
        "ELSE CASE WHEN P1.PED_ESTADO=2 And P1.PED_TRATADO=1  THEN 2 " &_
        "ELSE CASE WHEN P1.PED_ESTADO=3 And P1.PED_TRATADO=1  THEN 4 " &_
        "ELSE CASE WHEN P1.PED_TRATADO=0 THEN 7 ELSE NULL END " &_
        "END END END END END " &_	
        ") as iconEstadoPedido " &_
        ", TabLineas.numLinPed , TabDevol.numDevol " &_
        "FROM PEDIDOS P1 INNER JOIN CONTACTOS C ON (P1.PED_SUCONTACTO = C.CON_CODIGO) " &_
        "INNER JOIN TIPOS_TIENDA TT ON (P1.PED_SUTIENDA=TT.TT_CODIGO) " &_
        "LEFT JOIN ( Select PCT.PED_PADRE_DEVOLUCION as pedConHijo From PEDIDOS PCT Where PCT.PED_TRATADO=1 And (PCT.PED_ANULADO=0 Or PCT.PED_ANULADO is null ) And PCT.PED_PADRE_DEVOLUCION is not null ) as PedHijos ON (P1.PED_CODIGO=PedHijos.pedConHijo) " &_
        "LEFT JOIN ( Select PLIN.PEDLI_SUPEDIDO as pedImpor, sum(PLIN.PEDLI_PRECIOPAGADO * PLIN.PEDLI_UNIDADES) as importeBruto, sum(PLIN.PEDLI_PRECIOPAGADO * PLIN.PEDLI_UNIDADES * 100 / (100 + PLIN.PEDLI_IVA)) as importeNeto From PEDIDOS_LINEAS PLIN Group by PLIN.PEDLI_SUPEDIDO ) as TabImportes ON (P1.PED_CODIGO=TabImportes.pedImpor) " &_
        "LEFT JOIN ( Select PLIN.PEDLI_SUPEDIDO as pedLineas, sum(PLIN.PEDLI_UNIDADES) as numLinPed From PEDIDOS_LINEAS PLIN Where PLIN.PEDLI_SUPRODUCTO_IDERP > 100 Group by PLIN.PEDLI_SUPEDIDO ) as TabLineas ON (P1.PED_CODIGO=TabLineas.pedLineas) " &_
        "LEFT JOIN ( Select PDEV.PEDD_SUPEDIDO as pedDevol,COUNT(PDEV.PEDD_CODIGO) as numDevol From PEDIDOS_DEVOLUCIONES PDEV Where PDEV.PEDD_FECHASOLICITUD is not null And PDEV.PEDD_REALIZADO=1 Group by PDEV.PEDD_SUPEDIDO ) as TabDevol ON (P1.PED_CODIGO=TabDevol.pedDevol) " &_
        "LEFT JOIN ( " &_
        "SELECT PDEV.PEDD_SUPEDIDO as pedProcDev,COUNT(PDEV.PEDD_CODIGO) as numProcDev " &_
        "FROM PEDIDOS_DEVOLUCIONES PDEV " &_
        "WHERE PDEV.PEDD_FECHASOLICITUD is not null And PDEV.PEDD_FECHAACEPTACION is null " &_
        "GROUP BY PDEV.PEDD_SUPEDIDO " &_
        "UNION " &_
        "SELECT PCAMDev.PED_CODIGO,1 as numProcDev " &_
        "FROM PEDIDOS PCAMDev " &_
        "WHERE (Select count(PC.PEDLI_CODIGO) From PEDIDOS_LINEAS PC Where PEDLI_SUPEDIDO in ( Select PCT.PED_PADRE_DEVOLUCION From PEDIDOS PCT INNER JOIN PEDIDOS_LINEAS PCL on (PCT.PED_CODIGO=PCL.PEDLI_SUPEDIDO) Where PCT.PED_TRATADO=1 And (PCT.PED_ANULADO=0 Or PCT.PED_ANULADO is null ) And PCT.PED_PADRE_DEVOLUCION is not null And PCL.PEDLI_FECHACAMBIO is null And PCL.PEDLI_SUPRODUCTO_IDERP<>10 And PCL.PEDLI_SUPRODUCTO_IDERP<>20 ) " &_
        "AND PC.PEDLI_SUPEDIDO=PED_CODIGO ) > 0 " &_
		") as TabProcDev ON (P1.PED_CODIGO=TabProcDev.pedProcDev) " &_			
        "WHERE (P1.PED_ANULADO=0 Or P1.PED_ANULADO is null ) AND ( P1.PED_PADRE_DEVOLUCION is null Or PedHijos.pedConHijo is not null ) " &_
        "AND CONVERT(date, "
                
if tipoFecha = 1 then query = query & "PED_FECHA" else query = query & "PED_FECHA_FACTURA" end if
        
query = query & ", 103) BETWEEN CONVERT(date, '" & fechaInicio & "', 103) AND CONVERT(date, '" & fechaFin & "', 103) AND P1.PED_TRATADO <> 0 " &_
                "GROUP BY P1.PED_SUTIENDA,TT.TT_NOMBRE,TT.TT_DOMINIO,P1.PED_CODIGO,P1.PED_SESION,P1.PED_IP, P1.PED_ID_IDERP, P1.PED_FECHA, P1.PED_FECHA_FACTURA, P1.PED_SUCONTACTO, P1.PED_SUMODOENVIO, P1.PED_SUMODOPAGO, P1.PED_PADRE_DEVOLUCION, " &_
                "P1.PED_FRAUDULENTO, P1.PED_TRATADO, P1.PED_MRW, P1.PED_DIRECCIONENVIO, P1.PED_NOTIFICADO, P1.PED_ESTADO,  P1.PED_SUCLIENTEEMPRESA, P1.PED_SUCLIENTEEMPRESA_IDERP, " &_
                "P1.PED_SUCOMERCIAL, CAST(P1.PED_OBSERVACIONES AS varchar), P1.PED_SUDEPARTAMENTO, P1.PED_SUCOMERCIO, " &_
                "isnull(P1.PED_ANULADO,0), C.CON_EMAIL, C.CON_EMAIL + ' (' + CON_NOMBRE + ISNULL(' ' + CON_APELLIDO1,'') + ')', " &_
		        "TabImportes.importeBruto,TabImportes.importeNeto, TabLineas.numLinPed , TabDevol.numDevol, TabProcDev.numProcDev " &_
                "ORDER BY "

if tipoFecha = 1 then query = query & "P1.PED_FECHA" else query = query & "P1.PED_FECHA_FACTURA" end if

query = query & ", PED_CODIGO"

Set rsPedidos = connCRM.abrirRecordSet(query, 0, 1)
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" charset="Unicode">
	<head>
        <title>Exportacion Costes</title>
    </head>
    <body>
        <table id="tablaExportacionCostes" cellpadding="0" cellspacing="0" width="100%" border="1">
            <thead>
                <tr style="background-color:#40a7d4"><th style="color:White" colspan="7">PERIODO: <%=periodo%></th></tr>
                <tr style="background-color:#40a7d4; height:60px">
                    <th style="color:White">Pedido AVELON</th>
                    <th style="color:White">Pedido CRM</th>
                    <th style="color:White"><% if tipoFecha = 1 then %>Fecha Compra<% else %>Fecha Factura<% end if %></th>
                    <th style="color:White">Tienda</th>
                    <th style="color:White">Forma de pago</th>
                    <th style="color:White">Estado</th>
                    <th style="color:White">
                        <table border="1">
                            <tr>
                                <th style="color:White">Articulo</th>
                                <th style="color:White">Unidades</th>
                                <th>
							        <table border="1">
								        <tr><th colspan="4" style="color:White">Venta</th></tr>
								        <tr>
									        <th style="color:White">Precio Original</th>
									        <th style="color:White">Precio Pagado</th>
                                            <th style="color:White">Descuento �</th>
                                            <th style="color:White">Descuento %</th>
								        </tr>
							        </table>
						        </th>
                            </tr>
                        </table>
                    </th>
                </tr>
            </thead>
            <tbody>
            <% 
            filaInicialReal = 4
            filaInicial = 4
            filaFinal = 4
            while not rsPedidos.eof
                '***** OMITIMOS LOS PEDIDOS DEVUELTOS SI EL USUARIO LO SOLICITA *****'
                if request("cbOmitirDevoluciones") <> 1 or rsPedidos("iconEstadoPedido") <> 6 then

                    '***** OBTENEMOS EL N�MERO DE LINEAS *****'
                    query = "SELECT COUNT(PEDLI_CODIGO) AS CANTIDAD FROM PEDIDOS_LINEAS WHERE PEDLI_SUPEDIDO = " & rsPedidos("PED_CODIGO")
                    Set rsCantidad = connCRM.abrirRecordSet(query, 0, 1)
                    if not rsCantidad.eof then
                        numLineas = rsCantidad("CANTIDAD") + 1
                    end if
                    rsCantidad.cerrar
                    Set rsCantidad = nothing
                
                    select case rsPedidos("iconEstadoPedido")
                        case 6
                            trClass = " style=""background-color:#ff0000"""
                        case else
                            trClass = ""
                    end select
                
                    '***** PINTAMOS LOS DATOS COMUNES *****'
                    response.Write("<tr" & trClass & ">")
                    response.Write("<td valign=""top"">" & rsPedidos("PED_ID_IDERP") & "</td>")
                    response.Write("<td valign=""top"">" & rsPedidos("PED_CODIGO") & "</td>")
                    response.Write("<td valign=""top"">")
                    if tipoFecha = 1 then response.Write(rsPedidos("FECHA")) else response.Write(rsPedidos("FECHAFACTURA")) end if
                    response.Write("</td>")
                    response.Write("<td valign=""top"">" & rsPedidos("TT_NOMBRE") & "</td>")
                    response.Write("<td valign=""top"">")
                    select case rsPedidos("PED_SUMODOPAGO")
                        case 1
                            modoPago = "Pago con tarjeta"
                        case 2 
                            modoPago = "Pago contra reembolso"
                    end select
                    response.Write(modoPago)
                    response.Write("</td>")
                    response.Write("<td valign=""top"">" & arrEstados(rsPedidos("iconEstadoPedido")) & "</td>")
                    response.Write("<td>")
                    response.Write("<table border=""1"">")
                    '***** PINTAMOS CADA LINEA *****'
                    query = "SELECT PEDLI_CODIGO AS ROWID, PEDLI_SUPRODUCTO_IDERP AS ID, PEDLI_DESCRIPCION AS PRODUCTO, (PEDLI_PRECIOORIGINAL * PEDLI_UNIDADES) AS PVPO, (PEDLI_PRECIOPAGADO * PEDLI_UNIDADES) AS PVP, PEDLI_UNIDADES AS UNIDADES " &_
                            "FROM PEDIDOS_LINEAS " &_
                            "WHERE PEDLI_SUPEDIDO = " & rsPedidos("PED_CODIGO") & " " &_
                            "ORDER BY ID DESC"       
                    Set rsLineaPedido = connCRM.abrirRecordSet(query, 0, 1)
                    PVPTotal = 0
                    PVPOTotal = 0
                    while not rsLineaPedido.eof
                        CodigoLineaPedido = rsLineaPedido("ROWID")

                        '***** SI EL PVP ES IGUAL AL PVPO Y ES UN ZAPATO HAY QUE TENER EN CUENTA QUE SE APLICAN DESCUENTOS *****'
                        if cdbl(rsLineaPedido("PVP")) = cdbl(rsLineaPedido("PVPO")) and rsLineaPedido("ID") > 100 AND rsPedidos("SEAPLICADESCUENTO") > 0 then
                            query = "SELECT GroupNumber FROM system.EStoreGroup01ItemLink WHERE ItemNumber = " & rsLineaPedido("ID")
                            Set rsDescuento = conn.abrirRecordSet(query, 0, 1)
                            if not rsDescuento.eof then
                                if rsDescuento("GroupNumber") = 1 or rsDescuento("GroupNumber") = 2 then
                                    aplicarIncremento = 5
                                else
                                    aplicarIncremento = 3
                                end if
                            end if
                            rsDescuento.cerrar()
                            Set rsDescuento = nothing
                        else
                            aplicarIncremento = 0
                        end if 
                        '*******************************************************************************************************'

                        PVP = cdbl(rsLineaPedido("PVP"))
                        PVPO = cdbl(rsLineaPedido("PVPO")) + aplicarIncremento
                        PVPTotal = PVPTotal + PVP
                        PVPOTotal = PVPOTotal + PVPO

                        response.Write("<tr>")                   
                        response.Write("<td style=""mso-number-format:'\@'"">" & quitarTildes(rsLineaPedido("Producto")) & "</td>")
                        response.Write("<td>" & rsLineaPedido("Unidades") & "</td>")
                    
                        response.Write("<td>")
                        response.Write("<table border=""1"">")
                        response.Write("<tr>")
                        response.Write("<td style=""mso-number-format:'0\.00 �'"">" & PVPO & "</td>")
                        response.Write("<td style=""mso-number-format:'0\.00 �'"">" & PVP & " �</td>")
                        response.Write("<td style=""mso-number-format:'0\.00 �'"">=I" & filaFinal & "-" & "J" & filaFinal & "</td>")
                        response.Write("<td style=""mso-number-format:'Percent'"">=SI(ESERROR(1-(J" & filaFinal & " / I" & filaFinal & "));0;1-(J" & filaFinal & " / I" & filaFinal & "))</td>")
                        response.Write("</tr>")
                        response.Write("</table>")
                        response.Write("</td>")     
                        response.Write("</tr>")
                        filaFinal = filaFinal + 1
                        rsLineaPedido.movenext
                    wend 

                    select case rsPedidos("iconEstadoPedido")
                        case 6
                            colorClass = "#FF0000"
                        case else
                            colorClass = "#c6efce"
                    end select

                    if 1 - (PVPTotal / PVPOTotal) >= 0.4 then
                        colorClass = "#ecaf0c"
                    end if                    

                    response.Write("<tr>")
                    response.Write("<td style=""background-color:" & colorClass & """ align=""right""><font color=""0000FF""><b>Total</b></font></td>")
                    response.Write("<td style=""background-color:" & colorClass & """><font color=""0000FF""><b>=SUMA(H" & filaInicial & ":" & "H" & filaFinal - 1 & ")</b></font></td>")
                    response.Write("<td>")
                    response.Write("<table border=""1"">")
                    response.Write("<tr>")
                    response.Write("<td style=""background-color:" & colorClass & "; mso-number-format:'0\.00 �'""><font color=""0000FF""><b>=SUMA(I" & filaInicial & ":" & "I" & filaFinal - 1 & ")</b></font></td>")
                    response.Write("<td style=""background-color:" & colorClass & "; mso-number-format:'0\.00 �'""><font color=""0000FF""><b>=SUMA(J" & filaInicial & ":" & "J" & filaFinal - 1 & ")</b></font></td>")
                    response.Write("<td style=""background-color:" & colorClass & "; mso-number-format:'0\.00 �'""><font color=""0000FF""><b>=I" & filaFinal & "-" & "J" & filaFinal & "</b></font></td>")
                    response.Write("<td style=""background-color:" & colorClass & "; mso-number-format:'Percent'""><font color=""0000FF""><b>=SI(ESERROR(1-(J" & filaFinal & " / I" & filaFinal & "));0;1-(J" & filaFinal & " / I" & filaFinal & "))</b></font></td>")
                    response.Write("</tr>")
                    response.Write("</table>")
                    response.Write("</td>")                  
                    response.Write("</table>")

                    filaInicial = filaFinal + 1
                    filaFinal = filaFinal + 1
                end if
                rsPedidos.movenext
            wend
            %>
            </tbody>
            <tfoot>
                <tr style="height:60px;">
                    <th colspan="6"></th>
                    <th>
                        <table border="1">
                            <tr>
                                <th colspan="2"></th>
                                <th>
							        <table border="1">
								        <tr>
									        <th style="background-color:#00b050; color:#FFF; mso-number-format:'0\.00 �'"><b>=SUMA(I<%=filaInicialReal & ":I" & filaFinal - 1%>) / 2</b></th>
									        <th style="background-color:#00b050; color:#FFF; mso-number-format:'0\.00 �'"><b>=SUMA(J<%=filaInicialReal & ":J" & filaFinal - 1%>) / 2</b></th>
									        <th style="background-color:#00b050; color:#FFF; mso-number-format:'0\.00 �'"><b>=I<%=filaFinal & "-J" & filaFinal%></b></th>
                                            <th style="background-color:#00b050; color:#FFF; mso-number-format:'Percent'"><b>=SI(ESERROR(<%="1-(J" & filaFinal & " / I" & filaFinal & "));0;1-(J" & filaFinal & " / I" & filaFinal%>))</b></th>
								        </tr>
							        </table>
						        </th>    
                            </tr>
                        </table>
                    </th>
                </tr>
                <tr>
                    <th colspan="6"></th>
                    <th>
                        <table border="1">
                            <tr>
                                <th colspan="2"></th>
                                <th>
							        <table border="1">
								        <tr>
									        <th style="color:#000"><b>Total Original</b></th>
									        <th style="color:#000"><b>Total Pagado</b></th>
									        <th style="color:#000"><b>Total Descuento �</b></th>
                                            <th style="color:#000"><b>Total Descuento %</b></th>
								        </tr>
							        </table>
						        </th>    
                            </tr>
                        </table>
                    </th>
                </tr>
            </tfoot>
        </table> 
    </body>
</html>
<%
rsPedidos.cerrar()
Set rsPedidos = nothing

conn.cerrar()
set conn=nothing
%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->





