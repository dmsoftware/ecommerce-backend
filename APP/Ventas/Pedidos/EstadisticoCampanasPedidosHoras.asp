<%'Abrimos la conexion a DMINTEGRA
blnAbrirConexionDMIntegra=true%>

<!-- #include virtual="/dmcrm/includes/inicioPantalla.asp"-->

<style type="text/css">
.paginacionDatosMes { background:#fff; margin:-40px 350px 30px 0; text-align:right; padding:2px 5px 5px 5px; width:auto;}
.paginacionDatosMes a, .paginacionDatosMes label {background:#f1f1f1; padding:2px 5px; text-decoration:none; color:#000; margin:5px 2px 0 2px; display:inline-block;}
.paginacionDatosMes span {background:#ff6600; padding:2px 5px;color:#fff; margin:5px 2px 0 2px; display:inline-block; float:left;}
.paginacionDatosMes a:hover {background:#f60; color:#fff;}
a.VerTabla {padding:2px 5px; display:inline-block; margin-top:5px; margin-left:10px; float:right;}

#TablaGeneralDatos { display:block; clear:both;margin-top:40px;}
.TablaGeneralDatos {width:100%; display:none; margin-top:40px;}
.FilaMeses td {background:#0080c6; color:#fff; font-size:13px; font-weight:bold; text-align:center;}
#TablaGeneralDatos tr td {border-bottom:solid 3px #0080c6; padding:10px 0;}
#TablaGeneralDatos tr td span {border-bottom:solid 1px #ccc; display:inline-block; width:96%; height:20px; overflow:hidden; padding:0 2%;}
td.CampanaNombre {font-weight:bold; background:#f1f1f1; text-align:center;}

#BotonesOpciones {padding:0; width:10%; float:left;}
#GraficosDatosGeneral {width:89%; float:right;}
#BotonesOpciones a { border:none; background:#f1f1f1; cursor:pointer; padding:6px 7px; margin:0 2px 5px 0; font-size:11px; display:block; position:relative;}
#BotonesOpciones a:hover, #BotonesOpciones a.activo { background:#f60; color:#fff;}
#BotonesOpciones a span.mostrarOpciones { position:absolute; top:0; right:-45px; background:#f1f1f1; display:none; width:35px; z-index:5; padding:3px 5px;}
#BotonesOpciones a span.masOpciones { position:absolute; top:0; left:45px; background:#bdbdbd; display:none; width:137px; z-index:5; padding:3px 5px;}
#BotonesOpciones a:hover span.mostrarOpciones, 
#BotonesOpciones a:hover span.mostrarOpciones:hover span.masOpciones {display:block;}
#BotonesOpciones a span img {float:left; margin:0 5px}
#BotonesOpciones a span img:hover {filter: alpha(opacity=50); opacity: 0.5;}

#BotonesOpciones a:hover span.masOpcionesNEW {position:absolute; top:0; right:-45px; background:#f1f1f1; display:none; width:35px; z-index:5; padding:3px 5px; display:block;}
#BotonesOpciones a span.masOpcionesNEW {position:absolute; top:0; right:-45px; background:#f1f1f1; display:none; width:35px; z-index:5; padding:3px 5px;}
#BotonesOpciones a span div.infoMas {width:190px; position:absolute; top:40px; left:10px; z-index:6; background:#fff; padding:10px; border:solid 1px #ccc; color:#555; display:none;}
#BotonesOpciones a span img.imgInfo:hover ~ div.infoMas { display:block;}

#CapaOcultoTodo {width:100%; height:100%; background:url(/dmcrm/app/comun2/imagenes/cuadro-trans.png); position:fixed; top:0; left:0; z-index:5; display:none;}
#FormBusqueda {width:400px; position:absolute; top:25%; right:40%; z-index:60; background:#fff; padding:15px 15px 0 15px; border:solid 1px #ccc; -webkit-box-shadow: 0 0 20px 5px #cdcdcd; box-shadow: 0 0 20px 5px #cdcdcd;}
#FormBusqueda ul {display:block; clear:both; list-style:none; margin:0; padding:0; width:100%;}
#FormBusqueda ul:after {display:block; clear:both; content:"";}
#FormBusqueda ul li {float:left; width:50%; margin-bottom:5px;}
#FormBusqueda ul li input {float:left;}
#FormBusqueda label {width:auto; display:inline-block; text-align:right;}
.FormBusqueda_int .NuevaBusqueda { position:absolute; top:0; right:0; text-decoration:none; background:#fff; font-size:16px; padding:5px 10px 0 0;color:#999;}
.FormBusqueda_int .NuevaBusqueda:hover { color:#f60;}

#CapaCargando {width:100%; text-align:center; padding:70px 0; display:none;}
.CapaSelectorInput2 { text-align:center; font-size:11px; color:#555; background:none!important; padding:25px 0 0 0!important}
.CapaSelectorInput2 span:hover { color:#f60; cursor:pointer;}

#CambioAnioCaja {background:#f1f1f1; border-bottom:dotted 2px #cdcdcd; margin:0 0 10px 0;}
#CambioAnioCaja label {font-weight:bold; padding:0 10px 0 0;}
#CambioAnioCaja a.ActualizoFechas {float:right; display:inline-block; background:#cdcdcd; color:#555; text-decoration:none; padding:2px 5px;}
#BotoNuevaBusqueda {margin:0 0 0 130px;}
.CapaSelectorInput2 {padding-top:10px!important;}

/*tablas datos graficos*/
.graficoTablas {width:90%; margin:0 auto;}
.graficoTablas strong.TitTabla {font-size:14px; display:block; padding:0 0 15px 0; color:#555;}
.graficoTablas table {width:100%; font-size:12px;}
.graficoTablas table tr td { border-top:solid 1px #cdcdcd; padding:3px 10px;}
.graficoTablas table tr td.esMes,
.graficoTablas table tr td.mediaCampana,
.graficoTablas table tr td.totalCampana {border:none; font-weight:bold;}
.graficoTablas table tr td.NomCampana {width:100px;}
.graficoTablas table tr td.mediaCampana,
.graficoTablas table tr td.totalCampana {text-align:center;}
.graficoTablas table tr td.MesInpar {background:#f4f4f4; border-left:solid 1px #cdcdcd; border-right:solid 1px #cdcdcd;}
.graficoTablas table tr.mediaPorMeses td {border-top:solid 2px #999; background:#f1f1f1;}
.graficoTablas table tr.totalPorMeses td {border-top:solid 2px #f60; background:#f1f1f1;}
.graficoTablas table tr td.mediaCampana {border-left:solid 3px #999; background:#f1f1f1;}
.graficoTablas table tr td.mediaCampanaTD {border-left:solid 3px #999; background:#f1f1f1;}
.graficoTablas table tr td.totalCampana {border-left:solid 2px #f60; background:#f1f1f1;}
.graficoTablas table tr td.totalCampanaTD {border-left:solid 2px #f60; background:#f1f1f1;}
.graficoTablas table tr td.NoBorder { border:none; background:#fff;}
.graficoTablas table tr td.AbsolutoTD { background:#cccccc;} 

#table_div,
.OcultoTabla { display:none;}
.MuestroTabla { display:block;}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$(".VerTabla").click(function(){
		$('.TablaGeneralDatos').toggle();
	});
	$(".NuevaBusqueda").click(function(){
		$('#CapaOcultoTodo').toggle();
		return false;
	});	
	$('#BotoNuevaBusqueda').click(function(){
		$('#CapaCargando').toggle();
		$('.FiltradoCab').toggle();
	});	
	
	
	$( ".SelectAll").click(function() {
		$(".CamSelInput").attr('checked',true);
	});
	$( ".SelectNone").click(function() {
		$(".CamSelInput").attr('checked',false);
	});
	
	$( ".ActualizoFechas").click(function() {
	    var txtFechaInicio = $('#txtFechaInicio').val();
	    var txtFechaFin = $('#txtFechaFin').val();
	    $(this).attr('href', '?txtFechaInicio=' + txtFechaInicio + '&txtFechaFin=' + txtFechaFin);
		$('#CapaCargando').toggle();
		$('.FiltradoCab').toggle();
	});
	
	var htmlString = $('#DatosTotalesTabla').html();
	$('#table_div2').html( htmlString );
	$('#DatosTotalesTabla').remove();
	
	qTablaMuestro(1);
	 
});
	function qTablaMuestro(DatoGrafico) {
		$('.graficoTablas').addClass('OcultoTabla');
		$('.graficoTablas').removeClass('MuestroTabla');
		$('#grafico' + DatoGrafico).removeClass('OcultoTabla');
		$('#grafico' + DatoGrafico).addClass('MuestroTabla');
	};	
</script> 


<%
'********************************************************************************************************************************************
'********************************************************************************************************************************************
'Estad�sticas anuales
'********************************************************************************************************************************************

'FECHAS
strFechaInicio=request("txtFechaInicio")
strFechaFin=request("txtFechaFin")

'Por defecto marcamos todo el a�o actual
if strFechaInicio="" then strFechaInicio="01/01/" & year(date)
if strFechaFin="" then strFechaFin=date()

strNombreDominio=dominioCRM

'String de colores del gr�fico
strColoresSeries="""#3366cc"",""#dc3912"",""#ff9900"",""#109618"",""#990099"",""#0099c6"",""#dd4477"",""#66aa00"",""#b82e2e"",""#316395"",""#994499"",""#22aa99"",""#8fabe3"",""#b96c70"",""#dc8338"",""#6c8d26"",""#7b853d"",""#615d6a"",""#56a5ff"",""#db25ff"",""#1447ff"",""#ffda0a"",""#ff9b0a"""

%>
<h1>Estad�stica de pedidos por horas</h1>
Fecha inicio: <%=strFechaInicio%><br />
Fecha fin: <%=strFechaFin%><br />

<div style="position:absolute; top:100px; right:0; width:300px;">
Este informe recoge datos de las campa�as de marketing dadas de alta en el servicio DM Integra.
</div>

<%'Pintamos el listado de campa�as activas
sql= " Select locf_fuente,locc_dmmailing_asunto,1 as orden, replace(locf_fuente,'CAM:DM M','Z') as orden2"
sql=sql & "	From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] as P left join LOC_FUENTES ON P.PED_SESION=LOC_FUENTES.LOCF_SESION AND P.PED_IP=LOC_FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT) "
'''sql=sql & "	LEFT Join LOC_CAMPANAS ON 'CAM:' + LOC_CAMPANAS.LOCC_CAMPANA=LOC_FUENTES.LOCF_FUENTE" 
'En la comparaci�n con LOCC_CAMPANA y LOCF_FUENTE, cortamos cuando encontramos el caracter "(" ya que la fecha del mailing a veces no coincide, pero siempre el c�digo
sql=sql & "	LEFT Join LOC_CAMPANAS ON 'CAM:' + left(LOC_CAMPANAS.LOCC_CAMPANA,CHARINDEX('(',LOC_CAMPANAS.LOCC_CAMPANA)) = left(LOC_FUENTES.LOCF_FUENTE,CHARINDEX('(',LOC_FUENTES.LOCF_FUENTE)) " 
sql=sql & "	Where left(locf_fuente,3)='cam'" 
sql=sql & "	and PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
'Condici�n de pedido pagado o pendiente de pago
sql=sql & " and iconEstadoPedido <= 5 "
sql2= "	Group by locf_fuente,locc_dmmailing_asunto "
sql3= "	UNION ALL "
sql3=sql3 & "	SELECT 'BUS' as locf_fuente,NULL,2,NULL "
sql3=sql3 & "	UNION ALL "
sql3=sql3 & "	SELECT 'EXT' as locf_fuente,NULL,3,NULL "
sql3=sql3 & "	UNION ALL "
sql3=sql3 & "	SELECT 'SOC' as locf_fuente,NULL,4,NULL "
sql3=sql3 & "	UNION ALL "
sql3=sql3 & "	SELECT 'NIN' as locf_fuente,NULL,5,NULL "
sql3=sql3 & "	UNION ALL "
sql3=sql3 & "	SELECT 'TOD' as locf_fuente,NULL,6,NULL "
sql3=sql3 & "	Order by Orden, Orden2 "
''response.write sql & sql2 & sql3
Set rstCampanas = connDMI.AbrirRecordset(sql & sql2 & sql3,0,1)
%>

<%'############################################################################################%>
<%
formOculto = ""
if request("ent")<>"" then%>
    <div class="paginacionDatosMes">
        <a href="#" class="NuevaBusqueda">Nueva b�squeda</a>
    </div>
<%
formOculto = "FormBusqueda_int"%>
<div id="CapaOcultoTodo">
<%end if%>

<form name="Filtros" method="POST" id="FormBusqueda" class="<%=formOculto%>" ID="Form1" action="EstadisticoCampanasPedidosHoras.asp">   
	
	<div id="CapaCargando">
		<img src="/DMCrm/APP/Ventas/Pedidos/gastos/cargando.gif" />
	</div>
    
    <%'CAMPA�AS DE MARKETING%>
    <div class="FiltradoCab">
	<div id="CambioAnioCaja">
        <label>Fecha inicio</label>
        <input id="txtFechaInicio" name="txtFechaInicio" type="text" class="cajaTextoFecha"  style="width:70px;" maxlength="10"  value="<%=strFechaInicio%>" />
        <label>Fecha fin</label>
        <input id="txtFechaFin" name="txtFechaFin" type="text" class="cajaTextoFecha" style="width:70px;" maxlength="10"  value="<%=strFechaFin%>"  />
        <a href="" class="ActualizoFechas">Cambiar fechas</a>
    </div>

    <%if request("ent") <> "" then%>
	    <a href="#" class="NuevaBusqueda">X</a>
    <%end if%>
	<%'verifico el a�o para cargar las campa�as%>
	
        <ul>
            <%sqlW=" and (0=1 "
            while not rstCampanas.eof
                strChecked=""
                strBoton=rstCampanas("locf_fuente")
                'Si vengo por primera vez
                if request("ent")<>"si" then
                    strChecked = " checked"
                    sqlW=sqlW & " or locf_fuente='" & rstCampanas("locf_fuente") & "'"
                    Select case lcase(rstCampanas("locf_fuente"))
                        case "ext"
                            strBoton="Enlaces externos"
                        case "bus"
                            strBoton="SEO"
                        case "soc"
                            strBoton="Redes sociales"
                        case "nin"
                            strBoton="Ninguna campa�a"
                        case "tod"
                            strBoton=strNombreDominio
                    end select
                else
                    'Si entro una vez seleccionado las campa�as
                    if left(lcase(rstCampanas("locf_fuente")),3)="cam" then
                        if request.form(""&lcase(rstCampanas("locf_fuente")))="on" then 
                            strChecked = " checked"
                            sqlW=sqlW & " or locf_fuente='" & rstCampanas("locf_fuente") & "'"
                        end if
                    else
                        Select case lcase(rstCampanas("locf_fuente"))
                            case "ext"
                                strBoton="Enlaces externos"
                                if request.form("ext")="on" then
                                    strChecked = " checked"
                                    sqlOtras=sqlOtras & " UNION ALL SELECT 'EXT:',NULL,NULL,NULL "
                                end if
                            case "bus"
                                strBoton="SEO"
                                if request.form("bus")="on" then
                                    strChecked = " checked"
                                    sqlOtras=sqlOtras & " UNION ALL SELECT 'BUS:',NULL,NULL,NULL "
                                end if
                            case "soc"
                                strBoton="Redes sociales"
                                if request.form("soc")="on" then
                                    strChecked = " checked"
                                    sqlOtras=sqlOtras & " UNION ALL SELECT 'SOC:',NULL,NULL,NULL "
                                end if
                            case "nin"
                                strBoton="Ninguna campa�a"
                                if request.form("nin")="on" then
                                    strChecked = " checked"
                                    sqlOtras=sqlOtras & " UNION ALL SELECT 'NIN:',NULL,NULL,NULL "
                                end if
                            case "tod"
                                strBoton=strNombreDominio
                                if request.form("tod")="on" then
                                    strChecked = " checked"
                                    sqlOtras=sqlOtras & " UNION ALL SELECT 'TOD:',NULL,NULL,NULL "
                                end if
                        end select
                    end if

                end if
                
                'Si la campa�a es un Mailing ponemos su asunto
                if not isnull(rstCampanas("locc_dmmailing_asunto")) then
                    strBoton = "DM Mailing: " & rstCampanas("locc_dmmailing_asunto")
                end if%>

                <li><input type="checkbox" class="CamSelInput" name="<%=lcase(rstCampanas("locf_fuente"))%>" <%=strChecked%>><%=strBoton%></li>
                <%rstCampanas.movenext
            wend
            rstCampanas.cerrar
            set rstCampanas=nothing
            sqlW=sqlW & ") "%>
        </ul>
        <%if request("txtFechaInicio") <> "" then%>
            <input type="hidden" value="si" id="ent" name="ent" />
        <%end if%>
		<p>&nbsp;</p>
       
        <input type="SUBMIT" class="boton" id="BotoNuevaBusqueda" value="Calcular estad�stica"/>
        <%'if request("a") <> "" then%>
	        <div class="CapaSelectorInput2">Seleccionar: <span class="SelectAll">Todas</span> | <span class="SelectNone">Ninguna</span></div>
        <%'end if%>
    </div>
</form>

<%if request("ent")<>"" then%>
	</div>
<%end if%>


<%'############################################################################################%>


<%if request("ent")<>"" then%>

    <%'capa en la que cargamos los graficos
    strGrafico1_Titulo = "N�mero de pedidos"
    strGrafico11_Titulo = "N�mero de pedidos (Ponderado)"
    strGrafico2_Titulo = "Pedido medio"
    strGrafico3_Titulo = "Importe bruto de los pedidos"
    strGrafico4_Titulo = "Importe bruto de los pedidos (Ponderado)"
    strGrafico7_Titulo = "N�mero de clientes nuevos (Ponderado)"
    strGrafico8_Titulo = "N�mero de visitas �nicas"
    strGrafico10_Titulo = "Tasa de conversi�n (Ponderada)"
    strGrafico101_Titulo = "Visitas para conseguir una venta (Ponderada)"    %>

    <div id="BotonesOpciones">
        <a id="b1" class="activo"><%=strGrafico1_Titulo%></a>
        <a id="b11"><%=strGrafico11_Titulo%></a>
        <a id="b2"><%=strGrafico2_Titulo%></a>
        <a id="b3"><%=strGrafico3_Titulo%></a>
        <a id="b4"><%=strGrafico4_Titulo%></a>
        <a id="b7"><%=strGrafico7_Titulo%></a>
        <a id="b8"><%=strGrafico8_Titulo%></a>
        <a id="b10"><%=strGrafico10_Titulo%></a>
        <a id="b101"><%=strGrafico101_Titulo%></a>
    </div>
    <div id="GraficosDatosGeneral">
        <div id="chart_div" style="height: 500px;"></div>
        <div id="table_div2"></div>
        <div id='table_div'></div>
    </div>


        <%
        'Hallamos el n�mero de campa�as
        sqlCampanas = sql & sqlW & sql2 & sqlOtras
        Set rstCampanas = connDMI.AbrirRecordset(sqlCampanas,0,1)
        intRecordCount=rstCampanas.recordcount
        redim arrGrafico1(intRecordCount,24)
        redim arrGrafico11(intRecordCount,24)
        redim arrGrafico2(intRecordCount,24)
        redim arrGrafico3(intRecordCount,24)
        redim arrGrafico4(intRecordCount,24)
        redim arrGrafico7(intRecordCount,24)
        redim arrGrafico8(intRecordCount,24)
        redim arrGrafico10(intRecordCount,24)
        redim arrGrafico101(intRecordCount,24)

        contCampana=0
        strBotonesEliminar=""
        strNombreCampana="" 
        intSerieTodas=0
        contSeries=0

        'Datos de las campa�as
        while not rstCampanas.eof

            select case left(rstCampanas("locf_fuente"),3)
                case "CAM"
                    'Si la campa�a es un Mailing ponemos su asunto
                    if not isnull(rstCampanas("locc_dmmailing_asunto")) then
                        strNombreCampana =  "DM Mailing: " & rstCampanas("locc_dmmailing_asunto")
                    else
                        strNombreCampana = rstCampanas("locf_fuente")
                    end if
                    strNombreCampana=replace(strNombreCampana,",","")
                case "BUS"
                    strNombreCampana = "SEO"
                case "EXT"
                    strNombreCampana = "Enlaces externos"
                case "SOC"
                    strNombreCampana = "Redes sociales"
                case "NIN"
                    strNombreCampana = "Ninguna campa�a"
                case "TOD"
                    strNombreCampana = strNombreDominio
                    intSerieTODAS=contSeries
            end select
            strGraficoNombreCampana=strGraficoNombreCampana & "'" & replace(strNombreCampana,"CAM:","") & "',"
            contCampana=contCampana+1

            'Hallamos los detalles de cada campa�a (num pedidos, importe, importe ponderado) agrupados por hora
            sql0=""
            sql1=""
            sql= " Select datepart(hour,ped_fecha) as Hora,convert(varchar,COUNT(PED_CODIGO)) + '_' + convert(varchar,COUNT(PED_CODIGO)*(avg(locf_valor)/100)) + '_' + CONVERT(varchar,SUM(ImporteBruto)) + '_' + convert(varchar,SUM(ImporteBruto*locf_valor/100)) as Datos "
            sql=sql & "	From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS PV left join LOC_FUENTES ON PV.PED_SESION=LOC_FUENTES.LOCF_SESION AND PV.PED_IP=LOC_FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT)  "
            select case left(rstCampanas("locf_fuente"),3)
                case "CAM"
                    sql=sql & "	Where locf_fuente='" & rstCampanas("locf_fuente") & "'"
                case "BUS"
                    sql=sql & "	Where left(locf_fuente,3)='bus'"
                case "EXT"
                    sql=sql & "	Where left(locf_fuente,3)='ext'"
                case "SOC"
                    sql=sql & "	Where left(locf_fuente,3)='soc'"
                case "NIN"
                    sql0 = " SELECT datepart(hour,ped_fecha) as Hora,convert(varchar,COUNT(NumPedidosAux)) + '_' + convert(varchar,COUNT(NumPedidosAux)) + '_' + CONVERT(varchar,SUM(CantidadPedidosAx)) + '_' + convert(varchar,SUM(CantidadPedidosAx)) as Datos "
                    sql0=sql0 & " from ("
					sql0=sql0 & "   Select PED_FECHA,COUNT(PED_CODIGO) as NumPedidosAux, AVG(ImporteBruto) as CantidadPedidosAx "
					sql0=sql0 & "	From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS P left join LOC_FUENTES ON P.PED_SESION=LOC_FUENTES.LOCF_SESION AND P.PED_IP=LOC_FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT ) "
					sql0=sql0 & "	Where PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
                    sql0=sql0 & "	and iconEstadoPedido <= 5 "
                    sql0=sql0 & "	group by PED_CODIGO,PED_FECHA "
                    sql0=sql0 & "	having AVG(locf_valor)=0 or AVG(locf_valor) is null "
                    sql0=sql0 & " ) as A " 
                    sql0=sql0 & " Group by datepart(hour,ped_fecha) "
                    sql0=sql0 & " Order by datepart(hour,ped_fecha) "
                case "TOD"
                    sql1 = " Select datepart(hour,ped_fecha) as Hora,convert(varchar,COUNT(PED_CODIGO)) + '_' + convert(varchar,COUNT(PED_CODIGO)) + '_' + CONVERT(varchar,SUM(ImporteBruto)) + '_' + convert(varchar,SUM(ImporteBruto)) as Datos "
                    sql1=sql1 & " From " & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS P  "
					sql1=sql1 & " Where PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
                    sql1=sql1 & " and iconEstadoPedido <= 5 " 
                    sql1=sql1 & " Group by datepart(hour,ped_fecha) "
                    sql1=sql1 & " Order by datepart(hour,ped_fecha) "                        
            end select
            sql=sql & "	and PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
            'Condici�n de pedido pagado o pendiente de pago
            sql=sql & " and iconEstadoPedido <= 5 "
            sql=sql & " Group by datepart(hour,ped_fecha) "
            sql=sql & " Order by datepart(hour,ped_fecha) "
			'Comprobamos si debemos ejectura la sql de NINGUNA CAMPA�A o la otra
			if sql0<>"" then
                Set rstDatos = connDMI.AbrirRecordset(sql0,0,1)
			else
                if sql1<>"" then
                    Set rstDatos = connDMI.AbrirRecordset(sql1,0,1)
                else
                    Set rstDatos = connDMI.AbrirRecordset(sql,0,1)
                end if
			end if
 'response.write SQL & "<BR>" & sql0 & "<br>" & sql1 & "<br><br>"

            'Hallamos el n�mero de visitas �nicas al sitio web cada mes
            sql0=""
            sql1=""
            sql= " Select datepart(hour,LOCF_FECHA) as Hora, count(distinct(locf_sesion)) as NumVisitas"
            sql=sql & "	From LOC_FUENTES  "
            select case left(rstCampanas("locf_fuente"),3)
                case "CAM"
                    sql=sql & "	Where LOCF_FUENTE='" & rstCampanas("locf_fuente") & "'" 
                case "BUS"
                    sql=sql & "	Where left(LOCF_FUENTE,3)='BUS'"
                case "EXT"
                    sql=sql & "	Where left(LOCF_FUENTE,3)='ext'"
                case "SOC"
                    sql=sql & "	Where left(LOCF_FUENTE,3)='soc'"
                case "NIN"
                    sql0= " Select datepart(hour,Hora) as Hora, count(distinct(sesion)) as NumVisitas "
                    sql0=sql0 & " from ( "
	                sql0=sql0 & "     Select min(CONVERT(varchar(10),locl_fecha,112) + ' ' + CONVERT(varchar(8),locl_hora,108)) as Hora, locl_sesion as sesion "
	                sql0=sql0 & "     from loc_logs "
	                sql0=sql0 & "     where locl_fecha>='" & FormatearFechaSQLSever(strFechaInicio) & "' and locl_fecha<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' " 
	                sql0=sql0 & "     group by locl_sesion "
	                sql0=sql0 & "     having max(locl_fuente_hit)='') as A "
                    sql0=sql0 & " group by datepart(hour,Hora) "
                    sql0=sql0 & " order by datepart(hour,Hora)"
                case "TOD"
                    sql1= " Select datepart(hour,locl_hora) as Hora, count(distinct(locl_sesion)) as NumVisitas "
	                sql1=sql1 & " from loc_logs "
	                sql1=sql1 & " where locl_fecha>='" & FormatearFechaSQLSever(strFechaInicio) & "' and locl_fecha<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' " 
                    sql1=sql1 & " and (LOCL_TIPOSESION='H' OR LOCL_TIPOSESION='' OR LOCL_TIPOSESION is null) "
                    sql1=sql1 & " group by datepart(hour,locl_hora) "
                    sql1=sql1 & " order by datepart(hour,locl_hora)"
            end select
            sql=sql & "	and LOCF_FUENTE_DIRECTA=1 "
            sql=sql & "	and LOCF_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and LOCF_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
            sql=sql & "	group by datepart(hour,LOCF_FECHA)"
            sql=sql & "	order by datepart(hour,LOCF_FECHA)"
			if sql0<>"" then
                Set rstVisitasMes = connDMI.AbrirRecordset(sql0,3,1)
			else
                if sql1<>"" then
                    Set rstVisitasMes = connDMI.AbrirRecordset(sql1,3,1)
                else
                    Set rstVisitasMes = connDMI.AbrirRecordset(sql,3,1)
                end if
			end if
 'response.write  SQL & "<BR>" & sql0 & "<br>" & sql1 & "<br><br>"
     			
            'Hallamos el n�mero de clientes nuevos generados desde esta campa�a (ponderados)
            sql0=""
            sql1=""
            sql= " Select datepart(hour,ped_fecha) as Hora, count(distinct(PED_SUCONTACTO))*(avg(locf_valor)/100) AS NuevosClientes"
            sql=sql & "	From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS PV left join LOC_FUENTES ON PV.PED_SESION=LOC_FUENTES.LOCF_SESION AND PV.PED_IP=LOC_FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT)  "
            select case left(rstCampanas("locf_fuente"),3)
                case "CAM"
                    sql=sql & "	Where locf_fuente='" & rstCampanas("locf_fuente") & "'"
                case "BUS"
                    sql=sql & "	Where left(locf_fuente,3)='bus'"
                case "EXT"
                    sql=sql & "	Where left(locf_fuente,3)='ext'"
                case "SOC"
                    sql=sql & "	Where left(locf_fuente,3)='soc'"
                case "NIN"
                    sql0 = " SELECT datepart(hour,ped_fecha) as Hora,count(distinct(CLIENTE)) AS NuevosClientes "
                    sql0=sql0 & " from ( "
	                sql0=sql0 & "     Select PED_FECHA,PED_SUCONTACTO AS CLIENTE "
	                sql0=sql0 & "     From (" & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] AS P left join LOC_FUENTES ON P.PED_SESION=LOC_FUENTES.LOCF_SESION AND P.PED_IP=LOC_FUENTES.LOCF_IP COLLATE DATABASE_DEFAULT ) "
	                sql0=sql0 & "     Where PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
		            sql0=sql0 & "           and iconEstadoPedido <= 5 " 
	                sql0=sql0 & "     group by PED_CODIGO,PED_FECHA,PED_SUCONTACTO "
	                sql0=sql0 & "     having AVG(locf_valor)=0 or AVG(locf_valor) is null " 
                    sql0=sql0 & " ) as A "
                    sql0=sql0 & " Group by datepart(hour,ped_fecha) "
                    sql0=sql0 & " Order by datepart(hour,ped_fecha) "
                case "TOD"
                    sql1 = " SELECT datepart(hour,ped_fecha) as Hora, count(distinct(PED_SUCONTACTO)) AS NuevosClientes"
                    sql1=sql1 & " From " & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] as PV"
                    sql1=sql1 & " Where "
                    sql1=sql1 & "	PED_SUCONTACTO NOT IN (SELECT PED_SUCONTACTO FROM " & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] as PV2 "
                    sql1=sql1 & "								WHERE PV2.PED_CODIGO<PV.PED_CODIGO "
                                                            'Condici�n de pedido pagado o pendiente de pago
                    sql1=sql1 & "                             and iconEstadoPedido <= 5 ) "
                    sql1=sql1 & "	and PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
                    sql1=sql1 & "   and iconEstadoPedido <= 5 "
                    sql1=sql1 & "	group by datepart(hour,ped_fecha)"
                    sql1=sql1 & "	order by datepart(hour,ped_fecha)"
            end select
            sql=sql & "		and PED_SUCONTACTO NOT IN (SELECT PED_SUCONTACTO FROM " & strNombreBDCRM & ".[dbo].[PEDIDOS_VENTAS] as PV2 "
            sql=sql & "								WHERE PV2.PED_CODIGO<PV.PED_CODIGO "
                                                    'Condici�n de pedido pagado o pendiente de pago
            sql=sql & "                             and iconEstadoPedido <= 5 ) "
            sql=sql & "	and PED_FECHA>='" & FormatearFechaSQLSever(strFechaInicio) & "' and PED_FECHA<='" & FormatearFechaSQLSever(strFechaFin) & " 23:59:59' "
            'Condici�n de pedido pagado o pendiente de pago
            sql=sql & " and iconEstadoPedido <= 5 "
            sql=sql & "	group by datepart(hour,ped_fecha)"
            sql=sql & "	order by datepart(hour,ped_fecha)"
			if sql0<>"" then
                Set rstNClientesMes = connDMI.AbrirRecordset(sql0,3,1)
			else
			    if sql1<>"" then
                    Set rstNClientesMes = connDMI.AbrirRecordset(sql1,3,1)
                else
                    Set rstNClientesMes = connDMI.AbrirRecordset(sql,3,1)
                end if
			end if
 'response.write sql & "<br>" & sql0 & "<br>" & sql1 & "<br><br>"

            'Recorremos todas las horas (00 a 23)
            for intI=0 to 23           
        
                'Hallamos los datos ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                blnFin=false
                strDatos=""
                'Recorremos el cursor hasta posicionarnos exactamente en la hora o hasta que la hora del cursor es mayor
                'Simulamos la instrucci�n � rstDatos.filter = "Hora='" & intI & "'" �
                while not blnFin
                    if not rstDatos.eof then 
                        if rstDatos("Hora") = intI then 
                            strDatos=rstDatos("Datos")
                            blnFin=true
                            rstDatos.movenext
                        else
                            if rstDatos("Hora") > intI then
                                blnFin=true
                            end if
                        end if
                    else
                        blnFin=true
                    end if
                wend
                
                if strDatos<>"" then
                    arrDatos=split(strDatos,"_")
                    intNumPedidos=cint(arrDatos(0))
                    intNumPedidosPonderados=cdbl(replace(arrDatos(1),".",","))
                    intImporte  = cdbl(replace(arrDatos(2),".",","))
                    intPedidoMedio = intImporte/intNumPedidos
                    intImportePonderado=cdbl(replace(arrDatos(3),".",","))
                else
                    intNumPedidos=0
                    intNumPedidosPonderados=0
                    intPedidoMedio=null
                    intImporte  = 0
                    intImportePonderado=null
                end if
    
                'Hallamos las visitas ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                blnFin=false
                intVisitasMes=null
                'Recorremos el cursor hasta posicionarnos exactamente en la hora o hasta que la hora del cursor es mayor
                'Simulamos la instrucci�n � rstDatos.filter = "Hora='" & intI & "'" �
                while not blnFin
                    if not rstVisitasMes.eof then 
                        if rstVisitasMes("Hora") = intI then 
                            intVisitasMes=rstVisitasMes("NumVisitas")
                            blnFin=true
                            rstVisitasMes.movenext
                        else
                            if rstVisitasMes("Hora") > intI then
                                blnFin=true
                            end if
                        end if
                    else
                        blnFin=true
                    end if
                wend
    
                'Hallamos los nuevos clientes (ponderados) ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                blnFin=false
                intNuevosClientes=null
                'Recorremos el cursor hasta posicionarnos exactamente en la hora o hasta que la hora del cursor es mayor
                'Simulamos la instrucci�n � rstDatos.filter = "Hora='" & intI & "'" �
                while not blnFin
                    if not rstNClientesMes.eof then 
                        if rstNClientesMes("Hora") = intI then 
                            intNuevosClientes=round(rstNClientesMes("NuevosClientes"),2)
                            blnFin=true
                            rstNClientesMes.movenext
                        else
                            if rstNClientesMes("Hora") > intI then
                                blnFin=true
                            end if
                        end if
                    else
                        blnFin=true
                    end if
                wend

                'Hallamos la tasa de conversi�n (Visitas / Num Pedidos ponderados) ++++++++++++++++++++++++++++++++++++
                intTasaConversion=null
                if intVisitasMes > 0 then 
                    if intNumPedidos>0 then
                        intTasaConversion = round(intNumPedidosPonderados*100 / intVisitasMes,2)
                    else
                        intTasaConversion = 0
                    end if
                else
                    intTasaConversion = null
                end if

                'Hallamos el num de visitas para conseguir 1 pedido ++++++++++++++++++++++++++++++++++++
                intNumVisitasParaPedido=null
                if intNumPedidosPonderados > 0 then 
                    if intNumPedidos>0 and intVisitasMes>0 then
                        intNumVisitasParaPedido = round(intVisitasMes / intNumPedidosPonderados,2)
                    else
                        intNumVisitasParaPedido = 0
                    end if
                else
                    intNumVisitasParaPedido = null
                end if    

                'Graficos. Si es nulo ponemos un valor similar a 0, pero lo trataremos en las tablas de cada m�trica
                if isnull(intNumPedidos) then arrGrafico1(contCampana,intI)="0.00001" else arrGrafico1(contCampana,intI)   = intNumPedidos
                if isnull(intNumPedidosPonderados) then arrGrafico11(contCampana,intI)="0.00001" else arrGrafico11(contCampana,intI)   =  replace(round(intNumPedidosPonderados,2),",",".")
                if isnull(intPedidoMedio) then arrGrafico2(contCampana,intI)="0.00001" else arrGrafico2(contCampana,intI)  = replace(round(intPedidoMedio,2),",",".")
                if isnull(intImporte) then arrGrafico3(contCampana,intI)="0.00001" else arrGrafico3(contCampana,intI)      = replace(round(intImporte,2),",",".") 
                if isnull(intImportePonderado) then arrGrafico4(contCampana,intI)="0.00001" else arrGrafico4(contCampana,intI) = replace(round(intImportePonderado,2),",",".") 
                if isnull(intNuevosClientes) then arrGrafico7(contCampana,intI)="0.00001" else arrGrafico7(contCampana,intI)   = replace(round(intNuevosClientes,2),",",".") 
                if isnull(intVisitasMes) then arrGrafico8(contCampana,intI)="0.00001" else arrGrafico8(contCampana,intI)       = intVisitasMes
                if isnull(intTasaConversion) then arrGrafico10(contCampana,intI)="0.00001" else arrGrafico10(contCampana,intI) = replace(intTasaConversion,",",".") 
                if isnull(intNumVisitasParaPedido) then arrGrafico101(contCampana,intI)="0.00001" else arrGrafico101(contCampana,intI) = replace(intNumVisitasParaPedido,",",".") 

            next
    
            rstDatos.cerrar
            rstVisitasMes.cerrar
            rstNClientesMes.cerrar
            rstCampanas.movenext
            
            contSeries=contSeries+1
        wend
        strGraficoNombreCampana=left(strGraficoNombreCampana,len(strGraficoNombreCampana)-1)
        rstCampanas.cerrar

    ' Gr�fico 1: NUM PEDIDOS
    ' Gr�fico 1: NUM PEDIDOS (ponderados)
    ' Gr�fico 2: Pedido medio
    ' Gr�fico 3: Importe
    ' Gr�fico 4: Importe ponderado
    ' Gr�fico 7: Clientes nuevos
    ' Gr�fico 8: Visitas �nicas
    ' Gr�fico 10: Tasa de conversi�n
    ' Gr�fico 101: Visitas para conseguir una venta

    strGrafico1 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico11 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico2 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico3 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico4 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico7 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico8 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico10 = "['Hora'," & strGraficoNombreCampana & "],"
    strGrafico101 = "['Hora'," & strGraficoNombreCampana & "],"
  
    for intI=0 to 23
        strGrafico1=strGrafico1 & "['" & intI & "',"
        strGrafico11=strGrafico11 & "['" & intI & "',"
        strGrafico2=strGrafico2 & "['" & intI & "',"
        strGrafico3=strGrafico3 & "['" & intI & "',"
        strGrafico4=strGrafico4 & "['" & intI & "',"
        strGrafico7=strGrafico7 & "['" & intI & "',"
        strGrafico8=strGrafico8 & "['" & intI & "',"
        strGrafico10=strGrafico10 & "['" & intI & "',"
        strGrafico101=strGrafico101 & "['" & intI & "',"
        for intX=1 to contCampana
            strGrafico1=strGrafico1 & arrGrafico1(intX,intI) & "," 
            strGrafico11=strGrafico11 & arrGrafico11(intX,intI) & "," 
            strGrafico2=strGrafico2 & arrGrafico2(intX,intI) & "," 
            strGrafico3=strGrafico3 & arrGrafico3(intX,intI) & "," 
            strGrafico4=strGrafico4 & arrGrafico4(intX,intI) & "," 
            strGrafico7=strGrafico7 & arrGrafico7(intX,intI) & "," 
            strGrafico8=strGrafico8 & arrGrafico8(intX,intI) & ","
            strGrafico10=strGrafico10 & arrGrafico10(intX,intI) & ","    
            strGrafico101=strGrafico101 & arrGrafico101(intX,intI) & ","                     
        next
        strGrafico1=left(strGrafico1,len(strGrafico1)-1)
        strGrafico11=left(strGrafico11,len(strGrafico11)-1)
        strGrafico2=left(strGrafico2,len(strGrafico2)-1)
        strGrafico3=left(strGrafico3,len(strGrafico3)-1)
        strGrafico4=left(strGrafico4,len(strGrafico4)-1)
        strGrafico7=left(strGrafico7,len(strGrafico7)-1)
        strGrafico8=left(strGrafico8,len(strGrafico8)-1)
        strGrafico10=left(strGrafico10,len(strGrafico10)-1)
        strGrafico101=left(strGrafico101,len(strGrafico101)-1)

        strGrafico1=strGrafico1 & "],"
        strGrafico11=strGrafico11 & "],"
        strGrafico2=strGrafico2 & "],"
        strGrafico3=strGrafico3 & "],"
        strGrafico4=strGrafico4 & "],"
        strGrafico7=strGrafico7 & "],"
        strGrafico8=strGrafico8 & "],"
        strGrafico10=strGrafico10 & "],"
        strGrafico101=strGrafico101 & "],"
    next

    strGrafico1=left(strGrafico1,len(strGrafico1)-1)
    strGrafico11=left(strGrafico11,len(strGrafico11)-1)
    strGrafico2=left(strGrafico2,len(strGrafico2)-1)
    strGrafico3=left(strGrafico3,len(strGrafico3)-1)
    strGrafico4=left(strGrafico4,len(strGrafico4)-1)
    strGrafico7=left(strGrafico7,len(strGrafico7)-1)
    strGrafico8=left(strGrafico8,len(strGrafico8)-1)
    strGrafico10=left(strGrafico10,len(strGrafico10)-1)
    strGrafico101=left(strGrafico101,len(strGrafico101)-1)

    strGrafico1_vAxis = "N�mero"
    strGrafico11_vAxis = "N�mero"
    strGrafico2_vAxis = "Euros"
    strGrafico3_vAxis = "Euros"
    strGrafico4_vAxis = "Euros"
    strGrafico7_vAxis = "Clientes"
    strGrafico8_vAxis = "Visitas �nicas"
    strGrafico10_vAxis = "Porcentaje"
    strGrafico10_vAxis = "Visitas �nicas"

    %>
    <script id="jqueryui" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js" defer async></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script type="text/javascript">
        google.load('visualization', '1.1', { packages: ['corechart','table'] });

        google.setOnLoadCallback(drawVisualization);
		var dataBakRelleno=false;
		var dataBak = []
        function drawVisualization() {
            var dataTitulo = []
            dataTitulo[0] = '<%=strGrafico1_Titulo%>';
            dataTitulo[10] = '<%=strGrafico11_Titulo%>';
            dataTitulo[1] = '<%=strGrafico2_Titulo%>';
            dataTitulo[2] = '<%=strGrafico3_Titulo%>';
            dataTitulo[3] = '<%=strGrafico4_Titulo%>';
            dataTitulo[6] = '<%=strGrafico7_Titulo%>';
            dataTitulo[7] = '<%=strGrafico8_Titulo%>';
            dataTitulo[9] = '<%=strGrafico10_Titulo%>';
            dataTitulo[101] = '<%=strGrafico101_Titulo%>';

            var datavAxis = []
            datavAxis[0] = '<%=strGrafico1_vAxis%>';
            datavAxis[10] = '<%=strGrafico11_vAxis%>';
            datavAxis[1] = '<%=strGrafico2_vAxis%>';
            datavAxis[2] = '<%=strGrafico3_vAxis%>';
            datavAxis[3] = '<%=strGrafico4_vAxis%>';
            datavAxis[6] = '<%=strGrafico7_vAxis%>';
            datavAxis[7] = '<%=strGrafico8_vAxis%>';
            datavAxis[9] = '<%=strGrafico10_vAxis%>';
            datavAxis[101] = '<%=strGrafico101_vAxis%>';
            // Some raw data (not necessarily accurate)

            var data = []
            data[0] = google.visualization.arrayToDataTable([<%=strGrafico1%>]);
            data[10] = google.visualization.arrayToDataTable([<%=strGrafico11%>]);
            data[1] = google.visualization.arrayToDataTable([<%=strGrafico2%>]);
            data[2] = google.visualization.arrayToDataTable([<%=strGrafico3%>]);
            data[3] = google.visualization.arrayToDataTable([<%=strGrafico4%>]);
            data[6] = google.visualization.arrayToDataTable([<%=strGrafico7%>]);
            data[7] = google.visualization.arrayToDataTable([<%=strGrafico8%>]);
            data[9] = google.visualization.arrayToDataTable([<%=strGrafico10%>]);
            data[101] = google.visualization.arrayToDataTable([<%=strGrafico101%>]);

            var current = 0;

            // Create and draw the visualization.
            var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
            var button1 = document.getElementById('b1');
            var button11 = document.getElementById('b11');
            var button2 = document.getElementById('b2');
            var button3 = document.getElementById('b3');
            var button4 = document.getElementById('b4');
            var button7 = document.getElementById('b7');
            var button8 = document.getElementById('b8');
            var button10 = document.getElementById('b10');
            var button101 = document.getElementById('b101');
			//var CapaDatosTabla = document.getElementsByClassName('graficoTablas');
			
			var capaGeneralA = document.getElementById('BotonesOpciones');

            function drawChart() {

                var options = {
                    title: dataTitulo[current],
                    vAxis: { title: datavAxis[current] },
                    hAxis: { title: "Hora" },
                    seriesType: "line",
                    colors: [<%=strColoresSeries%>],
                    //seriesType: "bars",
                    <%'Si existe la serie TODAS la pintamos con barras
                    if intSerieTODAS>0 then %>
                        series: { <%=intSerieTODAS%>: { type: "steppedArea", color: '#cdcdcd', } },
                    <%end if%>
                    animation: {
                        duration: 1000,
                        easing: 'out'
                    },
                };

                chart.draw(data[current], options);
                
            }
            drawChart();

			function QuitoClases() {
				button1.className = "";
                button11.className = "";
				button2.className = "";
				button3.className = "";
				button4.className = "";
				button7.className = "";
				button8.className = "";
				button10.className = "";
                button101.className = "";
            }

            button1.onclick = function() {
                current = 0;
				QuitoClases();
				qTablaMuestro(current+1);
				button1.className = button1.className + " activo";
                drawChart();
            }
            button11.onclick = function() {
                current = 10;
				QuitoClases();
				qTablaMuestro(current+1);
				button11.className = button11.className + " activo";
                drawChart();
            }
            button2.onclick = function() {
                current = 1
				QuitoClases();
				qTablaMuestro(current+1);
				button2.className = button1.className + " activo";
                drawChart();
            }
            button3.onclick = function() {
                current = 2
				QuitoClases();
				qTablaMuestro(current+1);
				button3.className = button1.className + " activo";
                drawChart();
            }
            button4.onclick = function() {
                current = 3
				QuitoClases();
				qTablaMuestro(current+1);
				button4.className = button1.className + " activo";
                drawChart();
            }
            button7.onclick = function() {
                current = 6
				QuitoClases();
				qTablaMuestro(current+1);
				button7.className = button1.className + " activo";
                drawChart();
            }
            button8.onclick = function() {
                current = 7
				QuitoClases();
				qTablaMuestro(current+1);
				button8.className = button1.className + " activo";
                drawChart();
            }
            button10.onclick = function() {
                current = 9
				QuitoClases();
				qTablaMuestro(current+1);
				button10.className = button10.className + " activo";
                drawChart();
            }
            button101.onclick = function() {
                current = 101
				QuitoClases();
				qTablaMuestro(current);
				button101.className = button101.className + " activo";
                drawChart();
            }
        }
    </script>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
	
    <div id="DatosTotalesTabla">
    <%'Pintamos las tablas de cada gr�fico
    'N�mero de meses pasado de este a�o.  Si el a�o es el actual s�lo contamos hasta el mes actual
    NumHoras=24
    arrColores=split(replace(strColoresSeries,"""",""),",")

    'Creamos un bucle para pintar las tablas de todos los gr�ficos
    for intGrafico=1 to 9

        'Array para llevar el total en cada hora
        ReDim arrTotalMes(24)

        intTotalGlobal=0
        intMediaGlobal=0

        select case intGrafico
            case 1
                intNumGrafico=1
                blnPintarTotal=true
                intNumDecimales=0 'Decimales para los datos y para los totales (la media siempre tendr� 2)
                strFinal=""
            case 2
                intNumGrafico=11
                blnPintarTotal=true
                intNumDecimales=2
                strFinal=""
            case 3
                intNumGrafico=2
                blnPintarTotal=false
                intNumDecimales=2
                strFinal=" �"
            case 4
                intNumGrafico=3
                blnPintarTotal=true
                intNumDecimales=2
                strFinal=" �"
            case 5
                intNumGrafico=4
                blnPintarTotal=true
                intNumDecimales=2
                strFinal=" �"
            case 6
                intNumGrafico=7
                blnPintarTotal=true
                intNumDecimales=2
                strFinal=""
            case 7
                intNumGrafico=8
                blnPintarTotal=true
                intNumDecimales=0
                strFinal=""
            case 8
                intNumGrafico=10
                blnPintarTotal=false
                intNumDecimales=2
                strFinal=" %"
            case 9
                intNumGrafico=101
                blnPintarTotal=false
                intNumDecimales=2
                strFinal=""
        end select
        'Seleccionamos los datos del gr�fico
        strGrafico=eval("strGrafico" & intNumGrafico)
		
		g1="<div class=""graficoTablas"" id=""grafico" & intNumGrafico & """>"
        g1=g1 & "<strong class=""TitTabla"">" & eval("strGrafico" & intNumGrafico & "_Titulo") & "</strong>"
        g1=g1 & "<table class=""graficoTabla" & intNumGrafico & """ border=""0"" cellspacing=""0"">"
        arrFilas=split(strGrafico,",[")
        arrCampanas=split(arrFilas(0),",")
        contCampanas=ubound(arrCampanas)
        for intCampana=0 to ubound(arrCampanas)
            intTotalAnualCampana=0
            'Si la campa�a es TODAS (EL SITIO WEB) pongo una clase para separarla en la tabla
            if instr(arrCampanas(intCampana),strNombreDominio)>0  then
                g1=g1 & "<tr class=""totalPorMeses"">"
                blnCampanaTodas=true
            else
                g1=g1 & "<tr>"
                blnCampanaTodas=false
            end if
            for intHora=0 to 24
                arrDato=split(arrFilas(intHora),",")
                strDato=arrDato(intCampana)
                'El primero es el nombre de la campa�a. Viene con comillas
                if intHora=0 or intCampana=0 then
                    strDato=replace(strDato,"'","")
                    strDato=replace(strDato,"]","")
                    strDato=replace(strDato,"[","")
                    if intHora=0 and intCampana=0 then strDato=""
					'pintamos cada columna de un color diferente
					if intHora mod 2 = 1 Then
						strColor= "class=""MesInpar esMes"""
					else
						strColor= "class=""MesPar esMes"""
					end If
                    if intCampana>0 then 
                        'Si estoy en la campa�a TODAS pinto el color propio
                        if blnCampanaTodas then
                            strColor= "class=""NomCampana"" style=""border-left:solid 10px #cdcdcd;"""
                        else
                            strColor= "class=""NomCampana"" style=""border-left:solid 10px " & arrColores(intCampana-1) & ";"""
                        end if
                    end if
                    if intHora>0 then
						strAlign=" align=""center"""
					else
						strAlign=""
					end if
					
                else
                    strDato=replace(strDato,".",",")
                    strDato=replace(strDato,"]","")
                    'Comprobamos si viene 0.0 para poner nulo
                    if strDato="0,00001" then
                        strDato="-"
                    else
                        strDato=cdbl(strDato)
                        intTotalAnualCampana=intTotalAnualCampana + strDato
                        arrTotalMes(intHora)=arrTotalMes(intHora) + strDato
                        strDato=FormatNumber(strDato,intNumDecimales) & strFinal 
                    end if
                    strColor=""
					'pintamos cada columna de un color diferente
					if intHora mod 2 = 1 Then
						strColor= "class=""MesInpar"""
					else
						strColor= "class=""MesPar"""
					end If
                    strAlign=" align=right"
                end if
                g1=g1 & "<td " & strColor & strAlign & ">" & strDato & "</td>"
            next
            if intCampana=0 then
                'Media mensual
                g1=g1 & "<td class=""mediaCampana"">Media</td>"
                'Total anual campa�a
                if blnPintarTotal then
                    g1=g1 & "<td class=""totalCampana"">Total</td>"
                end if
            else
                'Media mensual
                g1=g1 & "<td align=right class=""mediaCampanaTD"">" & FormatNumber(intTotalAnualCampana/NumHoras,intNumDecimales) & strFinal & "</td>"
                'Total anual campa�a
                if blnPintarTotal then
                    g1=g1 & "<td align=right class=""totalCampanaTD"">" & FormatNumber(intTotalAnualCampana,intNumDecimales) & strFinal & "</td>"
                end if
            end if
            g1=g1 & "</tr>"
        next

        g1=g1 & "</table></div>"
        Response.write g1 '& "<br><br>"
    
    next%>
	</div>
<%end if%>
<%'********************************************************************************************************************************************
'********************************************************************************************************************************************
%>


<!-- #include virtual="/dmcrm/includes/finPantalla.asp"-->





