<%

	
		''Comprabamos el tipo de recorrido que estamos haciendo.	
		if not sacarComparadores then
		
			cadaNuevoCliente=0
			cadaSqlNuevosClientes=sqlNuevosClientes
			Select case modoVista
				case "1ANOS"
					cadaSqlNuevosClientes=cadaSqlNuevosClientes & " and P.codPropietario not in (select P2.codPropietario from PEDIDOS_VENTAS P2 where year(P2.PED_fecha)<" & (AnoInicial + contMes -1) & " and P2.PED_SUCLIENTEEMPRESA= P.PED_SUCLIENTEEMPRESA) "
					 cadaSqlNuevosClientes=cadaSqlNuevosClientes & " And YEAR(P.PED_FECHA)=" & AnoInicial + contMes -1				
				case "2ANO"
					cadaSqlNuevosClientes=cadaSqlNuevosClientes & " and P.codPropietario not in (select P2.codPropietario from PEDIDOS_VENTAS P2 where year(P2.PED_fecha)<" & anno & "  and P2.PED_SUCLIENTEEMPRESA= P.PED_SUCLIENTEEMPRESA) "			
					cadaSqlNuevosClientes=cadaSqlNuevosClientes & " And year(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & contMes
				case "3MES"
					cadaSqlNuevosClientes=cadaSqlNuevosClientes & " and P.codPropietario not in (select P2.codPropietario from PEDIDOS_VENTAS P2 where year(P2.PED_fecha)<" & anno & "  and P2.PED_SUCLIENTEEMPRESA= P.PED_SUCLIENTEEMPRESA) "			
					cadaSqlNuevosClientes=cadaSqlNuevosClientes & " And year(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes & " And day(P.PED_FECHA)=" & contMes		
			end select
				Set rsNuevosClientes = connCRM.AbrirRecordSet(cadaSqlNuevosClientes,3,1) 	
				if not rsNuevosClientes.eof then
					if not isnull(rsNuevosClientes("contClientes")) And trim(rsNuevosClientes("contClientes"))<>"0" then
						cadaNuevoCliente=rsNuevosClientes("contClientes")
						tieneNuevosClientes=true
					else
						cadaNuevoCliente=0
					end if
				end if
				rsNuevosClientes.cerrar()
				set rsNuevosClientes=nothing	
				
				redim preserve arrNuevosClientes(contMes)
				arrNuevosClientes(contMes)=cadaNuevoCliente
				numNuevosClientes=numNuevosClientes + cadaNuevoCliente
		
		elseif tieneNuevosClientes then 'else de if not sacarComparadores then

			cadaNuevoCliente=0
			cadaSqlNuevosClientes=sqlNuevosClientes
			cadaSqlNuevosClientes=cadaSqlNuevosClientes & " and P.codPropietario not in (select P2.codPropietario from PEDIDOS_VENTAS P2 where year(P2.PED_fecha)<" & cadaAnno & "  and P2.PED_SUCLIENTEEMPRESA= P.PED_SUCLIENTEEMPRESA) "						
			cadaSqlNuevosClientes=cadaSqlNuevosClientes & " And year(P.PED_FECHA)=" & cadaAnno & " And month(P.PED_FECHA)=" & contMes

			Set rsNuevosClientes = connCRM.AbrirRecordSet(cadaSqlNuevosClientes,3,1) 	
			if not rsNuevosClientes.eof then
				if not isnull(rsNuevosClientes("contClientes")) And trim(rsNuevosClientes("contClientes"))<>"0" then
					cadaNuevoCliente=rsNuevosClientes("contClientes")
					tieneNuevosClientes=true					
				else
					cadaNuevoCliente=0
				end if
			end if
			rsNuevosClientes.cerrar()
			set rsNuevosClientes=nothing	
			
			arrNumNuevosClientes(contComp)=arrNumNuevosClientes(contComp) + cadaNuevoCliente
			arrNuevosClientesComp(contMes,contComp)=cadaNuevoCliente
			
		end if 'end de if not sacarComparadores then	
	

		''Comprabamos el tipo de recorrido que estamos haciendo.	
		if not sacarComparadores then		
			
				''Contador pedidos
				cadaNumPedidos=0			
				cadaSqlNumPedidos=sqlNumPedidos
				Select case modoVista
					case "1ANOS"
						cadaSqlNumPedidos=cadaSqlNumPedidos & " and YEAR(P.PED_FECHA)=" & AnoInicial + contMes -1
					case "2ANO"
						cadaSqlNumPedidos=cadaSqlNumPedidos & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & contMes
					case "3MES"
						cadaSqlNumPedidos=cadaSqlNumPedidos & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes & " And day(P.PED_FECHA)=" & contMes
				end select	          			

				Set rsNumPedidos = connCRM.AbrirRecordSet(cadaSqlNumPedidos,3,1) 	
				if not rsNumPedidos.eof then
					if not isnull(rsNumPedidos("contPedidos")) And trim(rsNumPedidos("contPedidos"))<>"0" then
						cadaNumPedidos=rsNumPedidos("contPedidos")
						tienePedidos=true						
					else
						cadaNumPedidos=0
					end if						
				end if
				rsNumPedidos.cerrar()
				set rsNumPedidos=nothing
				
				redim preserve arrPedidos(contMes)
				arrPedidos(contMes)=cadaNumPedidos
				numPedidos=numPedidos + cadaNumPedidos

		elseif tienePedidos then 'else de if not sacarComparadores then			
			
				cadaNumPedidos=0			
				cadaSqlNumPedidos=sqlNumPedidos
				cadaSqlNumPedidos=cadaSqlNumPedidos & " and YEAR(P.PED_FECHA)=" & cadaAnno & " And month(P.PED_FECHA)=" & contMes
				Set rsNumPedidos = connCRM.AbrirRecordSet(cadaSqlNumPedidos,3,1) 	
				if not rsNumPedidos.eof then
					if not isnull(rsNumPedidos("contPedidos")) And trim(rsNumPedidos("contPedidos"))<>"0" then
						cadaNumPedidos=rsNumPedidos("contPedidos")
						tienePedidos=true												
					else
						cadaNumPedidos=0
					end if						
				end if
				rsNumPedidos.cerrar()
				set rsNumPedidos=nothing
				
				arrNumPedidos(contComp)=arrNumPedidos(contComp) + cadaNumPedidos				
				arrPedidosComp(contMes,contComp)=cadaNumPedidos
			
		end if 'end de if not sacarComparadores then	
		
		
			
		''Comprabamos el tipo de recorrido que estamos haciendo.	
		if not sacarComparadores then			
				''Suma ingresos
				cadaCantPedido=0			
				cadaSqlCantPedidos=sqlCantPedidos
				Select case modoVista
					case "1ANOS"
						cadaSqlCantPedidos=cadaSqlCantPedidos & " and YEAR(P.PED_FECHA)=" & AnoInicial + contMes -1
					case "2ANO"
						cadaSqlCantPedidos=cadaSqlCantPedidos & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & contMes
					case "3MES"
						cadaSqlCantPedidos=cadaSqlCantPedidos & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes & " And day(P.PED_FECHA)=" & contMes
				end select
	
				Set rsCantPedidos = connCRM.AbrirRecordSet(cadaSqlCantPedidos,3,1) 	
				if not rsCantPedidos.eof then
					if not isnull(rsCantPedidos("cantPedidos")) And trim(rsCantPedidos("cantPedidos"))<>"0"  then
						cadaCantPedido=round(rsCantPedidos("cantPedidos"),2)
						tieneIngresos=true						
					else
						cadaCantPedido=0
					end if
				end if
				rsCantPedidos.cerrar()
				set rsCantPedidos=nothing
	
				redim preserve arrIngresos(contMes)
				arrIngresos(contMes)=cadaCantPedido
				cantIngresosVentas=cantIngresosVentas + cadaCantPedido
				
		elseif tieneIngresos then 'else de if not sacarComparadores then			
			
				cadaCantPedido=0	
				cadaSqlCantPedidos=sqlCantPedidos						
				cadaSqlCantPedidos=cadaSqlCantPedidos & " and YEAR(P.PED_FECHA)=" & cadaAnno & " And month(P.PED_FECHA)=" & contMes
				Set rsCantPedidos = connCRM.AbrirRecordSet(cadaSqlCantPedidos,3,1) 	
				if not rsCantPedidos.eof then
					if not isnull(rsCantPedidos("cantPedidos")) And trim(rsCantPedidos("cantPedidos"))<>"0"  then
						cadaCantPedido=round(rsCantPedidos("cantPedidos"),2)
						tieneIngresos=true												
					else
						cadaCantPedido=0
					end if
				end if
				rsCantPedidos.cerrar()
				set rsCantPedidos=nothing			
			
				arrCantPedido(contComp)=arrCantPedido(contComp) + cadaCantPedido			
				arrIngresosComp(contMes,contComp)=cadaCantPedido
			
		end if 'end de if not sacarComparadores then					
			


		''Comprabamos el tipo de recorrido que estamos haciendo.	
		if not sacarComparadores then
				''Suma gastos envio
				cadaCantGastosEnvioEntrega=0								
				cadaSqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega
				Select case modoVista
					case "1ANOS"
						cadaSqlCantGastosEnvioEntrega=cadaSqlCantGastosEnvioEntrega & " and YEAR(P.PED_FECHA)=" & AnoInicial + contMes -1
					case "2ANO"
						cadaSqlCantGastosEnvioEntrega=cadaSqlCantGastosEnvioEntrega & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & contMes
					case "3MES"
						cadaSqlCantGastosEnvioEntrega=cadaSqlCantGastosEnvioEntrega & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes & " And day(P.PED_FECHA)=" & contMes
				end select                    
	
				Set rsCantGastosEnvioEntrega = connCRM.AbrirRecordSet(cadaSqlCantGastosEnvioEntrega,3,1) 	
				if not rsCantGastosEnvioEntrega.eof then
					if not isnull(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega")) And trim(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega"))<>"0"  then
						cadaCantGastosEnvioEntrega=round(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega"),2)
						tieneGastosEnvioEntrega=true						
					else
						cadaCantGastosEnvioEntrega=0	
					end if
				end if
				rsCantGastosEnvioEntrega.cerrar()
				set rsCantGastosEnvioEntrega=nothing
	
				redim preserve arrGastosEnvioEntrega(contMes)
				arrGastosEnvioEntrega(contMes)=cadaCantGastosEnvioEntrega	
				cantResumenGastosEnvioDomicilio=cantResumenGastosEnvioDomicilio + cadaCantGastosEnvioEntrega

		elseif tieneGastosEnvioEntrega then 'else de if not sacarComparadores then			
			
				cadaCantGastosEnvioEntrega=0								
				cadaSqlCantGastosEnvioEntrega=sqlCantGastosEnvioEntrega
				cadaSqlCantGastosEnvioEntrega=cadaSqlCantGastosEnvioEntrega & " and YEAR(P.PED_FECHA)=" & cadaAnno & " And month(P.PED_FECHA)=" & contMes
	
				Set rsCantGastosEnvioEntrega = connCRM.AbrirRecordSet(cadaSqlCantGastosEnvioEntrega,3,1) 	
				if not rsCantGastosEnvioEntrega.eof then
					if not isnull(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega")) And trim(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega"))<>"0"  then
						cadaCantGastosEnvioEntrega=round(rsCantGastosEnvioEntrega("cantGastosEnvioEntrega"),2)
						tieneGastosEnvioEntrega=true												
					else
						cadaCantGastosEnvioEntrega=0	
					end if
				end if
				rsCantGastosEnvioEntrega.cerrar()
				set rsCantGastosEnvioEntrega=nothing			
			
			arrCantGastosEnvioEntrega(contComp)=arrCantGastosEnvioEntrega(contComp) + cadaCantGastosEnvioEntrega			
			arrGastosEnvioEntregaComp(contMes,contComp)=cadaCantGastosEnvioEntrega
			
		end if 'end de if not sacarComparadores then	

			
			
		''Comprabamos el tipo de recorrido que estamos haciendo.	
		if not sacarComparadores then			
				cadaCantGastosEnvioDevolucion=0		
				cadaSqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion
				Select case modoVista
					case "1ANOS"
						cadaSqlCantGastosEnvioDevolucion=cadaSqlCantGastosEnvioDevolucion & " and YEAR(P.PED_FECHA)=" & AnoInicial + contMes -1
					case "2ANO"
						cadaSqlCantGastosEnvioDevolucion=cadaSqlCantGastosEnvioDevolucion & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & contMes
					case "3MES"
						cadaSqlCantGastosEnvioDevolucion=cadaSqlCantGastosEnvioDevolucion & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes & " And day(P.PED_FECHA)=" & contMes
				end select                      	
		
				Set rsCantGastosEnvioDevolucion = connCRM.AbrirRecordSet(cadaSqlCantGastosEnvioDevolucion,3,1) 	
				if not rsCantGastosEnvioDevolucion.eof then
					if not isnull(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion")) And trim(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion"))<>"0" then
						cadaCantGastosEnvioDevolucion=round(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion"),2)
						tieneGastosEnvioDevolucion=true						
					else
						cadaCantGastosEnvioDevolucion=0	
					end if			
				end if
				rsCantGastosEnvioDevolucion.cerrar()
				set rsCantGastosEnvioDevolucion=nothing
	
				redim preserve arrGastosEnvioDevolucion(contMes)
				arrGastosEnvioDevolucion(contMes)=cadaCantGastosEnvioDevolucion
				cantResumenGastosEnvioDevolucion=cantResumenGastosEnvioDevolucion + cadaCantGastosEnvioDevolucion
				
		elseif tieneGastosEnvioDevolucion then 'else de if not sacarComparadores then			
			
				cadaCantGastosEnvioDevolucion=0		
				cadaSqlCantGastosEnvioDevolucion=sqlCantGastosEnvioDevolucion
				cadaSqlCantGastosEnvioDevolucion=cadaSqlCantGastosEnvioDevolucion & " and YEAR(P.PED_FECHA)=" & cadaAnno & " And month(P.PED_FECHA)=" & contMes
		
				Set rsCantGastosEnvioDevolucion = connCRM.AbrirRecordSet(cadaSqlCantGastosEnvioDevolucion,3,1) 	
				if not rsCantGastosEnvioDevolucion.eof then
					if not isnull(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion")) And trim(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion"))<>"0" then
						cadaCantGastosEnvioDevolucion=round(rsCantGastosEnvioDevolucion("cantGastosEnvioDevolucion"),2)
						tieneGastosEnvioDevolucion=true												
					else
						cadaCantGastosEnvioDevolucion=0	
					end if			
				end if
				rsCantGastosEnvioDevolucion.cerrar()
				set rsCantGastosEnvioDevolucion=nothing			
			
				arrCantGastosEnvioDevolucion(contComp)=arrCantGastosEnvioDevolucion(contComp) + cadaCantGastosEnvioDevolucion			
				arrGastosEnvioDevolucionComp(contMes,contComp)=cadaCantGastosEnvioDevolucion
			
		end if 'end de if not sacarComparadores then					
			
			
			
		''Comprabamos el tipo de recorrido que estamos haciendo.	
		if not sacarComparadores then
					
				cadaCantGastosEnvioCambioTalla=0	
				cadaSqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla
				Select case modoVista
					case "1ANOS"
						cadaSqlCantGastosEnvioCambioTalla=cadaSqlCantGastosEnvioCambioTalla & " and YEAR(P.PED_FECHA)=" & AnoInicial + contMes -1
					case "2ANO"
						cadaSqlCantGastosEnvioCambioTalla=cadaSqlCantGastosEnvioCambioTalla & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & contMes
					case "3MES"
						cadaSqlCantGastosEnvioCambioTalla=cadaSqlCantGastosEnvioCambioTalla & " and YEAR(P.PED_FECHA)=" & anno & " And month(P.PED_FECHA)=" & Mes & " And day(P.PED_FECHA)=" & contMes
				end select                      	
		
				Set rsCantGastosEnvioCambioTalla = connCRM.AbrirRecordSet(cadaSqlCantGastosEnvioCambioTalla,3,1) 	
				if not rsCantGastosEnvioCambioTalla.eof then
					if not isnull(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla")) And trim(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla"))<>"0" then
						cadaCantGastosEnvioCambioTalla=round(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla"),2)
						tieneGastosEnvioCambioTalla=true						
					else
						cadaCantGastosEnvioCambioTalla=0	
					end if				
				end if
				rsCantGastosEnvioCambioTalla.cerrar()
				set rsCantGastosEnvioCambioTalla=nothing
	
				redim preserve arrGastosEnvioCambioTalla(contMes)
				arrGastosEnvioCambioTalla(contMes)=cadaCantGastosEnvioCambioTalla			
				cantResumenGastosEnvioCambioTalla=cantResumenGastosEnvioCambioTalla + cadaCantGastosEnvioCambioTalla

		elseif tieneGastosEnvioCambioTalla then 'else de if not sacarComparadores then			
			
				cadaCantGastosEnvioCambioTalla=0	
				cadaSqlCantGastosEnvioCambioTalla=sqlCantGastosEnvioCambioTalla
				cadaSqlCantGastosEnvioCambioTalla=cadaSqlCantGastosEnvioCambioTalla & " and YEAR(P.PED_FECHA)=" & cadaAnno & " And month(P.PED_FECHA)=" & contMes
		
				Set rsCantGastosEnvioCambioTalla = connCRM.AbrirRecordSet(cadaSqlCantGastosEnvioCambioTalla,3,1) 	
				if not rsCantGastosEnvioCambioTalla.eof then
					if not isnull(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla")) And trim(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla"))<>"0" then
						cadaCantGastosEnvioCambioTalla=round(rsCantGastosEnvioCambioTalla("cantGastosEnvioCambioTalla"),2)
						tieneGastosEnvioCambioTalla=true												
					else
						cadaCantGastosEnvioCambioTalla=0	
					end if				
				end if
				rsCantGastosEnvioCambioTalla.cerrar()
				set rsCantGastosEnvioCambioTalla=nothing			
			
				arrCantGastosEnvioCambioTalla(contComp)=arrCantGastosEnvioCambioTalla(contComp) + cadaCantGastosEnvioCambioTalla			
				arrGastosEnvioCambioTallaComp(contMes,contComp)=cadaCantGastosEnvioCambioTalla
			
		end if 'end de if not sacarComparadores then				
			
%>