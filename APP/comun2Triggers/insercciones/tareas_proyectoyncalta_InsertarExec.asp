<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->

<!-- #include virtual="/dmcrm/conf/enviarEmail.asp"-->

<%
' En Codigo tenemos el c�digo de la tarea
codigo=session(denominacion & "_codigoelemento")

sql="Select * from ((proyectos LEFT join analistas on proyectos.proy_suanalista=analistas.ana_codigo) LEFT join clientes on clientes.clie_codigo=proyectos.proy_sucliente) left join tareas on tareas.tar_suproyecto=proyectos.proy_codigo"
sql=sql & " where tar_codigo=" & codigo
sql=sql & " order by proy_codigo desc"
response.Write(sql)
Set rstProyecto= connCRM.AbrirRecordset(sql,0,1)

'Hallamos el que la ha creado
creadoPor=""
idAna=session("codigoUsuario")
if trim(idAna)<>"" then
	sql="select ana_Nombre from analistas where ANA_CODIGO=" & idAna
	Set rstAnalistas=connCRM.AbrirRecordset(sql,0,1)
	if not rstAnalistas.eof then
		creadoPor=rstAnalistas("ana_Nombre")
	end if
	rstAnalistas.cerrar
	set rstAnalistas=nothing
end if


'Componemos el cuerpo del mensaje:
strBody= "Este email es un aviso de que se ha creado una tarea en un proyecto donde <b>" & rstProyecto("ana_nombre") & "</b> es el responsable.  Valida la tarea. Los datos son los siguientes:<br><br>"
strBody=strBody & "Nueva tarea creada: " & "cod " & codigo & "<br>"
strBody=strBody & "Descripci�n: " & rstProyecto("tar_descripcion") & "<br>"
strBody=strBody & "Descripci�n interna: " & rstProyecto("tar_descripcioninterna") & "<br>"
strBody=strBody & "Tiempo estimado: " & rstProyecto("tar_tiempoestimado") & "<br>"
strBody=strBody & "Proyecto: " & rstProyecto("proy_descripcion") & "<br>"
strBody=strBody & "Cliente: " & rstProyecto("clie_nombre") & "<br>"
if trim(creadoPor)<>"" then
	strBody=strBody & "Creada por: " & creadoPor & "<br><br>"
end if
strComercial=rstProyecto("ana_email")


'Enviamos email al comercial encargado del proyecto
'Preparamos y llamamos a la funci�n de env�o ************************************************************************************************
MailHost = "smtp.1and1.es"              
MailUsername = ""
MailPassword = ""			
MailFrom = ""              
MailFromName = "dpto. producci�n" 
MailAddAddress = strComercial
MailSubject = "Creada NUEVA TAREA " & "[" & codigo & "]"
MailBody = strBody
NumError=0
DescripcionError=""

EnvioEmailDM NumError,DescripcionError,MailHost,MailUsername,MailPassword,MailFrom,MailFromName,MailAddAddress,MailAddBcc,MailSubject,MailBody
If NumError <> 0 Then 
  Response.write "<tr><td>" & DescripcionError & "</td></tr>"
end if	

rstProyecto.cerrar
set rstProyecto=nothing
%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->