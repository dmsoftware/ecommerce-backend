<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!-- #include virtual="/dmcrm/APP/comun2/FuncionesMaestra.inc"  -->
<% 
' En ids tenemos el c�digo
codigo = session(denominacion & "_codigoelemento")

if trim(codigo)<>"" then

    'Abrimos la conexi�n de la base de datos en ARSYS DMI_DMINTEGRA
    conexionAux = "Provider=SQLNCLI10;Data Source=;Database=;Uid=;Password="
    set conexionAux = new DMWConexion
    conexionAux.abrir ("" & conexionMaestra)
    
	'        **********************************************
	'        ************* Acciones a realizar ************
	'        **********************************************

	'***************************************************************************************
	' 1) Eliminamos los registros relacionados en diferentes tablas
	'***************************************************************************************
			''conexionAux.beginTrans

				'Eliminamos los LOC_RESULTADOS
				Sql= "Delete From LOC_RESULTADOS where LOCR_SUDOMINIO=" & Codigo  
				conexionAux.execute Sql,intNumRegistros
			
				'Hallamos LCOD_DOMINIO
				sql = "Select LOCD_DOMINIO,LOCD_CODIGO from LOC_DOMINIOS where LOCD_CODIGO=" & Codigo  
				Set rsMiCodigo = conexionAux.AbrirRecordset(sql,0,1)

				'Eliminamos los registros en POS_POSICIONES Y POS_TERMINOS
				sql = "Select * from POS_TERMINOS where TERM_DOMINIO=" & rsMiCodigo("LOCD_DOMINIO")
				Set rsTerminos = conexionAux.AbrirRecordset(sql,0,1)			
				while not rsTerminos.eof
					
					'Borro las posiciones del t�rmino
					Sql= "Delete From POS_POSICIONES where POS_SUTERMINO=" & rsTerminos("term_codigo")  
					conexionAux.execute Sql,intNumRegistros
								
					rsTerminos.movenext
				wend
				rsTerminos.cerrar
				set rsTerminos=nothing
			
				'Eliminamos los t�rminos a posiconar
				Sql= "Delete From POS_TERMINOS where TERM_DOMINIO=" & rsMiCodigo("LOCD_DOMINIO")
				conexionAux.execute Sql,intNumRegistros

				'Eliminamos los registros de CM_OBJETIVOS
				Sql= "Delete From CM_OBJETIVOS where CMO_SUDOMINIO=" & rsMiCodigo("LOCD_CODIGO")
				conexionAux.execute Sql,intNumRegistros

				'Eliminamos los registros de CM_INDICADORES_DOMINIO
				Sql= "Delete From CM_INDICADORES_DOMINIO where CMID_SUDOMINIO=" & rsMiCodigo("LOCD_CODIGO")
				conexionAux.execute Sql,intNumRegistros

				'Eliminamos los registros de CM_INDICADORES_EXTERNOS
				Sql= "Delete From CM_INDICADORES_EXTERNOS where CMIE_SUDOMINIO=" & rsMiCodigo("LOCD_CODIGO")
				conexionAux.execute Sql,intNumRegistros

				'Eliminamos los registros de LOC_DOMINIOS_IMPACTOSAMEDIDA
				Sql= "Delete From LOC_DOMINIOS_IMPACTOSAMEDIDA where LOCDI_SUDOMINIO=" & rsMiCodigo("LOCD_CODIGO")
				conexionAux.execute Sql,intNumRegistros


				rsMiCodigo.cerrar			

			''conexionAux.CommitTrans

	'************************************************************************************************************
	' 2) Eliminamos las bases de datos de BDLOGS (logs_dominio_a�o.mdb)--> las renombramos en OBTENERPOSICION.ASP
	'************************************************************************************************************

	'************************************************************************************************************
	' 3) Eliminar ficheros en www.dominio.com/dominio/FlogP
	'     por ahora no hacemos, habr� que hacer a mano.  De este modo podemos recuperarlo
	'************************************************************************************************************

	'************************************************************************************************************
	' 4) Damos de baja en el dominio el servicio y reescribimos el conf
			Sql= "Select LOCD_NOMBREDOMINIO From LOC_dominios where LOCD_CODIGO=" & Codigo  
			Set rstDominio= conexionAux.AbrirRecordset(sql,0,1)

			strUrl= "http://www." & rstDominio("LOCD_NOMBREDOMINIO") & "/modulos/accionesbd/sql.asp?clave=pswx&sentencia=" & SERVER.URLEncode("update conf set conf_valor=0 WHERE CONF_VARIABLE='ServDMLocalizacion'") 
			strUrl2= "http://www." & rstDominio("LOCD_NOMBREDOMINIO") & "/MODULOS/conf/reescribir.asp"
			strAuxResultados=getCodigo(strUrl)
			strAuxResultados=getCodigo(strUrl2)
			
			rstDominio.cerrar
			set rstDominio=nothing
	'************************************************************************************************************
	
	conexionAux.cerrar

end if	

function getCodigo(url)
     'obtiene el c�digo fuente de la p�gina
     ''Response.Buffer = True
     Dim  xml
     Set xml = Server.CreateObject("Microsoft.XMLHTTP")
     
     on error resume next
     
     xml.Open "GET", url, False 
     xml.Send

     codigo = xml.responseText
     if err.number<>0 then
        'Si no existe la url el error es -2146697211 
                codigo = "error"
                err.Clear
     end if
     set xml = nothing
     getCodigo = codigo 
end function
%>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->