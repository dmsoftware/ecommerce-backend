<%
if trim(lcase(request.ServerVariables("SERVER_NAME")))<>"dmsrv06" then
	 if trim(lcase(request.ServerVariables("HTTPS")))="off" then
	'	response.Redirect("https://crm.opeconsultores.es/dmcrm/index.asp") 
	 end if	
end if	 
%>

<!-- #include virtual="/dmcrm/conf/conf.asp"-->
<!-- #include virtual="/dmcrm/conf/conbd.asp"-->
<!-- #include virtual="/dmcrm/conf/inc_func_asp.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/dmcrm/css/estilos.css" />
    <link rel="stylesheet" href="/dmcrm/js/alerts/jquery.alerts.css" />
    <link rel="shortcut icon" href="/dmcrm/favicon.ico">
    <script type="text/javascript" src="/dmcrm/js/jquery.js"></script>
    <script type="text/javascript" src="/dmcrm/js/jquery.livequery.js"></script>
    <script type="text/javascript" language="javascript" src="/dmcrm/js/alerts/jquery.alerts.js"></script> 

    <title>DM-CRM</title>

    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
</head>
<body  class="loquinCMR" >

	<%  'Inicializamos las variables de sesi�n a vac�o
		session("esDirectorCuenta") = ""
		session("emailAdministracion") = ""
        session("codigoUsuario") = ""
		session("horaConex")=""
        session("DepartamentoUsuario")=""
        session("TipoUsuario")=""
        session("nombreUsuario")=""		

		'DMFidelizaComercio
		terminacionComerciosCSS=""
		if lcase(trim(request("tip")))="com" then
			terminacionComerciosCSS="_comercios"
		end if

		if trim(request.form("nombreus")) <> "" then
			email = quitaScripts(request.form("nombreus"))
			clave = quitaScripts(request.form("clave"))
			sql = "SELECT ANA_PASSWORD,ANA_DIRECTORCUENTA,ANA_CODIGO,ANA_FECHACAMBIOPASSWORD,ANA_SuDepartamento,ANA_IDIOMA,'analistas',ANA_NOMBRE as nomUsuario FROM ANALISTAS WHERE ANA_EMAIL = '" & email & "'"	
            'sql=sql & " UNION ALL " 
            'sql=sql & " SELECT COMER_PASSWORD,0,COMER_CODIGO,COMER_FECHACAMBIOPASSWORD,0,COMER_IDIOMA,'comercios',COMER_NOMBRE as nomUsuario FROM CLUB_COMERCIOS WHERE COMER_EMAIL = '" & email & "'"	          	
			Set rsClaves = connCRM.AbrirRecordset(sql,0,1)
			if not rsClaves.EOF then
				
				contrasena = decode(rsClaves(0)) 'decode(rsClaves("ANA_PASSWORD")) 'Decodificamos la contrase�a
                blnDirectorCuenta = rsClaves(1) 'rsClaves("ANA_DIRECTORCUENTA")
                codigoUsuario = rsClaves(2) 'rsClaves("ANA_CODIGO")
                FechaCambioPassword = rsClaves(3) 'rsClaves("ANA_FECHACAMBIOPASSWORD")
                SuDepartamento = rsClaves(4) 'rsClaves("ANA_SuDepartamento")
                IdiomaUsuarioEntrada = rsClaves(5) 'rsClaves("ANA_IDIOMA")
                TipoUsuario= rsClaves(6)
				nomUsuario=rsClaves(7)

                if isnull(contrasena) then contrasena=""
				if cstr(contrasena) = cstr(clave) then
					session("esDirectorCuenta") = blnDirectorCuenta
					session("emailAdministracion") = email
                    session("codigoUsuario") = codigoUsuario
					session("horaConex")=time()
                    session("DepartamentoUsuario")=SuDepartamento
                    session("TipoUsuario")=TipoUsuario
					session("nombreUsuario")=nomUsuario	
                    'La variable UltimoUsuarioIdioma est� leida de la cookie en conbd.asp
                    if UltimoUsuarioIdioma<>"" then
                        session("IdiomaUsuario")=UltimoUsuarioIdioma
                        'Modifico el idioma del usuario, por si acaso lo ha cambiado
                        sql="Update ANALISTAS SET ANA_IDIOMA='" & session("IdiomaUsuario") & "' WHERE ANA_CODIGO=" & session("codigoUsuario")
                        connCRM.execute sql,intNumeroRegistros
                    else
                        if rsClaves("ANA_IDIOMA")<>"" then
                            session("IdiomaUsuario")= IdiomaUsuarioEntrada
                        else
                            session("IdiomaUsuario")=DEFAULT_LANGUAGE 'Definida en CONF/CONBD.ASP
                        end if
                    end if
                    'Si la contrase�a es vacia (es la primera vez que entra el usuario) lo guardamos
                    if contrasena="" or isnull(contrasena) then 
                        session("ContrasenaVacia")=true
                    else
                        session("ContrasenaVacia")=false
                    end if

                    'Si la contrase�a hace m�s de un mes que no se ha cambiado lo guardamos
                    if not isnull(FechaCambioPassword) then
                        if datediff("d",FechaCambioPassword,date())>30 then 
                            session("ContrasenaCambiar")=true
                        else
                            session("ContrasenaCambiar")=false
                        end if
                    else
                        session("ContrasenaCambiar")=true
                    end if

					nomTipoUsuario="RHL_SUANALISTA"
					if trim(lcase(TipoUsuario))="comercios" then
						nomTipoUsuario="RHL_SUCOMERCIO"					
					end if
		            'Insertamos en la tabla log RH_LOGENTRADAS **************************************************************************
					sql=" Insert into RH_LOGENTRADAS (" & nomTipoUsuario & ",RHL_FECHALOG,RHL_HORALOG,RHL_IPLOG) values (" & codigoUsuario & ",'" & FormatearFechaSQLSever(date()) & " " & time() & "','" & time() & "','" & Request.ServerVariables("REMOTE_ADDR") & "')"

		            connCRM.execute sql,intNumRegistros
                    '********************************************************************************************************************
                    'Alberto.- 09/01/2012
                    'si la variable session("PagRetorno") contiene algo enviamos al usuario a la p�gina de retorno
                    'Alberto.- 8/6/2012.- Eliminamos la opci�n de volver a la p�gina donde nos echa ya que hemos detectado problemas cuando la p�gina donde perdemos la sesi�n es de modificaci�n de datos
                    'por ejemplo, modificaciones.asp de COMUN.
                    ''if session("PagRetorno")<>"" then
                    ''    strUrl=session("PagRetorno")
                    ''    session("PagRetorno")=""
                    ''    response.Redirect(strUrl)
                    ''else
					    response.Redirect("/dmcrm/APP/principal.asp")
                    ''end if

				else
					logError = true
				end if
			else
				logError = true
			end if
			rsClaves.cerrar
			Set rsClaves = nothing
		end if%>





	<div id="capacentro<%=terminacionComerciosCSS%>" >
    
		<%nomImgCabecera="/dmcrm/app/imagenes/DM-crm.png"
          if conClubCompra then
              nomImgCabecera="/dmcrm/app/imagenes/DM-fideliza.png"
          end if%>    
		<div class="capacentro2<%=terminacionComerciosCSS%>" style="background:url(<%=nomImgCabecera%>); ">
        
			<%	sql="select CONF_RUTAIMGCABECERA from CONFIGURACION "
				Set rsImgLogIn= connCRM.AbrirRecordset(sql,0,1)
				if not rsImgLogIn.eof then
					%><img src="<%=rsImgLogIn("CONF_RUTAIMGCABECERA")%>" class="logoI" /><%
				end if
				rsImgLogIn.cerrar()
				set rsImgLogIn=nothing%>
        	
        	<div class="c_texto<%=terminacionComerciosCSS%>">
			  <p class="pLogin<%=terminacionComerciosCSS%>">              
			  <form name="form1" action="index.asp" method="post" class="formulario">
              <div class="capacentro3<%=terminacionComerciosCSS%>" >
                        <div class="idiomasCabH<%=terminacionComerciosCSS%>">
                            <%Select case Idioma
                                case "es"
                                    Response.write "<b><a href=""index.asp?lang=eu"">eus</a></b> | cas"
                                case "eu"
                                    Response.write "eus | <b><a href=""index.asp?lang=es"">cas</a></b>"                            
                            end select%>
                        </div>
                        <div>
                            <label for="nombreus" class="c_izquerda"><%=objIdiomaGeneral.getIdioma("Index_1")%></label>
                            <%'Hallamos el email a a�adir en la caja de texto
                            if request("nombreus")<>"" then
                                strEmail=request("nombreus")
                            else
                                ''strEmail=Request.Cookies("UltimoUsuario")
                                'UltimoUsuario lo hemos leido en conbd.asp de la cookie 
                                strEmail=UltimoUsuario
                            end if%>
                            <div class="c_derecha"><input id="usuario" name="nombreus" type="text" value="<%=strEmail%>" class="cajaHome" size="12"/></div>
                        </div>
                        <div>
                            <label for="clave" class="c_izquerda"><%=objIdiomaGeneral.getIdioma("Index_2")%></label>
                            <div class="c_derecha"><input name="clave" id="password" type="password"  class="cajaHome" size="12"/></div>
                        </div>
                        <div style="margin:0 auto; width:180px;"><input type="submit" name="submit" class="button_150" value="<%=objIdiomaGeneral.getIdioma("Index_5")%>"></div>
                    </div>
				</form>
    		<span class="recorContra"><%=objIdiomaGeneral.getIdioma("Index_3")%></span>
            <div id="resultadoCorrecto" class="aInicio">
                <form id="formularioRecordar" action="/dmcrm/app/rrhh/ajaxRecordarContrasena.asp?id=<%=encode(year(date) & "935" & month(date) & day(date))%>">  
                	<a id="cerrarRecordarContra"><%=objIdiomaGeneral.getIdioma("Index_9")%></a><br/>
                    <strong><%=objIdiomaGeneral.getIdioma("Index_4")%></strong><br />
                    <input type="text" name="RecordarEmail" id="RecordarEmail" size="30" value="" tabindex="1">
                    <input type="submit" value="<%=objIdiomaGeneral.getIdioma("Index_7")%>" name="Recordar" id="Recordar" class="button_150"> 
                </form>   
            </div>
            <div id="resultadoError"></div>
            </div>
			<%if logError then%>
                <span class="errorLogin"><strong><%=objIdiomaGeneral.getIdioma("Index_6")%></strong></span><br />
            <%end if%>
            
    	</div>
        <!--<img style="float:right; margin-right:20px; top:-30px; position:relative;" src="/dmcrm/app/imagenes/GobiernoV.jpg" /> -->     
	</div>


    <%'*******************************************************************************************
    '*** Recordar contrase�a *********************************************************************%>
    <script type="text/javascript">
		
		$(document).ready(function() {
			$('.aInicio').css('display','none');
			$('.recorContra').click(function () { 
				$('.aInicio').toggle('slow');
			});
	        $('#cerrarRecordarContra').click(function () {
				$('#resultadoCorrecto').hide();										
				//$('.aInicio').show();														
			});										
		});
		
		
        $('#formularioRecordar').submit(function (event) {
            event.preventDefault();
            var url = $(this).attr('action');
            var datos = $(this).serialize();
            $.get(url, datos, function (resultado) {

                if (resultado.indexOf('<%=objIdiomaGeneral.getIdioma("Index_8")%>') == -1) {
                    $('#resultadoError').html(resultado);
                }
                else {
                    $('#resultadoCorrecto').html(resultado);
                    $('#resultadoError').html('');
                }
          
            });
        });  
    </script>
    <%'*** Fin Recordar contrase�a *********************************************************************
    '*******************************************************************************************%>

    <script type="text/javascript">
        $(document).ready(function () {
            <%if strEmail="" then%>
			    $('#usuario').focus();	
            <%else%>
                $('#password').focus();	
            <%end if%>
			$('input[type="text"]').addClass("cajaHome");
			$('input[type="password"]').addClass("cajaHome");
			$('input[type="text"]').focus(function() {
				$(this).removeClass("cajaHome").addClass("cajaHomeActiva");
				this.select();
			});
			$('input[type="text"]').blur(function() {
				$(this).removeClass("cajaHomeActiva").addClass("cajaHome");
			});
			$('input[type="password"]').focus(function() {
				$(this).removeClass("cajaHome").addClass("cajaHomeActiva");
			});
			$('input[type="password"]').blur(function() {
				$(this).removeClass("cajaHomeActiva").addClass("cajaHome");
			});
		});
	</script>

</body>
</html>
<!-- #include virtual="/dmcrm/conf/cerrarconbd.asp"-->